<!--
 * Copyright (c) 2022 CEA LIST, and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-FileType: DOCUMENTATION
 * SPDX-FileCopyrightText: 2020 Eclipse Foundation
 * SPDX-License-Identifier: EPL-2.0
 *
 -->


# Eclipse Papyrus for Manufacturing

* Papyrus4Manufacturing is graphical modeling tool for Industry 4.0 applications that complies with the Asset Administration Shell specification.
* https://www.eclipse.org/papyrus/components/manufacturing/

## Sub Projects

### AAS Designer

### OPC UA Designer

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and/or testing purposes.



### Prerequisites

The things you need before installing the software.

### Installation

In order to install the software you will have to open the downloaded IDE and install it via the integrated installer.

## Compile


### Usefull commands


## Contributing

You can contribute to the Eclipse Papyrus project via [Gerrit] (https://git.eclipse.org/r/). 
If you are interested, you can see a detailed sequence in [this page] (https://wiki.eclipse.org/Gerrit).
Contributions should be done based on the master branch in order to be automatically verified by its associated Jenkins job. 


## Additional Documentation and Acknowledgments

* Project folder on server: https://git.eclipse.org/c/papyrus/org.eclipse.papyrus-manufacturing.git 
* News and documentation to contribute: https://wiki.eclipse.org/Papyrus
* Forum: https://www.eclipse.org/forums/index.php/f/121/
* Mailing list: https://accounts.eclipse.org/mailing-list/mdt-papyrus.dev


## Website

* https://www.eclipse.org/papyrus/components/manufacturing/
