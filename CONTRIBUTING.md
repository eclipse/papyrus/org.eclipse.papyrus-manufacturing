# Contributing to Eclipse Papyrus

Thanks for your interest in this project.

## Project description

Papyrus4Manufacturing is graphical modeling tool for Industry 4.0 applications 
that complies with the Asset Administration Shell specification.

## Developer resources

Information regarding source code management, builds, coding standards, and
more.

* https://projects.eclipse.org/projects/modeling.mdt.papyrus/developer
* https://wiki.eclipse.org/Papyrus_Developer_Guide

The project maintains the following source code repositories

* https://git.eclipse.org/c/papyrus/org.eclipse.papyrus-manufacturing.git/ 

This project uses Bugzilla to track ongoing development and issues.

* Search for issues: https://bugs.eclipse.org/bugs/buglist.cgi?product=Papyrus&component=Manufacturing
* Create a new report:
   https://bugs.eclipse.org/bugs/enter_bug.cgi?product=Papyrus&component=Manufacturing

Be sure to search for existing bugs before you create another one. Remember that
contributions are always welcome!

## Eclipse Contributor Agreement

Before your contribution can be accepted by the project team contributors must
electronically sign the Eclipse Contributor Agreement (ECA).

* http://www.eclipse.org/legal/ECA.php

Commits that are provided by non-committers must have a Signed-off-by field in
the footer indicating that the author is aware of the terms by which the
contribution has been provided to the project. The non-committer must
additionally have an Eclipse Foundation account and must have a signed Eclipse
Contributor Agreement (ECA) on file.

For more information, please see the Eclipse Committer Handbook:
https://www.eclipse.org/projects/handbook/#resources-commit

## Contact

Contact the project developers via the project's "dev" list.

* https://dev.eclipse.org/mailman/listinfo/mdt-papyrus.dev

