/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
package org.eclipse.papyrus.opcua.uml2opcua.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;  
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdapterManager;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.m2m.qvt.oml.ModelExtent;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;
import org.eclipse.ui.handlers.HandlerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.eclipse.papyrus.infra.emf.utils.EMFHelper;
import org.eclipse.papyrus.opcua.uml2opcua.qvt.Uml2OpcuaTransformer;  



public class UMLToOPCUAQvtHandler extends AbstractHandler {

	private MessageConsole findConsole(String name) {
		ConsolePlugin plugin = ConsolePlugin.getDefault();
		IConsoleManager conMan = plugin.getConsoleManager();
		IConsole[] existing = conMan.getConsoles();
		for (int i = 0; i < existing.length; i++)
		if (name.equals(existing[i].getName()))
			return (MessageConsole) existing[i];
		//no console found, so create a new one
		MessageConsole myConsole = new MessageConsole(name, null);
		conMan.addConsoles(new IConsole[]{myConsole});
		return myConsole;
	}
	   
	private static final Logger logger = LoggerFactory.getLogger(UMLToOPCUAQvtHandler.class);
	
	@Override
    public Object execute(ExecutionEvent event) throws ExecutionException {
        System.out.println("TransformWithQvtHandler is executed");
        logger.info("Testing the logger");
        
        MessageConsole myConsole = findConsole("Console");
        MessageConsoleStream out = myConsole.newMessageStream();
        
        // get the selected uml file
    	IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
    	ISelectionService service = window.getSelectionService();
    	IStructuredSelection selection = (IStructuredSelection) service.getSelection();
    	
    	if(selection.size() > 1) // Error, only a single file can be transformed at the moment 
    		return null;
        
    	if (! (selection instanceof IStructuredSelection)) // cannot be transformed
    		return null;
    	Object selected = ((IStructuredSelection)selection).getFirstElement();
    	EObject selectedElement = EMFHelper.getEObject(selection.getFirstElement());
		
    	IAdapterManager am = Platform.getAdapterManager();
		IFile adapter = am.getAdapter(selected, IFile.class);
		IFile file = (IFile)adapter;
		String umlFilePath = file.getRawLocation().makeAbsolute().toString();
		if (!umlFilePath.endsWith("uml")) {
    		MessageDialog.openError(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Error", "The file extension needs to be uml");
			return null;
    	}
		URI resourceURI = selectedElement.eResource().getURI();
		
        String opcuaNodesetFilePath = umlFilePath.replace(".uml", ".xml");
        String opcuaNodeIdsCsvFilePath = umlFilePath.replace(".uml", ".csv");
		
		Uml2OpcuaTransformer transformer = new Uml2OpcuaTransformer();  
		
		// the transformer uses its internal UML to OPC UA QVTo transformation null is passed instead of a QVTo transformation file
        ModelExtent[] transformationResults = {null, null};
        
		boolean success = transformer.transform(resourceURI, null, transformationResults, out);
		System.out.println("2 calling QVTo transformation" + success + "test");
		if(success) {
    		ModelExtent opcuaNodesetOutput = transformationResults[0];
    		ModelExtent opcuaNodeIdsCsvOutput = transformationResults[1];

    		transformer.serializeNodeset(opcuaNodesetOutput, opcuaNodesetFilePath, out);
			transformer.serializeNodeIdsCsv(opcuaNodeIdsCsvOutput, opcuaNodeIdsCsvFilePath, out);
			
            // Refresh the workspace (causing the new file to appear in the Project Explorer)
            try {
            	file.getProject().refreshLocal( IResource.DEPTH_INFINITE, new NullProgressMonitor() );
    		} catch (CoreException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
		}
		
        // Refresh the workspace (causing the new file to appear in the Project Explorer)
        try {
        	file.getProject().refreshLocal( IResource.DEPTH_INFINITE, new NullProgressMonitor() );
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
        
        return null;
    }
}
