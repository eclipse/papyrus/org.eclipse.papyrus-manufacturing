/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.uml2opcua.nodeIdsCsv.NodeIdsCsv;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Row</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.opcua.uml2opcua.nodeIdsCsv.NodeIdsCsv.NodeIdsCsvRow#getSymbolName <em>Symbol Name</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.uml2opcua.nodeIdsCsv.NodeIdsCsv.NodeIdsCsvRow#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.uml2opcua.nodeIdsCsv.NodeIdsCsv.NodeIdsCsvRow#getNodeClass <em>Node Class</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.opcua.uml2opcua.nodeIdsCsv.NodeIdsCsv.NodeIdsCsvPackage#getNodeIdsCsvRow()
 * @model
 * @generated
 */
public interface NodeIdsCsvRow extends EObject {
	/**
	 * Returns the value of the '<em><b>Symbol Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symbol Name</em>' attribute.
	 * @see #setSymbolName(String)
	 * @see org.eclipse.papyrus.opcua.uml2opcua.nodeIdsCsv.NodeIdsCsv.NodeIdsCsvPackage#getNodeIdsCsvRow_SymbolName()
	 * @model
	 * @generated
	 */
	String getSymbolName();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.uml2opcua.nodeIdsCsv.NodeIdsCsv.NodeIdsCsvRow#getSymbolName <em>Symbol Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Symbol Name</em>' attribute.
	 * @see #getSymbolName()
	 * @generated
	 */
	void setSymbolName(String value);

	/**
	 * Returns the value of the '<em><b>Identifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identifier</em>' attribute.
	 * @see #setIdentifier(String)
	 * @see org.eclipse.papyrus.opcua.uml2opcua.nodeIdsCsv.NodeIdsCsv.NodeIdsCsvPackage#getNodeIdsCsvRow_Identifier()
	 * @model
	 * @generated
	 */
	String getIdentifier();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.uml2opcua.nodeIdsCsv.NodeIdsCsv.NodeIdsCsvRow#getIdentifier <em>Identifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Identifier</em>' attribute.
	 * @see #getIdentifier()
	 * @generated
	 */
	void setIdentifier(String value);

	/**
	 * Returns the value of the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node Class</em>' attribute.
	 * @see #setNodeClass(String)
	 * @see org.eclipse.papyrus.opcua.uml2opcua.nodeIdsCsv.NodeIdsCsv.NodeIdsCsvPackage#getNodeIdsCsvRow_NodeClass()
	 * @model
	 * @generated
	 */
	String getNodeClass();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.uml2opcua.nodeIdsCsv.NodeIdsCsv.NodeIdsCsvRow#getNodeClass <em>Node Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Node Class</em>' attribute.
	 * @see #getNodeClass()
	 * @generated
	 */
	void setNodeClass(String value);

} // NodeIdsCsvRow
