/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.uml2opcua.nodeIdsCsv.NodeIdsCsv;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>File</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.opcua.uml2opcua.nodeIdsCsv.NodeIdsCsv.NodeIdsCsvFile#getNodeIdsCsvRows <em>Node Ids Csv Rows</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.opcua.uml2opcua.nodeIdsCsv.NodeIdsCsv.NodeIdsCsvPackage#getNodeIdsCsvFile()
 * @model
 * @generated
 */
public interface NodeIdsCsvFile extends EObject {
	/**
	 * Returns the value of the '<em><b>Node Ids Csv Rows</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node Ids Csv Rows</em>' reference.
	 * @see #setNodeIdsCsvRows(NodeIdsCsvRow)
	 * @see org.eclipse.papyrus.opcua.uml2opcua.nodeIdsCsv.NodeIdsCsv.NodeIdsCsvPackage#getNodeIdsCsvFile_NodeIdsCsvRows()
	 * @model upper="-2"
	 * @generated
	 */
	NodeIdsCsvRow getNodeIdsCsvRows();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.uml2opcua.nodeIdsCsv.NodeIdsCsv.NodeIdsCsvFile#getNodeIdsCsvRows <em>Node Ids Csv Rows</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Node Ids Csv Rows</em>' reference.
	 * @see #getNodeIdsCsvRows()
	 * @generated
	 */
	void setNodeIdsCsvRows(NodeIdsCsvRow value);

} // NodeIdsCsvFile
