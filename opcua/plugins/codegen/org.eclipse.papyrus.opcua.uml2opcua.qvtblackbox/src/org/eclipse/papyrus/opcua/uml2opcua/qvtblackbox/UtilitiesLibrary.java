/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
package org.eclipse.papyrus.opcua.uml2opcua.qvtblackbox;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.m2m.qvt.oml.blackbox.java.Operation;
import org.eclipse.m2m.qvt.oml.blackbox.java.Parameter;
import org.eclipse.ocl.ecore.OrderedSetType;
import org.eclipse.ocl.util.Bag;
import org.eclipse.ocl.util.CollectionUtil;
  

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap.Entry;
import org.eclipse.emf.ecore.util.FeatureMapUtil;
public class UtilitiesLibrary {
	protected EClass cls;
	
	public UtilitiesLibrary() {
		super();
	}

	public Date createDate(String dateStr) {
		return (Date)EcoreFactory.eINSTANCE.createFromString(EcorePackage.eINSTANCE.getEDate(), dateStr);
	}
	
	public FeatureMap.Entry createEfeature(String value) {
		 	
		    Entry newEntry = FeatureMapUtil.createCDATAEntry(value);
		    return newEntry;
	}
	
	
//	public FeatureMap.Entry createFeatureMapEntry(EStructuralFeature eStructuralFeature, Object value)  {
//		FeatureMap.Entry.Internal prototype = ((EStructuralFeature.Internal)eStructuralFeature).getFeatureMapEntryPrototype();
//		 prototype.validate(value);
//		 return prototype.createEntry(value);
//	}

	public FeatureMap.Entry createFeatureMapEntry(String literals)  {
		setTmpModel(literals);
		return FeatureMapUtil.createEntry(getLiterals(), literals);
	}
	// Utils methods not directly accessible by QVTo
		//
		protected void setTmpModel(String literals) {
			// EStructuralFeature
			EAttribute stringEnum = EcoreFactory.eINSTANCE.createEAttribute();
			stringEnum.setName("LocalizedText");
			
			stringEnum.setEType(EcorePackage.eINSTANCE.getEString());

	 		// containing EClass
			cls = EcoreFactory.eINSTANCE.createEClass();
			cls.getEStructuralFeatures().add(stringEnum);
		}
		protected EStructuralFeature getLiterals() {
			return cls.getEStructuralFeature(0);
		}
		
	
	//returns the date on the XMLGregorianCalendar format
	public XMLGregorianCalendar createDateTime(String dateTimeString) {
		XMLGregorianCalendar xmlDate;
		if (dateTimeString.equals("currentDate")) {
			 // Create Date Object
	        Date current_date = new Date();
	 
			 // Gregorian Calendar object creation
	        GregorianCalendar gc = new GregorianCalendar();
	 
	        // giving current date and time to gc
	        gc.setTime(current_date);
	 
	        try {
	            xmlDate = DatatypeFactory.newInstance()
	                          .newXMLGregorianCalendar(gc);
	            return xmlDate;
	        }
	        catch (Exception e) {
	            e.printStackTrace();
	        }
		};
		LocalDate dob = null;
		//DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		//DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);

		
		try {
			xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(dateTimeString);
			System.out.println(xmlDate);
			return xmlDate;
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		return null;
		
	}
	
	@Operation(contextual=true)
	public static boolean before(Date self, Date when) {
		return self.before(when);
	}
	
	@Operation(contextual=true)
	public static List<String> split(
			String self, 
			@Parameter(name="regexp") String regexp) {
		return CollectionUtil.createNewSequence(Arrays.asList(self.split(regexp)));
	}

	@Operation(contextual=true)
	public static String getQualifiedName(EClassifier self) {
		return self.getEPackage().getName() + "::" + self.getName();
	}

	public static Set<LinkedHashSet<List<Bag<Boolean>>>> testAllCollectionTypes(boolean element) {
		final Bag<Boolean> createNewBag = CollectionUtil.createNewBag();
		createNewBag.add(element);
		final List<Bag<Boolean>> createNewSequence = CollectionUtil.createNewSequence();
		createNewSequence.add(createNewBag);
		final LinkedHashSet<List<Bag<Boolean>>> createNewOrderedSet = CollectionUtil.createNewOrderedSet();
		createNewOrderedSet.add(createNewSequence);
		final Set<LinkedHashSet<List<Bag<Boolean>>>> result = CollectionUtil.createNewSet();
		result.add(createNewOrderedSet);
		return result;
	}
}
