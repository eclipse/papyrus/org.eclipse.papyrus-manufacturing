/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package OPC_UA_Library.impl;

import OPC_UA_Library.OPC_UA_LibraryPackage;
import OPC_UA_Library.RsaEncryptedSecret;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Rsa Encrypted Secret</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RsaEncryptedSecretImpl extends BaseDataTypeImpl implements RsaEncryptedSecret {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RsaEncryptedSecretImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPC_UA_LibraryPackage.Literals.RSA_ENCRYPTED_SECRET;
	}

} //RsaEncryptedSecretImpl
