/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package OPC_UA_Library.impl;

import OPC_UA_Library.AnalogUnitType;
import OPC_UA_Library.Argument;
import OPC_UA_Library.BMP;
import OPC_UA_Library.BaseDataType;
import OPC_UA_Library.ByteString;
import OPC_UA_Library.DataValue;
import OPC_UA_Library.DateTime;
import OPC_UA_Library.DiagnosticInfo;
import OPC_UA_Library.Duration;
import OPC_UA_Library.EccEncryptedSecret;
import OPC_UA_Library.ExpandedNodeId;
import OPC_UA_Library.GIF;
import OPC_UA_Library.Guid;
import OPC_UA_Library.IdType;
import OPC_UA_Library.Image;
import OPC_UA_Library.Int16;
import OPC_UA_Library.Int32;
import OPC_UA_Library.Int64;
import OPC_UA_Library.JPG;
import OPC_UA_Library.LocalId;
import OPC_UA_Library.LocalizedText;
import OPC_UA_Library.MessageSecurityMode;
import OPC_UA_Library.NamingRuleType;
import OPC_UA_Library.NodeCLass;
import OPC_UA_Library.NodeId;
import OPC_UA_Library.OPC_UA_LibraryFactory;
import OPC_UA_Library.OPC_UA_LibraryPackage;
import OPC_UA_Library.PNG;
import OPC_UA_Library.QualifiedName;
import OPC_UA_Library.RedundancySupport;
import OPC_UA_Library.RsaEncryptedSecret;
import OPC_UA_Library.SByte;
import OPC_UA_Library.SecurityTokenRequestType;
import OPC_UA_Library.ServerState;
import OPC_UA_Library.StatusCode;
import OPC_UA_Library.Structure;
import OPC_UA_Library.UInt16;
import OPC_UA_Library.UInt32;
import OPC_UA_Library.UInt64;
import OPC_UA_Library.UInteger;
import OPC_UA_Library.XmlElement;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OPC_UA_LibraryFactoryImpl extends EFactoryImpl implements OPC_UA_LibraryFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OPC_UA_LibraryFactory init() {
		try {
			OPC_UA_LibraryFactory theOPC_UA_LibraryFactory = (OPC_UA_LibraryFactory)EPackage.Registry.INSTANCE.getEFactory(OPC_UA_LibraryPackage.eNS_URI);
			if (theOPC_UA_LibraryFactory != null) {
				return theOPC_UA_LibraryFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OPC_UA_LibraryFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPC_UA_LibraryFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case OPC_UA_LibraryPackage.ANALOG_UNIT_TYPE: return createAnalogUnitType();
			case OPC_UA_LibraryPackage.DOUBLE: return createDouble();
			case OPC_UA_LibraryPackage.NUMBER: return createNumber();
			case OPC_UA_LibraryPackage.BASE_DATA_TYPE: return createBaseDataType();
			case OPC_UA_LibraryPackage.LOCALIZED_TEXT: return createLocalizedText();
			case OPC_UA_LibraryPackage.LOCAL_ID: return createLocalId();
			case OPC_UA_LibraryPackage.STRING: return createString();
			case OPC_UA_LibraryPackage.DATE_TIME: return createDateTime();
			case OPC_UA_LibraryPackage.GUID: return createGuid();
			case OPC_UA_LibraryPackage.BYTE_STRING: return createByteString();
			case OPC_UA_LibraryPackage.XML_ELEMENT: return createXmlElement();
			case OPC_UA_LibraryPackage.NODE_ID: return createNodeId();
			case OPC_UA_LibraryPackage.EXPANDED_NODE_ID: return createExpandedNodeId();
			case OPC_UA_LibraryPackage.STATUS_CODE: return createStatusCode();
			case OPC_UA_LibraryPackage.QUALIFIED_NAME: return createQualifiedName();
			case OPC_UA_LibraryPackage.STRUCTURE: return createStructure();
			case OPC_UA_LibraryPackage.DATA_VALUE: return createDataValue();
			case OPC_UA_LibraryPackage.DIAGNOSTIC_INFO: return createDiagnosticInfo();
			case OPC_UA_LibraryPackage.RSA_ENCRYPTED_SECRET: return createRsaEncryptedSecret();
			case OPC_UA_LibraryPackage.ECC_ENCRYPTED_SECRET: return createEccEncryptedSecret();
			case OPC_UA_LibraryPackage.INTEGER: return createInteger();
			case OPC_UA_LibraryPackage.FLOAT: return createFloat();
			case OPC_UA_LibraryPackage.UINTEGER: return createUInteger();
			case OPC_UA_LibraryPackage.DURATION: return createDuration();
			case OPC_UA_LibraryPackage.INT64: return createInt64();
			case OPC_UA_LibraryPackage.INT32: return createInt32();
			case OPC_UA_LibraryPackage.INT16: return createInt16();
			case OPC_UA_LibraryPackage.SBYTE: return createSByte();
			case OPC_UA_LibraryPackage.BYTE: return createByte();
			case OPC_UA_LibraryPackage.ARGUMENT: return createArgument();
			case OPC_UA_LibraryPackage.UINT16: return createUInt16();
			case OPC_UA_LibraryPackage.UINT32: return createUInt32();
			case OPC_UA_LibraryPackage.UINT64: return createUInt64();
			case OPC_UA_LibraryPackage.IMAGE: return createImage();
			case OPC_UA_LibraryPackage.GIF: return createGIF();
			case OPC_UA_LibraryPackage.BMP: return createBMP();
			case OPC_UA_LibraryPackage.PNG: return createPNG();
			case OPC_UA_LibraryPackage.JPG: return createJPG();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case OPC_UA_LibraryPackage.NODE_CLASS:
				return createNodeCLassFromString(eDataType, initialValue);
			case OPC_UA_LibraryPackage.ID_TYPE:
				return createIdTypeFromString(eDataType, initialValue);
			case OPC_UA_LibraryPackage.NAMING_RULE_TYPE:
				return createNamingRuleTypeFromString(eDataType, initialValue);
			case OPC_UA_LibraryPackage.SECURITY_TOKEN_REQUEST_TYPE:
				return createSecurityTokenRequestTypeFromString(eDataType, initialValue);
			case OPC_UA_LibraryPackage.REDUNDANCY_SUPPORT:
				return createRedundancySupportFromString(eDataType, initialValue);
			case OPC_UA_LibraryPackage.MESSAGE_SECURITY_MODE:
				return createMessageSecurityModeFromString(eDataType, initialValue);
			case OPC_UA_LibraryPackage.SERVER_STATE:
				return createServerStateFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case OPC_UA_LibraryPackage.NODE_CLASS:
				return convertNodeCLassToString(eDataType, instanceValue);
			case OPC_UA_LibraryPackage.ID_TYPE:
				return convertIdTypeToString(eDataType, instanceValue);
			case OPC_UA_LibraryPackage.NAMING_RULE_TYPE:
				return convertNamingRuleTypeToString(eDataType, instanceValue);
			case OPC_UA_LibraryPackage.SECURITY_TOKEN_REQUEST_TYPE:
				return convertSecurityTokenRequestTypeToString(eDataType, instanceValue);
			case OPC_UA_LibraryPackage.REDUNDANCY_SUPPORT:
				return convertRedundancySupportToString(eDataType, instanceValue);
			case OPC_UA_LibraryPackage.MESSAGE_SECURITY_MODE:
				return convertMessageSecurityModeToString(eDataType, instanceValue);
			case OPC_UA_LibraryPackage.SERVER_STATE:
				return convertServerStateToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnalogUnitType createAnalogUnitType() {
		AnalogUnitTypeImpl analogUnitType = new AnalogUnitTypeImpl();
		return analogUnitType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPC_UA_Library.Double createDouble() {
		DoubleImpl double_ = new DoubleImpl();
		return double_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPC_UA_Library.Number createNumber() {
		NumberImpl number = new NumberImpl();
		return number;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseDataType createBaseDataType() {
		BaseDataTypeImpl baseDataType = new BaseDataTypeImpl();
		return baseDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocalizedText createLocalizedText() {
		LocalizedTextImpl localizedText = new LocalizedTextImpl();
		return localizedText;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocalId createLocalId() {
		LocalIdImpl localId = new LocalIdImpl();
		return localId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPC_UA_Library.String createString() {
		StringImpl string = new StringImpl();
		return string;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DateTime createDateTime() {
		DateTimeImpl dateTime = new DateTimeImpl();
		return dateTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Guid createGuid() {
		GuidImpl guid = new GuidImpl();
		return guid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ByteString createByteString() {
		ByteStringImpl byteString = new ByteStringImpl();
		return byteString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XmlElement createXmlElement() {
		XmlElementImpl xmlElement = new XmlElementImpl();
		return xmlElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodeId createNodeId() {
		NodeIdImpl nodeId = new NodeIdImpl();
		return nodeId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpandedNodeId createExpandedNodeId() {
		ExpandedNodeIdImpl expandedNodeId = new ExpandedNodeIdImpl();
		return expandedNodeId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatusCode createStatusCode() {
		StatusCodeImpl statusCode = new StatusCodeImpl();
		return statusCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QualifiedName createQualifiedName() {
		QualifiedNameImpl qualifiedName = new QualifiedNameImpl();
		return qualifiedName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Structure createStructure() {
		StructureImpl structure = new StructureImpl();
		return structure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataValue createDataValue() {
		DataValueImpl dataValue = new DataValueImpl();
		return dataValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiagnosticInfo createDiagnosticInfo() {
		DiagnosticInfoImpl diagnosticInfo = new DiagnosticInfoImpl();
		return diagnosticInfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RsaEncryptedSecret createRsaEncryptedSecret() {
		RsaEncryptedSecretImpl rsaEncryptedSecret = new RsaEncryptedSecretImpl();
		return rsaEncryptedSecret;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EccEncryptedSecret createEccEncryptedSecret() {
		EccEncryptedSecretImpl eccEncryptedSecret = new EccEncryptedSecretImpl();
		return eccEncryptedSecret;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPC_UA_Library.Integer createInteger() {
		IntegerImpl integer = new IntegerImpl();
		return integer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPC_UA_Library.Float createFloat() {
		FloatImpl float_ = new FloatImpl();
		return float_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UInteger createUInteger() {
		UIntegerImpl uInteger = new UIntegerImpl();
		return uInteger;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Duration createDuration() {
		DurationImpl duration = new DurationImpl();
		return duration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Int64 createInt64() {
		Int64Impl int64 = new Int64Impl();
		return int64;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Int32 createInt32() {
		Int32Impl int32 = new Int32Impl();
		return int32;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Int16 createInt16() {
		Int16Impl int16 = new Int16Impl();
		return int16;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SByte createSByte() {
		SByteImpl sByte = new SByteImpl();
		return sByte;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPC_UA_Library.Byte createByte() {
		ByteImpl byte_ = new ByteImpl();
		return byte_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Argument createArgument() {
		ArgumentImpl argument = new ArgumentImpl();
		return argument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UInt16 createUInt16() {
		UInt16Impl uInt16 = new UInt16Impl();
		return uInt16;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UInt32 createUInt32() {
		UInt32Impl uInt32 = new UInt32Impl();
		return uInt32;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UInt64 createUInt64() {
		UInt64Impl uInt64 = new UInt64Impl();
		return uInt64;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Image createImage() {
		ImageImpl image = new ImageImpl();
		return image;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GIF createGIF() {
		GIFImpl gif = new GIFImpl();
		return gif;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BMP createBMP() {
		BMPImpl bmp = new BMPImpl();
		return bmp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PNG createPNG() {
		PNGImpl png = new PNGImpl();
		return png;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JPG createJPG() {
		JPGImpl jpg = new JPGImpl();
		return jpg;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodeCLass createNodeCLassFromString(EDataType eDataType, String initialValue) {
		NodeCLass result = NodeCLass.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertNodeCLassToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdType createIdTypeFromString(EDataType eDataType, String initialValue) {
		IdType result = IdType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIdTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NamingRuleType createNamingRuleTypeFromString(EDataType eDataType, String initialValue) {
		NamingRuleType result = NamingRuleType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertNamingRuleTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SecurityTokenRequestType createSecurityTokenRequestTypeFromString(EDataType eDataType, String initialValue) {
		SecurityTokenRequestType result = SecurityTokenRequestType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSecurityTokenRequestTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RedundancySupport createRedundancySupportFromString(EDataType eDataType, String initialValue) {
		RedundancySupport result = RedundancySupport.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRedundancySupportToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessageSecurityMode createMessageSecurityModeFromString(EDataType eDataType, String initialValue) {
		MessageSecurityMode result = MessageSecurityMode.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMessageSecurityModeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServerState createServerStateFromString(EDataType eDataType, String initialValue) {
		ServerState result = ServerState.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertServerStateToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPC_UA_LibraryPackage getOPC_UA_LibraryPackage() {
		return (OPC_UA_LibraryPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static OPC_UA_LibraryPackage getPackage() {
		return OPC_UA_LibraryPackage.eINSTANCE;
	}

} //OPC_UA_LibraryFactoryImpl
