/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package OPC_UA_Library.util;

import OPC_UA_Library.AnalogUnitType;
import OPC_UA_Library.Argument;
import OPC_UA_Library.BMP;
import OPC_UA_Library.BaseDataType;
import OPC_UA_Library.ByteString;
import OPC_UA_Library.DataValue;
import OPC_UA_Library.DateTime;
import OPC_UA_Library.DiagnosticInfo;
import OPC_UA_Library.Duration;
import OPC_UA_Library.EccEncryptedSecret;
import OPC_UA_Library.ExpandedNodeId;
import OPC_UA_Library.GIF;
import OPC_UA_Library.Guid;
import OPC_UA_Library.Image;
import OPC_UA_Library.Int16;
import OPC_UA_Library.Int32;
import OPC_UA_Library.Int64;
import OPC_UA_Library.JPG;
import OPC_UA_Library.LocalId;
import OPC_UA_Library.LocalizedText;
import OPC_UA_Library.NodeId;
import OPC_UA_Library.OPC_UA_LibraryPackage;
import OPC_UA_Library.PNG;
import OPC_UA_Library.QualifiedName;
import OPC_UA_Library.RsaEncryptedSecret;
import OPC_UA_Library.SByte;
import OPC_UA_Library.StatusCode;
import OPC_UA_Library.Structure;
import OPC_UA_Library.UInt16;
import OPC_UA_Library.UInt32;
import OPC_UA_Library.UInt64;
import OPC_UA_Library.UInteger;
import OPC_UA_Library.XmlElement;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see OPC_UA_Library.OPC_UA_LibraryPackage
 * @generated
 */
public class OPC_UA_LibrarySwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static OPC_UA_LibraryPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPC_UA_LibrarySwitch() {
		if (modelPackage == null) {
			modelPackage = OPC_UA_LibraryPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case OPC_UA_LibraryPackage.ANALOG_UNIT_TYPE: {
				AnalogUnitType analogUnitType = (AnalogUnitType)theEObject;
				T result = caseAnalogUnitType(analogUnitType);
				if (result == null) result = caseDouble(analogUnitType);
				if (result == null) result = caseNumber(analogUnitType);
				if (result == null) result = caseBaseDataType(analogUnitType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.DOUBLE: {
				OPC_UA_Library.Double double_ = (OPC_UA_Library.Double)theEObject;
				T result = caseDouble(double_);
				if (result == null) result = caseNumber(double_);
				if (result == null) result = caseBaseDataType(double_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.NUMBER: {
				OPC_UA_Library.Number number = (OPC_UA_Library.Number)theEObject;
				T result = caseNumber(number);
				if (result == null) result = caseBaseDataType(number);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.BASE_DATA_TYPE: {
				BaseDataType baseDataType = (BaseDataType)theEObject;
				T result = caseBaseDataType(baseDataType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.LOCALIZED_TEXT: {
				LocalizedText localizedText = (LocalizedText)theEObject;
				T result = caseLocalizedText(localizedText);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.LOCAL_ID: {
				LocalId localId = (LocalId)theEObject;
				T result = caseLocalId(localId);
				if (result == null) result = caseString(localId);
				if (result == null) result = caseBaseDataType(localId);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.STRING: {
				OPC_UA_Library.String string = (OPC_UA_Library.String)theEObject;
				T result = caseString(string);
				if (result == null) result = caseBaseDataType(string);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.DATE_TIME: {
				DateTime dateTime = (DateTime)theEObject;
				T result = caseDateTime(dateTime);
				if (result == null) result = caseBaseDataType(dateTime);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.GUID: {
				Guid guid = (Guid)theEObject;
				T result = caseGuid(guid);
				if (result == null) result = caseBaseDataType(guid);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.BYTE_STRING: {
				ByteString byteString = (ByteString)theEObject;
				T result = caseByteString(byteString);
				if (result == null) result = caseBaseDataType(byteString);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.XML_ELEMENT: {
				XmlElement xmlElement = (XmlElement)theEObject;
				T result = caseXmlElement(xmlElement);
				if (result == null) result = caseBaseDataType(xmlElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.NODE_ID: {
				NodeId nodeId = (NodeId)theEObject;
				T result = caseNodeId(nodeId);
				if (result == null) result = caseBaseDataType(nodeId);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.EXPANDED_NODE_ID: {
				ExpandedNodeId expandedNodeId = (ExpandedNodeId)theEObject;
				T result = caseExpandedNodeId(expandedNodeId);
				if (result == null) result = caseBaseDataType(expandedNodeId);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.STATUS_CODE: {
				StatusCode statusCode = (StatusCode)theEObject;
				T result = caseStatusCode(statusCode);
				if (result == null) result = caseBaseDataType(statusCode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.QUALIFIED_NAME: {
				QualifiedName qualifiedName = (QualifiedName)theEObject;
				T result = caseQualifiedName(qualifiedName);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.STRUCTURE: {
				Structure structure = (Structure)theEObject;
				T result = caseStructure(structure);
				if (result == null) result = caseBaseDataType(structure);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.DATA_VALUE: {
				DataValue dataValue = (DataValue)theEObject;
				T result = caseDataValue(dataValue);
				if (result == null) result = caseBaseDataType(dataValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.DIAGNOSTIC_INFO: {
				DiagnosticInfo diagnosticInfo = (DiagnosticInfo)theEObject;
				T result = caseDiagnosticInfo(diagnosticInfo);
				if (result == null) result = caseBaseDataType(diagnosticInfo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.RSA_ENCRYPTED_SECRET: {
				RsaEncryptedSecret rsaEncryptedSecret = (RsaEncryptedSecret)theEObject;
				T result = caseRsaEncryptedSecret(rsaEncryptedSecret);
				if (result == null) result = caseBaseDataType(rsaEncryptedSecret);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.ECC_ENCRYPTED_SECRET: {
				EccEncryptedSecret eccEncryptedSecret = (EccEncryptedSecret)theEObject;
				T result = caseEccEncryptedSecret(eccEncryptedSecret);
				if (result == null) result = caseBaseDataType(eccEncryptedSecret);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.INTEGER: {
				OPC_UA_Library.Integer integer = (OPC_UA_Library.Integer)theEObject;
				T result = caseInteger(integer);
				if (result == null) result = caseNumber(integer);
				if (result == null) result = caseBaseDataType(integer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.FLOAT: {
				OPC_UA_Library.Float float_ = (OPC_UA_Library.Float)theEObject;
				T result = caseFloat(float_);
				if (result == null) result = caseNumber(float_);
				if (result == null) result = caseBaseDataType(float_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.UINTEGER: {
				UInteger uInteger = (UInteger)theEObject;
				T result = caseUInteger(uInteger);
				if (result == null) result = caseNumber(uInteger);
				if (result == null) result = caseBaseDataType(uInteger);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.DURATION: {
				Duration duration = (Duration)theEObject;
				T result = caseDuration(duration);
				if (result == null) result = caseDouble(duration);
				if (result == null) result = caseNumber(duration);
				if (result == null) result = caseBaseDataType(duration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.INT64: {
				Int64 int64 = (Int64)theEObject;
				T result = caseInt64(int64);
				if (result == null) result = caseInteger(int64);
				if (result == null) result = caseNumber(int64);
				if (result == null) result = caseBaseDataType(int64);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.INT32: {
				Int32 int32 = (Int32)theEObject;
				T result = caseInt32(int32);
				if (result == null) result = caseInteger(int32);
				if (result == null) result = caseNumber(int32);
				if (result == null) result = caseBaseDataType(int32);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.INT16: {
				Int16 int16 = (Int16)theEObject;
				T result = caseInt16(int16);
				if (result == null) result = caseInteger(int16);
				if (result == null) result = caseNumber(int16);
				if (result == null) result = caseBaseDataType(int16);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.SBYTE: {
				SByte sByte = (SByte)theEObject;
				T result = caseSByte(sByte);
				if (result == null) result = caseInteger(sByte);
				if (result == null) result = caseNumber(sByte);
				if (result == null) result = caseBaseDataType(sByte);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.BYTE: {
				OPC_UA_Library.Byte byte_ = (OPC_UA_Library.Byte)theEObject;
				T result = caseByte(byte_);
				if (result == null) result = caseUInteger(byte_);
				if (result == null) result = caseNumber(byte_);
				if (result == null) result = caseBaseDataType(byte_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.ARGUMENT: {
				Argument argument = (Argument)theEObject;
				T result = caseArgument(argument);
				if (result == null) result = caseStructure(argument);
				if (result == null) result = caseBaseDataType(argument);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.UINT16: {
				UInt16 uInt16 = (UInt16)theEObject;
				T result = caseUInt16(uInt16);
				if (result == null) result = caseUInteger(uInt16);
				if (result == null) result = caseNumber(uInt16);
				if (result == null) result = caseBaseDataType(uInt16);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.UINT32: {
				UInt32 uInt32 = (UInt32)theEObject;
				T result = caseUInt32(uInt32);
				if (result == null) result = caseUInteger(uInt32);
				if (result == null) result = caseNumber(uInt32);
				if (result == null) result = caseBaseDataType(uInt32);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.UINT64: {
				UInt64 uInt64 = (UInt64)theEObject;
				T result = caseUInt64(uInt64);
				if (result == null) result = caseUInteger(uInt64);
				if (result == null) result = caseNumber(uInt64);
				if (result == null) result = caseBaseDataType(uInt64);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.IMAGE: {
				Image image = (Image)theEObject;
				T result = caseImage(image);
				if (result == null) result = caseByteString(image);
				if (result == null) result = caseBaseDataType(image);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.GIF: {
				GIF gif = (GIF)theEObject;
				T result = caseGIF(gif);
				if (result == null) result = caseImage(gif);
				if (result == null) result = caseByteString(gif);
				if (result == null) result = caseBaseDataType(gif);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.BMP: {
				BMP bmp = (BMP)theEObject;
				T result = caseBMP(bmp);
				if (result == null) result = caseImage(bmp);
				if (result == null) result = caseByteString(bmp);
				if (result == null) result = caseBaseDataType(bmp);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.PNG: {
				PNG png = (PNG)theEObject;
				T result = casePNG(png);
				if (result == null) result = caseImage(png);
				if (result == null) result = caseByteString(png);
				if (result == null) result = caseBaseDataType(png);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_LibraryPackage.JPG: {
				JPG jpg = (JPG)theEObject;
				T result = caseJPG(jpg);
				if (result == null) result = caseImage(jpg);
				if (result == null) result = caseByteString(jpg);
				if (result == null) result = caseBaseDataType(jpg);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Analog Unit Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Analog Unit Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnalogUnitType(AnalogUnitType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Double</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Double</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDouble(OPC_UA_Library.Double object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Number</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Number</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNumber(OPC_UA_Library.Number object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Data Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Data Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseDataType(BaseDataType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Localized Text</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Localized Text</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLocalizedText(LocalizedText object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Local Id</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Local Id</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLocalId(LocalId object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseString(OPC_UA_Library.String object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Date Time</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Date Time</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDateTime(DateTime object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Guid</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Guid</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGuid(Guid object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Byte String</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Byte String</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseByteString(ByteString object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Xml Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Xml Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXmlElement(XmlElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Node Id</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node Id</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNodeId(NodeId object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Expanded Node Id</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expanded Node Id</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExpandedNodeId(ExpandedNodeId object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Status Code</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Status Code</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStatusCode(StatusCode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Qualified Name</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Qualified Name</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseQualifiedName(QualifiedName object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Structure</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Structure</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStructure(Structure object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataValue(DataValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Diagnostic Info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Diagnostic Info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDiagnosticInfo(DiagnosticInfo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rsa Encrypted Secret</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rsa Encrypted Secret</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRsaEncryptedSecret(RsaEncryptedSecret object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ecc Encrypted Secret</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ecc Encrypted Secret</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEccEncryptedSecret(EccEncryptedSecret object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Integer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Integer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInteger(OPC_UA_Library.Integer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Float</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Float</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFloat(OPC_UA_Library.Float object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UInteger</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UInteger</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUInteger(UInteger object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Duration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Duration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDuration(Duration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Int64</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Int64</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInt64(Int64 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Int32</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Int32</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInt32(Int32 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Int16</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Int16</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInt16(Int16 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SByte</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SByte</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSByte(SByte object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Byte</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Byte</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseByte(OPC_UA_Library.Byte object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Argument</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Argument</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseArgument(Argument object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UInt16</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UInt16</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUInt16(UInt16 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UInt32</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UInt32</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUInt32(UInt32 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UInt64</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UInt64</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUInt64(UInt64 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Image</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Image</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImage(Image object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GIF</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GIF</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGIF(GIF object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BMP</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BMP</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBMP(BMP object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PNG</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PNG</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePNG(PNG object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>JPG</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>JPG</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJPG(JPG object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //OPC_UA_LibrarySwitch
