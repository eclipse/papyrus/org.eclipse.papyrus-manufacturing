/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.di.opcuadiprofile;

import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int32;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.LocalizedText;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Block Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.BlockType#getTargetMode <em>Target Mode</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.BlockType#getRevisionCounter <em>Revision Counter</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.BlockType#getActualMode <em>Actual Mode</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.BlockType#getPermittedMode <em>Permitted Mode</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.BlockType#getNormalMode <em>Normal Mode</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getBlockType()
 * @model
 * @generated
 */
public interface BlockType extends TopologyElementType {
	/**
	 * Returns the value of the '<em><b>Target Mode</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Mode</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Mode</em>' containment reference.
	 * @see #setTargetMode(LocalizedText)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getBlockType_TargetMode()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	LocalizedText getTargetMode();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.BlockType#getTargetMode <em>Target Mode</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Mode</em>' containment reference.
	 * @see #getTargetMode()
	 * @generated
	 */
	void setTargetMode(LocalizedText value);

	/**
	 * Returns the value of the '<em><b>Revision Counter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Revision Counter</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Revision Counter</em>' containment reference.
	 * @see #setRevisionCounter(Int32)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getBlockType_RevisionCounter()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	Int32 getRevisionCounter();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.BlockType#getRevisionCounter <em>Revision Counter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Revision Counter</em>' containment reference.
	 * @see #getRevisionCounter()
	 * @generated
	 */
	void setRevisionCounter(Int32 value);

	/**
	 * Returns the value of the '<em><b>Actual Mode</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actual Mode</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actual Mode</em>' containment reference.
	 * @see #setActualMode(LocalizedText)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getBlockType_ActualMode()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	LocalizedText getActualMode();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.BlockType#getActualMode <em>Actual Mode</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Actual Mode</em>' containment reference.
	 * @see #getActualMode()
	 * @generated
	 */
	void setActualMode(LocalizedText value);

	/**
	 * Returns the value of the '<em><b>Permitted Mode</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Permitted Mode</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Permitted Mode</em>' containment reference.
	 * @see #setPermittedMode(LocalizedText)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getBlockType_PermittedMode()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	LocalizedText getPermittedMode();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.BlockType#getPermittedMode <em>Permitted Mode</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Permitted Mode</em>' containment reference.
	 * @see #getPermittedMode()
	 * @generated
	 */
	void setPermittedMode(LocalizedText value);

	/**
	 * Returns the value of the '<em><b>Normal Mode</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Normal Mode</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Normal Mode</em>' containment reference.
	 * @see #setNormalMode(LocalizedText)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getBlockType_NormalMode()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	LocalizedText getNormalMode();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.BlockType#getNormalMode <em>Normal Mode</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Normal Mode</em>' containment reference.
	 * @see #getNormalMode()
	 * @generated
	 */
	void setNormalMode(LocalizedText value);

} // BlockType
