/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.di.opcuadiprofile.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OPCUADIProfileFactoryImpl extends EFactoryImpl implements OPCUADIProfileFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OPCUADIProfileFactory init() {
		try {
			OPCUADIProfileFactory theOPCUADIProfileFactory = (OPCUADIProfileFactory)EPackage.Registry.INSTANCE.getEFactory(OPCUADIProfilePackage.eNS_URI);
			if (theOPCUADIProfileFactory != null) {
				return theOPCUADIProfileFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OPCUADIProfileFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPCUADIProfileFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case OPCUADIProfilePackage.FUNCTIONAL_GROUP_TYPE: return createFunctionalGroupType();
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE: return createLockingServicesType();
			case OPCUADIProfilePackage.PARAMETER_SET_TOPOLOGY_ELEMENT_TYPE: return createParameterSetTopologyElementType();
			case OPCUADIProfilePackage.METHOD_SET_TOPOLOGY_ELEMENT_TYPE: return createMethodSetTopologyElementType();
			case OPCUADIProfilePackage.DEVICE_TYPE: return createDeviceType();
			case OPCUADIProfilePackage.CONNECTION_POINT_TYPE: return createConnectionPointType();
			case OPCUADIProfilePackage.SOFTWARE_TYPE: return createSoftwareType();
			case OPCUADIProfilePackage.BLOCK_TYPE: return createBlockType();
			case OPCUADIProfilePackage.CONFIGURABLE_OBJECT_TYPE: return createConfigurableObjectType();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionalGroupType createFunctionalGroupType() {
		FunctionalGroupTypeImpl functionalGroupType = new FunctionalGroupTypeImpl();
		return functionalGroupType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LockingServicesType createLockingServicesType() {
		LockingServicesTypeImpl lockingServicesType = new LockingServicesTypeImpl();
		return lockingServicesType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterSetTopologyElementType createParameterSetTopologyElementType() {
		ParameterSetTopologyElementTypeImpl parameterSetTopologyElementType = new ParameterSetTopologyElementTypeImpl();
		return parameterSetTopologyElementType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MethodSetTopologyElementType createMethodSetTopologyElementType() {
		MethodSetTopologyElementTypeImpl methodSetTopologyElementType = new MethodSetTopologyElementTypeImpl();
		return methodSetTopologyElementType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeviceType createDeviceType() {
		DeviceTypeImpl deviceType = new DeviceTypeImpl();
		return deviceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionPointType createConnectionPointType() {
		ConnectionPointTypeImpl connectionPointType = new ConnectionPointTypeImpl();
		return connectionPointType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SoftwareType createSoftwareType() {
		SoftwareTypeImpl softwareType = new SoftwareTypeImpl();
		return softwareType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlockType createBlockType() {
		BlockTypeImpl blockType = new BlockTypeImpl();
		return blockType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfigurableObjectType createConfigurableObjectType() {
		ConfigurableObjectTypeImpl configurableObjectType = new ConfigurableObjectTypeImpl();
		return configurableObjectType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPCUADIProfilePackage getOPCUADIProfilePackage() {
		return (OPCUADIProfilePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static OPCUADIProfilePackage getPackage() {
		return OPCUADIProfilePackage.eINSTANCE;
	}

} //OPCUADIProfileFactoryImpl
