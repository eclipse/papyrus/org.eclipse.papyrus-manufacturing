/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.di.opcuadiprofile.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage;

import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int32;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.LocalizedText;

import org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseInterfaceTypeImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IVendor Nameplate Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.IVendorNameplateTypeImpl#getManufacturer <em>Manufacturer</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.IVendorNameplateTypeImpl#getManufacturerUri <em>Manufacturer Uri</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.IVendorNameplateTypeImpl#getModel <em>Model</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.IVendorNameplateTypeImpl#getProductCode <em>Product Code</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.IVendorNameplateTypeImpl#getHardwareRevision <em>Hardware Revision</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.IVendorNameplateTypeImpl#getSoftwareRevision <em>Software Revision</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.IVendorNameplateTypeImpl#getDeviceRevision <em>Device Revision</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.IVendorNameplateTypeImpl#getDeviceManual <em>Device Manual</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.IVendorNameplateTypeImpl#getDeviceClass <em>Device Class</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.IVendorNameplateTypeImpl#getSerialNumber <em>Serial Number</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.IVendorNameplateTypeImpl#getProductInstanceUri <em>Product Instance Uri</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.IVendorNameplateTypeImpl#getRevisionCounter <em>Revision Counter</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class IVendorNameplateTypeImpl extends BaseInterfaceTypeImpl implements IVendorNameplateType {
	/**
	 * The cached value of the '{@link #getManufacturer() <em>Manufacturer</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getManufacturer()
	 * @generated
	 * @ordered
	 */
	protected LocalizedText manufacturer;

	/**
	 * The default value of the '{@link #getManufacturerUri() <em>Manufacturer Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getManufacturerUri()
	 * @generated
	 * @ordered
	 */
	protected static final String MANUFACTURER_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getManufacturerUri() <em>Manufacturer Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getManufacturerUri()
	 * @generated
	 * @ordered
	 */
	protected String manufacturerUri = MANUFACTURER_URI_EDEFAULT;

	/**
	 * The cached value of the '{@link #getModel() <em>Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModel()
	 * @generated
	 * @ordered
	 */
	protected LocalizedText model;

	/**
	 * The default value of the '{@link #getProductCode() <em>Product Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProductCode()
	 * @generated
	 * @ordered
	 */
	protected static final String PRODUCT_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProductCode() <em>Product Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProductCode()
	 * @generated
	 * @ordered
	 */
	protected String productCode = PRODUCT_CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getHardwareRevision() <em>Hardware Revision</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHardwareRevision()
	 * @generated
	 * @ordered
	 */
	protected static final String HARDWARE_REVISION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHardwareRevision() <em>Hardware Revision</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHardwareRevision()
	 * @generated
	 * @ordered
	 */
	protected String hardwareRevision = HARDWARE_REVISION_EDEFAULT;

	/**
	 * The default value of the '{@link #getSoftwareRevision() <em>Software Revision</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSoftwareRevision()
	 * @generated
	 * @ordered
	 */
	protected static final String SOFTWARE_REVISION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSoftwareRevision() <em>Software Revision</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSoftwareRevision()
	 * @generated
	 * @ordered
	 */
	protected String softwareRevision = SOFTWARE_REVISION_EDEFAULT;

	/**
	 * The default value of the '{@link #getDeviceRevision() <em>Device Revision</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeviceRevision()
	 * @generated
	 * @ordered
	 */
	protected static final String DEVICE_REVISION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDeviceRevision() <em>Device Revision</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeviceRevision()
	 * @generated
	 * @ordered
	 */
	protected String deviceRevision = DEVICE_REVISION_EDEFAULT;

	/**
	 * The default value of the '{@link #getDeviceManual() <em>Device Manual</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeviceManual()
	 * @generated
	 * @ordered
	 */
	protected static final String DEVICE_MANUAL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDeviceManual() <em>Device Manual</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeviceManual()
	 * @generated
	 * @ordered
	 */
	protected String deviceManual = DEVICE_MANUAL_EDEFAULT;

	/**
	 * The default value of the '{@link #getDeviceClass() <em>Device Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeviceClass()
	 * @generated
	 * @ordered
	 */
	protected static final String DEVICE_CLASS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDeviceClass() <em>Device Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeviceClass()
	 * @generated
	 * @ordered
	 */
	protected String deviceClass = DEVICE_CLASS_EDEFAULT;

	/**
	 * The default value of the '{@link #getSerialNumber() <em>Serial Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSerialNumber()
	 * @generated
	 * @ordered
	 */
	protected static final String SERIAL_NUMBER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSerialNumber() <em>Serial Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSerialNumber()
	 * @generated
	 * @ordered
	 */
	protected String serialNumber = SERIAL_NUMBER_EDEFAULT;

	/**
	 * The default value of the '{@link #getProductInstanceUri() <em>Product Instance Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProductInstanceUri()
	 * @generated
	 * @ordered
	 */
	protected static final String PRODUCT_INSTANCE_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProductInstanceUri() <em>Product Instance Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProductInstanceUri()
	 * @generated
	 * @ordered
	 */
	protected String productInstanceUri = PRODUCT_INSTANCE_URI_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRevisionCounter() <em>Revision Counter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRevisionCounter()
	 * @generated
	 * @ordered
	 */
	protected Int32 revisionCounter;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IVendorNameplateTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUADIProfilePackage.Literals.IVENDOR_NAMEPLATE_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocalizedText getManufacturer() {
		return manufacturer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetManufacturer(LocalizedText newManufacturer, NotificationChain msgs) {
		LocalizedText oldManufacturer = manufacturer;
		manufacturer = newManufacturer;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__MANUFACTURER, oldManufacturer, newManufacturer);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setManufacturer(LocalizedText newManufacturer) {
		if (newManufacturer != manufacturer) {
			NotificationChain msgs = null;
			if (manufacturer != null)
				msgs = ((InternalEObject)manufacturer).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__MANUFACTURER, null, msgs);
			if (newManufacturer != null)
				msgs = ((InternalEObject)newManufacturer).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__MANUFACTURER, null, msgs);
			msgs = basicSetManufacturer(newManufacturer, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__MANUFACTURER, newManufacturer, newManufacturer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getManufacturerUri() {
		return manufacturerUri;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setManufacturerUri(String newManufacturerUri) {
		String oldManufacturerUri = manufacturerUri;
		manufacturerUri = newManufacturerUri;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__MANUFACTURER_URI, oldManufacturerUri, manufacturerUri));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocalizedText getModel() {
		return model;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetModel(LocalizedText newModel, NotificationChain msgs) {
		LocalizedText oldModel = model;
		model = newModel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__MODEL, oldModel, newModel);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModel(LocalizedText newModel) {
		if (newModel != model) {
			NotificationChain msgs = null;
			if (model != null)
				msgs = ((InternalEObject)model).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__MODEL, null, msgs);
			if (newModel != null)
				msgs = ((InternalEObject)newModel).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__MODEL, null, msgs);
			msgs = basicSetModel(newModel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__MODEL, newModel, newModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getProductCode() {
		return productCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProductCode(String newProductCode) {
		String oldProductCode = productCode;
		productCode = newProductCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__PRODUCT_CODE, oldProductCode, productCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHardwareRevision() {
		return hardwareRevision;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHardwareRevision(String newHardwareRevision) {
		String oldHardwareRevision = hardwareRevision;
		hardwareRevision = newHardwareRevision;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__HARDWARE_REVISION, oldHardwareRevision, hardwareRevision));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSoftwareRevision() {
		return softwareRevision;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSoftwareRevision(String newSoftwareRevision) {
		String oldSoftwareRevision = softwareRevision;
		softwareRevision = newSoftwareRevision;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__SOFTWARE_REVISION, oldSoftwareRevision, softwareRevision));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDeviceRevision() {
		return deviceRevision;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeviceRevision(String newDeviceRevision) {
		String oldDeviceRevision = deviceRevision;
		deviceRevision = newDeviceRevision;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__DEVICE_REVISION, oldDeviceRevision, deviceRevision));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDeviceManual() {
		return deviceManual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeviceManual(String newDeviceManual) {
		String oldDeviceManual = deviceManual;
		deviceManual = newDeviceManual;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__DEVICE_MANUAL, oldDeviceManual, deviceManual));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDeviceClass() {
		return deviceClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeviceClass(String newDeviceClass) {
		String oldDeviceClass = deviceClass;
		deviceClass = newDeviceClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__DEVICE_CLASS, oldDeviceClass, deviceClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSerialNumber() {
		return serialNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSerialNumber(String newSerialNumber) {
		String oldSerialNumber = serialNumber;
		serialNumber = newSerialNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__SERIAL_NUMBER, oldSerialNumber, serialNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getProductInstanceUri() {
		return productInstanceUri;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProductInstanceUri(String newProductInstanceUri) {
		String oldProductInstanceUri = productInstanceUri;
		productInstanceUri = newProductInstanceUri;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__PRODUCT_INSTANCE_URI, oldProductInstanceUri, productInstanceUri));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Int32 getRevisionCounter() {
		return revisionCounter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRevisionCounter(Int32 newRevisionCounter, NotificationChain msgs) {
		Int32 oldRevisionCounter = revisionCounter;
		revisionCounter = newRevisionCounter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__REVISION_COUNTER, oldRevisionCounter, newRevisionCounter);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRevisionCounter(Int32 newRevisionCounter) {
		if (newRevisionCounter != revisionCounter) {
			NotificationChain msgs = null;
			if (revisionCounter != null)
				msgs = ((InternalEObject)revisionCounter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__REVISION_COUNTER, null, msgs);
			if (newRevisionCounter != null)
				msgs = ((InternalEObject)newRevisionCounter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__REVISION_COUNTER, null, msgs);
			msgs = basicSetRevisionCounter(newRevisionCounter, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__REVISION_COUNTER, newRevisionCounter, newRevisionCounter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__MANUFACTURER:
				return basicSetManufacturer(null, msgs);
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__MODEL:
				return basicSetModel(null, msgs);
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__REVISION_COUNTER:
				return basicSetRevisionCounter(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__MANUFACTURER:
				return getManufacturer();
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__MANUFACTURER_URI:
				return getManufacturerUri();
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__MODEL:
				return getModel();
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__PRODUCT_CODE:
				return getProductCode();
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__HARDWARE_REVISION:
				return getHardwareRevision();
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__SOFTWARE_REVISION:
				return getSoftwareRevision();
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__DEVICE_REVISION:
				return getDeviceRevision();
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__DEVICE_MANUAL:
				return getDeviceManual();
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__DEVICE_CLASS:
				return getDeviceClass();
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__SERIAL_NUMBER:
				return getSerialNumber();
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__PRODUCT_INSTANCE_URI:
				return getProductInstanceUri();
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__REVISION_COUNTER:
				return getRevisionCounter();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__MANUFACTURER:
				setManufacturer((LocalizedText)newValue);
				return;
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__MANUFACTURER_URI:
				setManufacturerUri((String)newValue);
				return;
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__MODEL:
				setModel((LocalizedText)newValue);
				return;
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__PRODUCT_CODE:
				setProductCode((String)newValue);
				return;
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__HARDWARE_REVISION:
				setHardwareRevision((String)newValue);
				return;
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__SOFTWARE_REVISION:
				setSoftwareRevision((String)newValue);
				return;
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__DEVICE_REVISION:
				setDeviceRevision((String)newValue);
				return;
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__DEVICE_MANUAL:
				setDeviceManual((String)newValue);
				return;
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__DEVICE_CLASS:
				setDeviceClass((String)newValue);
				return;
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__SERIAL_NUMBER:
				setSerialNumber((String)newValue);
				return;
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__PRODUCT_INSTANCE_URI:
				setProductInstanceUri((String)newValue);
				return;
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__REVISION_COUNTER:
				setRevisionCounter((Int32)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__MANUFACTURER:
				setManufacturer((LocalizedText)null);
				return;
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__MANUFACTURER_URI:
				setManufacturerUri(MANUFACTURER_URI_EDEFAULT);
				return;
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__MODEL:
				setModel((LocalizedText)null);
				return;
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__PRODUCT_CODE:
				setProductCode(PRODUCT_CODE_EDEFAULT);
				return;
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__HARDWARE_REVISION:
				setHardwareRevision(HARDWARE_REVISION_EDEFAULT);
				return;
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__SOFTWARE_REVISION:
				setSoftwareRevision(SOFTWARE_REVISION_EDEFAULT);
				return;
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__DEVICE_REVISION:
				setDeviceRevision(DEVICE_REVISION_EDEFAULT);
				return;
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__DEVICE_MANUAL:
				setDeviceManual(DEVICE_MANUAL_EDEFAULT);
				return;
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__DEVICE_CLASS:
				setDeviceClass(DEVICE_CLASS_EDEFAULT);
				return;
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__SERIAL_NUMBER:
				setSerialNumber(SERIAL_NUMBER_EDEFAULT);
				return;
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__PRODUCT_INSTANCE_URI:
				setProductInstanceUri(PRODUCT_INSTANCE_URI_EDEFAULT);
				return;
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__REVISION_COUNTER:
				setRevisionCounter((Int32)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__MANUFACTURER:
				return manufacturer != null;
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__MANUFACTURER_URI:
				return MANUFACTURER_URI_EDEFAULT == null ? manufacturerUri != null : !MANUFACTURER_URI_EDEFAULT.equals(manufacturerUri);
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__MODEL:
				return model != null;
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__PRODUCT_CODE:
				return PRODUCT_CODE_EDEFAULT == null ? productCode != null : !PRODUCT_CODE_EDEFAULT.equals(productCode);
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__HARDWARE_REVISION:
				return HARDWARE_REVISION_EDEFAULT == null ? hardwareRevision != null : !HARDWARE_REVISION_EDEFAULT.equals(hardwareRevision);
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__SOFTWARE_REVISION:
				return SOFTWARE_REVISION_EDEFAULT == null ? softwareRevision != null : !SOFTWARE_REVISION_EDEFAULT.equals(softwareRevision);
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__DEVICE_REVISION:
				return DEVICE_REVISION_EDEFAULT == null ? deviceRevision != null : !DEVICE_REVISION_EDEFAULT.equals(deviceRevision);
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__DEVICE_MANUAL:
				return DEVICE_MANUAL_EDEFAULT == null ? deviceManual != null : !DEVICE_MANUAL_EDEFAULT.equals(deviceManual);
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__DEVICE_CLASS:
				return DEVICE_CLASS_EDEFAULT == null ? deviceClass != null : !DEVICE_CLASS_EDEFAULT.equals(deviceClass);
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__SERIAL_NUMBER:
				return SERIAL_NUMBER_EDEFAULT == null ? serialNumber != null : !SERIAL_NUMBER_EDEFAULT.equals(serialNumber);
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__PRODUCT_INSTANCE_URI:
				return PRODUCT_INSTANCE_URI_EDEFAULT == null ? productInstanceUri != null : !PRODUCT_INSTANCE_URI_EDEFAULT.equals(productInstanceUri);
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE__REVISION_COUNTER:
				return revisionCounter != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (ManufacturerUri: ");
		result.append(manufacturerUri);
		result.append(", ProductCode: ");
		result.append(productCode);
		result.append(", HardwareRevision: ");
		result.append(hardwareRevision);
		result.append(", SoftwareRevision: ");
		result.append(softwareRevision);
		result.append(", DeviceRevision: ");
		result.append(deviceRevision);
		result.append(", DeviceManual: ");
		result.append(deviceManual);
		result.append(", DeviceClass: ");
		result.append(deviceClass);
		result.append(", SerialNumber: ");
		result.append(serialNumber);
		result.append(", ProductInstanceUri: ");
		result.append(productInstanceUri);
		result.append(')');
		return result.toString();
	}

} //IVendorNameplateTypeImpl
