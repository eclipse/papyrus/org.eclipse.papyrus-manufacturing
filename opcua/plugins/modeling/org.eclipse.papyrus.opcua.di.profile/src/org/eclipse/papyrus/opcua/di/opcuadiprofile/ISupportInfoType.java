/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.di.opcuadiprofile;

import org.eclipse.emf.common.util.EList;

import org.eclipse.papyrus.opcua.opcuaprofile.BaseInterfaceType;

import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.ByteString;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Image;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISupport Info Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ISupportInfoType#getDeviceTypeImage <em>Device Type Image</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ISupportInfoType#getDocumentation <em>Documentation</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ISupportInfoType#getProtocolSupport <em>Protocol Support</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ISupportInfoType#getImageSet <em>Image Set</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getISupportInfoType()
 * @model abstract="true"
 * @generated
 */
public interface ISupportInfoType extends BaseInterfaceType {
	/**
	 * Returns the value of the '<em><b>Device Type Image</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Image}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Device Type Image</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Device Type Image</em>' containment reference list.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getISupportInfoType_DeviceTypeImage()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Image> getDeviceTypeImage();

	/**
	 * Returns the value of the '<em><b>Documentation</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.ByteString}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Documentation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Documentation</em>' containment reference list.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getISupportInfoType_Documentation()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<ByteString> getDocumentation();

	/**
	 * Returns the value of the '<em><b>Protocol Support</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.ByteString}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protocol Support</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protocol Support</em>' containment reference list.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getISupportInfoType_ProtocolSupport()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<ByteString> getProtocolSupport();

	/**
	 * Returns the value of the '<em><b>Image Set</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Image}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Image Set</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Image Set</em>' containment reference list.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getISupportInfoType_ImageSet()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Image> getImageSet();

} // ISupportInfoType
