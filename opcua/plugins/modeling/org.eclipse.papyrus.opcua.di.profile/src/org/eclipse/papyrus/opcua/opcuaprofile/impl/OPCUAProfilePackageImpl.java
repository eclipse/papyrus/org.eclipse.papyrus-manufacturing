/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.opcuaprofile.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.papyrus.opcua.opcuaprofile.AggregateConfigurationType;
import org.eclipse.papyrus.opcua.opcuaprofile.AggregateFunctionType;
import org.eclipse.papyrus.opcua.opcuaprofile.AlarmMetricsType;
import org.eclipse.papyrus.opcua.opcuaprofile.AliasNameType;
import org.eclipse.papyrus.opcua.opcuaprofile.AuthorizationServiceConfigurationType;
import org.eclipse.papyrus.opcua.opcuaprofile.BaseConditionClassType;
import org.eclipse.papyrus.opcua.opcuaprofile.BaseDataVariableType;
import org.eclipse.papyrus.opcua.opcuaprofile.BaseEventType;
import org.eclipse.papyrus.opcua.opcuaprofile.BaseInterfaceType;
import org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType;
import org.eclipse.papyrus.opcua.opcuaprofile.BaseVariableType;
import org.eclipse.papyrus.opcua.opcuaprofile.BuildInfoType;
import org.eclipse.papyrus.opcua.opcuaprofile.CertificateGroupType;
import org.eclipse.papyrus.opcua.opcuaprofile.CertificateType;
import org.eclipse.papyrus.opcua.opcuaprofile.ConnectionTransportType;
import org.eclipse.papyrus.opcua.opcuaprofile.DataSetReaderMessageType;
import org.eclipse.papyrus.opcua.opcuaprofile.DataSetReaderTransportType;
import org.eclipse.papyrus.opcua.opcuaprofile.DataSetReaderType;
import org.eclipse.papyrus.opcua.opcuaprofile.DataSetWriterMessageType;
import org.eclipse.papyrus.opcua.opcuaprofile.DataSetWriterTransportType;
import org.eclipse.papyrus.opcua.opcuaprofile.DataSetWriterType;
import org.eclipse.papyrus.opcua.opcuaprofile.DataTypeEncodingType;
import org.eclipse.papyrus.opcua.opcuaprofile.DataTypeSystemType;
import org.eclipse.papyrus.opcua.opcuaprofile.DictionaryEntryType;
import org.eclipse.papyrus.opcua.opcuaprofile.ExtensionFieldsType;
import org.eclipse.papyrus.opcua.opcuaprofile.FileType;
import org.eclipse.papyrus.opcua.opcuaprofile.FolderType;
import org.eclipse.papyrus.opcua.opcuaprofile.HistoricalDataConfigurationType;
import org.eclipse.papyrus.opcua.opcuaprofile.HistoryServerCapabilitiesType;
import org.eclipse.papyrus.opcua.opcuaprofile.KeyCredentialConfigurationType;
import org.eclipse.papyrus.opcua.opcuaprofile.ModellingRuleType;
import org.eclipse.papyrus.opcua.opcuaprofile.NamespaceMetadataType;
import org.eclipse.papyrus.opcua.opcuaprofile.NamespacesType;
import org.eclipse.papyrus.opcua.opcuaprofile.NetworkAddressType;
import org.eclipse.papyrus.opcua.opcuaprofile.OPCUAProfileFactory;
import org.eclipse.papyrus.opcua.opcuaprofile.OPCUAProfilePackage;

import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.OPC_UA_LibraryPackage;

import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl;

import org.eclipse.papyrus.opcua.opcuaprofile.OptionSetType;
import org.eclipse.papyrus.opcua.opcuaprofile.OrderedListType;
import org.eclipse.papyrus.opcua.opcuaprofile.PropertyType;
import org.eclipse.papyrus.opcua.opcuaprofile.PubSubConnectionType;
import org.eclipse.papyrus.opcua.opcuaprofile.PubSubDiagnosticsType;
import org.eclipse.papyrus.opcua.opcuaprofile.PubSubGroupType;
import org.eclipse.papyrus.opcua.opcuaprofile.PubSubKeyServiceType;
import org.eclipse.papyrus.opcua.opcuaprofile.PubSubStatusType;
import org.eclipse.papyrus.opcua.opcuaprofile.PublishedDataSetType;
import org.eclipse.papyrus.opcua.opcuaprofile.ReaderGroupMessageType;
import org.eclipse.papyrus.opcua.opcuaprofile.ReaderGroupTransportType;
import org.eclipse.papyrus.opcua.opcuaprofile.RoleSetType;
import org.eclipse.papyrus.opcua.opcuaprofile.RoleType;
import org.eclipse.papyrus.opcua.opcuaprofile.SamplingIntervalDiagnosticsArrayType;
import org.eclipse.papyrus.opcua.opcuaprofile.SamplingIntervalDiagnosticsType;
import org.eclipse.papyrus.opcua.opcuaprofile.SecurityGroupType;
import org.eclipse.papyrus.opcua.opcuaprofile.ServerCapabilitiesType;
import org.eclipse.papyrus.opcua.opcuaprofile.ServerConfigurationType;
import org.eclipse.papyrus.opcua.opcuaprofile.ServerDiagnosticsSummaryType;
import org.eclipse.papyrus.opcua.opcuaprofile.ServerDiagnosticsType;
import org.eclipse.papyrus.opcua.opcuaprofile.ServerRedundancyType;
import org.eclipse.papyrus.opcua.opcuaprofile.ServerStatusType;
import org.eclipse.papyrus.opcua.opcuaprofile.ServerType;
import org.eclipse.papyrus.opcua.opcuaprofile.ServerVendorCapabilityType;
import org.eclipse.papyrus.opcua.opcuaprofile.SessionDiagnosticsArrayType;
import org.eclipse.papyrus.opcua.opcuaprofile.SessionDiagnosticsObjectType;
import org.eclipse.papyrus.opcua.opcuaprofile.SessionDiagnosticsVariableType;
import org.eclipse.papyrus.opcua.opcuaprofile.SessionSecurityDiagnosticsArrayType;
import org.eclipse.papyrus.opcua.opcuaprofile.SessionSecurityDiagnosticsType;
import org.eclipse.papyrus.opcua.opcuaprofile.SessionsDiagnosticsSummaryType;
import org.eclipse.papyrus.opcua.opcuaprofile.StateMachineType;
import org.eclipse.papyrus.opcua.opcuaprofile.StateType;
import org.eclipse.papyrus.opcua.opcuaprofile.SubscribedDataSetType;
import org.eclipse.papyrus.opcua.opcuaprofile.SubscriptionDiagnosticsArrayType;
import org.eclipse.papyrus.opcua.opcuaprofile.SubscriptionDiagnosticsType;
import org.eclipse.papyrus.opcua.opcuaprofile.TemporaryFileTransferType;
import org.eclipse.papyrus.opcua.opcuaprofile.TransitionType;
import org.eclipse.papyrus.opcua.opcuaprofile.VendorServerInfoType;
import org.eclipse.papyrus.opcua.opcuaprofile.WriterGroupMessageType;
import org.eclipse.papyrus.opcua.opcuaprofile.WriterGroupTransportType;

import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OPCUAProfilePackageImpl extends EPackageImpl implements OPCUAProfilePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataTypeSystemTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modellingRuleTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serverTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass folderTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataTypeEncodingTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serverCapabilitiesTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serverDiagnosticsTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sessionsDiagnosticsSummaryTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sessionDiagnosticsObjectTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vendorServerInfoTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serverRedundancyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fileTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namespacesTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseEventTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aggregateFunctionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stateMachineTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stateTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namespaceMetadataTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass writerGroupMessageTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transitionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass temporaryFileTransferTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roleSetTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roleTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseInterfaceTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dictionaryEntryTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass orderedListTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseConditionClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass alarmMetricsTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass historicalDataConfigurationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass historyServerCapabilitiesTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass certificateGroupTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass certificateTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serverConfigurationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass keyCredentialConfigurationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass authorizationServiceConfigurationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aggregateConfigurationTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pubSubKeyServiceTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass securityGroupTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass publishedDataSetTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass extensionFieldsTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pubSubConnectionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionTransportTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pubSubGroupTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass writerGroupTransportTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass readerGroupTransportTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass readerGroupMessageTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataSetWriterTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataSetWriterTransportTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataSetWriterMessageTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataSetReaderTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataSetReaderTransportTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataSetReaderMessageTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass subscribedDataSetTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pubSubStatusTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pubSubDiagnosticsTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass networkAddressTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aliasNameTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseVariableTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseDataVariableTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propertyTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serverVendorCapabilityTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass samplingIntervalDiagnosticsArrayTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass samplingIntervalDiagnosticsTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass subscriptionDiagnosticsArrayTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass subscriptionDiagnosticsTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sessionDiagnosticsArrayTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sessionDiagnosticsVariableTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sessionSecurityDiagnosticsArrayTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sessionSecurityDiagnosticsTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass optionSetTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serverDiagnosticsSummaryTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass buildInfoTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serverStatusTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseObjectTypeEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPCUAProfilePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OPCUAProfilePackageImpl() {
		super(eNS_URI, OPCUAProfileFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link OPCUAProfilePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static OPCUAProfilePackage init() {
		if (isInited) return (OPCUAProfilePackage)EPackage.Registry.INSTANCE.getEPackage(OPCUAProfilePackage.eNS_URI);

		// Obtain or create and register package
		Object registeredOPCUAProfilePackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		OPCUAProfilePackageImpl theOPCUAProfilePackage = registeredOPCUAProfilePackage instanceof OPCUAProfilePackageImpl ? (OPCUAProfilePackageImpl)registeredOPCUAProfilePackage : new OPCUAProfilePackageImpl();

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OPC_UA_LibraryPackage.eNS_URI);
		OPC_UA_LibraryPackageImpl theOPC_UA_LibraryPackage = (OPC_UA_LibraryPackageImpl)(registeredPackage instanceof OPC_UA_LibraryPackageImpl ? registeredPackage : OPC_UA_LibraryPackage.eINSTANCE);

		// Create package meta-data objects
		theOPCUAProfilePackage.createPackageContents();
		theOPC_UA_LibraryPackage.createPackageContents();

		// Initialize created meta-data
		theOPCUAProfilePackage.initializePackageContents();
		theOPC_UA_LibraryPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theOPCUAProfilePackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(OPCUAProfilePackage.eNS_URI, theOPCUAProfilePackage);
		return theOPCUAProfilePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataTypeSystemType() {
		return dataTypeSystemTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModellingRuleType() {
		return modellingRuleTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getServerType() {
		return serverTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFolderType() {
		return folderTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataTypeEncodingType() {
		return dataTypeEncodingTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getServerCapabilitiesType() {
		return serverCapabilitiesTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getServerDiagnosticsType() {
		return serverDiagnosticsTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSessionsDiagnosticsSummaryType() {
		return sessionsDiagnosticsSummaryTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSessionDiagnosticsObjectType() {
		return sessionDiagnosticsObjectTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVendorServerInfoType() {
		return vendorServerInfoTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getServerRedundancyType() {
		return serverRedundancyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFileType() {
		return fileTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamespacesType() {
		return namespacesTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseEventType() {
		return baseEventTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAggregateFunctionType() {
		return aggregateFunctionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStateMachineType() {
		return stateMachineTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStateType() {
		return stateTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamespaceMetadataType() {
		return namespaceMetadataTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWriterGroupMessageType() {
		return writerGroupMessageTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransitionType() {
		return transitionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTemporaryFileTransferType() {
		return temporaryFileTransferTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoleSetType() {
		return roleSetTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoleType() {
		return roleTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseInterfaceType() {
		return baseInterfaceTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseInterfaceType_Base_Interface() {
		return (EReference)baseInterfaceTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDictionaryEntryType() {
		return dictionaryEntryTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOrderedListType() {
		return orderedListTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseConditionClassType() {
		return baseConditionClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAlarmMetricsType() {
		return alarmMetricsTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHistoricalDataConfigurationType() {
		return historicalDataConfigurationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHistoryServerCapabilitiesType() {
		return historyServerCapabilitiesTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCertificateGroupType() {
		return certificateGroupTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCertificateType() {
		return certificateTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getServerConfigurationType() {
		return serverConfigurationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getKeyCredentialConfigurationType() {
		return keyCredentialConfigurationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAuthorizationServiceConfigurationType() {
		return authorizationServiceConfigurationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAggregateConfigurationType() {
		return aggregateConfigurationTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPubSubKeyServiceType() {
		return pubSubKeyServiceTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSecurityGroupType() {
		return securityGroupTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPublishedDataSetType() {
		return publishedDataSetTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExtensionFieldsType() {
		return extensionFieldsTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPubSubConnectionType() {
		return pubSubConnectionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConnectionTransportType() {
		return connectionTransportTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPubSubGroupType() {
		return pubSubGroupTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWriterGroupTransportType() {
		return writerGroupTransportTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReaderGroupTransportType() {
		return readerGroupTransportTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReaderGroupMessageType() {
		return readerGroupMessageTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataSetWriterType() {
		return dataSetWriterTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataSetWriterTransportType() {
		return dataSetWriterTransportTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataSetWriterMessageType() {
		return dataSetWriterMessageTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataSetReaderType() {
		return dataSetReaderTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataSetReaderTransportType() {
		return dataSetReaderTransportTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataSetReaderMessageType() {
		return dataSetReaderMessageTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSubscribedDataSetType() {
		return subscribedDataSetTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPubSubStatusType() {
		return pubSubStatusTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPubSubDiagnosticsType() {
		return pubSubDiagnosticsTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNetworkAddressType() {
		return networkAddressTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAliasNameType() {
		return aliasNameTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseVariableType() {
		return baseVariableTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseVariableType_Base_Class() {
		return (EReference)baseVariableTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseVariableType_Base_Property() {
		return (EReference)baseVariableTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseVariableType_ValueRank() {
		return (EAttribute)baseVariableTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseVariableType_DataType() {
		return (EAttribute)baseVariableTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseDataVariableType() {
		return baseDataVariableTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPropertyType() {
		return propertyTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getServerVendorCapabilityType() {
		return serverVendorCapabilityTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSamplingIntervalDiagnosticsArrayType() {
		return samplingIntervalDiagnosticsArrayTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSamplingIntervalDiagnosticsType() {
		return samplingIntervalDiagnosticsTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSubscriptionDiagnosticsArrayType() {
		return subscriptionDiagnosticsArrayTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSubscriptionDiagnosticsType() {
		return subscriptionDiagnosticsTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSessionDiagnosticsArrayType() {
		return sessionDiagnosticsArrayTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSessionDiagnosticsVariableType() {
		return sessionDiagnosticsVariableTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSessionSecurityDiagnosticsArrayType() {
		return sessionSecurityDiagnosticsArrayTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSessionSecurityDiagnosticsType() {
		return sessionSecurityDiagnosticsTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOptionSetType() {
		return optionSetTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getServerDiagnosticsSummaryType() {
		return serverDiagnosticsSummaryTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBuildInfoType() {
		return buildInfoTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getServerStatusType() {
		return serverStatusTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseObjectType() {
		return baseObjectTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBaseObjectType_Base_Class() {
		return (EReference)baseObjectTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseObjectType_NodeId() {
		return (EAttribute)baseObjectTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseObjectType_NamespaceUri() {
		return (EAttribute)baseObjectTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseObjectType_BrowseName() {
		return (EAttribute)baseObjectTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseObjectType_NodeClass() {
		return (EAttribute)baseObjectTypeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPCUAProfileFactory getOPCUAProfileFactory() {
		return (OPCUAProfileFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		baseObjectTypeEClass = createEClass(BASE_OBJECT_TYPE);
		createEReference(baseObjectTypeEClass, BASE_OBJECT_TYPE__BASE_CLASS);
		createEAttribute(baseObjectTypeEClass, BASE_OBJECT_TYPE__NODE_ID);
		createEAttribute(baseObjectTypeEClass, BASE_OBJECT_TYPE__NAMESPACE_URI);
		createEAttribute(baseObjectTypeEClass, BASE_OBJECT_TYPE__BROWSE_NAME);
		createEAttribute(baseObjectTypeEClass, BASE_OBJECT_TYPE__NODE_CLASS);

		dataTypeSystemTypeEClass = createEClass(DATA_TYPE_SYSTEM_TYPE);

		modellingRuleTypeEClass = createEClass(MODELLING_RULE_TYPE);

		serverTypeEClass = createEClass(SERVER_TYPE);

		folderTypeEClass = createEClass(FOLDER_TYPE);

		dataTypeEncodingTypeEClass = createEClass(DATA_TYPE_ENCODING_TYPE);

		serverCapabilitiesTypeEClass = createEClass(SERVER_CAPABILITIES_TYPE);

		serverDiagnosticsTypeEClass = createEClass(SERVER_DIAGNOSTICS_TYPE);

		sessionsDiagnosticsSummaryTypeEClass = createEClass(SESSIONS_DIAGNOSTICS_SUMMARY_TYPE);

		sessionDiagnosticsObjectTypeEClass = createEClass(SESSION_DIAGNOSTICS_OBJECT_TYPE);

		vendorServerInfoTypeEClass = createEClass(VENDOR_SERVER_INFO_TYPE);

		serverRedundancyTypeEClass = createEClass(SERVER_REDUNDANCY_TYPE);

		fileTypeEClass = createEClass(FILE_TYPE);

		namespacesTypeEClass = createEClass(NAMESPACES_TYPE);

		baseEventTypeEClass = createEClass(BASE_EVENT_TYPE);

		aggregateFunctionTypeEClass = createEClass(AGGREGATE_FUNCTION_TYPE);

		stateMachineTypeEClass = createEClass(STATE_MACHINE_TYPE);

		stateTypeEClass = createEClass(STATE_TYPE);

		namespaceMetadataTypeEClass = createEClass(NAMESPACE_METADATA_TYPE);

		writerGroupMessageTypeEClass = createEClass(WRITER_GROUP_MESSAGE_TYPE);

		transitionTypeEClass = createEClass(TRANSITION_TYPE);

		temporaryFileTransferTypeEClass = createEClass(TEMPORARY_FILE_TRANSFER_TYPE);

		roleSetTypeEClass = createEClass(ROLE_SET_TYPE);

		roleTypeEClass = createEClass(ROLE_TYPE);

		baseInterfaceTypeEClass = createEClass(BASE_INTERFACE_TYPE);
		createEReference(baseInterfaceTypeEClass, BASE_INTERFACE_TYPE__BASE_INTERFACE);

		dictionaryEntryTypeEClass = createEClass(DICTIONARY_ENTRY_TYPE);

		orderedListTypeEClass = createEClass(ORDERED_LIST_TYPE);

		baseConditionClassTypeEClass = createEClass(BASE_CONDITION_CLASS_TYPE);

		alarmMetricsTypeEClass = createEClass(ALARM_METRICS_TYPE);

		historicalDataConfigurationTypeEClass = createEClass(HISTORICAL_DATA_CONFIGURATION_TYPE);

		historyServerCapabilitiesTypeEClass = createEClass(HISTORY_SERVER_CAPABILITIES_TYPE);

		certificateGroupTypeEClass = createEClass(CERTIFICATE_GROUP_TYPE);

		certificateTypeEClass = createEClass(CERTIFICATE_TYPE);

		serverConfigurationTypeEClass = createEClass(SERVER_CONFIGURATION_TYPE);

		keyCredentialConfigurationTypeEClass = createEClass(KEY_CREDENTIAL_CONFIGURATION_TYPE);

		authorizationServiceConfigurationTypeEClass = createEClass(AUTHORIZATION_SERVICE_CONFIGURATION_TYPE);

		aggregateConfigurationTypeEClass = createEClass(AGGREGATE_CONFIGURATION_TYPE);

		pubSubKeyServiceTypeEClass = createEClass(PUB_SUB_KEY_SERVICE_TYPE);

		securityGroupTypeEClass = createEClass(SECURITY_GROUP_TYPE);

		publishedDataSetTypeEClass = createEClass(PUBLISHED_DATA_SET_TYPE);

		extensionFieldsTypeEClass = createEClass(EXTENSION_FIELDS_TYPE);

		pubSubConnectionTypeEClass = createEClass(PUB_SUB_CONNECTION_TYPE);

		connectionTransportTypeEClass = createEClass(CONNECTION_TRANSPORT_TYPE);

		pubSubGroupTypeEClass = createEClass(PUB_SUB_GROUP_TYPE);

		writerGroupTransportTypeEClass = createEClass(WRITER_GROUP_TRANSPORT_TYPE);

		readerGroupTransportTypeEClass = createEClass(READER_GROUP_TRANSPORT_TYPE);

		readerGroupMessageTypeEClass = createEClass(READER_GROUP_MESSAGE_TYPE);

		dataSetWriterTypeEClass = createEClass(DATA_SET_WRITER_TYPE);

		dataSetWriterTransportTypeEClass = createEClass(DATA_SET_WRITER_TRANSPORT_TYPE);

		dataSetWriterMessageTypeEClass = createEClass(DATA_SET_WRITER_MESSAGE_TYPE);

		dataSetReaderTypeEClass = createEClass(DATA_SET_READER_TYPE);

		dataSetReaderTransportTypeEClass = createEClass(DATA_SET_READER_TRANSPORT_TYPE);

		dataSetReaderMessageTypeEClass = createEClass(DATA_SET_READER_MESSAGE_TYPE);

		subscribedDataSetTypeEClass = createEClass(SUBSCRIBED_DATA_SET_TYPE);

		pubSubStatusTypeEClass = createEClass(PUB_SUB_STATUS_TYPE);

		pubSubDiagnosticsTypeEClass = createEClass(PUB_SUB_DIAGNOSTICS_TYPE);

		networkAddressTypeEClass = createEClass(NETWORK_ADDRESS_TYPE);

		aliasNameTypeEClass = createEClass(ALIAS_NAME_TYPE);

		baseVariableTypeEClass = createEClass(BASE_VARIABLE_TYPE);
		createEReference(baseVariableTypeEClass, BASE_VARIABLE_TYPE__BASE_CLASS);
		createEReference(baseVariableTypeEClass, BASE_VARIABLE_TYPE__BASE_PROPERTY);
		createEAttribute(baseVariableTypeEClass, BASE_VARIABLE_TYPE__VALUE_RANK);
		createEAttribute(baseVariableTypeEClass, BASE_VARIABLE_TYPE__DATA_TYPE);

		baseDataVariableTypeEClass = createEClass(BASE_DATA_VARIABLE_TYPE);

		propertyTypeEClass = createEClass(PROPERTY_TYPE);

		serverVendorCapabilityTypeEClass = createEClass(SERVER_VENDOR_CAPABILITY_TYPE);

		samplingIntervalDiagnosticsArrayTypeEClass = createEClass(SAMPLING_INTERVAL_DIAGNOSTICS_ARRAY_TYPE);

		samplingIntervalDiagnosticsTypeEClass = createEClass(SAMPLING_INTERVAL_DIAGNOSTICS_TYPE);

		subscriptionDiagnosticsArrayTypeEClass = createEClass(SUBSCRIPTION_DIAGNOSTICS_ARRAY_TYPE);

		subscriptionDiagnosticsTypeEClass = createEClass(SUBSCRIPTION_DIAGNOSTICS_TYPE);

		sessionDiagnosticsArrayTypeEClass = createEClass(SESSION_DIAGNOSTICS_ARRAY_TYPE);

		sessionDiagnosticsVariableTypeEClass = createEClass(SESSION_DIAGNOSTICS_VARIABLE_TYPE);

		sessionSecurityDiagnosticsArrayTypeEClass = createEClass(SESSION_SECURITY_DIAGNOSTICS_ARRAY_TYPE);

		sessionSecurityDiagnosticsTypeEClass = createEClass(SESSION_SECURITY_DIAGNOSTICS_TYPE);

		optionSetTypeEClass = createEClass(OPTION_SET_TYPE);

		serverDiagnosticsSummaryTypeEClass = createEClass(SERVER_DIAGNOSTICS_SUMMARY_TYPE);

		buildInfoTypeEClass = createEClass(BUILD_INFO_TYPE);

		serverStatusTypeEClass = createEClass(SERVER_STATUS_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		OPC_UA_LibraryPackage theOPC_UA_LibraryPackage = (OPC_UA_LibraryPackage)EPackage.Registry.INSTANCE.getEPackage(OPC_UA_LibraryPackage.eNS_URI);
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theOPC_UA_LibraryPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		dataTypeSystemTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		modellingRuleTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		serverTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		folderTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		dataTypeEncodingTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		serverCapabilitiesTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		serverDiagnosticsTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		sessionsDiagnosticsSummaryTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		sessionDiagnosticsObjectTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		vendorServerInfoTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		serverRedundancyTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		fileTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		namespacesTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		baseEventTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		aggregateFunctionTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		stateMachineTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		stateTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		namespaceMetadataTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		writerGroupMessageTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		transitionTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		temporaryFileTransferTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		roleSetTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		roleTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		baseInterfaceTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		dictionaryEntryTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		orderedListTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		baseConditionClassTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		alarmMetricsTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		historicalDataConfigurationTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		historyServerCapabilitiesTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		certificateGroupTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		certificateTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		serverConfigurationTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		keyCredentialConfigurationTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		authorizationServiceConfigurationTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		aggregateConfigurationTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		pubSubKeyServiceTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		securityGroupTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		publishedDataSetTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		extensionFieldsTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		pubSubConnectionTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		connectionTransportTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		pubSubGroupTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		writerGroupTransportTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		readerGroupTransportTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		readerGroupMessageTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		dataSetWriterTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		dataSetWriterTransportTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		dataSetWriterMessageTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		dataSetReaderTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		dataSetReaderTransportTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		dataSetReaderMessageTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		subscribedDataSetTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		pubSubStatusTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		pubSubDiagnosticsTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		networkAddressTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		aliasNameTypeEClass.getESuperTypes().add(this.getBaseObjectType());
		baseDataVariableTypeEClass.getESuperTypes().add(this.getBaseVariableType());
		propertyTypeEClass.getESuperTypes().add(this.getBaseVariableType());
		serverVendorCapabilityTypeEClass.getESuperTypes().add(this.getBaseDataVariableType());
		samplingIntervalDiagnosticsArrayTypeEClass.getESuperTypes().add(this.getBaseDataVariableType());
		samplingIntervalDiagnosticsTypeEClass.getESuperTypes().add(this.getBaseDataVariableType());
		subscriptionDiagnosticsArrayTypeEClass.getESuperTypes().add(this.getBaseDataVariableType());
		subscriptionDiagnosticsTypeEClass.getESuperTypes().add(this.getBaseDataVariableType());
		sessionDiagnosticsArrayTypeEClass.getESuperTypes().add(this.getBaseDataVariableType());
		sessionDiagnosticsVariableTypeEClass.getESuperTypes().add(this.getBaseDataVariableType());
		optionSetTypeEClass.getESuperTypes().add(this.getBaseDataVariableType());
		serverDiagnosticsSummaryTypeEClass.getESuperTypes().add(this.getBaseDataVariableType());
		buildInfoTypeEClass.getESuperTypes().add(this.getBaseDataVariableType());
		serverStatusTypeEClass.getESuperTypes().add(this.getBaseDataVariableType());

		// Initialize classes, features, and operations; add parameters
		initEClass(baseObjectTypeEClass, BaseObjectType.class, "BaseObjectType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBaseObjectType_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 0, 1, BaseObjectType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBaseObjectType_NodeId(), theTypesPackage.getString(), "NodeId", "i=58", 1, 1, BaseObjectType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBaseObjectType_NamespaceUri(), theTypesPackage.getString(), "NamespaceUri", "http://opcfoundation.org/UA/", 1, 1, BaseObjectType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBaseObjectType_BrowseName(), theTypesPackage.getString(), "BrowseName", "", 1, 1, BaseObjectType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBaseObjectType_NodeClass(), theTypesPackage.getString(), "NodeClass", "ObjectType", 1, 1, BaseObjectType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(dataTypeSystemTypeEClass, DataTypeSystemType.class, "DataTypeSystemType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(modellingRuleTypeEClass, ModellingRuleType.class, "ModellingRuleType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(serverTypeEClass, ServerType.class, "ServerType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(folderTypeEClass, FolderType.class, "FolderType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dataTypeEncodingTypeEClass, DataTypeEncodingType.class, "DataTypeEncodingType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(serverCapabilitiesTypeEClass, ServerCapabilitiesType.class, "ServerCapabilitiesType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(serverDiagnosticsTypeEClass, ServerDiagnosticsType.class, "ServerDiagnosticsType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(sessionsDiagnosticsSummaryTypeEClass, SessionsDiagnosticsSummaryType.class, "SessionsDiagnosticsSummaryType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(sessionDiagnosticsObjectTypeEClass, SessionDiagnosticsObjectType.class, "SessionDiagnosticsObjectType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(vendorServerInfoTypeEClass, VendorServerInfoType.class, "VendorServerInfoType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(serverRedundancyTypeEClass, ServerRedundancyType.class, "ServerRedundancyType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fileTypeEClass, FileType.class, "FileType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(namespacesTypeEClass, NamespacesType.class, "NamespacesType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(baseEventTypeEClass, BaseEventType.class, "BaseEventType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(aggregateFunctionTypeEClass, AggregateFunctionType.class, "AggregateFunctionType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(stateMachineTypeEClass, StateMachineType.class, "StateMachineType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(stateTypeEClass, StateType.class, "StateType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(namespaceMetadataTypeEClass, NamespaceMetadataType.class, "NamespaceMetadataType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(writerGroupMessageTypeEClass, WriterGroupMessageType.class, "WriterGroupMessageType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(transitionTypeEClass, TransitionType.class, "TransitionType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(temporaryFileTransferTypeEClass, TemporaryFileTransferType.class, "TemporaryFileTransferType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(roleSetTypeEClass, RoleSetType.class, "RoleSetType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(roleTypeEClass, RoleType.class, "RoleType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(baseInterfaceTypeEClass, BaseInterfaceType.class, "BaseInterfaceType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBaseInterfaceType_Base_Interface(), theUMLPackage.getInterface(), null, "base_Interface", null, 0, 1, BaseInterfaceType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(dictionaryEntryTypeEClass, DictionaryEntryType.class, "DictionaryEntryType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(orderedListTypeEClass, OrderedListType.class, "OrderedListType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(baseConditionClassTypeEClass, BaseConditionClassType.class, "BaseConditionClassType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(alarmMetricsTypeEClass, AlarmMetricsType.class, "AlarmMetricsType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(historicalDataConfigurationTypeEClass, HistoricalDataConfigurationType.class, "HistoricalDataConfigurationType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(historyServerCapabilitiesTypeEClass, HistoryServerCapabilitiesType.class, "HistoryServerCapabilitiesType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(certificateGroupTypeEClass, CertificateGroupType.class, "CertificateGroupType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(certificateTypeEClass, CertificateType.class, "CertificateType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(serverConfigurationTypeEClass, ServerConfigurationType.class, "ServerConfigurationType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(keyCredentialConfigurationTypeEClass, KeyCredentialConfigurationType.class, "KeyCredentialConfigurationType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(authorizationServiceConfigurationTypeEClass, AuthorizationServiceConfigurationType.class, "AuthorizationServiceConfigurationType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(aggregateConfigurationTypeEClass, AggregateConfigurationType.class, "AggregateConfigurationType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(pubSubKeyServiceTypeEClass, PubSubKeyServiceType.class, "PubSubKeyServiceType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(securityGroupTypeEClass, SecurityGroupType.class, "SecurityGroupType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(publishedDataSetTypeEClass, PublishedDataSetType.class, "PublishedDataSetType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(extensionFieldsTypeEClass, ExtensionFieldsType.class, "ExtensionFieldsType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(pubSubConnectionTypeEClass, PubSubConnectionType.class, "PubSubConnectionType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(connectionTransportTypeEClass, ConnectionTransportType.class, "ConnectionTransportType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(pubSubGroupTypeEClass, PubSubGroupType.class, "PubSubGroupType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(writerGroupTransportTypeEClass, WriterGroupTransportType.class, "WriterGroupTransportType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(readerGroupTransportTypeEClass, ReaderGroupTransportType.class, "ReaderGroupTransportType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(readerGroupMessageTypeEClass, ReaderGroupMessageType.class, "ReaderGroupMessageType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dataSetWriterTypeEClass, DataSetWriterType.class, "DataSetWriterType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dataSetWriterTransportTypeEClass, DataSetWriterTransportType.class, "DataSetWriterTransportType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dataSetWriterMessageTypeEClass, DataSetWriterMessageType.class, "DataSetWriterMessageType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dataSetReaderTypeEClass, DataSetReaderType.class, "DataSetReaderType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dataSetReaderTransportTypeEClass, DataSetReaderTransportType.class, "DataSetReaderTransportType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dataSetReaderMessageTypeEClass, DataSetReaderMessageType.class, "DataSetReaderMessageType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(subscribedDataSetTypeEClass, SubscribedDataSetType.class, "SubscribedDataSetType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(pubSubStatusTypeEClass, PubSubStatusType.class, "PubSubStatusType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(pubSubDiagnosticsTypeEClass, PubSubDiagnosticsType.class, "PubSubDiagnosticsType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(networkAddressTypeEClass, NetworkAddressType.class, "NetworkAddressType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(aliasNameTypeEClass, AliasNameType.class, "AliasNameType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(baseVariableTypeEClass, BaseVariableType.class, "BaseVariableType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBaseVariableType_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 0, 1, BaseVariableType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getBaseVariableType_Base_Property(), theUMLPackage.getProperty(), null, "base_Property", null, 0, 1, BaseVariableType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBaseVariableType_ValueRank(), theTypesPackage.getString(), "ValueRank", "ValueRank", 1, 1, BaseVariableType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBaseVariableType_DataType(), theTypesPackage.getString(), "DataType", "BaseDataType", 1, 1, BaseVariableType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(baseDataVariableTypeEClass, BaseDataVariableType.class, "BaseDataVariableType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(propertyTypeEClass, PropertyType.class, "PropertyType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(serverVendorCapabilityTypeEClass, ServerVendorCapabilityType.class, "ServerVendorCapabilityType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(samplingIntervalDiagnosticsArrayTypeEClass, SamplingIntervalDiagnosticsArrayType.class, "SamplingIntervalDiagnosticsArrayType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(samplingIntervalDiagnosticsTypeEClass, SamplingIntervalDiagnosticsType.class, "SamplingIntervalDiagnosticsType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(subscriptionDiagnosticsArrayTypeEClass, SubscriptionDiagnosticsArrayType.class, "SubscriptionDiagnosticsArrayType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(subscriptionDiagnosticsTypeEClass, SubscriptionDiagnosticsType.class, "SubscriptionDiagnosticsType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(sessionDiagnosticsArrayTypeEClass, SessionDiagnosticsArrayType.class, "SessionDiagnosticsArrayType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(sessionDiagnosticsVariableTypeEClass, SessionDiagnosticsVariableType.class, "SessionDiagnosticsVariableType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(sessionSecurityDiagnosticsArrayTypeEClass, SessionSecurityDiagnosticsArrayType.class, "SessionSecurityDiagnosticsArrayType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(sessionSecurityDiagnosticsTypeEClass, SessionSecurityDiagnosticsType.class, "SessionSecurityDiagnosticsType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(optionSetTypeEClass, OptionSetType.class, "OptionSetType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(serverDiagnosticsSummaryTypeEClass, ServerDiagnosticsSummaryType.class, "ServerDiagnosticsSummaryType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(buildInfoTypeEClass, BuildInfoType.class, "BuildInfoType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(serverStatusTypeEClass, ServerStatusType.class, "ServerStatusType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/uml2/2.0.0/UML
		createUMLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/uml2/2.0.0/UML</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createUMLAnnotations() {
		String source = "http://www.eclipse.org/uml2/2.0.0/UML";
		addAnnotation
		  (this,
		   source,
		   new String[] {
			   "originalName", "OPC_UA"
		   });
	}

} //OPCUAProfilePackageImpl
