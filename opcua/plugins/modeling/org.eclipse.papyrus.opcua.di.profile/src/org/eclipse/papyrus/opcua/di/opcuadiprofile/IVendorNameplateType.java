/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.di.opcuadiprofile;

import org.eclipse.papyrus.opcua.opcuaprofile.BaseInterfaceType;

import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int32;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.LocalizedText;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IVendor Nameplate Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getManufacturer <em>Manufacturer</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getManufacturerUri <em>Manufacturer Uri</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getModel <em>Model</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getProductCode <em>Product Code</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getHardwareRevision <em>Hardware Revision</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getSoftwareRevision <em>Software Revision</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getDeviceRevision <em>Device Revision</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getDeviceManual <em>Device Manual</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getDeviceClass <em>Device Class</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getSerialNumber <em>Serial Number</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getProductInstanceUri <em>Product Instance Uri</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getRevisionCounter <em>Revision Counter</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getIVendorNameplateType()
 * @model abstract="true"
 * @generated
 */
public interface IVendorNameplateType extends BaseInterfaceType {
	/**
	 * Returns the value of the '<em><b>Manufacturer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Manufacturer</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Manufacturer</em>' containment reference.
	 * @see #setManufacturer(LocalizedText)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getIVendorNameplateType_Manufacturer()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	LocalizedText getManufacturer();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getManufacturer <em>Manufacturer</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Manufacturer</em>' containment reference.
	 * @see #getManufacturer()
	 * @generated
	 */
	void setManufacturer(LocalizedText value);

	/**
	 * Returns the value of the '<em><b>Manufacturer Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Manufacturer Uri</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Manufacturer Uri</em>' attribute.
	 * @see #setManufacturerUri(String)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getIVendorNameplateType_ManufacturerUri()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getManufacturerUri();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getManufacturerUri <em>Manufacturer Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Manufacturer Uri</em>' attribute.
	 * @see #getManufacturerUri()
	 * @generated
	 */
	void setManufacturerUri(String value);

	/**
	 * Returns the value of the '<em><b>Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model</em>' containment reference.
	 * @see #setModel(LocalizedText)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getIVendorNameplateType_Model()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	LocalizedText getModel();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getModel <em>Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model</em>' containment reference.
	 * @see #getModel()
	 * @generated
	 */
	void setModel(LocalizedText value);

	/**
	 * Returns the value of the '<em><b>Product Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Product Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Product Code</em>' attribute.
	 * @see #setProductCode(String)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getIVendorNameplateType_ProductCode()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getProductCode();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getProductCode <em>Product Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Product Code</em>' attribute.
	 * @see #getProductCode()
	 * @generated
	 */
	void setProductCode(String value);

	/**
	 * Returns the value of the '<em><b>Hardware Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hardware Revision</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hardware Revision</em>' attribute.
	 * @see #setHardwareRevision(String)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getIVendorNameplateType_HardwareRevision()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getHardwareRevision();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getHardwareRevision <em>Hardware Revision</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hardware Revision</em>' attribute.
	 * @see #getHardwareRevision()
	 * @generated
	 */
	void setHardwareRevision(String value);

	/**
	 * Returns the value of the '<em><b>Software Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Software Revision</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Software Revision</em>' attribute.
	 * @see #setSoftwareRevision(String)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getIVendorNameplateType_SoftwareRevision()
	 * @model dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
	 * @generated
	 */
	String getSoftwareRevision();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getSoftwareRevision <em>Software Revision</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Software Revision</em>' attribute.
	 * @see #getSoftwareRevision()
	 * @generated
	 */
	void setSoftwareRevision(String value);

	/**
	 * Returns the value of the '<em><b>Device Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Device Revision</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Device Revision</em>' attribute.
	 * @see #setDeviceRevision(String)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getIVendorNameplateType_DeviceRevision()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getDeviceRevision();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getDeviceRevision <em>Device Revision</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Device Revision</em>' attribute.
	 * @see #getDeviceRevision()
	 * @generated
	 */
	void setDeviceRevision(String value);

	/**
	 * Returns the value of the '<em><b>Device Manual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Device Manual</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Device Manual</em>' attribute.
	 * @see #setDeviceManual(String)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getIVendorNameplateType_DeviceManual()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getDeviceManual();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getDeviceManual <em>Device Manual</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Device Manual</em>' attribute.
	 * @see #getDeviceManual()
	 * @generated
	 */
	void setDeviceManual(String value);

	/**
	 * Returns the value of the '<em><b>Device Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Device Class</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Device Class</em>' attribute.
	 * @see #setDeviceClass(String)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getIVendorNameplateType_DeviceClass()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getDeviceClass();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getDeviceClass <em>Device Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Device Class</em>' attribute.
	 * @see #getDeviceClass()
	 * @generated
	 */
	void setDeviceClass(String value);

	/**
	 * Returns the value of the '<em><b>Serial Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Serial Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Serial Number</em>' attribute.
	 * @see #setSerialNumber(String)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getIVendorNameplateType_SerialNumber()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getSerialNumber();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getSerialNumber <em>Serial Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Serial Number</em>' attribute.
	 * @see #getSerialNumber()
	 * @generated
	 */
	void setSerialNumber(String value);

	/**
	 * Returns the value of the '<em><b>Product Instance Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Product Instance Uri</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Product Instance Uri</em>' attribute.
	 * @see #setProductInstanceUri(String)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getIVendorNameplateType_ProductInstanceUri()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getProductInstanceUri();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getProductInstanceUri <em>Product Instance Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Product Instance Uri</em>' attribute.
	 * @see #getProductInstanceUri()
	 * @generated
	 */
	void setProductInstanceUri(String value);

	/**
	 * Returns the value of the '<em><b>Revision Counter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Revision Counter</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Revision Counter</em>' containment reference.
	 * @see #setRevisionCounter(Int32)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getIVendorNameplateType_RevisionCounter()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	Int32 getRevisionCounter();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getRevisionCounter <em>Revision Counter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Revision Counter</em>' containment reference.
	 * @see #getRevisionCounter()
	 * @generated
	 */
	void setRevisionCounter(Int32 value);

} // IVendorNameplateType
