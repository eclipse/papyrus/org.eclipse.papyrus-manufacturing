/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.papyrus.opcua.opcuaprofile.OPCUAProfilePackage;

import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.AnalogUnitType;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Argument;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.BaseDataType;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.ByteString;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.DataValue;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.DateTime;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.DiagnosticInfo;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Duration;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.EccEncryptedSecret;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.ExpandedNodeId;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Guid;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.IdType;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Image;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int16;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int32;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int64;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.LocalId;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.LocalizedText;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.MessageSecurityMode;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.NamingRuleType;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.NodeCLass;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.NodeId;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.OPC_UA_LibraryFactory;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.OPC_UA_LibraryPackage;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.QualifiedName;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.RedundancySupport;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.RsaEncryptedSecret;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.SByte;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.SecurityTokenRequestType;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.ServerState;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.StatusCode;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Structure;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.UInt16;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.UInt32;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.UInt64;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.UInteger;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.XmlElement;

import org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl;

import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OPC_UA_LibraryPackageImpl extends EPackageImpl implements OPC_UA_LibraryPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass analogUnitTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass doubleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass numberEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseDataTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass localizedTextEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass localIdEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dateTimeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass guidEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass byteStringEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xmlElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nodeIdEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass expandedNodeIdEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass statusCodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass qualifiedNameEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass structureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass diagnosticInfoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rsaEncryptedSecretEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eccEncryptedSecretEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass integerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass floatEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uIntegerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass durationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass int64EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass int32EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass int16EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sByteEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass byteEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass argumentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uInt16EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uInt32EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uInt64EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass imageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gifEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bmpEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pngEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass jpgEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum nodeCLassEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum idTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum namingRuleTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum securityTokenRequestTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum redundancySupportEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum messageSecurityModeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum serverStateEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.OPC_UA_LibraryPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OPC_UA_LibraryPackageImpl() {
		super(eNS_URI, OPC_UA_LibraryFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link OPC_UA_LibraryPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static OPC_UA_LibraryPackage init() {
		if (isInited) return (OPC_UA_LibraryPackage)EPackage.Registry.INSTANCE.getEPackage(OPC_UA_LibraryPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredOPC_UA_LibraryPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		OPC_UA_LibraryPackageImpl theOPC_UA_LibraryPackage = registeredOPC_UA_LibraryPackage instanceof OPC_UA_LibraryPackageImpl ? (OPC_UA_LibraryPackageImpl)registeredOPC_UA_LibraryPackage : new OPC_UA_LibraryPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OPCUAProfilePackage.eNS_URI);
		OPCUAProfilePackageImpl theOPCUAProfilePackage = (OPCUAProfilePackageImpl)(registeredPackage instanceof OPCUAProfilePackageImpl ? registeredPackage : OPCUAProfilePackage.eINSTANCE);

		// Create package meta-data objects
		theOPC_UA_LibraryPackage.createPackageContents();
		theOPCUAProfilePackage.createPackageContents();

		// Initialize created meta-data
		theOPC_UA_LibraryPackage.initializePackageContents();
		theOPCUAProfilePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theOPC_UA_LibraryPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(OPC_UA_LibraryPackage.eNS_URI, theOPC_UA_LibraryPackage);
		return theOPC_UA_LibraryPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnalogUnitType() {
		return analogUnitTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDouble() {
		return doubleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNumber() {
		return numberEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseDataType() {
		return baseDataTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseDataType_NodeId() {
		return (EAttribute)baseDataTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBaseDataType_NamespaceUri() {
		return (EAttribute)baseDataTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLocalizedText() {
		return localizedTextEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLocalizedText_Value() {
		return (EAttribute)localizedTextEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLocalizedText_Local() {
		return (EReference)localizedTextEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLocalId() {
		return localIdEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getString() {
		return stringEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDateTime() {
		return dateTimeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGuid() {
		return guidEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getByteString() {
		return byteStringEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXmlElement() {
		return xmlElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNodeId() {
		return nodeIdEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExpandedNodeId() {
		return expandedNodeIdEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStatusCode() {
		return statusCodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getQualifiedName() {
		return qualifiedNameEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStructure() {
		return structureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataValue() {
		return dataValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDiagnosticInfo() {
		return diagnosticInfoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRsaEncryptedSecret() {
		return rsaEncryptedSecretEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEccEncryptedSecret() {
		return eccEncryptedSecretEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInteger() {
		return integerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFloat() {
		return floatEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUInteger() {
		return uIntegerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDuration() {
		return durationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInt64() {
		return int64EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInt32() {
		return int32EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInt16() {
		return int16EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSByte() {
		return sByteEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getByte() {
		return byteEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getArgument() {
		return argumentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUInt16() {
		return uInt16EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUInt32() {
		return uInt32EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUInt64() {
		return uInt64EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImage() {
		return imageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGIF() {
		return gifEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBMP() {
		return bmpEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPNG() {
		return pngEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJPG() {
		return jpgEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getNodeCLass() {
		return nodeCLassEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getIdType() {
		return idTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getNamingRuleType() {
		return namingRuleTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSecurityTokenRequestType() {
		return securityTokenRequestTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRedundancySupport() {
		return redundancySupportEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMessageSecurityMode() {
		return messageSecurityModeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getServerState() {
		return serverStateEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPC_UA_LibraryFactory getOPC_UA_LibraryFactory() {
		return (OPC_UA_LibraryFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		analogUnitTypeEClass = createEClass(ANALOG_UNIT_TYPE);

		doubleEClass = createEClass(DOUBLE);

		numberEClass = createEClass(NUMBER);

		baseDataTypeEClass = createEClass(BASE_DATA_TYPE);
		createEAttribute(baseDataTypeEClass, BASE_DATA_TYPE__NODE_ID);
		createEAttribute(baseDataTypeEClass, BASE_DATA_TYPE__NAMESPACE_URI);

		localizedTextEClass = createEClass(LOCALIZED_TEXT);
		createEAttribute(localizedTextEClass, LOCALIZED_TEXT__VALUE);
		createEReference(localizedTextEClass, LOCALIZED_TEXT__LOCAL);

		localIdEClass = createEClass(LOCAL_ID);

		stringEClass = createEClass(STRING);

		dateTimeEClass = createEClass(DATE_TIME);

		guidEClass = createEClass(GUID);

		byteStringEClass = createEClass(BYTE_STRING);

		xmlElementEClass = createEClass(XML_ELEMENT);

		nodeIdEClass = createEClass(NODE_ID);

		expandedNodeIdEClass = createEClass(EXPANDED_NODE_ID);

		statusCodeEClass = createEClass(STATUS_CODE);

		qualifiedNameEClass = createEClass(QUALIFIED_NAME);

		structureEClass = createEClass(STRUCTURE);

		dataValueEClass = createEClass(DATA_VALUE);

		diagnosticInfoEClass = createEClass(DIAGNOSTIC_INFO);

		rsaEncryptedSecretEClass = createEClass(RSA_ENCRYPTED_SECRET);

		eccEncryptedSecretEClass = createEClass(ECC_ENCRYPTED_SECRET);

		integerEClass = createEClass(INTEGER);

		floatEClass = createEClass(FLOAT);

		uIntegerEClass = createEClass(UINTEGER);

		durationEClass = createEClass(DURATION);

		int64EClass = createEClass(INT64);

		int32EClass = createEClass(INT32);

		int16EClass = createEClass(INT16);

		sByteEClass = createEClass(SBYTE);

		byteEClass = createEClass(BYTE);

		argumentEClass = createEClass(ARGUMENT);

		uInt16EClass = createEClass(UINT16);

		uInt32EClass = createEClass(UINT32);

		uInt64EClass = createEClass(UINT64);

		imageEClass = createEClass(IMAGE);

		gifEClass = createEClass(GIF);

		bmpEClass = createEClass(BMP);

		pngEClass = createEClass(PNG);

		jpgEClass = createEClass(JPG);

		// Create enums
		nodeCLassEEnum = createEEnum(NODE_CLASS);
		idTypeEEnum = createEEnum(ID_TYPE);
		namingRuleTypeEEnum = createEEnum(NAMING_RULE_TYPE);
		securityTokenRequestTypeEEnum = createEEnum(SECURITY_TOKEN_REQUEST_TYPE);
		redundancySupportEEnum = createEEnum(REDUNDANCY_SUPPORT);
		messageSecurityModeEEnum = createEEnum(MESSAGE_SECURITY_MODE);
		serverStateEEnum = createEEnum(SERVER_STATE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		analogUnitTypeEClass.getESuperTypes().add(this.getDouble());
		doubleEClass.getESuperTypes().add(this.getNumber());
		numberEClass.getESuperTypes().add(this.getBaseDataType());
		localIdEClass.getESuperTypes().add(this.getString());
		stringEClass.getESuperTypes().add(this.getBaseDataType());
		dateTimeEClass.getESuperTypes().add(this.getBaseDataType());
		guidEClass.getESuperTypes().add(this.getBaseDataType());
		byteStringEClass.getESuperTypes().add(this.getBaseDataType());
		xmlElementEClass.getESuperTypes().add(this.getBaseDataType());
		nodeIdEClass.getESuperTypes().add(this.getBaseDataType());
		expandedNodeIdEClass.getESuperTypes().add(this.getBaseDataType());
		statusCodeEClass.getESuperTypes().add(this.getBaseDataType());
		structureEClass.getESuperTypes().add(this.getBaseDataType());
		dataValueEClass.getESuperTypes().add(this.getBaseDataType());
		diagnosticInfoEClass.getESuperTypes().add(this.getBaseDataType());
		rsaEncryptedSecretEClass.getESuperTypes().add(this.getBaseDataType());
		eccEncryptedSecretEClass.getESuperTypes().add(this.getBaseDataType());
		integerEClass.getESuperTypes().add(this.getNumber());
		floatEClass.getESuperTypes().add(this.getNumber());
		uIntegerEClass.getESuperTypes().add(this.getNumber());
		durationEClass.getESuperTypes().add(this.getDouble());
		int64EClass.getESuperTypes().add(this.getInteger());
		int32EClass.getESuperTypes().add(this.getInteger());
		int16EClass.getESuperTypes().add(this.getInteger());
		sByteEClass.getESuperTypes().add(this.getInteger());
		byteEClass.getESuperTypes().add(this.getUInteger());
		argumentEClass.getESuperTypes().add(this.getStructure());
		uInt16EClass.getESuperTypes().add(this.getUInteger());
		uInt32EClass.getESuperTypes().add(this.getUInteger());
		uInt64EClass.getESuperTypes().add(this.getUInteger());
		imageEClass.getESuperTypes().add(this.getByteString());
		gifEClass.getESuperTypes().add(this.getImage());
		bmpEClass.getESuperTypes().add(this.getImage());
		pngEClass.getESuperTypes().add(this.getImage());
		jpgEClass.getESuperTypes().add(this.getImage());

		// Initialize classes, features, and operations; add parameters
		initEClass(analogUnitTypeEClass, AnalogUnitType.class, "AnalogUnitType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(doubleEClass, org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Double.class, "Double", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(numberEClass, org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Number.class, "Number", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(baseDataTypeEClass, BaseDataType.class, "BaseDataType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBaseDataType_NodeId(), theTypesPackage.getString(), "NodeId", "i=24", 1, 1, BaseDataType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBaseDataType_NamespaceUri(), theTypesPackage.getString(), "NamespaceUri", "http://opcfoundation.org/UA/", 1, 1, BaseDataType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(localizedTextEClass, LocalizedText.class, "LocalizedText", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLocalizedText_Value(), theTypesPackage.getString(), "value", null, 1, 1, LocalizedText.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getLocalizedText_Local(), this.getLocalId(), null, "local", null, 1, 1, LocalizedText.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(localIdEClass, LocalId.class, "LocalId", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(stringEClass, org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.String.class, "String", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dateTimeEClass, DateTime.class, "DateTime", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(guidEClass, Guid.class, "Guid", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(byteStringEClass, ByteString.class, "ByteString", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(xmlElementEClass, XmlElement.class, "XmlElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(nodeIdEClass, NodeId.class, "NodeId", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(expandedNodeIdEClass, ExpandedNodeId.class, "ExpandedNodeId", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(statusCodeEClass, StatusCode.class, "StatusCode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(qualifiedNameEClass, QualifiedName.class, "QualifiedName", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(structureEClass, Structure.class, "Structure", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dataValueEClass, DataValue.class, "DataValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(diagnosticInfoEClass, DiagnosticInfo.class, "DiagnosticInfo", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(rsaEncryptedSecretEClass, RsaEncryptedSecret.class, "RsaEncryptedSecret", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(eccEncryptedSecretEClass, EccEncryptedSecret.class, "EccEncryptedSecret", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(integerEClass, org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Integer.class, "Integer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(floatEClass, org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Float.class, "Float", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(uIntegerEClass, UInteger.class, "UInteger", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(durationEClass, Duration.class, "Duration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(int64EClass, Int64.class, "Int64", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(int32EClass, Int32.class, "Int32", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(int16EClass, Int16.class, "Int16", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(sByteEClass, SByte.class, "SByte", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(byteEClass, org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Byte.class, "Byte", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(argumentEClass, Argument.class, "Argument", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(uInt16EClass, UInt16.class, "UInt16", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(uInt32EClass, UInt32.class, "UInt32", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(uInt64EClass, UInt64.class, "UInt64", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(imageEClass, Image.class, "Image", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(gifEClass, org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.GIF.class, "GIF", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(bmpEClass, org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.BMP.class, "BMP", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(pngEClass, org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.PNG.class, "PNG", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(jpgEClass, org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.JPG.class, "JPG", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(nodeCLassEEnum, NodeCLass.class, "NodeCLass");
		addEEnumLiteral(nodeCLassEEnum, NodeCLass.UNSPECIFIED);
		addEEnumLiteral(nodeCLassEEnum, NodeCLass.OBJECT);
		addEEnumLiteral(nodeCLassEEnum, NodeCLass.VARIABLE);
		addEEnumLiteral(nodeCLassEEnum, NodeCLass.METHOD);
		addEEnumLiteral(nodeCLassEEnum, NodeCLass.OBJECT_TYPE);
		addEEnumLiteral(nodeCLassEEnum, NodeCLass.VARIABLE_TYPE);
		addEEnumLiteral(nodeCLassEEnum, NodeCLass.REFERENCE_TYPE);
		addEEnumLiteral(nodeCLassEEnum, NodeCLass.DATA_TYPE);
		addEEnumLiteral(nodeCLassEEnum, NodeCLass.VIEW);

		initEEnum(idTypeEEnum, IdType.class, "IdType");
		addEEnumLiteral(idTypeEEnum, IdType.NUMERIC);
		addEEnumLiteral(idTypeEEnum, IdType.STRING);
		addEEnumLiteral(idTypeEEnum, IdType.GUID);
		addEEnumLiteral(idTypeEEnum, IdType.OPAQUE);

		initEEnum(namingRuleTypeEEnum, NamingRuleType.class, "NamingRuleType");

		initEEnum(securityTokenRequestTypeEEnum, SecurityTokenRequestType.class, "SecurityTokenRequestType");

		initEEnum(redundancySupportEEnum, RedundancySupport.class, "RedundancySupport");

		initEEnum(messageSecurityModeEEnum, MessageSecurityMode.class, "MessageSecurityMode");

		initEEnum(serverStateEEnum, ServerState.class, "ServerState");
	}

} //OPC_UA_LibraryPackageImpl
