/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.di.opcuadiprofile;

import org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Topology Element Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType#getGroupIdentifier <em>Group Identifier</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType#getIdentification <em>Identification</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType#getLock <em>Lock</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType#getParameterSet <em>Parameter Set</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType#getMethodSet <em>Method Set</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getTopologyElementType()
 * @model abstract="true"
 * @generated
 */
public interface TopologyElementType extends BaseObjectType {
	/**
	 * Returns the value of the '<em><b>Group Identifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group Identifier</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group Identifier</em>' reference.
	 * @see #setGroupIdentifier(FunctionalGroupType)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getTopologyElementType_GroupIdentifier()
	 * @model ordered="false"
	 * @generated
	 */
	FunctionalGroupType getGroupIdentifier();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType#getGroupIdentifier <em>Group Identifier</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group Identifier</em>' reference.
	 * @see #getGroupIdentifier()
	 * @generated
	 */
	void setGroupIdentifier(FunctionalGroupType value);

	/**
	 * Returns the value of the '<em><b>Identification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Identification</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identification</em>' reference.
	 * @see #setIdentification(FunctionalGroupType)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getTopologyElementType_Identification()
	 * @model ordered="false"
	 * @generated
	 */
	FunctionalGroupType getIdentification();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType#getIdentification <em>Identification</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Identification</em>' reference.
	 * @see #getIdentification()
	 * @generated
	 */
	void setIdentification(FunctionalGroupType value);

	/**
	 * Returns the value of the '<em><b>Lock</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lock</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lock</em>' reference.
	 * @see #setLock(LockingServicesType)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getTopologyElementType_Lock()
	 * @model ordered="false"
	 * @generated
	 */
	LockingServicesType getLock();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType#getLock <em>Lock</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lock</em>' reference.
	 * @see #getLock()
	 * @generated
	 */
	void setLock(LockingServicesType value);

	/**
	 * Returns the value of the '<em><b>Parameter Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Set</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Set</em>' containment reference.
	 * @see #setParameterSet(ParameterSetTopologyElementType)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getTopologyElementType_ParameterSet()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	ParameterSetTopologyElementType getParameterSet();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType#getParameterSet <em>Parameter Set</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Set</em>' containment reference.
	 * @see #getParameterSet()
	 * @generated
	 */
	void setParameterSet(ParameterSetTopologyElementType value);

	/**
	 * Returns the value of the '<em><b>Method Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Method Set</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Method Set</em>' containment reference.
	 * @see #setMethodSet(MethodSetTopologyElementType)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getTopologyElementType_MethodSet()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	MethodSetTopologyElementType getMethodSet();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType#getMethodSet <em>Method Set</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Method Set</em>' containment reference.
	 * @see #getMethodSet()
	 * @generated
	 */
	void setMethodSet(MethodSetTopologyElementType value);

} // TopologyElementType
