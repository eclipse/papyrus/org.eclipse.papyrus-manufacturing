/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.opcuaprofile;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPCUAProfileFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='OPC_UA'"
 * @generated
 */
public interface OPCUAProfilePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "opcuaprofile";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://Papyrus/OPCUA";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "OPCUAProfile";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OPCUAProfilePackage eINSTANCE = org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseObjectTypeImpl <em>Base Object Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseObjectTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getBaseObjectType()
	 * @generated
	 */
	int BASE_OBJECT_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_OBJECT_TYPE__BASE_CLASS = 0;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_OBJECT_TYPE__NODE_ID = 1;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_OBJECT_TYPE__NAMESPACE_URI = 2;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_OBJECT_TYPE__BROWSE_NAME = 3;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_OBJECT_TYPE__NODE_CLASS = 4;

	/**
	 * The number of structural features of the '<em>Base Object Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_OBJECT_TYPE_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Base Object Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_OBJECT_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.DataTypeSystemTypeImpl <em>Data Type System Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.DataTypeSystemTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getDataTypeSystemType()
	 * @generated
	 */
	int DATA_TYPE_SYSTEM_TYPE = 1;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE_SYSTEM_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE_SYSTEM_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE_SYSTEM_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE_SYSTEM_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE_SYSTEM_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Data Type System Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE_SYSTEM_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Data Type System Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE_SYSTEM_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ModellingRuleTypeImpl <em>Modelling Rule Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ModellingRuleTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getModellingRuleType()
	 * @generated
	 */
	int MODELLING_RULE_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODELLING_RULE_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODELLING_RULE_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODELLING_RULE_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODELLING_RULE_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODELLING_RULE_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Modelling Rule Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODELLING_RULE_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Modelling Rule Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODELLING_RULE_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerTypeImpl <em>Server Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getServerType()
	 * @generated
	 */
	int SERVER_TYPE = 3;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Server Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Server Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.FolderTypeImpl <em>Folder Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.FolderTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getFolderType()
	 * @generated
	 */
	int FOLDER_TYPE = 4;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOLDER_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOLDER_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOLDER_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOLDER_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOLDER_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Folder Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOLDER_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Folder Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOLDER_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.DataTypeEncodingTypeImpl <em>Data Type Encoding Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.DataTypeEncodingTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getDataTypeEncodingType()
	 * @generated
	 */
	int DATA_TYPE_ENCODING_TYPE = 5;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE_ENCODING_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE_ENCODING_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE_ENCODING_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE_ENCODING_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE_ENCODING_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Data Type Encoding Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE_ENCODING_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Data Type Encoding Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE_ENCODING_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerCapabilitiesTypeImpl <em>Server Capabilities Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerCapabilitiesTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getServerCapabilitiesType()
	 * @generated
	 */
	int SERVER_CAPABILITIES_TYPE = 6;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_CAPABILITIES_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_CAPABILITIES_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_CAPABILITIES_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_CAPABILITIES_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_CAPABILITIES_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Server Capabilities Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_CAPABILITIES_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Server Capabilities Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_CAPABILITIES_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerDiagnosticsTypeImpl <em>Server Diagnostics Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerDiagnosticsTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getServerDiagnosticsType()
	 * @generated
	 */
	int SERVER_DIAGNOSTICS_TYPE = 7;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_DIAGNOSTICS_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_DIAGNOSTICS_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_DIAGNOSTICS_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_DIAGNOSTICS_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_DIAGNOSTICS_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Server Diagnostics Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_DIAGNOSTICS_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Server Diagnostics Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_DIAGNOSTICS_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.SessionsDiagnosticsSummaryTypeImpl <em>Sessions Diagnostics Summary Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.SessionsDiagnosticsSummaryTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getSessionsDiagnosticsSummaryType()
	 * @generated
	 */
	int SESSIONS_DIAGNOSTICS_SUMMARY_TYPE = 8;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSIONS_DIAGNOSTICS_SUMMARY_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSIONS_DIAGNOSTICS_SUMMARY_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSIONS_DIAGNOSTICS_SUMMARY_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSIONS_DIAGNOSTICS_SUMMARY_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSIONS_DIAGNOSTICS_SUMMARY_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Sessions Diagnostics Summary Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSIONS_DIAGNOSTICS_SUMMARY_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Sessions Diagnostics Summary Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSIONS_DIAGNOSTICS_SUMMARY_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.SessionDiagnosticsObjectTypeImpl <em>Session Diagnostics Object Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.SessionDiagnosticsObjectTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getSessionDiagnosticsObjectType()
	 * @generated
	 */
	int SESSION_DIAGNOSTICS_OBJECT_TYPE = 9;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_DIAGNOSTICS_OBJECT_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_DIAGNOSTICS_OBJECT_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_DIAGNOSTICS_OBJECT_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_DIAGNOSTICS_OBJECT_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_DIAGNOSTICS_OBJECT_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Session Diagnostics Object Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_DIAGNOSTICS_OBJECT_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Session Diagnostics Object Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_DIAGNOSTICS_OBJECT_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.VendorServerInfoTypeImpl <em>Vendor Server Info Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.VendorServerInfoTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getVendorServerInfoType()
	 * @generated
	 */
	int VENDOR_SERVER_INFO_TYPE = 10;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VENDOR_SERVER_INFO_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VENDOR_SERVER_INFO_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VENDOR_SERVER_INFO_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VENDOR_SERVER_INFO_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VENDOR_SERVER_INFO_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Vendor Server Info Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VENDOR_SERVER_INFO_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Vendor Server Info Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VENDOR_SERVER_INFO_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerRedundancyTypeImpl <em>Server Redundancy Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerRedundancyTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getServerRedundancyType()
	 * @generated
	 */
	int SERVER_REDUNDANCY_TYPE = 11;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_REDUNDANCY_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_REDUNDANCY_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_REDUNDANCY_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_REDUNDANCY_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_REDUNDANCY_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Server Redundancy Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_REDUNDANCY_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Server Redundancy Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_REDUNDANCY_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.FileTypeImpl <em>File Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.FileTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getFileType()
	 * @generated
	 */
	int FILE_TYPE = 12;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>File Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>File Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.NamespacesTypeImpl <em>Namespaces Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.NamespacesTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getNamespacesType()
	 * @generated
	 */
	int NAMESPACES_TYPE = 13;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACES_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACES_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACES_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACES_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACES_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Namespaces Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACES_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Namespaces Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACES_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseEventTypeImpl <em>Base Event Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseEventTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getBaseEventType()
	 * @generated
	 */
	int BASE_EVENT_TYPE = 14;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_EVENT_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_EVENT_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_EVENT_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_EVENT_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_EVENT_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Base Event Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_EVENT_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Base Event Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_EVENT_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.AggregateFunctionTypeImpl <em>Aggregate Function Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.AggregateFunctionTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getAggregateFunctionType()
	 * @generated
	 */
	int AGGREGATE_FUNCTION_TYPE = 15;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATE_FUNCTION_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATE_FUNCTION_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATE_FUNCTION_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATE_FUNCTION_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATE_FUNCTION_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Aggregate Function Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATE_FUNCTION_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Aggregate Function Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATE_FUNCTION_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.StateMachineTypeImpl <em>State Machine Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.StateMachineTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getStateMachineType()
	 * @generated
	 */
	int STATE_MACHINE_TYPE = 16;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>State Machine Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>State Machine Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.StateTypeImpl <em>State Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.StateTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getStateType()
	 * @generated
	 */
	int STATE_TYPE = 17;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>State Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>State Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.NamespaceMetadataTypeImpl <em>Namespace Metadata Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.NamespaceMetadataTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getNamespaceMetadataType()
	 * @generated
	 */
	int NAMESPACE_METADATA_TYPE = 18;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACE_METADATA_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACE_METADATA_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACE_METADATA_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACE_METADATA_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACE_METADATA_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Namespace Metadata Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACE_METADATA_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Namespace Metadata Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACE_METADATA_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.WriterGroupMessageTypeImpl <em>Writer Group Message Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.WriterGroupMessageTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getWriterGroupMessageType()
	 * @generated
	 */
	int WRITER_GROUP_MESSAGE_TYPE = 19;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITER_GROUP_MESSAGE_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITER_GROUP_MESSAGE_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITER_GROUP_MESSAGE_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITER_GROUP_MESSAGE_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITER_GROUP_MESSAGE_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Writer Group Message Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITER_GROUP_MESSAGE_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Writer Group Message Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITER_GROUP_MESSAGE_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.TransitionTypeImpl <em>Transition Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.TransitionTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getTransitionType()
	 * @generated
	 */
	int TRANSITION_TYPE = 20;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Transition Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Transition Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.TemporaryFileTransferTypeImpl <em>Temporary File Transfer Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.TemporaryFileTransferTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getTemporaryFileTransferType()
	 * @generated
	 */
	int TEMPORARY_FILE_TRANSFER_TYPE = 21;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPORARY_FILE_TRANSFER_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPORARY_FILE_TRANSFER_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPORARY_FILE_TRANSFER_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPORARY_FILE_TRANSFER_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPORARY_FILE_TRANSFER_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Temporary File Transfer Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPORARY_FILE_TRANSFER_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Temporary File Transfer Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPORARY_FILE_TRANSFER_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.RoleSetTypeImpl <em>Role Set Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.RoleSetTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getRoleSetType()
	 * @generated
	 */
	int ROLE_SET_TYPE = 22;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_SET_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_SET_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_SET_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_SET_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_SET_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Role Set Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_SET_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Role Set Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_SET_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.RoleTypeImpl <em>Role Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.RoleTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getRoleType()
	 * @generated
	 */
	int ROLE_TYPE = 23;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Role Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Role Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseInterfaceTypeImpl <em>Base Interface Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseInterfaceTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getBaseInterfaceType()
	 * @generated
	 */
	int BASE_INTERFACE_TYPE = 24;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_INTERFACE_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_INTERFACE_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_INTERFACE_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_INTERFACE_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_INTERFACE_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_INTERFACE_TYPE__BASE_INTERFACE = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Base Interface Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_INTERFACE_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Base Interface Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_INTERFACE_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.DictionaryEntryTypeImpl <em>Dictionary Entry Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.DictionaryEntryTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getDictionaryEntryType()
	 * @generated
	 */
	int DICTIONARY_ENTRY_TYPE = 25;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DICTIONARY_ENTRY_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DICTIONARY_ENTRY_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DICTIONARY_ENTRY_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DICTIONARY_ENTRY_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DICTIONARY_ENTRY_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Dictionary Entry Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DICTIONARY_ENTRY_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dictionary Entry Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DICTIONARY_ENTRY_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.OrderedListTypeImpl <em>Ordered List Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OrderedListTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getOrderedListType()
	 * @generated
	 */
	int ORDERED_LIST_TYPE = 26;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDERED_LIST_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDERED_LIST_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDERED_LIST_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDERED_LIST_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDERED_LIST_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Ordered List Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDERED_LIST_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ordered List Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORDERED_LIST_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseConditionClassTypeImpl <em>Base Condition Class Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseConditionClassTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getBaseConditionClassType()
	 * @generated
	 */
	int BASE_CONDITION_CLASS_TYPE = 27;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CONDITION_CLASS_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CONDITION_CLASS_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CONDITION_CLASS_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CONDITION_CLASS_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CONDITION_CLASS_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Base Condition Class Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CONDITION_CLASS_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Base Condition Class Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CONDITION_CLASS_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.AlarmMetricsTypeImpl <em>Alarm Metrics Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.AlarmMetricsTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getAlarmMetricsType()
	 * @generated
	 */
	int ALARM_METRICS_TYPE = 28;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_METRICS_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_METRICS_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_METRICS_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_METRICS_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_METRICS_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Alarm Metrics Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_METRICS_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Alarm Metrics Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_METRICS_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.HistoricalDataConfigurationTypeImpl <em>Historical Data Configuration Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.HistoricalDataConfigurationTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getHistoricalDataConfigurationType()
	 * @generated
	 */
	int HISTORICAL_DATA_CONFIGURATION_TYPE = 29;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORICAL_DATA_CONFIGURATION_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORICAL_DATA_CONFIGURATION_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORICAL_DATA_CONFIGURATION_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORICAL_DATA_CONFIGURATION_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORICAL_DATA_CONFIGURATION_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Historical Data Configuration Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORICAL_DATA_CONFIGURATION_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Historical Data Configuration Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORICAL_DATA_CONFIGURATION_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.HistoryServerCapabilitiesTypeImpl <em>History Server Capabilities Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.HistoryServerCapabilitiesTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getHistoryServerCapabilitiesType()
	 * @generated
	 */
	int HISTORY_SERVER_CAPABILITIES_TYPE = 30;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_SERVER_CAPABILITIES_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_SERVER_CAPABILITIES_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_SERVER_CAPABILITIES_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_SERVER_CAPABILITIES_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_SERVER_CAPABILITIES_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>History Server Capabilities Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_SERVER_CAPABILITIES_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>History Server Capabilities Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_SERVER_CAPABILITIES_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.CertificateGroupTypeImpl <em>Certificate Group Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.CertificateGroupTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getCertificateGroupType()
	 * @generated
	 */
	int CERTIFICATE_GROUP_TYPE = 31;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFICATE_GROUP_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFICATE_GROUP_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFICATE_GROUP_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFICATE_GROUP_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFICATE_GROUP_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Certificate Group Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFICATE_GROUP_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Certificate Group Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFICATE_GROUP_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.CertificateTypeImpl <em>Certificate Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.CertificateTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getCertificateType()
	 * @generated
	 */
	int CERTIFICATE_TYPE = 32;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFICATE_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFICATE_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFICATE_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFICATE_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFICATE_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Certificate Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFICATE_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Certificate Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFICATE_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerConfigurationTypeImpl <em>Server Configuration Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerConfigurationTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getServerConfigurationType()
	 * @generated
	 */
	int SERVER_CONFIGURATION_TYPE = 33;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_CONFIGURATION_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_CONFIGURATION_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_CONFIGURATION_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_CONFIGURATION_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_CONFIGURATION_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Server Configuration Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_CONFIGURATION_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Server Configuration Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_CONFIGURATION_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.KeyCredentialConfigurationTypeImpl <em>Key Credential Configuration Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.KeyCredentialConfigurationTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getKeyCredentialConfigurationType()
	 * @generated
	 */
	int KEY_CREDENTIAL_CONFIGURATION_TYPE = 34;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_CREDENTIAL_CONFIGURATION_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_CREDENTIAL_CONFIGURATION_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_CREDENTIAL_CONFIGURATION_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_CREDENTIAL_CONFIGURATION_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_CREDENTIAL_CONFIGURATION_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Key Credential Configuration Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_CREDENTIAL_CONFIGURATION_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Key Credential Configuration Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_CREDENTIAL_CONFIGURATION_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.AuthorizationServiceConfigurationTypeImpl <em>Authorization Service Configuration Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.AuthorizationServiceConfigurationTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getAuthorizationServiceConfigurationType()
	 * @generated
	 */
	int AUTHORIZATION_SERVICE_CONFIGURATION_TYPE = 35;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTHORIZATION_SERVICE_CONFIGURATION_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTHORIZATION_SERVICE_CONFIGURATION_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTHORIZATION_SERVICE_CONFIGURATION_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTHORIZATION_SERVICE_CONFIGURATION_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTHORIZATION_SERVICE_CONFIGURATION_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Authorization Service Configuration Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTHORIZATION_SERVICE_CONFIGURATION_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Authorization Service Configuration Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTHORIZATION_SERVICE_CONFIGURATION_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.AggregateConfigurationTypeImpl <em>Aggregate Configuration Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.AggregateConfigurationTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getAggregateConfigurationType()
	 * @generated
	 */
	int AGGREGATE_CONFIGURATION_TYPE = 36;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATE_CONFIGURATION_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATE_CONFIGURATION_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATE_CONFIGURATION_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATE_CONFIGURATION_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATE_CONFIGURATION_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Aggregate Configuration Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATE_CONFIGURATION_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Aggregate Configuration Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATE_CONFIGURATION_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.PubSubKeyServiceTypeImpl <em>Pub Sub Key Service Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.PubSubKeyServiceTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getPubSubKeyServiceType()
	 * @generated
	 */
	int PUB_SUB_KEY_SERVICE_TYPE = 37;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_KEY_SERVICE_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_KEY_SERVICE_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_KEY_SERVICE_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_KEY_SERVICE_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_KEY_SERVICE_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Pub Sub Key Service Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_KEY_SERVICE_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Pub Sub Key Service Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_KEY_SERVICE_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.SecurityGroupTypeImpl <em>Security Group Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.SecurityGroupTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getSecurityGroupType()
	 * @generated
	 */
	int SECURITY_GROUP_TYPE = 38;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_GROUP_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_GROUP_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_GROUP_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_GROUP_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_GROUP_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Security Group Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_GROUP_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Security Group Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_GROUP_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.PublishedDataSetTypeImpl <em>Published Data Set Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.PublishedDataSetTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getPublishedDataSetType()
	 * @generated
	 */
	int PUBLISHED_DATA_SET_TYPE = 39;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLISHED_DATA_SET_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLISHED_DATA_SET_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLISHED_DATA_SET_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLISHED_DATA_SET_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLISHED_DATA_SET_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Published Data Set Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLISHED_DATA_SET_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Published Data Set Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLISHED_DATA_SET_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ExtensionFieldsTypeImpl <em>Extension Fields Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ExtensionFieldsTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getExtensionFieldsType()
	 * @generated
	 */
	int EXTENSION_FIELDS_TYPE = 40;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_FIELDS_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_FIELDS_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_FIELDS_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_FIELDS_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_FIELDS_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Extension Fields Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_FIELDS_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Extension Fields Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_FIELDS_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.PubSubConnectionTypeImpl <em>Pub Sub Connection Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.PubSubConnectionTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getPubSubConnectionType()
	 * @generated
	 */
	int PUB_SUB_CONNECTION_TYPE = 41;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_CONNECTION_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_CONNECTION_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_CONNECTION_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_CONNECTION_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_CONNECTION_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Pub Sub Connection Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_CONNECTION_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Pub Sub Connection Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_CONNECTION_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ConnectionTransportTypeImpl <em>Connection Transport Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ConnectionTransportTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getConnectionTransportType()
	 * @generated
	 */
	int CONNECTION_TRANSPORT_TYPE = 42;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_TRANSPORT_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_TRANSPORT_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_TRANSPORT_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_TRANSPORT_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_TRANSPORT_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Connection Transport Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_TRANSPORT_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Connection Transport Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_TRANSPORT_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.PubSubGroupTypeImpl <em>Pub Sub Group Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.PubSubGroupTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getPubSubGroupType()
	 * @generated
	 */
	int PUB_SUB_GROUP_TYPE = 43;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_GROUP_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_GROUP_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_GROUP_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_GROUP_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_GROUP_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Pub Sub Group Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_GROUP_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Pub Sub Group Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_GROUP_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.WriterGroupTransportTypeImpl <em>Writer Group Transport Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.WriterGroupTransportTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getWriterGroupTransportType()
	 * @generated
	 */
	int WRITER_GROUP_TRANSPORT_TYPE = 44;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITER_GROUP_TRANSPORT_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITER_GROUP_TRANSPORT_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITER_GROUP_TRANSPORT_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITER_GROUP_TRANSPORT_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITER_GROUP_TRANSPORT_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Writer Group Transport Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITER_GROUP_TRANSPORT_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Writer Group Transport Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITER_GROUP_TRANSPORT_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ReaderGroupTransportTypeImpl <em>Reader Group Transport Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ReaderGroupTransportTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getReaderGroupTransportType()
	 * @generated
	 */
	int READER_GROUP_TRANSPORT_TYPE = 45;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READER_GROUP_TRANSPORT_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READER_GROUP_TRANSPORT_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READER_GROUP_TRANSPORT_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READER_GROUP_TRANSPORT_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READER_GROUP_TRANSPORT_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Reader Group Transport Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READER_GROUP_TRANSPORT_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Reader Group Transport Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READER_GROUP_TRANSPORT_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ReaderGroupMessageTypeImpl <em>Reader Group Message Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ReaderGroupMessageTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getReaderGroupMessageType()
	 * @generated
	 */
	int READER_GROUP_MESSAGE_TYPE = 46;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READER_GROUP_MESSAGE_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READER_GROUP_MESSAGE_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READER_GROUP_MESSAGE_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READER_GROUP_MESSAGE_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READER_GROUP_MESSAGE_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Reader Group Message Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READER_GROUP_MESSAGE_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Reader Group Message Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READER_GROUP_MESSAGE_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.DataSetWriterTypeImpl <em>Data Set Writer Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.DataSetWriterTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getDataSetWriterType()
	 * @generated
	 */
	int DATA_SET_WRITER_TYPE = 47;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_WRITER_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_WRITER_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_WRITER_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_WRITER_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_WRITER_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Data Set Writer Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_WRITER_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Data Set Writer Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_WRITER_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.DataSetWriterTransportTypeImpl <em>Data Set Writer Transport Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.DataSetWriterTransportTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getDataSetWriterTransportType()
	 * @generated
	 */
	int DATA_SET_WRITER_TRANSPORT_TYPE = 48;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_WRITER_TRANSPORT_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_WRITER_TRANSPORT_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_WRITER_TRANSPORT_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_WRITER_TRANSPORT_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_WRITER_TRANSPORT_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Data Set Writer Transport Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_WRITER_TRANSPORT_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Data Set Writer Transport Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_WRITER_TRANSPORT_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.DataSetWriterMessageTypeImpl <em>Data Set Writer Message Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.DataSetWriterMessageTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getDataSetWriterMessageType()
	 * @generated
	 */
	int DATA_SET_WRITER_MESSAGE_TYPE = 49;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_WRITER_MESSAGE_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_WRITER_MESSAGE_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_WRITER_MESSAGE_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_WRITER_MESSAGE_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_WRITER_MESSAGE_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Data Set Writer Message Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_WRITER_MESSAGE_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Data Set Writer Message Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_WRITER_MESSAGE_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.DataSetReaderTypeImpl <em>Data Set Reader Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.DataSetReaderTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getDataSetReaderType()
	 * @generated
	 */
	int DATA_SET_READER_TYPE = 50;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_READER_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_READER_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_READER_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_READER_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_READER_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Data Set Reader Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_READER_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Data Set Reader Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_READER_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.DataSetReaderTransportTypeImpl <em>Data Set Reader Transport Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.DataSetReaderTransportTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getDataSetReaderTransportType()
	 * @generated
	 */
	int DATA_SET_READER_TRANSPORT_TYPE = 51;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_READER_TRANSPORT_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_READER_TRANSPORT_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_READER_TRANSPORT_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_READER_TRANSPORT_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_READER_TRANSPORT_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Data Set Reader Transport Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_READER_TRANSPORT_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Data Set Reader Transport Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_READER_TRANSPORT_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.DataSetReaderMessageTypeImpl <em>Data Set Reader Message Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.DataSetReaderMessageTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getDataSetReaderMessageType()
	 * @generated
	 */
	int DATA_SET_READER_MESSAGE_TYPE = 52;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_READER_MESSAGE_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_READER_MESSAGE_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_READER_MESSAGE_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_READER_MESSAGE_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_READER_MESSAGE_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Data Set Reader Message Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_READER_MESSAGE_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Data Set Reader Message Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_READER_MESSAGE_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.SubscribedDataSetTypeImpl <em>Subscribed Data Set Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.SubscribedDataSetTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getSubscribedDataSetType()
	 * @generated
	 */
	int SUBSCRIBED_DATA_SET_TYPE = 53;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIBED_DATA_SET_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIBED_DATA_SET_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIBED_DATA_SET_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIBED_DATA_SET_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIBED_DATA_SET_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Subscribed Data Set Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIBED_DATA_SET_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Subscribed Data Set Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIBED_DATA_SET_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.PubSubStatusTypeImpl <em>Pub Sub Status Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.PubSubStatusTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getPubSubStatusType()
	 * @generated
	 */
	int PUB_SUB_STATUS_TYPE = 54;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_STATUS_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_STATUS_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_STATUS_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_STATUS_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_STATUS_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Pub Sub Status Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_STATUS_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Pub Sub Status Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_STATUS_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.PubSubDiagnosticsTypeImpl <em>Pub Sub Diagnostics Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.PubSubDiagnosticsTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getPubSubDiagnosticsType()
	 * @generated
	 */
	int PUB_SUB_DIAGNOSTICS_TYPE = 55;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_DIAGNOSTICS_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_DIAGNOSTICS_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_DIAGNOSTICS_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_DIAGNOSTICS_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_DIAGNOSTICS_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Pub Sub Diagnostics Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_DIAGNOSTICS_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Pub Sub Diagnostics Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUB_SUB_DIAGNOSTICS_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.NetworkAddressTypeImpl <em>Network Address Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.NetworkAddressTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getNetworkAddressType()
	 * @generated
	 */
	int NETWORK_ADDRESS_TYPE = 56;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NETWORK_ADDRESS_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NETWORK_ADDRESS_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NETWORK_ADDRESS_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NETWORK_ADDRESS_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NETWORK_ADDRESS_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Network Address Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NETWORK_ADDRESS_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Network Address Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NETWORK_ADDRESS_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.AliasNameTypeImpl <em>Alias Name Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.AliasNameTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getAliasNameType()
	 * @generated
	 */
	int ALIAS_NAME_TYPE = 57;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALIAS_NAME_TYPE__BASE_CLASS = BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALIAS_NAME_TYPE__NODE_ID = BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALIAS_NAME_TYPE__NAMESPACE_URI = BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALIAS_NAME_TYPE__BROWSE_NAME = BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALIAS_NAME_TYPE__NODE_CLASS = BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Alias Name Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALIAS_NAME_TYPE_FEATURE_COUNT = BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Alias Name Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALIAS_NAME_TYPE_OPERATION_COUNT = BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseVariableTypeImpl <em>Base Variable Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseVariableTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getBaseVariableType()
	 * @generated
	 */
	int BASE_VARIABLE_TYPE = 58;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_VARIABLE_TYPE__BASE_CLASS = 0;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_VARIABLE_TYPE__BASE_PROPERTY = 1;

	/**
	 * The feature id for the '<em><b>Value Rank</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_VARIABLE_TYPE__VALUE_RANK = 2;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_VARIABLE_TYPE__DATA_TYPE = 3;

	/**
	 * The number of structural features of the '<em>Base Variable Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_VARIABLE_TYPE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Base Variable Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_VARIABLE_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseDataVariableTypeImpl <em>Base Data Variable Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseDataVariableTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getBaseDataVariableType()
	 * @generated
	 */
	int BASE_DATA_VARIABLE_TYPE = 59;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_DATA_VARIABLE_TYPE__BASE_CLASS = BASE_VARIABLE_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_DATA_VARIABLE_TYPE__BASE_PROPERTY = BASE_VARIABLE_TYPE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Value Rank</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_DATA_VARIABLE_TYPE__VALUE_RANK = BASE_VARIABLE_TYPE__VALUE_RANK;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_DATA_VARIABLE_TYPE__DATA_TYPE = BASE_VARIABLE_TYPE__DATA_TYPE;

	/**
	 * The number of structural features of the '<em>Base Data Variable Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_DATA_VARIABLE_TYPE_FEATURE_COUNT = BASE_VARIABLE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Base Data Variable Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_DATA_VARIABLE_TYPE_OPERATION_COUNT = BASE_VARIABLE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.PropertyTypeImpl <em>Property Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.PropertyTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getPropertyType()
	 * @generated
	 */
	int PROPERTY_TYPE = 60;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TYPE__BASE_CLASS = BASE_VARIABLE_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TYPE__BASE_PROPERTY = BASE_VARIABLE_TYPE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Value Rank</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TYPE__VALUE_RANK = BASE_VARIABLE_TYPE__VALUE_RANK;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TYPE__DATA_TYPE = BASE_VARIABLE_TYPE__DATA_TYPE;

	/**
	 * The number of structural features of the '<em>Property Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TYPE_FEATURE_COUNT = BASE_VARIABLE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Property Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_TYPE_OPERATION_COUNT = BASE_VARIABLE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerVendorCapabilityTypeImpl <em>Server Vendor Capability Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerVendorCapabilityTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getServerVendorCapabilityType()
	 * @generated
	 */
	int SERVER_VENDOR_CAPABILITY_TYPE = 61;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_VENDOR_CAPABILITY_TYPE__BASE_CLASS = BASE_DATA_VARIABLE_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_VENDOR_CAPABILITY_TYPE__BASE_PROPERTY = BASE_DATA_VARIABLE_TYPE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Value Rank</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_VENDOR_CAPABILITY_TYPE__VALUE_RANK = BASE_DATA_VARIABLE_TYPE__VALUE_RANK;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_VENDOR_CAPABILITY_TYPE__DATA_TYPE = BASE_DATA_VARIABLE_TYPE__DATA_TYPE;

	/**
	 * The number of structural features of the '<em>Server Vendor Capability Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_VENDOR_CAPABILITY_TYPE_FEATURE_COUNT = BASE_DATA_VARIABLE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Server Vendor Capability Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_VENDOR_CAPABILITY_TYPE_OPERATION_COUNT = BASE_DATA_VARIABLE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.SamplingIntervalDiagnosticsArrayTypeImpl <em>Sampling Interval Diagnostics Array Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.SamplingIntervalDiagnosticsArrayTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getSamplingIntervalDiagnosticsArrayType()
	 * @generated
	 */
	int SAMPLING_INTERVAL_DIAGNOSTICS_ARRAY_TYPE = 62;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAMPLING_INTERVAL_DIAGNOSTICS_ARRAY_TYPE__BASE_CLASS = BASE_DATA_VARIABLE_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAMPLING_INTERVAL_DIAGNOSTICS_ARRAY_TYPE__BASE_PROPERTY = BASE_DATA_VARIABLE_TYPE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Value Rank</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAMPLING_INTERVAL_DIAGNOSTICS_ARRAY_TYPE__VALUE_RANK = BASE_DATA_VARIABLE_TYPE__VALUE_RANK;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAMPLING_INTERVAL_DIAGNOSTICS_ARRAY_TYPE__DATA_TYPE = BASE_DATA_VARIABLE_TYPE__DATA_TYPE;

	/**
	 * The number of structural features of the '<em>Sampling Interval Diagnostics Array Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAMPLING_INTERVAL_DIAGNOSTICS_ARRAY_TYPE_FEATURE_COUNT = BASE_DATA_VARIABLE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Sampling Interval Diagnostics Array Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAMPLING_INTERVAL_DIAGNOSTICS_ARRAY_TYPE_OPERATION_COUNT = BASE_DATA_VARIABLE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.SamplingIntervalDiagnosticsTypeImpl <em>Sampling Interval Diagnostics Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.SamplingIntervalDiagnosticsTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getSamplingIntervalDiagnosticsType()
	 * @generated
	 */
	int SAMPLING_INTERVAL_DIAGNOSTICS_TYPE = 63;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAMPLING_INTERVAL_DIAGNOSTICS_TYPE__BASE_CLASS = BASE_DATA_VARIABLE_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAMPLING_INTERVAL_DIAGNOSTICS_TYPE__BASE_PROPERTY = BASE_DATA_VARIABLE_TYPE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Value Rank</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAMPLING_INTERVAL_DIAGNOSTICS_TYPE__VALUE_RANK = BASE_DATA_VARIABLE_TYPE__VALUE_RANK;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAMPLING_INTERVAL_DIAGNOSTICS_TYPE__DATA_TYPE = BASE_DATA_VARIABLE_TYPE__DATA_TYPE;

	/**
	 * The number of structural features of the '<em>Sampling Interval Diagnostics Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAMPLING_INTERVAL_DIAGNOSTICS_TYPE_FEATURE_COUNT = BASE_DATA_VARIABLE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Sampling Interval Diagnostics Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAMPLING_INTERVAL_DIAGNOSTICS_TYPE_OPERATION_COUNT = BASE_DATA_VARIABLE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.SubscriptionDiagnosticsArrayTypeImpl <em>Subscription Diagnostics Array Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.SubscriptionDiagnosticsArrayTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getSubscriptionDiagnosticsArrayType()
	 * @generated
	 */
	int SUBSCRIPTION_DIAGNOSTICS_ARRAY_TYPE = 64;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIPTION_DIAGNOSTICS_ARRAY_TYPE__BASE_CLASS = BASE_DATA_VARIABLE_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIPTION_DIAGNOSTICS_ARRAY_TYPE__BASE_PROPERTY = BASE_DATA_VARIABLE_TYPE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Value Rank</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIPTION_DIAGNOSTICS_ARRAY_TYPE__VALUE_RANK = BASE_DATA_VARIABLE_TYPE__VALUE_RANK;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIPTION_DIAGNOSTICS_ARRAY_TYPE__DATA_TYPE = BASE_DATA_VARIABLE_TYPE__DATA_TYPE;

	/**
	 * The number of structural features of the '<em>Subscription Diagnostics Array Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIPTION_DIAGNOSTICS_ARRAY_TYPE_FEATURE_COUNT = BASE_DATA_VARIABLE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Subscription Diagnostics Array Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIPTION_DIAGNOSTICS_ARRAY_TYPE_OPERATION_COUNT = BASE_DATA_VARIABLE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.SubscriptionDiagnosticsTypeImpl <em>Subscription Diagnostics Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.SubscriptionDiagnosticsTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getSubscriptionDiagnosticsType()
	 * @generated
	 */
	int SUBSCRIPTION_DIAGNOSTICS_TYPE = 65;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIPTION_DIAGNOSTICS_TYPE__BASE_CLASS = BASE_DATA_VARIABLE_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIPTION_DIAGNOSTICS_TYPE__BASE_PROPERTY = BASE_DATA_VARIABLE_TYPE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Value Rank</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIPTION_DIAGNOSTICS_TYPE__VALUE_RANK = BASE_DATA_VARIABLE_TYPE__VALUE_RANK;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIPTION_DIAGNOSTICS_TYPE__DATA_TYPE = BASE_DATA_VARIABLE_TYPE__DATA_TYPE;

	/**
	 * The number of structural features of the '<em>Subscription Diagnostics Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIPTION_DIAGNOSTICS_TYPE_FEATURE_COUNT = BASE_DATA_VARIABLE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Subscription Diagnostics Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIPTION_DIAGNOSTICS_TYPE_OPERATION_COUNT = BASE_DATA_VARIABLE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.SessionDiagnosticsArrayTypeImpl <em>Session Diagnostics Array Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.SessionDiagnosticsArrayTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getSessionDiagnosticsArrayType()
	 * @generated
	 */
	int SESSION_DIAGNOSTICS_ARRAY_TYPE = 66;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_DIAGNOSTICS_ARRAY_TYPE__BASE_CLASS = BASE_DATA_VARIABLE_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_DIAGNOSTICS_ARRAY_TYPE__BASE_PROPERTY = BASE_DATA_VARIABLE_TYPE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Value Rank</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_DIAGNOSTICS_ARRAY_TYPE__VALUE_RANK = BASE_DATA_VARIABLE_TYPE__VALUE_RANK;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_DIAGNOSTICS_ARRAY_TYPE__DATA_TYPE = BASE_DATA_VARIABLE_TYPE__DATA_TYPE;

	/**
	 * The number of structural features of the '<em>Session Diagnostics Array Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_DIAGNOSTICS_ARRAY_TYPE_FEATURE_COUNT = BASE_DATA_VARIABLE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Session Diagnostics Array Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_DIAGNOSTICS_ARRAY_TYPE_OPERATION_COUNT = BASE_DATA_VARIABLE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.SessionDiagnosticsVariableTypeImpl <em>Session Diagnostics Variable Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.SessionDiagnosticsVariableTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getSessionDiagnosticsVariableType()
	 * @generated
	 */
	int SESSION_DIAGNOSTICS_VARIABLE_TYPE = 67;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_DIAGNOSTICS_VARIABLE_TYPE__BASE_CLASS = BASE_DATA_VARIABLE_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_DIAGNOSTICS_VARIABLE_TYPE__BASE_PROPERTY = BASE_DATA_VARIABLE_TYPE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Value Rank</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_DIAGNOSTICS_VARIABLE_TYPE__VALUE_RANK = BASE_DATA_VARIABLE_TYPE__VALUE_RANK;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_DIAGNOSTICS_VARIABLE_TYPE__DATA_TYPE = BASE_DATA_VARIABLE_TYPE__DATA_TYPE;

	/**
	 * The number of structural features of the '<em>Session Diagnostics Variable Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_DIAGNOSTICS_VARIABLE_TYPE_FEATURE_COUNT = BASE_DATA_VARIABLE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Session Diagnostics Variable Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_DIAGNOSTICS_VARIABLE_TYPE_OPERATION_COUNT = BASE_DATA_VARIABLE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.SessionSecurityDiagnosticsArrayTypeImpl <em>Session Security Diagnostics Array Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.SessionSecurityDiagnosticsArrayTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getSessionSecurityDiagnosticsArrayType()
	 * @generated
	 */
	int SESSION_SECURITY_DIAGNOSTICS_ARRAY_TYPE = 68;

	/**
	 * The number of structural features of the '<em>Session Security Diagnostics Array Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_SECURITY_DIAGNOSTICS_ARRAY_TYPE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Session Security Diagnostics Array Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_SECURITY_DIAGNOSTICS_ARRAY_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.SessionSecurityDiagnosticsTypeImpl <em>Session Security Diagnostics Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.SessionSecurityDiagnosticsTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getSessionSecurityDiagnosticsType()
	 * @generated
	 */
	int SESSION_SECURITY_DIAGNOSTICS_TYPE = 69;

	/**
	 * The number of structural features of the '<em>Session Security Diagnostics Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_SECURITY_DIAGNOSTICS_TYPE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Session Security Diagnostics Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_SECURITY_DIAGNOSTICS_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.OptionSetTypeImpl <em>Option Set Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OptionSetTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getOptionSetType()
	 * @generated
	 */
	int OPTION_SET_TYPE = 70;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTION_SET_TYPE__BASE_CLASS = BASE_DATA_VARIABLE_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTION_SET_TYPE__BASE_PROPERTY = BASE_DATA_VARIABLE_TYPE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Value Rank</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTION_SET_TYPE__VALUE_RANK = BASE_DATA_VARIABLE_TYPE__VALUE_RANK;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTION_SET_TYPE__DATA_TYPE = BASE_DATA_VARIABLE_TYPE__DATA_TYPE;

	/**
	 * The number of structural features of the '<em>Option Set Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTION_SET_TYPE_FEATURE_COUNT = BASE_DATA_VARIABLE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Option Set Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTION_SET_TYPE_OPERATION_COUNT = BASE_DATA_VARIABLE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerDiagnosticsSummaryTypeImpl <em>Server Diagnostics Summary Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerDiagnosticsSummaryTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getServerDiagnosticsSummaryType()
	 * @generated
	 */
	int SERVER_DIAGNOSTICS_SUMMARY_TYPE = 71;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_DIAGNOSTICS_SUMMARY_TYPE__BASE_CLASS = BASE_DATA_VARIABLE_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_DIAGNOSTICS_SUMMARY_TYPE__BASE_PROPERTY = BASE_DATA_VARIABLE_TYPE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Value Rank</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_DIAGNOSTICS_SUMMARY_TYPE__VALUE_RANK = BASE_DATA_VARIABLE_TYPE__VALUE_RANK;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_DIAGNOSTICS_SUMMARY_TYPE__DATA_TYPE = BASE_DATA_VARIABLE_TYPE__DATA_TYPE;

	/**
	 * The number of structural features of the '<em>Server Diagnostics Summary Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_DIAGNOSTICS_SUMMARY_TYPE_FEATURE_COUNT = BASE_DATA_VARIABLE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Server Diagnostics Summary Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_DIAGNOSTICS_SUMMARY_TYPE_OPERATION_COUNT = BASE_DATA_VARIABLE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.BuildInfoTypeImpl <em>Build Info Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.BuildInfoTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getBuildInfoType()
	 * @generated
	 */
	int BUILD_INFO_TYPE = 72;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILD_INFO_TYPE__BASE_CLASS = BASE_DATA_VARIABLE_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILD_INFO_TYPE__BASE_PROPERTY = BASE_DATA_VARIABLE_TYPE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Value Rank</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILD_INFO_TYPE__VALUE_RANK = BASE_DATA_VARIABLE_TYPE__VALUE_RANK;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILD_INFO_TYPE__DATA_TYPE = BASE_DATA_VARIABLE_TYPE__DATA_TYPE;

	/**
	 * The number of structural features of the '<em>Build Info Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILD_INFO_TYPE_FEATURE_COUNT = BASE_DATA_VARIABLE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Build Info Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILD_INFO_TYPE_OPERATION_COUNT = BASE_DATA_VARIABLE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerStatusTypeImpl <em>Server Status Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerStatusTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getServerStatusType()
	 * @generated
	 */
	int SERVER_STATUS_TYPE = 73;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_STATUS_TYPE__BASE_CLASS = BASE_DATA_VARIABLE_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_STATUS_TYPE__BASE_PROPERTY = BASE_DATA_VARIABLE_TYPE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Value Rank</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_STATUS_TYPE__VALUE_RANK = BASE_DATA_VARIABLE_TYPE__VALUE_RANK;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_STATUS_TYPE__DATA_TYPE = BASE_DATA_VARIABLE_TYPE__DATA_TYPE;

	/**
	 * The number of structural features of the '<em>Server Status Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_STATUS_TYPE_FEATURE_COUNT = BASE_DATA_VARIABLE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Server Status Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_STATUS_TYPE_OPERATION_COUNT = BASE_DATA_VARIABLE_TYPE_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.DataTypeSystemType <em>Data Type System Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Type System Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.DataTypeSystemType
	 * @generated
	 */
	EClass getDataTypeSystemType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ModellingRuleType <em>Modelling Rule Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Modelling Rule Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ModellingRuleType
	 * @generated
	 */
	EClass getModellingRuleType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ServerType <em>Server Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Server Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ServerType
	 * @generated
	 */
	EClass getServerType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.FolderType <em>Folder Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Folder Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.FolderType
	 * @generated
	 */
	EClass getFolderType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.DataTypeEncodingType <em>Data Type Encoding Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Type Encoding Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.DataTypeEncodingType
	 * @generated
	 */
	EClass getDataTypeEncodingType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ServerCapabilitiesType <em>Server Capabilities Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Server Capabilities Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ServerCapabilitiesType
	 * @generated
	 */
	EClass getServerCapabilitiesType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ServerDiagnosticsType <em>Server Diagnostics Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Server Diagnostics Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ServerDiagnosticsType
	 * @generated
	 */
	EClass getServerDiagnosticsType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.SessionsDiagnosticsSummaryType <em>Sessions Diagnostics Summary Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sessions Diagnostics Summary Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.SessionsDiagnosticsSummaryType
	 * @generated
	 */
	EClass getSessionsDiagnosticsSummaryType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.SessionDiagnosticsObjectType <em>Session Diagnostics Object Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Session Diagnostics Object Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.SessionDiagnosticsObjectType
	 * @generated
	 */
	EClass getSessionDiagnosticsObjectType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.VendorServerInfoType <em>Vendor Server Info Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vendor Server Info Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.VendorServerInfoType
	 * @generated
	 */
	EClass getVendorServerInfoType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ServerRedundancyType <em>Server Redundancy Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Server Redundancy Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ServerRedundancyType
	 * @generated
	 */
	EClass getServerRedundancyType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.FileType <em>File Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>File Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.FileType
	 * @generated
	 */
	EClass getFileType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.NamespacesType <em>Namespaces Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Namespaces Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.NamespacesType
	 * @generated
	 */
	EClass getNamespacesType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseEventType <em>Base Event Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Event Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BaseEventType
	 * @generated
	 */
	EClass getBaseEventType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.AggregateFunctionType <em>Aggregate Function Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aggregate Function Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.AggregateFunctionType
	 * @generated
	 */
	EClass getAggregateFunctionType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.StateMachineType <em>State Machine Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State Machine Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.StateMachineType
	 * @generated
	 */
	EClass getStateMachineType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.StateType <em>State Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.StateType
	 * @generated
	 */
	EClass getStateType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.NamespaceMetadataType <em>Namespace Metadata Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Namespace Metadata Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.NamespaceMetadataType
	 * @generated
	 */
	EClass getNamespaceMetadataType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.WriterGroupMessageType <em>Writer Group Message Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Writer Group Message Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.WriterGroupMessageType
	 * @generated
	 */
	EClass getWriterGroupMessageType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.TransitionType <em>Transition Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.TransitionType
	 * @generated
	 */
	EClass getTransitionType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.TemporaryFileTransferType <em>Temporary File Transfer Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Temporary File Transfer Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.TemporaryFileTransferType
	 * @generated
	 */
	EClass getTemporaryFileTransferType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.RoleSetType <em>Role Set Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role Set Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.RoleSetType
	 * @generated
	 */
	EClass getRoleSetType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.RoleType <em>Role Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.RoleType
	 * @generated
	 */
	EClass getRoleType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseInterfaceType <em>Base Interface Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Interface Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BaseInterfaceType
	 * @generated
	 */
	EClass getBaseInterfaceType();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseInterfaceType#getBase_Interface <em>Base Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Interface</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BaseInterfaceType#getBase_Interface()
	 * @see #getBaseInterfaceType()
	 * @generated
	 */
	EReference getBaseInterfaceType_Base_Interface();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.DictionaryEntryType <em>Dictionary Entry Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dictionary Entry Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.DictionaryEntryType
	 * @generated
	 */
	EClass getDictionaryEntryType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OrderedListType <em>Ordered List Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ordered List Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OrderedListType
	 * @generated
	 */
	EClass getOrderedListType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseConditionClassType <em>Base Condition Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Condition Class Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BaseConditionClassType
	 * @generated
	 */
	EClass getBaseConditionClassType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.AlarmMetricsType <em>Alarm Metrics Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alarm Metrics Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.AlarmMetricsType
	 * @generated
	 */
	EClass getAlarmMetricsType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.HistoricalDataConfigurationType <em>Historical Data Configuration Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Historical Data Configuration Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.HistoricalDataConfigurationType
	 * @generated
	 */
	EClass getHistoricalDataConfigurationType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.HistoryServerCapabilitiesType <em>History Server Capabilities Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>History Server Capabilities Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.HistoryServerCapabilitiesType
	 * @generated
	 */
	EClass getHistoryServerCapabilitiesType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.CertificateGroupType <em>Certificate Group Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Certificate Group Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.CertificateGroupType
	 * @generated
	 */
	EClass getCertificateGroupType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.CertificateType <em>Certificate Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Certificate Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.CertificateType
	 * @generated
	 */
	EClass getCertificateType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ServerConfigurationType <em>Server Configuration Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Server Configuration Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ServerConfigurationType
	 * @generated
	 */
	EClass getServerConfigurationType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.KeyCredentialConfigurationType <em>Key Credential Configuration Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Key Credential Configuration Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.KeyCredentialConfigurationType
	 * @generated
	 */
	EClass getKeyCredentialConfigurationType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.AuthorizationServiceConfigurationType <em>Authorization Service Configuration Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Authorization Service Configuration Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.AuthorizationServiceConfigurationType
	 * @generated
	 */
	EClass getAuthorizationServiceConfigurationType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.AggregateConfigurationType <em>Aggregate Configuration Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aggregate Configuration Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.AggregateConfigurationType
	 * @generated
	 */
	EClass getAggregateConfigurationType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.PubSubKeyServiceType <em>Pub Sub Key Service Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pub Sub Key Service Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.PubSubKeyServiceType
	 * @generated
	 */
	EClass getPubSubKeyServiceType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.SecurityGroupType <em>Security Group Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Security Group Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.SecurityGroupType
	 * @generated
	 */
	EClass getSecurityGroupType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.PublishedDataSetType <em>Published Data Set Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Published Data Set Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.PublishedDataSetType
	 * @generated
	 */
	EClass getPublishedDataSetType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ExtensionFieldsType <em>Extension Fields Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Extension Fields Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ExtensionFieldsType
	 * @generated
	 */
	EClass getExtensionFieldsType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.PubSubConnectionType <em>Pub Sub Connection Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pub Sub Connection Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.PubSubConnectionType
	 * @generated
	 */
	EClass getPubSubConnectionType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ConnectionTransportType <em>Connection Transport Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connection Transport Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ConnectionTransportType
	 * @generated
	 */
	EClass getConnectionTransportType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.PubSubGroupType <em>Pub Sub Group Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pub Sub Group Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.PubSubGroupType
	 * @generated
	 */
	EClass getPubSubGroupType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.WriterGroupTransportType <em>Writer Group Transport Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Writer Group Transport Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.WriterGroupTransportType
	 * @generated
	 */
	EClass getWriterGroupTransportType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ReaderGroupTransportType <em>Reader Group Transport Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reader Group Transport Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ReaderGroupTransportType
	 * @generated
	 */
	EClass getReaderGroupTransportType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ReaderGroupMessageType <em>Reader Group Message Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reader Group Message Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ReaderGroupMessageType
	 * @generated
	 */
	EClass getReaderGroupMessageType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.DataSetWriterType <em>Data Set Writer Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Set Writer Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.DataSetWriterType
	 * @generated
	 */
	EClass getDataSetWriterType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.DataSetWriterTransportType <em>Data Set Writer Transport Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Set Writer Transport Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.DataSetWriterTransportType
	 * @generated
	 */
	EClass getDataSetWriterTransportType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.DataSetWriterMessageType <em>Data Set Writer Message Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Set Writer Message Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.DataSetWriterMessageType
	 * @generated
	 */
	EClass getDataSetWriterMessageType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.DataSetReaderType <em>Data Set Reader Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Set Reader Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.DataSetReaderType
	 * @generated
	 */
	EClass getDataSetReaderType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.DataSetReaderTransportType <em>Data Set Reader Transport Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Set Reader Transport Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.DataSetReaderTransportType
	 * @generated
	 */
	EClass getDataSetReaderTransportType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.DataSetReaderMessageType <em>Data Set Reader Message Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Set Reader Message Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.DataSetReaderMessageType
	 * @generated
	 */
	EClass getDataSetReaderMessageType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.SubscribedDataSetType <em>Subscribed Data Set Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Subscribed Data Set Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.SubscribedDataSetType
	 * @generated
	 */
	EClass getSubscribedDataSetType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.PubSubStatusType <em>Pub Sub Status Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pub Sub Status Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.PubSubStatusType
	 * @generated
	 */
	EClass getPubSubStatusType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.PubSubDiagnosticsType <em>Pub Sub Diagnostics Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pub Sub Diagnostics Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.PubSubDiagnosticsType
	 * @generated
	 */
	EClass getPubSubDiagnosticsType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.NetworkAddressType <em>Network Address Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Network Address Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.NetworkAddressType
	 * @generated
	 */
	EClass getNetworkAddressType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.AliasNameType <em>Alias Name Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alias Name Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.AliasNameType
	 * @generated
	 */
	EClass getAliasNameType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseVariableType <em>Base Variable Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Variable Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BaseVariableType
	 * @generated
	 */
	EClass getBaseVariableType();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseVariableType#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BaseVariableType#getBase_Class()
	 * @see #getBaseVariableType()
	 * @generated
	 */
	EReference getBaseVariableType_Base_Class();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseVariableType#getBase_Property <em>Base Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Property</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BaseVariableType#getBase_Property()
	 * @see #getBaseVariableType()
	 * @generated
	 */
	EReference getBaseVariableType_Base_Property();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseVariableType#getValueRank <em>Value Rank</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Rank</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BaseVariableType#getValueRank()
	 * @see #getBaseVariableType()
	 * @generated
	 */
	EAttribute getBaseVariableType_ValueRank();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseVariableType#getDataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Data Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BaseVariableType#getDataType()
	 * @see #getBaseVariableType()
	 * @generated
	 */
	EAttribute getBaseVariableType_DataType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseDataVariableType <em>Base Data Variable Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Data Variable Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BaseDataVariableType
	 * @generated
	 */
	EClass getBaseDataVariableType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.PropertyType <em>Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.PropertyType
	 * @generated
	 */
	EClass getPropertyType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ServerVendorCapabilityType <em>Server Vendor Capability Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Server Vendor Capability Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ServerVendorCapabilityType
	 * @generated
	 */
	EClass getServerVendorCapabilityType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.SamplingIntervalDiagnosticsArrayType <em>Sampling Interval Diagnostics Array Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sampling Interval Diagnostics Array Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.SamplingIntervalDiagnosticsArrayType
	 * @generated
	 */
	EClass getSamplingIntervalDiagnosticsArrayType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.SamplingIntervalDiagnosticsType <em>Sampling Interval Diagnostics Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sampling Interval Diagnostics Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.SamplingIntervalDiagnosticsType
	 * @generated
	 */
	EClass getSamplingIntervalDiagnosticsType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.SubscriptionDiagnosticsArrayType <em>Subscription Diagnostics Array Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Subscription Diagnostics Array Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.SubscriptionDiagnosticsArrayType
	 * @generated
	 */
	EClass getSubscriptionDiagnosticsArrayType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.SubscriptionDiagnosticsType <em>Subscription Diagnostics Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Subscription Diagnostics Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.SubscriptionDiagnosticsType
	 * @generated
	 */
	EClass getSubscriptionDiagnosticsType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.SessionDiagnosticsArrayType <em>Session Diagnostics Array Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Session Diagnostics Array Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.SessionDiagnosticsArrayType
	 * @generated
	 */
	EClass getSessionDiagnosticsArrayType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.SessionDiagnosticsVariableType <em>Session Diagnostics Variable Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Session Diagnostics Variable Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.SessionDiagnosticsVariableType
	 * @generated
	 */
	EClass getSessionDiagnosticsVariableType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.SessionSecurityDiagnosticsArrayType <em>Session Security Diagnostics Array Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Session Security Diagnostics Array Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.SessionSecurityDiagnosticsArrayType
	 * @generated
	 */
	EClass getSessionSecurityDiagnosticsArrayType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.SessionSecurityDiagnosticsType <em>Session Security Diagnostics Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Session Security Diagnostics Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.SessionSecurityDiagnosticsType
	 * @generated
	 */
	EClass getSessionSecurityDiagnosticsType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OptionSetType <em>Option Set Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Option Set Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OptionSetType
	 * @generated
	 */
	EClass getOptionSetType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ServerDiagnosticsSummaryType <em>Server Diagnostics Summary Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Server Diagnostics Summary Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ServerDiagnosticsSummaryType
	 * @generated
	 */
	EClass getServerDiagnosticsSummaryType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.BuildInfoType <em>Build Info Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Build Info Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BuildInfoType
	 * @generated
	 */
	EClass getBuildInfoType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ServerStatusType <em>Server Status Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Server Status Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ServerStatusType
	 * @generated
	 */
	EClass getServerStatusType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType <em>Base Object Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Object Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType
	 * @generated
	 */
	EClass getBaseObjectType();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType#getBase_Class()
	 * @see #getBaseObjectType()
	 * @generated
	 */
	EReference getBaseObjectType_Base_Class();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType#getNodeId <em>Node Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Node Id</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType#getNodeId()
	 * @see #getBaseObjectType()
	 * @generated
	 */
	EAttribute getBaseObjectType_NodeId();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType#getNamespaceUri <em>Namespace Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Namespace Uri</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType#getNamespaceUri()
	 * @see #getBaseObjectType()
	 * @generated
	 */
	EAttribute getBaseObjectType_NamespaceUri();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType#getBrowseName <em>Browse Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Browse Name</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType#getBrowseName()
	 * @see #getBaseObjectType()
	 * @generated
	 */
	EAttribute getBaseObjectType_BrowseName();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType#getNodeClass <em>Node Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Node Class</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType#getNodeClass()
	 * @see #getBaseObjectType()
	 * @generated
	 */
	EAttribute getBaseObjectType_NodeClass();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OPCUAProfileFactory getOPCUAProfileFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.DataTypeSystemTypeImpl <em>Data Type System Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.DataTypeSystemTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getDataTypeSystemType()
		 * @generated
		 */
		EClass DATA_TYPE_SYSTEM_TYPE = eINSTANCE.getDataTypeSystemType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ModellingRuleTypeImpl <em>Modelling Rule Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ModellingRuleTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getModellingRuleType()
		 * @generated
		 */
		EClass MODELLING_RULE_TYPE = eINSTANCE.getModellingRuleType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerTypeImpl <em>Server Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getServerType()
		 * @generated
		 */
		EClass SERVER_TYPE = eINSTANCE.getServerType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.FolderTypeImpl <em>Folder Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.FolderTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getFolderType()
		 * @generated
		 */
		EClass FOLDER_TYPE = eINSTANCE.getFolderType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.DataTypeEncodingTypeImpl <em>Data Type Encoding Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.DataTypeEncodingTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getDataTypeEncodingType()
		 * @generated
		 */
		EClass DATA_TYPE_ENCODING_TYPE = eINSTANCE.getDataTypeEncodingType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerCapabilitiesTypeImpl <em>Server Capabilities Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerCapabilitiesTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getServerCapabilitiesType()
		 * @generated
		 */
		EClass SERVER_CAPABILITIES_TYPE = eINSTANCE.getServerCapabilitiesType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerDiagnosticsTypeImpl <em>Server Diagnostics Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerDiagnosticsTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getServerDiagnosticsType()
		 * @generated
		 */
		EClass SERVER_DIAGNOSTICS_TYPE = eINSTANCE.getServerDiagnosticsType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.SessionsDiagnosticsSummaryTypeImpl <em>Sessions Diagnostics Summary Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.SessionsDiagnosticsSummaryTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getSessionsDiagnosticsSummaryType()
		 * @generated
		 */
		EClass SESSIONS_DIAGNOSTICS_SUMMARY_TYPE = eINSTANCE.getSessionsDiagnosticsSummaryType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.SessionDiagnosticsObjectTypeImpl <em>Session Diagnostics Object Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.SessionDiagnosticsObjectTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getSessionDiagnosticsObjectType()
		 * @generated
		 */
		EClass SESSION_DIAGNOSTICS_OBJECT_TYPE = eINSTANCE.getSessionDiagnosticsObjectType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.VendorServerInfoTypeImpl <em>Vendor Server Info Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.VendorServerInfoTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getVendorServerInfoType()
		 * @generated
		 */
		EClass VENDOR_SERVER_INFO_TYPE = eINSTANCE.getVendorServerInfoType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerRedundancyTypeImpl <em>Server Redundancy Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerRedundancyTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getServerRedundancyType()
		 * @generated
		 */
		EClass SERVER_REDUNDANCY_TYPE = eINSTANCE.getServerRedundancyType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.FileTypeImpl <em>File Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.FileTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getFileType()
		 * @generated
		 */
		EClass FILE_TYPE = eINSTANCE.getFileType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.NamespacesTypeImpl <em>Namespaces Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.NamespacesTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getNamespacesType()
		 * @generated
		 */
		EClass NAMESPACES_TYPE = eINSTANCE.getNamespacesType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseEventTypeImpl <em>Base Event Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseEventTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getBaseEventType()
		 * @generated
		 */
		EClass BASE_EVENT_TYPE = eINSTANCE.getBaseEventType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.AggregateFunctionTypeImpl <em>Aggregate Function Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.AggregateFunctionTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getAggregateFunctionType()
		 * @generated
		 */
		EClass AGGREGATE_FUNCTION_TYPE = eINSTANCE.getAggregateFunctionType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.StateMachineTypeImpl <em>State Machine Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.StateMachineTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getStateMachineType()
		 * @generated
		 */
		EClass STATE_MACHINE_TYPE = eINSTANCE.getStateMachineType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.StateTypeImpl <em>State Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.StateTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getStateType()
		 * @generated
		 */
		EClass STATE_TYPE = eINSTANCE.getStateType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.NamespaceMetadataTypeImpl <em>Namespace Metadata Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.NamespaceMetadataTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getNamespaceMetadataType()
		 * @generated
		 */
		EClass NAMESPACE_METADATA_TYPE = eINSTANCE.getNamespaceMetadataType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.WriterGroupMessageTypeImpl <em>Writer Group Message Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.WriterGroupMessageTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getWriterGroupMessageType()
		 * @generated
		 */
		EClass WRITER_GROUP_MESSAGE_TYPE = eINSTANCE.getWriterGroupMessageType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.TransitionTypeImpl <em>Transition Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.TransitionTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getTransitionType()
		 * @generated
		 */
		EClass TRANSITION_TYPE = eINSTANCE.getTransitionType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.TemporaryFileTransferTypeImpl <em>Temporary File Transfer Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.TemporaryFileTransferTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getTemporaryFileTransferType()
		 * @generated
		 */
		EClass TEMPORARY_FILE_TRANSFER_TYPE = eINSTANCE.getTemporaryFileTransferType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.RoleSetTypeImpl <em>Role Set Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.RoleSetTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getRoleSetType()
		 * @generated
		 */
		EClass ROLE_SET_TYPE = eINSTANCE.getRoleSetType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.RoleTypeImpl <em>Role Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.RoleTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getRoleType()
		 * @generated
		 */
		EClass ROLE_TYPE = eINSTANCE.getRoleType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseInterfaceTypeImpl <em>Base Interface Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseInterfaceTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getBaseInterfaceType()
		 * @generated
		 */
		EClass BASE_INTERFACE_TYPE = eINSTANCE.getBaseInterfaceType();

		/**
		 * The meta object literal for the '<em><b>Base Interface</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_INTERFACE_TYPE__BASE_INTERFACE = eINSTANCE.getBaseInterfaceType_Base_Interface();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.DictionaryEntryTypeImpl <em>Dictionary Entry Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.DictionaryEntryTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getDictionaryEntryType()
		 * @generated
		 */
		EClass DICTIONARY_ENTRY_TYPE = eINSTANCE.getDictionaryEntryType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.OrderedListTypeImpl <em>Ordered List Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OrderedListTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getOrderedListType()
		 * @generated
		 */
		EClass ORDERED_LIST_TYPE = eINSTANCE.getOrderedListType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseConditionClassTypeImpl <em>Base Condition Class Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseConditionClassTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getBaseConditionClassType()
		 * @generated
		 */
		EClass BASE_CONDITION_CLASS_TYPE = eINSTANCE.getBaseConditionClassType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.AlarmMetricsTypeImpl <em>Alarm Metrics Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.AlarmMetricsTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getAlarmMetricsType()
		 * @generated
		 */
		EClass ALARM_METRICS_TYPE = eINSTANCE.getAlarmMetricsType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.HistoricalDataConfigurationTypeImpl <em>Historical Data Configuration Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.HistoricalDataConfigurationTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getHistoricalDataConfigurationType()
		 * @generated
		 */
		EClass HISTORICAL_DATA_CONFIGURATION_TYPE = eINSTANCE.getHistoricalDataConfigurationType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.HistoryServerCapabilitiesTypeImpl <em>History Server Capabilities Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.HistoryServerCapabilitiesTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getHistoryServerCapabilitiesType()
		 * @generated
		 */
		EClass HISTORY_SERVER_CAPABILITIES_TYPE = eINSTANCE.getHistoryServerCapabilitiesType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.CertificateGroupTypeImpl <em>Certificate Group Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.CertificateGroupTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getCertificateGroupType()
		 * @generated
		 */
		EClass CERTIFICATE_GROUP_TYPE = eINSTANCE.getCertificateGroupType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.CertificateTypeImpl <em>Certificate Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.CertificateTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getCertificateType()
		 * @generated
		 */
		EClass CERTIFICATE_TYPE = eINSTANCE.getCertificateType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerConfigurationTypeImpl <em>Server Configuration Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerConfigurationTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getServerConfigurationType()
		 * @generated
		 */
		EClass SERVER_CONFIGURATION_TYPE = eINSTANCE.getServerConfigurationType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.KeyCredentialConfigurationTypeImpl <em>Key Credential Configuration Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.KeyCredentialConfigurationTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getKeyCredentialConfigurationType()
		 * @generated
		 */
		EClass KEY_CREDENTIAL_CONFIGURATION_TYPE = eINSTANCE.getKeyCredentialConfigurationType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.AuthorizationServiceConfigurationTypeImpl <em>Authorization Service Configuration Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.AuthorizationServiceConfigurationTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getAuthorizationServiceConfigurationType()
		 * @generated
		 */
		EClass AUTHORIZATION_SERVICE_CONFIGURATION_TYPE = eINSTANCE.getAuthorizationServiceConfigurationType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.AggregateConfigurationTypeImpl <em>Aggregate Configuration Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.AggregateConfigurationTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getAggregateConfigurationType()
		 * @generated
		 */
		EClass AGGREGATE_CONFIGURATION_TYPE = eINSTANCE.getAggregateConfigurationType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.PubSubKeyServiceTypeImpl <em>Pub Sub Key Service Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.PubSubKeyServiceTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getPubSubKeyServiceType()
		 * @generated
		 */
		EClass PUB_SUB_KEY_SERVICE_TYPE = eINSTANCE.getPubSubKeyServiceType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.SecurityGroupTypeImpl <em>Security Group Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.SecurityGroupTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getSecurityGroupType()
		 * @generated
		 */
		EClass SECURITY_GROUP_TYPE = eINSTANCE.getSecurityGroupType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.PublishedDataSetTypeImpl <em>Published Data Set Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.PublishedDataSetTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getPublishedDataSetType()
		 * @generated
		 */
		EClass PUBLISHED_DATA_SET_TYPE = eINSTANCE.getPublishedDataSetType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ExtensionFieldsTypeImpl <em>Extension Fields Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ExtensionFieldsTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getExtensionFieldsType()
		 * @generated
		 */
		EClass EXTENSION_FIELDS_TYPE = eINSTANCE.getExtensionFieldsType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.PubSubConnectionTypeImpl <em>Pub Sub Connection Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.PubSubConnectionTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getPubSubConnectionType()
		 * @generated
		 */
		EClass PUB_SUB_CONNECTION_TYPE = eINSTANCE.getPubSubConnectionType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ConnectionTransportTypeImpl <em>Connection Transport Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ConnectionTransportTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getConnectionTransportType()
		 * @generated
		 */
		EClass CONNECTION_TRANSPORT_TYPE = eINSTANCE.getConnectionTransportType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.PubSubGroupTypeImpl <em>Pub Sub Group Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.PubSubGroupTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getPubSubGroupType()
		 * @generated
		 */
		EClass PUB_SUB_GROUP_TYPE = eINSTANCE.getPubSubGroupType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.WriterGroupTransportTypeImpl <em>Writer Group Transport Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.WriterGroupTransportTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getWriterGroupTransportType()
		 * @generated
		 */
		EClass WRITER_GROUP_TRANSPORT_TYPE = eINSTANCE.getWriterGroupTransportType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ReaderGroupTransportTypeImpl <em>Reader Group Transport Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ReaderGroupTransportTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getReaderGroupTransportType()
		 * @generated
		 */
		EClass READER_GROUP_TRANSPORT_TYPE = eINSTANCE.getReaderGroupTransportType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ReaderGroupMessageTypeImpl <em>Reader Group Message Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ReaderGroupMessageTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getReaderGroupMessageType()
		 * @generated
		 */
		EClass READER_GROUP_MESSAGE_TYPE = eINSTANCE.getReaderGroupMessageType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.DataSetWriterTypeImpl <em>Data Set Writer Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.DataSetWriterTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getDataSetWriterType()
		 * @generated
		 */
		EClass DATA_SET_WRITER_TYPE = eINSTANCE.getDataSetWriterType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.DataSetWriterTransportTypeImpl <em>Data Set Writer Transport Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.DataSetWriterTransportTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getDataSetWriterTransportType()
		 * @generated
		 */
		EClass DATA_SET_WRITER_TRANSPORT_TYPE = eINSTANCE.getDataSetWriterTransportType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.DataSetWriterMessageTypeImpl <em>Data Set Writer Message Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.DataSetWriterMessageTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getDataSetWriterMessageType()
		 * @generated
		 */
		EClass DATA_SET_WRITER_MESSAGE_TYPE = eINSTANCE.getDataSetWriterMessageType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.DataSetReaderTypeImpl <em>Data Set Reader Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.DataSetReaderTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getDataSetReaderType()
		 * @generated
		 */
		EClass DATA_SET_READER_TYPE = eINSTANCE.getDataSetReaderType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.DataSetReaderTransportTypeImpl <em>Data Set Reader Transport Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.DataSetReaderTransportTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getDataSetReaderTransportType()
		 * @generated
		 */
		EClass DATA_SET_READER_TRANSPORT_TYPE = eINSTANCE.getDataSetReaderTransportType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.DataSetReaderMessageTypeImpl <em>Data Set Reader Message Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.DataSetReaderMessageTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getDataSetReaderMessageType()
		 * @generated
		 */
		EClass DATA_SET_READER_MESSAGE_TYPE = eINSTANCE.getDataSetReaderMessageType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.SubscribedDataSetTypeImpl <em>Subscribed Data Set Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.SubscribedDataSetTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getSubscribedDataSetType()
		 * @generated
		 */
		EClass SUBSCRIBED_DATA_SET_TYPE = eINSTANCE.getSubscribedDataSetType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.PubSubStatusTypeImpl <em>Pub Sub Status Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.PubSubStatusTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getPubSubStatusType()
		 * @generated
		 */
		EClass PUB_SUB_STATUS_TYPE = eINSTANCE.getPubSubStatusType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.PubSubDiagnosticsTypeImpl <em>Pub Sub Diagnostics Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.PubSubDiagnosticsTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getPubSubDiagnosticsType()
		 * @generated
		 */
		EClass PUB_SUB_DIAGNOSTICS_TYPE = eINSTANCE.getPubSubDiagnosticsType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.NetworkAddressTypeImpl <em>Network Address Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.NetworkAddressTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getNetworkAddressType()
		 * @generated
		 */
		EClass NETWORK_ADDRESS_TYPE = eINSTANCE.getNetworkAddressType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.AliasNameTypeImpl <em>Alias Name Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.AliasNameTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getAliasNameType()
		 * @generated
		 */
		EClass ALIAS_NAME_TYPE = eINSTANCE.getAliasNameType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseVariableTypeImpl <em>Base Variable Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseVariableTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getBaseVariableType()
		 * @generated
		 */
		EClass BASE_VARIABLE_TYPE = eINSTANCE.getBaseVariableType();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_VARIABLE_TYPE__BASE_CLASS = eINSTANCE.getBaseVariableType_Base_Class();

		/**
		 * The meta object literal for the '<em><b>Base Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_VARIABLE_TYPE__BASE_PROPERTY = eINSTANCE.getBaseVariableType_Base_Property();

		/**
		 * The meta object literal for the '<em><b>Value Rank</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_VARIABLE_TYPE__VALUE_RANK = eINSTANCE.getBaseVariableType_ValueRank();

		/**
		 * The meta object literal for the '<em><b>Data Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_VARIABLE_TYPE__DATA_TYPE = eINSTANCE.getBaseVariableType_DataType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseDataVariableTypeImpl <em>Base Data Variable Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseDataVariableTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getBaseDataVariableType()
		 * @generated
		 */
		EClass BASE_DATA_VARIABLE_TYPE = eINSTANCE.getBaseDataVariableType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.PropertyTypeImpl <em>Property Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.PropertyTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getPropertyType()
		 * @generated
		 */
		EClass PROPERTY_TYPE = eINSTANCE.getPropertyType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerVendorCapabilityTypeImpl <em>Server Vendor Capability Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerVendorCapabilityTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getServerVendorCapabilityType()
		 * @generated
		 */
		EClass SERVER_VENDOR_CAPABILITY_TYPE = eINSTANCE.getServerVendorCapabilityType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.SamplingIntervalDiagnosticsArrayTypeImpl <em>Sampling Interval Diagnostics Array Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.SamplingIntervalDiagnosticsArrayTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getSamplingIntervalDiagnosticsArrayType()
		 * @generated
		 */
		EClass SAMPLING_INTERVAL_DIAGNOSTICS_ARRAY_TYPE = eINSTANCE.getSamplingIntervalDiagnosticsArrayType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.SamplingIntervalDiagnosticsTypeImpl <em>Sampling Interval Diagnostics Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.SamplingIntervalDiagnosticsTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getSamplingIntervalDiagnosticsType()
		 * @generated
		 */
		EClass SAMPLING_INTERVAL_DIAGNOSTICS_TYPE = eINSTANCE.getSamplingIntervalDiagnosticsType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.SubscriptionDiagnosticsArrayTypeImpl <em>Subscription Diagnostics Array Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.SubscriptionDiagnosticsArrayTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getSubscriptionDiagnosticsArrayType()
		 * @generated
		 */
		EClass SUBSCRIPTION_DIAGNOSTICS_ARRAY_TYPE = eINSTANCE.getSubscriptionDiagnosticsArrayType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.SubscriptionDiagnosticsTypeImpl <em>Subscription Diagnostics Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.SubscriptionDiagnosticsTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getSubscriptionDiagnosticsType()
		 * @generated
		 */
		EClass SUBSCRIPTION_DIAGNOSTICS_TYPE = eINSTANCE.getSubscriptionDiagnosticsType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.SessionDiagnosticsArrayTypeImpl <em>Session Diagnostics Array Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.SessionDiagnosticsArrayTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getSessionDiagnosticsArrayType()
		 * @generated
		 */
		EClass SESSION_DIAGNOSTICS_ARRAY_TYPE = eINSTANCE.getSessionDiagnosticsArrayType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.SessionDiagnosticsVariableTypeImpl <em>Session Diagnostics Variable Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.SessionDiagnosticsVariableTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getSessionDiagnosticsVariableType()
		 * @generated
		 */
		EClass SESSION_DIAGNOSTICS_VARIABLE_TYPE = eINSTANCE.getSessionDiagnosticsVariableType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.SessionSecurityDiagnosticsArrayTypeImpl <em>Session Security Diagnostics Array Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.SessionSecurityDiagnosticsArrayTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getSessionSecurityDiagnosticsArrayType()
		 * @generated
		 */
		EClass SESSION_SECURITY_DIAGNOSTICS_ARRAY_TYPE = eINSTANCE.getSessionSecurityDiagnosticsArrayType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.SessionSecurityDiagnosticsTypeImpl <em>Session Security Diagnostics Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.SessionSecurityDiagnosticsTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getSessionSecurityDiagnosticsType()
		 * @generated
		 */
		EClass SESSION_SECURITY_DIAGNOSTICS_TYPE = eINSTANCE.getSessionSecurityDiagnosticsType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.OptionSetTypeImpl <em>Option Set Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OptionSetTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getOptionSetType()
		 * @generated
		 */
		EClass OPTION_SET_TYPE = eINSTANCE.getOptionSetType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerDiagnosticsSummaryTypeImpl <em>Server Diagnostics Summary Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerDiagnosticsSummaryTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getServerDiagnosticsSummaryType()
		 * @generated
		 */
		EClass SERVER_DIAGNOSTICS_SUMMARY_TYPE = eINSTANCE.getServerDiagnosticsSummaryType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.BuildInfoTypeImpl <em>Build Info Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.BuildInfoTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getBuildInfoType()
		 * @generated
		 */
		EClass BUILD_INFO_TYPE = eINSTANCE.getBuildInfoType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerStatusTypeImpl <em>Server Status Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.ServerStatusTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getServerStatusType()
		 * @generated
		 */
		EClass SERVER_STATUS_TYPE = eINSTANCE.getServerStatusType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseObjectTypeImpl <em>Base Object Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseObjectTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl#getBaseObjectType()
		 * @generated
		 */
		EClass BASE_OBJECT_TYPE = eINSTANCE.getBaseObjectType();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_OBJECT_TYPE__BASE_CLASS = eINSTANCE.getBaseObjectType_Base_Class();

		/**
		 * The meta object literal for the '<em><b>Node Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_OBJECT_TYPE__NODE_ID = eINSTANCE.getBaseObjectType_NodeId();

		/**
		 * The meta object literal for the '<em><b>Namespace Uri</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_OBJECT_TYPE__NAMESPACE_URI = eINSTANCE.getBaseObjectType_NamespaceUri();

		/**
		 * The meta object literal for the '<em><b>Browse Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_OBJECT_TYPE__BROWSE_NAME = eINSTANCE.getBaseObjectType_BrowseName();

		/**
		 * The meta object literal for the '<em><b>Node Class</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_OBJECT_TYPE__NODE_CLASS = eINSTANCE.getBaseObjectType_NodeClass();

	}

} //OPCUAProfilePackage
