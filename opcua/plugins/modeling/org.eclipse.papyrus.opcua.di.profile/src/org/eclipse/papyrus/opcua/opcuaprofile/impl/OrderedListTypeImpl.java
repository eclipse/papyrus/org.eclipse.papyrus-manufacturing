/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.opcuaprofile.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.opcua.opcuaprofile.OPCUAProfilePackage;
import org.eclipse.papyrus.opcua.opcuaprofile.OrderedListType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ordered List Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class OrderedListTypeImpl extends BaseObjectTypeImpl implements OrderedListType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OrderedListTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUAProfilePackage.Literals.ORDERED_LIST_TYPE;
	}

} //OrderedListTypeImpl
