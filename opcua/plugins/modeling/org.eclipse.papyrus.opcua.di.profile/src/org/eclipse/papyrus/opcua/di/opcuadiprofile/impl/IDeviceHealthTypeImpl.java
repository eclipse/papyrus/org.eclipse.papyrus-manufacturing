/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.di.opcuadiprofile.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.IDeviceHealthType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.OPC_UA_DI_Library.DeviceHealthEnumeration;

import org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseInterfaceTypeImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IDevice Health Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.IDeviceHealthTypeImpl#getDeviceHealth <em>Device Health</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class IDeviceHealthTypeImpl extends BaseInterfaceTypeImpl implements IDeviceHealthType {
	/**
	 * The default value of the '{@link #getDeviceHealth() <em>Device Health</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeviceHealth()
	 * @generated
	 * @ordered
	 */
	protected static final DeviceHealthEnumeration DEVICE_HEALTH_EDEFAULT = DeviceHealthEnumeration.NORMAL;

	/**
	 * The cached value of the '{@link #getDeviceHealth() <em>Device Health</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeviceHealth()
	 * @generated
	 * @ordered
	 */
	protected DeviceHealthEnumeration deviceHealth = DEVICE_HEALTH_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IDeviceHealthTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUADIProfilePackage.Literals.IDEVICE_HEALTH_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeviceHealthEnumeration getDeviceHealth() {
		return deviceHealth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeviceHealth(DeviceHealthEnumeration newDeviceHealth) {
		DeviceHealthEnumeration oldDeviceHealth = deviceHealth;
		deviceHealth = newDeviceHealth == null ? DEVICE_HEALTH_EDEFAULT : newDeviceHealth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.IDEVICE_HEALTH_TYPE__DEVICE_HEALTH, oldDeviceHealth, deviceHealth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPCUADIProfilePackage.IDEVICE_HEALTH_TYPE__DEVICE_HEALTH:
				return getDeviceHealth();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPCUADIProfilePackage.IDEVICE_HEALTH_TYPE__DEVICE_HEALTH:
				setDeviceHealth((DeviceHealthEnumeration)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPCUADIProfilePackage.IDEVICE_HEALTH_TYPE__DEVICE_HEALTH:
				setDeviceHealth(DEVICE_HEALTH_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPCUADIProfilePackage.IDEVICE_HEALTH_TYPE__DEVICE_HEALTH:
				return deviceHealth != DEVICE_HEALTH_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (DeviceHealth: ");
		result.append(deviceHealth);
		result.append(')');
		return result.toString();
	}

} //IDeviceHealthTypeImpl
