/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.opcuaprofile.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.eclipse.papyrus.opcua.opcuaprofile.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPCUAProfilePackage
 * @generated
 */
public class OPCUAProfileSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static OPCUAProfilePackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPCUAProfileSwitch() {
		if (modelPackage == null) {
			modelPackage = OPCUAProfilePackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case OPCUAProfilePackage.BASE_OBJECT_TYPE: {
				BaseObjectType baseObjectType = (BaseObjectType)theEObject;
				T result = caseBaseObjectType(baseObjectType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.DATA_TYPE_SYSTEM_TYPE: {
				DataTypeSystemType dataTypeSystemType = (DataTypeSystemType)theEObject;
				T result = caseDataTypeSystemType(dataTypeSystemType);
				if (result == null) result = caseBaseObjectType(dataTypeSystemType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.MODELLING_RULE_TYPE: {
				ModellingRuleType modellingRuleType = (ModellingRuleType)theEObject;
				T result = caseModellingRuleType(modellingRuleType);
				if (result == null) result = caseBaseObjectType(modellingRuleType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.SERVER_TYPE: {
				ServerType serverType = (ServerType)theEObject;
				T result = caseServerType(serverType);
				if (result == null) result = caseBaseObjectType(serverType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.FOLDER_TYPE: {
				FolderType folderType = (FolderType)theEObject;
				T result = caseFolderType(folderType);
				if (result == null) result = caseBaseObjectType(folderType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.DATA_TYPE_ENCODING_TYPE: {
				DataTypeEncodingType dataTypeEncodingType = (DataTypeEncodingType)theEObject;
				T result = caseDataTypeEncodingType(dataTypeEncodingType);
				if (result == null) result = caseBaseObjectType(dataTypeEncodingType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.SERVER_CAPABILITIES_TYPE: {
				ServerCapabilitiesType serverCapabilitiesType = (ServerCapabilitiesType)theEObject;
				T result = caseServerCapabilitiesType(serverCapabilitiesType);
				if (result == null) result = caseBaseObjectType(serverCapabilitiesType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.SERVER_DIAGNOSTICS_TYPE: {
				ServerDiagnosticsType serverDiagnosticsType = (ServerDiagnosticsType)theEObject;
				T result = caseServerDiagnosticsType(serverDiagnosticsType);
				if (result == null) result = caseBaseObjectType(serverDiagnosticsType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.SESSIONS_DIAGNOSTICS_SUMMARY_TYPE: {
				SessionsDiagnosticsSummaryType sessionsDiagnosticsSummaryType = (SessionsDiagnosticsSummaryType)theEObject;
				T result = caseSessionsDiagnosticsSummaryType(sessionsDiagnosticsSummaryType);
				if (result == null) result = caseBaseObjectType(sessionsDiagnosticsSummaryType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.SESSION_DIAGNOSTICS_OBJECT_TYPE: {
				SessionDiagnosticsObjectType sessionDiagnosticsObjectType = (SessionDiagnosticsObjectType)theEObject;
				T result = caseSessionDiagnosticsObjectType(sessionDiagnosticsObjectType);
				if (result == null) result = caseBaseObjectType(sessionDiagnosticsObjectType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.VENDOR_SERVER_INFO_TYPE: {
				VendorServerInfoType vendorServerInfoType = (VendorServerInfoType)theEObject;
				T result = caseVendorServerInfoType(vendorServerInfoType);
				if (result == null) result = caseBaseObjectType(vendorServerInfoType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.SERVER_REDUNDANCY_TYPE: {
				ServerRedundancyType serverRedundancyType = (ServerRedundancyType)theEObject;
				T result = caseServerRedundancyType(serverRedundancyType);
				if (result == null) result = caseBaseObjectType(serverRedundancyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.FILE_TYPE: {
				FileType fileType = (FileType)theEObject;
				T result = caseFileType(fileType);
				if (result == null) result = caseBaseObjectType(fileType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.NAMESPACES_TYPE: {
				NamespacesType namespacesType = (NamespacesType)theEObject;
				T result = caseNamespacesType(namespacesType);
				if (result == null) result = caseBaseObjectType(namespacesType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.BASE_EVENT_TYPE: {
				BaseEventType baseEventType = (BaseEventType)theEObject;
				T result = caseBaseEventType(baseEventType);
				if (result == null) result = caseBaseObjectType(baseEventType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.AGGREGATE_FUNCTION_TYPE: {
				AggregateFunctionType aggregateFunctionType = (AggregateFunctionType)theEObject;
				T result = caseAggregateFunctionType(aggregateFunctionType);
				if (result == null) result = caseBaseObjectType(aggregateFunctionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.STATE_MACHINE_TYPE: {
				StateMachineType stateMachineType = (StateMachineType)theEObject;
				T result = caseStateMachineType(stateMachineType);
				if (result == null) result = caseBaseObjectType(stateMachineType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.STATE_TYPE: {
				StateType stateType = (StateType)theEObject;
				T result = caseStateType(stateType);
				if (result == null) result = caseBaseObjectType(stateType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.NAMESPACE_METADATA_TYPE: {
				NamespaceMetadataType namespaceMetadataType = (NamespaceMetadataType)theEObject;
				T result = caseNamespaceMetadataType(namespaceMetadataType);
				if (result == null) result = caseBaseObjectType(namespaceMetadataType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.WRITER_GROUP_MESSAGE_TYPE: {
				WriterGroupMessageType writerGroupMessageType = (WriterGroupMessageType)theEObject;
				T result = caseWriterGroupMessageType(writerGroupMessageType);
				if (result == null) result = caseBaseObjectType(writerGroupMessageType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.TRANSITION_TYPE: {
				TransitionType transitionType = (TransitionType)theEObject;
				T result = caseTransitionType(transitionType);
				if (result == null) result = caseBaseObjectType(transitionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.TEMPORARY_FILE_TRANSFER_TYPE: {
				TemporaryFileTransferType temporaryFileTransferType = (TemporaryFileTransferType)theEObject;
				T result = caseTemporaryFileTransferType(temporaryFileTransferType);
				if (result == null) result = caseBaseObjectType(temporaryFileTransferType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.ROLE_SET_TYPE: {
				RoleSetType roleSetType = (RoleSetType)theEObject;
				T result = caseRoleSetType(roleSetType);
				if (result == null) result = caseBaseObjectType(roleSetType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.ROLE_TYPE: {
				RoleType roleType = (RoleType)theEObject;
				T result = caseRoleType(roleType);
				if (result == null) result = caseBaseObjectType(roleType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.BASE_INTERFACE_TYPE: {
				BaseInterfaceType baseInterfaceType = (BaseInterfaceType)theEObject;
				T result = caseBaseInterfaceType(baseInterfaceType);
				if (result == null) result = caseBaseObjectType(baseInterfaceType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.DICTIONARY_ENTRY_TYPE: {
				DictionaryEntryType dictionaryEntryType = (DictionaryEntryType)theEObject;
				T result = caseDictionaryEntryType(dictionaryEntryType);
				if (result == null) result = caseBaseObjectType(dictionaryEntryType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.ORDERED_LIST_TYPE: {
				OrderedListType orderedListType = (OrderedListType)theEObject;
				T result = caseOrderedListType(orderedListType);
				if (result == null) result = caseBaseObjectType(orderedListType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.BASE_CONDITION_CLASS_TYPE: {
				BaseConditionClassType baseConditionClassType = (BaseConditionClassType)theEObject;
				T result = caseBaseConditionClassType(baseConditionClassType);
				if (result == null) result = caseBaseObjectType(baseConditionClassType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.ALARM_METRICS_TYPE: {
				AlarmMetricsType alarmMetricsType = (AlarmMetricsType)theEObject;
				T result = caseAlarmMetricsType(alarmMetricsType);
				if (result == null) result = caseBaseObjectType(alarmMetricsType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.HISTORICAL_DATA_CONFIGURATION_TYPE: {
				HistoricalDataConfigurationType historicalDataConfigurationType = (HistoricalDataConfigurationType)theEObject;
				T result = caseHistoricalDataConfigurationType(historicalDataConfigurationType);
				if (result == null) result = caseBaseObjectType(historicalDataConfigurationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.HISTORY_SERVER_CAPABILITIES_TYPE: {
				HistoryServerCapabilitiesType historyServerCapabilitiesType = (HistoryServerCapabilitiesType)theEObject;
				T result = caseHistoryServerCapabilitiesType(historyServerCapabilitiesType);
				if (result == null) result = caseBaseObjectType(historyServerCapabilitiesType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.CERTIFICATE_GROUP_TYPE: {
				CertificateGroupType certificateGroupType = (CertificateGroupType)theEObject;
				T result = caseCertificateGroupType(certificateGroupType);
				if (result == null) result = caseBaseObjectType(certificateGroupType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.CERTIFICATE_TYPE: {
				CertificateType certificateType = (CertificateType)theEObject;
				T result = caseCertificateType(certificateType);
				if (result == null) result = caseBaseObjectType(certificateType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.SERVER_CONFIGURATION_TYPE: {
				ServerConfigurationType serverConfigurationType = (ServerConfigurationType)theEObject;
				T result = caseServerConfigurationType(serverConfigurationType);
				if (result == null) result = caseBaseObjectType(serverConfigurationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.KEY_CREDENTIAL_CONFIGURATION_TYPE: {
				KeyCredentialConfigurationType keyCredentialConfigurationType = (KeyCredentialConfigurationType)theEObject;
				T result = caseKeyCredentialConfigurationType(keyCredentialConfigurationType);
				if (result == null) result = caseBaseObjectType(keyCredentialConfigurationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.AUTHORIZATION_SERVICE_CONFIGURATION_TYPE: {
				AuthorizationServiceConfigurationType authorizationServiceConfigurationType = (AuthorizationServiceConfigurationType)theEObject;
				T result = caseAuthorizationServiceConfigurationType(authorizationServiceConfigurationType);
				if (result == null) result = caseBaseObjectType(authorizationServiceConfigurationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.AGGREGATE_CONFIGURATION_TYPE: {
				AggregateConfigurationType aggregateConfigurationType = (AggregateConfigurationType)theEObject;
				T result = caseAggregateConfigurationType(aggregateConfigurationType);
				if (result == null) result = caseBaseObjectType(aggregateConfigurationType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.PUB_SUB_KEY_SERVICE_TYPE: {
				PubSubKeyServiceType pubSubKeyServiceType = (PubSubKeyServiceType)theEObject;
				T result = casePubSubKeyServiceType(pubSubKeyServiceType);
				if (result == null) result = caseBaseObjectType(pubSubKeyServiceType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.SECURITY_GROUP_TYPE: {
				SecurityGroupType securityGroupType = (SecurityGroupType)theEObject;
				T result = caseSecurityGroupType(securityGroupType);
				if (result == null) result = caseBaseObjectType(securityGroupType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.PUBLISHED_DATA_SET_TYPE: {
				PublishedDataSetType publishedDataSetType = (PublishedDataSetType)theEObject;
				T result = casePublishedDataSetType(publishedDataSetType);
				if (result == null) result = caseBaseObjectType(publishedDataSetType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.EXTENSION_FIELDS_TYPE: {
				ExtensionFieldsType extensionFieldsType = (ExtensionFieldsType)theEObject;
				T result = caseExtensionFieldsType(extensionFieldsType);
				if (result == null) result = caseBaseObjectType(extensionFieldsType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.PUB_SUB_CONNECTION_TYPE: {
				PubSubConnectionType pubSubConnectionType = (PubSubConnectionType)theEObject;
				T result = casePubSubConnectionType(pubSubConnectionType);
				if (result == null) result = caseBaseObjectType(pubSubConnectionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.CONNECTION_TRANSPORT_TYPE: {
				ConnectionTransportType connectionTransportType = (ConnectionTransportType)theEObject;
				T result = caseConnectionTransportType(connectionTransportType);
				if (result == null) result = caseBaseObjectType(connectionTransportType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.PUB_SUB_GROUP_TYPE: {
				PubSubGroupType pubSubGroupType = (PubSubGroupType)theEObject;
				T result = casePubSubGroupType(pubSubGroupType);
				if (result == null) result = caseBaseObjectType(pubSubGroupType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.WRITER_GROUP_TRANSPORT_TYPE: {
				WriterGroupTransportType writerGroupTransportType = (WriterGroupTransportType)theEObject;
				T result = caseWriterGroupTransportType(writerGroupTransportType);
				if (result == null) result = caseBaseObjectType(writerGroupTransportType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.READER_GROUP_TRANSPORT_TYPE: {
				ReaderGroupTransportType readerGroupTransportType = (ReaderGroupTransportType)theEObject;
				T result = caseReaderGroupTransportType(readerGroupTransportType);
				if (result == null) result = caseBaseObjectType(readerGroupTransportType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.READER_GROUP_MESSAGE_TYPE: {
				ReaderGroupMessageType readerGroupMessageType = (ReaderGroupMessageType)theEObject;
				T result = caseReaderGroupMessageType(readerGroupMessageType);
				if (result == null) result = caseBaseObjectType(readerGroupMessageType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.DATA_SET_WRITER_TYPE: {
				DataSetWriterType dataSetWriterType = (DataSetWriterType)theEObject;
				T result = caseDataSetWriterType(dataSetWriterType);
				if (result == null) result = caseBaseObjectType(dataSetWriterType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.DATA_SET_WRITER_TRANSPORT_TYPE: {
				DataSetWriterTransportType dataSetWriterTransportType = (DataSetWriterTransportType)theEObject;
				T result = caseDataSetWriterTransportType(dataSetWriterTransportType);
				if (result == null) result = caseBaseObjectType(dataSetWriterTransportType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.DATA_SET_WRITER_MESSAGE_TYPE: {
				DataSetWriterMessageType dataSetWriterMessageType = (DataSetWriterMessageType)theEObject;
				T result = caseDataSetWriterMessageType(dataSetWriterMessageType);
				if (result == null) result = caseBaseObjectType(dataSetWriterMessageType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.DATA_SET_READER_TYPE: {
				DataSetReaderType dataSetReaderType = (DataSetReaderType)theEObject;
				T result = caseDataSetReaderType(dataSetReaderType);
				if (result == null) result = caseBaseObjectType(dataSetReaderType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.DATA_SET_READER_TRANSPORT_TYPE: {
				DataSetReaderTransportType dataSetReaderTransportType = (DataSetReaderTransportType)theEObject;
				T result = caseDataSetReaderTransportType(dataSetReaderTransportType);
				if (result == null) result = caseBaseObjectType(dataSetReaderTransportType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.DATA_SET_READER_MESSAGE_TYPE: {
				DataSetReaderMessageType dataSetReaderMessageType = (DataSetReaderMessageType)theEObject;
				T result = caseDataSetReaderMessageType(dataSetReaderMessageType);
				if (result == null) result = caseBaseObjectType(dataSetReaderMessageType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.SUBSCRIBED_DATA_SET_TYPE: {
				SubscribedDataSetType subscribedDataSetType = (SubscribedDataSetType)theEObject;
				T result = caseSubscribedDataSetType(subscribedDataSetType);
				if (result == null) result = caseBaseObjectType(subscribedDataSetType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.PUB_SUB_STATUS_TYPE: {
				PubSubStatusType pubSubStatusType = (PubSubStatusType)theEObject;
				T result = casePubSubStatusType(pubSubStatusType);
				if (result == null) result = caseBaseObjectType(pubSubStatusType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.PUB_SUB_DIAGNOSTICS_TYPE: {
				PubSubDiagnosticsType pubSubDiagnosticsType = (PubSubDiagnosticsType)theEObject;
				T result = casePubSubDiagnosticsType(pubSubDiagnosticsType);
				if (result == null) result = caseBaseObjectType(pubSubDiagnosticsType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.NETWORK_ADDRESS_TYPE: {
				NetworkAddressType networkAddressType = (NetworkAddressType)theEObject;
				T result = caseNetworkAddressType(networkAddressType);
				if (result == null) result = caseBaseObjectType(networkAddressType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.ALIAS_NAME_TYPE: {
				AliasNameType aliasNameType = (AliasNameType)theEObject;
				T result = caseAliasNameType(aliasNameType);
				if (result == null) result = caseBaseObjectType(aliasNameType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.BASE_VARIABLE_TYPE: {
				BaseVariableType baseVariableType = (BaseVariableType)theEObject;
				T result = caseBaseVariableType(baseVariableType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.BASE_DATA_VARIABLE_TYPE: {
				BaseDataVariableType baseDataVariableType = (BaseDataVariableType)theEObject;
				T result = caseBaseDataVariableType(baseDataVariableType);
				if (result == null) result = caseBaseVariableType(baseDataVariableType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.PROPERTY_TYPE: {
				PropertyType propertyType = (PropertyType)theEObject;
				T result = casePropertyType(propertyType);
				if (result == null) result = caseBaseVariableType(propertyType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.SERVER_VENDOR_CAPABILITY_TYPE: {
				ServerVendorCapabilityType serverVendorCapabilityType = (ServerVendorCapabilityType)theEObject;
				T result = caseServerVendorCapabilityType(serverVendorCapabilityType);
				if (result == null) result = caseBaseDataVariableType(serverVendorCapabilityType);
				if (result == null) result = caseBaseVariableType(serverVendorCapabilityType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.SAMPLING_INTERVAL_DIAGNOSTICS_ARRAY_TYPE: {
				SamplingIntervalDiagnosticsArrayType samplingIntervalDiagnosticsArrayType = (SamplingIntervalDiagnosticsArrayType)theEObject;
				T result = caseSamplingIntervalDiagnosticsArrayType(samplingIntervalDiagnosticsArrayType);
				if (result == null) result = caseBaseDataVariableType(samplingIntervalDiagnosticsArrayType);
				if (result == null) result = caseBaseVariableType(samplingIntervalDiagnosticsArrayType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.SAMPLING_INTERVAL_DIAGNOSTICS_TYPE: {
				SamplingIntervalDiagnosticsType samplingIntervalDiagnosticsType = (SamplingIntervalDiagnosticsType)theEObject;
				T result = caseSamplingIntervalDiagnosticsType(samplingIntervalDiagnosticsType);
				if (result == null) result = caseBaseDataVariableType(samplingIntervalDiagnosticsType);
				if (result == null) result = caseBaseVariableType(samplingIntervalDiagnosticsType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.SUBSCRIPTION_DIAGNOSTICS_ARRAY_TYPE: {
				SubscriptionDiagnosticsArrayType subscriptionDiagnosticsArrayType = (SubscriptionDiagnosticsArrayType)theEObject;
				T result = caseSubscriptionDiagnosticsArrayType(subscriptionDiagnosticsArrayType);
				if (result == null) result = caseBaseDataVariableType(subscriptionDiagnosticsArrayType);
				if (result == null) result = caseBaseVariableType(subscriptionDiagnosticsArrayType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.SUBSCRIPTION_DIAGNOSTICS_TYPE: {
				SubscriptionDiagnosticsType subscriptionDiagnosticsType = (SubscriptionDiagnosticsType)theEObject;
				T result = caseSubscriptionDiagnosticsType(subscriptionDiagnosticsType);
				if (result == null) result = caseBaseDataVariableType(subscriptionDiagnosticsType);
				if (result == null) result = caseBaseVariableType(subscriptionDiagnosticsType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.SESSION_DIAGNOSTICS_ARRAY_TYPE: {
				SessionDiagnosticsArrayType sessionDiagnosticsArrayType = (SessionDiagnosticsArrayType)theEObject;
				T result = caseSessionDiagnosticsArrayType(sessionDiagnosticsArrayType);
				if (result == null) result = caseBaseDataVariableType(sessionDiagnosticsArrayType);
				if (result == null) result = caseBaseVariableType(sessionDiagnosticsArrayType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.SESSION_DIAGNOSTICS_VARIABLE_TYPE: {
				SessionDiagnosticsVariableType sessionDiagnosticsVariableType = (SessionDiagnosticsVariableType)theEObject;
				T result = caseSessionDiagnosticsVariableType(sessionDiagnosticsVariableType);
				if (result == null) result = caseBaseDataVariableType(sessionDiagnosticsVariableType);
				if (result == null) result = caseBaseVariableType(sessionDiagnosticsVariableType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.SESSION_SECURITY_DIAGNOSTICS_ARRAY_TYPE: {
				SessionSecurityDiagnosticsArrayType sessionSecurityDiagnosticsArrayType = (SessionSecurityDiagnosticsArrayType)theEObject;
				T result = caseSessionSecurityDiagnosticsArrayType(sessionSecurityDiagnosticsArrayType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.SESSION_SECURITY_DIAGNOSTICS_TYPE: {
				SessionSecurityDiagnosticsType sessionSecurityDiagnosticsType = (SessionSecurityDiagnosticsType)theEObject;
				T result = caseSessionSecurityDiagnosticsType(sessionSecurityDiagnosticsType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.OPTION_SET_TYPE: {
				OptionSetType optionSetType = (OptionSetType)theEObject;
				T result = caseOptionSetType(optionSetType);
				if (result == null) result = caseBaseDataVariableType(optionSetType);
				if (result == null) result = caseBaseVariableType(optionSetType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.SERVER_DIAGNOSTICS_SUMMARY_TYPE: {
				ServerDiagnosticsSummaryType serverDiagnosticsSummaryType = (ServerDiagnosticsSummaryType)theEObject;
				T result = caseServerDiagnosticsSummaryType(serverDiagnosticsSummaryType);
				if (result == null) result = caseBaseDataVariableType(serverDiagnosticsSummaryType);
				if (result == null) result = caseBaseVariableType(serverDiagnosticsSummaryType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.BUILD_INFO_TYPE: {
				BuildInfoType buildInfoType = (BuildInfoType)theEObject;
				T result = caseBuildInfoType(buildInfoType);
				if (result == null) result = caseBaseDataVariableType(buildInfoType);
				if (result == null) result = caseBaseVariableType(buildInfoType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUAProfilePackage.SERVER_STATUS_TYPE: {
				ServerStatusType serverStatusType = (ServerStatusType)theEObject;
				T result = caseServerStatusType(serverStatusType);
				if (result == null) result = caseBaseDataVariableType(serverStatusType);
				if (result == null) result = caseBaseVariableType(serverStatusType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Type System Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Type System Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataTypeSystemType(DataTypeSystemType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Modelling Rule Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Modelling Rule Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModellingRuleType(ModellingRuleType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Server Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Server Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServerType(ServerType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Folder Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Folder Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFolderType(FolderType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Type Encoding Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Type Encoding Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataTypeEncodingType(DataTypeEncodingType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Server Capabilities Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Server Capabilities Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServerCapabilitiesType(ServerCapabilitiesType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Server Diagnostics Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Server Diagnostics Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServerDiagnosticsType(ServerDiagnosticsType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sessions Diagnostics Summary Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sessions Diagnostics Summary Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSessionsDiagnosticsSummaryType(SessionsDiagnosticsSummaryType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Session Diagnostics Object Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Session Diagnostics Object Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSessionDiagnosticsObjectType(SessionDiagnosticsObjectType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Vendor Server Info Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vendor Server Info Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVendorServerInfoType(VendorServerInfoType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Server Redundancy Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Server Redundancy Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServerRedundancyType(ServerRedundancyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>File Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>File Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFileType(FileType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Namespaces Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Namespaces Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamespacesType(NamespacesType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Event Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Event Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseEventType(BaseEventType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aggregate Function Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aggregate Function Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAggregateFunctionType(AggregateFunctionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>State Machine Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>State Machine Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStateMachineType(StateMachineType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>State Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>State Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStateType(StateType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Namespace Metadata Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Namespace Metadata Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamespaceMetadataType(NamespaceMetadataType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Writer Group Message Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Writer Group Message Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWriterGroupMessageType(WriterGroupMessageType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Transition Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Transition Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransitionType(TransitionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Temporary File Transfer Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Temporary File Transfer Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTemporaryFileTransferType(TemporaryFileTransferType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role Set Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role Set Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRoleSetType(RoleSetType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRoleType(RoleType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Interface Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Interface Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseInterfaceType(BaseInterfaceType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dictionary Entry Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dictionary Entry Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDictionaryEntryType(DictionaryEntryType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ordered List Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ordered List Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOrderedListType(OrderedListType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Condition Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Condition Class Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseConditionClassType(BaseConditionClassType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Alarm Metrics Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Alarm Metrics Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAlarmMetricsType(AlarmMetricsType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Historical Data Configuration Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Historical Data Configuration Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHistoricalDataConfigurationType(HistoricalDataConfigurationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>History Server Capabilities Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>History Server Capabilities Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHistoryServerCapabilitiesType(HistoryServerCapabilitiesType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Certificate Group Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Certificate Group Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCertificateGroupType(CertificateGroupType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Certificate Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Certificate Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCertificateType(CertificateType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Server Configuration Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Server Configuration Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServerConfigurationType(ServerConfigurationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Key Credential Configuration Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Key Credential Configuration Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseKeyCredentialConfigurationType(KeyCredentialConfigurationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Authorization Service Configuration Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Authorization Service Configuration Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAuthorizationServiceConfigurationType(AuthorizationServiceConfigurationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aggregate Configuration Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aggregate Configuration Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAggregateConfigurationType(AggregateConfigurationType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pub Sub Key Service Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pub Sub Key Service Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePubSubKeyServiceType(PubSubKeyServiceType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Security Group Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Security Group Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSecurityGroupType(SecurityGroupType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Published Data Set Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Published Data Set Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePublishedDataSetType(PublishedDataSetType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Extension Fields Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Extension Fields Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExtensionFieldsType(ExtensionFieldsType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pub Sub Connection Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pub Sub Connection Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePubSubConnectionType(PubSubConnectionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connection Transport Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connection Transport Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnectionTransportType(ConnectionTransportType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pub Sub Group Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pub Sub Group Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePubSubGroupType(PubSubGroupType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Writer Group Transport Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Writer Group Transport Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWriterGroupTransportType(WriterGroupTransportType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reader Group Transport Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reader Group Transport Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReaderGroupTransportType(ReaderGroupTransportType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reader Group Message Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reader Group Message Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReaderGroupMessageType(ReaderGroupMessageType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Set Writer Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Set Writer Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataSetWriterType(DataSetWriterType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Set Writer Transport Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Set Writer Transport Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataSetWriterTransportType(DataSetWriterTransportType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Set Writer Message Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Set Writer Message Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataSetWriterMessageType(DataSetWriterMessageType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Set Reader Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Set Reader Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataSetReaderType(DataSetReaderType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Set Reader Transport Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Set Reader Transport Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataSetReaderTransportType(DataSetReaderTransportType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Set Reader Message Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Set Reader Message Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataSetReaderMessageType(DataSetReaderMessageType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Subscribed Data Set Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Subscribed Data Set Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubscribedDataSetType(SubscribedDataSetType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pub Sub Status Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pub Sub Status Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePubSubStatusType(PubSubStatusType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pub Sub Diagnostics Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pub Sub Diagnostics Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePubSubDiagnosticsType(PubSubDiagnosticsType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Network Address Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Network Address Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNetworkAddressType(NetworkAddressType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Alias Name Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Alias Name Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAliasNameType(AliasNameType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Variable Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Variable Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseVariableType(BaseVariableType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Data Variable Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Data Variable Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseDataVariableType(BaseDataVariableType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Property Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePropertyType(PropertyType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Server Vendor Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Server Vendor Capability Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServerVendorCapabilityType(ServerVendorCapabilityType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sampling Interval Diagnostics Array Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sampling Interval Diagnostics Array Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSamplingIntervalDiagnosticsArrayType(SamplingIntervalDiagnosticsArrayType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sampling Interval Diagnostics Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sampling Interval Diagnostics Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSamplingIntervalDiagnosticsType(SamplingIntervalDiagnosticsType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Subscription Diagnostics Array Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Subscription Diagnostics Array Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubscriptionDiagnosticsArrayType(SubscriptionDiagnosticsArrayType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Subscription Diagnostics Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Subscription Diagnostics Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubscriptionDiagnosticsType(SubscriptionDiagnosticsType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Session Diagnostics Array Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Session Diagnostics Array Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSessionDiagnosticsArrayType(SessionDiagnosticsArrayType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Session Diagnostics Variable Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Session Diagnostics Variable Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSessionDiagnosticsVariableType(SessionDiagnosticsVariableType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Session Security Diagnostics Array Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Session Security Diagnostics Array Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSessionSecurityDiagnosticsArrayType(SessionSecurityDiagnosticsArrayType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Session Security Diagnostics Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Session Security Diagnostics Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSessionSecurityDiagnosticsType(SessionSecurityDiagnosticsType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Option Set Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Option Set Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOptionSetType(OptionSetType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Server Diagnostics Summary Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Server Diagnostics Summary Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServerDiagnosticsSummaryType(ServerDiagnosticsSummaryType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Build Info Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Build Info Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBuildInfoType(BuildInfoType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Server Status Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Server Status Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServerStatusType(ServerStatusType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Object Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Object Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseObjectType(BaseObjectType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //OPCUAProfileSwitch
