/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.di.opcuadiprofile.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.ConnectionPointType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.DeviceType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.IDeviceHealthType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.ISupportInfoType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.OPC_UA_DI_Library.DeviceHealthEnumeration;

import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.ByteString;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Image;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Device Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.DeviceTypeImpl#getDeviceTypeImage <em>Device Type Image</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.DeviceTypeImpl#getDocumentation <em>Documentation</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.DeviceTypeImpl#getProtocolSupport <em>Protocol Support</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.DeviceTypeImpl#getImageSet <em>Image Set</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.DeviceTypeImpl#getDeviceHealth <em>Device Health</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.DeviceTypeImpl#getCPIIdentifier <em>CPI Identifier</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeviceTypeImpl extends ComponentTypeImpl implements DeviceType {
	/**
	 * The cached value of the '{@link #getDeviceTypeImage() <em>Device Type Image</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeviceTypeImage()
	 * @generated
	 * @ordered
	 */
	protected EList<Image> deviceTypeImage;

	/**
	 * The cached value of the '{@link #getDocumentation() <em>Documentation</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDocumentation()
	 * @generated
	 * @ordered
	 */
	protected EList<ByteString> documentation;

	/**
	 * The cached value of the '{@link #getProtocolSupport() <em>Protocol Support</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProtocolSupport()
	 * @generated
	 * @ordered
	 */
	protected EList<ByteString> protocolSupport;

	/**
	 * The cached value of the '{@link #getImageSet() <em>Image Set</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImageSet()
	 * @generated
	 * @ordered
	 */
	protected EList<Image> imageSet;

	/**
	 * The default value of the '{@link #getDeviceHealth() <em>Device Health</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeviceHealth()
	 * @generated
	 * @ordered
	 */
	protected static final DeviceHealthEnumeration DEVICE_HEALTH_EDEFAULT = DeviceHealthEnumeration.NORMAL;

	/**
	 * The cached value of the '{@link #getDeviceHealth() <em>Device Health</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeviceHealth()
	 * @generated
	 * @ordered
	 */
	protected DeviceHealthEnumeration deviceHealth = DEVICE_HEALTH_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCPIIdentifier() <em>CPI Identifier</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCPIIdentifier()
	 * @generated
	 * @ordered
	 */
	protected ConnectionPointType cpiIdentifier;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeviceTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUADIProfilePackage.Literals.DEVICE_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Image> getDeviceTypeImage() {
		if (deviceTypeImage == null) {
			deviceTypeImage = new EObjectContainmentEList<Image>(Image.class, this, OPCUADIProfilePackage.DEVICE_TYPE__DEVICE_TYPE_IMAGE);
		}
		return deviceTypeImage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ByteString> getDocumentation() {
		if (documentation == null) {
			documentation = new EObjectContainmentEList<ByteString>(ByteString.class, this, OPCUADIProfilePackage.DEVICE_TYPE__DOCUMENTATION);
		}
		return documentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ByteString> getProtocolSupport() {
		if (protocolSupport == null) {
			protocolSupport = new EObjectContainmentEList<ByteString>(ByteString.class, this, OPCUADIProfilePackage.DEVICE_TYPE__PROTOCOL_SUPPORT);
		}
		return protocolSupport;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Image> getImageSet() {
		if (imageSet == null) {
			imageSet = new EObjectContainmentEList<Image>(Image.class, this, OPCUADIProfilePackage.DEVICE_TYPE__IMAGE_SET);
		}
		return imageSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeviceHealthEnumeration getDeviceHealth() {
		return deviceHealth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeviceHealth(DeviceHealthEnumeration newDeviceHealth) {
		DeviceHealthEnumeration oldDeviceHealth = deviceHealth;
		deviceHealth = newDeviceHealth == null ? DEVICE_HEALTH_EDEFAULT : newDeviceHealth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.DEVICE_TYPE__DEVICE_HEALTH, oldDeviceHealth, deviceHealth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionPointType getCPIIdentifier() {
		if (cpiIdentifier != null && cpiIdentifier.eIsProxy()) {
			InternalEObject oldCPIIdentifier = (InternalEObject)cpiIdentifier;
			cpiIdentifier = (ConnectionPointType)eResolveProxy(oldCPIIdentifier);
			if (cpiIdentifier != oldCPIIdentifier) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OPCUADIProfilePackage.DEVICE_TYPE__CPI_IDENTIFIER, oldCPIIdentifier, cpiIdentifier));
			}
		}
		return cpiIdentifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionPointType basicGetCPIIdentifier() {
		return cpiIdentifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCPIIdentifier(ConnectionPointType newCPIIdentifier) {
		ConnectionPointType oldCPIIdentifier = cpiIdentifier;
		cpiIdentifier = newCPIIdentifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.DEVICE_TYPE__CPI_IDENTIFIER, oldCPIIdentifier, cpiIdentifier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OPCUADIProfilePackage.DEVICE_TYPE__DEVICE_TYPE_IMAGE:
				return ((InternalEList<?>)getDeviceTypeImage()).basicRemove(otherEnd, msgs);
			case OPCUADIProfilePackage.DEVICE_TYPE__DOCUMENTATION:
				return ((InternalEList<?>)getDocumentation()).basicRemove(otherEnd, msgs);
			case OPCUADIProfilePackage.DEVICE_TYPE__PROTOCOL_SUPPORT:
				return ((InternalEList<?>)getProtocolSupport()).basicRemove(otherEnd, msgs);
			case OPCUADIProfilePackage.DEVICE_TYPE__IMAGE_SET:
				return ((InternalEList<?>)getImageSet()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPCUADIProfilePackage.DEVICE_TYPE__DEVICE_TYPE_IMAGE:
				return getDeviceTypeImage();
			case OPCUADIProfilePackage.DEVICE_TYPE__DOCUMENTATION:
				return getDocumentation();
			case OPCUADIProfilePackage.DEVICE_TYPE__PROTOCOL_SUPPORT:
				return getProtocolSupport();
			case OPCUADIProfilePackage.DEVICE_TYPE__IMAGE_SET:
				return getImageSet();
			case OPCUADIProfilePackage.DEVICE_TYPE__DEVICE_HEALTH:
				return getDeviceHealth();
			case OPCUADIProfilePackage.DEVICE_TYPE__CPI_IDENTIFIER:
				if (resolve) return getCPIIdentifier();
				return basicGetCPIIdentifier();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPCUADIProfilePackage.DEVICE_TYPE__DEVICE_TYPE_IMAGE:
				getDeviceTypeImage().clear();
				getDeviceTypeImage().addAll((Collection<? extends Image>)newValue);
				return;
			case OPCUADIProfilePackage.DEVICE_TYPE__DOCUMENTATION:
				getDocumentation().clear();
				getDocumentation().addAll((Collection<? extends ByteString>)newValue);
				return;
			case OPCUADIProfilePackage.DEVICE_TYPE__PROTOCOL_SUPPORT:
				getProtocolSupport().clear();
				getProtocolSupport().addAll((Collection<? extends ByteString>)newValue);
				return;
			case OPCUADIProfilePackage.DEVICE_TYPE__IMAGE_SET:
				getImageSet().clear();
				getImageSet().addAll((Collection<? extends Image>)newValue);
				return;
			case OPCUADIProfilePackage.DEVICE_TYPE__DEVICE_HEALTH:
				setDeviceHealth((DeviceHealthEnumeration)newValue);
				return;
			case OPCUADIProfilePackage.DEVICE_TYPE__CPI_IDENTIFIER:
				setCPIIdentifier((ConnectionPointType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPCUADIProfilePackage.DEVICE_TYPE__DEVICE_TYPE_IMAGE:
				getDeviceTypeImage().clear();
				return;
			case OPCUADIProfilePackage.DEVICE_TYPE__DOCUMENTATION:
				getDocumentation().clear();
				return;
			case OPCUADIProfilePackage.DEVICE_TYPE__PROTOCOL_SUPPORT:
				getProtocolSupport().clear();
				return;
			case OPCUADIProfilePackage.DEVICE_TYPE__IMAGE_SET:
				getImageSet().clear();
				return;
			case OPCUADIProfilePackage.DEVICE_TYPE__DEVICE_HEALTH:
				setDeviceHealth(DEVICE_HEALTH_EDEFAULT);
				return;
			case OPCUADIProfilePackage.DEVICE_TYPE__CPI_IDENTIFIER:
				setCPIIdentifier((ConnectionPointType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPCUADIProfilePackage.DEVICE_TYPE__DEVICE_TYPE_IMAGE:
				return deviceTypeImage != null && !deviceTypeImage.isEmpty();
			case OPCUADIProfilePackage.DEVICE_TYPE__DOCUMENTATION:
				return documentation != null && !documentation.isEmpty();
			case OPCUADIProfilePackage.DEVICE_TYPE__PROTOCOL_SUPPORT:
				return protocolSupport != null && !protocolSupport.isEmpty();
			case OPCUADIProfilePackage.DEVICE_TYPE__IMAGE_SET:
				return imageSet != null && !imageSet.isEmpty();
			case OPCUADIProfilePackage.DEVICE_TYPE__DEVICE_HEALTH:
				return deviceHealth != DEVICE_HEALTH_EDEFAULT;
			case OPCUADIProfilePackage.DEVICE_TYPE__CPI_IDENTIFIER:
				return cpiIdentifier != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ISupportInfoType.class) {
			switch (derivedFeatureID) {
				case OPCUADIProfilePackage.DEVICE_TYPE__DEVICE_TYPE_IMAGE: return OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__DEVICE_TYPE_IMAGE;
				case OPCUADIProfilePackage.DEVICE_TYPE__DOCUMENTATION: return OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__DOCUMENTATION;
				case OPCUADIProfilePackage.DEVICE_TYPE__PROTOCOL_SUPPORT: return OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__PROTOCOL_SUPPORT;
				case OPCUADIProfilePackage.DEVICE_TYPE__IMAGE_SET: return OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__IMAGE_SET;
				default: return -1;
			}
		}
		if (baseClass == IDeviceHealthType.class) {
			switch (derivedFeatureID) {
				case OPCUADIProfilePackage.DEVICE_TYPE__DEVICE_HEALTH: return OPCUADIProfilePackage.IDEVICE_HEALTH_TYPE__DEVICE_HEALTH;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ISupportInfoType.class) {
			switch (baseFeatureID) {
				case OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__DEVICE_TYPE_IMAGE: return OPCUADIProfilePackage.DEVICE_TYPE__DEVICE_TYPE_IMAGE;
				case OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__DOCUMENTATION: return OPCUADIProfilePackage.DEVICE_TYPE__DOCUMENTATION;
				case OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__PROTOCOL_SUPPORT: return OPCUADIProfilePackage.DEVICE_TYPE__PROTOCOL_SUPPORT;
				case OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__IMAGE_SET: return OPCUADIProfilePackage.DEVICE_TYPE__IMAGE_SET;
				default: return -1;
			}
		}
		if (baseClass == IDeviceHealthType.class) {
			switch (baseFeatureID) {
				case OPCUADIProfilePackage.IDEVICE_HEALTH_TYPE__DEVICE_HEALTH: return OPCUADIProfilePackage.DEVICE_TYPE__DEVICE_HEALTH;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (DeviceHealth: ");
		result.append(deviceHealth);
		result.append(')');
		return result.toString();
	}

} //DeviceTypeImpl
