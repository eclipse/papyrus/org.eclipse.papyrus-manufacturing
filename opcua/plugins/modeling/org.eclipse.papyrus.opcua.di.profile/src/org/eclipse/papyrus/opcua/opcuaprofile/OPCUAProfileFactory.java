/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.opcuaprofile;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPCUAProfilePackage
 * @generated
 */
public interface OPCUAProfileFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OPCUAProfileFactory eINSTANCE = org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfileFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Data Type System Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Type System Type</em>'.
	 * @generated
	 */
	DataTypeSystemType createDataTypeSystemType();

	/**
	 * Returns a new object of class '<em>Modelling Rule Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Modelling Rule Type</em>'.
	 * @generated
	 */
	ModellingRuleType createModellingRuleType();

	/**
	 * Returns a new object of class '<em>Server Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Server Type</em>'.
	 * @generated
	 */
	ServerType createServerType();

	/**
	 * Returns a new object of class '<em>Folder Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Folder Type</em>'.
	 * @generated
	 */
	FolderType createFolderType();

	/**
	 * Returns a new object of class '<em>Data Type Encoding Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Type Encoding Type</em>'.
	 * @generated
	 */
	DataTypeEncodingType createDataTypeEncodingType();

	/**
	 * Returns a new object of class '<em>Server Capabilities Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Server Capabilities Type</em>'.
	 * @generated
	 */
	ServerCapabilitiesType createServerCapabilitiesType();

	/**
	 * Returns a new object of class '<em>Server Diagnostics Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Server Diagnostics Type</em>'.
	 * @generated
	 */
	ServerDiagnosticsType createServerDiagnosticsType();

	/**
	 * Returns a new object of class '<em>Sessions Diagnostics Summary Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sessions Diagnostics Summary Type</em>'.
	 * @generated
	 */
	SessionsDiagnosticsSummaryType createSessionsDiagnosticsSummaryType();

	/**
	 * Returns a new object of class '<em>Session Diagnostics Object Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Session Diagnostics Object Type</em>'.
	 * @generated
	 */
	SessionDiagnosticsObjectType createSessionDiagnosticsObjectType();

	/**
	 * Returns a new object of class '<em>Vendor Server Info Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vendor Server Info Type</em>'.
	 * @generated
	 */
	VendorServerInfoType createVendorServerInfoType();

	/**
	 * Returns a new object of class '<em>Server Redundancy Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Server Redundancy Type</em>'.
	 * @generated
	 */
	ServerRedundancyType createServerRedundancyType();

	/**
	 * Returns a new object of class '<em>File Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>File Type</em>'.
	 * @generated
	 */
	FileType createFileType();

	/**
	 * Returns a new object of class '<em>Namespaces Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Namespaces Type</em>'.
	 * @generated
	 */
	NamespacesType createNamespacesType();

	/**
	 * Returns a new object of class '<em>Base Event Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Event Type</em>'.
	 * @generated
	 */
	BaseEventType createBaseEventType();

	/**
	 * Returns a new object of class '<em>Aggregate Function Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aggregate Function Type</em>'.
	 * @generated
	 */
	AggregateFunctionType createAggregateFunctionType();

	/**
	 * Returns a new object of class '<em>State Machine Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>State Machine Type</em>'.
	 * @generated
	 */
	StateMachineType createStateMachineType();

	/**
	 * Returns a new object of class '<em>State Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>State Type</em>'.
	 * @generated
	 */
	StateType createStateType();

	/**
	 * Returns a new object of class '<em>Namespace Metadata Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Namespace Metadata Type</em>'.
	 * @generated
	 */
	NamespaceMetadataType createNamespaceMetadataType();

	/**
	 * Returns a new object of class '<em>Writer Group Message Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Writer Group Message Type</em>'.
	 * @generated
	 */
	WriterGroupMessageType createWriterGroupMessageType();

	/**
	 * Returns a new object of class '<em>Transition Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Transition Type</em>'.
	 * @generated
	 */
	TransitionType createTransitionType();

	/**
	 * Returns a new object of class '<em>Temporary File Transfer Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Temporary File Transfer Type</em>'.
	 * @generated
	 */
	TemporaryFileTransferType createTemporaryFileTransferType();

	/**
	 * Returns a new object of class '<em>Role Set Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Role Set Type</em>'.
	 * @generated
	 */
	RoleSetType createRoleSetType();

	/**
	 * Returns a new object of class '<em>Role Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Role Type</em>'.
	 * @generated
	 */
	RoleType createRoleType();

	/**
	 * Returns a new object of class '<em>Base Interface Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Interface Type</em>'.
	 * @generated
	 */
	BaseInterfaceType createBaseInterfaceType();

	/**
	 * Returns a new object of class '<em>Dictionary Entry Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dictionary Entry Type</em>'.
	 * @generated
	 */
	DictionaryEntryType createDictionaryEntryType();

	/**
	 * Returns a new object of class '<em>Ordered List Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ordered List Type</em>'.
	 * @generated
	 */
	OrderedListType createOrderedListType();

	/**
	 * Returns a new object of class '<em>Base Condition Class Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Condition Class Type</em>'.
	 * @generated
	 */
	BaseConditionClassType createBaseConditionClassType();

	/**
	 * Returns a new object of class '<em>Alarm Metrics Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Alarm Metrics Type</em>'.
	 * @generated
	 */
	AlarmMetricsType createAlarmMetricsType();

	/**
	 * Returns a new object of class '<em>Historical Data Configuration Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Historical Data Configuration Type</em>'.
	 * @generated
	 */
	HistoricalDataConfigurationType createHistoricalDataConfigurationType();

	/**
	 * Returns a new object of class '<em>History Server Capabilities Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>History Server Capabilities Type</em>'.
	 * @generated
	 */
	HistoryServerCapabilitiesType createHistoryServerCapabilitiesType();

	/**
	 * Returns a new object of class '<em>Certificate Group Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Certificate Group Type</em>'.
	 * @generated
	 */
	CertificateGroupType createCertificateGroupType();

	/**
	 * Returns a new object of class '<em>Certificate Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Certificate Type</em>'.
	 * @generated
	 */
	CertificateType createCertificateType();

	/**
	 * Returns a new object of class '<em>Server Configuration Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Server Configuration Type</em>'.
	 * @generated
	 */
	ServerConfigurationType createServerConfigurationType();

	/**
	 * Returns a new object of class '<em>Key Credential Configuration Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Key Credential Configuration Type</em>'.
	 * @generated
	 */
	KeyCredentialConfigurationType createKeyCredentialConfigurationType();

	/**
	 * Returns a new object of class '<em>Authorization Service Configuration Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Authorization Service Configuration Type</em>'.
	 * @generated
	 */
	AuthorizationServiceConfigurationType createAuthorizationServiceConfigurationType();

	/**
	 * Returns a new object of class '<em>Aggregate Configuration Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aggregate Configuration Type</em>'.
	 * @generated
	 */
	AggregateConfigurationType createAggregateConfigurationType();

	/**
	 * Returns a new object of class '<em>Pub Sub Key Service Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pub Sub Key Service Type</em>'.
	 * @generated
	 */
	PubSubKeyServiceType createPubSubKeyServiceType();

	/**
	 * Returns a new object of class '<em>Security Group Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Security Group Type</em>'.
	 * @generated
	 */
	SecurityGroupType createSecurityGroupType();

	/**
	 * Returns a new object of class '<em>Published Data Set Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Published Data Set Type</em>'.
	 * @generated
	 */
	PublishedDataSetType createPublishedDataSetType();

	/**
	 * Returns a new object of class '<em>Extension Fields Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Extension Fields Type</em>'.
	 * @generated
	 */
	ExtensionFieldsType createExtensionFieldsType();

	/**
	 * Returns a new object of class '<em>Pub Sub Connection Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pub Sub Connection Type</em>'.
	 * @generated
	 */
	PubSubConnectionType createPubSubConnectionType();

	/**
	 * Returns a new object of class '<em>Connection Transport Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Connection Transport Type</em>'.
	 * @generated
	 */
	ConnectionTransportType createConnectionTransportType();

	/**
	 * Returns a new object of class '<em>Pub Sub Group Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pub Sub Group Type</em>'.
	 * @generated
	 */
	PubSubGroupType createPubSubGroupType();

	/**
	 * Returns a new object of class '<em>Writer Group Transport Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Writer Group Transport Type</em>'.
	 * @generated
	 */
	WriterGroupTransportType createWriterGroupTransportType();

	/**
	 * Returns a new object of class '<em>Reader Group Transport Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reader Group Transport Type</em>'.
	 * @generated
	 */
	ReaderGroupTransportType createReaderGroupTransportType();

	/**
	 * Returns a new object of class '<em>Reader Group Message Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reader Group Message Type</em>'.
	 * @generated
	 */
	ReaderGroupMessageType createReaderGroupMessageType();

	/**
	 * Returns a new object of class '<em>Data Set Writer Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Set Writer Type</em>'.
	 * @generated
	 */
	DataSetWriterType createDataSetWriterType();

	/**
	 * Returns a new object of class '<em>Data Set Writer Transport Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Set Writer Transport Type</em>'.
	 * @generated
	 */
	DataSetWriterTransportType createDataSetWriterTransportType();

	/**
	 * Returns a new object of class '<em>Data Set Writer Message Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Set Writer Message Type</em>'.
	 * @generated
	 */
	DataSetWriterMessageType createDataSetWriterMessageType();

	/**
	 * Returns a new object of class '<em>Data Set Reader Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Set Reader Type</em>'.
	 * @generated
	 */
	DataSetReaderType createDataSetReaderType();

	/**
	 * Returns a new object of class '<em>Data Set Reader Transport Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Set Reader Transport Type</em>'.
	 * @generated
	 */
	DataSetReaderTransportType createDataSetReaderTransportType();

	/**
	 * Returns a new object of class '<em>Data Set Reader Message Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Set Reader Message Type</em>'.
	 * @generated
	 */
	DataSetReaderMessageType createDataSetReaderMessageType();

	/**
	 * Returns a new object of class '<em>Subscribed Data Set Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Subscribed Data Set Type</em>'.
	 * @generated
	 */
	SubscribedDataSetType createSubscribedDataSetType();

	/**
	 * Returns a new object of class '<em>Pub Sub Status Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pub Sub Status Type</em>'.
	 * @generated
	 */
	PubSubStatusType createPubSubStatusType();

	/**
	 * Returns a new object of class '<em>Pub Sub Diagnostics Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pub Sub Diagnostics Type</em>'.
	 * @generated
	 */
	PubSubDiagnosticsType createPubSubDiagnosticsType();

	/**
	 * Returns a new object of class '<em>Network Address Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Network Address Type</em>'.
	 * @generated
	 */
	NetworkAddressType createNetworkAddressType();

	/**
	 * Returns a new object of class '<em>Alias Name Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Alias Name Type</em>'.
	 * @generated
	 */
	AliasNameType createAliasNameType();

	/**
	 * Returns a new object of class '<em>Base Variable Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Variable Type</em>'.
	 * @generated
	 */
	BaseVariableType createBaseVariableType();

	/**
	 * Returns a new object of class '<em>Base Data Variable Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Data Variable Type</em>'.
	 * @generated
	 */
	BaseDataVariableType createBaseDataVariableType();

	/**
	 * Returns a new object of class '<em>Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Property Type</em>'.
	 * @generated
	 */
	PropertyType createPropertyType();

	/**
	 * Returns a new object of class '<em>Server Vendor Capability Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Server Vendor Capability Type</em>'.
	 * @generated
	 */
	ServerVendorCapabilityType createServerVendorCapabilityType();

	/**
	 * Returns a new object of class '<em>Sampling Interval Diagnostics Array Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sampling Interval Diagnostics Array Type</em>'.
	 * @generated
	 */
	SamplingIntervalDiagnosticsArrayType createSamplingIntervalDiagnosticsArrayType();

	/**
	 * Returns a new object of class '<em>Sampling Interval Diagnostics Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sampling Interval Diagnostics Type</em>'.
	 * @generated
	 */
	SamplingIntervalDiagnosticsType createSamplingIntervalDiagnosticsType();

	/**
	 * Returns a new object of class '<em>Subscription Diagnostics Array Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Subscription Diagnostics Array Type</em>'.
	 * @generated
	 */
	SubscriptionDiagnosticsArrayType createSubscriptionDiagnosticsArrayType();

	/**
	 * Returns a new object of class '<em>Subscription Diagnostics Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Subscription Diagnostics Type</em>'.
	 * @generated
	 */
	SubscriptionDiagnosticsType createSubscriptionDiagnosticsType();

	/**
	 * Returns a new object of class '<em>Session Diagnostics Array Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Session Diagnostics Array Type</em>'.
	 * @generated
	 */
	SessionDiagnosticsArrayType createSessionDiagnosticsArrayType();

	/**
	 * Returns a new object of class '<em>Session Diagnostics Variable Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Session Diagnostics Variable Type</em>'.
	 * @generated
	 */
	SessionDiagnosticsVariableType createSessionDiagnosticsVariableType();

	/**
	 * Returns a new object of class '<em>Session Security Diagnostics Array Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Session Security Diagnostics Array Type</em>'.
	 * @generated
	 */
	SessionSecurityDiagnosticsArrayType createSessionSecurityDiagnosticsArrayType();

	/**
	 * Returns a new object of class '<em>Session Security Diagnostics Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Session Security Diagnostics Type</em>'.
	 * @generated
	 */
	SessionSecurityDiagnosticsType createSessionSecurityDiagnosticsType();

	/**
	 * Returns a new object of class '<em>Option Set Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Option Set Type</em>'.
	 * @generated
	 */
	OptionSetType createOptionSetType();

	/**
	 * Returns a new object of class '<em>Server Diagnostics Summary Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Server Diagnostics Summary Type</em>'.
	 * @generated
	 */
	ServerDiagnosticsSummaryType createServerDiagnosticsSummaryType();

	/**
	 * Returns a new object of class '<em>Build Info Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Build Info Type</em>'.
	 * @generated
	 */
	BuildInfoType createBuildInfoType();

	/**
	 * Returns a new object of class '<em>Server Status Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Server Status Type</em>'.
	 * @generated
	 */
	ServerStatusType createServerStatusType();

	/**
	 * Returns a new object of class '<em>Base Object Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Object Type</em>'.
	 * @generated
	 */
	BaseObjectType createBaseObjectType();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	OPCUAProfilePackage getOPCUAProfilePackage();

} //OPCUAProfileFactory
