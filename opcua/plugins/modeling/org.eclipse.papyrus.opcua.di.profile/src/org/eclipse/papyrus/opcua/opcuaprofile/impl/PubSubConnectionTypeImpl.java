/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.opcuaprofile.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.opcua.opcuaprofile.OPCUAProfilePackage;
import org.eclipse.papyrus.opcua.opcuaprofile.PubSubConnectionType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Pub Sub Connection Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PubSubConnectionTypeImpl extends BaseObjectTypeImpl implements PubSubConnectionType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PubSubConnectionTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUAProfilePackage.Literals.PUB_SUB_CONNECTION_TYPE;
	}

} //PubSubConnectionTypeImpl
