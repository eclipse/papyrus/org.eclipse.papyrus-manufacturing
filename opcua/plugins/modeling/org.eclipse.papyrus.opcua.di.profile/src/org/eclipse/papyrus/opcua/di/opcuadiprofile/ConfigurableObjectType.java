/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.di.opcuadiprofile;

import org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Configurable Object Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getConfigurableObjectType()
 * @model
 * @generated
 */
public interface ConfigurableObjectType extends BaseObjectType {
} // ConfigurableObjectType
