/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library;

import java.lang.String;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.OPC_UA_LibraryFactory
 * @model kind="package"
 * @generated
 */
public interface OPC_UA_LibraryPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "OPC_UA_Library";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///OPC_UA/OPC_UA_Library.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "OPC_UA.OPC_UA_Library";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OPC_UA_LibraryPackage eINSTANCE = org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.BaseDataTypeImpl <em>Base Data Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.BaseDataTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getBaseDataType()
	 * @generated
	 */
	int BASE_DATA_TYPE = 3;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_DATA_TYPE__NODE_ID = 0;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_DATA_TYPE__NAMESPACE_URI = 1;

	/**
	 * The number of structural features of the '<em>Base Data Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_DATA_TYPE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Base Data Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_DATA_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.NumberImpl <em>Number</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.NumberImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getNumber()
	 * @generated
	 */
	int NUMBER = 2;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER__NODE_ID = BASE_DATA_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER__NAMESPACE_URI = BASE_DATA_TYPE__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>Number</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER_FEATURE_COUNT = BASE_DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Number</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER_OPERATION_COUNT = BASE_DATA_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.DoubleImpl <em>Double</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.DoubleImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getDouble()
	 * @generated
	 */
	int DOUBLE = 1;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE__NODE_ID = NUMBER__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE__NAMESPACE_URI = NUMBER__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>Double</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_FEATURE_COUNT = NUMBER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Double</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_OPERATION_COUNT = NUMBER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.AnalogUnitTypeImpl <em>Analog Unit Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.AnalogUnitTypeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getAnalogUnitType()
	 * @generated
	 */
	int ANALOG_UNIT_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALOG_UNIT_TYPE__NODE_ID = DOUBLE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALOG_UNIT_TYPE__NAMESPACE_URI = DOUBLE__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>Analog Unit Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALOG_UNIT_TYPE_FEATURE_COUNT = DOUBLE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Analog Unit Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALOG_UNIT_TYPE_OPERATION_COUNT = DOUBLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.LocalizedTextImpl <em>Localized Text</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.LocalizedTextImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getLocalizedText()
	 * @generated
	 */
	int LOCALIZED_TEXT = 4;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCALIZED_TEXT__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Local</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCALIZED_TEXT__LOCAL = 1;

	/**
	 * The number of structural features of the '<em>Localized Text</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCALIZED_TEXT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Localized Text</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCALIZED_TEXT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.StringImpl <em>String</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.StringImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getString()
	 * @generated
	 */
	int STRING = 6;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING__NODE_ID = BASE_DATA_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING__NAMESPACE_URI = BASE_DATA_TYPE__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_FEATURE_COUNT = BASE_DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_OPERATION_COUNT = BASE_DATA_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.LocalIdImpl <em>Local Id</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.LocalIdImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getLocalId()
	 * @generated
	 */
	int LOCAL_ID = 5;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_ID__NODE_ID = STRING__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_ID__NAMESPACE_URI = STRING__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>Local Id</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_ID_FEATURE_COUNT = STRING_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Local Id</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_ID_OPERATION_COUNT = STRING_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.DateTimeImpl <em>Date Time</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.DateTimeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getDateTime()
	 * @generated
	 */
	int DATE_TIME = 7;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATE_TIME__NODE_ID = BASE_DATA_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATE_TIME__NAMESPACE_URI = BASE_DATA_TYPE__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>Date Time</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATE_TIME_FEATURE_COUNT = BASE_DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Date Time</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATE_TIME_OPERATION_COUNT = BASE_DATA_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.GuidImpl <em>Guid</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.GuidImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getGuid()
	 * @generated
	 */
	int GUID = 8;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUID__NODE_ID = BASE_DATA_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUID__NAMESPACE_URI = BASE_DATA_TYPE__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>Guid</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUID_FEATURE_COUNT = BASE_DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Guid</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUID_OPERATION_COUNT = BASE_DATA_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.ByteStringImpl <em>Byte String</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.ByteStringImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getByteString()
	 * @generated
	 */
	int BYTE_STRING = 9;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BYTE_STRING__NODE_ID = BASE_DATA_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BYTE_STRING__NAMESPACE_URI = BASE_DATA_TYPE__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>Byte String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BYTE_STRING_FEATURE_COUNT = BASE_DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Byte String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BYTE_STRING_OPERATION_COUNT = BASE_DATA_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.XmlElementImpl <em>Xml Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.XmlElementImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getXmlElement()
	 * @generated
	 */
	int XML_ELEMENT = 10;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ELEMENT__NODE_ID = BASE_DATA_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ELEMENT__NAMESPACE_URI = BASE_DATA_TYPE__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>Xml Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ELEMENT_FEATURE_COUNT = BASE_DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Xml Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ELEMENT_OPERATION_COUNT = BASE_DATA_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.NodeIdImpl <em>Node Id</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.NodeIdImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getNodeId()
	 * @generated
	 */
	int NODE_ID = 11;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_ID__NODE_ID = BASE_DATA_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_ID__NAMESPACE_URI = BASE_DATA_TYPE__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>Node Id</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_ID_FEATURE_COUNT = BASE_DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Node Id</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_ID_OPERATION_COUNT = BASE_DATA_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.ExpandedNodeIdImpl <em>Expanded Node Id</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.ExpandedNodeIdImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getExpandedNodeId()
	 * @generated
	 */
	int EXPANDED_NODE_ID = 12;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPANDED_NODE_ID__NODE_ID = BASE_DATA_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPANDED_NODE_ID__NAMESPACE_URI = BASE_DATA_TYPE__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>Expanded Node Id</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPANDED_NODE_ID_FEATURE_COUNT = BASE_DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Expanded Node Id</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPANDED_NODE_ID_OPERATION_COUNT = BASE_DATA_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.StatusCodeImpl <em>Status Code</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.StatusCodeImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getStatusCode()
	 * @generated
	 */
	int STATUS_CODE = 13;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATUS_CODE__NODE_ID = BASE_DATA_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATUS_CODE__NAMESPACE_URI = BASE_DATA_TYPE__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>Status Code</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATUS_CODE_FEATURE_COUNT = BASE_DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Status Code</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATUS_CODE_OPERATION_COUNT = BASE_DATA_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.QualifiedNameImpl <em>Qualified Name</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.QualifiedNameImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getQualifiedName()
	 * @generated
	 */
	int QUALIFIED_NAME = 14;

	/**
	 * The number of structural features of the '<em>Qualified Name</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFIED_NAME_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Qualified Name</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFIED_NAME_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.StructureImpl <em>Structure</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.StructureImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getStructure()
	 * @generated
	 */
	int STRUCTURE = 15;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE__NODE_ID = BASE_DATA_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE__NAMESPACE_URI = BASE_DATA_TYPE__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>Structure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_FEATURE_COUNT = BASE_DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Structure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURE_OPERATION_COUNT = BASE_DATA_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.DataValueImpl <em>Data Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.DataValueImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getDataValue()
	 * @generated
	 */
	int DATA_VALUE = 16;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_VALUE__NODE_ID = BASE_DATA_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_VALUE__NAMESPACE_URI = BASE_DATA_TYPE__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>Data Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_VALUE_FEATURE_COUNT = BASE_DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Data Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_VALUE_OPERATION_COUNT = BASE_DATA_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.DiagnosticInfoImpl <em>Diagnostic Info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.DiagnosticInfoImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getDiagnosticInfo()
	 * @generated
	 */
	int DIAGNOSTIC_INFO = 17;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGNOSTIC_INFO__NODE_ID = BASE_DATA_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGNOSTIC_INFO__NAMESPACE_URI = BASE_DATA_TYPE__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>Diagnostic Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGNOSTIC_INFO_FEATURE_COUNT = BASE_DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Diagnostic Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGNOSTIC_INFO_OPERATION_COUNT = BASE_DATA_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.RsaEncryptedSecretImpl <em>Rsa Encrypted Secret</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.RsaEncryptedSecretImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getRsaEncryptedSecret()
	 * @generated
	 */
	int RSA_ENCRYPTED_SECRET = 18;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RSA_ENCRYPTED_SECRET__NODE_ID = BASE_DATA_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RSA_ENCRYPTED_SECRET__NAMESPACE_URI = BASE_DATA_TYPE__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>Rsa Encrypted Secret</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RSA_ENCRYPTED_SECRET_FEATURE_COUNT = BASE_DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Rsa Encrypted Secret</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RSA_ENCRYPTED_SECRET_OPERATION_COUNT = BASE_DATA_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.EccEncryptedSecretImpl <em>Ecc Encrypted Secret</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.EccEncryptedSecretImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getEccEncryptedSecret()
	 * @generated
	 */
	int ECC_ENCRYPTED_SECRET = 19;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECC_ENCRYPTED_SECRET__NODE_ID = BASE_DATA_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECC_ENCRYPTED_SECRET__NAMESPACE_URI = BASE_DATA_TYPE__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>Ecc Encrypted Secret</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECC_ENCRYPTED_SECRET_FEATURE_COUNT = BASE_DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ecc Encrypted Secret</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECC_ENCRYPTED_SECRET_OPERATION_COUNT = BASE_DATA_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.IntegerImpl <em>Integer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.IntegerImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getInteger()
	 * @generated
	 */
	int INTEGER = 20;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER__NODE_ID = NUMBER__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER__NAMESPACE_URI = NUMBER__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>Integer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_FEATURE_COUNT = NUMBER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Integer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_OPERATION_COUNT = NUMBER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.FloatImpl <em>Float</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.FloatImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getFloat()
	 * @generated
	 */
	int FLOAT = 21;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT__NODE_ID = NUMBER__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT__NAMESPACE_URI = NUMBER__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>Float</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_FEATURE_COUNT = NUMBER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Float</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_OPERATION_COUNT = NUMBER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.UIntegerImpl <em>UInteger</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.UIntegerImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getUInteger()
	 * @generated
	 */
	int UINTEGER = 22;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UINTEGER__NODE_ID = NUMBER__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UINTEGER__NAMESPACE_URI = NUMBER__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>UInteger</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UINTEGER_FEATURE_COUNT = NUMBER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>UInteger</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UINTEGER_OPERATION_COUNT = NUMBER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.DurationImpl <em>Duration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.DurationImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getDuration()
	 * @generated
	 */
	int DURATION = 23;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DURATION__NODE_ID = DOUBLE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DURATION__NAMESPACE_URI = DOUBLE__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>Duration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DURATION_FEATURE_COUNT = DOUBLE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Duration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DURATION_OPERATION_COUNT = DOUBLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.Int64Impl <em>Int64</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.Int64Impl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getInt64()
	 * @generated
	 */
	int INT64 = 24;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT64__NODE_ID = INTEGER__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT64__NAMESPACE_URI = INTEGER__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>Int64</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT64_FEATURE_COUNT = INTEGER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Int64</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT64_OPERATION_COUNT = INTEGER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.Int32Impl <em>Int32</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.Int32Impl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getInt32()
	 * @generated
	 */
	int INT32 = 25;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT32__NODE_ID = INTEGER__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT32__NAMESPACE_URI = INTEGER__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>Int32</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT32_FEATURE_COUNT = INTEGER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Int32</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT32_OPERATION_COUNT = INTEGER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.Int16Impl <em>Int16</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.Int16Impl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getInt16()
	 * @generated
	 */
	int INT16 = 26;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT16__NODE_ID = INTEGER__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT16__NAMESPACE_URI = INTEGER__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>Int16</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT16_FEATURE_COUNT = INTEGER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Int16</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT16_OPERATION_COUNT = INTEGER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.SByteImpl <em>SByte</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.SByteImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getSByte()
	 * @generated
	 */
	int SBYTE = 27;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SBYTE__NODE_ID = INTEGER__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SBYTE__NAMESPACE_URI = INTEGER__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>SByte</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SBYTE_FEATURE_COUNT = INTEGER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>SByte</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SBYTE_OPERATION_COUNT = INTEGER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.ByteImpl <em>Byte</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.ByteImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getByte()
	 * @generated
	 */
	int BYTE = 28;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BYTE__NODE_ID = UINTEGER__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BYTE__NAMESPACE_URI = UINTEGER__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>Byte</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BYTE_FEATURE_COUNT = UINTEGER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Byte</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BYTE_OPERATION_COUNT = UINTEGER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.ArgumentImpl <em>Argument</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.ArgumentImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getArgument()
	 * @generated
	 */
	int ARGUMENT = 29;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT__NODE_ID = STRUCTURE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT__NAMESPACE_URI = STRUCTURE__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>Argument</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_FEATURE_COUNT = STRUCTURE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Argument</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_OPERATION_COUNT = STRUCTURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.UInt16Impl <em>UInt16</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.UInt16Impl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getUInt16()
	 * @generated
	 */
	int UINT16 = 30;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UINT16__NODE_ID = UINTEGER__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UINT16__NAMESPACE_URI = UINTEGER__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>UInt16</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UINT16_FEATURE_COUNT = UINTEGER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>UInt16</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UINT16_OPERATION_COUNT = UINTEGER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.UInt32Impl <em>UInt32</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.UInt32Impl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getUInt32()
	 * @generated
	 */
	int UINT32 = 31;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UINT32__NODE_ID = UINTEGER__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UINT32__NAMESPACE_URI = UINTEGER__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>UInt32</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UINT32_FEATURE_COUNT = UINTEGER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>UInt32</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UINT32_OPERATION_COUNT = UINTEGER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.UInt64Impl <em>UInt64</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.UInt64Impl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getUInt64()
	 * @generated
	 */
	int UINT64 = 32;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UINT64__NODE_ID = UINTEGER__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UINT64__NAMESPACE_URI = UINTEGER__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>UInt64</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UINT64_FEATURE_COUNT = UINTEGER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>UInt64</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UINT64_OPERATION_COUNT = UINTEGER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.ImageImpl <em>Image</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.ImageImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getImage()
	 * @generated
	 */
	int IMAGE = 33;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE__NODE_ID = BYTE_STRING__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE__NAMESPACE_URI = BYTE_STRING__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>Image</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE_FEATURE_COUNT = BYTE_STRING_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Image</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE_OPERATION_COUNT = BYTE_STRING_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.GIFImpl <em>GIF</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.GIFImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getGIF()
	 * @generated
	 */
	int GIF = 34;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GIF__NODE_ID = IMAGE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GIF__NAMESPACE_URI = IMAGE__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>GIF</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GIF_FEATURE_COUNT = IMAGE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>GIF</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GIF_OPERATION_COUNT = IMAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.BMPImpl <em>BMP</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.BMPImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getBMP()
	 * @generated
	 */
	int BMP = 35;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BMP__NODE_ID = IMAGE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BMP__NAMESPACE_URI = IMAGE__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>BMP</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BMP_FEATURE_COUNT = IMAGE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>BMP</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BMP_OPERATION_COUNT = IMAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.PNGImpl <em>PNG</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.PNGImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getPNG()
	 * @generated
	 */
	int PNG = 36;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PNG__NODE_ID = IMAGE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PNG__NAMESPACE_URI = IMAGE__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>PNG</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PNG_FEATURE_COUNT = IMAGE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>PNG</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PNG_OPERATION_COUNT = IMAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.JPGImpl <em>JPG</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.JPGImpl
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getJPG()
	 * @generated
	 */
	int JPG = 37;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JPG__NODE_ID = IMAGE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JPG__NAMESPACE_URI = IMAGE__NAMESPACE_URI;

	/**
	 * The number of structural features of the '<em>JPG</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JPG_FEATURE_COUNT = IMAGE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>JPG</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JPG_OPERATION_COUNT = IMAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.NodeCLass <em>Node CLass</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.NodeCLass
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getNodeCLass()
	 * @generated
	 */
	int NODE_CLASS = 38;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.IdType <em>Id Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.IdType
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getIdType()
	 * @generated
	 */
	int ID_TYPE = 39;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.NamingRuleType <em>Naming Rule Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.NamingRuleType
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getNamingRuleType()
	 * @generated
	 */
	int NAMING_RULE_TYPE = 40;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.SecurityTokenRequestType <em>Security Token Request Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.SecurityTokenRequestType
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getSecurityTokenRequestType()
	 * @generated
	 */
	int SECURITY_TOKEN_REQUEST_TYPE = 41;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.RedundancySupport <em>Redundancy Support</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.RedundancySupport
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getRedundancySupport()
	 * @generated
	 */
	int REDUNDANCY_SUPPORT = 42;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.MessageSecurityMode <em>Message Security Mode</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.MessageSecurityMode
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getMessageSecurityMode()
	 * @generated
	 */
	int MESSAGE_SECURITY_MODE = 43;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.ServerState <em>Server State</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.ServerState
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getServerState()
	 * @generated
	 */
	int SERVER_STATE = 44;


	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.AnalogUnitType <em>Analog Unit Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Analog Unit Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.AnalogUnitType
	 * @generated
	 */
	EClass getAnalogUnitType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Double <em>Double</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Double</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Double
	 * @generated
	 */
	EClass getDouble();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Number <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Number</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Number
	 * @generated
	 */
	EClass getNumber();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.BaseDataType <em>Base Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Data Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.BaseDataType
	 * @generated
	 */
	EClass getBaseDataType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.BaseDataType#getNodeId <em>Node Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Node Id</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.BaseDataType#getNodeId()
	 * @see #getBaseDataType()
	 * @generated
	 */
	EAttribute getBaseDataType_NodeId();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.BaseDataType#getNamespaceUri <em>Namespace Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Namespace Uri</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.BaseDataType#getNamespaceUri()
	 * @see #getBaseDataType()
	 * @generated
	 */
	EAttribute getBaseDataType_NamespaceUri();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.LocalizedText <em>Localized Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Localized Text</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.LocalizedText
	 * @generated
	 */
	EClass getLocalizedText();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.LocalizedText#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.LocalizedText#getValue()
	 * @see #getLocalizedText()
	 * @generated
	 */
	EAttribute getLocalizedText_Value();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.LocalizedText#getLocal <em>Local</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Local</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.LocalizedText#getLocal()
	 * @see #getLocalizedText()
	 * @generated
	 */
	EReference getLocalizedText_Local();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.LocalId <em>Local Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Local Id</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.LocalId
	 * @generated
	 */
	EClass getLocalId();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.String <em>String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.String
	 * @generated
	 */
	EClass getString();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.DateTime <em>Date Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Date Time</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.DateTime
	 * @generated
	 */
	EClass getDateTime();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Guid <em>Guid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Guid</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Guid
	 * @generated
	 */
	EClass getGuid();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.ByteString <em>Byte String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Byte String</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.ByteString
	 * @generated
	 */
	EClass getByteString();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.XmlElement <em>Xml Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Xml Element</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.XmlElement
	 * @generated
	 */
	EClass getXmlElement();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.NodeId <em>Node Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Node Id</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.NodeId
	 * @generated
	 */
	EClass getNodeId();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.ExpandedNodeId <em>Expanded Node Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expanded Node Id</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.ExpandedNodeId
	 * @generated
	 */
	EClass getExpandedNodeId();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.StatusCode <em>Status Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Status Code</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.StatusCode
	 * @generated
	 */
	EClass getStatusCode();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.QualifiedName <em>Qualified Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Qualified Name</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.QualifiedName
	 * @generated
	 */
	EClass getQualifiedName();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Structure <em>Structure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Structure</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Structure
	 * @generated
	 */
	EClass getStructure();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.DataValue <em>Data Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Value</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.DataValue
	 * @generated
	 */
	EClass getDataValue();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.DiagnosticInfo <em>Diagnostic Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Diagnostic Info</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.DiagnosticInfo
	 * @generated
	 */
	EClass getDiagnosticInfo();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.RsaEncryptedSecret <em>Rsa Encrypted Secret</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rsa Encrypted Secret</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.RsaEncryptedSecret
	 * @generated
	 */
	EClass getRsaEncryptedSecret();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.EccEncryptedSecret <em>Ecc Encrypted Secret</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ecc Encrypted Secret</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.EccEncryptedSecret
	 * @generated
	 */
	EClass getEccEncryptedSecret();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Integer <em>Integer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Integer
	 * @generated
	 */
	EClass getInteger();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Float <em>Float</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Float</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Float
	 * @generated
	 */
	EClass getFloat();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.UInteger <em>UInteger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UInteger</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.UInteger
	 * @generated
	 */
	EClass getUInteger();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Duration <em>Duration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Duration</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Duration
	 * @generated
	 */
	EClass getDuration();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int64 <em>Int64</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int64</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int64
	 * @generated
	 */
	EClass getInt64();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int32 <em>Int32</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int32</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int32
	 * @generated
	 */
	EClass getInt32();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int16 <em>Int16</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int16</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int16
	 * @generated
	 */
	EClass getInt16();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.SByte <em>SByte</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SByte</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.SByte
	 * @generated
	 */
	EClass getSByte();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Byte <em>Byte</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Byte</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Byte
	 * @generated
	 */
	EClass getByte();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Argument <em>Argument</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Argument</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Argument
	 * @generated
	 */
	EClass getArgument();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.UInt16 <em>UInt16</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UInt16</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.UInt16
	 * @generated
	 */
	EClass getUInt16();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.UInt32 <em>UInt32</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UInt32</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.UInt32
	 * @generated
	 */
	EClass getUInt32();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.UInt64 <em>UInt64</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UInt64</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.UInt64
	 * @generated
	 */
	EClass getUInt64();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Image <em>Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Image</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Image
	 * @generated
	 */
	EClass getImage();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.GIF <em>GIF</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>GIF</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.GIF
	 * @generated
	 */
	EClass getGIF();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.BMP <em>BMP</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BMP</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.BMP
	 * @generated
	 */
	EClass getBMP();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.PNG <em>PNG</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>PNG</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.PNG
	 * @generated
	 */
	EClass getPNG();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.JPG <em>JPG</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>JPG</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.JPG
	 * @generated
	 */
	EClass getJPG();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.NodeCLass <em>Node CLass</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Node CLass</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.NodeCLass
	 * @generated
	 */
	EEnum getNodeCLass();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.IdType <em>Id Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Id Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.IdType
	 * @generated
	 */
	EEnum getIdType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.NamingRuleType <em>Naming Rule Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Naming Rule Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.NamingRuleType
	 * @generated
	 */
	EEnum getNamingRuleType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.SecurityTokenRequestType <em>Security Token Request Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Security Token Request Type</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.SecurityTokenRequestType
	 * @generated
	 */
	EEnum getSecurityTokenRequestType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.RedundancySupport <em>Redundancy Support</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Redundancy Support</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.RedundancySupport
	 * @generated
	 */
	EEnum getRedundancySupport();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.MessageSecurityMode <em>Message Security Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Message Security Mode</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.MessageSecurityMode
	 * @generated
	 */
	EEnum getMessageSecurityMode();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.ServerState <em>Server State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Server State</em>'.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.ServerState
	 * @generated
	 */
	EEnum getServerState();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OPC_UA_LibraryFactory getOPC_UA_LibraryFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.AnalogUnitTypeImpl <em>Analog Unit Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.AnalogUnitTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getAnalogUnitType()
		 * @generated
		 */
		EClass ANALOG_UNIT_TYPE = eINSTANCE.getAnalogUnitType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.DoubleImpl <em>Double</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.DoubleImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getDouble()
		 * @generated
		 */
		EClass DOUBLE = eINSTANCE.getDouble();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.NumberImpl <em>Number</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.NumberImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getNumber()
		 * @generated
		 */
		EClass NUMBER = eINSTANCE.getNumber();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.BaseDataTypeImpl <em>Base Data Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.BaseDataTypeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getBaseDataType()
		 * @generated
		 */
		EClass BASE_DATA_TYPE = eINSTANCE.getBaseDataType();

		/**
		 * The meta object literal for the '<em><b>Node Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_DATA_TYPE__NODE_ID = eINSTANCE.getBaseDataType_NodeId();

		/**
		 * The meta object literal for the '<em><b>Namespace Uri</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_DATA_TYPE__NAMESPACE_URI = eINSTANCE.getBaseDataType_NamespaceUri();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.LocalizedTextImpl <em>Localized Text</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.LocalizedTextImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getLocalizedText()
		 * @generated
		 */
		EClass LOCALIZED_TEXT = eINSTANCE.getLocalizedText();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOCALIZED_TEXT__VALUE = eINSTANCE.getLocalizedText_Value();

		/**
		 * The meta object literal for the '<em><b>Local</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOCALIZED_TEXT__LOCAL = eINSTANCE.getLocalizedText_Local();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.LocalIdImpl <em>Local Id</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.LocalIdImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getLocalId()
		 * @generated
		 */
		EClass LOCAL_ID = eINSTANCE.getLocalId();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.StringImpl <em>String</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.StringImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getString()
		 * @generated
		 */
		EClass STRING = eINSTANCE.getString();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.DateTimeImpl <em>Date Time</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.DateTimeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getDateTime()
		 * @generated
		 */
		EClass DATE_TIME = eINSTANCE.getDateTime();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.GuidImpl <em>Guid</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.GuidImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getGuid()
		 * @generated
		 */
		EClass GUID = eINSTANCE.getGuid();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.ByteStringImpl <em>Byte String</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.ByteStringImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getByteString()
		 * @generated
		 */
		EClass BYTE_STRING = eINSTANCE.getByteString();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.XmlElementImpl <em>Xml Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.XmlElementImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getXmlElement()
		 * @generated
		 */
		EClass XML_ELEMENT = eINSTANCE.getXmlElement();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.NodeIdImpl <em>Node Id</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.NodeIdImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getNodeId()
		 * @generated
		 */
		EClass NODE_ID = eINSTANCE.getNodeId();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.ExpandedNodeIdImpl <em>Expanded Node Id</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.ExpandedNodeIdImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getExpandedNodeId()
		 * @generated
		 */
		EClass EXPANDED_NODE_ID = eINSTANCE.getExpandedNodeId();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.StatusCodeImpl <em>Status Code</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.StatusCodeImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getStatusCode()
		 * @generated
		 */
		EClass STATUS_CODE = eINSTANCE.getStatusCode();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.QualifiedNameImpl <em>Qualified Name</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.QualifiedNameImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getQualifiedName()
		 * @generated
		 */
		EClass QUALIFIED_NAME = eINSTANCE.getQualifiedName();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.StructureImpl <em>Structure</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.StructureImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getStructure()
		 * @generated
		 */
		EClass STRUCTURE = eINSTANCE.getStructure();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.DataValueImpl <em>Data Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.DataValueImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getDataValue()
		 * @generated
		 */
		EClass DATA_VALUE = eINSTANCE.getDataValue();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.DiagnosticInfoImpl <em>Diagnostic Info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.DiagnosticInfoImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getDiagnosticInfo()
		 * @generated
		 */
		EClass DIAGNOSTIC_INFO = eINSTANCE.getDiagnosticInfo();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.RsaEncryptedSecretImpl <em>Rsa Encrypted Secret</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.RsaEncryptedSecretImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getRsaEncryptedSecret()
		 * @generated
		 */
		EClass RSA_ENCRYPTED_SECRET = eINSTANCE.getRsaEncryptedSecret();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.EccEncryptedSecretImpl <em>Ecc Encrypted Secret</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.EccEncryptedSecretImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getEccEncryptedSecret()
		 * @generated
		 */
		EClass ECC_ENCRYPTED_SECRET = eINSTANCE.getEccEncryptedSecret();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.IntegerImpl <em>Integer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.IntegerImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getInteger()
		 * @generated
		 */
		EClass INTEGER = eINSTANCE.getInteger();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.FloatImpl <em>Float</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.FloatImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getFloat()
		 * @generated
		 */
		EClass FLOAT = eINSTANCE.getFloat();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.UIntegerImpl <em>UInteger</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.UIntegerImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getUInteger()
		 * @generated
		 */
		EClass UINTEGER = eINSTANCE.getUInteger();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.DurationImpl <em>Duration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.DurationImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getDuration()
		 * @generated
		 */
		EClass DURATION = eINSTANCE.getDuration();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.Int64Impl <em>Int64</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.Int64Impl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getInt64()
		 * @generated
		 */
		EClass INT64 = eINSTANCE.getInt64();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.Int32Impl <em>Int32</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.Int32Impl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getInt32()
		 * @generated
		 */
		EClass INT32 = eINSTANCE.getInt32();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.Int16Impl <em>Int16</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.Int16Impl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getInt16()
		 * @generated
		 */
		EClass INT16 = eINSTANCE.getInt16();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.SByteImpl <em>SByte</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.SByteImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getSByte()
		 * @generated
		 */
		EClass SBYTE = eINSTANCE.getSByte();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.ByteImpl <em>Byte</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.ByteImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getByte()
		 * @generated
		 */
		EClass BYTE = eINSTANCE.getByte();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.ArgumentImpl <em>Argument</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.ArgumentImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getArgument()
		 * @generated
		 */
		EClass ARGUMENT = eINSTANCE.getArgument();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.UInt16Impl <em>UInt16</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.UInt16Impl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getUInt16()
		 * @generated
		 */
		EClass UINT16 = eINSTANCE.getUInt16();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.UInt32Impl <em>UInt32</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.UInt32Impl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getUInt32()
		 * @generated
		 */
		EClass UINT32 = eINSTANCE.getUInt32();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.UInt64Impl <em>UInt64</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.UInt64Impl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getUInt64()
		 * @generated
		 */
		EClass UINT64 = eINSTANCE.getUInt64();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.ImageImpl <em>Image</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.ImageImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getImage()
		 * @generated
		 */
		EClass IMAGE = eINSTANCE.getImage();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.GIFImpl <em>GIF</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.GIFImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getGIF()
		 * @generated
		 */
		EClass GIF = eINSTANCE.getGIF();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.BMPImpl <em>BMP</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.BMPImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getBMP()
		 * @generated
		 */
		EClass BMP = eINSTANCE.getBMP();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.PNGImpl <em>PNG</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.PNGImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getPNG()
		 * @generated
		 */
		EClass PNG = eINSTANCE.getPNG();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.JPGImpl <em>JPG</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.JPGImpl
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getJPG()
		 * @generated
		 */
		EClass JPG = eINSTANCE.getJPG();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.NodeCLass <em>Node CLass</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.NodeCLass
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getNodeCLass()
		 * @generated
		 */
		EEnum NODE_CLASS = eINSTANCE.getNodeCLass();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.IdType <em>Id Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.IdType
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getIdType()
		 * @generated
		 */
		EEnum ID_TYPE = eINSTANCE.getIdType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.NamingRuleType <em>Naming Rule Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.NamingRuleType
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getNamingRuleType()
		 * @generated
		 */
		EEnum NAMING_RULE_TYPE = eINSTANCE.getNamingRuleType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.SecurityTokenRequestType <em>Security Token Request Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.SecurityTokenRequestType
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getSecurityTokenRequestType()
		 * @generated
		 */
		EEnum SECURITY_TOKEN_REQUEST_TYPE = eINSTANCE.getSecurityTokenRequestType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.RedundancySupport <em>Redundancy Support</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.RedundancySupport
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getRedundancySupport()
		 * @generated
		 */
		EEnum REDUNDANCY_SUPPORT = eINSTANCE.getRedundancySupport();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.MessageSecurityMode <em>Message Security Mode</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.MessageSecurityMode
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getMessageSecurityMode()
		 * @generated
		 */
		EEnum MESSAGE_SECURITY_MODE = eINSTANCE.getMessageSecurityMode();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.ServerState <em>Server State</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.ServerState
		 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl#getServerState()
		 * @generated
		 */
		EEnum SERVER_STATE = eINSTANCE.getServerState();

	}

} //OPC_UA_LibraryPackage
