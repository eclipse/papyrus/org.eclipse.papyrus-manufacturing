/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.di.opcuadiprofile;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.papyrus.opcua.opcuaprofile.OPCUAProfilePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfileFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='OPC_UA_DI'"
 * @generated
 */
public interface OPCUADIProfilePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "opcuadiprofile";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://Papyrus/OPCUADI";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "OPCUADIProfile";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OPCUADIProfilePackage eINSTANCE = org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.IVendorNameplateTypeImpl <em>IVendor Nameplate Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.IVendorNameplateTypeImpl
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getIVendorNameplateType()
	 * @generated
	 */
	int IVENDOR_NAMEPLATE_TYPE = 6;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVENDOR_NAMEPLATE_TYPE__BASE_CLASS = OPCUAProfilePackage.BASE_INTERFACE_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVENDOR_NAMEPLATE_TYPE__NODE_ID = OPCUAProfilePackage.BASE_INTERFACE_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVENDOR_NAMEPLATE_TYPE__NAMESPACE_URI = OPCUAProfilePackage.BASE_INTERFACE_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVENDOR_NAMEPLATE_TYPE__BROWSE_NAME = OPCUAProfilePackage.BASE_INTERFACE_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVENDOR_NAMEPLATE_TYPE__NODE_CLASS = OPCUAProfilePackage.BASE_INTERFACE_TYPE__NODE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVENDOR_NAMEPLATE_TYPE__BASE_INTERFACE = OPCUAProfilePackage.BASE_INTERFACE_TYPE__BASE_INTERFACE;

	/**
	 * The feature id for the '<em><b>Manufacturer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVENDOR_NAMEPLATE_TYPE__MANUFACTURER = OPCUAProfilePackage.BASE_INTERFACE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Manufacturer Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVENDOR_NAMEPLATE_TYPE__MANUFACTURER_URI = OPCUAProfilePackage.BASE_INTERFACE_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVENDOR_NAMEPLATE_TYPE__MODEL = OPCUAProfilePackage.BASE_INTERFACE_TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Product Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVENDOR_NAMEPLATE_TYPE__PRODUCT_CODE = OPCUAProfilePackage.BASE_INTERFACE_TYPE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Hardware Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVENDOR_NAMEPLATE_TYPE__HARDWARE_REVISION = OPCUAProfilePackage.BASE_INTERFACE_TYPE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Software Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVENDOR_NAMEPLATE_TYPE__SOFTWARE_REVISION = OPCUAProfilePackage.BASE_INTERFACE_TYPE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Device Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVENDOR_NAMEPLATE_TYPE__DEVICE_REVISION = OPCUAProfilePackage.BASE_INTERFACE_TYPE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Device Manual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVENDOR_NAMEPLATE_TYPE__DEVICE_MANUAL = OPCUAProfilePackage.BASE_INTERFACE_TYPE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Device Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVENDOR_NAMEPLATE_TYPE__DEVICE_CLASS = OPCUAProfilePackage.BASE_INTERFACE_TYPE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Serial Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVENDOR_NAMEPLATE_TYPE__SERIAL_NUMBER = OPCUAProfilePackage.BASE_INTERFACE_TYPE_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Product Instance Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVENDOR_NAMEPLATE_TYPE__PRODUCT_INSTANCE_URI = OPCUAProfilePackage.BASE_INTERFACE_TYPE_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Revision Counter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVENDOR_NAMEPLATE_TYPE__REVISION_COUNTER = OPCUAProfilePackage.BASE_INTERFACE_TYPE_FEATURE_COUNT + 11;

	/**
	 * The number of structural features of the '<em>IVendor Nameplate Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVENDOR_NAMEPLATE_TYPE_FEATURE_COUNT = OPCUAProfilePackage.BASE_INTERFACE_TYPE_FEATURE_COUNT + 12;

	/**
	 * The number of operations of the '<em>IVendor Nameplate Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVENDOR_NAMEPLATE_TYPE_OPERATION_COUNT = OPCUAProfilePackage.BASE_INTERFACE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ComponentTypeImpl <em>Component Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ComponentTypeImpl
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getComponentType()
	 * @generated
	 */
	int COMPONENT_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__BASE_CLASS = IVENDOR_NAMEPLATE_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__NODE_ID = IVENDOR_NAMEPLATE_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__NAMESPACE_URI = IVENDOR_NAMEPLATE_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__BROWSE_NAME = IVENDOR_NAMEPLATE_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__NODE_CLASS = IVENDOR_NAMEPLATE_TYPE__NODE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__BASE_INTERFACE = IVENDOR_NAMEPLATE_TYPE__BASE_INTERFACE;

	/**
	 * The feature id for the '<em><b>Manufacturer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__MANUFACTURER = IVENDOR_NAMEPLATE_TYPE__MANUFACTURER;

	/**
	 * The feature id for the '<em><b>Manufacturer Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__MANUFACTURER_URI = IVENDOR_NAMEPLATE_TYPE__MANUFACTURER_URI;

	/**
	 * The feature id for the '<em><b>Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__MODEL = IVENDOR_NAMEPLATE_TYPE__MODEL;

	/**
	 * The feature id for the '<em><b>Product Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__PRODUCT_CODE = IVENDOR_NAMEPLATE_TYPE__PRODUCT_CODE;

	/**
	 * The feature id for the '<em><b>Hardware Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__HARDWARE_REVISION = IVENDOR_NAMEPLATE_TYPE__HARDWARE_REVISION;

	/**
	 * The feature id for the '<em><b>Software Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__SOFTWARE_REVISION = IVENDOR_NAMEPLATE_TYPE__SOFTWARE_REVISION;

	/**
	 * The feature id for the '<em><b>Device Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__DEVICE_REVISION = IVENDOR_NAMEPLATE_TYPE__DEVICE_REVISION;

	/**
	 * The feature id for the '<em><b>Device Manual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__DEVICE_MANUAL = IVENDOR_NAMEPLATE_TYPE__DEVICE_MANUAL;

	/**
	 * The feature id for the '<em><b>Device Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__DEVICE_CLASS = IVENDOR_NAMEPLATE_TYPE__DEVICE_CLASS;

	/**
	 * The feature id for the '<em><b>Serial Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__SERIAL_NUMBER = IVENDOR_NAMEPLATE_TYPE__SERIAL_NUMBER;

	/**
	 * The feature id for the '<em><b>Product Instance Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__PRODUCT_INSTANCE_URI = IVENDOR_NAMEPLATE_TYPE__PRODUCT_INSTANCE_URI;

	/**
	 * The feature id for the '<em><b>Revision Counter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__REVISION_COUNTER = IVENDOR_NAMEPLATE_TYPE__REVISION_COUNTER;

	/**
	 * The feature id for the '<em><b>Group Identifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__GROUP_IDENTIFIER = IVENDOR_NAMEPLATE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Identification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__IDENTIFICATION = IVENDOR_NAMEPLATE_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Lock</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__LOCK = IVENDOR_NAMEPLATE_TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Parameter Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__PARAMETER_SET = IVENDOR_NAMEPLATE_TYPE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Method Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__METHOD_SET = IVENDOR_NAMEPLATE_TYPE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Asset Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__ASSET_ID = IVENDOR_NAMEPLATE_TYPE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Component Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__COMPONENT_NAME = IVENDOR_NAMEPLATE_TYPE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__BASE_PROPERTY = IVENDOR_NAMEPLATE_TYPE_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Component Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE_FEATURE_COUNT = IVENDOR_NAMEPLATE_TYPE_FEATURE_COUNT + 8;

	/**
	 * The number of operations of the '<em>Component Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE_OPERATION_COUNT = IVENDOR_NAMEPLATE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.TopologyElementTypeImpl <em>Topology Element Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.TopologyElementTypeImpl
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getTopologyElementType()
	 * @generated
	 */
	int TOPOLOGY_ELEMENT_TYPE = 1;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOPOLOGY_ELEMENT_TYPE__BASE_CLASS = OPCUAProfilePackage.BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOPOLOGY_ELEMENT_TYPE__NODE_ID = OPCUAProfilePackage.BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOPOLOGY_ELEMENT_TYPE__NAMESPACE_URI = OPCUAProfilePackage.BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOPOLOGY_ELEMENT_TYPE__BROWSE_NAME = OPCUAProfilePackage.BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOPOLOGY_ELEMENT_TYPE__NODE_CLASS = OPCUAProfilePackage.BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The feature id for the '<em><b>Group Identifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOPOLOGY_ELEMENT_TYPE__GROUP_IDENTIFIER = OPCUAProfilePackage.BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Identification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOPOLOGY_ELEMENT_TYPE__IDENTIFICATION = OPCUAProfilePackage.BASE_OBJECT_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Lock</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOPOLOGY_ELEMENT_TYPE__LOCK = OPCUAProfilePackage.BASE_OBJECT_TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Parameter Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOPOLOGY_ELEMENT_TYPE__PARAMETER_SET = OPCUAProfilePackage.BASE_OBJECT_TYPE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Method Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOPOLOGY_ELEMENT_TYPE__METHOD_SET = OPCUAProfilePackage.BASE_OBJECT_TYPE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Topology Element Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOPOLOGY_ELEMENT_TYPE_FEATURE_COUNT = OPCUAProfilePackage.BASE_OBJECT_TYPE_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Topology Element Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOPOLOGY_ELEMENT_TYPE_OPERATION_COUNT = OPCUAProfilePackage.BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.FunctionalGroupTypeImpl <em>Functional Group Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.FunctionalGroupTypeImpl
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getFunctionalGroupType()
	 * @generated
	 */
	int FUNCTIONAL_GROUP_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_GROUP_TYPE__BASE_CLASS = OPCUAProfilePackage.FOLDER_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_GROUP_TYPE__NODE_ID = OPCUAProfilePackage.FOLDER_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_GROUP_TYPE__NAMESPACE_URI = OPCUAProfilePackage.FOLDER_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_GROUP_TYPE__BROWSE_NAME = OPCUAProfilePackage.FOLDER_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_GROUP_TYPE__NODE_CLASS = OPCUAProfilePackage.FOLDER_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Functional Group Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_GROUP_TYPE_FEATURE_COUNT = OPCUAProfilePackage.FOLDER_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Functional Group Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_GROUP_TYPE_OPERATION_COUNT = OPCUAProfilePackage.FOLDER_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.LockingServicesTypeImpl <em>Locking Services Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.LockingServicesTypeImpl
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getLockingServicesType()
	 * @generated
	 */
	int LOCKING_SERVICES_TYPE = 3;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCKING_SERVICES_TYPE__BASE_CLASS = OPCUAProfilePackage.BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCKING_SERVICES_TYPE__NODE_ID = OPCUAProfilePackage.BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCKING_SERVICES_TYPE__NAMESPACE_URI = OPCUAProfilePackage.BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCKING_SERVICES_TYPE__BROWSE_NAME = OPCUAProfilePackage.BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCKING_SERVICES_TYPE__NODE_CLASS = OPCUAProfilePackage.BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The feature id for the '<em><b>Default Instance Browse Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCKING_SERVICES_TYPE__DEFAULT_INSTANCE_BROWSE_NAME = OPCUAProfilePackage.BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Locked</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCKING_SERVICES_TYPE__LOCKED = OPCUAProfilePackage.BASE_OBJECT_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Locking Client</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCKING_SERVICES_TYPE__LOCKING_CLIENT = OPCUAProfilePackage.BASE_OBJECT_TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Locking User</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCKING_SERVICES_TYPE__LOCKING_USER = OPCUAProfilePackage.BASE_OBJECT_TYPE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Remaining Lock Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCKING_SERVICES_TYPE__REMAINING_LOCK_TIME = OPCUAProfilePackage.BASE_OBJECT_TYPE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Locking Services Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCKING_SERVICES_TYPE_FEATURE_COUNT = OPCUAProfilePackage.BASE_OBJECT_TYPE_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Init Lock</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCKING_SERVICES_TYPE___INIT_LOCK__STRING_INT32 = OPCUAProfilePackage.BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Renew Lock</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCKING_SERVICES_TYPE___RENEW_LOCK__INT32 = OPCUAProfilePackage.BASE_OBJECT_TYPE_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Exit Lock</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCKING_SERVICES_TYPE___EXIT_LOCK__INT32 = OPCUAProfilePackage.BASE_OBJECT_TYPE_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Break Lock</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCKING_SERVICES_TYPE___BREAK_LOCK__INT32 = OPCUAProfilePackage.BASE_OBJECT_TYPE_OPERATION_COUNT + 3;

	/**
	 * The number of operations of the '<em>Locking Services Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCKING_SERVICES_TYPE_OPERATION_COUNT = OPCUAProfilePackage.BASE_OBJECT_TYPE_OPERATION_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ParameterSetTopologyElementTypeImpl <em>Parameter Set Topology Element Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ParameterSetTopologyElementTypeImpl
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getParameterSetTopologyElementType()
	 * @generated
	 */
	int PARAMETER_SET_TOPOLOGY_ELEMENT_TYPE = 4;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_TOPOLOGY_ELEMENT_TYPE__BASE_CLASS = OPCUAProfilePackage.BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_TOPOLOGY_ELEMENT_TYPE__NODE_ID = OPCUAProfilePackage.BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_TOPOLOGY_ELEMENT_TYPE__NAMESPACE_URI = OPCUAProfilePackage.BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_TOPOLOGY_ELEMENT_TYPE__BROWSE_NAME = OPCUAProfilePackage.BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_TOPOLOGY_ELEMENT_TYPE__NODE_CLASS = OPCUAProfilePackage.BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The feature id for the '<em><b>Parameter Identifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_TOPOLOGY_ELEMENT_TYPE__PARAMETER_IDENTIFIER = OPCUAProfilePackage.BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Parameter Set Topology Element Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_TOPOLOGY_ELEMENT_TYPE_FEATURE_COUNT = OPCUAProfilePackage.BASE_OBJECT_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Parameter Set Topology Element Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_TOPOLOGY_ELEMENT_TYPE_OPERATION_COUNT = OPCUAProfilePackage.BASE_OBJECT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.MethodSetTopologyElementTypeImpl <em>Method Set Topology Element Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.MethodSetTopologyElementTypeImpl
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getMethodSetTopologyElementType()
	 * @generated
	 */
	int METHOD_SET_TOPOLOGY_ELEMENT_TYPE = 5;

	/**
	 * The number of structural features of the '<em>Method Set Topology Element Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_SET_TOPOLOGY_ELEMENT_TYPE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Method Set Topology Element Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_SET_TOPOLOGY_ELEMENT_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ITagNameplateTypeImpl <em>ITag Nameplate Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ITagNameplateTypeImpl
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getITagNameplateType()
	 * @generated
	 */
	int ITAG_NAMEPLATE_TYPE = 7;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITAG_NAMEPLATE_TYPE__BASE_CLASS = OPCUAProfilePackage.BASE_INTERFACE_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITAG_NAMEPLATE_TYPE__NODE_ID = OPCUAProfilePackage.BASE_INTERFACE_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITAG_NAMEPLATE_TYPE__NAMESPACE_URI = OPCUAProfilePackage.BASE_INTERFACE_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITAG_NAMEPLATE_TYPE__BROWSE_NAME = OPCUAProfilePackage.BASE_INTERFACE_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITAG_NAMEPLATE_TYPE__NODE_CLASS = OPCUAProfilePackage.BASE_INTERFACE_TYPE__NODE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITAG_NAMEPLATE_TYPE__BASE_INTERFACE = OPCUAProfilePackage.BASE_INTERFACE_TYPE__BASE_INTERFACE;

	/**
	 * The feature id for the '<em><b>Asset Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITAG_NAMEPLATE_TYPE__ASSET_ID = OPCUAProfilePackage.BASE_INTERFACE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Component Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITAG_NAMEPLATE_TYPE__COMPONENT_NAME = OPCUAProfilePackage.BASE_INTERFACE_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>ITag Nameplate Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITAG_NAMEPLATE_TYPE_FEATURE_COUNT = OPCUAProfilePackage.BASE_INTERFACE_TYPE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>ITag Nameplate Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITAG_NAMEPLATE_TYPE_OPERATION_COUNT = OPCUAProfilePackage.BASE_INTERFACE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.DeviceTypeImpl <em>Device Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.DeviceTypeImpl
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getDeviceType()
	 * @generated
	 */
	int DEVICE_TYPE = 8;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__BASE_CLASS = COMPONENT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__NODE_ID = COMPONENT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__NAMESPACE_URI = COMPONENT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__BROWSE_NAME = COMPONENT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__NODE_CLASS = COMPONENT_TYPE__NODE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__BASE_INTERFACE = COMPONENT_TYPE__BASE_INTERFACE;

	/**
	 * The feature id for the '<em><b>Manufacturer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__MANUFACTURER = COMPONENT_TYPE__MANUFACTURER;

	/**
	 * The feature id for the '<em><b>Manufacturer Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__MANUFACTURER_URI = COMPONENT_TYPE__MANUFACTURER_URI;

	/**
	 * The feature id for the '<em><b>Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__MODEL = COMPONENT_TYPE__MODEL;

	/**
	 * The feature id for the '<em><b>Product Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__PRODUCT_CODE = COMPONENT_TYPE__PRODUCT_CODE;

	/**
	 * The feature id for the '<em><b>Hardware Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__HARDWARE_REVISION = COMPONENT_TYPE__HARDWARE_REVISION;

	/**
	 * The feature id for the '<em><b>Software Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__SOFTWARE_REVISION = COMPONENT_TYPE__SOFTWARE_REVISION;

	/**
	 * The feature id for the '<em><b>Device Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__DEVICE_REVISION = COMPONENT_TYPE__DEVICE_REVISION;

	/**
	 * The feature id for the '<em><b>Device Manual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__DEVICE_MANUAL = COMPONENT_TYPE__DEVICE_MANUAL;

	/**
	 * The feature id for the '<em><b>Device Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__DEVICE_CLASS = COMPONENT_TYPE__DEVICE_CLASS;

	/**
	 * The feature id for the '<em><b>Serial Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__SERIAL_NUMBER = COMPONENT_TYPE__SERIAL_NUMBER;

	/**
	 * The feature id for the '<em><b>Product Instance Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__PRODUCT_INSTANCE_URI = COMPONENT_TYPE__PRODUCT_INSTANCE_URI;

	/**
	 * The feature id for the '<em><b>Revision Counter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__REVISION_COUNTER = COMPONENT_TYPE__REVISION_COUNTER;

	/**
	 * The feature id for the '<em><b>Group Identifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__GROUP_IDENTIFIER = COMPONENT_TYPE__GROUP_IDENTIFIER;

	/**
	 * The feature id for the '<em><b>Identification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__IDENTIFICATION = COMPONENT_TYPE__IDENTIFICATION;

	/**
	 * The feature id for the '<em><b>Lock</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__LOCK = COMPONENT_TYPE__LOCK;

	/**
	 * The feature id for the '<em><b>Parameter Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__PARAMETER_SET = COMPONENT_TYPE__PARAMETER_SET;

	/**
	 * The feature id for the '<em><b>Method Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__METHOD_SET = COMPONENT_TYPE__METHOD_SET;

	/**
	 * The feature id for the '<em><b>Asset Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__ASSET_ID = COMPONENT_TYPE__ASSET_ID;

	/**
	 * The feature id for the '<em><b>Component Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__COMPONENT_NAME = COMPONENT_TYPE__COMPONENT_NAME;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__BASE_PROPERTY = COMPONENT_TYPE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Device Type Image</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__DEVICE_TYPE_IMAGE = COMPONENT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__DOCUMENTATION = COMPONENT_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Protocol Support</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__PROTOCOL_SUPPORT = COMPONENT_TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Image Set</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__IMAGE_SET = COMPONENT_TYPE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Device Health</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__DEVICE_HEALTH = COMPONENT_TYPE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>CPI Identifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE__CPI_IDENTIFIER = COMPONENT_TYPE_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Device Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE_FEATURE_COUNT = COMPONENT_TYPE_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Device Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEVICE_TYPE_OPERATION_COUNT = COMPONENT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ISupportInfoTypeImpl <em>ISupport Info Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ISupportInfoTypeImpl
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getISupportInfoType()
	 * @generated
	 */
	int ISUPPORT_INFO_TYPE = 9;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUPPORT_INFO_TYPE__BASE_CLASS = OPCUAProfilePackage.BASE_INTERFACE_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUPPORT_INFO_TYPE__NODE_ID = OPCUAProfilePackage.BASE_INTERFACE_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUPPORT_INFO_TYPE__NAMESPACE_URI = OPCUAProfilePackage.BASE_INTERFACE_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUPPORT_INFO_TYPE__BROWSE_NAME = OPCUAProfilePackage.BASE_INTERFACE_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUPPORT_INFO_TYPE__NODE_CLASS = OPCUAProfilePackage.BASE_INTERFACE_TYPE__NODE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUPPORT_INFO_TYPE__BASE_INTERFACE = OPCUAProfilePackage.BASE_INTERFACE_TYPE__BASE_INTERFACE;

	/**
	 * The feature id for the '<em><b>Device Type Image</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUPPORT_INFO_TYPE__DEVICE_TYPE_IMAGE = OPCUAProfilePackage.BASE_INTERFACE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUPPORT_INFO_TYPE__DOCUMENTATION = OPCUAProfilePackage.BASE_INTERFACE_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Protocol Support</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUPPORT_INFO_TYPE__PROTOCOL_SUPPORT = OPCUAProfilePackage.BASE_INTERFACE_TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Image Set</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUPPORT_INFO_TYPE__IMAGE_SET = OPCUAProfilePackage.BASE_INTERFACE_TYPE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>ISupport Info Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUPPORT_INFO_TYPE_FEATURE_COUNT = OPCUAProfilePackage.BASE_INTERFACE_TYPE_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>ISupport Info Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUPPORT_INFO_TYPE_OPERATION_COUNT = OPCUAProfilePackage.BASE_INTERFACE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.IDeviceHealthTypeImpl <em>IDevice Health Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.IDeviceHealthTypeImpl
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getIDeviceHealthType()
	 * @generated
	 */
	int IDEVICE_HEALTH_TYPE = 10;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDEVICE_HEALTH_TYPE__BASE_CLASS = OPCUAProfilePackage.BASE_INTERFACE_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDEVICE_HEALTH_TYPE__NODE_ID = OPCUAProfilePackage.BASE_INTERFACE_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDEVICE_HEALTH_TYPE__NAMESPACE_URI = OPCUAProfilePackage.BASE_INTERFACE_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDEVICE_HEALTH_TYPE__BROWSE_NAME = OPCUAProfilePackage.BASE_INTERFACE_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDEVICE_HEALTH_TYPE__NODE_CLASS = OPCUAProfilePackage.BASE_INTERFACE_TYPE__NODE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDEVICE_HEALTH_TYPE__BASE_INTERFACE = OPCUAProfilePackage.BASE_INTERFACE_TYPE__BASE_INTERFACE;

	/**
	 * The feature id for the '<em><b>Device Health</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDEVICE_HEALTH_TYPE__DEVICE_HEALTH = OPCUAProfilePackage.BASE_INTERFACE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>IDevice Health Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDEVICE_HEALTH_TYPE_FEATURE_COUNT = OPCUAProfilePackage.BASE_INTERFACE_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>IDevice Health Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDEVICE_HEALTH_TYPE_OPERATION_COUNT = OPCUAProfilePackage.BASE_INTERFACE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ConnectionPointTypeImpl <em>Connection Point Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ConnectionPointTypeImpl
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getConnectionPointType()
	 * @generated
	 */
	int CONNECTION_POINT_TYPE = 11;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_POINT_TYPE__BASE_CLASS = TOPOLOGY_ELEMENT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_POINT_TYPE__NODE_ID = TOPOLOGY_ELEMENT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_POINT_TYPE__NAMESPACE_URI = TOPOLOGY_ELEMENT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_POINT_TYPE__BROWSE_NAME = TOPOLOGY_ELEMENT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_POINT_TYPE__NODE_CLASS = TOPOLOGY_ELEMENT_TYPE__NODE_CLASS;

	/**
	 * The feature id for the '<em><b>Group Identifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_POINT_TYPE__GROUP_IDENTIFIER = TOPOLOGY_ELEMENT_TYPE__GROUP_IDENTIFIER;

	/**
	 * The feature id for the '<em><b>Identification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_POINT_TYPE__IDENTIFICATION = TOPOLOGY_ELEMENT_TYPE__IDENTIFICATION;

	/**
	 * The feature id for the '<em><b>Lock</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_POINT_TYPE__LOCK = TOPOLOGY_ELEMENT_TYPE__LOCK;

	/**
	 * The feature id for the '<em><b>Parameter Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_POINT_TYPE__PARAMETER_SET = TOPOLOGY_ELEMENT_TYPE__PARAMETER_SET;

	/**
	 * The feature id for the '<em><b>Method Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_POINT_TYPE__METHOD_SET = TOPOLOGY_ELEMENT_TYPE__METHOD_SET;

	/**
	 * The number of structural features of the '<em>Connection Point Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_POINT_TYPE_FEATURE_COUNT = TOPOLOGY_ELEMENT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Connection Point Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_POINT_TYPE_OPERATION_COUNT = TOPOLOGY_ELEMENT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.SoftwareTypeImpl <em>Software Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.SoftwareTypeImpl
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getSoftwareType()
	 * @generated
	 */
	int SOFTWARE_TYPE = 12;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__BASE_CLASS = COMPONENT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__NODE_ID = COMPONENT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__NAMESPACE_URI = COMPONENT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__BROWSE_NAME = COMPONENT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__NODE_CLASS = COMPONENT_TYPE__NODE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__BASE_INTERFACE = COMPONENT_TYPE__BASE_INTERFACE;

	/**
	 * The feature id for the '<em><b>Manufacturer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__MANUFACTURER = COMPONENT_TYPE__MANUFACTURER;

	/**
	 * The feature id for the '<em><b>Manufacturer Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__MANUFACTURER_URI = COMPONENT_TYPE__MANUFACTURER_URI;

	/**
	 * The feature id for the '<em><b>Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__MODEL = COMPONENT_TYPE__MODEL;

	/**
	 * The feature id for the '<em><b>Product Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__PRODUCT_CODE = COMPONENT_TYPE__PRODUCT_CODE;

	/**
	 * The feature id for the '<em><b>Hardware Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__HARDWARE_REVISION = COMPONENT_TYPE__HARDWARE_REVISION;

	/**
	 * The feature id for the '<em><b>Software Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__SOFTWARE_REVISION = COMPONENT_TYPE__SOFTWARE_REVISION;

	/**
	 * The feature id for the '<em><b>Device Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__DEVICE_REVISION = COMPONENT_TYPE__DEVICE_REVISION;

	/**
	 * The feature id for the '<em><b>Device Manual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__DEVICE_MANUAL = COMPONENT_TYPE__DEVICE_MANUAL;

	/**
	 * The feature id for the '<em><b>Device Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__DEVICE_CLASS = COMPONENT_TYPE__DEVICE_CLASS;

	/**
	 * The feature id for the '<em><b>Serial Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__SERIAL_NUMBER = COMPONENT_TYPE__SERIAL_NUMBER;

	/**
	 * The feature id for the '<em><b>Product Instance Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__PRODUCT_INSTANCE_URI = COMPONENT_TYPE__PRODUCT_INSTANCE_URI;

	/**
	 * The feature id for the '<em><b>Revision Counter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__REVISION_COUNTER = COMPONENT_TYPE__REVISION_COUNTER;

	/**
	 * The feature id for the '<em><b>Group Identifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__GROUP_IDENTIFIER = COMPONENT_TYPE__GROUP_IDENTIFIER;

	/**
	 * The feature id for the '<em><b>Identification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__IDENTIFICATION = COMPONENT_TYPE__IDENTIFICATION;

	/**
	 * The feature id for the '<em><b>Lock</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__LOCK = COMPONENT_TYPE__LOCK;

	/**
	 * The feature id for the '<em><b>Parameter Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__PARAMETER_SET = COMPONENT_TYPE__PARAMETER_SET;

	/**
	 * The feature id for the '<em><b>Method Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__METHOD_SET = COMPONENT_TYPE__METHOD_SET;

	/**
	 * The feature id for the '<em><b>Asset Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__ASSET_ID = COMPONENT_TYPE__ASSET_ID;

	/**
	 * The feature id for the '<em><b>Component Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__COMPONENT_NAME = COMPONENT_TYPE__COMPONENT_NAME;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE__BASE_PROPERTY = COMPONENT_TYPE__BASE_PROPERTY;

	/**
	 * The number of structural features of the '<em>Software Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE_FEATURE_COUNT = COMPONENT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Software Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_TYPE_OPERATION_COUNT = COMPONENT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.BlockTypeImpl <em>Block Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.BlockTypeImpl
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getBlockType()
	 * @generated
	 */
	int BLOCK_TYPE = 13;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__BASE_CLASS = TOPOLOGY_ELEMENT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__NODE_ID = TOPOLOGY_ELEMENT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__NAMESPACE_URI = TOPOLOGY_ELEMENT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__BROWSE_NAME = TOPOLOGY_ELEMENT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__NODE_CLASS = TOPOLOGY_ELEMENT_TYPE__NODE_CLASS;

	/**
	 * The feature id for the '<em><b>Group Identifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__GROUP_IDENTIFIER = TOPOLOGY_ELEMENT_TYPE__GROUP_IDENTIFIER;

	/**
	 * The feature id for the '<em><b>Identification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__IDENTIFICATION = TOPOLOGY_ELEMENT_TYPE__IDENTIFICATION;

	/**
	 * The feature id for the '<em><b>Lock</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__LOCK = TOPOLOGY_ELEMENT_TYPE__LOCK;

	/**
	 * The feature id for the '<em><b>Parameter Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__PARAMETER_SET = TOPOLOGY_ELEMENT_TYPE__PARAMETER_SET;

	/**
	 * The feature id for the '<em><b>Method Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__METHOD_SET = TOPOLOGY_ELEMENT_TYPE__METHOD_SET;

	/**
	 * The feature id for the '<em><b>Target Mode</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__TARGET_MODE = TOPOLOGY_ELEMENT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Revision Counter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__REVISION_COUNTER = TOPOLOGY_ELEMENT_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Actual Mode</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__ACTUAL_MODE = TOPOLOGY_ELEMENT_TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Permitted Mode</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__PERMITTED_MODE = TOPOLOGY_ELEMENT_TYPE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Normal Mode</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE__NORMAL_MODE = TOPOLOGY_ELEMENT_TYPE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Block Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE_FEATURE_COUNT = TOPOLOGY_ELEMENT_TYPE_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Block Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_TYPE_OPERATION_COUNT = TOPOLOGY_ELEMENT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ConfigurableObjectTypeImpl <em>Configurable Object Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ConfigurableObjectTypeImpl
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getConfigurableObjectType()
	 * @generated
	 */
	int CONFIGURABLE_OBJECT_TYPE = 14;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURABLE_OBJECT_TYPE__BASE_CLASS = OPCUAProfilePackage.BASE_OBJECT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURABLE_OBJECT_TYPE__NODE_ID = OPCUAProfilePackage.BASE_OBJECT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURABLE_OBJECT_TYPE__NAMESPACE_URI = OPCUAProfilePackage.BASE_OBJECT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURABLE_OBJECT_TYPE__BROWSE_NAME = OPCUAProfilePackage.BASE_OBJECT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURABLE_OBJECT_TYPE__NODE_CLASS = OPCUAProfilePackage.BASE_OBJECT_TYPE__NODE_CLASS;

	/**
	 * The number of structural features of the '<em>Configurable Object Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURABLE_OBJECT_TYPE_FEATURE_COUNT = OPCUAProfilePackage.BASE_OBJECT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Configurable Object Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURABLE_OBJECT_TYPE_OPERATION_COUNT = OPCUAProfilePackage.BASE_OBJECT_TYPE_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ComponentType <em>Component Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Type</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.ComponentType
	 * @generated
	 */
	EClass getComponentType();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ComponentType#getBase_Property <em>Base Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Property</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.ComponentType#getBase_Property()
	 * @see #getComponentType()
	 * @generated
	 */
	EReference getComponentType_Base_Property();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType <em>Topology Element Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Topology Element Type</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType
	 * @generated
	 */
	EClass getTopologyElementType();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType#getGroupIdentifier <em>Group Identifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Group Identifier</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType#getGroupIdentifier()
	 * @see #getTopologyElementType()
	 * @generated
	 */
	EReference getTopologyElementType_GroupIdentifier();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType#getIdentification <em>Identification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Identification</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType#getIdentification()
	 * @see #getTopologyElementType()
	 * @generated
	 */
	EReference getTopologyElementType_Identification();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType#getLock <em>Lock</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Lock</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType#getLock()
	 * @see #getTopologyElementType()
	 * @generated
	 */
	EReference getTopologyElementType_Lock();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType#getParameterSet <em>Parameter Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parameter Set</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType#getParameterSet()
	 * @see #getTopologyElementType()
	 * @generated
	 */
	EReference getTopologyElementType_ParameterSet();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType#getMethodSet <em>Method Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Method Set</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType#getMethodSet()
	 * @see #getTopologyElementType()
	 * @generated
	 */
	EReference getTopologyElementType_MethodSet();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.FunctionalGroupType <em>Functional Group Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Functional Group Type</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.FunctionalGroupType
	 * @generated
	 */
	EClass getFunctionalGroupType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType <em>Locking Services Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Locking Services Type</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType
	 * @generated
	 */
	EClass getLockingServicesType();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#getDefaultInstanceBrowseName <em>Default Instance Browse Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Default Instance Browse Name</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#getDefaultInstanceBrowseName()
	 * @see #getLockingServicesType()
	 * @generated
	 */
	EReference getLockingServicesType_DefaultInstanceBrowseName();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#isLocked <em>Locked</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Locked</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#isLocked()
	 * @see #getLockingServicesType()
	 * @generated
	 */
	EAttribute getLockingServicesType_Locked();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#getLockingClient <em>Locking Client</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Locking Client</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#getLockingClient()
	 * @see #getLockingServicesType()
	 * @generated
	 */
	EAttribute getLockingServicesType_LockingClient();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#getLockingUser <em>Locking User</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Locking User</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#getLockingUser()
	 * @see #getLockingServicesType()
	 * @generated
	 */
	EAttribute getLockingServicesType_LockingUser();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#getRemainingLockTime <em>Remaining Lock Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Remaining Lock Time</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#getRemainingLockTime()
	 * @see #getLockingServicesType()
	 * @generated
	 */
	EReference getLockingServicesType_RemainingLockTime();

	/**
	 * Returns the meta object for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#InitLock(java.lang.String, org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int32) <em>Init Lock</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init Lock</em>' operation.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#InitLock(java.lang.String, org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int32)
	 * @generated
	 */
	EOperation getLockingServicesType__InitLock__String_Int32();

	/**
	 * Returns the meta object for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#RenewLock(org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int32) <em>Renew Lock</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Renew Lock</em>' operation.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#RenewLock(org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int32)
	 * @generated
	 */
	EOperation getLockingServicesType__RenewLock__Int32();

	/**
	 * Returns the meta object for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#ExitLock(org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int32) <em>Exit Lock</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Exit Lock</em>' operation.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#ExitLock(org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int32)
	 * @generated
	 */
	EOperation getLockingServicesType__ExitLock__Int32();

	/**
	 * Returns the meta object for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#BreakLock(org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int32) <em>Break Lock</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Break Lock</em>' operation.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#BreakLock(org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int32)
	 * @generated
	 */
	EOperation getLockingServicesType__BreakLock__Int32();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ParameterSetTopologyElementType <em>Parameter Set Topology Element Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter Set Topology Element Type</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.ParameterSetTopologyElementType
	 * @generated
	 */
	EClass getParameterSetTopologyElementType();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ParameterSetTopologyElementType#getParameterIdentifier <em>Parameter Identifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parameter Identifier</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.ParameterSetTopologyElementType#getParameterIdentifier()
	 * @see #getParameterSetTopologyElementType()
	 * @generated
	 */
	EReference getParameterSetTopologyElementType_ParameterIdentifier();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.MethodSetTopologyElementType <em>Method Set Topology Element Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Method Set Topology Element Type</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.MethodSetTopologyElementType
	 * @generated
	 */
	EClass getMethodSetTopologyElementType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType <em>IVendor Nameplate Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IVendor Nameplate Type</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType
	 * @generated
	 */
	EClass getIVendorNameplateType();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getManufacturer <em>Manufacturer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Manufacturer</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getManufacturer()
	 * @see #getIVendorNameplateType()
	 * @generated
	 */
	EReference getIVendorNameplateType_Manufacturer();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getManufacturerUri <em>Manufacturer Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Manufacturer Uri</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getManufacturerUri()
	 * @see #getIVendorNameplateType()
	 * @generated
	 */
	EAttribute getIVendorNameplateType_ManufacturerUri();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Model</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getModel()
	 * @see #getIVendorNameplateType()
	 * @generated
	 */
	EReference getIVendorNameplateType_Model();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getProductCode <em>Product Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Product Code</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getProductCode()
	 * @see #getIVendorNameplateType()
	 * @generated
	 */
	EAttribute getIVendorNameplateType_ProductCode();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getHardwareRevision <em>Hardware Revision</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hardware Revision</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getHardwareRevision()
	 * @see #getIVendorNameplateType()
	 * @generated
	 */
	EAttribute getIVendorNameplateType_HardwareRevision();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getSoftwareRevision <em>Software Revision</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Software Revision</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getSoftwareRevision()
	 * @see #getIVendorNameplateType()
	 * @generated
	 */
	EAttribute getIVendorNameplateType_SoftwareRevision();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getDeviceRevision <em>Device Revision</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Device Revision</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getDeviceRevision()
	 * @see #getIVendorNameplateType()
	 * @generated
	 */
	EAttribute getIVendorNameplateType_DeviceRevision();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getDeviceManual <em>Device Manual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Device Manual</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getDeviceManual()
	 * @see #getIVendorNameplateType()
	 * @generated
	 */
	EAttribute getIVendorNameplateType_DeviceManual();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getDeviceClass <em>Device Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Device Class</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getDeviceClass()
	 * @see #getIVendorNameplateType()
	 * @generated
	 */
	EAttribute getIVendorNameplateType_DeviceClass();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getSerialNumber <em>Serial Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Serial Number</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getSerialNumber()
	 * @see #getIVendorNameplateType()
	 * @generated
	 */
	EAttribute getIVendorNameplateType_SerialNumber();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getProductInstanceUri <em>Product Instance Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Product Instance Uri</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getProductInstanceUri()
	 * @see #getIVendorNameplateType()
	 * @generated
	 */
	EAttribute getIVendorNameplateType_ProductInstanceUri();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getRevisionCounter <em>Revision Counter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Revision Counter</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType#getRevisionCounter()
	 * @see #getIVendorNameplateType()
	 * @generated
	 */
	EReference getIVendorNameplateType_RevisionCounter();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ITagNameplateType <em>ITag Nameplate Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ITag Nameplate Type</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.ITagNameplateType
	 * @generated
	 */
	EClass getITagNameplateType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ITagNameplateType#getAssetId <em>Asset Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Asset Id</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.ITagNameplateType#getAssetId()
	 * @see #getITagNameplateType()
	 * @generated
	 */
	EAttribute getITagNameplateType_AssetId();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ITagNameplateType#getComponentName <em>Component Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Component Name</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.ITagNameplateType#getComponentName()
	 * @see #getITagNameplateType()
	 * @generated
	 */
	EReference getITagNameplateType_ComponentName();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.DeviceType <em>Device Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Device Type</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.DeviceType
	 * @generated
	 */
	EClass getDeviceType();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.DeviceType#getCPIIdentifier <em>CPI Identifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CPI Identifier</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.DeviceType#getCPIIdentifier()
	 * @see #getDeviceType()
	 * @generated
	 */
	EReference getDeviceType_CPIIdentifier();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ISupportInfoType <em>ISupport Info Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISupport Info Type</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.ISupportInfoType
	 * @generated
	 */
	EClass getISupportInfoType();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ISupportInfoType#getDeviceTypeImage <em>Device Type Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Device Type Image</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.ISupportInfoType#getDeviceTypeImage()
	 * @see #getISupportInfoType()
	 * @generated
	 */
	EReference getISupportInfoType_DeviceTypeImage();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ISupportInfoType#getDocumentation <em>Documentation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Documentation</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.ISupportInfoType#getDocumentation()
	 * @see #getISupportInfoType()
	 * @generated
	 */
	EReference getISupportInfoType_Documentation();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ISupportInfoType#getProtocolSupport <em>Protocol Support</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Protocol Support</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.ISupportInfoType#getProtocolSupport()
	 * @see #getISupportInfoType()
	 * @generated
	 */
	EReference getISupportInfoType_ProtocolSupport();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ISupportInfoType#getImageSet <em>Image Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Image Set</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.ISupportInfoType#getImageSet()
	 * @see #getISupportInfoType()
	 * @generated
	 */
	EReference getISupportInfoType_ImageSet();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IDeviceHealthType <em>IDevice Health Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IDevice Health Type</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.IDeviceHealthType
	 * @generated
	 */
	EClass getIDeviceHealthType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IDeviceHealthType#getDeviceHealth <em>Device Health</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Device Health</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.IDeviceHealthType#getDeviceHealth()
	 * @see #getIDeviceHealthType()
	 * @generated
	 */
	EAttribute getIDeviceHealthType_DeviceHealth();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ConnectionPointType <em>Connection Point Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connection Point Type</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.ConnectionPointType
	 * @generated
	 */
	EClass getConnectionPointType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.SoftwareType <em>Software Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Software Type</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.SoftwareType
	 * @generated
	 */
	EClass getSoftwareType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.BlockType <em>Block Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Block Type</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.BlockType
	 * @generated
	 */
	EClass getBlockType();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.BlockType#getTargetMode <em>Target Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Target Mode</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.BlockType#getTargetMode()
	 * @see #getBlockType()
	 * @generated
	 */
	EReference getBlockType_TargetMode();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.BlockType#getRevisionCounter <em>Revision Counter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Revision Counter</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.BlockType#getRevisionCounter()
	 * @see #getBlockType()
	 * @generated
	 */
	EReference getBlockType_RevisionCounter();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.BlockType#getActualMode <em>Actual Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Actual Mode</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.BlockType#getActualMode()
	 * @see #getBlockType()
	 * @generated
	 */
	EReference getBlockType_ActualMode();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.BlockType#getPermittedMode <em>Permitted Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Permitted Mode</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.BlockType#getPermittedMode()
	 * @see #getBlockType()
	 * @generated
	 */
	EReference getBlockType_PermittedMode();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.BlockType#getNormalMode <em>Normal Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Normal Mode</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.BlockType#getNormalMode()
	 * @see #getBlockType()
	 * @generated
	 */
	EReference getBlockType_NormalMode();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ConfigurableObjectType <em>Configurable Object Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Configurable Object Type</em>'.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.ConfigurableObjectType
	 * @generated
	 */
	EClass getConfigurableObjectType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OPCUADIProfileFactory getOPCUADIProfileFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ComponentTypeImpl <em>Component Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ComponentTypeImpl
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getComponentType()
		 * @generated
		 */
		EClass COMPONENT_TYPE = eINSTANCE.getComponentType();

		/**
		 * The meta object literal for the '<em><b>Base Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_TYPE__BASE_PROPERTY = eINSTANCE.getComponentType_Base_Property();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.TopologyElementTypeImpl <em>Topology Element Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.TopologyElementTypeImpl
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getTopologyElementType()
		 * @generated
		 */
		EClass TOPOLOGY_ELEMENT_TYPE = eINSTANCE.getTopologyElementType();

		/**
		 * The meta object literal for the '<em><b>Group Identifier</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TOPOLOGY_ELEMENT_TYPE__GROUP_IDENTIFIER = eINSTANCE.getTopologyElementType_GroupIdentifier();

		/**
		 * The meta object literal for the '<em><b>Identification</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TOPOLOGY_ELEMENT_TYPE__IDENTIFICATION = eINSTANCE.getTopologyElementType_Identification();

		/**
		 * The meta object literal for the '<em><b>Lock</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TOPOLOGY_ELEMENT_TYPE__LOCK = eINSTANCE.getTopologyElementType_Lock();

		/**
		 * The meta object literal for the '<em><b>Parameter Set</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TOPOLOGY_ELEMENT_TYPE__PARAMETER_SET = eINSTANCE.getTopologyElementType_ParameterSet();

		/**
		 * The meta object literal for the '<em><b>Method Set</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TOPOLOGY_ELEMENT_TYPE__METHOD_SET = eINSTANCE.getTopologyElementType_MethodSet();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.FunctionalGroupTypeImpl <em>Functional Group Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.FunctionalGroupTypeImpl
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getFunctionalGroupType()
		 * @generated
		 */
		EClass FUNCTIONAL_GROUP_TYPE = eINSTANCE.getFunctionalGroupType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.LockingServicesTypeImpl <em>Locking Services Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.LockingServicesTypeImpl
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getLockingServicesType()
		 * @generated
		 */
		EClass LOCKING_SERVICES_TYPE = eINSTANCE.getLockingServicesType();

		/**
		 * The meta object literal for the '<em><b>Default Instance Browse Name</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOCKING_SERVICES_TYPE__DEFAULT_INSTANCE_BROWSE_NAME = eINSTANCE.getLockingServicesType_DefaultInstanceBrowseName();

		/**
		 * The meta object literal for the '<em><b>Locked</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOCKING_SERVICES_TYPE__LOCKED = eINSTANCE.getLockingServicesType_Locked();

		/**
		 * The meta object literal for the '<em><b>Locking Client</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOCKING_SERVICES_TYPE__LOCKING_CLIENT = eINSTANCE.getLockingServicesType_LockingClient();

		/**
		 * The meta object literal for the '<em><b>Locking User</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOCKING_SERVICES_TYPE__LOCKING_USER = eINSTANCE.getLockingServicesType_LockingUser();

		/**
		 * The meta object literal for the '<em><b>Remaining Lock Time</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOCKING_SERVICES_TYPE__REMAINING_LOCK_TIME = eINSTANCE.getLockingServicesType_RemainingLockTime();

		/**
		 * The meta object literal for the '<em><b>Init Lock</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LOCKING_SERVICES_TYPE___INIT_LOCK__STRING_INT32 = eINSTANCE.getLockingServicesType__InitLock__String_Int32();

		/**
		 * The meta object literal for the '<em><b>Renew Lock</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LOCKING_SERVICES_TYPE___RENEW_LOCK__INT32 = eINSTANCE.getLockingServicesType__RenewLock__Int32();

		/**
		 * The meta object literal for the '<em><b>Exit Lock</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LOCKING_SERVICES_TYPE___EXIT_LOCK__INT32 = eINSTANCE.getLockingServicesType__ExitLock__Int32();

		/**
		 * The meta object literal for the '<em><b>Break Lock</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LOCKING_SERVICES_TYPE___BREAK_LOCK__INT32 = eINSTANCE.getLockingServicesType__BreakLock__Int32();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ParameterSetTopologyElementTypeImpl <em>Parameter Set Topology Element Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ParameterSetTopologyElementTypeImpl
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getParameterSetTopologyElementType()
		 * @generated
		 */
		EClass PARAMETER_SET_TOPOLOGY_ELEMENT_TYPE = eINSTANCE.getParameterSetTopologyElementType();

		/**
		 * The meta object literal for the '<em><b>Parameter Identifier</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER_SET_TOPOLOGY_ELEMENT_TYPE__PARAMETER_IDENTIFIER = eINSTANCE.getParameterSetTopologyElementType_ParameterIdentifier();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.MethodSetTopologyElementTypeImpl <em>Method Set Topology Element Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.MethodSetTopologyElementTypeImpl
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getMethodSetTopologyElementType()
		 * @generated
		 */
		EClass METHOD_SET_TOPOLOGY_ELEMENT_TYPE = eINSTANCE.getMethodSetTopologyElementType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.IVendorNameplateTypeImpl <em>IVendor Nameplate Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.IVendorNameplateTypeImpl
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getIVendorNameplateType()
		 * @generated
		 */
		EClass IVENDOR_NAMEPLATE_TYPE = eINSTANCE.getIVendorNameplateType();

		/**
		 * The meta object literal for the '<em><b>Manufacturer</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IVENDOR_NAMEPLATE_TYPE__MANUFACTURER = eINSTANCE.getIVendorNameplateType_Manufacturer();

		/**
		 * The meta object literal for the '<em><b>Manufacturer Uri</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVENDOR_NAMEPLATE_TYPE__MANUFACTURER_URI = eINSTANCE.getIVendorNameplateType_ManufacturerUri();

		/**
		 * The meta object literal for the '<em><b>Model</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IVENDOR_NAMEPLATE_TYPE__MODEL = eINSTANCE.getIVendorNameplateType_Model();

		/**
		 * The meta object literal for the '<em><b>Product Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVENDOR_NAMEPLATE_TYPE__PRODUCT_CODE = eINSTANCE.getIVendorNameplateType_ProductCode();

		/**
		 * The meta object literal for the '<em><b>Hardware Revision</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVENDOR_NAMEPLATE_TYPE__HARDWARE_REVISION = eINSTANCE.getIVendorNameplateType_HardwareRevision();

		/**
		 * The meta object literal for the '<em><b>Software Revision</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVENDOR_NAMEPLATE_TYPE__SOFTWARE_REVISION = eINSTANCE.getIVendorNameplateType_SoftwareRevision();

		/**
		 * The meta object literal for the '<em><b>Device Revision</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVENDOR_NAMEPLATE_TYPE__DEVICE_REVISION = eINSTANCE.getIVendorNameplateType_DeviceRevision();

		/**
		 * The meta object literal for the '<em><b>Device Manual</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVENDOR_NAMEPLATE_TYPE__DEVICE_MANUAL = eINSTANCE.getIVendorNameplateType_DeviceManual();

		/**
		 * The meta object literal for the '<em><b>Device Class</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVENDOR_NAMEPLATE_TYPE__DEVICE_CLASS = eINSTANCE.getIVendorNameplateType_DeviceClass();

		/**
		 * The meta object literal for the '<em><b>Serial Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVENDOR_NAMEPLATE_TYPE__SERIAL_NUMBER = eINSTANCE.getIVendorNameplateType_SerialNumber();

		/**
		 * The meta object literal for the '<em><b>Product Instance Uri</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVENDOR_NAMEPLATE_TYPE__PRODUCT_INSTANCE_URI = eINSTANCE.getIVendorNameplateType_ProductInstanceUri();

		/**
		 * The meta object literal for the '<em><b>Revision Counter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IVENDOR_NAMEPLATE_TYPE__REVISION_COUNTER = eINSTANCE.getIVendorNameplateType_RevisionCounter();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ITagNameplateTypeImpl <em>ITag Nameplate Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ITagNameplateTypeImpl
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getITagNameplateType()
		 * @generated
		 */
		EClass ITAG_NAMEPLATE_TYPE = eINSTANCE.getITagNameplateType();

		/**
		 * The meta object literal for the '<em><b>Asset Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ITAG_NAMEPLATE_TYPE__ASSET_ID = eINSTANCE.getITagNameplateType_AssetId();

		/**
		 * The meta object literal for the '<em><b>Component Name</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ITAG_NAMEPLATE_TYPE__COMPONENT_NAME = eINSTANCE.getITagNameplateType_ComponentName();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.DeviceTypeImpl <em>Device Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.DeviceTypeImpl
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getDeviceType()
		 * @generated
		 */
		EClass DEVICE_TYPE = eINSTANCE.getDeviceType();

		/**
		 * The meta object literal for the '<em><b>CPI Identifier</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEVICE_TYPE__CPI_IDENTIFIER = eINSTANCE.getDeviceType_CPIIdentifier();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ISupportInfoTypeImpl <em>ISupport Info Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ISupportInfoTypeImpl
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getISupportInfoType()
		 * @generated
		 */
		EClass ISUPPORT_INFO_TYPE = eINSTANCE.getISupportInfoType();

		/**
		 * The meta object literal for the '<em><b>Device Type Image</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISUPPORT_INFO_TYPE__DEVICE_TYPE_IMAGE = eINSTANCE.getISupportInfoType_DeviceTypeImage();

		/**
		 * The meta object literal for the '<em><b>Documentation</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISUPPORT_INFO_TYPE__DOCUMENTATION = eINSTANCE.getISupportInfoType_Documentation();

		/**
		 * The meta object literal for the '<em><b>Protocol Support</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISUPPORT_INFO_TYPE__PROTOCOL_SUPPORT = eINSTANCE.getISupportInfoType_ProtocolSupport();

		/**
		 * The meta object literal for the '<em><b>Image Set</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISUPPORT_INFO_TYPE__IMAGE_SET = eINSTANCE.getISupportInfoType_ImageSet();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.IDeviceHealthTypeImpl <em>IDevice Health Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.IDeviceHealthTypeImpl
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getIDeviceHealthType()
		 * @generated
		 */
		EClass IDEVICE_HEALTH_TYPE = eINSTANCE.getIDeviceHealthType();

		/**
		 * The meta object literal for the '<em><b>Device Health</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IDEVICE_HEALTH_TYPE__DEVICE_HEALTH = eINSTANCE.getIDeviceHealthType_DeviceHealth();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ConnectionPointTypeImpl <em>Connection Point Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ConnectionPointTypeImpl
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getConnectionPointType()
		 * @generated
		 */
		EClass CONNECTION_POINT_TYPE = eINSTANCE.getConnectionPointType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.SoftwareTypeImpl <em>Software Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.SoftwareTypeImpl
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getSoftwareType()
		 * @generated
		 */
		EClass SOFTWARE_TYPE = eINSTANCE.getSoftwareType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.BlockTypeImpl <em>Block Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.BlockTypeImpl
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getBlockType()
		 * @generated
		 */
		EClass BLOCK_TYPE = eINSTANCE.getBlockType();

		/**
		 * The meta object literal for the '<em><b>Target Mode</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK_TYPE__TARGET_MODE = eINSTANCE.getBlockType_TargetMode();

		/**
		 * The meta object literal for the '<em><b>Revision Counter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK_TYPE__REVISION_COUNTER = eINSTANCE.getBlockType_RevisionCounter();

		/**
		 * The meta object literal for the '<em><b>Actual Mode</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK_TYPE__ACTUAL_MODE = eINSTANCE.getBlockType_ActualMode();

		/**
		 * The meta object literal for the '<em><b>Permitted Mode</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK_TYPE__PERMITTED_MODE = eINSTANCE.getBlockType_PermittedMode();

		/**
		 * The meta object literal for the '<em><b>Normal Mode</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK_TYPE__NORMAL_MODE = eINSTANCE.getBlockType_NormalMode();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ConfigurableObjectTypeImpl <em>Configurable Object Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ConfigurableObjectTypeImpl
		 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl#getConfigurableObjectType()
		 * @generated
		 */
		EClass CONFIGURABLE_OBJECT_TYPE = eINSTANCE.getConfigurableObjectType();

	}

} //OPCUADIProfilePackage
