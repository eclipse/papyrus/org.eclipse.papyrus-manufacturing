/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package OPC_UA_DI_Library.impl;

import OPC_UA_DI_Library.DeviceHealthEnumeration;
import OPC_UA_DI_Library.OPC_UA_DI_LibraryFactory;
import OPC_UA_DI_Library.OPC_UA_DI_LibraryPackage;

import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OPC_UA_DI_LibraryPackageImpl extends EPackageImpl implements OPC_UA_DI_LibraryPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum deviceHealthEnumerationEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see OPC_UA_DI_Library.OPC_UA_DI_LibraryPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OPC_UA_DI_LibraryPackageImpl() {
		super(eNS_URI, OPC_UA_DI_LibraryFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link OPC_UA_DI_LibraryPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static OPC_UA_DI_LibraryPackage init() {
		if (isInited) return (OPC_UA_DI_LibraryPackage)EPackage.Registry.INSTANCE.getEPackage(OPC_UA_DI_LibraryPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredOPC_UA_DI_LibraryPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		OPC_UA_DI_LibraryPackageImpl theOPC_UA_DI_LibraryPackage = registeredOPC_UA_DI_LibraryPackage instanceof OPC_UA_DI_LibraryPackageImpl ? (OPC_UA_DI_LibraryPackageImpl)registeredOPC_UA_DI_LibraryPackage : new OPC_UA_DI_LibraryPackageImpl();

		isInited = true;

		// Create package meta-data objects
		theOPC_UA_DI_LibraryPackage.createPackageContents();

		// Initialize created meta-data
		theOPC_UA_DI_LibraryPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theOPC_UA_DI_LibraryPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(OPC_UA_DI_LibraryPackage.eNS_URI, theOPC_UA_DI_LibraryPackage);
		return theOPC_UA_DI_LibraryPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDeviceHealthEnumeration() {
		return deviceHealthEnumerationEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPC_UA_DI_LibraryFactory getOPC_UA_DI_LibraryFactory() {
		return (OPC_UA_DI_LibraryFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create enums
		deviceHealthEnumerationEEnum = createEEnum(DEVICE_HEALTH_ENUMERATION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Initialize enums and add enum literals
		initEEnum(deviceHealthEnumerationEEnum, DeviceHealthEnumeration.class, "DeviceHealthEnumeration");
		addEEnumLiteral(deviceHealthEnumerationEEnum, DeviceHealthEnumeration.NORMAL);
		addEEnumLiteral(deviceHealthEnumerationEEnum, DeviceHealthEnumeration.FAILURE);
		addEEnumLiteral(deviceHealthEnumerationEEnum, DeviceHealthEnumeration.CHECK_FUNCTION);
		addEEnumLiteral(deviceHealthEnumerationEEnum, DeviceHealthEnumeration.OFF_SPEC);
		addEEnumLiteral(deviceHealthEnumerationEEnum, DeviceHealthEnumeration.MAINTENANCE_REQUIRED);

		// Create resource
		createResource(eNS_URI);
	}

} //OPC_UA_DI_LibraryPackageImpl
