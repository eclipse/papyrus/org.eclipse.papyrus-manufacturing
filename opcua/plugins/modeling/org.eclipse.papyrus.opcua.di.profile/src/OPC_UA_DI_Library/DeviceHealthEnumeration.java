/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package OPC_UA_DI_Library;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Device Health Enumeration</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see OPC_UA_DI_Library.OPC_UA_DI_LibraryPackage#getDeviceHealthEnumeration()
 * @model
 * @generated
 */
public enum DeviceHealthEnumeration implements Enumerator {
	/**
	 * The '<em><b>NORMAL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This device functions normally.
	 * <!-- end-model-doc -->
	 * @see #NORMAL_VALUE
	 * @generated
	 * @ordered
	 */
	NORMAL(1, "NORMAL", "NORMAL"),

	/**
	 * The '<em><b>FAILURE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Malfunction of the device or any of its peripherals.
	 * <!-- end-model-doc -->
	 * @see #FAILURE_VALUE
	 * @generated
	 * @ordered
	 */
	FAILURE(1, "FAILURE", "FAILURE"),

	/**
	 * The '<em><b>CHECK FUNCTION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Functional checks are currently performed.
	 * <!-- end-model-doc -->
	 * @see #CHECK_FUNCTION_VALUE
	 * @generated
	 * @ordered
	 */
	CHECK_FUNCTION(2, "CHECK_FUNCTION", "CHECK_FUNCTION"),

	/**
	 * The '<em><b>OFF SPEC</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The device is currently working outside of its specified range or that internal diagnoses indicate deviations from measured or set values.
	 * <!-- end-model-doc -->
	 * @see #OFF_SPEC_VALUE
	 * @generated
	 * @ordered
	 */
	OFF_SPEC(3, "OFF_SPEC", "OFF_SPEC"),

	/**
	 * The '<em><b>MAINTENANCE REQUIRED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This element is working, but a maintenance operation is required.
	 * <!-- end-model-doc -->
	 * @see #MAINTENANCE_REQUIRED_VALUE
	 * @generated
	 * @ordered
	 */
	MAINTENANCE_REQUIRED(4, "MAINTENANCE_REQUIRED", "MAINTENANCE_REQUIRED");

	/**
	 * The '<em><b>NORMAL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This device functions normally.
	 * <!-- end-model-doc -->
	 * @see #NORMAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NORMAL_VALUE = 1;

	/**
	 * The '<em><b>FAILURE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Malfunction of the device or any of its peripherals.
	 * <!-- end-model-doc -->
	 * @see #FAILURE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FAILURE_VALUE = 1;

	/**
	 * The '<em><b>CHECK FUNCTION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Functional checks are currently performed.
	 * <!-- end-model-doc -->
	 * @see #CHECK_FUNCTION
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CHECK_FUNCTION_VALUE = 2;

	/**
	 * The '<em><b>OFF SPEC</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The device is currently working outside of its specified range or that internal diagnoses indicate deviations from measured or set values.
	 * <!-- end-model-doc -->
	 * @see #OFF_SPEC
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int OFF_SPEC_VALUE = 3;

	/**
	 * The '<em><b>MAINTENANCE REQUIRED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This element is working, but a maintenance operation is required.
	 * <!-- end-model-doc -->
	 * @see #MAINTENANCE_REQUIRED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MAINTENANCE_REQUIRED_VALUE = 4;

	/**
	 * An array of all the '<em><b>Device Health Enumeration</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final DeviceHealthEnumeration[] VALUES_ARRAY =
		new DeviceHealthEnumeration[] {
			NORMAL,
			FAILURE,
			CHECK_FUNCTION,
			OFF_SPEC,
			MAINTENANCE_REQUIRED,
		};

	/**
	 * A public read-only list of all the '<em><b>Device Health Enumeration</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<DeviceHealthEnumeration> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Device Health Enumeration</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DeviceHealthEnumeration get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DeviceHealthEnumeration result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Device Health Enumeration</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DeviceHealthEnumeration getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DeviceHealthEnumeration result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Device Health Enumeration</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DeviceHealthEnumeration get(int value) {
		switch (value) {
			case NORMAL_VALUE: return NORMAL;
			case CHECK_FUNCTION_VALUE: return CHECK_FUNCTION;
			case OFF_SPEC_VALUE: return OFF_SPEC;
			case MAINTENANCE_REQUIRED_VALUE: return MAINTENANCE_REQUIRED;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private DeviceHealthEnumeration(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //DeviceHealthEnumeration
