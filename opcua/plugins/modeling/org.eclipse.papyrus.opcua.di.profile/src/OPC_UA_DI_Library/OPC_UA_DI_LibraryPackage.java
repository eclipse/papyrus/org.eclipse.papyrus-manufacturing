/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package OPC_UA_DI_Library;

import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see OPC_UA_DI_Library.OPC_UA_DI_LibraryFactory
 * @model kind="package"
 * @generated
 */
public interface OPC_UA_DI_LibraryPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "OPC_UA_DI_Library";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///OPC_UA_DI_Library.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "OPC_UA_DI_Library";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OPC_UA_DI_LibraryPackage eINSTANCE = OPC_UA_DI_Library.impl.OPC_UA_DI_LibraryPackageImpl.init();

	/**
	 * The meta object id for the '{@link OPC_UA_DI_Library.DeviceHealthEnumeration <em>Device Health Enumeration</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see OPC_UA_DI_Library.DeviceHealthEnumeration
	 * @see OPC_UA_DI_Library.impl.OPC_UA_DI_LibraryPackageImpl#getDeviceHealthEnumeration()
	 * @generated
	 */
	int DEVICE_HEALTH_ENUMERATION = 0;


	/**
	 * Returns the meta object for enum '{@link OPC_UA_DI_Library.DeviceHealthEnumeration <em>Device Health Enumeration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Device Health Enumeration</em>'.
	 * @see OPC_UA_DI_Library.DeviceHealthEnumeration
	 * @generated
	 */
	EEnum getDeviceHealthEnumeration();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OPC_UA_DI_LibraryFactory getOPC_UA_DI_LibraryFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link OPC_UA_DI_Library.DeviceHealthEnumeration <em>Device Health Enumeration</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see OPC_UA_DI_Library.DeviceHealthEnumeration
		 * @see OPC_UA_DI_Library.impl.OPC_UA_DI_LibraryPackageImpl#getDeviceHealthEnumeration()
		 * @generated
		 */
		EEnum DEVICE_HEALTH_ENUMERATION = eINSTANCE.getDeviceHealthEnumeration();

	}

} //OPC_UA_DI_LibraryPackage
