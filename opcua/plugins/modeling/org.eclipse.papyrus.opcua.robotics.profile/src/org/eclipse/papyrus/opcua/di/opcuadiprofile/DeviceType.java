/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.di.opcuadiprofile;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Device Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.DeviceType#getCPIIdentifier <em>CPI Identifier</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getDeviceType()
 * @model
 * @generated
 */
public interface DeviceType extends ComponentType, ISupportInfoType, IDeviceHealthType {
	/**
	 * Returns the value of the '<em><b>CPI Identifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>CPI Identifier</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CPI Identifier</em>' reference.
	 * @see #setCPIIdentifier(ConnectionPointType)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getDeviceType_CPIIdentifier()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	ConnectionPointType getCPIIdentifier();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.DeviceType#getCPIIdentifier <em>CPI Identifier</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CPI Identifier</em>' reference.
	 * @see #getCPIIdentifier()
	 * @generated
	 */
	void setCPIIdentifier(ConnectionPointType value);

} // DeviceType
