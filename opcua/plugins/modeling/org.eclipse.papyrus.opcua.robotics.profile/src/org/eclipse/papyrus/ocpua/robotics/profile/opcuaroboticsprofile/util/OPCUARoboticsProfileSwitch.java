/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.*;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.ComponentType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.ITagNameplateType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType;

import org.eclipse.papyrus.opcua.opcuaprofile.BaseInterfaceType;
import org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage
 * @generated
 */
public class OPCUARoboticsProfileSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static OPCUARoboticsProfilePackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPCUARoboticsProfileSwitch() {
		if (modelPackage == null) {
			modelPackage = OPCUARoboticsProfilePackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case OPCUARoboticsProfilePackage.LOAD_TYPE: {
				LoadType loadType = (LoadType)theEObject;
				T result = caseLoadType(loadType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTOR_TYPE: {
				ParameterSetMotorType parameterSetMotorType = (ParameterSetMotorType)theEObject;
				T result = caseParameterSetMotorType(parameterSetMotorType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE: {
				ParameterSetControllerType parameterSetControllerType = (ParameterSetControllerType)theEObject;
				T result = caseParameterSetControllerType(parameterSetControllerType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.PARAMETER_SET_TASK_CONTROL_TYPE: {
				ParameterSetTaskControlType parameterSetTaskControlType = (ParameterSetTaskControlType)theEObject;
				T result = caseParameterSetTaskControlType(parameterSetTaskControlType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.PARAMETER_SET_SAFETY_STATE_TYPE: {
				ParameterSetSafetyStateType parameterSetSafetyStateType = (ParameterSetSafetyStateType)theEObject;
				T result = caseParameterSetSafetyStateType(parameterSetSafetyStateType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.GEAR_TYPE: {
				GearType gearType = (GearType)theEObject;
				T result = caseGearType(gearType);
				if (result == null) result = caseComponentType(gearType);
				if (result == null) result = caseIVendorNameplateType(gearType);
				if (result == null) result = caseTopologyElementType(gearType);
				if (result == null) result = caseITagNameplateType(gearType);
				if (result == null) result = caseBaseInterfaceType(gearType);
				if (result == null) result = caseBaseObjectType(gearType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.MOTOR_TYPE: {
				MotorType motorType = (MotorType)theEObject;
				T result = caseMotorType(motorType);
				if (result == null) result = caseComponentType(motorType);
				if (result == null) result = caseIVendorNameplateType(motorType);
				if (result == null) result = caseTopologyElementType(motorType);
				if (result == null) result = caseITagNameplateType(motorType);
				if (result == null) result = caseBaseInterfaceType(motorType);
				if (result == null) result = caseBaseObjectType(motorType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.PARAMETER_SET_AXIS_TYPE: {
				ParameterSetAxisType parameterSetAxisType = (ParameterSetAxisType)theEObject;
				T result = caseParameterSetAxisType(parameterSetAxisType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE: {
				ParameterSetMotionDeviceType parameterSetMotionDeviceType = (ParameterSetMotionDeviceType)theEObject;
				T result = caseParameterSetMotionDeviceType(parameterSetMotionDeviceType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.POWER_TRAIN_TYPE: {
				PowerTrainType powerTrainType = (PowerTrainType)theEObject;
				T result = casePowerTrainType(powerTrainType);
				if (result == null) result = caseComponentType(powerTrainType);
				if (result == null) result = caseIVendorNameplateType(powerTrainType);
				if (result == null) result = caseTopologyElementType(powerTrainType);
				if (result == null) result = caseITagNameplateType(powerTrainType);
				if (result == null) result = caseBaseInterfaceType(powerTrainType);
				if (result == null) result = caseBaseObjectType(powerTrainType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_SYSTEM_TYPE: {
				MotionDeviceSystemType motionDeviceSystemType = (MotionDeviceSystemType)theEObject;
				T result = caseMotionDeviceSystemType(motionDeviceSystemType);
				if (result == null) result = caseComponentType(motionDeviceSystemType);
				if (result == null) result = caseIVendorNameplateType(motionDeviceSystemType);
				if (result == null) result = caseTopologyElementType(motionDeviceSystemType);
				if (result == null) result = caseITagNameplateType(motionDeviceSystemType);
				if (result == null) result = caseBaseInterfaceType(motionDeviceSystemType);
				if (result == null) result = caseBaseObjectType(motionDeviceSystemType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE: {
				MotionDeviceType motionDeviceType = (MotionDeviceType)theEObject;
				T result = caseMotionDeviceType(motionDeviceType);
				if (result == null) result = caseComponentType(motionDeviceType);
				if (result == null) result = caseIVendorNameplateType(motionDeviceType);
				if (result == null) result = caseTopologyElementType(motionDeviceType);
				if (result == null) result = caseITagNameplateType(motionDeviceType);
				if (result == null) result = caseBaseInterfaceType(motionDeviceType);
				if (result == null) result = caseBaseObjectType(motionDeviceType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.AXIS_TYPE: {
				AxisType axisType = (AxisType)theEObject;
				T result = caseAxisType(axisType);
				if (result == null) result = caseComponentType(axisType);
				if (result == null) result = caseIVendorNameplateType(axisType);
				if (result == null) result = caseTopologyElementType(axisType);
				if (result == null) result = caseITagNameplateType(axisType);
				if (result == null) result = caseBaseInterfaceType(axisType);
				if (result == null) result = caseBaseObjectType(axisType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.CONTROLLER_TYPE: {
				ControllerType controllerType = (ControllerType)theEObject;
				T result = caseControllerType(controllerType);
				if (result == null) result = caseComponentType(controllerType);
				if (result == null) result = caseIVendorNameplateType(controllerType);
				if (result == null) result = caseTopologyElementType(controllerType);
				if (result == null) result = caseITagNameplateType(controllerType);
				if (result == null) result = caseBaseInterfaceType(controllerType);
				if (result == null) result = caseBaseObjectType(controllerType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.TASK_CONTROL_TYPE: {
				TaskControlType taskControlType = (TaskControlType)theEObject;
				T result = caseTaskControlType(taskControlType);
				if (result == null) result = caseComponentType(taskControlType);
				if (result == null) result = caseIVendorNameplateType(taskControlType);
				if (result == null) result = caseTopologyElementType(taskControlType);
				if (result == null) result = caseITagNameplateType(taskControlType);
				if (result == null) result = caseBaseInterfaceType(taskControlType);
				if (result == null) result = caseBaseObjectType(taskControlType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.SAFETY_STATE_TYPE: {
				SafetyStateType safetyStateType = (SafetyStateType)theEObject;
				T result = caseSafetyStateType(safetyStateType);
				if (result == null) result = caseComponentType(safetyStateType);
				if (result == null) result = caseIVendorNameplateType(safetyStateType);
				if (result == null) result = caseTopologyElementType(safetyStateType);
				if (result == null) result = caseITagNameplateType(safetyStateType);
				if (result == null) result = caseBaseInterfaceType(safetyStateType);
				if (result == null) result = caseBaseObjectType(safetyStateType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.EMERGENCY_STOP_FUNCTION_TYPE: {
				EmergencyStopFunctionType emergencyStopFunctionType = (EmergencyStopFunctionType)theEObject;
				T result = caseEmergencyStopFunctionType(emergencyStopFunctionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.PROTECTIVE_STOP_FUNCTION_TYPE: {
				ProtectiveStopFunctionType protectiveStopFunctionType = (ProtectiveStopFunctionType)theEObject;
				T result = caseProtectiveStopFunctionType(protectiveStopFunctionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.REFERENCES: {
				References references = (References)theEObject;
				T result = caseReferences(references);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.HIERARCHICAL_REFERENCES: {
				HierarchicalReferences hierarchicalReferences = (HierarchicalReferences)theEObject;
				T result = caseHierarchicalReferences(hierarchicalReferences);
				if (result == null) result = caseReferences(hierarchicalReferences);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.CONTROLS: {
				Controls controls = (Controls)theEObject;
				T result = caseControls(controls);
				if (result == null) result = caseHierarchicalReferences(controls);
				if (result == null) result = caseReferences(controls);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.IS_DRIVEN_BY: {
				IsDrivenBy isDrivenBy = (IsDrivenBy)theEObject;
				T result = caseIsDrivenBy(isDrivenBy);
				if (result == null) result = caseHierarchicalReferences(isDrivenBy);
				if (result == null) result = caseReferences(isDrivenBy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.MOVES: {
				Moves moves = (Moves)theEObject;
				T result = caseMoves(moves);
				if (result == null) result = caseHierarchicalReferences(moves);
				if (result == null) result = caseReferences(moves);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.REQUIRES: {
				Requires requires = (Requires)theEObject;
				T result = caseRequires(requires);
				if (result == null) result = caseHierarchicalReferences(requires);
				if (result == null) result = caseReferences(requires);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.NON_HIERARCHICAL_REFERENCES: {
				NonHierarchicalReferences nonHierarchicalReferences = (NonHierarchicalReferences)theEObject;
				T result = caseNonHierarchicalReferences(nonHierarchicalReferences);
				if (result == null) result = caseReferences(nonHierarchicalReferences);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.HAS_SAFETY_STATES: {
				HasSafetyStates hasSafetyStates = (HasSafetyStates)theEObject;
				T result = caseHasSafetyStates(hasSafetyStates);
				if (result == null) result = caseHierarchicalReferences(hasSafetyStates);
				if (result == null) result = caseReferences(hasSafetyStates);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.HAS_SLAVES: {
				HasSlaves hasSlaves = (HasSlaves)theEObject;
				T result = caseHasSlaves(hasSlaves);
				if (result == null) result = caseHierarchicalReferences(hasSlaves);
				if (result == null) result = caseReferences(hasSlaves);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUARoboticsProfilePackage.IS_CONNECTED_TO: {
				IsConnectedTo isConnectedTo = (IsConnectedTo)theEObject;
				T result = caseIsConnectedTo(isConnectedTo);
				if (result == null) result = caseNonHierarchicalReferences(isConnectedTo);
				if (result == null) result = caseReferences(isConnectedTo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Load Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Load Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLoadType(LoadType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter Set Motor Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter Set Motor Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameterSetMotorType(ParameterSetMotorType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter Set Controller Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter Set Controller Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameterSetControllerType(ParameterSetControllerType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter Set Task Control Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter Set Task Control Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameterSetTaskControlType(ParameterSetTaskControlType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter Set Safety State Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter Set Safety State Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameterSetSafetyStateType(ParameterSetSafetyStateType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Gear Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Gear Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGearType(GearType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Motor Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Motor Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMotorType(MotorType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter Set Axis Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter Set Axis Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameterSetAxisType(ParameterSetAxisType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter Set Motion Device Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter Set Motion Device Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameterSetMotionDeviceType(ParameterSetMotionDeviceType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Power Train Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Power Train Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePowerTrainType(PowerTrainType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Motion Device System Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Motion Device System Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMotionDeviceSystemType(MotionDeviceSystemType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Motion Device Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Motion Device Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMotionDeviceType(MotionDeviceType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Axis Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Axis Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAxisType(AxisType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Controller Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Controller Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseControllerType(ControllerType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Task Control Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Task Control Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTaskControlType(TaskControlType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Safety State Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Safety State Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSafetyStateType(SafetyStateType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Emergency Stop Function Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Emergency Stop Function Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEmergencyStopFunctionType(EmergencyStopFunctionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Protective Stop Function Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Protective Stop Function Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProtectiveStopFunctionType(ProtectiveStopFunctionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>References</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>References</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReferences(References object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Hierarchical References</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Hierarchical References</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHierarchicalReferences(HierarchicalReferences object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Controls</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Controls</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseControls(Controls object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Driven By</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Driven By</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsDrivenBy(IsDrivenBy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Moves</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Moves</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMoves(Moves object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Requires</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Requires</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRequires(Requires object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Non Hierarchical References</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Non Hierarchical References</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNonHierarchicalReferences(NonHierarchicalReferences object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Safety States</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Safety States</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasSafetyStates(HasSafetyStates object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Slaves</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Slaves</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasSlaves(HasSlaves object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Connected To</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Connected To</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsConnectedTo(IsConnectedTo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Object Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Object Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseObjectType(BaseObjectType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Interface Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Interface Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseInterfaceType(BaseInterfaceType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IVendor Nameplate Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IVendor Nameplate Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIVendorNameplateType(IVendorNameplateType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Topology Element Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Topology Element Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTopologyElementType(TopologyElementType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ITag Nameplate Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ITag Nameplate Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseITagNameplateType(ITagNameplateType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentType(ComponentType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //OPCUARoboticsProfileSwitch
