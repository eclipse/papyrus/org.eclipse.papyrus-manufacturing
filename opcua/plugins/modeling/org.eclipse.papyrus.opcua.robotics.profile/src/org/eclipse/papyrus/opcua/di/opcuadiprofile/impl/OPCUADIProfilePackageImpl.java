/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.di.opcuadiprofile.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.OPC_UA_Robotics_CS_LibraryPackage;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.BlockType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.ComponentType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.ConfigurableObjectType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.ConnectionPointType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.DeviceType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.FunctionalGroupType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.IDeviceHealthType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.ISupportInfoType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.ITagNameplateType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.MethodSetTopologyElementType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfileFactory;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.OPC_UA_DI_Library.OPC_UA_DI_LibraryPackage;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.OPC_UA_DI_Library.impl.OPC_UA_DI_LibraryPackageImpl;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.ParameterSetTopologyElementType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.SoftwareType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType;

import org.eclipse.papyrus.opcua.opcuaprofile.OPCUAProfilePackage;

import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.OPC_UA_LibraryPackage;

import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl;

import org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl;

import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OPCUADIProfilePackageImpl extends EPackageImpl implements OPCUADIProfilePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deviceTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iSupportInfoTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iDeviceHealthTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionPointTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass topologyElementTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functionalGroupTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lockingServicesTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterSetTopologyElementTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass methodSetTopologyElementTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass softwareTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blockTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass configurableObjectTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iVendorNameplateTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iTagNameplateTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentTypeEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OPCUADIProfilePackageImpl() {
		super(eNS_URI, OPCUADIProfileFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link OPCUADIProfilePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static OPCUADIProfilePackage init() {
		if (isInited) return (OPCUADIProfilePackage)EPackage.Registry.INSTANCE.getEPackage(OPCUADIProfilePackage.eNS_URI);

		// Obtain or create and register package
		Object registeredOPCUADIProfilePackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		OPCUADIProfilePackageImpl theOPCUADIProfilePackage = registeredOPCUADIProfilePackage instanceof OPCUADIProfilePackageImpl ? (OPCUADIProfilePackageImpl)registeredOPCUADIProfilePackage : new OPCUADIProfilePackageImpl();

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OPCUARoboticsProfilePackage.eNS_URI);
		OPCUARoboticsProfilePackageImpl theOPCUARoboticsProfilePackage = (OPCUARoboticsProfilePackageImpl)(registeredPackage instanceof OPCUARoboticsProfilePackageImpl ? registeredPackage : OPCUARoboticsProfilePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OPC_UA_Robotics_CS_LibraryPackage.eNS_URI);
		OPC_UA_Robotics_CS_LibraryPackageImpl theOPC_UA_Robotics_CS_LibraryPackage = (OPC_UA_Robotics_CS_LibraryPackageImpl)(registeredPackage instanceof OPC_UA_Robotics_CS_LibraryPackageImpl ? registeredPackage : OPC_UA_Robotics_CS_LibraryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OPCUAProfilePackage.eNS_URI);
		OPCUAProfilePackageImpl theOPCUAProfilePackage = (OPCUAProfilePackageImpl)(registeredPackage instanceof OPCUAProfilePackageImpl ? registeredPackage : OPCUAProfilePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OPC_UA_LibraryPackage.eNS_URI);
		OPC_UA_LibraryPackageImpl theOPC_UA_LibraryPackage = (OPC_UA_LibraryPackageImpl)(registeredPackage instanceof OPC_UA_LibraryPackageImpl ? registeredPackage : OPC_UA_LibraryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OPC_UA_DI_LibraryPackage.eNS_URI);
		OPC_UA_DI_LibraryPackageImpl theOPC_UA_DI_LibraryPackage = (OPC_UA_DI_LibraryPackageImpl)(registeredPackage instanceof OPC_UA_DI_LibraryPackageImpl ? registeredPackage : OPC_UA_DI_LibraryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Library.OPC_UA_LibraryPackage.eNS_URI);
		org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl theOPC_UA_LibraryPackage_1 = (org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl)(registeredPackage instanceof org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl ? registeredPackage : org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Library.OPC_UA_LibraryPackage.eINSTANCE);

		// Create package meta-data objects
		theOPCUADIProfilePackage.createPackageContents();
		theOPCUARoboticsProfilePackage.createPackageContents();
		theOPC_UA_Robotics_CS_LibraryPackage.createPackageContents();
		theOPCUAProfilePackage.createPackageContents();
		theOPC_UA_LibraryPackage.createPackageContents();
		theOPC_UA_DI_LibraryPackage.createPackageContents();
		theOPC_UA_LibraryPackage_1.createPackageContents();

		// Initialize created meta-data
		theOPCUADIProfilePackage.initializePackageContents();
		theOPCUARoboticsProfilePackage.initializePackageContents();
		theOPC_UA_Robotics_CS_LibraryPackage.initializePackageContents();
		theOPCUAProfilePackage.initializePackageContents();
		theOPC_UA_LibraryPackage.initializePackageContents();
		theOPC_UA_DI_LibraryPackage.initializePackageContents();
		theOPC_UA_LibraryPackage_1.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theOPCUADIProfilePackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(OPCUADIProfilePackage.eNS_URI, theOPCUADIProfilePackage);
		return theOPCUADIProfilePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeviceType() {
		return deviceTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeviceType_CPIIdentifier() {
		return (EReference)deviceTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getISupportInfoType() {
		return iSupportInfoTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getISupportInfoType_DeviceTypeImage() {
		return (EReference)iSupportInfoTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getISupportInfoType_Documentation() {
		return (EReference)iSupportInfoTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getISupportInfoType_ProtocolSupport() {
		return (EReference)iSupportInfoTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getISupportInfoType_ImageSet() {
		return (EReference)iSupportInfoTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIDeviceHealthType() {
		return iDeviceHealthTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIDeviceHealthType_DeviceHealth() {
		return (EAttribute)iDeviceHealthTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConnectionPointType() {
		return connectionPointTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTopologyElementType() {
		return topologyElementTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTopologyElementType_GroupIdentifier() {
		return (EReference)topologyElementTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTopologyElementType_Identification() {
		return (EReference)topologyElementTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTopologyElementType_Lock() {
		return (EReference)topologyElementTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTopologyElementType_ParameterSet() {
		return (EReference)topologyElementTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTopologyElementType_MethodSet() {
		return (EReference)topologyElementTypeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFunctionalGroupType() {
		return functionalGroupTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLockingServicesType() {
		return lockingServicesTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLockingServicesType_DefaultInstanceBrowseName() {
		return (EReference)lockingServicesTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLockingServicesType_Locked() {
		return (EAttribute)lockingServicesTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLockingServicesType_LockingClient() {
		return (EAttribute)lockingServicesTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLockingServicesType_LockingUser() {
		return (EAttribute)lockingServicesTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLockingServicesType_RemainingLockTime() {
		return (EReference)lockingServicesTypeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLockingServicesType__InitLock__String_Int32() {
		return lockingServicesTypeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLockingServicesType__RenewLock__Int32() {
		return lockingServicesTypeEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLockingServicesType__ExitLock__Int32() {
		return lockingServicesTypeEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLockingServicesType__BreakLock__Int32() {
		return lockingServicesTypeEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParameterSetTopologyElementType() {
		return parameterSetTopologyElementTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getParameterSetTopologyElementType_ParameterIdentifier() {
		return (EReference)parameterSetTopologyElementTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMethodSetTopologyElementType() {
		return methodSetTopologyElementTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSoftwareType() {
		return softwareTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlockType() {
		return blockTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlockType_TargetMode() {
		return (EReference)blockTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlockType_RevisionCounter() {
		return (EReference)blockTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlockType_ActualMode() {
		return (EReference)blockTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlockType_PermittedMode() {
		return (EReference)blockTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlockType_NormalMode() {
		return (EReference)blockTypeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConfigurableObjectType() {
		return configurableObjectTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIVendorNameplateType() {
		return iVendorNameplateTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIVendorNameplateType_Manufacturer() {
		return (EReference)iVendorNameplateTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVendorNameplateType_ManufacturerUri() {
		return (EAttribute)iVendorNameplateTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIVendorNameplateType_Model() {
		return (EReference)iVendorNameplateTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVendorNameplateType_ProductCode() {
		return (EAttribute)iVendorNameplateTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVendorNameplateType_HardwareRevision() {
		return (EAttribute)iVendorNameplateTypeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVendorNameplateType_SoftwareRevision() {
		return (EAttribute)iVendorNameplateTypeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVendorNameplateType_DeviceRevision() {
		return (EAttribute)iVendorNameplateTypeEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVendorNameplateType_DeviceManual() {
		return (EAttribute)iVendorNameplateTypeEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVendorNameplateType_DeviceClass() {
		return (EAttribute)iVendorNameplateTypeEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVendorNameplateType_SerialNumber() {
		return (EAttribute)iVendorNameplateTypeEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVendorNameplateType_ProductInstanceUri() {
		return (EAttribute)iVendorNameplateTypeEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIVendorNameplateType_RevisionCounter() {
		return (EReference)iVendorNameplateTypeEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getITagNameplateType() {
		return iTagNameplateTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getITagNameplateType_AssetId() {
		return (EAttribute)iTagNameplateTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getITagNameplateType_ComponentName() {
		return (EReference)iTagNameplateTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponentType() {
		return componentTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentType_Base_Property() {
		return (EReference)componentTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPCUADIProfileFactory getOPCUADIProfileFactory() {
		return (OPCUADIProfileFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		deviceTypeEClass = createEClass(DEVICE_TYPE);
		createEReference(deviceTypeEClass, DEVICE_TYPE__CPI_IDENTIFIER);

		iSupportInfoTypeEClass = createEClass(ISUPPORT_INFO_TYPE);
		createEReference(iSupportInfoTypeEClass, ISUPPORT_INFO_TYPE__DEVICE_TYPE_IMAGE);
		createEReference(iSupportInfoTypeEClass, ISUPPORT_INFO_TYPE__DOCUMENTATION);
		createEReference(iSupportInfoTypeEClass, ISUPPORT_INFO_TYPE__PROTOCOL_SUPPORT);
		createEReference(iSupportInfoTypeEClass, ISUPPORT_INFO_TYPE__IMAGE_SET);

		iDeviceHealthTypeEClass = createEClass(IDEVICE_HEALTH_TYPE);
		createEAttribute(iDeviceHealthTypeEClass, IDEVICE_HEALTH_TYPE__DEVICE_HEALTH);

		connectionPointTypeEClass = createEClass(CONNECTION_POINT_TYPE);

		topologyElementTypeEClass = createEClass(TOPOLOGY_ELEMENT_TYPE);
		createEReference(topologyElementTypeEClass, TOPOLOGY_ELEMENT_TYPE__GROUP_IDENTIFIER);
		createEReference(topologyElementTypeEClass, TOPOLOGY_ELEMENT_TYPE__IDENTIFICATION);
		createEReference(topologyElementTypeEClass, TOPOLOGY_ELEMENT_TYPE__LOCK);
		createEReference(topologyElementTypeEClass, TOPOLOGY_ELEMENT_TYPE__PARAMETER_SET);
		createEReference(topologyElementTypeEClass, TOPOLOGY_ELEMENT_TYPE__METHOD_SET);

		functionalGroupTypeEClass = createEClass(FUNCTIONAL_GROUP_TYPE);

		lockingServicesTypeEClass = createEClass(LOCKING_SERVICES_TYPE);
		createEReference(lockingServicesTypeEClass, LOCKING_SERVICES_TYPE__DEFAULT_INSTANCE_BROWSE_NAME);
		createEAttribute(lockingServicesTypeEClass, LOCKING_SERVICES_TYPE__LOCKED);
		createEAttribute(lockingServicesTypeEClass, LOCKING_SERVICES_TYPE__LOCKING_CLIENT);
		createEAttribute(lockingServicesTypeEClass, LOCKING_SERVICES_TYPE__LOCKING_USER);
		createEReference(lockingServicesTypeEClass, LOCKING_SERVICES_TYPE__REMAINING_LOCK_TIME);
		createEOperation(lockingServicesTypeEClass, LOCKING_SERVICES_TYPE___INIT_LOCK__STRING_INT32);
		createEOperation(lockingServicesTypeEClass, LOCKING_SERVICES_TYPE___RENEW_LOCK__INT32);
		createEOperation(lockingServicesTypeEClass, LOCKING_SERVICES_TYPE___EXIT_LOCK__INT32);
		createEOperation(lockingServicesTypeEClass, LOCKING_SERVICES_TYPE___BREAK_LOCK__INT32);

		parameterSetTopologyElementTypeEClass = createEClass(PARAMETER_SET_TOPOLOGY_ELEMENT_TYPE);
		createEReference(parameterSetTopologyElementTypeEClass, PARAMETER_SET_TOPOLOGY_ELEMENT_TYPE__PARAMETER_IDENTIFIER);

		methodSetTopologyElementTypeEClass = createEClass(METHOD_SET_TOPOLOGY_ELEMENT_TYPE);

		softwareTypeEClass = createEClass(SOFTWARE_TYPE);

		blockTypeEClass = createEClass(BLOCK_TYPE);
		createEReference(blockTypeEClass, BLOCK_TYPE__TARGET_MODE);
		createEReference(blockTypeEClass, BLOCK_TYPE__REVISION_COUNTER);
		createEReference(blockTypeEClass, BLOCK_TYPE__ACTUAL_MODE);
		createEReference(blockTypeEClass, BLOCK_TYPE__PERMITTED_MODE);
		createEReference(blockTypeEClass, BLOCK_TYPE__NORMAL_MODE);

		configurableObjectTypeEClass = createEClass(CONFIGURABLE_OBJECT_TYPE);

		iVendorNameplateTypeEClass = createEClass(IVENDOR_NAMEPLATE_TYPE);
		createEReference(iVendorNameplateTypeEClass, IVENDOR_NAMEPLATE_TYPE__MANUFACTURER);
		createEAttribute(iVendorNameplateTypeEClass, IVENDOR_NAMEPLATE_TYPE__MANUFACTURER_URI);
		createEReference(iVendorNameplateTypeEClass, IVENDOR_NAMEPLATE_TYPE__MODEL);
		createEAttribute(iVendorNameplateTypeEClass, IVENDOR_NAMEPLATE_TYPE__PRODUCT_CODE);
		createEAttribute(iVendorNameplateTypeEClass, IVENDOR_NAMEPLATE_TYPE__HARDWARE_REVISION);
		createEAttribute(iVendorNameplateTypeEClass, IVENDOR_NAMEPLATE_TYPE__SOFTWARE_REVISION);
		createEAttribute(iVendorNameplateTypeEClass, IVENDOR_NAMEPLATE_TYPE__DEVICE_REVISION);
		createEAttribute(iVendorNameplateTypeEClass, IVENDOR_NAMEPLATE_TYPE__DEVICE_MANUAL);
		createEAttribute(iVendorNameplateTypeEClass, IVENDOR_NAMEPLATE_TYPE__DEVICE_CLASS);
		createEAttribute(iVendorNameplateTypeEClass, IVENDOR_NAMEPLATE_TYPE__SERIAL_NUMBER);
		createEAttribute(iVendorNameplateTypeEClass, IVENDOR_NAMEPLATE_TYPE__PRODUCT_INSTANCE_URI);
		createEReference(iVendorNameplateTypeEClass, IVENDOR_NAMEPLATE_TYPE__REVISION_COUNTER);

		iTagNameplateTypeEClass = createEClass(ITAG_NAMEPLATE_TYPE);
		createEAttribute(iTagNameplateTypeEClass, ITAG_NAMEPLATE_TYPE__ASSET_ID);
		createEReference(iTagNameplateTypeEClass, ITAG_NAMEPLATE_TYPE__COMPONENT_NAME);

		componentTypeEClass = createEClass(COMPONENT_TYPE);
		createEReference(componentTypeEClass, COMPONENT_TYPE__BASE_PROPERTY);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		OPCUAProfilePackage theOPCUAProfilePackage = (OPCUAProfilePackage)EPackage.Registry.INSTANCE.getEPackage(OPCUAProfilePackage.eNS_URI);
		OPC_UA_DI_LibraryPackage theOPC_UA_DI_LibraryPackage = (OPC_UA_DI_LibraryPackage)EPackage.Registry.INSTANCE.getEPackage(OPC_UA_DI_LibraryPackage.eNS_URI);
		OPC_UA_LibraryPackage theOPC_UA_LibraryPackage = (OPC_UA_LibraryPackage)EPackage.Registry.INSTANCE.getEPackage(OPC_UA_LibraryPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theOPCUAProfilePackage);
		getESubpackages().add(theOPC_UA_DI_LibraryPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		deviceTypeEClass.getESuperTypes().add(this.getComponentType());
		deviceTypeEClass.getESuperTypes().add(this.getISupportInfoType());
		deviceTypeEClass.getESuperTypes().add(this.getIDeviceHealthType());
		iSupportInfoTypeEClass.getESuperTypes().add(theOPCUAProfilePackage.getBaseInterfaceType());
		iDeviceHealthTypeEClass.getESuperTypes().add(theOPCUAProfilePackage.getBaseInterfaceType());
		connectionPointTypeEClass.getESuperTypes().add(this.getTopologyElementType());
		topologyElementTypeEClass.getESuperTypes().add(theOPCUAProfilePackage.getBaseObjectType());
		functionalGroupTypeEClass.getESuperTypes().add(theOPCUAProfilePackage.getFolderType());
		lockingServicesTypeEClass.getESuperTypes().add(theOPCUAProfilePackage.getBaseObjectType());
		parameterSetTopologyElementTypeEClass.getESuperTypes().add(theOPCUAProfilePackage.getBaseObjectType());
		softwareTypeEClass.getESuperTypes().add(this.getComponentType());
		blockTypeEClass.getESuperTypes().add(this.getTopologyElementType());
		configurableObjectTypeEClass.getESuperTypes().add(theOPCUAProfilePackage.getBaseObjectType());
		iVendorNameplateTypeEClass.getESuperTypes().add(theOPCUAProfilePackage.getBaseInterfaceType());
		iTagNameplateTypeEClass.getESuperTypes().add(theOPCUAProfilePackage.getBaseInterfaceType());
		componentTypeEClass.getESuperTypes().add(this.getIVendorNameplateType());
		componentTypeEClass.getESuperTypes().add(this.getTopologyElementType());
		componentTypeEClass.getESuperTypes().add(this.getITagNameplateType());

		// Initialize classes, features, and operations; add parameters
		initEClass(deviceTypeEClass, DeviceType.class, "DeviceType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDeviceType_CPIIdentifier(), this.getConnectionPointType(), null, "CPIIdentifier", null, 1, 1, DeviceType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(iSupportInfoTypeEClass, ISupportInfoType.class, "ISupportInfoType", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISupportInfoType_DeviceTypeImage(), theOPC_UA_LibraryPackage.getImage(), null, "DeviceTypeImage", null, 0, -1, ISupportInfoType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getISupportInfoType_Documentation(), theOPC_UA_LibraryPackage.getByteString(), null, "Documentation", null, 0, -1, ISupportInfoType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getISupportInfoType_ProtocolSupport(), theOPC_UA_LibraryPackage.getByteString(), null, "ProtocolSupport", null, 0, -1, ISupportInfoType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getISupportInfoType_ImageSet(), theOPC_UA_LibraryPackage.getImage(), null, "ImageSet", null, 0, -1, ISupportInfoType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(iDeviceHealthTypeEClass, IDeviceHealthType.class, "IDeviceHealthType", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIDeviceHealthType_DeviceHealth(), theOPC_UA_DI_LibraryPackage.getDeviceHealthEnumeration(), "DeviceHealth", null, 1, 1, IDeviceHealthType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(connectionPointTypeEClass, ConnectionPointType.class, "ConnectionPointType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(topologyElementTypeEClass, TopologyElementType.class, "TopologyElementType", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTopologyElementType_GroupIdentifier(), this.getFunctionalGroupType(), null, "GroupIdentifier", null, 0, 1, TopologyElementType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTopologyElementType_Identification(), this.getFunctionalGroupType(), null, "Identification", null, 0, 1, TopologyElementType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTopologyElementType_Lock(), this.getLockingServicesType(), null, "Lock", null, 0, 1, TopologyElementType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTopologyElementType_ParameterSet(), this.getParameterSetTopologyElementType(), null, "ParameterSet", null, 0, 1, TopologyElementType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTopologyElementType_MethodSet(), this.getMethodSetTopologyElementType(), null, "MethodSet", null, 0, 1, TopologyElementType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(functionalGroupTypeEClass, FunctionalGroupType.class, "FunctionalGroupType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(lockingServicesTypeEClass, LockingServicesType.class, "LockingServicesType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLockingServicesType_DefaultInstanceBrowseName(), theOPC_UA_LibraryPackage.getQualifiedName(), null, "DefaultInstanceBrowseName", null, 1, 1, LockingServicesType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getLockingServicesType_Locked(), theTypesPackage.getBoolean(), "Locked", null, 1, 1, LockingServicesType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getLockingServicesType_LockingClient(), theTypesPackage.getString(), "LockingClient", null, 1, 1, LockingServicesType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getLockingServicesType_LockingUser(), theTypesPackage.getString(), "LockingUser", null, 1, 1, LockingServicesType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getLockingServicesType_RemainingLockTime(), theOPC_UA_LibraryPackage.getDuration(), null, "RemainingLockTime", null, 1, 1, LockingServicesType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		EOperation op = initEOperation(getLockingServicesType__InitLock__String_Int32(), null, "InitLock", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theTypesPackage.getString(), "Context", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theOPC_UA_LibraryPackage.getInt32(), "InitLockStatus", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getLockingServicesType__RenewLock__Int32(), null, "RenewLock", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theOPC_UA_LibraryPackage.getInt32(), "RenewLockStatus", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getLockingServicesType__ExitLock__Int32(), null, "ExitLock", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theOPC_UA_LibraryPackage.getInt32(), "ExitLockStatus", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getLockingServicesType__BreakLock__Int32(), null, "BreakLock", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theOPC_UA_LibraryPackage.getInt32(), "BreakLockStatus", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(parameterSetTopologyElementTypeEClass, ParameterSetTopologyElementType.class, "ParameterSetTopologyElementType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getParameterSetTopologyElementType_ParameterIdentifier(), theOPCUAProfilePackage.getBaseDataVariableType(), null, "ParameterIdentifier", null, 1, 1, ParameterSetTopologyElementType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(methodSetTopologyElementTypeEClass, MethodSetTopologyElementType.class, "MethodSetTopologyElementType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(softwareTypeEClass, SoftwareType.class, "SoftwareType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(blockTypeEClass, BlockType.class, "BlockType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBlockType_TargetMode(), theOPC_UA_LibraryPackage.getLocalizedText(), null, "TargetMode", null, 1, 1, BlockType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getBlockType_RevisionCounter(), theOPC_UA_LibraryPackage.getInt32(), null, "RevisionCounter", null, 1, 1, BlockType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getBlockType_ActualMode(), theOPC_UA_LibraryPackage.getLocalizedText(), null, "ActualMode", null, 1, 1, BlockType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getBlockType_PermittedMode(), theOPC_UA_LibraryPackage.getLocalizedText(), null, "PermittedMode", null, 1, 1, BlockType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getBlockType_NormalMode(), theOPC_UA_LibraryPackage.getLocalizedText(), null, "NormalMode", null, 1, 1, BlockType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(configurableObjectTypeEClass, ConfigurableObjectType.class, "ConfigurableObjectType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(iVendorNameplateTypeEClass, IVendorNameplateType.class, "IVendorNameplateType", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIVendorNameplateType_Manufacturer(), theOPC_UA_LibraryPackage.getLocalizedText(), null, "Manufacturer", null, 1, 1, IVendorNameplateType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getIVendorNameplateType_ManufacturerUri(), theTypesPackage.getString(), "ManufacturerUri", null, 0, 1, IVendorNameplateType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getIVendorNameplateType_Model(), theOPC_UA_LibraryPackage.getLocalizedText(), null, "Model", null, 0, 1, IVendorNameplateType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getIVendorNameplateType_ProductCode(), theTypesPackage.getString(), "ProductCode", null, 0, 1, IVendorNameplateType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getIVendorNameplateType_HardwareRevision(), theTypesPackage.getString(), "HardwareRevision", null, 0, 1, IVendorNameplateType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getIVendorNameplateType_SoftwareRevision(), theTypesPackage.getString(), "SoftwareRevision", null, 1, 1, IVendorNameplateType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getIVendorNameplateType_DeviceRevision(), theTypesPackage.getString(), "DeviceRevision", null, 0, 1, IVendorNameplateType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getIVendorNameplateType_DeviceManual(), theTypesPackage.getString(), "DeviceManual", null, 0, 1, IVendorNameplateType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getIVendorNameplateType_DeviceClass(), theTypesPackage.getString(), "DeviceClass", null, 0, 1, IVendorNameplateType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getIVendorNameplateType_SerialNumber(), theTypesPackage.getString(), "SerialNumber", null, 0, 1, IVendorNameplateType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getIVendorNameplateType_ProductInstanceUri(), theTypesPackage.getString(), "ProductInstanceUri", null, 0, 1, IVendorNameplateType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getIVendorNameplateType_RevisionCounter(), theOPC_UA_LibraryPackage.getInt32(), null, "RevisionCounter", null, 0, 1, IVendorNameplateType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(iTagNameplateTypeEClass, ITagNameplateType.class, "ITagNameplateType", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getITagNameplateType_AssetId(), theTypesPackage.getString(), "AssetId", null, 0, 1, ITagNameplateType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getITagNameplateType_ComponentName(), theOPC_UA_LibraryPackage.getLocalizedText(), null, "ComponentName", null, 0, 1, ITagNameplateType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(componentTypeEClass, ComponentType.class, "ComponentType", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComponentType_Base_Property(), theUMLPackage.getProperty(), null, "base_Property", null, 0, 1, ComponentType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		// Create annotations
		// http://www.eclipse.org/uml2/2.0.0/UML
		createUMLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/uml2/2.0.0/UML</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createUMLAnnotations() {
		String source = "http://www.eclipse.org/uml2/2.0.0/UML";
		addAnnotation
		  (this,
		   source,
		   new String[] {
			   "originalName", "OPC_UA_DI"
		   });
	}

} //OPCUADIProfilePackageImpl
