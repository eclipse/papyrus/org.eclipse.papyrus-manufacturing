/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile;

import org.eclipse.emf.common.util.EList;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.ComponentType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Motion Device System Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceSystemType#getMotionDevices <em>Motion Devices</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceSystemType#getControllers <em>Controllers</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceSystemType#getSafetyStates <em>Safety States</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getMotionDeviceSystemType()
 * @model
 * @generated
 */
public interface MotionDeviceSystemType extends ComponentType {
	/**
	 * Returns the value of the '<em><b>Motion Devices</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Motion Devices</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Motion Devices</em>' reference list.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getMotionDeviceSystemType_MotionDevices()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	EList<MotionDeviceType> getMotionDevices();

	/**
	 * Returns the value of the '<em><b>Controllers</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ControllerType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controllers</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controllers</em>' reference list.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getMotionDeviceSystemType_Controllers()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	EList<ControllerType> getControllers();

	/**
	 * Returns the value of the '<em><b>Safety States</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.SafetyStateType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Safety States</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Safety States</em>' reference list.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getMotionDeviceSystemType_SafetyStates()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	EList<SafetyStateType> getSafetyStates();

} // MotionDeviceSystemType
