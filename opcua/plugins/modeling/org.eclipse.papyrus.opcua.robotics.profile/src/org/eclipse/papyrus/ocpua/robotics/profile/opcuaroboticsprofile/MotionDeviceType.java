/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile;

import org.eclipse.emf.common.util.EList;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.ComponentType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Motion Device Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType#getMotionDeviceCategory <em>Motion Device Category</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType#getParameterSetMotionDeviceType <em>Parameter Set Motion Device Type</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType#getAxes <em>Axes</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType#getPowerTrains <em>Power Trains</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType#getAdditionalComponents <em>Additional Components</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType#getFlangeLoad <em>Flange Load</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getMotionDeviceType()
 * @model
 * @generated
 */
public interface MotionDeviceType extends ComponentType {
	/**
	 * Returns the value of the '<em><b>Motion Device Category</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceCategoryEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Motion Device Category</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Motion Device Category</em>' attribute.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceCategoryEnum
	 * @see #setMotionDeviceCategory(MotionDeviceCategoryEnum)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getMotionDeviceType_MotionDeviceCategory()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	MotionDeviceCategoryEnum getMotionDeviceCategory();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType#getMotionDeviceCategory <em>Motion Device Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Motion Device Category</em>' attribute.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceCategoryEnum
	 * @see #getMotionDeviceCategory()
	 * @generated
	 */
	void setMotionDeviceCategory(MotionDeviceCategoryEnum value);

	/**
	 * Returns the value of the '<em><b>Parameter Set Motion Device Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Set Motion Device Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Set Motion Device Type</em>' containment reference.
	 * @see #setParameterSetMotionDeviceType(ParameterSetMotionDeviceType)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getMotionDeviceType_ParameterSetMotionDeviceType()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	ParameterSetMotionDeviceType getParameterSetMotionDeviceType();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType#getParameterSetMotionDeviceType <em>Parameter Set Motion Device Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Set Motion Device Type</em>' containment reference.
	 * @see #getParameterSetMotionDeviceType()
	 * @generated
	 */
	void setParameterSetMotionDeviceType(ParameterSetMotionDeviceType value);

	/**
	 * Returns the value of the '<em><b>Axes</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Axes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Axes</em>' reference list.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getMotionDeviceType_Axes()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	EList<AxisType> getAxes();

	/**
	 * Returns the value of the '<em><b>Power Trains</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.PowerTrainType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Power Trains</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Power Trains</em>' reference list.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getMotionDeviceType_PowerTrains()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	EList<PowerTrainType> getPowerTrains();

	/**
	 * Returns the value of the '<em><b>Additional Components</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ComponentType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Additional Components</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Additional Components</em>' reference list.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getMotionDeviceType_AdditionalComponents()
	 * @model ordered="false"
	 * @generated
	 */
	EList<ComponentType> getAdditionalComponents();

	/**
	 * Returns the value of the '<em><b>Flange Load</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Flange Load</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Flange Load</em>' containment reference.
	 * @see #setFlangeLoad(LoadType)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getMotionDeviceType_FlangeLoad()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	LoadType getFlangeLoad();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType#getFlangeLoad <em>Flange Load</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Flange Load</em>' containment reference.
	 * @see #getFlangeLoad()
	 * @generated
	 */
	void setFlangeLoad(LoadType value);

} // MotionDeviceType
