/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile;

import org.eclipse.emf.common.util.EList;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.ComponentType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.SoftwareType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Controller Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ControllerType#getParameterSetControllerType <em>Parameter Set Controller Type</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ControllerType#getComponents <em>Components</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ControllerType#getSoftware <em>Software</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ControllerType#getTaskControls <em>Task Controls</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getControllerType()
 * @model
 * @generated
 */
public interface ControllerType extends ComponentType {
	/**
	 * Returns the value of the '<em><b>Parameter Set Controller Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Set Controller Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Set Controller Type</em>' containment reference.
	 * @see #setParameterSetControllerType(ParameterSetControllerType)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getControllerType_ParameterSetControllerType()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	ParameterSetControllerType getParameterSetControllerType();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ControllerType#getParameterSetControllerType <em>Parameter Set Controller Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Set Controller Type</em>' containment reference.
	 * @see #getParameterSetControllerType()
	 * @generated
	 */
	void setParameterSetControllerType(ParameterSetControllerType value);

	/**
	 * Returns the value of the '<em><b>Components</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ComponentType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Components</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Components</em>' reference list.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getControllerType_Components()
	 * @model ordered="false"
	 * @generated
	 */
	EList<ComponentType> getComponents();

	/**
	 * Returns the value of the '<em><b>Software</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Software</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Software</em>' reference.
	 * @see #setSoftware(SoftwareType)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getControllerType_Software()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	SoftwareType getSoftware();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ControllerType#getSoftware <em>Software</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Software</em>' reference.
	 * @see #getSoftware()
	 * @generated
	 */
	void setSoftware(SoftwareType value);

	/**
	 * Returns the value of the '<em><b>Task Controls</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.TaskControlType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Controls</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Controls</em>' reference list.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getControllerType_TaskControls()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	EList<TaskControlType> getTaskControls();

} // ControllerType
