/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.di.opcuadiprofile;

import org.eclipse.papyrus.opcua.opcuaprofile.BaseInterfaceType;

import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.LocalizedText;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ITag Nameplate Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ITagNameplateType#getAssetId <em>Asset Id</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ITagNameplateType#getComponentName <em>Component Name</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getITagNameplateType()
 * @model abstract="true"
 * @generated
 */
public interface ITagNameplateType extends BaseInterfaceType {
	/**
	 * Returns the value of the '<em><b>Asset Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asset Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asset Id</em>' attribute.
	 * @see #setAssetId(String)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getITagNameplateType_AssetId()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getAssetId();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ITagNameplateType#getAssetId <em>Asset Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Asset Id</em>' attribute.
	 * @see #getAssetId()
	 * @generated
	 */
	void setAssetId(String value);

	/**
	 * Returns the value of the '<em><b>Component Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Name</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Name</em>' containment reference.
	 * @see #setComponentName(LocalizedText)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getITagNameplateType_ComponentName()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	LocalizedText getComponentName();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ITagNameplateType#getComponentName <em>Component Name</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component Name</em>' containment reference.
	 * @see #getComponentName()
	 * @generated
	 */
	void setComponentName(LocalizedText value);

} // ITagNameplateType
