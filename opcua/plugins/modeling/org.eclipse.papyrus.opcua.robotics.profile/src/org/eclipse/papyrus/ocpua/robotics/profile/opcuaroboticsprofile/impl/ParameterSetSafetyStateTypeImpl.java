/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OperationalModeEnumeration;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetSafetyStateType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameter Set Safety State Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetSafetyStateTypeImpl#isEmergencyStop <em>Emergency Stop</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetSafetyStateTypeImpl#getOperationalMode <em>Operational Mode</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetSafetyStateTypeImpl#isProtectiveStop <em>Protective Stop</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ParameterSetSafetyStateTypeImpl extends MinimalEObjectImpl.Container implements ParameterSetSafetyStateType {
	/**
	 * The default value of the '{@link #isEmergencyStop() <em>Emergency Stop</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEmergencyStop()
	 * @generated
	 * @ordered
	 */
	protected static final boolean EMERGENCY_STOP_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isEmergencyStop() <em>Emergency Stop</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEmergencyStop()
	 * @generated
	 * @ordered
	 */
	protected boolean emergencyStop = EMERGENCY_STOP_EDEFAULT;

	/**
	 * The default value of the '{@link #getOperationalMode() <em>Operational Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationalMode()
	 * @generated
	 * @ordered
	 */
	protected static final OperationalModeEnumeration OPERATIONAL_MODE_EDEFAULT = OperationalModeEnumeration.OTHER;

	/**
	 * The cached value of the '{@link #getOperationalMode() <em>Operational Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationalMode()
	 * @generated
	 * @ordered
	 */
	protected OperationalModeEnumeration operationalMode = OPERATIONAL_MODE_EDEFAULT;

	/**
	 * The default value of the '{@link #isProtectiveStop() <em>Protective Stop</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isProtectiveStop()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PROTECTIVE_STOP_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isProtectiveStop() <em>Protective Stop</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isProtectiveStop()
	 * @generated
	 * @ordered
	 */
	protected boolean protectiveStop = PROTECTIVE_STOP_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParameterSetSafetyStateTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUARoboticsProfilePackage.Literals.PARAMETER_SET_SAFETY_STATE_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEmergencyStop() {
		return emergencyStop;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEmergencyStop(boolean newEmergencyStop) {
		boolean oldEmergencyStop = emergencyStop;
		emergencyStop = newEmergencyStop;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_SAFETY_STATE_TYPE__EMERGENCY_STOP, oldEmergencyStop, emergencyStop));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationalModeEnumeration getOperationalMode() {
		return operationalMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationalMode(OperationalModeEnumeration newOperationalMode) {
		OperationalModeEnumeration oldOperationalMode = operationalMode;
		operationalMode = newOperationalMode == null ? OPERATIONAL_MODE_EDEFAULT : newOperationalMode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_SAFETY_STATE_TYPE__OPERATIONAL_MODE, oldOperationalMode, operationalMode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isProtectiveStop() {
		return protectiveStop;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProtectiveStop(boolean newProtectiveStop) {
		boolean oldProtectiveStop = protectiveStop;
		protectiveStop = newProtectiveStop;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_SAFETY_STATE_TYPE__PROTECTIVE_STOP, oldProtectiveStop, protectiveStop));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.PARAMETER_SET_SAFETY_STATE_TYPE__EMERGENCY_STOP:
				return isEmergencyStop();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_SAFETY_STATE_TYPE__OPERATIONAL_MODE:
				return getOperationalMode();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_SAFETY_STATE_TYPE__PROTECTIVE_STOP:
				return isProtectiveStop();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.PARAMETER_SET_SAFETY_STATE_TYPE__EMERGENCY_STOP:
				setEmergencyStop((Boolean)newValue);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_SAFETY_STATE_TYPE__OPERATIONAL_MODE:
				setOperationalMode((OperationalModeEnumeration)newValue);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_SAFETY_STATE_TYPE__PROTECTIVE_STOP:
				setProtectiveStop((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.PARAMETER_SET_SAFETY_STATE_TYPE__EMERGENCY_STOP:
				setEmergencyStop(EMERGENCY_STOP_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_SAFETY_STATE_TYPE__OPERATIONAL_MODE:
				setOperationalMode(OPERATIONAL_MODE_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_SAFETY_STATE_TYPE__PROTECTIVE_STOP:
				setProtectiveStop(PROTECTIVE_STOP_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.PARAMETER_SET_SAFETY_STATE_TYPE__EMERGENCY_STOP:
				return emergencyStop != EMERGENCY_STOP_EDEFAULT;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_SAFETY_STATE_TYPE__OPERATIONAL_MODE:
				return operationalMode != OPERATIONAL_MODE_EDEFAULT;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_SAFETY_STATE_TYPE__PROTECTIVE_STOP:
				return protectiveStop != PROTECTIVE_STOP_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (EmergencyStop: ");
		result.append(emergencyStop);
		result.append(", OperationalMode: ");
		result.append(operationalMode);
		result.append(", ProtectiveStop: ");
		result.append(protectiveStop);
		result.append(')');
		return result.toString();
	}

} //ParameterSetSafetyStateTypeImpl
