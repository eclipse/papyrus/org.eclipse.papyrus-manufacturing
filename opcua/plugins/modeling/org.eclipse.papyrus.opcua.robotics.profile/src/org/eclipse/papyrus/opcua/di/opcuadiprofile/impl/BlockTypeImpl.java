/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.di.opcuadiprofile.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.BlockType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage;

import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int32;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.LocalizedText;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Block Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.BlockTypeImpl#getTargetMode <em>Target Mode</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.BlockTypeImpl#getRevisionCounter <em>Revision Counter</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.BlockTypeImpl#getActualMode <em>Actual Mode</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.BlockTypeImpl#getPermittedMode <em>Permitted Mode</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.BlockTypeImpl#getNormalMode <em>Normal Mode</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BlockTypeImpl extends TopologyElementTypeImpl implements BlockType {
	/**
	 * The cached value of the '{@link #getTargetMode() <em>Target Mode</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetMode()
	 * @generated
	 * @ordered
	 */
	protected LocalizedText targetMode;

	/**
	 * The cached value of the '{@link #getRevisionCounter() <em>Revision Counter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRevisionCounter()
	 * @generated
	 * @ordered
	 */
	protected Int32 revisionCounter;

	/**
	 * The cached value of the '{@link #getActualMode() <em>Actual Mode</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActualMode()
	 * @generated
	 * @ordered
	 */
	protected LocalizedText actualMode;

	/**
	 * The cached value of the '{@link #getPermittedMode() <em>Permitted Mode</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPermittedMode()
	 * @generated
	 * @ordered
	 */
	protected LocalizedText permittedMode;

	/**
	 * The cached value of the '{@link #getNormalMode() <em>Normal Mode</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNormalMode()
	 * @generated
	 * @ordered
	 */
	protected LocalizedText normalMode;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BlockTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUADIProfilePackage.Literals.BLOCK_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocalizedText getTargetMode() {
		return targetMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTargetMode(LocalizedText newTargetMode, NotificationChain msgs) {
		LocalizedText oldTargetMode = targetMode;
		targetMode = newTargetMode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.BLOCK_TYPE__TARGET_MODE, oldTargetMode, newTargetMode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetMode(LocalizedText newTargetMode) {
		if (newTargetMode != targetMode) {
			NotificationChain msgs = null;
			if (targetMode != null)
				msgs = ((InternalEObject)targetMode).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.BLOCK_TYPE__TARGET_MODE, null, msgs);
			if (newTargetMode != null)
				msgs = ((InternalEObject)newTargetMode).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.BLOCK_TYPE__TARGET_MODE, null, msgs);
			msgs = basicSetTargetMode(newTargetMode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.BLOCK_TYPE__TARGET_MODE, newTargetMode, newTargetMode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Int32 getRevisionCounter() {
		return revisionCounter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRevisionCounter(Int32 newRevisionCounter, NotificationChain msgs) {
		Int32 oldRevisionCounter = revisionCounter;
		revisionCounter = newRevisionCounter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.BLOCK_TYPE__REVISION_COUNTER, oldRevisionCounter, newRevisionCounter);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRevisionCounter(Int32 newRevisionCounter) {
		if (newRevisionCounter != revisionCounter) {
			NotificationChain msgs = null;
			if (revisionCounter != null)
				msgs = ((InternalEObject)revisionCounter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.BLOCK_TYPE__REVISION_COUNTER, null, msgs);
			if (newRevisionCounter != null)
				msgs = ((InternalEObject)newRevisionCounter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.BLOCK_TYPE__REVISION_COUNTER, null, msgs);
			msgs = basicSetRevisionCounter(newRevisionCounter, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.BLOCK_TYPE__REVISION_COUNTER, newRevisionCounter, newRevisionCounter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocalizedText getActualMode() {
		return actualMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetActualMode(LocalizedText newActualMode, NotificationChain msgs) {
		LocalizedText oldActualMode = actualMode;
		actualMode = newActualMode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.BLOCK_TYPE__ACTUAL_MODE, oldActualMode, newActualMode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActualMode(LocalizedText newActualMode) {
		if (newActualMode != actualMode) {
			NotificationChain msgs = null;
			if (actualMode != null)
				msgs = ((InternalEObject)actualMode).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.BLOCK_TYPE__ACTUAL_MODE, null, msgs);
			if (newActualMode != null)
				msgs = ((InternalEObject)newActualMode).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.BLOCK_TYPE__ACTUAL_MODE, null, msgs);
			msgs = basicSetActualMode(newActualMode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.BLOCK_TYPE__ACTUAL_MODE, newActualMode, newActualMode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocalizedText getPermittedMode() {
		return permittedMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPermittedMode(LocalizedText newPermittedMode, NotificationChain msgs) {
		LocalizedText oldPermittedMode = permittedMode;
		permittedMode = newPermittedMode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.BLOCK_TYPE__PERMITTED_MODE, oldPermittedMode, newPermittedMode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPermittedMode(LocalizedText newPermittedMode) {
		if (newPermittedMode != permittedMode) {
			NotificationChain msgs = null;
			if (permittedMode != null)
				msgs = ((InternalEObject)permittedMode).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.BLOCK_TYPE__PERMITTED_MODE, null, msgs);
			if (newPermittedMode != null)
				msgs = ((InternalEObject)newPermittedMode).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.BLOCK_TYPE__PERMITTED_MODE, null, msgs);
			msgs = basicSetPermittedMode(newPermittedMode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.BLOCK_TYPE__PERMITTED_MODE, newPermittedMode, newPermittedMode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocalizedText getNormalMode() {
		return normalMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNormalMode(LocalizedText newNormalMode, NotificationChain msgs) {
		LocalizedText oldNormalMode = normalMode;
		normalMode = newNormalMode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.BLOCK_TYPE__NORMAL_MODE, oldNormalMode, newNormalMode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNormalMode(LocalizedText newNormalMode) {
		if (newNormalMode != normalMode) {
			NotificationChain msgs = null;
			if (normalMode != null)
				msgs = ((InternalEObject)normalMode).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.BLOCK_TYPE__NORMAL_MODE, null, msgs);
			if (newNormalMode != null)
				msgs = ((InternalEObject)newNormalMode).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.BLOCK_TYPE__NORMAL_MODE, null, msgs);
			msgs = basicSetNormalMode(newNormalMode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.BLOCK_TYPE__NORMAL_MODE, newNormalMode, newNormalMode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OPCUADIProfilePackage.BLOCK_TYPE__TARGET_MODE:
				return basicSetTargetMode(null, msgs);
			case OPCUADIProfilePackage.BLOCK_TYPE__REVISION_COUNTER:
				return basicSetRevisionCounter(null, msgs);
			case OPCUADIProfilePackage.BLOCK_TYPE__ACTUAL_MODE:
				return basicSetActualMode(null, msgs);
			case OPCUADIProfilePackage.BLOCK_TYPE__PERMITTED_MODE:
				return basicSetPermittedMode(null, msgs);
			case OPCUADIProfilePackage.BLOCK_TYPE__NORMAL_MODE:
				return basicSetNormalMode(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPCUADIProfilePackage.BLOCK_TYPE__TARGET_MODE:
				return getTargetMode();
			case OPCUADIProfilePackage.BLOCK_TYPE__REVISION_COUNTER:
				return getRevisionCounter();
			case OPCUADIProfilePackage.BLOCK_TYPE__ACTUAL_MODE:
				return getActualMode();
			case OPCUADIProfilePackage.BLOCK_TYPE__PERMITTED_MODE:
				return getPermittedMode();
			case OPCUADIProfilePackage.BLOCK_TYPE__NORMAL_MODE:
				return getNormalMode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPCUADIProfilePackage.BLOCK_TYPE__TARGET_MODE:
				setTargetMode((LocalizedText)newValue);
				return;
			case OPCUADIProfilePackage.BLOCK_TYPE__REVISION_COUNTER:
				setRevisionCounter((Int32)newValue);
				return;
			case OPCUADIProfilePackage.BLOCK_TYPE__ACTUAL_MODE:
				setActualMode((LocalizedText)newValue);
				return;
			case OPCUADIProfilePackage.BLOCK_TYPE__PERMITTED_MODE:
				setPermittedMode((LocalizedText)newValue);
				return;
			case OPCUADIProfilePackage.BLOCK_TYPE__NORMAL_MODE:
				setNormalMode((LocalizedText)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPCUADIProfilePackage.BLOCK_TYPE__TARGET_MODE:
				setTargetMode((LocalizedText)null);
				return;
			case OPCUADIProfilePackage.BLOCK_TYPE__REVISION_COUNTER:
				setRevisionCounter((Int32)null);
				return;
			case OPCUADIProfilePackage.BLOCK_TYPE__ACTUAL_MODE:
				setActualMode((LocalizedText)null);
				return;
			case OPCUADIProfilePackage.BLOCK_TYPE__PERMITTED_MODE:
				setPermittedMode((LocalizedText)null);
				return;
			case OPCUADIProfilePackage.BLOCK_TYPE__NORMAL_MODE:
				setNormalMode((LocalizedText)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPCUADIProfilePackage.BLOCK_TYPE__TARGET_MODE:
				return targetMode != null;
			case OPCUADIProfilePackage.BLOCK_TYPE__REVISION_COUNTER:
				return revisionCounter != null;
			case OPCUADIProfilePackage.BLOCK_TYPE__ACTUAL_MODE:
				return actualMode != null;
			case OPCUADIProfilePackage.BLOCK_TYPE__PERMITTED_MODE:
				return permittedMode != null;
			case OPCUADIProfilePackage.BLOCK_TYPE__NORMAL_MODE:
				return normalMode != null;
		}
		return super.eIsSet(featureID);
	}

} //BlockTypeImpl
