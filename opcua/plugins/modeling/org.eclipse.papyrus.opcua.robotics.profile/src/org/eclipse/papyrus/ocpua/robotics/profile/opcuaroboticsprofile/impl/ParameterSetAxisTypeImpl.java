/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisStateEnumeration;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetAxisType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameter Set Axis Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetAxisTypeImpl#getActualAcceleration <em>Actual Acceleration</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetAxisTypeImpl#getActualPosition <em>Actual Position</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetAxisTypeImpl#getActualSpeed <em>Actual Speed</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetAxisTypeImpl#getNotCS_AxisState <em>Not CS Axis State</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ParameterSetAxisTypeImpl extends MinimalEObjectImpl.Container implements ParameterSetAxisType {
	/**
	 * The default value of the '{@link #getActualAcceleration() <em>Actual Acceleration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActualAcceleration()
	 * @generated
	 * @ordered
	 */
	protected static final double ACTUAL_ACCELERATION_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getActualAcceleration() <em>Actual Acceleration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActualAcceleration()
	 * @generated
	 * @ordered
	 */
	protected double actualAcceleration = ACTUAL_ACCELERATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getActualPosition() <em>Actual Position</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActualPosition()
	 * @generated
	 * @ordered
	 */
	protected static final double ACTUAL_POSITION_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getActualPosition() <em>Actual Position</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActualPosition()
	 * @generated
	 * @ordered
	 */
	protected double actualPosition = ACTUAL_POSITION_EDEFAULT;

	/**
	 * The default value of the '{@link #getActualSpeed() <em>Actual Speed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActualSpeed()
	 * @generated
	 * @ordered
	 */
	protected static final double ACTUAL_SPEED_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getActualSpeed() <em>Actual Speed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActualSpeed()
	 * @generated
	 * @ordered
	 */
	protected double actualSpeed = ACTUAL_SPEED_EDEFAULT;

	/**
	 * The default value of the '{@link #getNotCS_AxisState() <em>Not CS Axis State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotCS_AxisState()
	 * @generated
	 * @ordered
	 */
	protected static final AxisStateEnumeration NOT_CS_AXIS_STATE_EDEFAULT = AxisStateEnumeration.JOINT_SHUTTING_DOWN_MODE;

	/**
	 * The cached value of the '{@link #getNotCS_AxisState() <em>Not CS Axis State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotCS_AxisState()
	 * @generated
	 * @ordered
	 */
	protected AxisStateEnumeration notCS_AxisState = NOT_CS_AXIS_STATE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParameterSetAxisTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUARoboticsProfilePackage.Literals.PARAMETER_SET_AXIS_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getActualAcceleration() {
		return actualAcceleration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActualAcceleration(double newActualAcceleration) {
		double oldActualAcceleration = actualAcceleration;
		actualAcceleration = newActualAcceleration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_AXIS_TYPE__ACTUAL_ACCELERATION, oldActualAcceleration, actualAcceleration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getActualPosition() {
		return actualPosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActualPosition(double newActualPosition) {
		double oldActualPosition = actualPosition;
		actualPosition = newActualPosition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_AXIS_TYPE__ACTUAL_POSITION, oldActualPosition, actualPosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getActualSpeed() {
		return actualSpeed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActualSpeed(double newActualSpeed) {
		double oldActualSpeed = actualSpeed;
		actualSpeed = newActualSpeed;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_AXIS_TYPE__ACTUAL_SPEED, oldActualSpeed, actualSpeed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AxisStateEnumeration getNotCS_AxisState() {
		return notCS_AxisState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotCS_AxisState(AxisStateEnumeration newNotCS_AxisState) {
		AxisStateEnumeration oldNotCS_AxisState = notCS_AxisState;
		notCS_AxisState = newNotCS_AxisState == null ? NOT_CS_AXIS_STATE_EDEFAULT : newNotCS_AxisState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_AXIS_TYPE__NOT_CS_AXIS_STATE, oldNotCS_AxisState, notCS_AxisState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.PARAMETER_SET_AXIS_TYPE__ACTUAL_ACCELERATION:
				return getActualAcceleration();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_AXIS_TYPE__ACTUAL_POSITION:
				return getActualPosition();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_AXIS_TYPE__ACTUAL_SPEED:
				return getActualSpeed();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_AXIS_TYPE__NOT_CS_AXIS_STATE:
				return getNotCS_AxisState();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.PARAMETER_SET_AXIS_TYPE__ACTUAL_ACCELERATION:
				setActualAcceleration((Double)newValue);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_AXIS_TYPE__ACTUAL_POSITION:
				setActualPosition((Double)newValue);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_AXIS_TYPE__ACTUAL_SPEED:
				setActualSpeed((Double)newValue);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_AXIS_TYPE__NOT_CS_AXIS_STATE:
				setNotCS_AxisState((AxisStateEnumeration)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.PARAMETER_SET_AXIS_TYPE__ACTUAL_ACCELERATION:
				setActualAcceleration(ACTUAL_ACCELERATION_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_AXIS_TYPE__ACTUAL_POSITION:
				setActualPosition(ACTUAL_POSITION_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_AXIS_TYPE__ACTUAL_SPEED:
				setActualSpeed(ACTUAL_SPEED_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_AXIS_TYPE__NOT_CS_AXIS_STATE:
				setNotCS_AxisState(NOT_CS_AXIS_STATE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.PARAMETER_SET_AXIS_TYPE__ACTUAL_ACCELERATION:
				return actualAcceleration != ACTUAL_ACCELERATION_EDEFAULT;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_AXIS_TYPE__ACTUAL_POSITION:
				return actualPosition != ACTUAL_POSITION_EDEFAULT;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_AXIS_TYPE__ACTUAL_SPEED:
				return actualSpeed != ACTUAL_SPEED_EDEFAULT;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_AXIS_TYPE__NOT_CS_AXIS_STATE:
				return notCS_AxisState != NOT_CS_AXIS_STATE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (ActualAcceleration: ");
		result.append(actualAcceleration);
		result.append(", ActualPosition: ");
		result.append(actualPosition);
		result.append(", ActualSpeed: ");
		result.append(actualSpeed);
		result.append(", notCS_AxisState: ");
		result.append(notCS_AxisState);
		result.append(')');
		return result.toString();
	}

} //ParameterSetAxisTypeImpl
