/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile;

import org.eclipse.emf.common.util.EList;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.ComponentType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Safety State Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.SafetyStateType#getEmergencyStopFunctions <em>Emergency Stop Functions</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.SafetyStateType#getProtectiveStopFunctions <em>Protective Stop Functions</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.SafetyStateType#getParameterSetSafetyStateType <em>Parameter Set Safety State Type</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getSafetyStateType()
 * @model
 * @generated
 */
public interface SafetyStateType extends ComponentType {
	/**
	 * Returns the value of the '<em><b>Emergency Stop Functions</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.EmergencyStopFunctionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Emergency Stop Functions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Emergency Stop Functions</em>' reference list.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getSafetyStateType_EmergencyStopFunctions()
	 * @model ordered="false"
	 * @generated
	 */
	EList<EmergencyStopFunctionType> getEmergencyStopFunctions();

	/**
	 * Returns the value of the '<em><b>Protective Stop Functions</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ProtectiveStopFunctionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protective Stop Functions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protective Stop Functions</em>' reference list.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getSafetyStateType_ProtectiveStopFunctions()
	 * @model ordered="false"
	 * @generated
	 */
	EList<ProtectiveStopFunctionType> getProtectiveStopFunctions();

	/**
	 * Returns the value of the '<em><b>Parameter Set Safety State Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Set Safety State Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Set Safety State Type</em>' containment reference.
	 * @see #setParameterSetSafetyStateType(ParameterSetSafetyStateType)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getSafetyStateType_ParameterSetSafetyStateType()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	ParameterSetSafetyStateType getParameterSetSafetyStateType();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.SafetyStateType#getParameterSetSafetyStateType <em>Parameter Set Safety State Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Set Safety State Type</em>' containment reference.
	 * @see #getParameterSetSafetyStateType()
	 * @generated
	 */
	void setParameterSetSafetyStateType(ParameterSetSafetyStateType value);

} // SafetyStateType
