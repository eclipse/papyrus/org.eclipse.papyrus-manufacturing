/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.EmergencyStopFunctionType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetSafetyStateType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ProtectiveStopFunctionType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.SafetyStateType;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ComponentTypeImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Safety State Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.SafetyStateTypeImpl#getEmergencyStopFunctions <em>Emergency Stop Functions</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.SafetyStateTypeImpl#getProtectiveStopFunctions <em>Protective Stop Functions</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.SafetyStateTypeImpl#getParameterSetSafetyStateType <em>Parameter Set Safety State Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SafetyStateTypeImpl extends ComponentTypeImpl implements SafetyStateType {
	/**
	 * The cached value of the '{@link #getEmergencyStopFunctions() <em>Emergency Stop Functions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEmergencyStopFunctions()
	 * @generated
	 * @ordered
	 */
	protected EList<EmergencyStopFunctionType> emergencyStopFunctions;

	/**
	 * The cached value of the '{@link #getProtectiveStopFunctions() <em>Protective Stop Functions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProtectiveStopFunctions()
	 * @generated
	 * @ordered
	 */
	protected EList<ProtectiveStopFunctionType> protectiveStopFunctions;

	/**
	 * The cached value of the '{@link #getParameterSetSafetyStateType() <em>Parameter Set Safety State Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterSetSafetyStateType()
	 * @generated
	 * @ordered
	 */
	protected ParameterSetSafetyStateType parameterSetSafetyStateType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SafetyStateTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUARoboticsProfilePackage.Literals.SAFETY_STATE_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EmergencyStopFunctionType> getEmergencyStopFunctions() {
		if (emergencyStopFunctions == null) {
			emergencyStopFunctions = new EObjectResolvingEList<EmergencyStopFunctionType>(EmergencyStopFunctionType.class, this, OPCUARoboticsProfilePackage.SAFETY_STATE_TYPE__EMERGENCY_STOP_FUNCTIONS);
		}
		return emergencyStopFunctions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProtectiveStopFunctionType> getProtectiveStopFunctions() {
		if (protectiveStopFunctions == null) {
			protectiveStopFunctions = new EObjectResolvingEList<ProtectiveStopFunctionType>(ProtectiveStopFunctionType.class, this, OPCUARoboticsProfilePackage.SAFETY_STATE_TYPE__PROTECTIVE_STOP_FUNCTIONS);
		}
		return protectiveStopFunctions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterSetSafetyStateType getParameterSetSafetyStateType() {
		return parameterSetSafetyStateType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParameterSetSafetyStateType(ParameterSetSafetyStateType newParameterSetSafetyStateType, NotificationChain msgs) {
		ParameterSetSafetyStateType oldParameterSetSafetyStateType = parameterSetSafetyStateType;
		parameterSetSafetyStateType = newParameterSetSafetyStateType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.SAFETY_STATE_TYPE__PARAMETER_SET_SAFETY_STATE_TYPE, oldParameterSetSafetyStateType, newParameterSetSafetyStateType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterSetSafetyStateType(ParameterSetSafetyStateType newParameterSetSafetyStateType) {
		if (newParameterSetSafetyStateType != parameterSetSafetyStateType) {
			NotificationChain msgs = null;
			if (parameterSetSafetyStateType != null)
				msgs = ((InternalEObject)parameterSetSafetyStateType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.SAFETY_STATE_TYPE__PARAMETER_SET_SAFETY_STATE_TYPE, null, msgs);
			if (newParameterSetSafetyStateType != null)
				msgs = ((InternalEObject)newParameterSetSafetyStateType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.SAFETY_STATE_TYPE__PARAMETER_SET_SAFETY_STATE_TYPE, null, msgs);
			msgs = basicSetParameterSetSafetyStateType(newParameterSetSafetyStateType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.SAFETY_STATE_TYPE__PARAMETER_SET_SAFETY_STATE_TYPE, newParameterSetSafetyStateType, newParameterSetSafetyStateType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.SAFETY_STATE_TYPE__PARAMETER_SET_SAFETY_STATE_TYPE:
				return basicSetParameterSetSafetyStateType(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.SAFETY_STATE_TYPE__EMERGENCY_STOP_FUNCTIONS:
				return getEmergencyStopFunctions();
			case OPCUARoboticsProfilePackage.SAFETY_STATE_TYPE__PROTECTIVE_STOP_FUNCTIONS:
				return getProtectiveStopFunctions();
			case OPCUARoboticsProfilePackage.SAFETY_STATE_TYPE__PARAMETER_SET_SAFETY_STATE_TYPE:
				return getParameterSetSafetyStateType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.SAFETY_STATE_TYPE__EMERGENCY_STOP_FUNCTIONS:
				getEmergencyStopFunctions().clear();
				getEmergencyStopFunctions().addAll((Collection<? extends EmergencyStopFunctionType>)newValue);
				return;
			case OPCUARoboticsProfilePackage.SAFETY_STATE_TYPE__PROTECTIVE_STOP_FUNCTIONS:
				getProtectiveStopFunctions().clear();
				getProtectiveStopFunctions().addAll((Collection<? extends ProtectiveStopFunctionType>)newValue);
				return;
			case OPCUARoboticsProfilePackage.SAFETY_STATE_TYPE__PARAMETER_SET_SAFETY_STATE_TYPE:
				setParameterSetSafetyStateType((ParameterSetSafetyStateType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.SAFETY_STATE_TYPE__EMERGENCY_STOP_FUNCTIONS:
				getEmergencyStopFunctions().clear();
				return;
			case OPCUARoboticsProfilePackage.SAFETY_STATE_TYPE__PROTECTIVE_STOP_FUNCTIONS:
				getProtectiveStopFunctions().clear();
				return;
			case OPCUARoboticsProfilePackage.SAFETY_STATE_TYPE__PARAMETER_SET_SAFETY_STATE_TYPE:
				setParameterSetSafetyStateType((ParameterSetSafetyStateType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.SAFETY_STATE_TYPE__EMERGENCY_STOP_FUNCTIONS:
				return emergencyStopFunctions != null && !emergencyStopFunctions.isEmpty();
			case OPCUARoboticsProfilePackage.SAFETY_STATE_TYPE__PROTECTIVE_STOP_FUNCTIONS:
				return protectiveStopFunctions != null && !protectiveStopFunctions.isEmpty();
			case OPCUARoboticsProfilePackage.SAFETY_STATE_TYPE__PARAMETER_SET_SAFETY_STATE_TYPE:
				return parameterSetSafetyStateType != null;
		}
		return super.eIsSet(featureID);
	}

} //SafetyStateTypeImpl
