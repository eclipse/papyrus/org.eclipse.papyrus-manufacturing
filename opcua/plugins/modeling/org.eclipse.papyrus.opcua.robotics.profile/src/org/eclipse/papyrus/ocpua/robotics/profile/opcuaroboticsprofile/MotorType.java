/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.ComponentType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Motor Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotorType#getParameterSetMotorType <em>Parameter Set Motor Type</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getMotorType()
 * @model
 * @generated
 */
public interface MotorType extends ComponentType {
	/**
	 * Returns the value of the '<em><b>Parameter Set Motor Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Set Motor Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Set Motor Type</em>' containment reference.
	 * @see #setParameterSetMotorType(ParameterSetMotorType)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getMotorType_ParameterSetMotorType()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	ParameterSetMotorType getParameterSetMotorType();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotorType#getParameterSetMotorType <em>Parameter Set Motor Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Set Motor Type</em>' containment reference.
	 * @see #getParameterSetMotorType()
	 * @generated
	 */
	void setParameterSetMotorType(ParameterSetMotorType value);

} // MotorType
