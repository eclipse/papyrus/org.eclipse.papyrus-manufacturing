/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ControllerType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.TaskControlType;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.ComponentType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.SoftwareType;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ComponentTypeImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Controller Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ControllerTypeImpl#getParameterSetControllerType <em>Parameter Set Controller Type</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ControllerTypeImpl#getComponents <em>Components</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ControllerTypeImpl#getSoftware <em>Software</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ControllerTypeImpl#getTaskControls <em>Task Controls</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ControllerTypeImpl extends ComponentTypeImpl implements ControllerType {
	/**
	 * The cached value of the '{@link #getParameterSetControllerType() <em>Parameter Set Controller Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterSetControllerType()
	 * @generated
	 * @ordered
	 */
	protected ParameterSetControllerType parameterSetControllerType;

	/**
	 * The cached value of the '{@link #getComponents() <em>Components</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponents()
	 * @generated
	 * @ordered
	 */
	protected EList<ComponentType> components;

	/**
	 * The cached value of the '{@link #getSoftware() <em>Software</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSoftware()
	 * @generated
	 * @ordered
	 */
	protected SoftwareType software;

	/**
	 * The cached value of the '{@link #getTaskControls() <em>Task Controls</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskControls()
	 * @generated
	 * @ordered
	 */
	protected EList<TaskControlType> taskControls;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ControllerTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUARoboticsProfilePackage.Literals.CONTROLLER_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterSetControllerType getParameterSetControllerType() {
		return parameterSetControllerType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParameterSetControllerType(ParameterSetControllerType newParameterSetControllerType, NotificationChain msgs) {
		ParameterSetControllerType oldParameterSetControllerType = parameterSetControllerType;
		parameterSetControllerType = newParameterSetControllerType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.CONTROLLER_TYPE__PARAMETER_SET_CONTROLLER_TYPE, oldParameterSetControllerType, newParameterSetControllerType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterSetControllerType(ParameterSetControllerType newParameterSetControllerType) {
		if (newParameterSetControllerType != parameterSetControllerType) {
			NotificationChain msgs = null;
			if (parameterSetControllerType != null)
				msgs = ((InternalEObject)parameterSetControllerType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.CONTROLLER_TYPE__PARAMETER_SET_CONTROLLER_TYPE, null, msgs);
			if (newParameterSetControllerType != null)
				msgs = ((InternalEObject)newParameterSetControllerType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.CONTROLLER_TYPE__PARAMETER_SET_CONTROLLER_TYPE, null, msgs);
			msgs = basicSetParameterSetControllerType(newParameterSetControllerType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.CONTROLLER_TYPE__PARAMETER_SET_CONTROLLER_TYPE, newParameterSetControllerType, newParameterSetControllerType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComponentType> getComponents() {
		if (components == null) {
			components = new EObjectResolvingEList<ComponentType>(ComponentType.class, this, OPCUARoboticsProfilePackage.CONTROLLER_TYPE__COMPONENTS);
		}
		return components;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SoftwareType getSoftware() {
		if (software != null && software.eIsProxy()) {
			InternalEObject oldSoftware = (InternalEObject)software;
			software = (SoftwareType)eResolveProxy(oldSoftware);
			if (software != oldSoftware) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OPCUARoboticsProfilePackage.CONTROLLER_TYPE__SOFTWARE, oldSoftware, software));
			}
		}
		return software;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SoftwareType basicGetSoftware() {
		return software;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSoftware(SoftwareType newSoftware) {
		SoftwareType oldSoftware = software;
		software = newSoftware;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.CONTROLLER_TYPE__SOFTWARE, oldSoftware, software));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskControlType> getTaskControls() {
		if (taskControls == null) {
			taskControls = new EObjectResolvingEList<TaskControlType>(TaskControlType.class, this, OPCUARoboticsProfilePackage.CONTROLLER_TYPE__TASK_CONTROLS);
		}
		return taskControls;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.CONTROLLER_TYPE__PARAMETER_SET_CONTROLLER_TYPE:
				return basicSetParameterSetControllerType(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.CONTROLLER_TYPE__PARAMETER_SET_CONTROLLER_TYPE:
				return getParameterSetControllerType();
			case OPCUARoboticsProfilePackage.CONTROLLER_TYPE__COMPONENTS:
				return getComponents();
			case OPCUARoboticsProfilePackage.CONTROLLER_TYPE__SOFTWARE:
				if (resolve) return getSoftware();
				return basicGetSoftware();
			case OPCUARoboticsProfilePackage.CONTROLLER_TYPE__TASK_CONTROLS:
				return getTaskControls();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.CONTROLLER_TYPE__PARAMETER_SET_CONTROLLER_TYPE:
				setParameterSetControllerType((ParameterSetControllerType)newValue);
				return;
			case OPCUARoboticsProfilePackage.CONTROLLER_TYPE__COMPONENTS:
				getComponents().clear();
				getComponents().addAll((Collection<? extends ComponentType>)newValue);
				return;
			case OPCUARoboticsProfilePackage.CONTROLLER_TYPE__SOFTWARE:
				setSoftware((SoftwareType)newValue);
				return;
			case OPCUARoboticsProfilePackage.CONTROLLER_TYPE__TASK_CONTROLS:
				getTaskControls().clear();
				getTaskControls().addAll((Collection<? extends TaskControlType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.CONTROLLER_TYPE__PARAMETER_SET_CONTROLLER_TYPE:
				setParameterSetControllerType((ParameterSetControllerType)null);
				return;
			case OPCUARoboticsProfilePackage.CONTROLLER_TYPE__COMPONENTS:
				getComponents().clear();
				return;
			case OPCUARoboticsProfilePackage.CONTROLLER_TYPE__SOFTWARE:
				setSoftware((SoftwareType)null);
				return;
			case OPCUARoboticsProfilePackage.CONTROLLER_TYPE__TASK_CONTROLS:
				getTaskControls().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.CONTROLLER_TYPE__PARAMETER_SET_CONTROLLER_TYPE:
				return parameterSetControllerType != null;
			case OPCUARoboticsProfilePackage.CONTROLLER_TYPE__COMPONENTS:
				return components != null && !components.isEmpty();
			case OPCUARoboticsProfilePackage.CONTROLLER_TYPE__SOFTWARE:
				return software != null;
			case OPCUARoboticsProfilePackage.CONTROLLER_TYPE__TASK_CONTROLS:
				return taskControls != null && !taskControls.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ControllerTypeImpl
