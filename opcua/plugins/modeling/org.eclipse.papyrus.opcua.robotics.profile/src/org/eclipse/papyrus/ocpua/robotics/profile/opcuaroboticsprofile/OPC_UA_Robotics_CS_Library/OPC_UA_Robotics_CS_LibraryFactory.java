/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.OPC_UA_Robotics_CS_LibraryPackage
 * @generated
 */
public interface OPC_UA_Robotics_CS_LibraryFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OPC_UA_Robotics_CS_LibraryFactory eINSTANCE = org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>3D Frame Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>3D Frame Type</em>'.
	 * @generated
	 */
	_3DFrameType create_3DFrameType();

	/**
	 * Returns a new object of class '<em>3D Cartesian Coordinates</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>3D Cartesian Coordinates</em>'.
	 * @generated
	 */
	_3DCartesianCoordinates create_3DCartesianCoordinates();

	/**
	 * Returns a new object of class '<em>3D Orientation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>3D Orientation</em>'.
	 * @generated
	 */
	_3DOrientation create_3DOrientation();

	/**
	 * Returns a new object of class '<em>Analog Unit Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Analog Unit Type</em>'.
	 * @generated
	 */
	AnalogUnitType createAnalogUnitType();

	/**
	 * Returns a new object of class '<em>Base Data Variable Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Data Variable Type</em>'.
	 * @generated
	 */
	BaseDataVariableType createBaseDataVariableType();

	/**
	 * Returns a new object of class '<em>Duration String</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Duration String</em>'.
	 * @generated
	 */
	DurationString createDurationString();

	/**
	 * Returns a new object of class '<em>Date Time</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Date Time</em>'.
	 * @generated
	 */
	DateTime createDateTime();

	/**
	 * Returns a new object of class '<em>3D Vector</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>3D Vector</em>'.
	 * @generated
	 */
	_3DVector create_3DVector();

	/**
	 * Returns a new object of class '<em>3D Vector Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>3D Vector Type</em>'.
	 * @generated
	 */
	_3DVectorType create_3DVectorType();

	/**
	 * Returns a new object of class '<em>Localized Text</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Localized Text</em>'.
	 * @generated
	 */
	LocalizedText createLocalizedText();

	/**
	 * Returns a new object of class '<em>Rational Number</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rational Number</em>'.
	 * @generated
	 */
	RationalNumber createRationalNumber();

	/**
	 * Returns a new object of class '<em>Double</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Double</em>'.
	 * @generated
	 */
	Double createDouble();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	OPC_UA_Robotics_CS_LibraryPackage getOPC_UA_Robotics_CS_LibraryPackage();

} //OPC_UA_Robotics_CS_LibraryFactory
