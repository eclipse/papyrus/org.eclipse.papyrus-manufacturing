/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter Set Task Control Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * � Variable TaskProgramName: The TaskProgramName variable provides a customer given identifier for the task program.
 * � Variable TaskProgramLoaded: The TaskProgramLoaded variable is TRUE if a task program is loaded in the task control, FALSE otherwise.
 * � Variable ExecutionMode: The ExecutionMode variable tells how the task control executes the task program.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetTaskControlType#getExecutionMode <em>Execution Mode</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetTaskControlType#isTaskProgramLoaded <em>Task Program Loaded</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetTaskControlType#getTaskProgramName <em>Task Program Name</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetTaskControlType()
 * @model
 * @generated
 */
public interface ParameterSetTaskControlType extends EObject {
	/**
	 * Returns the value of the '<em><b>Execution Mode</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ExecutionModeEnumeration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execution Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Mode</em>' attribute.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ExecutionModeEnumeration
	 * @see #setExecutionMode(ExecutionModeEnumeration)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetTaskControlType_ExecutionMode()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	ExecutionModeEnumeration getExecutionMode();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetTaskControlType#getExecutionMode <em>Execution Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Execution Mode</em>' attribute.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ExecutionModeEnumeration
	 * @see #getExecutionMode()
	 * @generated
	 */
	void setExecutionMode(ExecutionModeEnumeration value);

	/**
	 * Returns the value of the '<em><b>Task Program Loaded</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Program Loaded</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Program Loaded</em>' attribute.
	 * @see #setTaskProgramLoaded(boolean)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetTaskControlType_TaskProgramLoaded()
	 * @model dataType="org.eclipse.uml2.types.Boolean" required="true" ordered="false"
	 * @generated
	 */
	boolean isTaskProgramLoaded();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetTaskControlType#isTaskProgramLoaded <em>Task Program Loaded</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Program Loaded</em>' attribute.
	 * @see #isTaskProgramLoaded()
	 * @generated
	 */
	void setTaskProgramLoaded(boolean value);

	/**
	 * Returns the value of the '<em><b>Task Program Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Program Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Program Name</em>' attribute.
	 * @see #setTaskProgramName(String)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetTaskControlType_TaskProgramName()
	 * @model dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
	 * @generated
	 */
	String getTaskProgramName();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetTaskControlType#getTaskProgramName <em>Task Program Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Program Name</em>' attribute.
	 * @see #getTaskProgramName()
	 * @generated
	 */
	void setTaskProgramName(String value);

} // ParameterSetTaskControlType
