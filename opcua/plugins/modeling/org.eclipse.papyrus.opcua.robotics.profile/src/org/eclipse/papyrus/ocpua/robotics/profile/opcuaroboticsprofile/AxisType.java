/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.ComponentType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Axis Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisType#getMotionProfile <em>Motion Profile</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisType#getAdditionalLoad <em>Additional Load</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisType#getParameterSetAxisType <em>Parameter Set Axis Type</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getAxisType()
 * @model
 * @generated
 */
public interface AxisType extends ComponentType {
	/**
	 * Returns the value of the '<em><b>Motion Profile</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisMotionProfileEnumeration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Motion Profile</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Motion Profile</em>' attribute.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisMotionProfileEnumeration
	 * @see #setMotionProfile(AxisMotionProfileEnumeration)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getAxisType_MotionProfile()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	AxisMotionProfileEnumeration getMotionProfile();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisType#getMotionProfile <em>Motion Profile</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Motion Profile</em>' attribute.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisMotionProfileEnumeration
	 * @see #getMotionProfile()
	 * @generated
	 */
	void setMotionProfile(AxisMotionProfileEnumeration value);

	/**
	 * Returns the value of the '<em><b>Additional Load</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Additional Load</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Additional Load</em>' containment reference.
	 * @see #setAdditionalLoad(LoadType)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getAxisType_AdditionalLoad()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	LoadType getAdditionalLoad();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisType#getAdditionalLoad <em>Additional Load</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Additional Load</em>' containment reference.
	 * @see #getAdditionalLoad()
	 * @generated
	 */
	void setAdditionalLoad(LoadType value);

	/**
	 * Returns the value of the '<em><b>Parameter Set Axis Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Set Axis Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Set Axis Type</em>' containment reference.
	 * @see #setParameterSetAxisType(ParameterSetAxisType)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getAxisType_ParameterSetAxisType()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	ParameterSetAxisType getParameterSetAxisType();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisType#getParameterSetAxisType <em>Parameter Set Axis Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Set Axis Type</em>' containment reference.
	 * @see #getParameterSetAxisType()
	 * @generated
	 */
	void setParameterSetAxisType(ParameterSetAxisType value);

} // AxisType
