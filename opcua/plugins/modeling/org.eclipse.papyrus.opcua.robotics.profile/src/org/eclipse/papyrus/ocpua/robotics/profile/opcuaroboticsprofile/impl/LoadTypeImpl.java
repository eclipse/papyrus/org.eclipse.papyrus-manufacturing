/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.LoadType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library._3DFrameType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library._3DVectorType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Load Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.LoadTypeImpl#getMass <em>Mass</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.LoadTypeImpl#getCenterOfMass <em>Center Of Mass</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.LoadTypeImpl#getInertia <em>Inertia</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LoadTypeImpl extends MinimalEObjectImpl.Container implements LoadType {
	/**
	 * The cached value of the '{@link #getMass() <em>Mass</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMass()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.Double mass;

	/**
	 * The cached value of the '{@link #getCenterOfMass() <em>Center Of Mass</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCenterOfMass()
	 * @generated
	 * @ordered
	 */
	protected _3DFrameType centerOfMass;

	/**
	 * The cached value of the '{@link #getInertia() <em>Inertia</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInertia()
	 * @generated
	 * @ordered
	 */
	protected _3DVectorType inertia;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LoadTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUARoboticsProfilePackage.Literals.LOAD_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.Double getMass() {
		return mass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMass(org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.Double newMass, NotificationChain msgs) {
		org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.Double oldMass = mass;
		mass = newMass;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.LOAD_TYPE__MASS, oldMass, newMass);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMass(org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.Double newMass) {
		if (newMass != mass) {
			NotificationChain msgs = null;
			if (mass != null)
				msgs = ((InternalEObject)mass).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.LOAD_TYPE__MASS, null, msgs);
			if (newMass != null)
				msgs = ((InternalEObject)newMass).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.LOAD_TYPE__MASS, null, msgs);
			msgs = basicSetMass(newMass, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.LOAD_TYPE__MASS, newMass, newMass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public _3DFrameType getCenterOfMass() {
		return centerOfMass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCenterOfMass(_3DFrameType newCenterOfMass, NotificationChain msgs) {
		_3DFrameType oldCenterOfMass = centerOfMass;
		centerOfMass = newCenterOfMass;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.LOAD_TYPE__CENTER_OF_MASS, oldCenterOfMass, newCenterOfMass);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCenterOfMass(_3DFrameType newCenterOfMass) {
		if (newCenterOfMass != centerOfMass) {
			NotificationChain msgs = null;
			if (centerOfMass != null)
				msgs = ((InternalEObject)centerOfMass).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.LOAD_TYPE__CENTER_OF_MASS, null, msgs);
			if (newCenterOfMass != null)
				msgs = ((InternalEObject)newCenterOfMass).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.LOAD_TYPE__CENTER_OF_MASS, null, msgs);
			msgs = basicSetCenterOfMass(newCenterOfMass, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.LOAD_TYPE__CENTER_OF_MASS, newCenterOfMass, newCenterOfMass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public _3DVectorType getInertia() {
		return inertia;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInertia(_3DVectorType newInertia, NotificationChain msgs) {
		_3DVectorType oldInertia = inertia;
		inertia = newInertia;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.LOAD_TYPE__INERTIA, oldInertia, newInertia);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInertia(_3DVectorType newInertia) {
		if (newInertia != inertia) {
			NotificationChain msgs = null;
			if (inertia != null)
				msgs = ((InternalEObject)inertia).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.LOAD_TYPE__INERTIA, null, msgs);
			if (newInertia != null)
				msgs = ((InternalEObject)newInertia).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.LOAD_TYPE__INERTIA, null, msgs);
			msgs = basicSetInertia(newInertia, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.LOAD_TYPE__INERTIA, newInertia, newInertia));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.LOAD_TYPE__MASS:
				return basicSetMass(null, msgs);
			case OPCUARoboticsProfilePackage.LOAD_TYPE__CENTER_OF_MASS:
				return basicSetCenterOfMass(null, msgs);
			case OPCUARoboticsProfilePackage.LOAD_TYPE__INERTIA:
				return basicSetInertia(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.LOAD_TYPE__MASS:
				return getMass();
			case OPCUARoboticsProfilePackage.LOAD_TYPE__CENTER_OF_MASS:
				return getCenterOfMass();
			case OPCUARoboticsProfilePackage.LOAD_TYPE__INERTIA:
				return getInertia();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.LOAD_TYPE__MASS:
				setMass((org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.Double)newValue);
				return;
			case OPCUARoboticsProfilePackage.LOAD_TYPE__CENTER_OF_MASS:
				setCenterOfMass((_3DFrameType)newValue);
				return;
			case OPCUARoboticsProfilePackage.LOAD_TYPE__INERTIA:
				setInertia((_3DVectorType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.LOAD_TYPE__MASS:
				setMass((org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.Double)null);
				return;
			case OPCUARoboticsProfilePackage.LOAD_TYPE__CENTER_OF_MASS:
				setCenterOfMass((_3DFrameType)null);
				return;
			case OPCUARoboticsProfilePackage.LOAD_TYPE__INERTIA:
				setInertia((_3DVectorType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.LOAD_TYPE__MASS:
				return mass != null;
			case OPCUARoboticsProfilePackage.LOAD_TYPE__CENTER_OF_MASS:
				return centerOfMass != null;
			case OPCUARoboticsProfilePackage.LOAD_TYPE__INERTIA:
				return inertia != null;
		}
		return super.eIsSet(featureID);
	}

} //LoadTypeImpl
