/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.LoadType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceCategoryEnum;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.PowerTrainType;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.ComponentType;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ComponentTypeImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Motion Device Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MotionDeviceTypeImpl#getMotionDeviceCategory <em>Motion Device Category</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MotionDeviceTypeImpl#getParameterSetMotionDeviceType <em>Parameter Set Motion Device Type</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MotionDeviceTypeImpl#getAxes <em>Axes</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MotionDeviceTypeImpl#getPowerTrains <em>Power Trains</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MotionDeviceTypeImpl#getAdditionalComponents <em>Additional Components</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MotionDeviceTypeImpl#getFlangeLoad <em>Flange Load</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MotionDeviceTypeImpl extends ComponentTypeImpl implements MotionDeviceType {
	/**
	 * The default value of the '{@link #getMotionDeviceCategory() <em>Motion Device Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMotionDeviceCategory()
	 * @generated
	 * @ordered
	 */
	protected static final MotionDeviceCategoryEnum MOTION_DEVICE_CATEGORY_EDEFAULT = MotionDeviceCategoryEnum.OTHER;

	/**
	 * The cached value of the '{@link #getMotionDeviceCategory() <em>Motion Device Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMotionDeviceCategory()
	 * @generated
	 * @ordered
	 */
	protected MotionDeviceCategoryEnum motionDeviceCategory = MOTION_DEVICE_CATEGORY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getParameterSetMotionDeviceType() <em>Parameter Set Motion Device Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterSetMotionDeviceType()
	 * @generated
	 * @ordered
	 */
	protected ParameterSetMotionDeviceType parameterSetMotionDeviceType;

	/**
	 * The cached value of the '{@link #getAxes() <em>Axes</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAxes()
	 * @generated
	 * @ordered
	 */
	protected EList<AxisType> axes;

	/**
	 * The cached value of the '{@link #getPowerTrains() <em>Power Trains</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPowerTrains()
	 * @generated
	 * @ordered
	 */
	protected EList<PowerTrainType> powerTrains;

	/**
	 * The cached value of the '{@link #getAdditionalComponents() <em>Additional Components</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAdditionalComponents()
	 * @generated
	 * @ordered
	 */
	protected EList<ComponentType> additionalComponents;

	/**
	 * The cached value of the '{@link #getFlangeLoad() <em>Flange Load</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFlangeLoad()
	 * @generated
	 * @ordered
	 */
	protected LoadType flangeLoad;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MotionDeviceTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUARoboticsProfilePackage.Literals.MOTION_DEVICE_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MotionDeviceCategoryEnum getMotionDeviceCategory() {
		return motionDeviceCategory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMotionDeviceCategory(MotionDeviceCategoryEnum newMotionDeviceCategory) {
		MotionDeviceCategoryEnum oldMotionDeviceCategory = motionDeviceCategory;
		motionDeviceCategory = newMotionDeviceCategory == null ? MOTION_DEVICE_CATEGORY_EDEFAULT : newMotionDeviceCategory;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__MOTION_DEVICE_CATEGORY, oldMotionDeviceCategory, motionDeviceCategory));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterSetMotionDeviceType getParameterSetMotionDeviceType() {
		return parameterSetMotionDeviceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParameterSetMotionDeviceType(ParameterSetMotionDeviceType newParameterSetMotionDeviceType, NotificationChain msgs) {
		ParameterSetMotionDeviceType oldParameterSetMotionDeviceType = parameterSetMotionDeviceType;
		parameterSetMotionDeviceType = newParameterSetMotionDeviceType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__PARAMETER_SET_MOTION_DEVICE_TYPE, oldParameterSetMotionDeviceType, newParameterSetMotionDeviceType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterSetMotionDeviceType(ParameterSetMotionDeviceType newParameterSetMotionDeviceType) {
		if (newParameterSetMotionDeviceType != parameterSetMotionDeviceType) {
			NotificationChain msgs = null;
			if (parameterSetMotionDeviceType != null)
				msgs = ((InternalEObject)parameterSetMotionDeviceType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__PARAMETER_SET_MOTION_DEVICE_TYPE, null, msgs);
			if (newParameterSetMotionDeviceType != null)
				msgs = ((InternalEObject)newParameterSetMotionDeviceType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__PARAMETER_SET_MOTION_DEVICE_TYPE, null, msgs);
			msgs = basicSetParameterSetMotionDeviceType(newParameterSetMotionDeviceType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__PARAMETER_SET_MOTION_DEVICE_TYPE, newParameterSetMotionDeviceType, newParameterSetMotionDeviceType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AxisType> getAxes() {
		if (axes == null) {
			axes = new EObjectResolvingEList<AxisType>(AxisType.class, this, OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__AXES);
		}
		return axes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PowerTrainType> getPowerTrains() {
		if (powerTrains == null) {
			powerTrains = new EObjectResolvingEList<PowerTrainType>(PowerTrainType.class, this, OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__POWER_TRAINS);
		}
		return powerTrains;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComponentType> getAdditionalComponents() {
		if (additionalComponents == null) {
			additionalComponents = new EObjectResolvingEList<ComponentType>(ComponentType.class, this, OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__ADDITIONAL_COMPONENTS);
		}
		return additionalComponents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LoadType getFlangeLoad() {
		return flangeLoad;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFlangeLoad(LoadType newFlangeLoad, NotificationChain msgs) {
		LoadType oldFlangeLoad = flangeLoad;
		flangeLoad = newFlangeLoad;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__FLANGE_LOAD, oldFlangeLoad, newFlangeLoad);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFlangeLoad(LoadType newFlangeLoad) {
		if (newFlangeLoad != flangeLoad) {
			NotificationChain msgs = null;
			if (flangeLoad != null)
				msgs = ((InternalEObject)flangeLoad).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__FLANGE_LOAD, null, msgs);
			if (newFlangeLoad != null)
				msgs = ((InternalEObject)newFlangeLoad).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__FLANGE_LOAD, null, msgs);
			msgs = basicSetFlangeLoad(newFlangeLoad, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__FLANGE_LOAD, newFlangeLoad, newFlangeLoad));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__PARAMETER_SET_MOTION_DEVICE_TYPE:
				return basicSetParameterSetMotionDeviceType(null, msgs);
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__FLANGE_LOAD:
				return basicSetFlangeLoad(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__MOTION_DEVICE_CATEGORY:
				return getMotionDeviceCategory();
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__PARAMETER_SET_MOTION_DEVICE_TYPE:
				return getParameterSetMotionDeviceType();
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__AXES:
				return getAxes();
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__POWER_TRAINS:
				return getPowerTrains();
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__ADDITIONAL_COMPONENTS:
				return getAdditionalComponents();
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__FLANGE_LOAD:
				return getFlangeLoad();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__MOTION_DEVICE_CATEGORY:
				setMotionDeviceCategory((MotionDeviceCategoryEnum)newValue);
				return;
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__PARAMETER_SET_MOTION_DEVICE_TYPE:
				setParameterSetMotionDeviceType((ParameterSetMotionDeviceType)newValue);
				return;
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__AXES:
				getAxes().clear();
				getAxes().addAll((Collection<? extends AxisType>)newValue);
				return;
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__POWER_TRAINS:
				getPowerTrains().clear();
				getPowerTrains().addAll((Collection<? extends PowerTrainType>)newValue);
				return;
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__ADDITIONAL_COMPONENTS:
				getAdditionalComponents().clear();
				getAdditionalComponents().addAll((Collection<? extends ComponentType>)newValue);
				return;
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__FLANGE_LOAD:
				setFlangeLoad((LoadType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__MOTION_DEVICE_CATEGORY:
				setMotionDeviceCategory(MOTION_DEVICE_CATEGORY_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__PARAMETER_SET_MOTION_DEVICE_TYPE:
				setParameterSetMotionDeviceType((ParameterSetMotionDeviceType)null);
				return;
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__AXES:
				getAxes().clear();
				return;
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__POWER_TRAINS:
				getPowerTrains().clear();
				return;
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__ADDITIONAL_COMPONENTS:
				getAdditionalComponents().clear();
				return;
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__FLANGE_LOAD:
				setFlangeLoad((LoadType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__MOTION_DEVICE_CATEGORY:
				return motionDeviceCategory != MOTION_DEVICE_CATEGORY_EDEFAULT;
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__PARAMETER_SET_MOTION_DEVICE_TYPE:
				return parameterSetMotionDeviceType != null;
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__AXES:
				return axes != null && !axes.isEmpty();
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__POWER_TRAINS:
				return powerTrains != null && !powerTrains.isEmpty();
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__ADDITIONAL_COMPONENTS:
				return additionalComponents != null && !additionalComponents.isEmpty();
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE__FLANGE_LOAD:
				return flangeLoad != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (MotionDeviceCategory: ");
		result.append(motionDeviceCategory);
		result.append(')');
		return result.toString();
	}

} //MotionDeviceTypeImpl
