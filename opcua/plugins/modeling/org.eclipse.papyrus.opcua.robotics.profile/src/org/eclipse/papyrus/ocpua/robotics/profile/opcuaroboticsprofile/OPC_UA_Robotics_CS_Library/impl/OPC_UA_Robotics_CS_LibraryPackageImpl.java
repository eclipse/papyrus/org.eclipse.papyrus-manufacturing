/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.AnalogUnitType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.BaseDataVariableType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.DateTime;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.DurationString;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.LocalizedText;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.OPC_UA_Robotics_CS_LibraryFactory;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.OPC_UA_Robotics_CS_LibraryPackage;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.RationalNumber;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library._3DCartesianCoordinates;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library._3DFrameType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library._3DOrientation;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library._3DVector;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library._3DVectorType;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.OPC_UA_DI_Library.OPC_UA_DI_LibraryPackage;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.OPC_UA_DI_Library.impl.OPC_UA_DI_LibraryPackageImpl;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl;

import org.eclipse.papyrus.opcua.opcuaprofile.OPCUAProfilePackage;

import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.OPC_UA_LibraryPackage;

import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl;

import org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl;

import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OPC_UA_Robotics_CS_LibraryPackageImpl extends EPackageImpl implements OPC_UA_Robotics_CS_LibraryPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass _3DFrameTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass _3DCartesianCoordinatesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass _3DOrientationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass analogUnitTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseDataVariableTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass durationStringEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dateTimeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass _3DVectorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass _3DVectorTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass localizedTextEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rationalNumberEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass doubleEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.OPC_UA_Robotics_CS_LibraryPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OPC_UA_Robotics_CS_LibraryPackageImpl() {
		super(eNS_URI, OPC_UA_Robotics_CS_LibraryFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link OPC_UA_Robotics_CS_LibraryPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static OPC_UA_Robotics_CS_LibraryPackage init() {
		if (isInited) return (OPC_UA_Robotics_CS_LibraryPackage)EPackage.Registry.INSTANCE.getEPackage(OPC_UA_Robotics_CS_LibraryPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredOPC_UA_Robotics_CS_LibraryPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		OPC_UA_Robotics_CS_LibraryPackageImpl theOPC_UA_Robotics_CS_LibraryPackage = registeredOPC_UA_Robotics_CS_LibraryPackage instanceof OPC_UA_Robotics_CS_LibraryPackageImpl ? (OPC_UA_Robotics_CS_LibraryPackageImpl)registeredOPC_UA_Robotics_CS_LibraryPackage : new OPC_UA_Robotics_CS_LibraryPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OPCUARoboticsProfilePackage.eNS_URI);
		OPCUARoboticsProfilePackageImpl theOPCUARoboticsProfilePackage = (OPCUARoboticsProfilePackageImpl)(registeredPackage instanceof OPCUARoboticsProfilePackageImpl ? registeredPackage : OPCUARoboticsProfilePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OPCUADIProfilePackage.eNS_URI);
		OPCUADIProfilePackageImpl theOPCUADIProfilePackage = (OPCUADIProfilePackageImpl)(registeredPackage instanceof OPCUADIProfilePackageImpl ? registeredPackage : OPCUADIProfilePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OPCUAProfilePackage.eNS_URI);
		OPCUAProfilePackageImpl theOPCUAProfilePackage = (OPCUAProfilePackageImpl)(registeredPackage instanceof OPCUAProfilePackageImpl ? registeredPackage : OPCUAProfilePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OPC_UA_LibraryPackage.eNS_URI);
		OPC_UA_LibraryPackageImpl theOPC_UA_LibraryPackage = (OPC_UA_LibraryPackageImpl)(registeredPackage instanceof OPC_UA_LibraryPackageImpl ? registeredPackage : OPC_UA_LibraryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OPC_UA_DI_LibraryPackage.eNS_URI);
		OPC_UA_DI_LibraryPackageImpl theOPC_UA_DI_LibraryPackage = (OPC_UA_DI_LibraryPackageImpl)(registeredPackage instanceof OPC_UA_DI_LibraryPackageImpl ? registeredPackage : OPC_UA_DI_LibraryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Library.OPC_UA_LibraryPackage.eNS_URI);
		org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl theOPC_UA_LibraryPackage_1 = (org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl)(registeredPackage instanceof org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl ? registeredPackage : org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Library.OPC_UA_LibraryPackage.eINSTANCE);

		// Create package meta-data objects
		theOPC_UA_Robotics_CS_LibraryPackage.createPackageContents();
		theOPCUARoboticsProfilePackage.createPackageContents();
		theOPCUADIProfilePackage.createPackageContents();
		theOPCUAProfilePackage.createPackageContents();
		theOPC_UA_LibraryPackage.createPackageContents();
		theOPC_UA_DI_LibraryPackage.createPackageContents();
		theOPC_UA_LibraryPackage_1.createPackageContents();

		// Initialize created meta-data
		theOPC_UA_Robotics_CS_LibraryPackage.initializePackageContents();
		theOPCUARoboticsProfilePackage.initializePackageContents();
		theOPCUADIProfilePackage.initializePackageContents();
		theOPCUAProfilePackage.initializePackageContents();
		theOPC_UA_LibraryPackage.initializePackageContents();
		theOPC_UA_DI_LibraryPackage.initializePackageContents();
		theOPC_UA_LibraryPackage_1.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theOPC_UA_Robotics_CS_LibraryPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(OPC_UA_Robotics_CS_LibraryPackage.eNS_URI, theOPC_UA_Robotics_CS_LibraryPackage);
		return theOPC_UA_Robotics_CS_LibraryPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass get_3DFrameType() {
		return _3DFrameTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference get_3DFrameType_CartesianCoordinates() {
		return (EReference)_3DFrameTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference get_3DFrameType_Orientation() {
		return (EReference)_3DFrameTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass get_3DCartesianCoordinates() {
		return _3DCartesianCoordinatesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass get_3DOrientation() {
		return _3DOrientationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnalogUnitType() {
		return analogUnitTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaseDataVariableType() {
		return baseDataVariableTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDurationString() {
		return durationStringEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDateTime() {
		return dateTimeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass get_3DVector() {
		return _3DVectorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass get_3DVectorType() {
		return _3DVectorTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute get_3DVectorType_X() {
		return (EAttribute)_3DVectorTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute get_3DVectorType_Y() {
		return (EAttribute)_3DVectorTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute get_3DVectorType_Z() {
		return (EAttribute)_3DVectorTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLocalizedText() {
		return localizedTextEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRationalNumber() {
		return rationalNumberEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDouble() {
		return doubleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPC_UA_Robotics_CS_LibraryFactory getOPC_UA_Robotics_CS_LibraryFactory() {
		return (OPC_UA_Robotics_CS_LibraryFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		_3DFrameTypeEClass = createEClass(_3D_FRAME_TYPE);
		createEReference(_3DFrameTypeEClass, _3D_FRAME_TYPE__CARTESIAN_COORDINATES);
		createEReference(_3DFrameTypeEClass, _3D_FRAME_TYPE__ORIENTATION);

		_3DCartesianCoordinatesEClass = createEClass(_3D_CARTESIAN_COORDINATES);

		_3DOrientationEClass = createEClass(_3D_ORIENTATION);

		analogUnitTypeEClass = createEClass(ANALOG_UNIT_TYPE);

		baseDataVariableTypeEClass = createEClass(BASE_DATA_VARIABLE_TYPE);

		durationStringEClass = createEClass(DURATION_STRING);

		dateTimeEClass = createEClass(DATE_TIME);

		_3DVectorEClass = createEClass(_3D_VECTOR);

		_3DVectorTypeEClass = createEClass(_3D_VECTOR_TYPE);
		createEAttribute(_3DVectorTypeEClass, _3D_VECTOR_TYPE__X);
		createEAttribute(_3DVectorTypeEClass, _3D_VECTOR_TYPE__Y);
		createEAttribute(_3DVectorTypeEClass, _3D_VECTOR_TYPE__Z);

		localizedTextEClass = createEClass(LOCALIZED_TEXT);

		rationalNumberEClass = createEClass(RATIONAL_NUMBER);

		doubleEClass = createEClass(DOUBLE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		analogUnitTypeEClass.getESuperTypes().add(this.getDouble());
		durationStringEClass.getESuperTypes().add(this.getBaseDataVariableType());
		dateTimeEClass.getESuperTypes().add(this.getBaseDataVariableType());

		// Initialize classes, features, and operations; add parameters
		initEClass(_3DFrameTypeEClass, _3DFrameType.class, "_3DFrameType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(get_3DFrameType_CartesianCoordinates(), this.get_3DCartesianCoordinates(), null, "CartesianCoordinates", null, 1, 1, _3DFrameType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(get_3DFrameType_Orientation(), this.get_3DOrientation(), null, "Orientation", null, 1, 1, _3DFrameType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(_3DCartesianCoordinatesEClass, _3DCartesianCoordinates.class, "_3DCartesianCoordinates", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(_3DOrientationEClass, _3DOrientation.class, "_3DOrientation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(analogUnitTypeEClass, AnalogUnitType.class, "AnalogUnitType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(baseDataVariableTypeEClass, BaseDataVariableType.class, "BaseDataVariableType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(durationStringEClass, DurationString.class, "DurationString", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dateTimeEClass, DateTime.class, "DateTime", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(_3DVectorEClass, _3DVector.class, "_3DVector", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(_3DVectorTypeEClass, _3DVectorType.class, "_3DVectorType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(get_3DVectorType_X(), theTypesPackage.getReal(), "X", null, 1, 1, _3DVectorType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(get_3DVectorType_Y(), theTypesPackage.getReal(), "Y", null, 1, 1, _3DVectorType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(get_3DVectorType_Z(), theTypesPackage.getReal(), "Z", null, 1, 1, _3DVectorType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(localizedTextEClass, LocalizedText.class, "LocalizedText", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(rationalNumberEClass, RationalNumber.class, "RationalNumber", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(doubleEClass, org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.Double.class, "Double", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create annotations
		// http://www.eclipse.org/uml2/2.0.0/UML
		createUMLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/uml2/2.0.0/UML</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createUMLAnnotations() {
		String source = "http://www.eclipse.org/uml2/2.0.0/UML";
		addAnnotation
		  (_3DFrameTypeEClass,
		   source,
		   new String[] {
			   "originalName", "3DFrameType"
		   });
		addAnnotation
		  (_3DCartesianCoordinatesEClass,
		   source,
		   new String[] {
			   "originalName", "3DCartesianCoordinates"
		   });
		addAnnotation
		  (_3DOrientationEClass,
		   source,
		   new String[] {
			   "originalName", "3DOrientation"
		   });
		addAnnotation
		  (_3DVectorEClass,
		   source,
		   new String[] {
			   "originalName", "3DVector"
		   });
		addAnnotation
		  (_3DVectorTypeEClass,
		   source,
		   new String[] {
			   "originalName", "3DVectorType"
		   });
	}

} //OPC_UA_Robotics_CS_LibraryPackageImpl
