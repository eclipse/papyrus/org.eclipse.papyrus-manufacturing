/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter Set Safety State Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * � Variable OperationalMode: The OperationalMode variable provides information about the current operational mode. Allowed values are described in OperationalModeEnumeration, see ISO 10218-1:2011 Ch.5.7 Operational Modes.
 * � Variable EmergencyStop: The EmergencyStop variable is TRUE if one or more of the emergency stop functions in the robot system are active, FALSE otherwise. If the EmergencyStopFunctions object is provided, then the value of this variable is TRUE if one or more of the listed emergency stop functions are active.
 * � Variable ProtectiveStop: The ProtectiveStop variable is TRUE if one or more of the enabled protective stop functions in the system are active, FALSE otherwise. If the ProtectiveStopFunctions object is provided, then the value of this variable is TRUE if one or more of the listed protective stop functions are enabled and active.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetSafetyStateType#isEmergencyStop <em>Emergency Stop</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetSafetyStateType#getOperationalMode <em>Operational Mode</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetSafetyStateType#isProtectiveStop <em>Protective Stop</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetSafetyStateType()
 * @model
 * @generated
 */
public interface ParameterSetSafetyStateType extends EObject {
	/**
	 * Returns the value of the '<em><b>Emergency Stop</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Emergency Stop</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Emergency Stop</em>' attribute.
	 * @see #setEmergencyStop(boolean)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetSafetyStateType_EmergencyStop()
	 * @model dataType="org.eclipse.uml2.types.Boolean" required="true" ordered="false"
	 * @generated
	 */
	boolean isEmergencyStop();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetSafetyStateType#isEmergencyStop <em>Emergency Stop</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Emergency Stop</em>' attribute.
	 * @see #isEmergencyStop()
	 * @generated
	 */
	void setEmergencyStop(boolean value);

	/**
	 * Returns the value of the '<em><b>Operational Mode</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OperationalModeEnumeration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operational Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operational Mode</em>' attribute.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OperationalModeEnumeration
	 * @see #setOperationalMode(OperationalModeEnumeration)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetSafetyStateType_OperationalMode()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	OperationalModeEnumeration getOperationalMode();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetSafetyStateType#getOperationalMode <em>Operational Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operational Mode</em>' attribute.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OperationalModeEnumeration
	 * @see #getOperationalMode()
	 * @generated
	 */
	void setOperationalMode(OperationalModeEnumeration value);

	/**
	 * Returns the value of the '<em><b>Protective Stop</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protective Stop</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protective Stop</em>' attribute.
	 * @see #setProtectiveStop(boolean)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetSafetyStateType_ProtectiveStop()
	 * @model dataType="org.eclipse.uml2.types.Boolean" required="true" ordered="false"
	 * @generated
	 */
	boolean isProtectiveStop();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetSafetyStateType#isProtectiveStop <em>Protective Stop</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Protective Stop</em>' attribute.
	 * @see #isProtectiveStop()
	 * @generated
	 */
	void setProtectiveStop(boolean value);

} // ParameterSetSafetyStateType
