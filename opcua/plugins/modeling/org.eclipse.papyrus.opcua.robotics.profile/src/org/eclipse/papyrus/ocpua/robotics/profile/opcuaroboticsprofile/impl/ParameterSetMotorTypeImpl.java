/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotorType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameter Set Motor Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetMotorTypeImpl#isBrakeReleased <em>Brake Released</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetMotorTypeImpl#getEffectiveLoadRate <em>Effective Load Rate</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetMotorTypeImpl#getMotorTemperature <em>Motor Temperature</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetMotorTypeImpl#getNotCS_MotorIntensity <em>Not CS Motor Intensity</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ParameterSetMotorTypeImpl extends MinimalEObjectImpl.Container implements ParameterSetMotorType {
	/**
	 * The default value of the '{@link #isBrakeReleased() <em>Brake Released</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBrakeReleased()
	 * @generated
	 * @ordered
	 */
	protected static final boolean BRAKE_RELEASED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isBrakeReleased() <em>Brake Released</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBrakeReleased()
	 * @generated
	 * @ordered
	 */
	protected boolean brakeReleased = BRAKE_RELEASED_EDEFAULT;

	/**
	 * The default value of the '{@link #getEffectiveLoadRate() <em>Effective Load Rate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEffectiveLoadRate()
	 * @generated
	 * @ordered
	 */
	protected static final int EFFECTIVE_LOAD_RATE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getEffectiveLoadRate() <em>Effective Load Rate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEffectiveLoadRate()
	 * @generated
	 * @ordered
	 */
	protected int effectiveLoadRate = EFFECTIVE_LOAD_RATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getMotorTemperature() <em>Motor Temperature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMotorTemperature()
	 * @generated
	 * @ordered
	 */
	protected static final double MOTOR_TEMPERATURE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getMotorTemperature() <em>Motor Temperature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMotorTemperature()
	 * @generated
	 * @ordered
	 */
	protected double motorTemperature = MOTOR_TEMPERATURE_EDEFAULT;

	/**
	 * The default value of the '{@link #getNotCS_MotorIntensity() <em>Not CS Motor Intensity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotCS_MotorIntensity()
	 * @generated
	 * @ordered
	 */
	protected static final int NOT_CS_MOTOR_INTENSITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNotCS_MotorIntensity() <em>Not CS Motor Intensity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotCS_MotorIntensity()
	 * @generated
	 * @ordered
	 */
	protected int notCS_MotorIntensity = NOT_CS_MOTOR_INTENSITY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParameterSetMotorTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUARoboticsProfilePackage.Literals.PARAMETER_SET_MOTOR_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isBrakeReleased() {
		return brakeReleased;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBrakeReleased(boolean newBrakeReleased) {
		boolean oldBrakeReleased = brakeReleased;
		brakeReleased = newBrakeReleased;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_MOTOR_TYPE__BRAKE_RELEASED, oldBrakeReleased, brakeReleased));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getEffectiveLoadRate() {
		return effectiveLoadRate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEffectiveLoadRate(int newEffectiveLoadRate) {
		int oldEffectiveLoadRate = effectiveLoadRate;
		effectiveLoadRate = newEffectiveLoadRate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_MOTOR_TYPE__EFFECTIVE_LOAD_RATE, oldEffectiveLoadRate, effectiveLoadRate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getMotorTemperature() {
		return motorTemperature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMotorTemperature(double newMotorTemperature) {
		double oldMotorTemperature = motorTemperature;
		motorTemperature = newMotorTemperature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_MOTOR_TYPE__MOTOR_TEMPERATURE, oldMotorTemperature, motorTemperature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNotCS_MotorIntensity() {
		return notCS_MotorIntensity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotCS_MotorIntensity(int newNotCS_MotorIntensity) {
		int oldNotCS_MotorIntensity = notCS_MotorIntensity;
		notCS_MotorIntensity = newNotCS_MotorIntensity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_MOTOR_TYPE__NOT_CS_MOTOR_INTENSITY, oldNotCS_MotorIntensity, notCS_MotorIntensity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTOR_TYPE__BRAKE_RELEASED:
				return isBrakeReleased();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTOR_TYPE__EFFECTIVE_LOAD_RATE:
				return getEffectiveLoadRate();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTOR_TYPE__MOTOR_TEMPERATURE:
				return getMotorTemperature();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTOR_TYPE__NOT_CS_MOTOR_INTENSITY:
				return getNotCS_MotorIntensity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTOR_TYPE__BRAKE_RELEASED:
				setBrakeReleased((Boolean)newValue);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTOR_TYPE__EFFECTIVE_LOAD_RATE:
				setEffectiveLoadRate((Integer)newValue);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTOR_TYPE__MOTOR_TEMPERATURE:
				setMotorTemperature((Double)newValue);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTOR_TYPE__NOT_CS_MOTOR_INTENSITY:
				setNotCS_MotorIntensity((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTOR_TYPE__BRAKE_RELEASED:
				setBrakeReleased(BRAKE_RELEASED_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTOR_TYPE__EFFECTIVE_LOAD_RATE:
				setEffectiveLoadRate(EFFECTIVE_LOAD_RATE_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTOR_TYPE__MOTOR_TEMPERATURE:
				setMotorTemperature(MOTOR_TEMPERATURE_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTOR_TYPE__NOT_CS_MOTOR_INTENSITY:
				setNotCS_MotorIntensity(NOT_CS_MOTOR_INTENSITY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTOR_TYPE__BRAKE_RELEASED:
				return brakeReleased != BRAKE_RELEASED_EDEFAULT;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTOR_TYPE__EFFECTIVE_LOAD_RATE:
				return effectiveLoadRate != EFFECTIVE_LOAD_RATE_EDEFAULT;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTOR_TYPE__MOTOR_TEMPERATURE:
				return motorTemperature != MOTOR_TEMPERATURE_EDEFAULT;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTOR_TYPE__NOT_CS_MOTOR_INTENSITY:
				return notCS_MotorIntensity != NOT_CS_MOTOR_INTENSITY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (BrakeReleased: ");
		result.append(brakeReleased);
		result.append(", EffectiveLoadRate: ");
		result.append(effectiveLoadRate);
		result.append(", MotorTemperature: ");
		result.append(motorTemperature);
		result.append(", notCS_MotorIntensity: ");
		result.append(notCS_MotorIntensity);
		result.append(')');
		return result.toString();
	}

} //ParameterSetMotorTypeImpl
