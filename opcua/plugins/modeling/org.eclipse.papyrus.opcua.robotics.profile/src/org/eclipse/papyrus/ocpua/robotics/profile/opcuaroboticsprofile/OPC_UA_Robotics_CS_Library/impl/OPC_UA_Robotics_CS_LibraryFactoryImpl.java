/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.AnalogUnitType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.BaseDataVariableType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.DateTime;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.DurationString;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.LocalizedText;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.OPC_UA_Robotics_CS_LibraryFactory;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.OPC_UA_Robotics_CS_LibraryPackage;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.RationalNumber;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library._3DCartesianCoordinates;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library._3DFrameType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library._3DOrientation;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library._3DVector;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library._3DVectorType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OPC_UA_Robotics_CS_LibraryFactoryImpl extends EFactoryImpl implements OPC_UA_Robotics_CS_LibraryFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OPC_UA_Robotics_CS_LibraryFactory init() {
		try {
			OPC_UA_Robotics_CS_LibraryFactory theOPC_UA_Robotics_CS_LibraryFactory = (OPC_UA_Robotics_CS_LibraryFactory)EPackage.Registry.INSTANCE.getEFactory(OPC_UA_Robotics_CS_LibraryPackage.eNS_URI);
			if (theOPC_UA_Robotics_CS_LibraryFactory != null) {
				return theOPC_UA_Robotics_CS_LibraryFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OPC_UA_Robotics_CS_LibraryFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPC_UA_Robotics_CS_LibraryFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case OPC_UA_Robotics_CS_LibraryPackage._3D_FRAME_TYPE: return create_3DFrameType();
			case OPC_UA_Robotics_CS_LibraryPackage._3D_CARTESIAN_COORDINATES: return create_3DCartesianCoordinates();
			case OPC_UA_Robotics_CS_LibraryPackage._3D_ORIENTATION: return create_3DOrientation();
			case OPC_UA_Robotics_CS_LibraryPackage.ANALOG_UNIT_TYPE: return createAnalogUnitType();
			case OPC_UA_Robotics_CS_LibraryPackage.BASE_DATA_VARIABLE_TYPE: return createBaseDataVariableType();
			case OPC_UA_Robotics_CS_LibraryPackage.DURATION_STRING: return createDurationString();
			case OPC_UA_Robotics_CS_LibraryPackage.DATE_TIME: return createDateTime();
			case OPC_UA_Robotics_CS_LibraryPackage._3D_VECTOR: return create_3DVector();
			case OPC_UA_Robotics_CS_LibraryPackage._3D_VECTOR_TYPE: return create_3DVectorType();
			case OPC_UA_Robotics_CS_LibraryPackage.LOCALIZED_TEXT: return createLocalizedText();
			case OPC_UA_Robotics_CS_LibraryPackage.RATIONAL_NUMBER: return createRationalNumber();
			case OPC_UA_Robotics_CS_LibraryPackage.DOUBLE: return createDouble();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public _3DFrameType create_3DFrameType() {
		_3DFrameTypeImpl _3DFrameType = new _3DFrameTypeImpl();
		return _3DFrameType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public _3DCartesianCoordinates create_3DCartesianCoordinates() {
		_3DCartesianCoordinatesImpl _3DCartesianCoordinates = new _3DCartesianCoordinatesImpl();
		return _3DCartesianCoordinates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public _3DOrientation create_3DOrientation() {
		_3DOrientationImpl _3DOrientation = new _3DOrientationImpl();
		return _3DOrientation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnalogUnitType createAnalogUnitType() {
		AnalogUnitTypeImpl analogUnitType = new AnalogUnitTypeImpl();
		return analogUnitType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseDataVariableType createBaseDataVariableType() {
		BaseDataVariableTypeImpl baseDataVariableType = new BaseDataVariableTypeImpl();
		return baseDataVariableType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DurationString createDurationString() {
		DurationStringImpl durationString = new DurationStringImpl();
		return durationString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DateTime createDateTime() {
		DateTimeImpl dateTime = new DateTimeImpl();
		return dateTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public _3DVector create_3DVector() {
		_3DVectorImpl _3DVector = new _3DVectorImpl();
		return _3DVector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public _3DVectorType create_3DVectorType() {
		_3DVectorTypeImpl _3DVectorType = new _3DVectorTypeImpl();
		return _3DVectorType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocalizedText createLocalizedText() {
		LocalizedTextImpl localizedText = new LocalizedTextImpl();
		return localizedText;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RationalNumber createRationalNumber() {
		RationalNumberImpl rationalNumber = new RationalNumberImpl();
		return rationalNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.Double createDouble() {
		DoubleImpl double_ = new DoubleImpl();
		return double_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPC_UA_Robotics_CS_LibraryPackage getOPC_UA_Robotics_CS_LibraryPackage() {
		return (OPC_UA_Robotics_CS_LibraryPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static OPC_UA_Robotics_CS_LibraryPackage getPackage() {
		return OPC_UA_Robotics_CS_LibraryPackage.eINSTANCE;
	}

} //OPC_UA_Robotics_CS_LibraryFactoryImpl
