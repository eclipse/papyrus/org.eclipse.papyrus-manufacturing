/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.di.opcuadiprofile.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.ParameterSetTopologyElementType;

import org.eclipse.papyrus.opcua.opcuaprofile.BaseDataVariableType;

import org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseObjectTypeImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameter Set Topology Element Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ParameterSetTopologyElementTypeImpl#getParameterIdentifier <em>Parameter Identifier</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ParameterSetTopologyElementTypeImpl extends BaseObjectTypeImpl implements ParameterSetTopologyElementType {
	/**
	 * The cached value of the '{@link #getParameterIdentifier() <em>Parameter Identifier</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterIdentifier()
	 * @generated
	 * @ordered
	 */
	protected BaseDataVariableType parameterIdentifier;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParameterSetTopologyElementTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUADIProfilePackage.Literals.PARAMETER_SET_TOPOLOGY_ELEMENT_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseDataVariableType getParameterIdentifier() {
		if (parameterIdentifier != null && parameterIdentifier.eIsProxy()) {
			InternalEObject oldParameterIdentifier = (InternalEObject)parameterIdentifier;
			parameterIdentifier = (BaseDataVariableType)eResolveProxy(oldParameterIdentifier);
			if (parameterIdentifier != oldParameterIdentifier) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OPCUADIProfilePackage.PARAMETER_SET_TOPOLOGY_ELEMENT_TYPE__PARAMETER_IDENTIFIER, oldParameterIdentifier, parameterIdentifier));
			}
		}
		return parameterIdentifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseDataVariableType basicGetParameterIdentifier() {
		return parameterIdentifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterIdentifier(BaseDataVariableType newParameterIdentifier) {
		BaseDataVariableType oldParameterIdentifier = parameterIdentifier;
		parameterIdentifier = newParameterIdentifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.PARAMETER_SET_TOPOLOGY_ELEMENT_TYPE__PARAMETER_IDENTIFIER, oldParameterIdentifier, parameterIdentifier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPCUADIProfilePackage.PARAMETER_SET_TOPOLOGY_ELEMENT_TYPE__PARAMETER_IDENTIFIER:
				if (resolve) return getParameterIdentifier();
				return basicGetParameterIdentifier();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPCUADIProfilePackage.PARAMETER_SET_TOPOLOGY_ELEMENT_TYPE__PARAMETER_IDENTIFIER:
				setParameterIdentifier((BaseDataVariableType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPCUADIProfilePackage.PARAMETER_SET_TOPOLOGY_ELEMENT_TYPE__PARAMETER_IDENTIFIER:
				setParameterIdentifier((BaseDataVariableType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPCUADIProfilePackage.PARAMETER_SET_TOPOLOGY_ELEMENT_TYPE__PARAMETER_IDENTIFIER:
				return parameterIdentifier != null;
		}
		return super.eIsSet(featureID);
	}

} //ParameterSetTopologyElementTypeImpl
