/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.di.opcuadiprofile;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage
 * @generated
 */
public interface OPCUADIProfileFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OPCUADIProfileFactory eINSTANCE = org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfileFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Device Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Device Type</em>'.
	 * @generated
	 */
	DeviceType createDeviceType();

	/**
	 * Returns a new object of class '<em>Connection Point Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Connection Point Type</em>'.
	 * @generated
	 */
	ConnectionPointType createConnectionPointType();

	/**
	 * Returns a new object of class '<em>Functional Group Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Functional Group Type</em>'.
	 * @generated
	 */
	FunctionalGroupType createFunctionalGroupType();

	/**
	 * Returns a new object of class '<em>Locking Services Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Locking Services Type</em>'.
	 * @generated
	 */
	LockingServicesType createLockingServicesType();

	/**
	 * Returns a new object of class '<em>Parameter Set Topology Element Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parameter Set Topology Element Type</em>'.
	 * @generated
	 */
	ParameterSetTopologyElementType createParameterSetTopologyElementType();

	/**
	 * Returns a new object of class '<em>Method Set Topology Element Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Method Set Topology Element Type</em>'.
	 * @generated
	 */
	MethodSetTopologyElementType createMethodSetTopologyElementType();

	/**
	 * Returns a new object of class '<em>Software Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Software Type</em>'.
	 * @generated
	 */
	SoftwareType createSoftwareType();

	/**
	 * Returns a new object of class '<em>Block Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Block Type</em>'.
	 * @generated
	 */
	BlockType createBlockType();

	/**
	 * Returns a new object of class '<em>Configurable Object Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Configurable Object Type</em>'.
	 * @generated
	 */
	ConfigurableObjectType createConfigurableObjectType();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	OPCUADIProfilePackage getOPCUADIProfilePackage();

} //OPCUADIProfileFactory
