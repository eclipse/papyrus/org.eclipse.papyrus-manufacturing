/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.di.opcuadiprofile.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.SoftwareType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Software Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SoftwareTypeImpl extends ComponentTypeImpl implements SoftwareType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SoftwareTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUADIProfilePackage.Literals.SOFTWARE_TYPE;
	}

} //SoftwareTypeImpl
