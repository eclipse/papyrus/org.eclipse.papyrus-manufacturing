/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.di.opcuadiprofile.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.FunctionalGroupType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.MethodSetTopologyElementType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.ParameterSetTopologyElementType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType;

import org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseObjectTypeImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Topology Element Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.TopologyElementTypeImpl#getGroupIdentifier <em>Group Identifier</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.TopologyElementTypeImpl#getIdentification <em>Identification</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.TopologyElementTypeImpl#getLock <em>Lock</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.TopologyElementTypeImpl#getParameterSet <em>Parameter Set</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.TopologyElementTypeImpl#getMethodSet <em>Method Set</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class TopologyElementTypeImpl extends BaseObjectTypeImpl implements TopologyElementType {
	/**
	 * The cached value of the '{@link #getGroupIdentifier() <em>Group Identifier</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroupIdentifier()
	 * @generated
	 * @ordered
	 */
	protected FunctionalGroupType groupIdentifier;

	/**
	 * The cached value of the '{@link #getIdentification() <em>Identification</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdentification()
	 * @generated
	 * @ordered
	 */
	protected FunctionalGroupType identification;

	/**
	 * The cached value of the '{@link #getLock() <em>Lock</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLock()
	 * @generated
	 * @ordered
	 */
	protected LockingServicesType lock;

	/**
	 * The cached value of the '{@link #getParameterSet() <em>Parameter Set</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterSet()
	 * @generated
	 * @ordered
	 */
	protected ParameterSetTopologyElementType parameterSet;

	/**
	 * The cached value of the '{@link #getMethodSet() <em>Method Set</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMethodSet()
	 * @generated
	 * @ordered
	 */
	protected MethodSetTopologyElementType methodSet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TopologyElementTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUADIProfilePackage.Literals.TOPOLOGY_ELEMENT_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionalGroupType getGroupIdentifier() {
		if (groupIdentifier != null && groupIdentifier.eIsProxy()) {
			InternalEObject oldGroupIdentifier = (InternalEObject)groupIdentifier;
			groupIdentifier = (FunctionalGroupType)eResolveProxy(oldGroupIdentifier);
			if (groupIdentifier != oldGroupIdentifier) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__GROUP_IDENTIFIER, oldGroupIdentifier, groupIdentifier));
			}
		}
		return groupIdentifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionalGroupType basicGetGroupIdentifier() {
		return groupIdentifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGroupIdentifier(FunctionalGroupType newGroupIdentifier) {
		FunctionalGroupType oldGroupIdentifier = groupIdentifier;
		groupIdentifier = newGroupIdentifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__GROUP_IDENTIFIER, oldGroupIdentifier, groupIdentifier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionalGroupType getIdentification() {
		if (identification != null && identification.eIsProxy()) {
			InternalEObject oldIdentification = (InternalEObject)identification;
			identification = (FunctionalGroupType)eResolveProxy(oldIdentification);
			if (identification != oldIdentification) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__IDENTIFICATION, oldIdentification, identification));
			}
		}
		return identification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionalGroupType basicGetIdentification() {
		return identification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdentification(FunctionalGroupType newIdentification) {
		FunctionalGroupType oldIdentification = identification;
		identification = newIdentification;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__IDENTIFICATION, oldIdentification, identification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LockingServicesType getLock() {
		if (lock != null && lock.eIsProxy()) {
			InternalEObject oldLock = (InternalEObject)lock;
			lock = (LockingServicesType)eResolveProxy(oldLock);
			if (lock != oldLock) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__LOCK, oldLock, lock));
			}
		}
		return lock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LockingServicesType basicGetLock() {
		return lock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLock(LockingServicesType newLock) {
		LockingServicesType oldLock = lock;
		lock = newLock;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__LOCK, oldLock, lock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterSetTopologyElementType getParameterSet() {
		return parameterSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParameterSet(ParameterSetTopologyElementType newParameterSet, NotificationChain msgs) {
		ParameterSetTopologyElementType oldParameterSet = parameterSet;
		parameterSet = newParameterSet;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__PARAMETER_SET, oldParameterSet, newParameterSet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterSet(ParameterSetTopologyElementType newParameterSet) {
		if (newParameterSet != parameterSet) {
			NotificationChain msgs = null;
			if (parameterSet != null)
				msgs = ((InternalEObject)parameterSet).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__PARAMETER_SET, null, msgs);
			if (newParameterSet != null)
				msgs = ((InternalEObject)newParameterSet).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__PARAMETER_SET, null, msgs);
			msgs = basicSetParameterSet(newParameterSet, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__PARAMETER_SET, newParameterSet, newParameterSet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MethodSetTopologyElementType getMethodSet() {
		return methodSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMethodSet(MethodSetTopologyElementType newMethodSet, NotificationChain msgs) {
		MethodSetTopologyElementType oldMethodSet = methodSet;
		methodSet = newMethodSet;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__METHOD_SET, oldMethodSet, newMethodSet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMethodSet(MethodSetTopologyElementType newMethodSet) {
		if (newMethodSet != methodSet) {
			NotificationChain msgs = null;
			if (methodSet != null)
				msgs = ((InternalEObject)methodSet).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__METHOD_SET, null, msgs);
			if (newMethodSet != null)
				msgs = ((InternalEObject)newMethodSet).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__METHOD_SET, null, msgs);
			msgs = basicSetMethodSet(newMethodSet, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__METHOD_SET, newMethodSet, newMethodSet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__PARAMETER_SET:
				return basicSetParameterSet(null, msgs);
			case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__METHOD_SET:
				return basicSetMethodSet(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__GROUP_IDENTIFIER:
				if (resolve) return getGroupIdentifier();
				return basicGetGroupIdentifier();
			case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__IDENTIFICATION:
				if (resolve) return getIdentification();
				return basicGetIdentification();
			case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__LOCK:
				if (resolve) return getLock();
				return basicGetLock();
			case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__PARAMETER_SET:
				return getParameterSet();
			case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__METHOD_SET:
				return getMethodSet();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__GROUP_IDENTIFIER:
				setGroupIdentifier((FunctionalGroupType)newValue);
				return;
			case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__IDENTIFICATION:
				setIdentification((FunctionalGroupType)newValue);
				return;
			case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__LOCK:
				setLock((LockingServicesType)newValue);
				return;
			case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__PARAMETER_SET:
				setParameterSet((ParameterSetTopologyElementType)newValue);
				return;
			case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__METHOD_SET:
				setMethodSet((MethodSetTopologyElementType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__GROUP_IDENTIFIER:
				setGroupIdentifier((FunctionalGroupType)null);
				return;
			case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__IDENTIFICATION:
				setIdentification((FunctionalGroupType)null);
				return;
			case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__LOCK:
				setLock((LockingServicesType)null);
				return;
			case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__PARAMETER_SET:
				setParameterSet((ParameterSetTopologyElementType)null);
				return;
			case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__METHOD_SET:
				setMethodSet((MethodSetTopologyElementType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__GROUP_IDENTIFIER:
				return groupIdentifier != null;
			case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__IDENTIFICATION:
				return identification != null;
			case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__LOCK:
				return lock != null;
			case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__PARAMETER_SET:
				return parameterSet != null;
			case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__METHOD_SET:
				return methodSet != null;
		}
		return super.eIsSet(featureID);
	}

} //TopologyElementTypeImpl
