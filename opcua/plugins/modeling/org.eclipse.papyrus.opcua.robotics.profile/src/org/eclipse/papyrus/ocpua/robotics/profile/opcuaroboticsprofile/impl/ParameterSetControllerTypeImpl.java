/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameter Set Controller Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetControllerTypeImpl#getTotalPowerOnTime <em>Total Power On Time</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetControllerTypeImpl#getStartUpTime <em>Start Up Time</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetControllerTypeImpl#getUpsState <em>Ups State</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetControllerTypeImpl#getTotalEnergyConsumption <em>Total Energy Consumption</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetControllerTypeImpl#getCabinetFanSpeed <em>Cabinet Fan Speed</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetControllerTypeImpl#getCPUFanSpeed <em>CPU Fan Speed</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetControllerTypeImpl#getInputVoltage <em>Input Voltage</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetControllerTypeImpl#getTemperature <em>Temperature</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ParameterSetControllerTypeImpl extends MinimalEObjectImpl.Container implements ParameterSetControllerType {
	/**
	 * The default value of the '{@link #getTotalPowerOnTime() <em>Total Power On Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTotalPowerOnTime()
	 * @generated
	 * @ordered
	 */
	protected static final String TOTAL_POWER_ON_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTotalPowerOnTime() <em>Total Power On Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTotalPowerOnTime()
	 * @generated
	 * @ordered
	 */
	protected String totalPowerOnTime = TOTAL_POWER_ON_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getStartUpTime() <em>Start Up Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartUpTime()
	 * @generated
	 * @ordered
	 */
	protected static final String START_UP_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStartUpTime() <em>Start Up Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartUpTime()
	 * @generated
	 * @ordered
	 */
	protected String startUpTime = START_UP_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getUpsState() <em>Ups State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpsState()
	 * @generated
	 * @ordered
	 */
	protected static final String UPS_STATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUpsState() <em>Ups State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpsState()
	 * @generated
	 * @ordered
	 */
	protected String upsState = UPS_STATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getTotalEnergyConsumption() <em>Total Energy Consumption</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTotalEnergyConsumption()
	 * @generated
	 * @ordered
	 */
	protected static final double TOTAL_ENERGY_CONSUMPTION_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getTotalEnergyConsumption() <em>Total Energy Consumption</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTotalEnergyConsumption()
	 * @generated
	 * @ordered
	 */
	protected double totalEnergyConsumption = TOTAL_ENERGY_CONSUMPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getCabinetFanSpeed() <em>Cabinet Fan Speed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCabinetFanSpeed()
	 * @generated
	 * @ordered
	 */
	protected static final double CABINET_FAN_SPEED_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getCabinetFanSpeed() <em>Cabinet Fan Speed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCabinetFanSpeed()
	 * @generated
	 * @ordered
	 */
	protected double cabinetFanSpeed = CABINET_FAN_SPEED_EDEFAULT;

	/**
	 * The default value of the '{@link #getCPUFanSpeed() <em>CPU Fan Speed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCPUFanSpeed()
	 * @generated
	 * @ordered
	 */
	protected static final double CPU_FAN_SPEED_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getCPUFanSpeed() <em>CPU Fan Speed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCPUFanSpeed()
	 * @generated
	 * @ordered
	 */
	protected double cpuFanSpeed = CPU_FAN_SPEED_EDEFAULT;

	/**
	 * The default value of the '{@link #getInputVoltage() <em>Input Voltage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputVoltage()
	 * @generated
	 * @ordered
	 */
	protected static final double INPUT_VOLTAGE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getInputVoltage() <em>Input Voltage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputVoltage()
	 * @generated
	 * @ordered
	 */
	protected double inputVoltage = INPUT_VOLTAGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getTemperature() <em>Temperature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemperature()
	 * @generated
	 * @ordered
	 */
	protected static final double TEMPERATURE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getTemperature() <em>Temperature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemperature()
	 * @generated
	 * @ordered
	 */
	protected double temperature = TEMPERATURE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParameterSetControllerTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUARoboticsProfilePackage.Literals.PARAMETER_SET_CONTROLLER_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTotalPowerOnTime() {
		return totalPowerOnTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTotalPowerOnTime(String newTotalPowerOnTime) {
		String oldTotalPowerOnTime = totalPowerOnTime;
		totalPowerOnTime = newTotalPowerOnTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__TOTAL_POWER_ON_TIME, oldTotalPowerOnTime, totalPowerOnTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getStartUpTime() {
		return startUpTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartUpTime(String newStartUpTime) {
		String oldStartUpTime = startUpTime;
		startUpTime = newStartUpTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__START_UP_TIME, oldStartUpTime, startUpTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUpsState() {
		return upsState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUpsState(String newUpsState) {
		String oldUpsState = upsState;
		upsState = newUpsState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__UPS_STATE, oldUpsState, upsState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getTotalEnergyConsumption() {
		return totalEnergyConsumption;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTotalEnergyConsumption(double newTotalEnergyConsumption) {
		double oldTotalEnergyConsumption = totalEnergyConsumption;
		totalEnergyConsumption = newTotalEnergyConsumption;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__TOTAL_ENERGY_CONSUMPTION, oldTotalEnergyConsumption, totalEnergyConsumption));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getCabinetFanSpeed() {
		return cabinetFanSpeed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCabinetFanSpeed(double newCabinetFanSpeed) {
		double oldCabinetFanSpeed = cabinetFanSpeed;
		cabinetFanSpeed = newCabinetFanSpeed;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__CABINET_FAN_SPEED, oldCabinetFanSpeed, cabinetFanSpeed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getCPUFanSpeed() {
		return cpuFanSpeed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCPUFanSpeed(double newCPUFanSpeed) {
		double oldCPUFanSpeed = cpuFanSpeed;
		cpuFanSpeed = newCPUFanSpeed;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__CPU_FAN_SPEED, oldCPUFanSpeed, cpuFanSpeed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getInputVoltage() {
		return inputVoltage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInputVoltage(double newInputVoltage) {
		double oldInputVoltage = inputVoltage;
		inputVoltage = newInputVoltage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__INPUT_VOLTAGE, oldInputVoltage, inputVoltage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getTemperature() {
		return temperature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTemperature(double newTemperature) {
		double oldTemperature = temperature;
		temperature = newTemperature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__TEMPERATURE, oldTemperature, temperature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__TOTAL_POWER_ON_TIME:
				return getTotalPowerOnTime();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__START_UP_TIME:
				return getStartUpTime();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__UPS_STATE:
				return getUpsState();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__TOTAL_ENERGY_CONSUMPTION:
				return getTotalEnergyConsumption();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__CABINET_FAN_SPEED:
				return getCabinetFanSpeed();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__CPU_FAN_SPEED:
				return getCPUFanSpeed();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__INPUT_VOLTAGE:
				return getInputVoltage();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__TEMPERATURE:
				return getTemperature();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__TOTAL_POWER_ON_TIME:
				setTotalPowerOnTime((String)newValue);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__START_UP_TIME:
				setStartUpTime((String)newValue);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__UPS_STATE:
				setUpsState((String)newValue);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__TOTAL_ENERGY_CONSUMPTION:
				setTotalEnergyConsumption((Double)newValue);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__CABINET_FAN_SPEED:
				setCabinetFanSpeed((Double)newValue);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__CPU_FAN_SPEED:
				setCPUFanSpeed((Double)newValue);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__INPUT_VOLTAGE:
				setInputVoltage((Double)newValue);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__TEMPERATURE:
				setTemperature((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__TOTAL_POWER_ON_TIME:
				setTotalPowerOnTime(TOTAL_POWER_ON_TIME_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__START_UP_TIME:
				setStartUpTime(START_UP_TIME_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__UPS_STATE:
				setUpsState(UPS_STATE_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__TOTAL_ENERGY_CONSUMPTION:
				setTotalEnergyConsumption(TOTAL_ENERGY_CONSUMPTION_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__CABINET_FAN_SPEED:
				setCabinetFanSpeed(CABINET_FAN_SPEED_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__CPU_FAN_SPEED:
				setCPUFanSpeed(CPU_FAN_SPEED_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__INPUT_VOLTAGE:
				setInputVoltage(INPUT_VOLTAGE_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__TEMPERATURE:
				setTemperature(TEMPERATURE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__TOTAL_POWER_ON_TIME:
				return TOTAL_POWER_ON_TIME_EDEFAULT == null ? totalPowerOnTime != null : !TOTAL_POWER_ON_TIME_EDEFAULT.equals(totalPowerOnTime);
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__START_UP_TIME:
				return START_UP_TIME_EDEFAULT == null ? startUpTime != null : !START_UP_TIME_EDEFAULT.equals(startUpTime);
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__UPS_STATE:
				return UPS_STATE_EDEFAULT == null ? upsState != null : !UPS_STATE_EDEFAULT.equals(upsState);
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__TOTAL_ENERGY_CONSUMPTION:
				return totalEnergyConsumption != TOTAL_ENERGY_CONSUMPTION_EDEFAULT;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__CABINET_FAN_SPEED:
				return cabinetFanSpeed != CABINET_FAN_SPEED_EDEFAULT;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__CPU_FAN_SPEED:
				return cpuFanSpeed != CPU_FAN_SPEED_EDEFAULT;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__INPUT_VOLTAGE:
				return inputVoltage != INPUT_VOLTAGE_EDEFAULT;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE__TEMPERATURE:
				return temperature != TEMPERATURE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (TotalPowerOnTime: ");
		result.append(totalPowerOnTime);
		result.append(", StartUpTime: ");
		result.append(startUpTime);
		result.append(", UpsState: ");
		result.append(upsState);
		result.append(", TotalEnergyConsumption: ");
		result.append(totalEnergyConsumption);
		result.append(", CabinetFanSpeed: ");
		result.append(cabinetFanSpeed);
		result.append(", CPUFanSpeed: ");
		result.append(cpuFanSpeed);
		result.append(", InputVoltage: ");
		result.append(inputVoltage);
		result.append(", Temperature: ");
		result.append(temperature);
		result.append(')');
		return result.toString();
	}

} //ParameterSetControllerTypeImpl
