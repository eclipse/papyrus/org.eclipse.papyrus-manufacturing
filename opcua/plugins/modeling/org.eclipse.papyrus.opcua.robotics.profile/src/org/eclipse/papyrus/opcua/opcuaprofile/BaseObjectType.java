/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.opcuaprofile;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Object Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType#getBase_Class <em>Base Class</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType#getNodeId <em>Node Id</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType#getNamespaceUri <em>Namespace Uri</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType#getBrowseName <em>Browse Name</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType#getNodeClass <em>Node Class</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPCUAProfilePackage#getBaseObjectType()
 * @model
 * @generated
 */
public interface BaseObjectType extends EObject {
	/**
	 * Returns the value of the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Class</em>' reference.
	 * @see #setBase_Class(org.eclipse.uml2.uml.Class)
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPCUAProfilePackage#getBaseObjectType_Base_Class()
	 * @model ordered="false"
	 * @generated
	 */
	org.eclipse.uml2.uml.Class getBase_Class();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType#getBase_Class <em>Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Class</em>' reference.
	 * @see #getBase_Class()
	 * @generated
	 */
	void setBase_Class(org.eclipse.uml2.uml.Class value);

	/**
	 * Returns the value of the '<em><b>Node Id</b></em>' attribute.
	 * The default value is <code>"i=58"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node Id</em>' attribute.
	 * @see #setNodeId(String)
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPCUAProfilePackage#getBaseObjectType_NodeId()
	 * @model default="i=58" dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
	 * @generated
	 */
	String getNodeId();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType#getNodeId <em>Node Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Node Id</em>' attribute.
	 * @see #getNodeId()
	 * @generated
	 */
	void setNodeId(String value);

	/**
	 * Returns the value of the '<em><b>Namespace Uri</b></em>' attribute.
	 * The default value is <code>"http://opcfoundation.org/UA/"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Namespace Uri</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Namespace Uri</em>' attribute.
	 * @see #setNamespaceUri(String)
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPCUAProfilePackage#getBaseObjectType_NamespaceUri()
	 * @model default="http://opcfoundation.org/UA/" dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
	 * @generated
	 */
	String getNamespaceUri();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType#getNamespaceUri <em>Namespace Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Namespace Uri</em>' attribute.
	 * @see #getNamespaceUri()
	 * @generated
	 */
	void setNamespaceUri(String value);

	/**
	 * Returns the value of the '<em><b>Browse Name</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Browse Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Browse Name</em>' attribute.
	 * @see #setBrowseName(String)
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPCUAProfilePackage#getBaseObjectType_BrowseName()
	 * @model default="" dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
	 * @generated
	 */
	String getBrowseName();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType#getBrowseName <em>Browse Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Browse Name</em>' attribute.
	 * @see #getBrowseName()
	 * @generated
	 */
	void setBrowseName(String value);

	/**
	 * Returns the value of the '<em><b>Node Class</b></em>' attribute.
	 * The default value is <code>"ObjectType"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node Class</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node Class</em>' attribute.
	 * @see #setNodeClass(String)
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPCUAProfilePackage#getBaseObjectType_NodeClass()
	 * @model default="ObjectType" dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
	 * @generated
	 */
	String getNodeClass();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType#getNodeClass <em>Node Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Node Class</em>' attribute.
	 * @see #getNodeClass()
	 * @generated
	 */
	void setNodeClass(String value);

} // BaseObjectType
