/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Axis State Enumeration</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getAxisStateEnumeration()
 * @model
 * @generated
 */
public enum AxisStateEnumeration implements Enumerator {
	/**
	 * The '<em><b>JOINT SHUTTING DOWN MODE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Linear motion is a one dimensional motion along a straight line with defined limits. Motion movement is not going always in the same direction. Control unit is mainly mm.
	 * <!-- end-model-doc -->
	 * @see #JOINT_SHUTTING_DOWN_MODE_VALUE
	 * @generated
	 * @ordered
	 */
	JOINT_SHUTTING_DOWN_MODE(236, "JOINT_SHUTTING_DOWN_MODE", "JOINT_SHUTTING_DOWN_MODE"),

	/**
	 * The '<em><b>JOINT PART DCALIBRATION MODE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Linear motion is a one dimensional motion along a straight line with no limits. Motion movement is going endless in the same direction. Control unit is mainly mm.
	 * <!-- end-model-doc -->
	 * @see #JOINT_PART_DCALIBRATION_MODE_VALUE
	 * @generated
	 * @ordered
	 */
	JOINT_PART_DCALIBRATION_MODE(237, "JOINT_PART_D_CALIBRATION_MODE", "JOINT_PART_D_CALIBRATION_MODE"),

	/**
	 * The '<em><b>JOINT BACKDRIVE MODE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Any motion-profile which is not defined by the AxisMotionProfileEnumeration
	 * <!-- end-model-doc -->
	 * @see #_JOINT_BACKDRIVE_MODE_VALUE
	 * @generated
	 * @ordered
	 */
	_JOINT_BACKDRIVE_MODE(238, "_JOINT_BACKDRIVE_MODE", "_JOINT_BACKDRIVE_MODE"),

	/**
	 * The '<em><b>JOINT POWER OFF MODE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Rotary motion is a rotation along a circular path with defined limits. Motion movement is not going always in the same direction. Control unit is mainly degree.
	 * <!-- end-model-doc -->
	 * @see #JOINT_POWER_OFF_MODE_VALUE
	 * @generated
	 * @ordered
	 */
	JOINT_POWER_OFF_MODE(239, "JOINT_POWER_OFF_MODE", "JOINT_POWER_OFF_MODE"),

	/**
	 * The '<em><b>JOINT NOT RESPONDING MODE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Rotary motion is a rotation along a circular path with no limits. Motion movement is going endless in the same direction. Control unit is mainly degree.
	 * <!-- end-model-doc -->
	 * @see #JOINT_NOT_RESPONDING_MODE_VALUE
	 * @generated
	 * @ordered
	 */
	JOINT_NOT_RESPONDING_MODE(245, "JOINT_NOT_RESPONDING_MODE", "JOINT_NOT_RESPONDING_MODE"),

	/**
	 * The '<em><b>JOINT MOTOR INITIALISATION MODE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Linear motion is a one dimensional motion along a straight line with defined limits. Motion movement is not going always in the same direction. Control unit is mainly mm.
	 * <!-- end-model-doc -->
	 * @see #JOINT_MOTOR_INITIALISATION_MODE_VALUE
	 * @generated
	 * @ordered
	 */
	JOINT_MOTOR_INITIALISATION_MODE(246, "JOINT_MOTOR_INITIALISATION_MODE", "JOINT_MOTOR_INITIALISATION_MODE"),

	/**
	 * The '<em><b>JOINT BOOTING MODE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Linear motion is a one dimensional motion along a straight line with no limits. Motion movement is going endless in the same direction. Control unit is mainly mm.
	 * <!-- end-model-doc -->
	 * @see #JOINT_BOOTING_MODE_VALUE
	 * @generated
	 * @ordered
	 */
	JOINT_BOOTING_MODE(247, "JOINT_BOOTING_MODE", "JOINT_BOOTING_MODE"),

	/**
	 * The '<em><b>JOINT PART DCALIBRATION ERROR MODE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Any motion-profile which is not defined by the AxisMotionProfileEnumeration
	 * <!-- end-model-doc -->
	 * @see #JOINT_PART_DCALIBRATION_ERROR_MODE_VALUE
	 * @generated
	 * @ordered
	 */
	JOINT_PART_DCALIBRATION_ERROR_MODE(248, "JOINT_PART_D_CALIBRATION_ERROR_MODE", "JOINT_PART_D_CALIBRATION_ERROR_MODE"),

	/**
	 * The '<em><b>JOINT BOOTLOADER MODE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Rotary motion is a rotation along a circular path with defined limits. Motion movement is not going always in the same direction. Control unit is mainly degree.
	 * <!-- end-model-doc -->
	 * @see #JOINT_BOOTLOADER_MODE_VALUE
	 * @generated
	 * @ordered
	 */
	JOINT_BOOTLOADER_MODE(249, "JOINT_BOOTLOADER_MODE", "JOINT_BOOTLOADER_MODE"),

	/**
	 * The '<em><b>JOINT CALIBRATION MODE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Rotary motion is a rotation along a circular path with no limits. Motion movement is going endless in the same direction. Control unit is mainly degree.
	 * <!-- end-model-doc -->
	 * @see #JOINT_CALIBRATION_MODE_VALUE
	 * @generated
	 * @ordered
	 */
	JOINT_CALIBRATION_MODE(250, "JOINT_CALIBRATION_MODE", "JOINT_CALIBRATION_MODE"),

	/**
	 * The '<em><b>JOINT FAULT MODE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #JOINT_FAULT_MODE_VALUE
	 * @generated
	 * @ordered
	 */
	JOINT_FAULT_MODE(252, "JOINT_FAULT_MODE", "JOINT_FAULT_MODE"),

	/**
	 * The '<em><b>JOINT RUNNING MODE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Any motion-profile which is not defined by the AxisMotionProfileEnumeration
	 * <!-- end-model-doc -->
	 * @see #JOINT_RUNNING_MODE_VALUE
	 * @generated
	 * @ordered
	 */
	JOINT_RUNNING_MODE(253, "JOINT_RUNNING_MODE", "JOINT_RUNNING_MODE"),

	/**
	 * The '<em><b>JOINT IDLE MODE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Rotary motion is a rotation along a circular path with defined limits. Motion movement is not going always in the same direction. Control unit is mainly degree.
	 * <!-- end-model-doc -->
	 * @see #JOINT_IDLE_MODE_VALUE
	 * @generated
	 * @ordered
	 */
	JOINT_IDLE_MODE(255, "JOINT_IDLE_MODE", "JOINT_IDLE_MODE");

	/**
	 * The '<em><b>JOINT SHUTTING DOWN MODE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Linear motion is a one dimensional motion along a straight line with defined limits. Motion movement is not going always in the same direction. Control unit is mainly mm.
	 * <!-- end-model-doc -->
	 * @see #JOINT_SHUTTING_DOWN_MODE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int JOINT_SHUTTING_DOWN_MODE_VALUE = 236;

	/**
	 * The '<em><b>JOINT PART DCALIBRATION MODE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Linear motion is a one dimensional motion along a straight line with no limits. Motion movement is going endless in the same direction. Control unit is mainly mm.
	 * <!-- end-model-doc -->
	 * @see #JOINT_PART_DCALIBRATION_MODE
	 * @model name="JOINT_PART_D_CALIBRATION_MODE"
	 * @generated
	 * @ordered
	 */
	public static final int JOINT_PART_DCALIBRATION_MODE_VALUE = 237;

	/**
	 * The '<em><b>JOINT BACKDRIVE MODE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Any motion-profile which is not defined by the AxisMotionProfileEnumeration
	 * <!-- end-model-doc -->
	 * @see #_JOINT_BACKDRIVE_MODE
	 * @model annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName=' JOINT_BACKDRIVE_MODE'"
	 * @generated
	 * @ordered
	 */
	public static final int _JOINT_BACKDRIVE_MODE_VALUE = 238;

	/**
	 * The '<em><b>JOINT POWER OFF MODE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Rotary motion is a rotation along a circular path with defined limits. Motion movement is not going always in the same direction. Control unit is mainly degree.
	 * <!-- end-model-doc -->
	 * @see #JOINT_POWER_OFF_MODE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int JOINT_POWER_OFF_MODE_VALUE = 239;

	/**
	 * The '<em><b>JOINT NOT RESPONDING MODE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Rotary motion is a rotation along a circular path with no limits. Motion movement is going endless in the same direction. Control unit is mainly degree.
	 * <!-- end-model-doc -->
	 * @see #JOINT_NOT_RESPONDING_MODE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int JOINT_NOT_RESPONDING_MODE_VALUE = 245;

	/**
	 * The '<em><b>JOINT MOTOR INITIALISATION MODE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Linear motion is a one dimensional motion along a straight line with defined limits. Motion movement is not going always in the same direction. Control unit is mainly mm.
	 * <!-- end-model-doc -->
	 * @see #JOINT_MOTOR_INITIALISATION_MODE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int JOINT_MOTOR_INITIALISATION_MODE_VALUE = 246;

	/**
	 * The '<em><b>JOINT BOOTING MODE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Linear motion is a one dimensional motion along a straight line with no limits. Motion movement is going endless in the same direction. Control unit is mainly mm.
	 * <!-- end-model-doc -->
	 * @see #JOINT_BOOTING_MODE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int JOINT_BOOTING_MODE_VALUE = 247;

	/**
	 * The '<em><b>JOINT PART DCALIBRATION ERROR MODE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Any motion-profile which is not defined by the AxisMotionProfileEnumeration
	 * <!-- end-model-doc -->
	 * @see #JOINT_PART_DCALIBRATION_ERROR_MODE
	 * @model name="JOINT_PART_D_CALIBRATION_ERROR_MODE"
	 * @generated
	 * @ordered
	 */
	public static final int JOINT_PART_DCALIBRATION_ERROR_MODE_VALUE = 248;

	/**
	 * The '<em><b>JOINT BOOTLOADER MODE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Rotary motion is a rotation along a circular path with defined limits. Motion movement is not going always in the same direction. Control unit is mainly degree.
	 * <!-- end-model-doc -->
	 * @see #JOINT_BOOTLOADER_MODE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int JOINT_BOOTLOADER_MODE_VALUE = 249;

	/**
	 * The '<em><b>JOINT CALIBRATION MODE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Rotary motion is a rotation along a circular path with no limits. Motion movement is going endless in the same direction. Control unit is mainly degree.
	 * <!-- end-model-doc -->
	 * @see #JOINT_CALIBRATION_MODE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int JOINT_CALIBRATION_MODE_VALUE = 250;

	/**
	 * The '<em><b>JOINT FAULT MODE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #JOINT_FAULT_MODE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int JOINT_FAULT_MODE_VALUE = 252;

	/**
	 * The '<em><b>JOINT RUNNING MODE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Any motion-profile which is not defined by the AxisMotionProfileEnumeration
	 * <!-- end-model-doc -->
	 * @see #JOINT_RUNNING_MODE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int JOINT_RUNNING_MODE_VALUE = 253;

	/**
	 * The '<em><b>JOINT IDLE MODE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Rotary motion is a rotation along a circular path with defined limits. Motion movement is not going always in the same direction. Control unit is mainly degree.
	 * <!-- end-model-doc -->
	 * @see #JOINT_IDLE_MODE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int JOINT_IDLE_MODE_VALUE = 255;

	/**
	 * An array of all the '<em><b>Axis State Enumeration</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final AxisStateEnumeration[] VALUES_ARRAY =
		new AxisStateEnumeration[] {
			JOINT_SHUTTING_DOWN_MODE,
			JOINT_PART_DCALIBRATION_MODE,
			_JOINT_BACKDRIVE_MODE,
			JOINT_POWER_OFF_MODE,
			JOINT_NOT_RESPONDING_MODE,
			JOINT_MOTOR_INITIALISATION_MODE,
			JOINT_BOOTING_MODE,
			JOINT_PART_DCALIBRATION_ERROR_MODE,
			JOINT_BOOTLOADER_MODE,
			JOINT_CALIBRATION_MODE,
			JOINT_FAULT_MODE,
			JOINT_RUNNING_MODE,
			JOINT_IDLE_MODE,
		};

	/**
	 * A public read-only list of all the '<em><b>Axis State Enumeration</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<AxisStateEnumeration> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Axis State Enumeration</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static AxisStateEnumeration get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			AxisStateEnumeration result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Axis State Enumeration</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static AxisStateEnumeration getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			AxisStateEnumeration result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Axis State Enumeration</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static AxisStateEnumeration get(int value) {
		switch (value) {
			case JOINT_SHUTTING_DOWN_MODE_VALUE: return JOINT_SHUTTING_DOWN_MODE;
			case JOINT_PART_DCALIBRATION_MODE_VALUE: return JOINT_PART_DCALIBRATION_MODE;
			case _JOINT_BACKDRIVE_MODE_VALUE: return _JOINT_BACKDRIVE_MODE;
			case JOINT_POWER_OFF_MODE_VALUE: return JOINT_POWER_OFF_MODE;
			case JOINT_NOT_RESPONDING_MODE_VALUE: return JOINT_NOT_RESPONDING_MODE;
			case JOINT_MOTOR_INITIALISATION_MODE_VALUE: return JOINT_MOTOR_INITIALISATION_MODE;
			case JOINT_BOOTING_MODE_VALUE: return JOINT_BOOTING_MODE;
			case JOINT_PART_DCALIBRATION_ERROR_MODE_VALUE: return JOINT_PART_DCALIBRATION_ERROR_MODE;
			case JOINT_BOOTLOADER_MODE_VALUE: return JOINT_BOOTLOADER_MODE;
			case JOINT_CALIBRATION_MODE_VALUE: return JOINT_CALIBRATION_MODE;
			case JOINT_FAULT_MODE_VALUE: return JOINT_FAULT_MODE;
			case JOINT_RUNNING_MODE_VALUE: return JOINT_RUNNING_MODE;
			case JOINT_IDLE_MODE_VALUE: return JOINT_IDLE_MODE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private AxisStateEnumeration(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //AxisStateEnumeration
