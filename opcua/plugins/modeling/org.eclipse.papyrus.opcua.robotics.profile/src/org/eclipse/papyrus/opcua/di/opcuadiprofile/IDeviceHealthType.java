/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.di.opcuadiprofile;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.OPC_UA_DI_Library.DeviceHealthEnumeration;

import org.eclipse.papyrus.opcua.opcuaprofile.BaseInterfaceType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IDevice Health Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IDeviceHealthType#getDeviceHealth <em>Device Health</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getIDeviceHealthType()
 * @model abstract="true"
 * @generated
 */
public interface IDeviceHealthType extends BaseInterfaceType {
	/**
	 * Returns the value of the '<em><b>Device Health</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.opcua.di.opcuadiprofile.OPC_UA_DI_Library.DeviceHealthEnumeration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Device Health</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Device Health</em>' attribute.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPC_UA_DI_Library.DeviceHealthEnumeration
	 * @see #setDeviceHealth(DeviceHealthEnumeration)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getIDeviceHealthType_DeviceHealth()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	DeviceHealthEnumeration getDeviceHealth();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IDeviceHealthType#getDeviceHealth <em>Device Health</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Device Health</em>' attribute.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPC_UA_DI_Library.DeviceHealthEnumeration
	 * @see #getDeviceHealth()
	 * @generated
	 */
	void setDeviceHealth(DeviceHealthEnumeration value);

} // IDeviceHealthType
