/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.di.opcuadiprofile.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.ISupportInfoType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage;

import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.ByteString;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Image;

import org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseInterfaceTypeImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISupport Info Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ISupportInfoTypeImpl#getDeviceTypeImage <em>Device Type Image</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ISupportInfoTypeImpl#getDocumentation <em>Documentation</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ISupportInfoTypeImpl#getProtocolSupport <em>Protocol Support</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ISupportInfoTypeImpl#getImageSet <em>Image Set</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ISupportInfoTypeImpl extends BaseInterfaceTypeImpl implements ISupportInfoType {
	/**
	 * The cached value of the '{@link #getDeviceTypeImage() <em>Device Type Image</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeviceTypeImage()
	 * @generated
	 * @ordered
	 */
	protected EList<Image> deviceTypeImage;

	/**
	 * The cached value of the '{@link #getDocumentation() <em>Documentation</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDocumentation()
	 * @generated
	 * @ordered
	 */
	protected EList<ByteString> documentation;

	/**
	 * The cached value of the '{@link #getProtocolSupport() <em>Protocol Support</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProtocolSupport()
	 * @generated
	 * @ordered
	 */
	protected EList<ByteString> protocolSupport;

	/**
	 * The cached value of the '{@link #getImageSet() <em>Image Set</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImageSet()
	 * @generated
	 * @ordered
	 */
	protected EList<Image> imageSet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISupportInfoTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUADIProfilePackage.Literals.ISUPPORT_INFO_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Image> getDeviceTypeImage() {
		if (deviceTypeImage == null) {
			deviceTypeImage = new EObjectContainmentEList<Image>(Image.class, this, OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__DEVICE_TYPE_IMAGE);
		}
		return deviceTypeImage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ByteString> getDocumentation() {
		if (documentation == null) {
			documentation = new EObjectContainmentEList<ByteString>(ByteString.class, this, OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__DOCUMENTATION);
		}
		return documentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ByteString> getProtocolSupport() {
		if (protocolSupport == null) {
			protocolSupport = new EObjectContainmentEList<ByteString>(ByteString.class, this, OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__PROTOCOL_SUPPORT);
		}
		return protocolSupport;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Image> getImageSet() {
		if (imageSet == null) {
			imageSet = new EObjectContainmentEList<Image>(Image.class, this, OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__IMAGE_SET);
		}
		return imageSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__DEVICE_TYPE_IMAGE:
				return ((InternalEList<?>)getDeviceTypeImage()).basicRemove(otherEnd, msgs);
			case OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__DOCUMENTATION:
				return ((InternalEList<?>)getDocumentation()).basicRemove(otherEnd, msgs);
			case OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__PROTOCOL_SUPPORT:
				return ((InternalEList<?>)getProtocolSupport()).basicRemove(otherEnd, msgs);
			case OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__IMAGE_SET:
				return ((InternalEList<?>)getImageSet()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__DEVICE_TYPE_IMAGE:
				return getDeviceTypeImage();
			case OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__DOCUMENTATION:
				return getDocumentation();
			case OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__PROTOCOL_SUPPORT:
				return getProtocolSupport();
			case OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__IMAGE_SET:
				return getImageSet();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__DEVICE_TYPE_IMAGE:
				getDeviceTypeImage().clear();
				getDeviceTypeImage().addAll((Collection<? extends Image>)newValue);
				return;
			case OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__DOCUMENTATION:
				getDocumentation().clear();
				getDocumentation().addAll((Collection<? extends ByteString>)newValue);
				return;
			case OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__PROTOCOL_SUPPORT:
				getProtocolSupport().clear();
				getProtocolSupport().addAll((Collection<? extends ByteString>)newValue);
				return;
			case OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__IMAGE_SET:
				getImageSet().clear();
				getImageSet().addAll((Collection<? extends Image>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__DEVICE_TYPE_IMAGE:
				getDeviceTypeImage().clear();
				return;
			case OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__DOCUMENTATION:
				getDocumentation().clear();
				return;
			case OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__PROTOCOL_SUPPORT:
				getProtocolSupport().clear();
				return;
			case OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__IMAGE_SET:
				getImageSet().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__DEVICE_TYPE_IMAGE:
				return deviceTypeImage != null && !deviceTypeImage.isEmpty();
			case OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__DOCUMENTATION:
				return documentation != null && !documentation.isEmpty();
			case OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__PROTOCOL_SUPPORT:
				return protocolSupport != null && !protocolSupport.isEmpty();
			case OPCUADIProfilePackage.ISUPPORT_INFO_TYPE__IMAGE_SET:
				return imageSet != null && !imageSet.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ISupportInfoTypeImpl
