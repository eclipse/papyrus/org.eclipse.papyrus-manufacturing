/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.ComponentType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task Control Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.TaskControlType#getParameterSetTaskContolType <em>Parameter Set Task Contol Type</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getTaskControlType()
 * @model
 * @generated
 */
public interface TaskControlType extends ComponentType {
	/**
	 * Returns the value of the '<em><b>Parameter Set Task Contol Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Set Task Contol Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Set Task Contol Type</em>' containment reference.
	 * @see #setParameterSetTaskContolType(ParameterSetTaskControlType)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getTaskControlType_ParameterSetTaskContolType()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	ParameterSetTaskControlType getParameterSetTaskContolType();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.TaskControlType#getParameterSetTaskContolType <em>Parameter Set Task Contol Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Set Task Contol Type</em>' containment reference.
	 * @see #getParameterSetTaskContolType()
	 * @generated
	 */
	void setParameterSetTaskContolType(ParameterSetTaskControlType value);

} // TaskControlType
