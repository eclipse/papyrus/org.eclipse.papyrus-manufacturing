/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.di.opcuadiprofile;

import org.eclipse.papyrus.opcua.opcuaprofile.BaseDataVariableType;
import org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter Set Topology Element Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ParameterSetTopologyElementType#getParameterIdentifier <em>Parameter Identifier</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getParameterSetTopologyElementType()
 * @model
 * @generated
 */
public interface ParameterSetTopologyElementType extends BaseObjectType {
	/**
	 * Returns the value of the '<em><b>Parameter Identifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Identifier</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Identifier</em>' reference.
	 * @see #setParameterIdentifier(BaseDataVariableType)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getParameterSetTopologyElementType_ParameterIdentifier()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	BaseDataVariableType getParameterIdentifier();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ParameterSetTopologyElementType#getParameterIdentifier <em>Parameter Identifier</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Identifier</em>' reference.
	 * @see #getParameterIdentifier()
	 * @generated
	 */
	void setParameterIdentifier(BaseDataVariableType value);

} // ParameterSetTopologyElementType
