/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>3D Cartesian Coordinates</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.OPC_UA_Robotics_CS_LibraryPackage#get_3DCartesianCoordinates()
 * @model annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='3DCartesianCoordinates'"
 * @generated
 */
public interface _3DCartesianCoordinates extends EObject {
} // _3DCartesianCoordinates
