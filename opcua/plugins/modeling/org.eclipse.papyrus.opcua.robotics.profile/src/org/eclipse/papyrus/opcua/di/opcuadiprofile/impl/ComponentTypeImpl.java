/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.di.opcuadiprofile.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.ComponentType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.FunctionalGroupType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.ITagNameplateType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.MethodSetTopologyElementType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.ParameterSetTopologyElementType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType;

import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.LocalizedText;

import org.eclipse.uml2.uml.Property;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ComponentTypeImpl#getGroupIdentifier <em>Group Identifier</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ComponentTypeImpl#getIdentification <em>Identification</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ComponentTypeImpl#getLock <em>Lock</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ComponentTypeImpl#getParameterSet <em>Parameter Set</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ComponentTypeImpl#getMethodSet <em>Method Set</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ComponentTypeImpl#getAssetId <em>Asset Id</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ComponentTypeImpl#getComponentName <em>Component Name</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ComponentTypeImpl#getBase_Property <em>Base Property</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ComponentTypeImpl extends IVendorNameplateTypeImpl implements ComponentType {
	/**
	 * The cached value of the '{@link #getGroupIdentifier() <em>Group Identifier</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroupIdentifier()
	 * @generated
	 * @ordered
	 */
	protected FunctionalGroupType groupIdentifier;

	/**
	 * The cached value of the '{@link #getIdentification() <em>Identification</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdentification()
	 * @generated
	 * @ordered
	 */
	protected FunctionalGroupType identification;

	/**
	 * The cached value of the '{@link #getLock() <em>Lock</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLock()
	 * @generated
	 * @ordered
	 */
	protected LockingServicesType lock;

	/**
	 * The cached value of the '{@link #getParameterSet() <em>Parameter Set</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterSet()
	 * @generated
	 * @ordered
	 */
	protected ParameterSetTopologyElementType parameterSet;

	/**
	 * The cached value of the '{@link #getMethodSet() <em>Method Set</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMethodSet()
	 * @generated
	 * @ordered
	 */
	protected MethodSetTopologyElementType methodSet;

	/**
	 * The default value of the '{@link #getAssetId() <em>Asset Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssetId()
	 * @generated
	 * @ordered
	 */
	protected static final String ASSET_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAssetId() <em>Asset Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssetId()
	 * @generated
	 * @ordered
	 */
	protected String assetId = ASSET_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getComponentName() <em>Component Name</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentName()
	 * @generated
	 * @ordered
	 */
	protected LocalizedText componentName;

	/**
	 * The cached value of the '{@link #getBase_Property() <em>Base Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Property()
	 * @generated
	 * @ordered
	 */
	protected Property base_Property;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUADIProfilePackage.Literals.COMPONENT_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionalGroupType getGroupIdentifier() {
		if (groupIdentifier != null && groupIdentifier.eIsProxy()) {
			InternalEObject oldGroupIdentifier = (InternalEObject)groupIdentifier;
			groupIdentifier = (FunctionalGroupType)eResolveProxy(oldGroupIdentifier);
			if (groupIdentifier != oldGroupIdentifier) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OPCUADIProfilePackage.COMPONENT_TYPE__GROUP_IDENTIFIER, oldGroupIdentifier, groupIdentifier));
			}
		}
		return groupIdentifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionalGroupType basicGetGroupIdentifier() {
		return groupIdentifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGroupIdentifier(FunctionalGroupType newGroupIdentifier) {
		FunctionalGroupType oldGroupIdentifier = groupIdentifier;
		groupIdentifier = newGroupIdentifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.COMPONENT_TYPE__GROUP_IDENTIFIER, oldGroupIdentifier, groupIdentifier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionalGroupType getIdentification() {
		if (identification != null && identification.eIsProxy()) {
			InternalEObject oldIdentification = (InternalEObject)identification;
			identification = (FunctionalGroupType)eResolveProxy(oldIdentification);
			if (identification != oldIdentification) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OPCUADIProfilePackage.COMPONENT_TYPE__IDENTIFICATION, oldIdentification, identification));
			}
		}
		return identification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionalGroupType basicGetIdentification() {
		return identification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdentification(FunctionalGroupType newIdentification) {
		FunctionalGroupType oldIdentification = identification;
		identification = newIdentification;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.COMPONENT_TYPE__IDENTIFICATION, oldIdentification, identification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LockingServicesType getLock() {
		if (lock != null && lock.eIsProxy()) {
			InternalEObject oldLock = (InternalEObject)lock;
			lock = (LockingServicesType)eResolveProxy(oldLock);
			if (lock != oldLock) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OPCUADIProfilePackage.COMPONENT_TYPE__LOCK, oldLock, lock));
			}
		}
		return lock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LockingServicesType basicGetLock() {
		return lock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLock(LockingServicesType newLock) {
		LockingServicesType oldLock = lock;
		lock = newLock;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.COMPONENT_TYPE__LOCK, oldLock, lock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterSetTopologyElementType getParameterSet() {
		return parameterSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParameterSet(ParameterSetTopologyElementType newParameterSet, NotificationChain msgs) {
		ParameterSetTopologyElementType oldParameterSet = parameterSet;
		parameterSet = newParameterSet;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.COMPONENT_TYPE__PARAMETER_SET, oldParameterSet, newParameterSet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterSet(ParameterSetTopologyElementType newParameterSet) {
		if (newParameterSet != parameterSet) {
			NotificationChain msgs = null;
			if (parameterSet != null)
				msgs = ((InternalEObject)parameterSet).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.COMPONENT_TYPE__PARAMETER_SET, null, msgs);
			if (newParameterSet != null)
				msgs = ((InternalEObject)newParameterSet).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.COMPONENT_TYPE__PARAMETER_SET, null, msgs);
			msgs = basicSetParameterSet(newParameterSet, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.COMPONENT_TYPE__PARAMETER_SET, newParameterSet, newParameterSet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MethodSetTopologyElementType getMethodSet() {
		return methodSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMethodSet(MethodSetTopologyElementType newMethodSet, NotificationChain msgs) {
		MethodSetTopologyElementType oldMethodSet = methodSet;
		methodSet = newMethodSet;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.COMPONENT_TYPE__METHOD_SET, oldMethodSet, newMethodSet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMethodSet(MethodSetTopologyElementType newMethodSet) {
		if (newMethodSet != methodSet) {
			NotificationChain msgs = null;
			if (methodSet != null)
				msgs = ((InternalEObject)methodSet).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.COMPONENT_TYPE__METHOD_SET, null, msgs);
			if (newMethodSet != null)
				msgs = ((InternalEObject)newMethodSet).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.COMPONENT_TYPE__METHOD_SET, null, msgs);
			msgs = basicSetMethodSet(newMethodSet, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.COMPONENT_TYPE__METHOD_SET, newMethodSet, newMethodSet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAssetId() {
		return assetId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssetId(String newAssetId) {
		String oldAssetId = assetId;
		assetId = newAssetId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.COMPONENT_TYPE__ASSET_ID, oldAssetId, assetId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocalizedText getComponentName() {
		return componentName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComponentName(LocalizedText newComponentName, NotificationChain msgs) {
		LocalizedText oldComponentName = componentName;
		componentName = newComponentName;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.COMPONENT_TYPE__COMPONENT_NAME, oldComponentName, newComponentName);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponentName(LocalizedText newComponentName) {
		if (newComponentName != componentName) {
			NotificationChain msgs = null;
			if (componentName != null)
				msgs = ((InternalEObject)componentName).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.COMPONENT_TYPE__COMPONENT_NAME, null, msgs);
			if (newComponentName != null)
				msgs = ((InternalEObject)newComponentName).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.COMPONENT_TYPE__COMPONENT_NAME, null, msgs);
			msgs = basicSetComponentName(newComponentName, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.COMPONENT_TYPE__COMPONENT_NAME, newComponentName, newComponentName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property getBase_Property() {
		if (base_Property != null && base_Property.eIsProxy()) {
			InternalEObject oldBase_Property = (InternalEObject)base_Property;
			base_Property = (Property)eResolveProxy(oldBase_Property);
			if (base_Property != oldBase_Property) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OPCUADIProfilePackage.COMPONENT_TYPE__BASE_PROPERTY, oldBase_Property, base_Property));
			}
		}
		return base_Property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property basicGetBase_Property() {
		return base_Property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBase_Property(Property newBase_Property) {
		Property oldBase_Property = base_Property;
		base_Property = newBase_Property;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.COMPONENT_TYPE__BASE_PROPERTY, oldBase_Property, base_Property));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OPCUADIProfilePackage.COMPONENT_TYPE__PARAMETER_SET:
				return basicSetParameterSet(null, msgs);
			case OPCUADIProfilePackage.COMPONENT_TYPE__METHOD_SET:
				return basicSetMethodSet(null, msgs);
			case OPCUADIProfilePackage.COMPONENT_TYPE__COMPONENT_NAME:
				return basicSetComponentName(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPCUADIProfilePackage.COMPONENT_TYPE__GROUP_IDENTIFIER:
				if (resolve) return getGroupIdentifier();
				return basicGetGroupIdentifier();
			case OPCUADIProfilePackage.COMPONENT_TYPE__IDENTIFICATION:
				if (resolve) return getIdentification();
				return basicGetIdentification();
			case OPCUADIProfilePackage.COMPONENT_TYPE__LOCK:
				if (resolve) return getLock();
				return basicGetLock();
			case OPCUADIProfilePackage.COMPONENT_TYPE__PARAMETER_SET:
				return getParameterSet();
			case OPCUADIProfilePackage.COMPONENT_TYPE__METHOD_SET:
				return getMethodSet();
			case OPCUADIProfilePackage.COMPONENT_TYPE__ASSET_ID:
				return getAssetId();
			case OPCUADIProfilePackage.COMPONENT_TYPE__COMPONENT_NAME:
				return getComponentName();
			case OPCUADIProfilePackage.COMPONENT_TYPE__BASE_PROPERTY:
				if (resolve) return getBase_Property();
				return basicGetBase_Property();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPCUADIProfilePackage.COMPONENT_TYPE__GROUP_IDENTIFIER:
				setGroupIdentifier((FunctionalGroupType)newValue);
				return;
			case OPCUADIProfilePackage.COMPONENT_TYPE__IDENTIFICATION:
				setIdentification((FunctionalGroupType)newValue);
				return;
			case OPCUADIProfilePackage.COMPONENT_TYPE__LOCK:
				setLock((LockingServicesType)newValue);
				return;
			case OPCUADIProfilePackage.COMPONENT_TYPE__PARAMETER_SET:
				setParameterSet((ParameterSetTopologyElementType)newValue);
				return;
			case OPCUADIProfilePackage.COMPONENT_TYPE__METHOD_SET:
				setMethodSet((MethodSetTopologyElementType)newValue);
				return;
			case OPCUADIProfilePackage.COMPONENT_TYPE__ASSET_ID:
				setAssetId((String)newValue);
				return;
			case OPCUADIProfilePackage.COMPONENT_TYPE__COMPONENT_NAME:
				setComponentName((LocalizedText)newValue);
				return;
			case OPCUADIProfilePackage.COMPONENT_TYPE__BASE_PROPERTY:
				setBase_Property((Property)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPCUADIProfilePackage.COMPONENT_TYPE__GROUP_IDENTIFIER:
				setGroupIdentifier((FunctionalGroupType)null);
				return;
			case OPCUADIProfilePackage.COMPONENT_TYPE__IDENTIFICATION:
				setIdentification((FunctionalGroupType)null);
				return;
			case OPCUADIProfilePackage.COMPONENT_TYPE__LOCK:
				setLock((LockingServicesType)null);
				return;
			case OPCUADIProfilePackage.COMPONENT_TYPE__PARAMETER_SET:
				setParameterSet((ParameterSetTopologyElementType)null);
				return;
			case OPCUADIProfilePackage.COMPONENT_TYPE__METHOD_SET:
				setMethodSet((MethodSetTopologyElementType)null);
				return;
			case OPCUADIProfilePackage.COMPONENT_TYPE__ASSET_ID:
				setAssetId(ASSET_ID_EDEFAULT);
				return;
			case OPCUADIProfilePackage.COMPONENT_TYPE__COMPONENT_NAME:
				setComponentName((LocalizedText)null);
				return;
			case OPCUADIProfilePackage.COMPONENT_TYPE__BASE_PROPERTY:
				setBase_Property((Property)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPCUADIProfilePackage.COMPONENT_TYPE__GROUP_IDENTIFIER:
				return groupIdentifier != null;
			case OPCUADIProfilePackage.COMPONENT_TYPE__IDENTIFICATION:
				return identification != null;
			case OPCUADIProfilePackage.COMPONENT_TYPE__LOCK:
				return lock != null;
			case OPCUADIProfilePackage.COMPONENT_TYPE__PARAMETER_SET:
				return parameterSet != null;
			case OPCUADIProfilePackage.COMPONENT_TYPE__METHOD_SET:
				return methodSet != null;
			case OPCUADIProfilePackage.COMPONENT_TYPE__ASSET_ID:
				return ASSET_ID_EDEFAULT == null ? assetId != null : !ASSET_ID_EDEFAULT.equals(assetId);
			case OPCUADIProfilePackage.COMPONENT_TYPE__COMPONENT_NAME:
				return componentName != null;
			case OPCUADIProfilePackage.COMPONENT_TYPE__BASE_PROPERTY:
				return base_Property != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == TopologyElementType.class) {
			switch (derivedFeatureID) {
				case OPCUADIProfilePackage.COMPONENT_TYPE__GROUP_IDENTIFIER: return OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__GROUP_IDENTIFIER;
				case OPCUADIProfilePackage.COMPONENT_TYPE__IDENTIFICATION: return OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__IDENTIFICATION;
				case OPCUADIProfilePackage.COMPONENT_TYPE__LOCK: return OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__LOCK;
				case OPCUADIProfilePackage.COMPONENT_TYPE__PARAMETER_SET: return OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__PARAMETER_SET;
				case OPCUADIProfilePackage.COMPONENT_TYPE__METHOD_SET: return OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__METHOD_SET;
				default: return -1;
			}
		}
		if (baseClass == ITagNameplateType.class) {
			switch (derivedFeatureID) {
				case OPCUADIProfilePackage.COMPONENT_TYPE__ASSET_ID: return OPCUADIProfilePackage.ITAG_NAMEPLATE_TYPE__ASSET_ID;
				case OPCUADIProfilePackage.COMPONENT_TYPE__COMPONENT_NAME: return OPCUADIProfilePackage.ITAG_NAMEPLATE_TYPE__COMPONENT_NAME;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == TopologyElementType.class) {
			switch (baseFeatureID) {
				case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__GROUP_IDENTIFIER: return OPCUADIProfilePackage.COMPONENT_TYPE__GROUP_IDENTIFIER;
				case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__IDENTIFICATION: return OPCUADIProfilePackage.COMPONENT_TYPE__IDENTIFICATION;
				case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__LOCK: return OPCUADIProfilePackage.COMPONENT_TYPE__LOCK;
				case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__PARAMETER_SET: return OPCUADIProfilePackage.COMPONENT_TYPE__PARAMETER_SET;
				case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE__METHOD_SET: return OPCUADIProfilePackage.COMPONENT_TYPE__METHOD_SET;
				default: return -1;
			}
		}
		if (baseClass == ITagNameplateType.class) {
			switch (baseFeatureID) {
				case OPCUADIProfilePackage.ITAG_NAMEPLATE_TYPE__ASSET_ID: return OPCUADIProfilePackage.COMPONENT_TYPE__ASSET_ID;
				case OPCUADIProfilePackage.ITAG_NAMEPLATE_TYPE__COMPONENT_NAME: return OPCUADIProfilePackage.COMPONENT_TYPE__COMPONENT_NAME;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (AssetId: ");
		result.append(assetId);
		result.append(')');
		return result.toString();
	}

} //ComponentTypeImpl
