/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>3D Frame Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * X, Y, Z define the position of the center of gravity relative to the mounting point coordinate system. 
 * A, B, C define the orientation of the principal axes of inertia relative to the mounting point coordinate system.
 * Orientation A, B, C can be "0" for systems which do not need these values.
 * 3DFrameType and 3DFrame are defined in OPC 10001-11 (SpatialTypes).
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library._3DFrameType#getCartesianCoordinates <em>Cartesian Coordinates</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library._3DFrameType#getOrientation <em>Orientation</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.OPC_UA_Robotics_CS_LibraryPackage#get_3DFrameType()
 * @model annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='3DFrameType'"
 * @generated
 */
public interface _3DFrameType extends EObject {
	/**
	 * Returns the value of the '<em><b>Cartesian Coordinates</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cartesian Coordinates</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cartesian Coordinates</em>' containment reference.
	 * @see #setCartesianCoordinates(_3DCartesianCoordinates)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.OPC_UA_Robotics_CS_LibraryPackage#get_3DFrameType_CartesianCoordinates()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	_3DCartesianCoordinates getCartesianCoordinates();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library._3DFrameType#getCartesianCoordinates <em>Cartesian Coordinates</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cartesian Coordinates</em>' containment reference.
	 * @see #getCartesianCoordinates()
	 * @generated
	 */
	void setCartesianCoordinates(_3DCartesianCoordinates value);

	/**
	 * Returns the value of the '<em><b>Orientation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Orientation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Orientation</em>' containment reference.
	 * @see #setOrientation(_3DOrientation)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.OPC_UA_Robotics_CS_LibraryPackage#get_3DFrameType_Orientation()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	_3DOrientation getOrientation();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library._3DFrameType#getOrientation <em>Orientation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Orientation</em>' containment reference.
	 * @see #getOrientation()
	 * @generated
	 */
	void setOrientation(_3DOrientation value);

} // _3DFrameType
