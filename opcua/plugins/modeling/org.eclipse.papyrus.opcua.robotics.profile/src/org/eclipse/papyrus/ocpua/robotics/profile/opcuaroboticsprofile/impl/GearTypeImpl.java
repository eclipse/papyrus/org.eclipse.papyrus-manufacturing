/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.GearType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotorType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.RationalNumber;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ComponentTypeImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Gear Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.GearTypeImpl#getGearRatio <em>Gear Ratio</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.GearTypeImpl#getPitch <em>Pitch</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.GearTypeImpl#getIsConnectedTo <em>Is Connected To</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GearTypeImpl extends ComponentTypeImpl implements GearType {
	/**
	 * The cached value of the '{@link #getGearRatio() <em>Gear Ratio</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGearRatio()
	 * @generated
	 * @ordered
	 */
	protected RationalNumber gearRatio;

	/**
	 * The cached value of the '{@link #getPitch() <em>Pitch</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPitch()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.Double pitch;

	/**
	 * The cached value of the '{@link #getIsConnectedTo() <em>Is Connected To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsConnectedTo()
	 * @generated
	 * @ordered
	 */
	protected MotorType isConnectedTo;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GearTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUARoboticsProfilePackage.Literals.GEAR_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RationalNumber getGearRatio() {
		return gearRatio;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGearRatio(RationalNumber newGearRatio, NotificationChain msgs) {
		RationalNumber oldGearRatio = gearRatio;
		gearRatio = newGearRatio;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.GEAR_TYPE__GEAR_RATIO, oldGearRatio, newGearRatio);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGearRatio(RationalNumber newGearRatio) {
		if (newGearRatio != gearRatio) {
			NotificationChain msgs = null;
			if (gearRatio != null)
				msgs = ((InternalEObject)gearRatio).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.GEAR_TYPE__GEAR_RATIO, null, msgs);
			if (newGearRatio != null)
				msgs = ((InternalEObject)newGearRatio).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.GEAR_TYPE__GEAR_RATIO, null, msgs);
			msgs = basicSetGearRatio(newGearRatio, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.GEAR_TYPE__GEAR_RATIO, newGearRatio, newGearRatio));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.Double getPitch() {
		return pitch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPitch(org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.Double newPitch, NotificationChain msgs) {
		org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.Double oldPitch = pitch;
		pitch = newPitch;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.GEAR_TYPE__PITCH, oldPitch, newPitch);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPitch(org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.Double newPitch) {
		if (newPitch != pitch) {
			NotificationChain msgs = null;
			if (pitch != null)
				msgs = ((InternalEObject)pitch).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.GEAR_TYPE__PITCH, null, msgs);
			if (newPitch != null)
				msgs = ((InternalEObject)newPitch).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.GEAR_TYPE__PITCH, null, msgs);
			msgs = basicSetPitch(newPitch, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.GEAR_TYPE__PITCH, newPitch, newPitch));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MotorType getIsConnectedTo() {
		if (isConnectedTo != null && isConnectedTo.eIsProxy()) {
			InternalEObject oldIsConnectedTo = (InternalEObject)isConnectedTo;
			isConnectedTo = (MotorType)eResolveProxy(oldIsConnectedTo);
			if (isConnectedTo != oldIsConnectedTo) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OPCUARoboticsProfilePackage.GEAR_TYPE__IS_CONNECTED_TO, oldIsConnectedTo, isConnectedTo));
			}
		}
		return isConnectedTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MotorType basicGetIsConnectedTo() {
		return isConnectedTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsConnectedTo(MotorType newIsConnectedTo) {
		MotorType oldIsConnectedTo = isConnectedTo;
		isConnectedTo = newIsConnectedTo;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.GEAR_TYPE__IS_CONNECTED_TO, oldIsConnectedTo, isConnectedTo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.GEAR_TYPE__GEAR_RATIO:
				return basicSetGearRatio(null, msgs);
			case OPCUARoboticsProfilePackage.GEAR_TYPE__PITCH:
				return basicSetPitch(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.GEAR_TYPE__GEAR_RATIO:
				return getGearRatio();
			case OPCUARoboticsProfilePackage.GEAR_TYPE__PITCH:
				return getPitch();
			case OPCUARoboticsProfilePackage.GEAR_TYPE__IS_CONNECTED_TO:
				if (resolve) return getIsConnectedTo();
				return basicGetIsConnectedTo();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.GEAR_TYPE__GEAR_RATIO:
				setGearRatio((RationalNumber)newValue);
				return;
			case OPCUARoboticsProfilePackage.GEAR_TYPE__PITCH:
				setPitch((org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.Double)newValue);
				return;
			case OPCUARoboticsProfilePackage.GEAR_TYPE__IS_CONNECTED_TO:
				setIsConnectedTo((MotorType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.GEAR_TYPE__GEAR_RATIO:
				setGearRatio((RationalNumber)null);
				return;
			case OPCUARoboticsProfilePackage.GEAR_TYPE__PITCH:
				setPitch((org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.Double)null);
				return;
			case OPCUARoboticsProfilePackage.GEAR_TYPE__IS_CONNECTED_TO:
				setIsConnectedTo((MotorType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.GEAR_TYPE__GEAR_RATIO:
				return gearRatio != null;
			case OPCUARoboticsProfilePackage.GEAR_TYPE__PITCH:
				return pitch != null;
			case OPCUARoboticsProfilePackage.GEAR_TYPE__IS_CONNECTED_TO:
				return isConnectedTo != null;
		}
		return super.eIsSet(featureID);
	}

} //GearTypeImpl
