/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter Set Controller Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * � Variable :The TotalPowerOnTime variable provides the total accumulated time the controller was powered on.
 * � Variable StartUpTime: The StartUpTime variable provides the date and time of the last start-up of the controller.
 * � Variable UpsState: The UpsState variable provides the vendor specific status of an integrated uninterruptible power supply or accumulator system.
 * � Variable TotalEnergyConsumption: The TotalEnergyConsumption variable provides total accumulated energy consumed by the motion devices related with this controller instance.
 * � Variable CabinetFanSpeed: The CabinetFanSpeed variable provides the speed of the cabinet fan.
 * � Variable CPUFanSpeed: The CPUFanSpeed variable provides the speed of the CPU fan.
 * � Variable InputVoltage: The InputVoltage variable provides the input voltage of the controller which can be a configured value. To distinguish between an AC or DC supply the optional property Definition of the base type DataItemType shall be used.
 * � Variable Temperature: The Temperature variable provides the controller temperature given by a temperature sensor inside of the controller.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getTotalPowerOnTime <em>Total Power On Time</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getStartUpTime <em>Start Up Time</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getUpsState <em>Ups State</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getTotalEnergyConsumption <em>Total Energy Consumption</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getCabinetFanSpeed <em>Cabinet Fan Speed</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getCPUFanSpeed <em>CPU Fan Speed</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getInputVoltage <em>Input Voltage</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getTemperature <em>Temperature</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetControllerType()
 * @model
 * @generated
 */
public interface ParameterSetControllerType extends EObject {
	/**
	 * Returns the value of the '<em><b>Total Power On Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Total Power On Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Total Power On Time</em>' attribute.
	 * @see #setTotalPowerOnTime(String)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetControllerType_TotalPowerOnTime()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getTotalPowerOnTime();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getTotalPowerOnTime <em>Total Power On Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Total Power On Time</em>' attribute.
	 * @see #getTotalPowerOnTime()
	 * @generated
	 */
	void setTotalPowerOnTime(String value);

	/**
	 * Returns the value of the '<em><b>Start Up Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Up Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Up Time</em>' attribute.
	 * @see #setStartUpTime(String)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetControllerType_StartUpTime()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getStartUpTime();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getStartUpTime <em>Start Up Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Up Time</em>' attribute.
	 * @see #getStartUpTime()
	 * @generated
	 */
	void setStartUpTime(String value);

	/**
	 * Returns the value of the '<em><b>Ups State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ups State</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ups State</em>' attribute.
	 * @see #setUpsState(String)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetControllerType_UpsState()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getUpsState();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getUpsState <em>Ups State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ups State</em>' attribute.
	 * @see #getUpsState()
	 * @generated
	 */
	void setUpsState(String value);

	/**
	 * Returns the value of the '<em><b>Total Energy Consumption</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Total Energy Consumption</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Total Energy Consumption</em>' attribute.
	 * @see #setTotalEnergyConsumption(double)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetControllerType_TotalEnergyConsumption()
	 * @model ordered="false"
	 * @generated
	 */
	double getTotalEnergyConsumption();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getTotalEnergyConsumption <em>Total Energy Consumption</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Total Energy Consumption</em>' attribute.
	 * @see #getTotalEnergyConsumption()
	 * @generated
	 */
	void setTotalEnergyConsumption(double value);

	/**
	 * Returns the value of the '<em><b>Cabinet Fan Speed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cabinet Fan Speed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cabinet Fan Speed</em>' attribute.
	 * @see #setCabinetFanSpeed(double)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetControllerType_CabinetFanSpeed()
	 * @model ordered="false"
	 * @generated
	 */
	double getCabinetFanSpeed();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getCabinetFanSpeed <em>Cabinet Fan Speed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cabinet Fan Speed</em>' attribute.
	 * @see #getCabinetFanSpeed()
	 * @generated
	 */
	void setCabinetFanSpeed(double value);

	/**
	 * Returns the value of the '<em><b>CPU Fan Speed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>CPU Fan Speed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CPU Fan Speed</em>' attribute.
	 * @see #setCPUFanSpeed(double)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetControllerType_CPUFanSpeed()
	 * @model ordered="false"
	 * @generated
	 */
	double getCPUFanSpeed();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getCPUFanSpeed <em>CPU Fan Speed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CPU Fan Speed</em>' attribute.
	 * @see #getCPUFanSpeed()
	 * @generated
	 */
	void setCPUFanSpeed(double value);

	/**
	 * Returns the value of the '<em><b>Input Voltage</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Voltage</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Voltage</em>' attribute.
	 * @see #setInputVoltage(double)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetControllerType_InputVoltage()
	 * @model ordered="false"
	 * @generated
	 */
	double getInputVoltage();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getInputVoltage <em>Input Voltage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input Voltage</em>' attribute.
	 * @see #getInputVoltage()
	 * @generated
	 */
	void setInputVoltage(double value);

	/**
	 * Returns the value of the '<em><b>Temperature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Temperature</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Temperature</em>' attribute.
	 * @see #setTemperature(double)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetControllerType_Temperature()
	 * @model ordered="false"
	 * @generated
	 */
	double getTemperature();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getTemperature <em>Temperature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Temperature</em>' attribute.
	 * @see #getTemperature()
	 * @generated
	 */
	void setTemperature(double value);

} // ParameterSetControllerType
