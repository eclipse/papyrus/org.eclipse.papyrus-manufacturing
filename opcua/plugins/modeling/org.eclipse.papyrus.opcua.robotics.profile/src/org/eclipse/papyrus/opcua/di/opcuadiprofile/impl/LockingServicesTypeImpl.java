/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.di.opcuadiprofile.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage;

import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Duration;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int32;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.QualifiedName;

import org.eclipse.papyrus.opcua.opcuaprofile.impl.BaseObjectTypeImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Locking Services Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.LockingServicesTypeImpl#getDefaultInstanceBrowseName <em>Default Instance Browse Name</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.LockingServicesTypeImpl#isLocked <em>Locked</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.LockingServicesTypeImpl#getLockingClient <em>Locking Client</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.LockingServicesTypeImpl#getLockingUser <em>Locking User</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.LockingServicesTypeImpl#getRemainingLockTime <em>Remaining Lock Time</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LockingServicesTypeImpl extends BaseObjectTypeImpl implements LockingServicesType {
	/**
	 * The cached value of the '{@link #getDefaultInstanceBrowseName() <em>Default Instance Browse Name</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultInstanceBrowseName()
	 * @generated
	 * @ordered
	 */
	protected QualifiedName defaultInstanceBrowseName;

	/**
	 * The default value of the '{@link #isLocked() <em>Locked</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLocked()
	 * @generated
	 * @ordered
	 */
	protected static final boolean LOCKED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isLocked() <em>Locked</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLocked()
	 * @generated
	 * @ordered
	 */
	protected boolean locked = LOCKED_EDEFAULT;

	/**
	 * The default value of the '{@link #getLockingClient() <em>Locking Client</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLockingClient()
	 * @generated
	 * @ordered
	 */
	protected static final String LOCKING_CLIENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLockingClient() <em>Locking Client</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLockingClient()
	 * @generated
	 * @ordered
	 */
	protected String lockingClient = LOCKING_CLIENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getLockingUser() <em>Locking User</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLockingUser()
	 * @generated
	 * @ordered
	 */
	protected static final String LOCKING_USER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLockingUser() <em>Locking User</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLockingUser()
	 * @generated
	 * @ordered
	 */
	protected String lockingUser = LOCKING_USER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRemainingLockTime() <em>Remaining Lock Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRemainingLockTime()
	 * @generated
	 * @ordered
	 */
	protected Duration remainingLockTime;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LockingServicesTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUADIProfilePackage.Literals.LOCKING_SERVICES_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QualifiedName getDefaultInstanceBrowseName() {
		return defaultInstanceBrowseName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefaultInstanceBrowseName(QualifiedName newDefaultInstanceBrowseName, NotificationChain msgs) {
		QualifiedName oldDefaultInstanceBrowseName = defaultInstanceBrowseName;
		defaultInstanceBrowseName = newDefaultInstanceBrowseName;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__DEFAULT_INSTANCE_BROWSE_NAME, oldDefaultInstanceBrowseName, newDefaultInstanceBrowseName);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultInstanceBrowseName(QualifiedName newDefaultInstanceBrowseName) {
		if (newDefaultInstanceBrowseName != defaultInstanceBrowseName) {
			NotificationChain msgs = null;
			if (defaultInstanceBrowseName != null)
				msgs = ((InternalEObject)defaultInstanceBrowseName).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__DEFAULT_INSTANCE_BROWSE_NAME, null, msgs);
			if (newDefaultInstanceBrowseName != null)
				msgs = ((InternalEObject)newDefaultInstanceBrowseName).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__DEFAULT_INSTANCE_BROWSE_NAME, null, msgs);
			msgs = basicSetDefaultInstanceBrowseName(newDefaultInstanceBrowseName, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__DEFAULT_INSTANCE_BROWSE_NAME, newDefaultInstanceBrowseName, newDefaultInstanceBrowseName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isLocked() {
		return locked;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocked(boolean newLocked) {
		boolean oldLocked = locked;
		locked = newLocked;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__LOCKED, oldLocked, locked));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLockingClient() {
		return lockingClient;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLockingClient(String newLockingClient) {
		String oldLockingClient = lockingClient;
		lockingClient = newLockingClient;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__LOCKING_CLIENT, oldLockingClient, lockingClient));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLockingUser() {
		return lockingUser;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLockingUser(String newLockingUser) {
		String oldLockingUser = lockingUser;
		lockingUser = newLockingUser;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__LOCKING_USER, oldLockingUser, lockingUser));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Duration getRemainingLockTime() {
		return remainingLockTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRemainingLockTime(Duration newRemainingLockTime, NotificationChain msgs) {
		Duration oldRemainingLockTime = remainingLockTime;
		remainingLockTime = newRemainingLockTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__REMAINING_LOCK_TIME, oldRemainingLockTime, newRemainingLockTime);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRemainingLockTime(Duration newRemainingLockTime) {
		if (newRemainingLockTime != remainingLockTime) {
			NotificationChain msgs = null;
			if (remainingLockTime != null)
				msgs = ((InternalEObject)remainingLockTime).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__REMAINING_LOCK_TIME, null, msgs);
			if (newRemainingLockTime != null)
				msgs = ((InternalEObject)newRemainingLockTime).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__REMAINING_LOCK_TIME, null, msgs);
			msgs = basicSetRemainingLockTime(newRemainingLockTime, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__REMAINING_LOCK_TIME, newRemainingLockTime, newRemainingLockTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void InitLock(String Context, Int32 InitLockStatus) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void RenewLock(Int32 RenewLockStatus) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void ExitLock(Int32 ExitLockStatus) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void BreakLock(Int32 BreakLockStatus) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__DEFAULT_INSTANCE_BROWSE_NAME:
				return basicSetDefaultInstanceBrowseName(null, msgs);
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__REMAINING_LOCK_TIME:
				return basicSetRemainingLockTime(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__DEFAULT_INSTANCE_BROWSE_NAME:
				return getDefaultInstanceBrowseName();
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__LOCKED:
				return isLocked();
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__LOCKING_CLIENT:
				return getLockingClient();
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__LOCKING_USER:
				return getLockingUser();
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__REMAINING_LOCK_TIME:
				return getRemainingLockTime();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__DEFAULT_INSTANCE_BROWSE_NAME:
				setDefaultInstanceBrowseName((QualifiedName)newValue);
				return;
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__LOCKED:
				setLocked((Boolean)newValue);
				return;
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__LOCKING_CLIENT:
				setLockingClient((String)newValue);
				return;
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__LOCKING_USER:
				setLockingUser((String)newValue);
				return;
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__REMAINING_LOCK_TIME:
				setRemainingLockTime((Duration)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__DEFAULT_INSTANCE_BROWSE_NAME:
				setDefaultInstanceBrowseName((QualifiedName)null);
				return;
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__LOCKED:
				setLocked(LOCKED_EDEFAULT);
				return;
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__LOCKING_CLIENT:
				setLockingClient(LOCKING_CLIENT_EDEFAULT);
				return;
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__LOCKING_USER:
				setLockingUser(LOCKING_USER_EDEFAULT);
				return;
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__REMAINING_LOCK_TIME:
				setRemainingLockTime((Duration)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__DEFAULT_INSTANCE_BROWSE_NAME:
				return defaultInstanceBrowseName != null;
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__LOCKED:
				return locked != LOCKED_EDEFAULT;
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__LOCKING_CLIENT:
				return LOCKING_CLIENT_EDEFAULT == null ? lockingClient != null : !LOCKING_CLIENT_EDEFAULT.equals(lockingClient);
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__LOCKING_USER:
				return LOCKING_USER_EDEFAULT == null ? lockingUser != null : !LOCKING_USER_EDEFAULT.equals(lockingUser);
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE__REMAINING_LOCK_TIME:
				return remainingLockTime != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE___INIT_LOCK__STRING_INT32:
				InitLock((String)arguments.get(0), (Int32)arguments.get(1));
				return null;
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE___RENEW_LOCK__INT32:
				RenewLock((Int32)arguments.get(0));
				return null;
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE___EXIT_LOCK__INT32:
				ExitLock((Int32)arguments.get(0));
				return null;
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE___BREAK_LOCK__INT32:
				BreakLock((Int32)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (Locked: ");
		result.append(locked);
		result.append(", LockingClient: ");
		result.append(lockingClient);
		result.append(", LockingUser: ");
		result.append(lockingUser);
		result.append(')');
		return result.toString();
	}

} //LockingServicesTypeImpl
