/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisMotionProfileEnumeration;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.LoadType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetAxisType;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ComponentTypeImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Axis Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.AxisTypeImpl#getMotionProfile <em>Motion Profile</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.AxisTypeImpl#getAdditionalLoad <em>Additional Load</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.AxisTypeImpl#getParameterSetAxisType <em>Parameter Set Axis Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AxisTypeImpl extends ComponentTypeImpl implements AxisType {
	/**
	 * The default value of the '{@link #getMotionProfile() <em>Motion Profile</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMotionProfile()
	 * @generated
	 * @ordered
	 */
	protected static final AxisMotionProfileEnumeration MOTION_PROFILE_EDEFAULT = AxisMotionProfileEnumeration.OTHER;

	/**
	 * The cached value of the '{@link #getMotionProfile() <em>Motion Profile</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMotionProfile()
	 * @generated
	 * @ordered
	 */
	protected AxisMotionProfileEnumeration motionProfile = MOTION_PROFILE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAdditionalLoad() <em>Additional Load</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAdditionalLoad()
	 * @generated
	 * @ordered
	 */
	protected LoadType additionalLoad;

	/**
	 * The cached value of the '{@link #getParameterSetAxisType() <em>Parameter Set Axis Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterSetAxisType()
	 * @generated
	 * @ordered
	 */
	protected ParameterSetAxisType parameterSetAxisType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AxisTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUARoboticsProfilePackage.Literals.AXIS_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AxisMotionProfileEnumeration getMotionProfile() {
		return motionProfile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMotionProfile(AxisMotionProfileEnumeration newMotionProfile) {
		AxisMotionProfileEnumeration oldMotionProfile = motionProfile;
		motionProfile = newMotionProfile == null ? MOTION_PROFILE_EDEFAULT : newMotionProfile;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.AXIS_TYPE__MOTION_PROFILE, oldMotionProfile, motionProfile));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LoadType getAdditionalLoad() {
		return additionalLoad;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAdditionalLoad(LoadType newAdditionalLoad, NotificationChain msgs) {
		LoadType oldAdditionalLoad = additionalLoad;
		additionalLoad = newAdditionalLoad;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.AXIS_TYPE__ADDITIONAL_LOAD, oldAdditionalLoad, newAdditionalLoad);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAdditionalLoad(LoadType newAdditionalLoad) {
		if (newAdditionalLoad != additionalLoad) {
			NotificationChain msgs = null;
			if (additionalLoad != null)
				msgs = ((InternalEObject)additionalLoad).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.AXIS_TYPE__ADDITIONAL_LOAD, null, msgs);
			if (newAdditionalLoad != null)
				msgs = ((InternalEObject)newAdditionalLoad).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.AXIS_TYPE__ADDITIONAL_LOAD, null, msgs);
			msgs = basicSetAdditionalLoad(newAdditionalLoad, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.AXIS_TYPE__ADDITIONAL_LOAD, newAdditionalLoad, newAdditionalLoad));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterSetAxisType getParameterSetAxisType() {
		return parameterSetAxisType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParameterSetAxisType(ParameterSetAxisType newParameterSetAxisType, NotificationChain msgs) {
		ParameterSetAxisType oldParameterSetAxisType = parameterSetAxisType;
		parameterSetAxisType = newParameterSetAxisType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.AXIS_TYPE__PARAMETER_SET_AXIS_TYPE, oldParameterSetAxisType, newParameterSetAxisType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterSetAxisType(ParameterSetAxisType newParameterSetAxisType) {
		if (newParameterSetAxisType != parameterSetAxisType) {
			NotificationChain msgs = null;
			if (parameterSetAxisType != null)
				msgs = ((InternalEObject)parameterSetAxisType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.AXIS_TYPE__PARAMETER_SET_AXIS_TYPE, null, msgs);
			if (newParameterSetAxisType != null)
				msgs = ((InternalEObject)newParameterSetAxisType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.AXIS_TYPE__PARAMETER_SET_AXIS_TYPE, null, msgs);
			msgs = basicSetParameterSetAxisType(newParameterSetAxisType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.AXIS_TYPE__PARAMETER_SET_AXIS_TYPE, newParameterSetAxisType, newParameterSetAxisType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.AXIS_TYPE__ADDITIONAL_LOAD:
				return basicSetAdditionalLoad(null, msgs);
			case OPCUARoboticsProfilePackage.AXIS_TYPE__PARAMETER_SET_AXIS_TYPE:
				return basicSetParameterSetAxisType(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.AXIS_TYPE__MOTION_PROFILE:
				return getMotionProfile();
			case OPCUARoboticsProfilePackage.AXIS_TYPE__ADDITIONAL_LOAD:
				return getAdditionalLoad();
			case OPCUARoboticsProfilePackage.AXIS_TYPE__PARAMETER_SET_AXIS_TYPE:
				return getParameterSetAxisType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.AXIS_TYPE__MOTION_PROFILE:
				setMotionProfile((AxisMotionProfileEnumeration)newValue);
				return;
			case OPCUARoboticsProfilePackage.AXIS_TYPE__ADDITIONAL_LOAD:
				setAdditionalLoad((LoadType)newValue);
				return;
			case OPCUARoboticsProfilePackage.AXIS_TYPE__PARAMETER_SET_AXIS_TYPE:
				setParameterSetAxisType((ParameterSetAxisType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.AXIS_TYPE__MOTION_PROFILE:
				setMotionProfile(MOTION_PROFILE_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.AXIS_TYPE__ADDITIONAL_LOAD:
				setAdditionalLoad((LoadType)null);
				return;
			case OPCUARoboticsProfilePackage.AXIS_TYPE__PARAMETER_SET_AXIS_TYPE:
				setParameterSetAxisType((ParameterSetAxisType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.AXIS_TYPE__MOTION_PROFILE:
				return motionProfile != MOTION_PROFILE_EDEFAULT;
			case OPCUARoboticsProfilePackage.AXIS_TYPE__ADDITIONAL_LOAD:
				return additionalLoad != null;
			case OPCUARoboticsProfilePackage.AXIS_TYPE__PARAMETER_SET_AXIS_TYPE:
				return parameterSetAxisType != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (MotionProfile: ");
		result.append(motionProfile);
		result.append(')');
		return result.toString();
	}

} //AxisTypeImpl
