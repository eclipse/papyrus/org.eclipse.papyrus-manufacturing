/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.di.opcuadiprofile;

import org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType;

import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Duration;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int32;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.QualifiedName;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Locking Services Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#getDefaultInstanceBrowseName <em>Default Instance Browse Name</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#isLocked <em>Locked</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#getLockingClient <em>Locking Client</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#getLockingUser <em>Locking User</em>}</li>
 *   <li>{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#getRemainingLockTime <em>Remaining Lock Time</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getLockingServicesType()
 * @model
 * @generated
 */
public interface LockingServicesType extends BaseObjectType {
	/**
	 * Returns the value of the '<em><b>Default Instance Browse Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Instance Browse Name</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Instance Browse Name</em>' containment reference.
	 * @see #setDefaultInstanceBrowseName(QualifiedName)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getLockingServicesType_DefaultInstanceBrowseName()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	QualifiedName getDefaultInstanceBrowseName();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#getDefaultInstanceBrowseName <em>Default Instance Browse Name</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Instance Browse Name</em>' containment reference.
	 * @see #getDefaultInstanceBrowseName()
	 * @generated
	 */
	void setDefaultInstanceBrowseName(QualifiedName value);

	/**
	 * Returns the value of the '<em><b>Locked</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Locked</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Locked</em>' attribute.
	 * @see #setLocked(boolean)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getLockingServicesType_Locked()
	 * @model dataType="org.eclipse.uml2.types.Boolean" required="true" ordered="false"
	 * @generated
	 */
	boolean isLocked();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#isLocked <em>Locked</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Locked</em>' attribute.
	 * @see #isLocked()
	 * @generated
	 */
	void setLocked(boolean value);

	/**
	 * Returns the value of the '<em><b>Locking Client</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Locking Client</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Locking Client</em>' attribute.
	 * @see #setLockingClient(String)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getLockingServicesType_LockingClient()
	 * @model dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
	 * @generated
	 */
	String getLockingClient();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#getLockingClient <em>Locking Client</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Locking Client</em>' attribute.
	 * @see #getLockingClient()
	 * @generated
	 */
	void setLockingClient(String value);

	/**
	 * Returns the value of the '<em><b>Locking User</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Locking User</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Locking User</em>' attribute.
	 * @see #setLockingUser(String)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getLockingServicesType_LockingUser()
	 * @model dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
	 * @generated
	 */
	String getLockingUser();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#getLockingUser <em>Locking User</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Locking User</em>' attribute.
	 * @see #getLockingUser()
	 * @generated
	 */
	void setLockingUser(String value);

	/**
	 * Returns the value of the '<em><b>Remaining Lock Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remaining Lock Time</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remaining Lock Time</em>' containment reference.
	 * @see #setRemainingLockTime(Duration)
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage#getLockingServicesType_RemainingLockTime()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	Duration getRemainingLockTime();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.LockingServicesType#getRemainingLockTime <em>Remaining Lock Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Remaining Lock Time</em>' containment reference.
	 * @see #getRemainingLockTime()
	 * @generated
	 */
	void setRemainingLockTime(Duration value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ContextDataType="org.eclipse.uml2.types.String" ContextRequired="true" ContextOrdered="false" InitLockStatusRequired="true" InitLockStatusOrdered="false"
	 * @generated
	 */
	void InitLock(String Context, Int32 InitLockStatus);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model RenewLockStatusRequired="true" RenewLockStatusOrdered="false"
	 * @generated
	 */
	void RenewLock(Int32 RenewLockStatus);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ExitLockStatusRequired="true" ExitLockStatusOrdered="false"
	 * @generated
	 */
	void ExitLock(Int32 ExitLockStatus);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model BreakLockStatusRequired="true" BreakLockStatusOrdered="false"
	 * @generated
	 */
	void BreakLock(Int32 BreakLockStatus);

} // LockingServicesType
