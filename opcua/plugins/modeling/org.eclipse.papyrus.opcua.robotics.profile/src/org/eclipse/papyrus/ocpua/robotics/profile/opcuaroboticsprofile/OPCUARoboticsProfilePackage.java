/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfileFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='OPC_UA_Robotics_CS'"
 * @generated
 */
public interface OPCUARoboticsProfilePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "opcuaroboticsprofile";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://Papyrus/OPCUARoboticsProfile";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "OPCUARoboticsProfile";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OPCUARoboticsProfilePackage eINSTANCE = org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.LoadTypeImpl <em>Load Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.LoadTypeImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getLoadType()
	 * @generated
	 */
	int LOAD_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Mass</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOAD_TYPE__MASS = 0;

	/**
	 * The feature id for the '<em><b>Center Of Mass</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOAD_TYPE__CENTER_OF_MASS = 1;

	/**
	 * The feature id for the '<em><b>Inertia</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOAD_TYPE__INERTIA = 2;

	/**
	 * The number of structural features of the '<em>Load Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOAD_TYPE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Load Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOAD_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetMotorTypeImpl <em>Parameter Set Motor Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetMotorTypeImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getParameterSetMotorType()
	 * @generated
	 */
	int PARAMETER_SET_MOTOR_TYPE = 1;

	/**
	 * The feature id for the '<em><b>Brake Released</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_MOTOR_TYPE__BRAKE_RELEASED = 0;

	/**
	 * The feature id for the '<em><b>Effective Load Rate</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_MOTOR_TYPE__EFFECTIVE_LOAD_RATE = 1;

	/**
	 * The feature id for the '<em><b>Motor Temperature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_MOTOR_TYPE__MOTOR_TEMPERATURE = 2;

	/**
	 * The feature id for the '<em><b>Not CS Motor Intensity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_MOTOR_TYPE__NOT_CS_MOTOR_INTENSITY = 3;

	/**
	 * The number of structural features of the '<em>Parameter Set Motor Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_MOTOR_TYPE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Parameter Set Motor Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_MOTOR_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetControllerTypeImpl <em>Parameter Set Controller Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetControllerTypeImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getParameterSetControllerType()
	 * @generated
	 */
	int PARAMETER_SET_CONTROLLER_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Total Power On Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_CONTROLLER_TYPE__TOTAL_POWER_ON_TIME = 0;

	/**
	 * The feature id for the '<em><b>Start Up Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_CONTROLLER_TYPE__START_UP_TIME = 1;

	/**
	 * The feature id for the '<em><b>Ups State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_CONTROLLER_TYPE__UPS_STATE = 2;

	/**
	 * The feature id for the '<em><b>Total Energy Consumption</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_CONTROLLER_TYPE__TOTAL_ENERGY_CONSUMPTION = 3;

	/**
	 * The feature id for the '<em><b>Cabinet Fan Speed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_CONTROLLER_TYPE__CABINET_FAN_SPEED = 4;

	/**
	 * The feature id for the '<em><b>CPU Fan Speed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_CONTROLLER_TYPE__CPU_FAN_SPEED = 5;

	/**
	 * The feature id for the '<em><b>Input Voltage</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_CONTROLLER_TYPE__INPUT_VOLTAGE = 6;

	/**
	 * The feature id for the '<em><b>Temperature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_CONTROLLER_TYPE__TEMPERATURE = 7;

	/**
	 * The number of structural features of the '<em>Parameter Set Controller Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_CONTROLLER_TYPE_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Parameter Set Controller Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_CONTROLLER_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetTaskControlTypeImpl <em>Parameter Set Task Control Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetTaskControlTypeImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getParameterSetTaskControlType()
	 * @generated
	 */
	int PARAMETER_SET_TASK_CONTROL_TYPE = 3;

	/**
	 * The feature id for the '<em><b>Execution Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_TASK_CONTROL_TYPE__EXECUTION_MODE = 0;

	/**
	 * The feature id for the '<em><b>Task Program Loaded</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_TASK_CONTROL_TYPE__TASK_PROGRAM_LOADED = 1;

	/**
	 * The feature id for the '<em><b>Task Program Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_TASK_CONTROL_TYPE__TASK_PROGRAM_NAME = 2;

	/**
	 * The number of structural features of the '<em>Parameter Set Task Control Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_TASK_CONTROL_TYPE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Parameter Set Task Control Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_TASK_CONTROL_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetSafetyStateTypeImpl <em>Parameter Set Safety State Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetSafetyStateTypeImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getParameterSetSafetyStateType()
	 * @generated
	 */
	int PARAMETER_SET_SAFETY_STATE_TYPE = 4;

	/**
	 * The feature id for the '<em><b>Emergency Stop</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_SAFETY_STATE_TYPE__EMERGENCY_STOP = 0;

	/**
	 * The feature id for the '<em><b>Operational Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_SAFETY_STATE_TYPE__OPERATIONAL_MODE = 1;

	/**
	 * The feature id for the '<em><b>Protective Stop</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_SAFETY_STATE_TYPE__PROTECTIVE_STOP = 2;

	/**
	 * The number of structural features of the '<em>Parameter Set Safety State Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_SAFETY_STATE_TYPE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Parameter Set Safety State Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_SAFETY_STATE_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.GearTypeImpl <em>Gear Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.GearTypeImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getGearType()
	 * @generated
	 */
	int GEAR_TYPE = 5;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__BASE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__NODE_ID = OPCUADIProfilePackage.COMPONENT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__NAMESPACE_URI = OPCUADIProfilePackage.COMPONENT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__BROWSE_NAME = OPCUADIProfilePackage.COMPONENT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__NODE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__NODE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__BASE_INTERFACE = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_INTERFACE;

	/**
	 * The feature id for the '<em><b>Manufacturer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__MANUFACTURER = OPCUADIProfilePackage.COMPONENT_TYPE__MANUFACTURER;

	/**
	 * The feature id for the '<em><b>Manufacturer Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__MANUFACTURER_URI = OPCUADIProfilePackage.COMPONENT_TYPE__MANUFACTURER_URI;

	/**
	 * The feature id for the '<em><b>Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__MODEL = OPCUADIProfilePackage.COMPONENT_TYPE__MODEL;

	/**
	 * The feature id for the '<em><b>Product Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__PRODUCT_CODE = OPCUADIProfilePackage.COMPONENT_TYPE__PRODUCT_CODE;

	/**
	 * The feature id for the '<em><b>Hardware Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__HARDWARE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__HARDWARE_REVISION;

	/**
	 * The feature id for the '<em><b>Software Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__SOFTWARE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__SOFTWARE_REVISION;

	/**
	 * The feature id for the '<em><b>Device Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__DEVICE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_REVISION;

	/**
	 * The feature id for the '<em><b>Device Manual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__DEVICE_MANUAL = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_MANUAL;

	/**
	 * The feature id for the '<em><b>Device Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__DEVICE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_CLASS;

	/**
	 * The feature id for the '<em><b>Serial Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__SERIAL_NUMBER = OPCUADIProfilePackage.COMPONENT_TYPE__SERIAL_NUMBER;

	/**
	 * The feature id for the '<em><b>Product Instance Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__PRODUCT_INSTANCE_URI = OPCUADIProfilePackage.COMPONENT_TYPE__PRODUCT_INSTANCE_URI;

	/**
	 * The feature id for the '<em><b>Revision Counter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__REVISION_COUNTER = OPCUADIProfilePackage.COMPONENT_TYPE__REVISION_COUNTER;

	/**
	 * The feature id for the '<em><b>Group Identifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__GROUP_IDENTIFIER = OPCUADIProfilePackage.COMPONENT_TYPE__GROUP_IDENTIFIER;

	/**
	 * The feature id for the '<em><b>Identification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__IDENTIFICATION = OPCUADIProfilePackage.COMPONENT_TYPE__IDENTIFICATION;

	/**
	 * The feature id for the '<em><b>Lock</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__LOCK = OPCUADIProfilePackage.COMPONENT_TYPE__LOCK;

	/**
	 * The feature id for the '<em><b>Parameter Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__PARAMETER_SET = OPCUADIProfilePackage.COMPONENT_TYPE__PARAMETER_SET;

	/**
	 * The feature id for the '<em><b>Method Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__METHOD_SET = OPCUADIProfilePackage.COMPONENT_TYPE__METHOD_SET;

	/**
	 * The feature id for the '<em><b>Asset Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__ASSET_ID = OPCUADIProfilePackage.COMPONENT_TYPE__ASSET_ID;

	/**
	 * The feature id for the '<em><b>Component Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__COMPONENT_NAME = OPCUADIProfilePackage.COMPONENT_TYPE__COMPONENT_NAME;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__BASE_PROPERTY = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Gear Ratio</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__GEAR_RATIO = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Pitch</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__PITCH = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Is Connected To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE__IS_CONNECTED_TO = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Gear Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE_FEATURE_COUNT = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Gear Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GEAR_TYPE_OPERATION_COUNT = OPCUADIProfilePackage.COMPONENT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MotorTypeImpl <em>Motor Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MotorTypeImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getMotorType()
	 * @generated
	 */
	int MOTOR_TYPE = 6;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__BASE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__NODE_ID = OPCUADIProfilePackage.COMPONENT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__NAMESPACE_URI = OPCUADIProfilePackage.COMPONENT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__BROWSE_NAME = OPCUADIProfilePackage.COMPONENT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__NODE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__NODE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__BASE_INTERFACE = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_INTERFACE;

	/**
	 * The feature id for the '<em><b>Manufacturer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__MANUFACTURER = OPCUADIProfilePackage.COMPONENT_TYPE__MANUFACTURER;

	/**
	 * The feature id for the '<em><b>Manufacturer Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__MANUFACTURER_URI = OPCUADIProfilePackage.COMPONENT_TYPE__MANUFACTURER_URI;

	/**
	 * The feature id for the '<em><b>Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__MODEL = OPCUADIProfilePackage.COMPONENT_TYPE__MODEL;

	/**
	 * The feature id for the '<em><b>Product Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__PRODUCT_CODE = OPCUADIProfilePackage.COMPONENT_TYPE__PRODUCT_CODE;

	/**
	 * The feature id for the '<em><b>Hardware Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__HARDWARE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__HARDWARE_REVISION;

	/**
	 * The feature id for the '<em><b>Software Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__SOFTWARE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__SOFTWARE_REVISION;

	/**
	 * The feature id for the '<em><b>Device Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__DEVICE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_REVISION;

	/**
	 * The feature id for the '<em><b>Device Manual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__DEVICE_MANUAL = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_MANUAL;

	/**
	 * The feature id for the '<em><b>Device Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__DEVICE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_CLASS;

	/**
	 * The feature id for the '<em><b>Serial Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__SERIAL_NUMBER = OPCUADIProfilePackage.COMPONENT_TYPE__SERIAL_NUMBER;

	/**
	 * The feature id for the '<em><b>Product Instance Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__PRODUCT_INSTANCE_URI = OPCUADIProfilePackage.COMPONENT_TYPE__PRODUCT_INSTANCE_URI;

	/**
	 * The feature id for the '<em><b>Revision Counter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__REVISION_COUNTER = OPCUADIProfilePackage.COMPONENT_TYPE__REVISION_COUNTER;

	/**
	 * The feature id for the '<em><b>Group Identifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__GROUP_IDENTIFIER = OPCUADIProfilePackage.COMPONENT_TYPE__GROUP_IDENTIFIER;

	/**
	 * The feature id for the '<em><b>Identification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__IDENTIFICATION = OPCUADIProfilePackage.COMPONENT_TYPE__IDENTIFICATION;

	/**
	 * The feature id for the '<em><b>Lock</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__LOCK = OPCUADIProfilePackage.COMPONENT_TYPE__LOCK;

	/**
	 * The feature id for the '<em><b>Parameter Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__PARAMETER_SET = OPCUADIProfilePackage.COMPONENT_TYPE__PARAMETER_SET;

	/**
	 * The feature id for the '<em><b>Method Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__METHOD_SET = OPCUADIProfilePackage.COMPONENT_TYPE__METHOD_SET;

	/**
	 * The feature id for the '<em><b>Asset Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__ASSET_ID = OPCUADIProfilePackage.COMPONENT_TYPE__ASSET_ID;

	/**
	 * The feature id for the '<em><b>Component Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__COMPONENT_NAME = OPCUADIProfilePackage.COMPONENT_TYPE__COMPONENT_NAME;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__BASE_PROPERTY = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Parameter Set Motor Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE__PARAMETER_SET_MOTOR_TYPE = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Motor Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE_FEATURE_COUNT = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Motor Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_TYPE_OPERATION_COUNT = OPCUADIProfilePackage.COMPONENT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetAxisTypeImpl <em>Parameter Set Axis Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetAxisTypeImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getParameterSetAxisType()
	 * @generated
	 */
	int PARAMETER_SET_AXIS_TYPE = 7;

	/**
	 * The feature id for the '<em><b>Actual Acceleration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_AXIS_TYPE__ACTUAL_ACCELERATION = 0;

	/**
	 * The feature id for the '<em><b>Actual Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_AXIS_TYPE__ACTUAL_POSITION = 1;

	/**
	 * The feature id for the '<em><b>Actual Speed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_AXIS_TYPE__ACTUAL_SPEED = 2;

	/**
	 * The feature id for the '<em><b>Not CS Axis State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_AXIS_TYPE__NOT_CS_AXIS_STATE = 3;

	/**
	 * The number of structural features of the '<em>Parameter Set Axis Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_AXIS_TYPE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Parameter Set Axis Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_AXIS_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetMotionDeviceTypeImpl <em>Parameter Set Motion Device Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetMotionDeviceTypeImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getParameterSetMotionDeviceType()
	 * @generated
	 */
	int PARAMETER_SET_MOTION_DEVICE_TYPE = 8;

	/**
	 * The feature id for the '<em><b>On Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_MOTION_DEVICE_TYPE__ON_PATH = 0;

	/**
	 * The feature id for the '<em><b>In Control</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_MOTION_DEVICE_TYPE__IN_CONTROL = 1;

	/**
	 * The feature id for the '<em><b>Speed Override</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_MOTION_DEVICE_TYPE__SPEED_OVERRIDE = 2;

	/**
	 * The feature id for the '<em><b>Not CS is Power Button Pressed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_IS_POWER_BUTTON_PRESSED = 3;

	/**
	 * The feature id for the '<em><b>Not CS Robot Intensity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_ROBOT_INTENSITY = 4;

	/**
	 * The feature id for the '<em><b>Not CS is Teach Button Pressed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_IS_TEACH_BUTTON_PRESSED = 5;

	/**
	 * The feature id for the '<em><b>Not CS is Power On Robot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_IS_POWER_ON_ROBOT = 6;

	/**
	 * The feature id for the '<em><b>Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_MOTION_DEVICE_TYPE__MODE = 7;

	/**
	 * The number of structural features of the '<em>Parameter Set Motion Device Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_MOTION_DEVICE_TYPE_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Parameter Set Motion Device Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_SET_MOTION_DEVICE_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.PowerTrainTypeImpl <em>Power Train Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.PowerTrainTypeImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getPowerTrainType()
	 * @generated
	 */
	int POWER_TRAIN_TYPE = 9;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__BASE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__NODE_ID = OPCUADIProfilePackage.COMPONENT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__NAMESPACE_URI = OPCUADIProfilePackage.COMPONENT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__BROWSE_NAME = OPCUADIProfilePackage.COMPONENT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__NODE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__NODE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__BASE_INTERFACE = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_INTERFACE;

	/**
	 * The feature id for the '<em><b>Manufacturer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__MANUFACTURER = OPCUADIProfilePackage.COMPONENT_TYPE__MANUFACTURER;

	/**
	 * The feature id for the '<em><b>Manufacturer Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__MANUFACTURER_URI = OPCUADIProfilePackage.COMPONENT_TYPE__MANUFACTURER_URI;

	/**
	 * The feature id for the '<em><b>Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__MODEL = OPCUADIProfilePackage.COMPONENT_TYPE__MODEL;

	/**
	 * The feature id for the '<em><b>Product Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__PRODUCT_CODE = OPCUADIProfilePackage.COMPONENT_TYPE__PRODUCT_CODE;

	/**
	 * The feature id for the '<em><b>Hardware Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__HARDWARE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__HARDWARE_REVISION;

	/**
	 * The feature id for the '<em><b>Software Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__SOFTWARE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__SOFTWARE_REVISION;

	/**
	 * The feature id for the '<em><b>Device Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__DEVICE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_REVISION;

	/**
	 * The feature id for the '<em><b>Device Manual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__DEVICE_MANUAL = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_MANUAL;

	/**
	 * The feature id for the '<em><b>Device Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__DEVICE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_CLASS;

	/**
	 * The feature id for the '<em><b>Serial Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__SERIAL_NUMBER = OPCUADIProfilePackage.COMPONENT_TYPE__SERIAL_NUMBER;

	/**
	 * The feature id for the '<em><b>Product Instance Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__PRODUCT_INSTANCE_URI = OPCUADIProfilePackage.COMPONENT_TYPE__PRODUCT_INSTANCE_URI;

	/**
	 * The feature id for the '<em><b>Revision Counter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__REVISION_COUNTER = OPCUADIProfilePackage.COMPONENT_TYPE__REVISION_COUNTER;

	/**
	 * The feature id for the '<em><b>Group Identifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__GROUP_IDENTIFIER = OPCUADIProfilePackage.COMPONENT_TYPE__GROUP_IDENTIFIER;

	/**
	 * The feature id for the '<em><b>Identification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__IDENTIFICATION = OPCUADIProfilePackage.COMPONENT_TYPE__IDENTIFICATION;

	/**
	 * The feature id for the '<em><b>Lock</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__LOCK = OPCUADIProfilePackage.COMPONENT_TYPE__LOCK;

	/**
	 * The feature id for the '<em><b>Parameter Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__PARAMETER_SET = OPCUADIProfilePackage.COMPONENT_TYPE__PARAMETER_SET;

	/**
	 * The feature id for the '<em><b>Method Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__METHOD_SET = OPCUADIProfilePackage.COMPONENT_TYPE__METHOD_SET;

	/**
	 * The feature id for the '<em><b>Asset Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__ASSET_ID = OPCUADIProfilePackage.COMPONENT_TYPE__ASSET_ID;

	/**
	 * The feature id for the '<em><b>Component Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__COMPONENT_NAME = OPCUADIProfilePackage.COMPONENT_TYPE__COMPONENT_NAME;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__BASE_PROPERTY = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Motor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__MOTOR = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Gear</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE__GEAR = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Power Train Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE_FEATURE_COUNT = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Power Train Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_TRAIN_TYPE_OPERATION_COUNT = OPCUADIProfilePackage.COMPONENT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MotionDeviceSystemTypeImpl <em>Motion Device System Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MotionDeviceSystemTypeImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getMotionDeviceSystemType()
	 * @generated
	 */
	int MOTION_DEVICE_SYSTEM_TYPE = 10;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__BASE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__NODE_ID = OPCUADIProfilePackage.COMPONENT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__NAMESPACE_URI = OPCUADIProfilePackage.COMPONENT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__BROWSE_NAME = OPCUADIProfilePackage.COMPONENT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__NODE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__NODE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__BASE_INTERFACE = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_INTERFACE;

	/**
	 * The feature id for the '<em><b>Manufacturer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__MANUFACTURER = OPCUADIProfilePackage.COMPONENT_TYPE__MANUFACTURER;

	/**
	 * The feature id for the '<em><b>Manufacturer Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__MANUFACTURER_URI = OPCUADIProfilePackage.COMPONENT_TYPE__MANUFACTURER_URI;

	/**
	 * The feature id for the '<em><b>Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__MODEL = OPCUADIProfilePackage.COMPONENT_TYPE__MODEL;

	/**
	 * The feature id for the '<em><b>Product Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__PRODUCT_CODE = OPCUADIProfilePackage.COMPONENT_TYPE__PRODUCT_CODE;

	/**
	 * The feature id for the '<em><b>Hardware Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__HARDWARE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__HARDWARE_REVISION;

	/**
	 * The feature id for the '<em><b>Software Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__SOFTWARE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__SOFTWARE_REVISION;

	/**
	 * The feature id for the '<em><b>Device Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__DEVICE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_REVISION;

	/**
	 * The feature id for the '<em><b>Device Manual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__DEVICE_MANUAL = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_MANUAL;

	/**
	 * The feature id for the '<em><b>Device Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__DEVICE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_CLASS;

	/**
	 * The feature id for the '<em><b>Serial Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__SERIAL_NUMBER = OPCUADIProfilePackage.COMPONENT_TYPE__SERIAL_NUMBER;

	/**
	 * The feature id for the '<em><b>Product Instance Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__PRODUCT_INSTANCE_URI = OPCUADIProfilePackage.COMPONENT_TYPE__PRODUCT_INSTANCE_URI;

	/**
	 * The feature id for the '<em><b>Revision Counter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__REVISION_COUNTER = OPCUADIProfilePackage.COMPONENT_TYPE__REVISION_COUNTER;

	/**
	 * The feature id for the '<em><b>Group Identifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__GROUP_IDENTIFIER = OPCUADIProfilePackage.COMPONENT_TYPE__GROUP_IDENTIFIER;

	/**
	 * The feature id for the '<em><b>Identification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__IDENTIFICATION = OPCUADIProfilePackage.COMPONENT_TYPE__IDENTIFICATION;

	/**
	 * The feature id for the '<em><b>Lock</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__LOCK = OPCUADIProfilePackage.COMPONENT_TYPE__LOCK;

	/**
	 * The feature id for the '<em><b>Parameter Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__PARAMETER_SET = OPCUADIProfilePackage.COMPONENT_TYPE__PARAMETER_SET;

	/**
	 * The feature id for the '<em><b>Method Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__METHOD_SET = OPCUADIProfilePackage.COMPONENT_TYPE__METHOD_SET;

	/**
	 * The feature id for the '<em><b>Asset Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__ASSET_ID = OPCUADIProfilePackage.COMPONENT_TYPE__ASSET_ID;

	/**
	 * The feature id for the '<em><b>Component Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__COMPONENT_NAME = OPCUADIProfilePackage.COMPONENT_TYPE__COMPONENT_NAME;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__BASE_PROPERTY = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Motion Devices</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__MOTION_DEVICES = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Controllers</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__CONTROLLERS = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Safety States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE__SAFETY_STATES = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Motion Device System Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE_FEATURE_COUNT = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Motion Device System Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_SYSTEM_TYPE_OPERATION_COUNT = OPCUADIProfilePackage.COMPONENT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MotionDeviceTypeImpl <em>Motion Device Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MotionDeviceTypeImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getMotionDeviceType()
	 * @generated
	 */
	int MOTION_DEVICE_TYPE = 11;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__BASE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__NODE_ID = OPCUADIProfilePackage.COMPONENT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__NAMESPACE_URI = OPCUADIProfilePackage.COMPONENT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__BROWSE_NAME = OPCUADIProfilePackage.COMPONENT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__NODE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__NODE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__BASE_INTERFACE = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_INTERFACE;

	/**
	 * The feature id for the '<em><b>Manufacturer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__MANUFACTURER = OPCUADIProfilePackage.COMPONENT_TYPE__MANUFACTURER;

	/**
	 * The feature id for the '<em><b>Manufacturer Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__MANUFACTURER_URI = OPCUADIProfilePackage.COMPONENT_TYPE__MANUFACTURER_URI;

	/**
	 * The feature id for the '<em><b>Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__MODEL = OPCUADIProfilePackage.COMPONENT_TYPE__MODEL;

	/**
	 * The feature id for the '<em><b>Product Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__PRODUCT_CODE = OPCUADIProfilePackage.COMPONENT_TYPE__PRODUCT_CODE;

	/**
	 * The feature id for the '<em><b>Hardware Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__HARDWARE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__HARDWARE_REVISION;

	/**
	 * The feature id for the '<em><b>Software Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__SOFTWARE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__SOFTWARE_REVISION;

	/**
	 * The feature id for the '<em><b>Device Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__DEVICE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_REVISION;

	/**
	 * The feature id for the '<em><b>Device Manual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__DEVICE_MANUAL = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_MANUAL;

	/**
	 * The feature id for the '<em><b>Device Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__DEVICE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_CLASS;

	/**
	 * The feature id for the '<em><b>Serial Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__SERIAL_NUMBER = OPCUADIProfilePackage.COMPONENT_TYPE__SERIAL_NUMBER;

	/**
	 * The feature id for the '<em><b>Product Instance Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__PRODUCT_INSTANCE_URI = OPCUADIProfilePackage.COMPONENT_TYPE__PRODUCT_INSTANCE_URI;

	/**
	 * The feature id for the '<em><b>Revision Counter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__REVISION_COUNTER = OPCUADIProfilePackage.COMPONENT_TYPE__REVISION_COUNTER;

	/**
	 * The feature id for the '<em><b>Group Identifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__GROUP_IDENTIFIER = OPCUADIProfilePackage.COMPONENT_TYPE__GROUP_IDENTIFIER;

	/**
	 * The feature id for the '<em><b>Identification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__IDENTIFICATION = OPCUADIProfilePackage.COMPONENT_TYPE__IDENTIFICATION;

	/**
	 * The feature id for the '<em><b>Lock</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__LOCK = OPCUADIProfilePackage.COMPONENT_TYPE__LOCK;

	/**
	 * The feature id for the '<em><b>Parameter Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__PARAMETER_SET = OPCUADIProfilePackage.COMPONENT_TYPE__PARAMETER_SET;

	/**
	 * The feature id for the '<em><b>Method Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__METHOD_SET = OPCUADIProfilePackage.COMPONENT_TYPE__METHOD_SET;

	/**
	 * The feature id for the '<em><b>Asset Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__ASSET_ID = OPCUADIProfilePackage.COMPONENT_TYPE__ASSET_ID;

	/**
	 * The feature id for the '<em><b>Component Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__COMPONENT_NAME = OPCUADIProfilePackage.COMPONENT_TYPE__COMPONENT_NAME;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__BASE_PROPERTY = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Motion Device Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__MOTION_DEVICE_CATEGORY = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parameter Set Motion Device Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__PARAMETER_SET_MOTION_DEVICE_TYPE = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Axes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__AXES = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Power Trains</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__POWER_TRAINS = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Additional Components</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__ADDITIONAL_COMPONENTS = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Flange Load</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE__FLANGE_LOAD = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Motion Device Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE_FEATURE_COUNT = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Motion Device Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_DEVICE_TYPE_OPERATION_COUNT = OPCUADIProfilePackage.COMPONENT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.AxisTypeImpl <em>Axis Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.AxisTypeImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getAxisType()
	 * @generated
	 */
	int AXIS_TYPE = 12;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__BASE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__NODE_ID = OPCUADIProfilePackage.COMPONENT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__NAMESPACE_URI = OPCUADIProfilePackage.COMPONENT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__BROWSE_NAME = OPCUADIProfilePackage.COMPONENT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__NODE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__NODE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__BASE_INTERFACE = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_INTERFACE;

	/**
	 * The feature id for the '<em><b>Manufacturer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__MANUFACTURER = OPCUADIProfilePackage.COMPONENT_TYPE__MANUFACTURER;

	/**
	 * The feature id for the '<em><b>Manufacturer Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__MANUFACTURER_URI = OPCUADIProfilePackage.COMPONENT_TYPE__MANUFACTURER_URI;

	/**
	 * The feature id for the '<em><b>Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__MODEL = OPCUADIProfilePackage.COMPONENT_TYPE__MODEL;

	/**
	 * The feature id for the '<em><b>Product Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__PRODUCT_CODE = OPCUADIProfilePackage.COMPONENT_TYPE__PRODUCT_CODE;

	/**
	 * The feature id for the '<em><b>Hardware Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__HARDWARE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__HARDWARE_REVISION;

	/**
	 * The feature id for the '<em><b>Software Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__SOFTWARE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__SOFTWARE_REVISION;

	/**
	 * The feature id for the '<em><b>Device Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__DEVICE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_REVISION;

	/**
	 * The feature id for the '<em><b>Device Manual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__DEVICE_MANUAL = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_MANUAL;

	/**
	 * The feature id for the '<em><b>Device Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__DEVICE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_CLASS;

	/**
	 * The feature id for the '<em><b>Serial Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__SERIAL_NUMBER = OPCUADIProfilePackage.COMPONENT_TYPE__SERIAL_NUMBER;

	/**
	 * The feature id for the '<em><b>Product Instance Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__PRODUCT_INSTANCE_URI = OPCUADIProfilePackage.COMPONENT_TYPE__PRODUCT_INSTANCE_URI;

	/**
	 * The feature id for the '<em><b>Revision Counter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__REVISION_COUNTER = OPCUADIProfilePackage.COMPONENT_TYPE__REVISION_COUNTER;

	/**
	 * The feature id for the '<em><b>Group Identifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__GROUP_IDENTIFIER = OPCUADIProfilePackage.COMPONENT_TYPE__GROUP_IDENTIFIER;

	/**
	 * The feature id for the '<em><b>Identification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__IDENTIFICATION = OPCUADIProfilePackage.COMPONENT_TYPE__IDENTIFICATION;

	/**
	 * The feature id for the '<em><b>Lock</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__LOCK = OPCUADIProfilePackage.COMPONENT_TYPE__LOCK;

	/**
	 * The feature id for the '<em><b>Parameter Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__PARAMETER_SET = OPCUADIProfilePackage.COMPONENT_TYPE__PARAMETER_SET;

	/**
	 * The feature id for the '<em><b>Method Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__METHOD_SET = OPCUADIProfilePackage.COMPONENT_TYPE__METHOD_SET;

	/**
	 * The feature id for the '<em><b>Asset Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__ASSET_ID = OPCUADIProfilePackage.COMPONENT_TYPE__ASSET_ID;

	/**
	 * The feature id for the '<em><b>Component Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__COMPONENT_NAME = OPCUADIProfilePackage.COMPONENT_TYPE__COMPONENT_NAME;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__BASE_PROPERTY = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Motion Profile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__MOTION_PROFILE = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Additional Load</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__ADDITIONAL_LOAD = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Parameter Set Axis Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE__PARAMETER_SET_AXIS_TYPE = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Axis Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE_FEATURE_COUNT = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Axis Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_TYPE_OPERATION_COUNT = OPCUADIProfilePackage.COMPONENT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ControllerTypeImpl <em>Controller Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ControllerTypeImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getControllerType()
	 * @generated
	 */
	int CONTROLLER_TYPE = 13;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__BASE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__NODE_ID = OPCUADIProfilePackage.COMPONENT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__NAMESPACE_URI = OPCUADIProfilePackage.COMPONENT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__BROWSE_NAME = OPCUADIProfilePackage.COMPONENT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__NODE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__NODE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__BASE_INTERFACE = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_INTERFACE;

	/**
	 * The feature id for the '<em><b>Manufacturer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__MANUFACTURER = OPCUADIProfilePackage.COMPONENT_TYPE__MANUFACTURER;

	/**
	 * The feature id for the '<em><b>Manufacturer Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__MANUFACTURER_URI = OPCUADIProfilePackage.COMPONENT_TYPE__MANUFACTURER_URI;

	/**
	 * The feature id for the '<em><b>Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__MODEL = OPCUADIProfilePackage.COMPONENT_TYPE__MODEL;

	/**
	 * The feature id for the '<em><b>Product Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__PRODUCT_CODE = OPCUADIProfilePackage.COMPONENT_TYPE__PRODUCT_CODE;

	/**
	 * The feature id for the '<em><b>Hardware Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__HARDWARE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__HARDWARE_REVISION;

	/**
	 * The feature id for the '<em><b>Software Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__SOFTWARE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__SOFTWARE_REVISION;

	/**
	 * The feature id for the '<em><b>Device Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__DEVICE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_REVISION;

	/**
	 * The feature id for the '<em><b>Device Manual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__DEVICE_MANUAL = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_MANUAL;

	/**
	 * The feature id for the '<em><b>Device Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__DEVICE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_CLASS;

	/**
	 * The feature id for the '<em><b>Serial Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__SERIAL_NUMBER = OPCUADIProfilePackage.COMPONENT_TYPE__SERIAL_NUMBER;

	/**
	 * The feature id for the '<em><b>Product Instance Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__PRODUCT_INSTANCE_URI = OPCUADIProfilePackage.COMPONENT_TYPE__PRODUCT_INSTANCE_URI;

	/**
	 * The feature id for the '<em><b>Revision Counter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__REVISION_COUNTER = OPCUADIProfilePackage.COMPONENT_TYPE__REVISION_COUNTER;

	/**
	 * The feature id for the '<em><b>Group Identifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__GROUP_IDENTIFIER = OPCUADIProfilePackage.COMPONENT_TYPE__GROUP_IDENTIFIER;

	/**
	 * The feature id for the '<em><b>Identification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__IDENTIFICATION = OPCUADIProfilePackage.COMPONENT_TYPE__IDENTIFICATION;

	/**
	 * The feature id for the '<em><b>Lock</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__LOCK = OPCUADIProfilePackage.COMPONENT_TYPE__LOCK;

	/**
	 * The feature id for the '<em><b>Parameter Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__PARAMETER_SET = OPCUADIProfilePackage.COMPONENT_TYPE__PARAMETER_SET;

	/**
	 * The feature id for the '<em><b>Method Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__METHOD_SET = OPCUADIProfilePackage.COMPONENT_TYPE__METHOD_SET;

	/**
	 * The feature id for the '<em><b>Asset Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__ASSET_ID = OPCUADIProfilePackage.COMPONENT_TYPE__ASSET_ID;

	/**
	 * The feature id for the '<em><b>Component Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__COMPONENT_NAME = OPCUADIProfilePackage.COMPONENT_TYPE__COMPONENT_NAME;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__BASE_PROPERTY = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Parameter Set Controller Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__PARAMETER_SET_CONTROLLER_TYPE = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Components</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__COMPONENTS = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Software</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__SOFTWARE = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Task Controls</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE__TASK_CONTROLS = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Controller Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE_FEATURE_COUNT = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Controller Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_TYPE_OPERATION_COUNT = OPCUADIProfilePackage.COMPONENT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.TaskControlTypeImpl <em>Task Control Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.TaskControlTypeImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getTaskControlType()
	 * @generated
	 */
	int TASK_CONTROL_TYPE = 14;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__BASE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__NODE_ID = OPCUADIProfilePackage.COMPONENT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__NAMESPACE_URI = OPCUADIProfilePackage.COMPONENT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__BROWSE_NAME = OPCUADIProfilePackage.COMPONENT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__NODE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__NODE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__BASE_INTERFACE = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_INTERFACE;

	/**
	 * The feature id for the '<em><b>Manufacturer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__MANUFACTURER = OPCUADIProfilePackage.COMPONENT_TYPE__MANUFACTURER;

	/**
	 * The feature id for the '<em><b>Manufacturer Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__MANUFACTURER_URI = OPCUADIProfilePackage.COMPONENT_TYPE__MANUFACTURER_URI;

	/**
	 * The feature id for the '<em><b>Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__MODEL = OPCUADIProfilePackage.COMPONENT_TYPE__MODEL;

	/**
	 * The feature id for the '<em><b>Product Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__PRODUCT_CODE = OPCUADIProfilePackage.COMPONENT_TYPE__PRODUCT_CODE;

	/**
	 * The feature id for the '<em><b>Hardware Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__HARDWARE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__HARDWARE_REVISION;

	/**
	 * The feature id for the '<em><b>Software Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__SOFTWARE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__SOFTWARE_REVISION;

	/**
	 * The feature id for the '<em><b>Device Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__DEVICE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_REVISION;

	/**
	 * The feature id for the '<em><b>Device Manual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__DEVICE_MANUAL = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_MANUAL;

	/**
	 * The feature id for the '<em><b>Device Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__DEVICE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_CLASS;

	/**
	 * The feature id for the '<em><b>Serial Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__SERIAL_NUMBER = OPCUADIProfilePackage.COMPONENT_TYPE__SERIAL_NUMBER;

	/**
	 * The feature id for the '<em><b>Product Instance Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__PRODUCT_INSTANCE_URI = OPCUADIProfilePackage.COMPONENT_TYPE__PRODUCT_INSTANCE_URI;

	/**
	 * The feature id for the '<em><b>Revision Counter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__REVISION_COUNTER = OPCUADIProfilePackage.COMPONENT_TYPE__REVISION_COUNTER;

	/**
	 * The feature id for the '<em><b>Group Identifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__GROUP_IDENTIFIER = OPCUADIProfilePackage.COMPONENT_TYPE__GROUP_IDENTIFIER;

	/**
	 * The feature id for the '<em><b>Identification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__IDENTIFICATION = OPCUADIProfilePackage.COMPONENT_TYPE__IDENTIFICATION;

	/**
	 * The feature id for the '<em><b>Lock</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__LOCK = OPCUADIProfilePackage.COMPONENT_TYPE__LOCK;

	/**
	 * The feature id for the '<em><b>Parameter Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__PARAMETER_SET = OPCUADIProfilePackage.COMPONENT_TYPE__PARAMETER_SET;

	/**
	 * The feature id for the '<em><b>Method Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__METHOD_SET = OPCUADIProfilePackage.COMPONENT_TYPE__METHOD_SET;

	/**
	 * The feature id for the '<em><b>Asset Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__ASSET_ID = OPCUADIProfilePackage.COMPONENT_TYPE__ASSET_ID;

	/**
	 * The feature id for the '<em><b>Component Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__COMPONENT_NAME = OPCUADIProfilePackage.COMPONENT_TYPE__COMPONENT_NAME;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__BASE_PROPERTY = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Parameter Set Task Contol Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE__PARAMETER_SET_TASK_CONTOL_TYPE = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Task Control Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE_FEATURE_COUNT = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Task Control Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CONTROL_TYPE_OPERATION_COUNT = OPCUADIProfilePackage.COMPONENT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.SafetyStateTypeImpl <em>Safety State Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.SafetyStateTypeImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getSafetyStateType()
	 * @generated
	 */
	int SAFETY_STATE_TYPE = 15;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__BASE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__NODE_ID = OPCUADIProfilePackage.COMPONENT_TYPE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Namespace Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__NAMESPACE_URI = OPCUADIProfilePackage.COMPONENT_TYPE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Browse Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__BROWSE_NAME = OPCUADIProfilePackage.COMPONENT_TYPE__BROWSE_NAME;

	/**
	 * The feature id for the '<em><b>Node Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__NODE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__NODE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__BASE_INTERFACE = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_INTERFACE;

	/**
	 * The feature id for the '<em><b>Manufacturer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__MANUFACTURER = OPCUADIProfilePackage.COMPONENT_TYPE__MANUFACTURER;

	/**
	 * The feature id for the '<em><b>Manufacturer Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__MANUFACTURER_URI = OPCUADIProfilePackage.COMPONENT_TYPE__MANUFACTURER_URI;

	/**
	 * The feature id for the '<em><b>Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__MODEL = OPCUADIProfilePackage.COMPONENT_TYPE__MODEL;

	/**
	 * The feature id for the '<em><b>Product Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__PRODUCT_CODE = OPCUADIProfilePackage.COMPONENT_TYPE__PRODUCT_CODE;

	/**
	 * The feature id for the '<em><b>Hardware Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__HARDWARE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__HARDWARE_REVISION;

	/**
	 * The feature id for the '<em><b>Software Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__SOFTWARE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__SOFTWARE_REVISION;

	/**
	 * The feature id for the '<em><b>Device Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__DEVICE_REVISION = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_REVISION;

	/**
	 * The feature id for the '<em><b>Device Manual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__DEVICE_MANUAL = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_MANUAL;

	/**
	 * The feature id for the '<em><b>Device Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__DEVICE_CLASS = OPCUADIProfilePackage.COMPONENT_TYPE__DEVICE_CLASS;

	/**
	 * The feature id for the '<em><b>Serial Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__SERIAL_NUMBER = OPCUADIProfilePackage.COMPONENT_TYPE__SERIAL_NUMBER;

	/**
	 * The feature id for the '<em><b>Product Instance Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__PRODUCT_INSTANCE_URI = OPCUADIProfilePackage.COMPONENT_TYPE__PRODUCT_INSTANCE_URI;

	/**
	 * The feature id for the '<em><b>Revision Counter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__REVISION_COUNTER = OPCUADIProfilePackage.COMPONENT_TYPE__REVISION_COUNTER;

	/**
	 * The feature id for the '<em><b>Group Identifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__GROUP_IDENTIFIER = OPCUADIProfilePackage.COMPONENT_TYPE__GROUP_IDENTIFIER;

	/**
	 * The feature id for the '<em><b>Identification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__IDENTIFICATION = OPCUADIProfilePackage.COMPONENT_TYPE__IDENTIFICATION;

	/**
	 * The feature id for the '<em><b>Lock</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__LOCK = OPCUADIProfilePackage.COMPONENT_TYPE__LOCK;

	/**
	 * The feature id for the '<em><b>Parameter Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__PARAMETER_SET = OPCUADIProfilePackage.COMPONENT_TYPE__PARAMETER_SET;

	/**
	 * The feature id for the '<em><b>Method Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__METHOD_SET = OPCUADIProfilePackage.COMPONENT_TYPE__METHOD_SET;

	/**
	 * The feature id for the '<em><b>Asset Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__ASSET_ID = OPCUADIProfilePackage.COMPONENT_TYPE__ASSET_ID;

	/**
	 * The feature id for the '<em><b>Component Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__COMPONENT_NAME = OPCUADIProfilePackage.COMPONENT_TYPE__COMPONENT_NAME;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__BASE_PROPERTY = OPCUADIProfilePackage.COMPONENT_TYPE__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Emergency Stop Functions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__EMERGENCY_STOP_FUNCTIONS = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Protective Stop Functions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__PROTECTIVE_STOP_FUNCTIONS = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Parameter Set Safety State Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE__PARAMETER_SET_SAFETY_STATE_TYPE = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Safety State Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE_FEATURE_COUNT = OPCUADIProfilePackage.COMPONENT_TYPE_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Safety State Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_STATE_TYPE_OPERATION_COUNT = OPCUADIProfilePackage.COMPONENT_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.EmergencyStopFunctionTypeImpl <em>Emergency Stop Function Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.EmergencyStopFunctionTypeImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getEmergencyStopFunctionType()
	 * @generated
	 */
	int EMERGENCY_STOP_FUNCTION_TYPE = 16;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMERGENCY_STOP_FUNCTION_TYPE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMERGENCY_STOP_FUNCTION_TYPE__ACTIVE = 1;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMERGENCY_STOP_FUNCTION_TYPE__BASE_CLASS = 2;

	/**
	 * The number of structural features of the '<em>Emergency Stop Function Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMERGENCY_STOP_FUNCTION_TYPE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Emergency Stop Function Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMERGENCY_STOP_FUNCTION_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ProtectiveStopFunctionTypeImpl <em>Protective Stop Function Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ProtectiveStopFunctionTypeImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getProtectiveStopFunctionType()
	 * @generated
	 */
	int PROTECTIVE_STOP_FUNCTION_TYPE = 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROTECTIVE_STOP_FUNCTION_TYPE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROTECTIVE_STOP_FUNCTION_TYPE__ENABLED = 1;

	/**
	 * The feature id for the '<em><b>Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROTECTIVE_STOP_FUNCTION_TYPE__ACTIVE = 2;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROTECTIVE_STOP_FUNCTION_TYPE__BASE_CLASS = 3;

	/**
	 * The number of structural features of the '<em>Protective Stop Function Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROTECTIVE_STOP_FUNCTION_TYPE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Protective Stop Function Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROTECTIVE_STOP_FUNCTION_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ReferencesImpl <em>References</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ReferencesImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getReferences()
	 * @generated
	 */
	int REFERENCES = 18;

	/**
	 * The feature id for the '<em><b>Base Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCES__BASE_ASSOCIATION = 0;

	/**
	 * The feature id for the '<em><b>Base Dependency</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCES__BASE_DEPENDENCY = 1;

	/**
	 * The number of structural features of the '<em>References</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCES_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>References</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCES_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.HierarchicalReferencesImpl <em>Hierarchical References</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.HierarchicalReferencesImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getHierarchicalReferences()
	 * @generated
	 */
	int HIERARCHICAL_REFERENCES = 19;

	/**
	 * The feature id for the '<em><b>Base Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HIERARCHICAL_REFERENCES__BASE_ASSOCIATION = REFERENCES__BASE_ASSOCIATION;

	/**
	 * The feature id for the '<em><b>Base Dependency</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HIERARCHICAL_REFERENCES__BASE_DEPENDENCY = REFERENCES__BASE_DEPENDENCY;

	/**
	 * The number of structural features of the '<em>Hierarchical References</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HIERARCHICAL_REFERENCES_FEATURE_COUNT = REFERENCES_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Hierarchical References</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HIERARCHICAL_REFERENCES_OPERATION_COUNT = REFERENCES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ControlsImpl <em>Controls</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ControlsImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getControls()
	 * @generated
	 */
	int CONTROLS = 20;

	/**
	 * The feature id for the '<em><b>Base Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLS__BASE_ASSOCIATION = HIERARCHICAL_REFERENCES__BASE_ASSOCIATION;

	/**
	 * The feature id for the '<em><b>Base Dependency</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLS__BASE_DEPENDENCY = HIERARCHICAL_REFERENCES__BASE_DEPENDENCY;

	/**
	 * The number of structural features of the '<em>Controls</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLS_FEATURE_COUNT = HIERARCHICAL_REFERENCES_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Controls</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLS_OPERATION_COUNT = HIERARCHICAL_REFERENCES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.IsDrivenByImpl <em>Is Driven By</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.IsDrivenByImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getIsDrivenBy()
	 * @generated
	 */
	int IS_DRIVEN_BY = 21;

	/**
	 * The feature id for the '<em><b>Base Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_DRIVEN_BY__BASE_ASSOCIATION = HIERARCHICAL_REFERENCES__BASE_ASSOCIATION;

	/**
	 * The feature id for the '<em><b>Base Dependency</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_DRIVEN_BY__BASE_DEPENDENCY = HIERARCHICAL_REFERENCES__BASE_DEPENDENCY;

	/**
	 * The number of structural features of the '<em>Is Driven By</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_DRIVEN_BY_FEATURE_COUNT = HIERARCHICAL_REFERENCES_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Is Driven By</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_DRIVEN_BY_OPERATION_COUNT = HIERARCHICAL_REFERENCES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MovesImpl <em>Moves</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MovesImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getMoves()
	 * @generated
	 */
	int MOVES = 22;

	/**
	 * The feature id for the '<em><b>Base Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOVES__BASE_ASSOCIATION = HIERARCHICAL_REFERENCES__BASE_ASSOCIATION;

	/**
	 * The feature id for the '<em><b>Base Dependency</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOVES__BASE_DEPENDENCY = HIERARCHICAL_REFERENCES__BASE_DEPENDENCY;

	/**
	 * The number of structural features of the '<em>Moves</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOVES_FEATURE_COUNT = HIERARCHICAL_REFERENCES_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Moves</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOVES_OPERATION_COUNT = HIERARCHICAL_REFERENCES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.RequiresImpl <em>Requires</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.RequiresImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getRequires()
	 * @generated
	 */
	int REQUIRES = 23;

	/**
	 * The feature id for the '<em><b>Base Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRES__BASE_ASSOCIATION = HIERARCHICAL_REFERENCES__BASE_ASSOCIATION;

	/**
	 * The feature id for the '<em><b>Base Dependency</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRES__BASE_DEPENDENCY = HIERARCHICAL_REFERENCES__BASE_DEPENDENCY;

	/**
	 * The number of structural features of the '<em>Requires</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRES_FEATURE_COUNT = HIERARCHICAL_REFERENCES_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Requires</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRES_OPERATION_COUNT = HIERARCHICAL_REFERENCES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.NonHierarchicalReferencesImpl <em>Non Hierarchical References</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.NonHierarchicalReferencesImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getNonHierarchicalReferences()
	 * @generated
	 */
	int NON_HIERARCHICAL_REFERENCES = 24;

	/**
	 * The feature id for the '<em><b>Base Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_HIERARCHICAL_REFERENCES__BASE_ASSOCIATION = REFERENCES__BASE_ASSOCIATION;

	/**
	 * The feature id for the '<em><b>Base Dependency</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_HIERARCHICAL_REFERENCES__BASE_DEPENDENCY = REFERENCES__BASE_DEPENDENCY;

	/**
	 * The number of structural features of the '<em>Non Hierarchical References</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_HIERARCHICAL_REFERENCES_FEATURE_COUNT = REFERENCES_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Non Hierarchical References</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_HIERARCHICAL_REFERENCES_OPERATION_COUNT = REFERENCES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.HasSafetyStatesImpl <em>Has Safety States</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.HasSafetyStatesImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getHasSafetyStates()
	 * @generated
	 */
	int HAS_SAFETY_STATES = 25;

	/**
	 * The feature id for the '<em><b>Base Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_SAFETY_STATES__BASE_ASSOCIATION = HIERARCHICAL_REFERENCES__BASE_ASSOCIATION;

	/**
	 * The feature id for the '<em><b>Base Dependency</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_SAFETY_STATES__BASE_DEPENDENCY = HIERARCHICAL_REFERENCES__BASE_DEPENDENCY;

	/**
	 * The number of structural features of the '<em>Has Safety States</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_SAFETY_STATES_FEATURE_COUNT = HIERARCHICAL_REFERENCES_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Has Safety States</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_SAFETY_STATES_OPERATION_COUNT = HIERARCHICAL_REFERENCES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.HasSlavesImpl <em>Has Slaves</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.HasSlavesImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getHasSlaves()
	 * @generated
	 */
	int HAS_SLAVES = 26;

	/**
	 * The feature id for the '<em><b>Base Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_SLAVES__BASE_ASSOCIATION = HIERARCHICAL_REFERENCES__BASE_ASSOCIATION;

	/**
	 * The feature id for the '<em><b>Base Dependency</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_SLAVES__BASE_DEPENDENCY = HIERARCHICAL_REFERENCES__BASE_DEPENDENCY;

	/**
	 * The number of structural features of the '<em>Has Slaves</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_SLAVES_FEATURE_COUNT = HIERARCHICAL_REFERENCES_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Has Slaves</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_SLAVES_OPERATION_COUNT = HIERARCHICAL_REFERENCES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.IsConnectedToImpl <em>Is Connected To</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.IsConnectedToImpl
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getIsConnectedTo()
	 * @generated
	 */
	int IS_CONNECTED_TO = 27;

	/**
	 * The feature id for the '<em><b>Base Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_CONNECTED_TO__BASE_ASSOCIATION = NON_HIERARCHICAL_REFERENCES__BASE_ASSOCIATION;

	/**
	 * The feature id for the '<em><b>Base Dependency</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_CONNECTED_TO__BASE_DEPENDENCY = NON_HIERARCHICAL_REFERENCES__BASE_DEPENDENCY;

	/**
	 * The number of structural features of the '<em>Is Connected To</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_CONNECTED_TO_FEATURE_COUNT = NON_HIERARCHICAL_REFERENCES_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Is Connected To</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_CONNECTED_TO_OPERATION_COUNT = NON_HIERARCHICAL_REFERENCES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ExecutionModeEnumeration <em>Execution Mode Enumeration</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ExecutionModeEnumeration
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getExecutionModeEnumeration()
	 * @generated
	 */
	int EXECUTION_MODE_ENUMERATION = 28;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OperationalModeEnumeration <em>Operational Mode Enumeration</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OperationalModeEnumeration
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getOperationalModeEnumeration()
	 * @generated
	 */
	int OPERATIONAL_MODE_ENUMERATION = 29;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisMotionProfileEnumeration <em>Axis Motion Profile Enumeration</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisMotionProfileEnumeration
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getAxisMotionProfileEnumeration()
	 * @generated
	 */
	int AXIS_MOTION_PROFILE_ENUMERATION = 30;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisStateEnumeration <em>Axis State Enumeration</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisStateEnumeration
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getAxisStateEnumeration()
	 * @generated
	 */
	int AXIS_STATE_ENUMERATION = 31;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ModeEnum <em>Mode Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ModeEnum
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getModeEnum()
	 * @generated
	 */
	int MODE_ENUM = 32;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceCategoryEnum <em>Motion Device Category Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceCategoryEnum
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getMotionDeviceCategoryEnum()
	 * @generated
	 */
	int MOTION_DEVICE_CATEGORY_ENUM = 33;


	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.LoadType <em>Load Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Load Type</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.LoadType
	 * @generated
	 */
	EClass getLoadType();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.LoadType#getMass <em>Mass</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Mass</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.LoadType#getMass()
	 * @see #getLoadType()
	 * @generated
	 */
	EReference getLoadType_Mass();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.LoadType#getCenterOfMass <em>Center Of Mass</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Center Of Mass</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.LoadType#getCenterOfMass()
	 * @see #getLoadType()
	 * @generated
	 */
	EReference getLoadType_CenterOfMass();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.LoadType#getInertia <em>Inertia</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Inertia</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.LoadType#getInertia()
	 * @see #getLoadType()
	 * @generated
	 */
	EReference getLoadType_Inertia();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotorType <em>Parameter Set Motor Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter Set Motor Type</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotorType
	 * @generated
	 */
	EClass getParameterSetMotorType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotorType#isBrakeReleased <em>Brake Released</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Brake Released</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotorType#isBrakeReleased()
	 * @see #getParameterSetMotorType()
	 * @generated
	 */
	EAttribute getParameterSetMotorType_BrakeReleased();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotorType#getEffectiveLoadRate <em>Effective Load Rate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Effective Load Rate</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotorType#getEffectiveLoadRate()
	 * @see #getParameterSetMotorType()
	 * @generated
	 */
	EAttribute getParameterSetMotorType_EffectiveLoadRate();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotorType#getMotorTemperature <em>Motor Temperature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Motor Temperature</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotorType#getMotorTemperature()
	 * @see #getParameterSetMotorType()
	 * @generated
	 */
	EAttribute getParameterSetMotorType_MotorTemperature();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotorType#getNotCS_MotorIntensity <em>Not CS Motor Intensity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Not CS Motor Intensity</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotorType#getNotCS_MotorIntensity()
	 * @see #getParameterSetMotorType()
	 * @generated
	 */
	EAttribute getParameterSetMotorType_NotCS_MotorIntensity();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType <em>Parameter Set Controller Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter Set Controller Type</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType
	 * @generated
	 */
	EClass getParameterSetControllerType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getTotalPowerOnTime <em>Total Power On Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Total Power On Time</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getTotalPowerOnTime()
	 * @see #getParameterSetControllerType()
	 * @generated
	 */
	EAttribute getParameterSetControllerType_TotalPowerOnTime();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getStartUpTime <em>Start Up Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Up Time</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getStartUpTime()
	 * @see #getParameterSetControllerType()
	 * @generated
	 */
	EAttribute getParameterSetControllerType_StartUpTime();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getUpsState <em>Ups State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ups State</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getUpsState()
	 * @see #getParameterSetControllerType()
	 * @generated
	 */
	EAttribute getParameterSetControllerType_UpsState();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getTotalEnergyConsumption <em>Total Energy Consumption</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Total Energy Consumption</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getTotalEnergyConsumption()
	 * @see #getParameterSetControllerType()
	 * @generated
	 */
	EAttribute getParameterSetControllerType_TotalEnergyConsumption();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getCabinetFanSpeed <em>Cabinet Fan Speed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cabinet Fan Speed</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getCabinetFanSpeed()
	 * @see #getParameterSetControllerType()
	 * @generated
	 */
	EAttribute getParameterSetControllerType_CabinetFanSpeed();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getCPUFanSpeed <em>CPU Fan Speed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>CPU Fan Speed</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getCPUFanSpeed()
	 * @see #getParameterSetControllerType()
	 * @generated
	 */
	EAttribute getParameterSetControllerType_CPUFanSpeed();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getInputVoltage <em>Input Voltage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Input Voltage</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getInputVoltage()
	 * @see #getParameterSetControllerType()
	 * @generated
	 */
	EAttribute getParameterSetControllerType_InputVoltage();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getTemperature <em>Temperature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Temperature</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType#getTemperature()
	 * @see #getParameterSetControllerType()
	 * @generated
	 */
	EAttribute getParameterSetControllerType_Temperature();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetTaskControlType <em>Parameter Set Task Control Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter Set Task Control Type</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetTaskControlType
	 * @generated
	 */
	EClass getParameterSetTaskControlType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetTaskControlType#getExecutionMode <em>Execution Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Execution Mode</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetTaskControlType#getExecutionMode()
	 * @see #getParameterSetTaskControlType()
	 * @generated
	 */
	EAttribute getParameterSetTaskControlType_ExecutionMode();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetTaskControlType#isTaskProgramLoaded <em>Task Program Loaded</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Task Program Loaded</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetTaskControlType#isTaskProgramLoaded()
	 * @see #getParameterSetTaskControlType()
	 * @generated
	 */
	EAttribute getParameterSetTaskControlType_TaskProgramLoaded();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetTaskControlType#getTaskProgramName <em>Task Program Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Task Program Name</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetTaskControlType#getTaskProgramName()
	 * @see #getParameterSetTaskControlType()
	 * @generated
	 */
	EAttribute getParameterSetTaskControlType_TaskProgramName();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetSafetyStateType <em>Parameter Set Safety State Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter Set Safety State Type</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetSafetyStateType
	 * @generated
	 */
	EClass getParameterSetSafetyStateType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetSafetyStateType#isEmergencyStop <em>Emergency Stop</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Emergency Stop</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetSafetyStateType#isEmergencyStop()
	 * @see #getParameterSetSafetyStateType()
	 * @generated
	 */
	EAttribute getParameterSetSafetyStateType_EmergencyStop();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetSafetyStateType#getOperationalMode <em>Operational Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operational Mode</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetSafetyStateType#getOperationalMode()
	 * @see #getParameterSetSafetyStateType()
	 * @generated
	 */
	EAttribute getParameterSetSafetyStateType_OperationalMode();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetSafetyStateType#isProtectiveStop <em>Protective Stop</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Protective Stop</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetSafetyStateType#isProtectiveStop()
	 * @see #getParameterSetSafetyStateType()
	 * @generated
	 */
	EAttribute getParameterSetSafetyStateType_ProtectiveStop();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.GearType <em>Gear Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Gear Type</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.GearType
	 * @generated
	 */
	EClass getGearType();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.GearType#getGearRatio <em>Gear Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Gear Ratio</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.GearType#getGearRatio()
	 * @see #getGearType()
	 * @generated
	 */
	EReference getGearType_GearRatio();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.GearType#getPitch <em>Pitch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Pitch</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.GearType#getPitch()
	 * @see #getGearType()
	 * @generated
	 */
	EReference getGearType_Pitch();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.GearType#getIsConnectedTo <em>Is Connected To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Is Connected To</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.GearType#getIsConnectedTo()
	 * @see #getGearType()
	 * @generated
	 */
	EReference getGearType_IsConnectedTo();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotorType <em>Motor Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Motor Type</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotorType
	 * @generated
	 */
	EClass getMotorType();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotorType#getParameterSetMotorType <em>Parameter Set Motor Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parameter Set Motor Type</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotorType#getParameterSetMotorType()
	 * @see #getMotorType()
	 * @generated
	 */
	EReference getMotorType_ParameterSetMotorType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetAxisType <em>Parameter Set Axis Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter Set Axis Type</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetAxisType
	 * @generated
	 */
	EClass getParameterSetAxisType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetAxisType#getActualAcceleration <em>Actual Acceleration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Actual Acceleration</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetAxisType#getActualAcceleration()
	 * @see #getParameterSetAxisType()
	 * @generated
	 */
	EAttribute getParameterSetAxisType_ActualAcceleration();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetAxisType#getActualPosition <em>Actual Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Actual Position</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetAxisType#getActualPosition()
	 * @see #getParameterSetAxisType()
	 * @generated
	 */
	EAttribute getParameterSetAxisType_ActualPosition();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetAxisType#getActualSpeed <em>Actual Speed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Actual Speed</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetAxisType#getActualSpeed()
	 * @see #getParameterSetAxisType()
	 * @generated
	 */
	EAttribute getParameterSetAxisType_ActualSpeed();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetAxisType#getNotCS_AxisState <em>Not CS Axis State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Not CS Axis State</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetAxisType#getNotCS_AxisState()
	 * @see #getParameterSetAxisType()
	 * @generated
	 */
	EAttribute getParameterSetAxisType_NotCS_AxisState();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType <em>Parameter Set Motion Device Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter Set Motion Device Type</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType
	 * @generated
	 */
	EClass getParameterSetMotionDeviceType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#isOnPath <em>On Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>On Path</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#isOnPath()
	 * @see #getParameterSetMotionDeviceType()
	 * @generated
	 */
	EAttribute getParameterSetMotionDeviceType_OnPath();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#isInControl <em>In Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>In Control</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#isInControl()
	 * @see #getParameterSetMotionDeviceType()
	 * @generated
	 */
	EAttribute getParameterSetMotionDeviceType_InControl();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#getSpeedOverride <em>Speed Override</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Speed Override</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#getSpeedOverride()
	 * @see #getParameterSetMotionDeviceType()
	 * @generated
	 */
	EAttribute getParameterSetMotionDeviceType_SpeedOverride();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#isNotCS_isPowerButtonPressed <em>Not CS is Power Button Pressed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Not CS is Power Button Pressed</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#isNotCS_isPowerButtonPressed()
	 * @see #getParameterSetMotionDeviceType()
	 * @generated
	 */
	EAttribute getParameterSetMotionDeviceType_NotCS_isPowerButtonPressed();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#getNotCS__RobotIntensity <em>Not CS Robot Intensity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Not CS Robot Intensity</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#getNotCS__RobotIntensity()
	 * @see #getParameterSetMotionDeviceType()
	 * @generated
	 */
	EAttribute getParameterSetMotionDeviceType_NotCS__RobotIntensity();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#isNotCS_isTeachButtonPressed <em>Not CS is Teach Button Pressed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Not CS is Teach Button Pressed</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#isNotCS_isTeachButtonPressed()
	 * @see #getParameterSetMotionDeviceType()
	 * @generated
	 */
	EAttribute getParameterSetMotionDeviceType_NotCS_isTeachButtonPressed();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#isNotCS_isPowerOnRobot <em>Not CS is Power On Robot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Not CS is Power On Robot</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#isNotCS_isPowerOnRobot()
	 * @see #getParameterSetMotionDeviceType()
	 * @generated
	 */
	EAttribute getParameterSetMotionDeviceType_NotCS_isPowerOnRobot();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#getMode <em>Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mode</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#getMode()
	 * @see #getParameterSetMotionDeviceType()
	 * @generated
	 */
	EAttribute getParameterSetMotionDeviceType_Mode();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.PowerTrainType <em>Power Train Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Power Train Type</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.PowerTrainType
	 * @generated
	 */
	EClass getPowerTrainType();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.PowerTrainType#getMotor <em>Motor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Motor</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.PowerTrainType#getMotor()
	 * @see #getPowerTrainType()
	 * @generated
	 */
	EReference getPowerTrainType_Motor();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.PowerTrainType#getGear <em>Gear</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Gear</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.PowerTrainType#getGear()
	 * @see #getPowerTrainType()
	 * @generated
	 */
	EReference getPowerTrainType_Gear();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceSystemType <em>Motion Device System Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Motion Device System Type</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceSystemType
	 * @generated
	 */
	EClass getMotionDeviceSystemType();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceSystemType#getMotionDevices <em>Motion Devices</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Motion Devices</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceSystemType#getMotionDevices()
	 * @see #getMotionDeviceSystemType()
	 * @generated
	 */
	EReference getMotionDeviceSystemType_MotionDevices();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceSystemType#getControllers <em>Controllers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Controllers</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceSystemType#getControllers()
	 * @see #getMotionDeviceSystemType()
	 * @generated
	 */
	EReference getMotionDeviceSystemType_Controllers();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceSystemType#getSafetyStates <em>Safety States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Safety States</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceSystemType#getSafetyStates()
	 * @see #getMotionDeviceSystemType()
	 * @generated
	 */
	EReference getMotionDeviceSystemType_SafetyStates();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType <em>Motion Device Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Motion Device Type</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType
	 * @generated
	 */
	EClass getMotionDeviceType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType#getMotionDeviceCategory <em>Motion Device Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Motion Device Category</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType#getMotionDeviceCategory()
	 * @see #getMotionDeviceType()
	 * @generated
	 */
	EAttribute getMotionDeviceType_MotionDeviceCategory();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType#getParameterSetMotionDeviceType <em>Parameter Set Motion Device Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parameter Set Motion Device Type</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType#getParameterSetMotionDeviceType()
	 * @see #getMotionDeviceType()
	 * @generated
	 */
	EReference getMotionDeviceType_ParameterSetMotionDeviceType();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType#getAxes <em>Axes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Axes</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType#getAxes()
	 * @see #getMotionDeviceType()
	 * @generated
	 */
	EReference getMotionDeviceType_Axes();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType#getPowerTrains <em>Power Trains</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Power Trains</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType#getPowerTrains()
	 * @see #getMotionDeviceType()
	 * @generated
	 */
	EReference getMotionDeviceType_PowerTrains();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType#getAdditionalComponents <em>Additional Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Additional Components</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType#getAdditionalComponents()
	 * @see #getMotionDeviceType()
	 * @generated
	 */
	EReference getMotionDeviceType_AdditionalComponents();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType#getFlangeLoad <em>Flange Load</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Flange Load</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType#getFlangeLoad()
	 * @see #getMotionDeviceType()
	 * @generated
	 */
	EReference getMotionDeviceType_FlangeLoad();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisType <em>Axis Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Axis Type</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisType
	 * @generated
	 */
	EClass getAxisType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisType#getMotionProfile <em>Motion Profile</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Motion Profile</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisType#getMotionProfile()
	 * @see #getAxisType()
	 * @generated
	 */
	EAttribute getAxisType_MotionProfile();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisType#getAdditionalLoad <em>Additional Load</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Additional Load</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisType#getAdditionalLoad()
	 * @see #getAxisType()
	 * @generated
	 */
	EReference getAxisType_AdditionalLoad();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisType#getParameterSetAxisType <em>Parameter Set Axis Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parameter Set Axis Type</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisType#getParameterSetAxisType()
	 * @see #getAxisType()
	 * @generated
	 */
	EReference getAxisType_ParameterSetAxisType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ControllerType <em>Controller Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Controller Type</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ControllerType
	 * @generated
	 */
	EClass getControllerType();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ControllerType#getParameterSetControllerType <em>Parameter Set Controller Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parameter Set Controller Type</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ControllerType#getParameterSetControllerType()
	 * @see #getControllerType()
	 * @generated
	 */
	EReference getControllerType_ParameterSetControllerType();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ControllerType#getComponents <em>Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Components</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ControllerType#getComponents()
	 * @see #getControllerType()
	 * @generated
	 */
	EReference getControllerType_Components();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ControllerType#getSoftware <em>Software</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Software</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ControllerType#getSoftware()
	 * @see #getControllerType()
	 * @generated
	 */
	EReference getControllerType_Software();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ControllerType#getTaskControls <em>Task Controls</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Task Controls</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ControllerType#getTaskControls()
	 * @see #getControllerType()
	 * @generated
	 */
	EReference getControllerType_TaskControls();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.TaskControlType <em>Task Control Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Task Control Type</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.TaskControlType
	 * @generated
	 */
	EClass getTaskControlType();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.TaskControlType#getParameterSetTaskContolType <em>Parameter Set Task Contol Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parameter Set Task Contol Type</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.TaskControlType#getParameterSetTaskContolType()
	 * @see #getTaskControlType()
	 * @generated
	 */
	EReference getTaskControlType_ParameterSetTaskContolType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.SafetyStateType <em>Safety State Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Safety State Type</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.SafetyStateType
	 * @generated
	 */
	EClass getSafetyStateType();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.SafetyStateType#getEmergencyStopFunctions <em>Emergency Stop Functions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Emergency Stop Functions</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.SafetyStateType#getEmergencyStopFunctions()
	 * @see #getSafetyStateType()
	 * @generated
	 */
	EReference getSafetyStateType_EmergencyStopFunctions();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.SafetyStateType#getProtectiveStopFunctions <em>Protective Stop Functions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Protective Stop Functions</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.SafetyStateType#getProtectiveStopFunctions()
	 * @see #getSafetyStateType()
	 * @generated
	 */
	EReference getSafetyStateType_ProtectiveStopFunctions();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.SafetyStateType#getParameterSetSafetyStateType <em>Parameter Set Safety State Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parameter Set Safety State Type</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.SafetyStateType#getParameterSetSafetyStateType()
	 * @see #getSafetyStateType()
	 * @generated
	 */
	EReference getSafetyStateType_ParameterSetSafetyStateType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.EmergencyStopFunctionType <em>Emergency Stop Function Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Emergency Stop Function Type</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.EmergencyStopFunctionType
	 * @generated
	 */
	EClass getEmergencyStopFunctionType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.EmergencyStopFunctionType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.EmergencyStopFunctionType#getName()
	 * @see #getEmergencyStopFunctionType()
	 * @generated
	 */
	EAttribute getEmergencyStopFunctionType_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.EmergencyStopFunctionType#isActive <em>Active</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Active</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.EmergencyStopFunctionType#isActive()
	 * @see #getEmergencyStopFunctionType()
	 * @generated
	 */
	EAttribute getEmergencyStopFunctionType_Active();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.EmergencyStopFunctionType#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.EmergencyStopFunctionType#getBase_Class()
	 * @see #getEmergencyStopFunctionType()
	 * @generated
	 */
	EReference getEmergencyStopFunctionType_Base_Class();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ProtectiveStopFunctionType <em>Protective Stop Function Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Protective Stop Function Type</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ProtectiveStopFunctionType
	 * @generated
	 */
	EClass getProtectiveStopFunctionType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ProtectiveStopFunctionType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ProtectiveStopFunctionType#getName()
	 * @see #getProtectiveStopFunctionType()
	 * @generated
	 */
	EAttribute getProtectiveStopFunctionType_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ProtectiveStopFunctionType#isEnabled <em>Enabled</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Enabled</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ProtectiveStopFunctionType#isEnabled()
	 * @see #getProtectiveStopFunctionType()
	 * @generated
	 */
	EAttribute getProtectiveStopFunctionType_Enabled();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ProtectiveStopFunctionType#isActive <em>Active</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Active</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ProtectiveStopFunctionType#isActive()
	 * @see #getProtectiveStopFunctionType()
	 * @generated
	 */
	EAttribute getProtectiveStopFunctionType_Active();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ProtectiveStopFunctionType#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ProtectiveStopFunctionType#getBase_Class()
	 * @see #getProtectiveStopFunctionType()
	 * @generated
	 */
	EReference getProtectiveStopFunctionType_Base_Class();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.References <em>References</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>References</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.References
	 * @generated
	 */
	EClass getReferences();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.References#getBase_Association <em>Base Association</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Association</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.References#getBase_Association()
	 * @see #getReferences()
	 * @generated
	 */
	EReference getReferences_Base_Association();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.References#getBase_Dependency <em>Base Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Dependency</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.References#getBase_Dependency()
	 * @see #getReferences()
	 * @generated
	 */
	EReference getReferences_Base_Dependency();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.HierarchicalReferences <em>Hierarchical References</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hierarchical References</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.HierarchicalReferences
	 * @generated
	 */
	EClass getHierarchicalReferences();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.Controls <em>Controls</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Controls</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.Controls
	 * @generated
	 */
	EClass getControls();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.IsDrivenBy <em>Is Driven By</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Is Driven By</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.IsDrivenBy
	 * @generated
	 */
	EClass getIsDrivenBy();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.Moves <em>Moves</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Moves</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.Moves
	 * @generated
	 */
	EClass getMoves();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.Requires <em>Requires</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Requires</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.Requires
	 * @generated
	 */
	EClass getRequires();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.NonHierarchicalReferences <em>Non Hierarchical References</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Non Hierarchical References</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.NonHierarchicalReferences
	 * @generated
	 */
	EClass getNonHierarchicalReferences();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.HasSafetyStates <em>Has Safety States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Has Safety States</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.HasSafetyStates
	 * @generated
	 */
	EClass getHasSafetyStates();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.HasSlaves <em>Has Slaves</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Has Slaves</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.HasSlaves
	 * @generated
	 */
	EClass getHasSlaves();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.IsConnectedTo <em>Is Connected To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Is Connected To</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.IsConnectedTo
	 * @generated
	 */
	EClass getIsConnectedTo();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ExecutionModeEnumeration <em>Execution Mode Enumeration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Execution Mode Enumeration</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ExecutionModeEnumeration
	 * @generated
	 */
	EEnum getExecutionModeEnumeration();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OperationalModeEnumeration <em>Operational Mode Enumeration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Operational Mode Enumeration</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OperationalModeEnumeration
	 * @generated
	 */
	EEnum getOperationalModeEnumeration();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisMotionProfileEnumeration <em>Axis Motion Profile Enumeration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Axis Motion Profile Enumeration</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisMotionProfileEnumeration
	 * @generated
	 */
	EEnum getAxisMotionProfileEnumeration();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisStateEnumeration <em>Axis State Enumeration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Axis State Enumeration</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisStateEnumeration
	 * @generated
	 */
	EEnum getAxisStateEnumeration();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ModeEnum <em>Mode Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Mode Enum</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ModeEnum
	 * @generated
	 */
	EEnum getModeEnum();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceCategoryEnum <em>Motion Device Category Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Motion Device Category Enum</em>'.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceCategoryEnum
	 * @generated
	 */
	EEnum getMotionDeviceCategoryEnum();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OPCUARoboticsProfileFactory getOPCUARoboticsProfileFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.LoadTypeImpl <em>Load Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.LoadTypeImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getLoadType()
		 * @generated
		 */
		EClass LOAD_TYPE = eINSTANCE.getLoadType();

		/**
		 * The meta object literal for the '<em><b>Mass</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOAD_TYPE__MASS = eINSTANCE.getLoadType_Mass();

		/**
		 * The meta object literal for the '<em><b>Center Of Mass</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOAD_TYPE__CENTER_OF_MASS = eINSTANCE.getLoadType_CenterOfMass();

		/**
		 * The meta object literal for the '<em><b>Inertia</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOAD_TYPE__INERTIA = eINSTANCE.getLoadType_Inertia();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetMotorTypeImpl <em>Parameter Set Motor Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetMotorTypeImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getParameterSetMotorType()
		 * @generated
		 */
		EClass PARAMETER_SET_MOTOR_TYPE = eINSTANCE.getParameterSetMotorType();

		/**
		 * The meta object literal for the '<em><b>Brake Released</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_MOTOR_TYPE__BRAKE_RELEASED = eINSTANCE.getParameterSetMotorType_BrakeReleased();

		/**
		 * The meta object literal for the '<em><b>Effective Load Rate</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_MOTOR_TYPE__EFFECTIVE_LOAD_RATE = eINSTANCE.getParameterSetMotorType_EffectiveLoadRate();

		/**
		 * The meta object literal for the '<em><b>Motor Temperature</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_MOTOR_TYPE__MOTOR_TEMPERATURE = eINSTANCE.getParameterSetMotorType_MotorTemperature();

		/**
		 * The meta object literal for the '<em><b>Not CS Motor Intensity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_MOTOR_TYPE__NOT_CS_MOTOR_INTENSITY = eINSTANCE.getParameterSetMotorType_NotCS_MotorIntensity();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetControllerTypeImpl <em>Parameter Set Controller Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetControllerTypeImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getParameterSetControllerType()
		 * @generated
		 */
		EClass PARAMETER_SET_CONTROLLER_TYPE = eINSTANCE.getParameterSetControllerType();

		/**
		 * The meta object literal for the '<em><b>Total Power On Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_CONTROLLER_TYPE__TOTAL_POWER_ON_TIME = eINSTANCE.getParameterSetControllerType_TotalPowerOnTime();

		/**
		 * The meta object literal for the '<em><b>Start Up Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_CONTROLLER_TYPE__START_UP_TIME = eINSTANCE.getParameterSetControllerType_StartUpTime();

		/**
		 * The meta object literal for the '<em><b>Ups State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_CONTROLLER_TYPE__UPS_STATE = eINSTANCE.getParameterSetControllerType_UpsState();

		/**
		 * The meta object literal for the '<em><b>Total Energy Consumption</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_CONTROLLER_TYPE__TOTAL_ENERGY_CONSUMPTION = eINSTANCE.getParameterSetControllerType_TotalEnergyConsumption();

		/**
		 * The meta object literal for the '<em><b>Cabinet Fan Speed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_CONTROLLER_TYPE__CABINET_FAN_SPEED = eINSTANCE.getParameterSetControllerType_CabinetFanSpeed();

		/**
		 * The meta object literal for the '<em><b>CPU Fan Speed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_CONTROLLER_TYPE__CPU_FAN_SPEED = eINSTANCE.getParameterSetControllerType_CPUFanSpeed();

		/**
		 * The meta object literal for the '<em><b>Input Voltage</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_CONTROLLER_TYPE__INPUT_VOLTAGE = eINSTANCE.getParameterSetControllerType_InputVoltage();

		/**
		 * The meta object literal for the '<em><b>Temperature</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_CONTROLLER_TYPE__TEMPERATURE = eINSTANCE.getParameterSetControllerType_Temperature();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetTaskControlTypeImpl <em>Parameter Set Task Control Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetTaskControlTypeImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getParameterSetTaskControlType()
		 * @generated
		 */
		EClass PARAMETER_SET_TASK_CONTROL_TYPE = eINSTANCE.getParameterSetTaskControlType();

		/**
		 * The meta object literal for the '<em><b>Execution Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_TASK_CONTROL_TYPE__EXECUTION_MODE = eINSTANCE.getParameterSetTaskControlType_ExecutionMode();

		/**
		 * The meta object literal for the '<em><b>Task Program Loaded</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_TASK_CONTROL_TYPE__TASK_PROGRAM_LOADED = eINSTANCE.getParameterSetTaskControlType_TaskProgramLoaded();

		/**
		 * The meta object literal for the '<em><b>Task Program Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_TASK_CONTROL_TYPE__TASK_PROGRAM_NAME = eINSTANCE.getParameterSetTaskControlType_TaskProgramName();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetSafetyStateTypeImpl <em>Parameter Set Safety State Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetSafetyStateTypeImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getParameterSetSafetyStateType()
		 * @generated
		 */
		EClass PARAMETER_SET_SAFETY_STATE_TYPE = eINSTANCE.getParameterSetSafetyStateType();

		/**
		 * The meta object literal for the '<em><b>Emergency Stop</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_SAFETY_STATE_TYPE__EMERGENCY_STOP = eINSTANCE.getParameterSetSafetyStateType_EmergencyStop();

		/**
		 * The meta object literal for the '<em><b>Operational Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_SAFETY_STATE_TYPE__OPERATIONAL_MODE = eINSTANCE.getParameterSetSafetyStateType_OperationalMode();

		/**
		 * The meta object literal for the '<em><b>Protective Stop</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_SAFETY_STATE_TYPE__PROTECTIVE_STOP = eINSTANCE.getParameterSetSafetyStateType_ProtectiveStop();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.GearTypeImpl <em>Gear Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.GearTypeImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getGearType()
		 * @generated
		 */
		EClass GEAR_TYPE = eINSTANCE.getGearType();

		/**
		 * The meta object literal for the '<em><b>Gear Ratio</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GEAR_TYPE__GEAR_RATIO = eINSTANCE.getGearType_GearRatio();

		/**
		 * The meta object literal for the '<em><b>Pitch</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GEAR_TYPE__PITCH = eINSTANCE.getGearType_Pitch();

		/**
		 * The meta object literal for the '<em><b>Is Connected To</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GEAR_TYPE__IS_CONNECTED_TO = eINSTANCE.getGearType_IsConnectedTo();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MotorTypeImpl <em>Motor Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MotorTypeImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getMotorType()
		 * @generated
		 */
		EClass MOTOR_TYPE = eINSTANCE.getMotorType();

		/**
		 * The meta object literal for the '<em><b>Parameter Set Motor Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOTOR_TYPE__PARAMETER_SET_MOTOR_TYPE = eINSTANCE.getMotorType_ParameterSetMotorType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetAxisTypeImpl <em>Parameter Set Axis Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetAxisTypeImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getParameterSetAxisType()
		 * @generated
		 */
		EClass PARAMETER_SET_AXIS_TYPE = eINSTANCE.getParameterSetAxisType();

		/**
		 * The meta object literal for the '<em><b>Actual Acceleration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_AXIS_TYPE__ACTUAL_ACCELERATION = eINSTANCE.getParameterSetAxisType_ActualAcceleration();

		/**
		 * The meta object literal for the '<em><b>Actual Position</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_AXIS_TYPE__ACTUAL_POSITION = eINSTANCE.getParameterSetAxisType_ActualPosition();

		/**
		 * The meta object literal for the '<em><b>Actual Speed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_AXIS_TYPE__ACTUAL_SPEED = eINSTANCE.getParameterSetAxisType_ActualSpeed();

		/**
		 * The meta object literal for the '<em><b>Not CS Axis State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_AXIS_TYPE__NOT_CS_AXIS_STATE = eINSTANCE.getParameterSetAxisType_NotCS_AxisState();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetMotionDeviceTypeImpl <em>Parameter Set Motion Device Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetMotionDeviceTypeImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getParameterSetMotionDeviceType()
		 * @generated
		 */
		EClass PARAMETER_SET_MOTION_DEVICE_TYPE = eINSTANCE.getParameterSetMotionDeviceType();

		/**
		 * The meta object literal for the '<em><b>On Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_MOTION_DEVICE_TYPE__ON_PATH = eINSTANCE.getParameterSetMotionDeviceType_OnPath();

		/**
		 * The meta object literal for the '<em><b>In Control</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_MOTION_DEVICE_TYPE__IN_CONTROL = eINSTANCE.getParameterSetMotionDeviceType_InControl();

		/**
		 * The meta object literal for the '<em><b>Speed Override</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_MOTION_DEVICE_TYPE__SPEED_OVERRIDE = eINSTANCE.getParameterSetMotionDeviceType_SpeedOverride();

		/**
		 * The meta object literal for the '<em><b>Not CS is Power Button Pressed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_IS_POWER_BUTTON_PRESSED = eINSTANCE.getParameterSetMotionDeviceType_NotCS_isPowerButtonPressed();

		/**
		 * The meta object literal for the '<em><b>Not CS Robot Intensity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_ROBOT_INTENSITY = eINSTANCE.getParameterSetMotionDeviceType_NotCS__RobotIntensity();

		/**
		 * The meta object literal for the '<em><b>Not CS is Teach Button Pressed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_IS_TEACH_BUTTON_PRESSED = eINSTANCE.getParameterSetMotionDeviceType_NotCS_isTeachButtonPressed();

		/**
		 * The meta object literal for the '<em><b>Not CS is Power On Robot</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_IS_POWER_ON_ROBOT = eINSTANCE.getParameterSetMotionDeviceType_NotCS_isPowerOnRobot();

		/**
		 * The meta object literal for the '<em><b>Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_SET_MOTION_DEVICE_TYPE__MODE = eINSTANCE.getParameterSetMotionDeviceType_Mode();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.PowerTrainTypeImpl <em>Power Train Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.PowerTrainTypeImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getPowerTrainType()
		 * @generated
		 */
		EClass POWER_TRAIN_TYPE = eINSTANCE.getPowerTrainType();

		/**
		 * The meta object literal for the '<em><b>Motor</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POWER_TRAIN_TYPE__MOTOR = eINSTANCE.getPowerTrainType_Motor();

		/**
		 * The meta object literal for the '<em><b>Gear</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POWER_TRAIN_TYPE__GEAR = eINSTANCE.getPowerTrainType_Gear();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MotionDeviceSystemTypeImpl <em>Motion Device System Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MotionDeviceSystemTypeImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getMotionDeviceSystemType()
		 * @generated
		 */
		EClass MOTION_DEVICE_SYSTEM_TYPE = eINSTANCE.getMotionDeviceSystemType();

		/**
		 * The meta object literal for the '<em><b>Motion Devices</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOTION_DEVICE_SYSTEM_TYPE__MOTION_DEVICES = eINSTANCE.getMotionDeviceSystemType_MotionDevices();

		/**
		 * The meta object literal for the '<em><b>Controllers</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOTION_DEVICE_SYSTEM_TYPE__CONTROLLERS = eINSTANCE.getMotionDeviceSystemType_Controllers();

		/**
		 * The meta object literal for the '<em><b>Safety States</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOTION_DEVICE_SYSTEM_TYPE__SAFETY_STATES = eINSTANCE.getMotionDeviceSystemType_SafetyStates();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MotionDeviceTypeImpl <em>Motion Device Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MotionDeviceTypeImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getMotionDeviceType()
		 * @generated
		 */
		EClass MOTION_DEVICE_TYPE = eINSTANCE.getMotionDeviceType();

		/**
		 * The meta object literal for the '<em><b>Motion Device Category</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOTION_DEVICE_TYPE__MOTION_DEVICE_CATEGORY = eINSTANCE.getMotionDeviceType_MotionDeviceCategory();

		/**
		 * The meta object literal for the '<em><b>Parameter Set Motion Device Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOTION_DEVICE_TYPE__PARAMETER_SET_MOTION_DEVICE_TYPE = eINSTANCE.getMotionDeviceType_ParameterSetMotionDeviceType();

		/**
		 * The meta object literal for the '<em><b>Axes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOTION_DEVICE_TYPE__AXES = eINSTANCE.getMotionDeviceType_Axes();

		/**
		 * The meta object literal for the '<em><b>Power Trains</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOTION_DEVICE_TYPE__POWER_TRAINS = eINSTANCE.getMotionDeviceType_PowerTrains();

		/**
		 * The meta object literal for the '<em><b>Additional Components</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOTION_DEVICE_TYPE__ADDITIONAL_COMPONENTS = eINSTANCE.getMotionDeviceType_AdditionalComponents();

		/**
		 * The meta object literal for the '<em><b>Flange Load</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOTION_DEVICE_TYPE__FLANGE_LOAD = eINSTANCE.getMotionDeviceType_FlangeLoad();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.AxisTypeImpl <em>Axis Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.AxisTypeImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getAxisType()
		 * @generated
		 */
		EClass AXIS_TYPE = eINSTANCE.getAxisType();

		/**
		 * The meta object literal for the '<em><b>Motion Profile</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AXIS_TYPE__MOTION_PROFILE = eINSTANCE.getAxisType_MotionProfile();

		/**
		 * The meta object literal for the '<em><b>Additional Load</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AXIS_TYPE__ADDITIONAL_LOAD = eINSTANCE.getAxisType_AdditionalLoad();

		/**
		 * The meta object literal for the '<em><b>Parameter Set Axis Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AXIS_TYPE__PARAMETER_SET_AXIS_TYPE = eINSTANCE.getAxisType_ParameterSetAxisType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ControllerTypeImpl <em>Controller Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ControllerTypeImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getControllerType()
		 * @generated
		 */
		EClass CONTROLLER_TYPE = eINSTANCE.getControllerType();

		/**
		 * The meta object literal for the '<em><b>Parameter Set Controller Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROLLER_TYPE__PARAMETER_SET_CONTROLLER_TYPE = eINSTANCE.getControllerType_ParameterSetControllerType();

		/**
		 * The meta object literal for the '<em><b>Components</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROLLER_TYPE__COMPONENTS = eINSTANCE.getControllerType_Components();

		/**
		 * The meta object literal for the '<em><b>Software</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROLLER_TYPE__SOFTWARE = eINSTANCE.getControllerType_Software();

		/**
		 * The meta object literal for the '<em><b>Task Controls</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROLLER_TYPE__TASK_CONTROLS = eINSTANCE.getControllerType_TaskControls();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.TaskControlTypeImpl <em>Task Control Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.TaskControlTypeImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getTaskControlType()
		 * @generated
		 */
		EClass TASK_CONTROL_TYPE = eINSTANCE.getTaskControlType();

		/**
		 * The meta object literal for the '<em><b>Parameter Set Task Contol Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK_CONTROL_TYPE__PARAMETER_SET_TASK_CONTOL_TYPE = eINSTANCE.getTaskControlType_ParameterSetTaskContolType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.SafetyStateTypeImpl <em>Safety State Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.SafetyStateTypeImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getSafetyStateType()
		 * @generated
		 */
		EClass SAFETY_STATE_TYPE = eINSTANCE.getSafetyStateType();

		/**
		 * The meta object literal for the '<em><b>Emergency Stop Functions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SAFETY_STATE_TYPE__EMERGENCY_STOP_FUNCTIONS = eINSTANCE.getSafetyStateType_EmergencyStopFunctions();

		/**
		 * The meta object literal for the '<em><b>Protective Stop Functions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SAFETY_STATE_TYPE__PROTECTIVE_STOP_FUNCTIONS = eINSTANCE.getSafetyStateType_ProtectiveStopFunctions();

		/**
		 * The meta object literal for the '<em><b>Parameter Set Safety State Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SAFETY_STATE_TYPE__PARAMETER_SET_SAFETY_STATE_TYPE = eINSTANCE.getSafetyStateType_ParameterSetSafetyStateType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.EmergencyStopFunctionTypeImpl <em>Emergency Stop Function Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.EmergencyStopFunctionTypeImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getEmergencyStopFunctionType()
		 * @generated
		 */
		EClass EMERGENCY_STOP_FUNCTION_TYPE = eINSTANCE.getEmergencyStopFunctionType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EMERGENCY_STOP_FUNCTION_TYPE__NAME = eINSTANCE.getEmergencyStopFunctionType_Name();

		/**
		 * The meta object literal for the '<em><b>Active</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EMERGENCY_STOP_FUNCTION_TYPE__ACTIVE = eINSTANCE.getEmergencyStopFunctionType_Active();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EMERGENCY_STOP_FUNCTION_TYPE__BASE_CLASS = eINSTANCE.getEmergencyStopFunctionType_Base_Class();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ProtectiveStopFunctionTypeImpl <em>Protective Stop Function Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ProtectiveStopFunctionTypeImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getProtectiveStopFunctionType()
		 * @generated
		 */
		EClass PROTECTIVE_STOP_FUNCTION_TYPE = eINSTANCE.getProtectiveStopFunctionType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROTECTIVE_STOP_FUNCTION_TYPE__NAME = eINSTANCE.getProtectiveStopFunctionType_Name();

		/**
		 * The meta object literal for the '<em><b>Enabled</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROTECTIVE_STOP_FUNCTION_TYPE__ENABLED = eINSTANCE.getProtectiveStopFunctionType_Enabled();

		/**
		 * The meta object literal for the '<em><b>Active</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROTECTIVE_STOP_FUNCTION_TYPE__ACTIVE = eINSTANCE.getProtectiveStopFunctionType_Active();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROTECTIVE_STOP_FUNCTION_TYPE__BASE_CLASS = eINSTANCE.getProtectiveStopFunctionType_Base_Class();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ReferencesImpl <em>References</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ReferencesImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getReferences()
		 * @generated
		 */
		EClass REFERENCES = eINSTANCE.getReferences();

		/**
		 * The meta object literal for the '<em><b>Base Association</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCES__BASE_ASSOCIATION = eINSTANCE.getReferences_Base_Association();

		/**
		 * The meta object literal for the '<em><b>Base Dependency</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCES__BASE_DEPENDENCY = eINSTANCE.getReferences_Base_Dependency();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.HierarchicalReferencesImpl <em>Hierarchical References</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.HierarchicalReferencesImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getHierarchicalReferences()
		 * @generated
		 */
		EClass HIERARCHICAL_REFERENCES = eINSTANCE.getHierarchicalReferences();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ControlsImpl <em>Controls</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ControlsImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getControls()
		 * @generated
		 */
		EClass CONTROLS = eINSTANCE.getControls();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.IsDrivenByImpl <em>Is Driven By</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.IsDrivenByImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getIsDrivenBy()
		 * @generated
		 */
		EClass IS_DRIVEN_BY = eINSTANCE.getIsDrivenBy();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MovesImpl <em>Moves</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MovesImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getMoves()
		 * @generated
		 */
		EClass MOVES = eINSTANCE.getMoves();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.RequiresImpl <em>Requires</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.RequiresImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getRequires()
		 * @generated
		 */
		EClass REQUIRES = eINSTANCE.getRequires();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.NonHierarchicalReferencesImpl <em>Non Hierarchical References</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.NonHierarchicalReferencesImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getNonHierarchicalReferences()
		 * @generated
		 */
		EClass NON_HIERARCHICAL_REFERENCES = eINSTANCE.getNonHierarchicalReferences();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.HasSafetyStatesImpl <em>Has Safety States</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.HasSafetyStatesImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getHasSafetyStates()
		 * @generated
		 */
		EClass HAS_SAFETY_STATES = eINSTANCE.getHasSafetyStates();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.HasSlavesImpl <em>Has Slaves</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.HasSlavesImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getHasSlaves()
		 * @generated
		 */
		EClass HAS_SLAVES = eINSTANCE.getHasSlaves();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.IsConnectedToImpl <em>Is Connected To</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.IsConnectedToImpl
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getIsConnectedTo()
		 * @generated
		 */
		EClass IS_CONNECTED_TO = eINSTANCE.getIsConnectedTo();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ExecutionModeEnumeration <em>Execution Mode Enumeration</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ExecutionModeEnumeration
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getExecutionModeEnumeration()
		 * @generated
		 */
		EEnum EXECUTION_MODE_ENUMERATION = eINSTANCE.getExecutionModeEnumeration();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OperationalModeEnumeration <em>Operational Mode Enumeration</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OperationalModeEnumeration
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getOperationalModeEnumeration()
		 * @generated
		 */
		EEnum OPERATIONAL_MODE_ENUMERATION = eINSTANCE.getOperationalModeEnumeration();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisMotionProfileEnumeration <em>Axis Motion Profile Enumeration</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisMotionProfileEnumeration
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getAxisMotionProfileEnumeration()
		 * @generated
		 */
		EEnum AXIS_MOTION_PROFILE_ENUMERATION = eINSTANCE.getAxisMotionProfileEnumeration();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisStateEnumeration <em>Axis State Enumeration</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisStateEnumeration
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getAxisStateEnumeration()
		 * @generated
		 */
		EEnum AXIS_STATE_ENUMERATION = eINSTANCE.getAxisStateEnumeration();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ModeEnum <em>Mode Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ModeEnum
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getModeEnum()
		 * @generated
		 */
		EEnum MODE_ENUM = eINSTANCE.getModeEnum();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceCategoryEnum <em>Motion Device Category Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceCategoryEnum
		 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfilePackageImpl#getMotionDeviceCategoryEnum()
		 * @generated
		 */
		EEnum MOTION_DEVICE_CATEGORY_ENUM = eINSTANCE.getMotionDeviceCategoryEnum();

	}

} //OPCUARoboticsProfilePackage
