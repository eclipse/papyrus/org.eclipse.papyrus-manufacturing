/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.*;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.ComponentType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.ITagNameplateType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType;
import org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType;

import org.eclipse.papyrus.opcua.opcuaprofile.BaseInterfaceType;
import org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage
 * @generated
 */
public class OPCUARoboticsProfileAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static OPCUARoboticsProfilePackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPCUARoboticsProfileAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = OPCUARoboticsProfilePackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OPCUARoboticsProfileSwitch<Adapter> modelSwitch =
		new OPCUARoboticsProfileSwitch<Adapter>() {
			@Override
			public Adapter caseLoadType(LoadType object) {
				return createLoadTypeAdapter();
			}
			@Override
			public Adapter caseParameterSetMotorType(ParameterSetMotorType object) {
				return createParameterSetMotorTypeAdapter();
			}
			@Override
			public Adapter caseParameterSetControllerType(ParameterSetControllerType object) {
				return createParameterSetControllerTypeAdapter();
			}
			@Override
			public Adapter caseParameterSetTaskControlType(ParameterSetTaskControlType object) {
				return createParameterSetTaskControlTypeAdapter();
			}
			@Override
			public Adapter caseParameterSetSafetyStateType(ParameterSetSafetyStateType object) {
				return createParameterSetSafetyStateTypeAdapter();
			}
			@Override
			public Adapter caseGearType(GearType object) {
				return createGearTypeAdapter();
			}
			@Override
			public Adapter caseMotorType(MotorType object) {
				return createMotorTypeAdapter();
			}
			@Override
			public Adapter caseParameterSetAxisType(ParameterSetAxisType object) {
				return createParameterSetAxisTypeAdapter();
			}
			@Override
			public Adapter caseParameterSetMotionDeviceType(ParameterSetMotionDeviceType object) {
				return createParameterSetMotionDeviceTypeAdapter();
			}
			@Override
			public Adapter casePowerTrainType(PowerTrainType object) {
				return createPowerTrainTypeAdapter();
			}
			@Override
			public Adapter caseMotionDeviceSystemType(MotionDeviceSystemType object) {
				return createMotionDeviceSystemTypeAdapter();
			}
			@Override
			public Adapter caseMotionDeviceType(MotionDeviceType object) {
				return createMotionDeviceTypeAdapter();
			}
			@Override
			public Adapter caseAxisType(AxisType object) {
				return createAxisTypeAdapter();
			}
			@Override
			public Adapter caseControllerType(ControllerType object) {
				return createControllerTypeAdapter();
			}
			@Override
			public Adapter caseTaskControlType(TaskControlType object) {
				return createTaskControlTypeAdapter();
			}
			@Override
			public Adapter caseSafetyStateType(SafetyStateType object) {
				return createSafetyStateTypeAdapter();
			}
			@Override
			public Adapter caseEmergencyStopFunctionType(EmergencyStopFunctionType object) {
				return createEmergencyStopFunctionTypeAdapter();
			}
			@Override
			public Adapter caseProtectiveStopFunctionType(ProtectiveStopFunctionType object) {
				return createProtectiveStopFunctionTypeAdapter();
			}
			@Override
			public Adapter caseReferences(References object) {
				return createReferencesAdapter();
			}
			@Override
			public Adapter caseHierarchicalReferences(HierarchicalReferences object) {
				return createHierarchicalReferencesAdapter();
			}
			@Override
			public Adapter caseControls(Controls object) {
				return createControlsAdapter();
			}
			@Override
			public Adapter caseIsDrivenBy(IsDrivenBy object) {
				return createIsDrivenByAdapter();
			}
			@Override
			public Adapter caseMoves(Moves object) {
				return createMovesAdapter();
			}
			@Override
			public Adapter caseRequires(Requires object) {
				return createRequiresAdapter();
			}
			@Override
			public Adapter caseNonHierarchicalReferences(NonHierarchicalReferences object) {
				return createNonHierarchicalReferencesAdapter();
			}
			@Override
			public Adapter caseHasSafetyStates(HasSafetyStates object) {
				return createHasSafetyStatesAdapter();
			}
			@Override
			public Adapter caseHasSlaves(HasSlaves object) {
				return createHasSlavesAdapter();
			}
			@Override
			public Adapter caseIsConnectedTo(IsConnectedTo object) {
				return createIsConnectedToAdapter();
			}
			@Override
			public Adapter caseBaseObjectType(BaseObjectType object) {
				return createBaseObjectTypeAdapter();
			}
			@Override
			public Adapter caseBaseInterfaceType(BaseInterfaceType object) {
				return createBaseInterfaceTypeAdapter();
			}
			@Override
			public Adapter caseIVendorNameplateType(IVendorNameplateType object) {
				return createIVendorNameplateTypeAdapter();
			}
			@Override
			public Adapter caseTopologyElementType(TopologyElementType object) {
				return createTopologyElementTypeAdapter();
			}
			@Override
			public Adapter caseITagNameplateType(ITagNameplateType object) {
				return createITagNameplateTypeAdapter();
			}
			@Override
			public Adapter caseComponentType(ComponentType object) {
				return createComponentTypeAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.LoadType <em>Load Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.LoadType
	 * @generated
	 */
	public Adapter createLoadTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotorType <em>Parameter Set Motor Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotorType
	 * @generated
	 */
	public Adapter createParameterSetMotorTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType <em>Parameter Set Controller Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType
	 * @generated
	 */
	public Adapter createParameterSetControllerTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetTaskControlType <em>Parameter Set Task Control Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetTaskControlType
	 * @generated
	 */
	public Adapter createParameterSetTaskControlTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetSafetyStateType <em>Parameter Set Safety State Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetSafetyStateType
	 * @generated
	 */
	public Adapter createParameterSetSafetyStateTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.GearType <em>Gear Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.GearType
	 * @generated
	 */
	public Adapter createGearTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotorType <em>Motor Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotorType
	 * @generated
	 */
	public Adapter createMotorTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetAxisType <em>Parameter Set Axis Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetAxisType
	 * @generated
	 */
	public Adapter createParameterSetAxisTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType <em>Parameter Set Motion Device Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType
	 * @generated
	 */
	public Adapter createParameterSetMotionDeviceTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.PowerTrainType <em>Power Train Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.PowerTrainType
	 * @generated
	 */
	public Adapter createPowerTrainTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceSystemType <em>Motion Device System Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceSystemType
	 * @generated
	 */
	public Adapter createMotionDeviceSystemTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType <em>Motion Device Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType
	 * @generated
	 */
	public Adapter createMotionDeviceTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisType <em>Axis Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisType
	 * @generated
	 */
	public Adapter createAxisTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ControllerType <em>Controller Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ControllerType
	 * @generated
	 */
	public Adapter createControllerTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.TaskControlType <em>Task Control Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.TaskControlType
	 * @generated
	 */
	public Adapter createTaskControlTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.SafetyStateType <em>Safety State Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.SafetyStateType
	 * @generated
	 */
	public Adapter createSafetyStateTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.EmergencyStopFunctionType <em>Emergency Stop Function Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.EmergencyStopFunctionType
	 * @generated
	 */
	public Adapter createEmergencyStopFunctionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ProtectiveStopFunctionType <em>Protective Stop Function Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ProtectiveStopFunctionType
	 * @generated
	 */
	public Adapter createProtectiveStopFunctionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.References <em>References</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.References
	 * @generated
	 */
	public Adapter createReferencesAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.HierarchicalReferences <em>Hierarchical References</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.HierarchicalReferences
	 * @generated
	 */
	public Adapter createHierarchicalReferencesAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.Controls <em>Controls</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.Controls
	 * @generated
	 */
	public Adapter createControlsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.IsDrivenBy <em>Is Driven By</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.IsDrivenBy
	 * @generated
	 */
	public Adapter createIsDrivenByAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.Moves <em>Moves</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.Moves
	 * @generated
	 */
	public Adapter createMovesAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.Requires <em>Requires</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.Requires
	 * @generated
	 */
	public Adapter createRequiresAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.NonHierarchicalReferences <em>Non Hierarchical References</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.NonHierarchicalReferences
	 * @generated
	 */
	public Adapter createNonHierarchicalReferencesAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.HasSafetyStates <em>Has Safety States</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.HasSafetyStates
	 * @generated
	 */
	public Adapter createHasSafetyStatesAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.HasSlaves <em>Has Slaves</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.HasSlaves
	 * @generated
	 */
	public Adapter createHasSlavesAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.IsConnectedTo <em>Is Connected To</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.IsConnectedTo
	 * @generated
	 */
	public Adapter createIsConnectedToAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType <em>Base Object Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType
	 * @generated
	 */
	public Adapter createBaseObjectTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseInterfaceType <em>Base Interface Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BaseInterfaceType
	 * @generated
	 */
	public Adapter createBaseInterfaceTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType <em>IVendor Nameplate Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.IVendorNameplateType
	 * @generated
	 */
	public Adapter createIVendorNameplateTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType <em>Topology Element Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.TopologyElementType
	 * @generated
	 */
	public Adapter createTopologyElementTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ITagNameplateType <em>ITag Nameplate Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.ITagNameplateType
	 * @generated
	 */
	public Adapter createITagNameplateTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.di.opcuadiprofile.ComponentType <em>Component Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.ComponentType
	 * @generated
	 */
	public Adapter createComponentTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //OPCUARoboticsProfileAdapterFactory
