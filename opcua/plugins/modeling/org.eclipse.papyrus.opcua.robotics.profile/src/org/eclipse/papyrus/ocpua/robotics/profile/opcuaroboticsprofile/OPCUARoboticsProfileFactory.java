/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage
 * @generated
 */
public interface OPCUARoboticsProfileFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OPCUARoboticsProfileFactory eINSTANCE = org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.OPCUARoboticsProfileFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Load Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Load Type</em>'.
	 * @generated
	 */
	LoadType createLoadType();

	/**
	 * Returns a new object of class '<em>Parameter Set Motor Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parameter Set Motor Type</em>'.
	 * @generated
	 */
	ParameterSetMotorType createParameterSetMotorType();

	/**
	 * Returns a new object of class '<em>Parameter Set Controller Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parameter Set Controller Type</em>'.
	 * @generated
	 */
	ParameterSetControllerType createParameterSetControllerType();

	/**
	 * Returns a new object of class '<em>Parameter Set Task Control Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parameter Set Task Control Type</em>'.
	 * @generated
	 */
	ParameterSetTaskControlType createParameterSetTaskControlType();

	/**
	 * Returns a new object of class '<em>Parameter Set Safety State Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parameter Set Safety State Type</em>'.
	 * @generated
	 */
	ParameterSetSafetyStateType createParameterSetSafetyStateType();

	/**
	 * Returns a new object of class '<em>Gear Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Gear Type</em>'.
	 * @generated
	 */
	GearType createGearType();

	/**
	 * Returns a new object of class '<em>Motor Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Motor Type</em>'.
	 * @generated
	 */
	MotorType createMotorType();

	/**
	 * Returns a new object of class '<em>Parameter Set Axis Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parameter Set Axis Type</em>'.
	 * @generated
	 */
	ParameterSetAxisType createParameterSetAxisType();

	/**
	 * Returns a new object of class '<em>Parameter Set Motion Device Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parameter Set Motion Device Type</em>'.
	 * @generated
	 */
	ParameterSetMotionDeviceType createParameterSetMotionDeviceType();

	/**
	 * Returns a new object of class '<em>Power Train Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Power Train Type</em>'.
	 * @generated
	 */
	PowerTrainType createPowerTrainType();

	/**
	 * Returns a new object of class '<em>Motion Device System Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Motion Device System Type</em>'.
	 * @generated
	 */
	MotionDeviceSystemType createMotionDeviceSystemType();

	/**
	 * Returns a new object of class '<em>Motion Device Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Motion Device Type</em>'.
	 * @generated
	 */
	MotionDeviceType createMotionDeviceType();

	/**
	 * Returns a new object of class '<em>Axis Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Axis Type</em>'.
	 * @generated
	 */
	AxisType createAxisType();

	/**
	 * Returns a new object of class '<em>Controller Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Controller Type</em>'.
	 * @generated
	 */
	ControllerType createControllerType();

	/**
	 * Returns a new object of class '<em>Task Control Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Task Control Type</em>'.
	 * @generated
	 */
	TaskControlType createTaskControlType();

	/**
	 * Returns a new object of class '<em>Safety State Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Safety State Type</em>'.
	 * @generated
	 */
	SafetyStateType createSafetyStateType();

	/**
	 * Returns a new object of class '<em>Emergency Stop Function Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Emergency Stop Function Type</em>'.
	 * @generated
	 */
	EmergencyStopFunctionType createEmergencyStopFunctionType();

	/**
	 * Returns a new object of class '<em>Protective Stop Function Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Protective Stop Function Type</em>'.
	 * @generated
	 */
	ProtectiveStopFunctionType createProtectiveStopFunctionType();

	/**
	 * Returns a new object of class '<em>References</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>References</em>'.
	 * @generated
	 */
	References createReferences();

	/**
	 * Returns a new object of class '<em>Hierarchical References</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Hierarchical References</em>'.
	 * @generated
	 */
	HierarchicalReferences createHierarchicalReferences();

	/**
	 * Returns a new object of class '<em>Controls</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Controls</em>'.
	 * @generated
	 */
	Controls createControls();

	/**
	 * Returns a new object of class '<em>Is Driven By</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Driven By</em>'.
	 * @generated
	 */
	IsDrivenBy createIsDrivenBy();

	/**
	 * Returns a new object of class '<em>Moves</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Moves</em>'.
	 * @generated
	 */
	Moves createMoves();

	/**
	 * Returns a new object of class '<em>Requires</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Requires</em>'.
	 * @generated
	 */
	Requires createRequires();

	/**
	 * Returns a new object of class '<em>Non Hierarchical References</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Non Hierarchical References</em>'.
	 * @generated
	 */
	NonHierarchicalReferences createNonHierarchicalReferences();

	/**
	 * Returns a new object of class '<em>Has Safety States</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Safety States</em>'.
	 * @generated
	 */
	HasSafetyStates createHasSafetyStates();

	/**
	 * Returns a new object of class '<em>Has Slaves</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Slaves</em>'.
	 * @generated
	 */
	HasSlaves createHasSlaves();

	/**
	 * Returns a new object of class '<em>Is Connected To</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Connected To</em>'.
	 * @generated
	 */
	IsConnectedTo createIsConnectedTo();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	OPCUARoboticsProfilePackage getOPCUARoboticsProfilePackage();

} //OPCUARoboticsProfileFactory
