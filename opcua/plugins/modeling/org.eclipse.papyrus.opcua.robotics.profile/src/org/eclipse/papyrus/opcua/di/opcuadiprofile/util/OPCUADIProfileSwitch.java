/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.di.opcuadiprofile.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.*;

import org.eclipse.papyrus.opcua.opcuaprofile.BaseInterfaceType;
import org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType;
import org.eclipse.papyrus.opcua.opcuaprofile.FolderType;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage
 * @generated
 */
public class OPCUADIProfileSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static OPCUADIProfilePackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPCUADIProfileSwitch() {
		if (modelPackage == null) {
			modelPackage = OPCUADIProfilePackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case OPCUADIProfilePackage.DEVICE_TYPE: {
				DeviceType deviceType = (DeviceType)theEObject;
				T result = caseDeviceType(deviceType);
				if (result == null) result = caseComponentType(deviceType);
				if (result == null) result = caseISupportInfoType(deviceType);
				if (result == null) result = caseIDeviceHealthType(deviceType);
				if (result == null) result = caseIVendorNameplateType(deviceType);
				if (result == null) result = caseTopologyElementType(deviceType);
				if (result == null) result = caseITagNameplateType(deviceType);
				if (result == null) result = caseBaseInterfaceType(deviceType);
				if (result == null) result = caseBaseObjectType(deviceType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUADIProfilePackage.ISUPPORT_INFO_TYPE: {
				ISupportInfoType iSupportInfoType = (ISupportInfoType)theEObject;
				T result = caseISupportInfoType(iSupportInfoType);
				if (result == null) result = caseBaseInterfaceType(iSupportInfoType);
				if (result == null) result = caseBaseObjectType(iSupportInfoType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUADIProfilePackage.IDEVICE_HEALTH_TYPE: {
				IDeviceHealthType iDeviceHealthType = (IDeviceHealthType)theEObject;
				T result = caseIDeviceHealthType(iDeviceHealthType);
				if (result == null) result = caseBaseInterfaceType(iDeviceHealthType);
				if (result == null) result = caseBaseObjectType(iDeviceHealthType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUADIProfilePackage.CONNECTION_POINT_TYPE: {
				ConnectionPointType connectionPointType = (ConnectionPointType)theEObject;
				T result = caseConnectionPointType(connectionPointType);
				if (result == null) result = caseTopologyElementType(connectionPointType);
				if (result == null) result = caseBaseObjectType(connectionPointType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUADIProfilePackage.TOPOLOGY_ELEMENT_TYPE: {
				TopologyElementType topologyElementType = (TopologyElementType)theEObject;
				T result = caseTopologyElementType(topologyElementType);
				if (result == null) result = caseBaseObjectType(topologyElementType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUADIProfilePackage.FUNCTIONAL_GROUP_TYPE: {
				FunctionalGroupType functionalGroupType = (FunctionalGroupType)theEObject;
				T result = caseFunctionalGroupType(functionalGroupType);
				if (result == null) result = caseFolderType(functionalGroupType);
				if (result == null) result = caseBaseObjectType(functionalGroupType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUADIProfilePackage.LOCKING_SERVICES_TYPE: {
				LockingServicesType lockingServicesType = (LockingServicesType)theEObject;
				T result = caseLockingServicesType(lockingServicesType);
				if (result == null) result = caseBaseObjectType(lockingServicesType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUADIProfilePackage.PARAMETER_SET_TOPOLOGY_ELEMENT_TYPE: {
				ParameterSetTopologyElementType parameterSetTopologyElementType = (ParameterSetTopologyElementType)theEObject;
				T result = caseParameterSetTopologyElementType(parameterSetTopologyElementType);
				if (result == null) result = caseBaseObjectType(parameterSetTopologyElementType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUADIProfilePackage.METHOD_SET_TOPOLOGY_ELEMENT_TYPE: {
				MethodSetTopologyElementType methodSetTopologyElementType = (MethodSetTopologyElementType)theEObject;
				T result = caseMethodSetTopologyElementType(methodSetTopologyElementType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUADIProfilePackage.SOFTWARE_TYPE: {
				SoftwareType softwareType = (SoftwareType)theEObject;
				T result = caseSoftwareType(softwareType);
				if (result == null) result = caseComponentType(softwareType);
				if (result == null) result = caseIVendorNameplateType(softwareType);
				if (result == null) result = caseTopologyElementType(softwareType);
				if (result == null) result = caseITagNameplateType(softwareType);
				if (result == null) result = caseBaseInterfaceType(softwareType);
				if (result == null) result = caseBaseObjectType(softwareType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUADIProfilePackage.BLOCK_TYPE: {
				BlockType blockType = (BlockType)theEObject;
				T result = caseBlockType(blockType);
				if (result == null) result = caseTopologyElementType(blockType);
				if (result == null) result = caseBaseObjectType(blockType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUADIProfilePackage.CONFIGURABLE_OBJECT_TYPE: {
				ConfigurableObjectType configurableObjectType = (ConfigurableObjectType)theEObject;
				T result = caseConfigurableObjectType(configurableObjectType);
				if (result == null) result = caseBaseObjectType(configurableObjectType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUADIProfilePackage.IVENDOR_NAMEPLATE_TYPE: {
				IVendorNameplateType iVendorNameplateType = (IVendorNameplateType)theEObject;
				T result = caseIVendorNameplateType(iVendorNameplateType);
				if (result == null) result = caseBaseInterfaceType(iVendorNameplateType);
				if (result == null) result = caseBaseObjectType(iVendorNameplateType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUADIProfilePackage.ITAG_NAMEPLATE_TYPE: {
				ITagNameplateType iTagNameplateType = (ITagNameplateType)theEObject;
				T result = caseITagNameplateType(iTagNameplateType);
				if (result == null) result = caseBaseInterfaceType(iTagNameplateType);
				if (result == null) result = caseBaseObjectType(iTagNameplateType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPCUADIProfilePackage.COMPONENT_TYPE: {
				ComponentType componentType = (ComponentType)theEObject;
				T result = caseComponentType(componentType);
				if (result == null) result = caseIVendorNameplateType(componentType);
				if (result == null) result = caseTopologyElementType(componentType);
				if (result == null) result = caseITagNameplateType(componentType);
				if (result == null) result = caseBaseInterfaceType(componentType);
				if (result == null) result = caseBaseObjectType(componentType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Device Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Device Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeviceType(DeviceType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISupport Info Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISupport Info Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISupportInfoType(ISupportInfoType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IDevice Health Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IDevice Health Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIDeviceHealthType(IDeviceHealthType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connection Point Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connection Point Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnectionPointType(ConnectionPointType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Topology Element Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Topology Element Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTopologyElementType(TopologyElementType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Functional Group Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Functional Group Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFunctionalGroupType(FunctionalGroupType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Locking Services Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Locking Services Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLockingServicesType(LockingServicesType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter Set Topology Element Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter Set Topology Element Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameterSetTopologyElementType(ParameterSetTopologyElementType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Method Set Topology Element Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Method Set Topology Element Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMethodSetTopologyElementType(MethodSetTopologyElementType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Software Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Software Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSoftwareType(SoftwareType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Block Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Block Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlockType(BlockType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Configurable Object Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Configurable Object Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConfigurableObjectType(ConfigurableObjectType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IVendor Nameplate Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IVendor Nameplate Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIVendorNameplateType(IVendorNameplateType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ITag Nameplate Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ITag Nameplate Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseITagNameplateType(ITagNameplateType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentType(ComponentType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Object Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Object Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseObjectType(BaseObjectType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Interface Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Interface Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseInterfaceType(BaseInterfaceType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Folder Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Folder Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFolderType(FolderType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //OPCUADIProfileSwitch
