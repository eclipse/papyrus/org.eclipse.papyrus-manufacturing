/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.opcuaprofile.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.opcua.opcuaprofile.OPCUAProfilePackage;
import org.eclipse.papyrus.opcua.opcuaprofile.ServerDiagnosticsType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Server Diagnostics Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ServerDiagnosticsTypeImpl extends BaseObjectTypeImpl implements ServerDiagnosticsType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServerDiagnosticsTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUAProfilePackage.Literals.SERVER_DIAGNOSTICS_TYPE;
	}

} //ServerDiagnosticsTypeImpl
