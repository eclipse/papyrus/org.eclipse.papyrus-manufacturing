/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.opcuaprofile;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Session Diagnostics Variable Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPCUAProfilePackage#getSessionDiagnosticsVariableType()
 * @model
 * @generated
 */
public interface SessionDiagnosticsVariableType extends BaseDataVariableType {
} // SessionDiagnosticsVariableType
