/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.AnalogUnitType;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Argument;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.BMP;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.BaseDataType;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.ByteString;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.DataValue;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.DateTime;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.DiagnosticInfo;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Duration;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.EccEncryptedSecret;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.ExpandedNodeId;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.GIF;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Guid;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Image;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int16;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int32;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int64;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.JPG;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.LocalId;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.LocalizedText;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.NodeId;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.OPC_UA_LibraryPackage;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.PNG;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.QualifiedName;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.RsaEncryptedSecret;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.SByte;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.StatusCode;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Structure;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.UInt16;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.UInt32;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.UInt64;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.UInteger;
import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.XmlElement;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.OPC_UA_LibraryPackage
 * @generated
 */
public class OPC_UA_LibraryAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static OPC_UA_LibraryPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPC_UA_LibraryAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = OPC_UA_LibraryPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OPC_UA_LibrarySwitch<Adapter> modelSwitch =
		new OPC_UA_LibrarySwitch<Adapter>() {
			@Override
			public Adapter caseAnalogUnitType(AnalogUnitType object) {
				return createAnalogUnitTypeAdapter();
			}
			@Override
			public Adapter caseDouble(org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Double object) {
				return createDoubleAdapter();
			}
			@Override
			public Adapter caseNumber(org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Number object) {
				return createNumberAdapter();
			}
			@Override
			public Adapter caseBaseDataType(BaseDataType object) {
				return createBaseDataTypeAdapter();
			}
			@Override
			public Adapter caseLocalizedText(LocalizedText object) {
				return createLocalizedTextAdapter();
			}
			@Override
			public Adapter caseLocalId(LocalId object) {
				return createLocalIdAdapter();
			}
			@Override
			public Adapter caseString(org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.String object) {
				return createStringAdapter();
			}
			@Override
			public Adapter caseDateTime(DateTime object) {
				return createDateTimeAdapter();
			}
			@Override
			public Adapter caseGuid(Guid object) {
				return createGuidAdapter();
			}
			@Override
			public Adapter caseByteString(ByteString object) {
				return createByteStringAdapter();
			}
			@Override
			public Adapter caseXmlElement(XmlElement object) {
				return createXmlElementAdapter();
			}
			@Override
			public Adapter caseNodeId(NodeId object) {
				return createNodeIdAdapter();
			}
			@Override
			public Adapter caseExpandedNodeId(ExpandedNodeId object) {
				return createExpandedNodeIdAdapter();
			}
			@Override
			public Adapter caseStatusCode(StatusCode object) {
				return createStatusCodeAdapter();
			}
			@Override
			public Adapter caseQualifiedName(QualifiedName object) {
				return createQualifiedNameAdapter();
			}
			@Override
			public Adapter caseStructure(Structure object) {
				return createStructureAdapter();
			}
			@Override
			public Adapter caseDataValue(DataValue object) {
				return createDataValueAdapter();
			}
			@Override
			public Adapter caseDiagnosticInfo(DiagnosticInfo object) {
				return createDiagnosticInfoAdapter();
			}
			@Override
			public Adapter caseRsaEncryptedSecret(RsaEncryptedSecret object) {
				return createRsaEncryptedSecretAdapter();
			}
			@Override
			public Adapter caseEccEncryptedSecret(EccEncryptedSecret object) {
				return createEccEncryptedSecretAdapter();
			}
			@Override
			public Adapter caseInteger(org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Integer object) {
				return createIntegerAdapter();
			}
			@Override
			public Adapter caseFloat(org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Float object) {
				return createFloatAdapter();
			}
			@Override
			public Adapter caseUInteger(UInteger object) {
				return createUIntegerAdapter();
			}
			@Override
			public Adapter caseDuration(Duration object) {
				return createDurationAdapter();
			}
			@Override
			public Adapter caseInt64(Int64 object) {
				return createInt64Adapter();
			}
			@Override
			public Adapter caseInt32(Int32 object) {
				return createInt32Adapter();
			}
			@Override
			public Adapter caseInt16(Int16 object) {
				return createInt16Adapter();
			}
			@Override
			public Adapter caseSByte(SByte object) {
				return createSByteAdapter();
			}
			@Override
			public Adapter caseByte(org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Byte object) {
				return createByteAdapter();
			}
			@Override
			public Adapter caseArgument(Argument object) {
				return createArgumentAdapter();
			}
			@Override
			public Adapter caseUInt16(UInt16 object) {
				return createUInt16Adapter();
			}
			@Override
			public Adapter caseUInt32(UInt32 object) {
				return createUInt32Adapter();
			}
			@Override
			public Adapter caseUInt64(UInt64 object) {
				return createUInt64Adapter();
			}
			@Override
			public Adapter caseImage(Image object) {
				return createImageAdapter();
			}
			@Override
			public Adapter caseGIF(GIF object) {
				return createGIFAdapter();
			}
			@Override
			public Adapter caseBMP(BMP object) {
				return createBMPAdapter();
			}
			@Override
			public Adapter casePNG(PNG object) {
				return createPNGAdapter();
			}
			@Override
			public Adapter caseJPG(JPG object) {
				return createJPGAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.AnalogUnitType <em>Analog Unit Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.AnalogUnitType
	 * @generated
	 */
	public Adapter createAnalogUnitTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Double <em>Double</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Double
	 * @generated
	 */
	public Adapter createDoubleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Number <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Number
	 * @generated
	 */
	public Adapter createNumberAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.BaseDataType <em>Base Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.BaseDataType
	 * @generated
	 */
	public Adapter createBaseDataTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.LocalizedText <em>Localized Text</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.LocalizedText
	 * @generated
	 */
	public Adapter createLocalizedTextAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.LocalId <em>Local Id</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.LocalId
	 * @generated
	 */
	public Adapter createLocalIdAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.String <em>String</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.String
	 * @generated
	 */
	public Adapter createStringAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.DateTime <em>Date Time</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.DateTime
	 * @generated
	 */
	public Adapter createDateTimeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Guid <em>Guid</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Guid
	 * @generated
	 */
	public Adapter createGuidAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.ByteString <em>Byte String</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.ByteString
	 * @generated
	 */
	public Adapter createByteStringAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.XmlElement <em>Xml Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.XmlElement
	 * @generated
	 */
	public Adapter createXmlElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.NodeId <em>Node Id</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.NodeId
	 * @generated
	 */
	public Adapter createNodeIdAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.ExpandedNodeId <em>Expanded Node Id</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.ExpandedNodeId
	 * @generated
	 */
	public Adapter createExpandedNodeIdAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.StatusCode <em>Status Code</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.StatusCode
	 * @generated
	 */
	public Adapter createStatusCodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.QualifiedName <em>Qualified Name</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.QualifiedName
	 * @generated
	 */
	public Adapter createQualifiedNameAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Structure <em>Structure</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Structure
	 * @generated
	 */
	public Adapter createStructureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.DataValue <em>Data Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.DataValue
	 * @generated
	 */
	public Adapter createDataValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.DiagnosticInfo <em>Diagnostic Info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.DiagnosticInfo
	 * @generated
	 */
	public Adapter createDiagnosticInfoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.RsaEncryptedSecret <em>Rsa Encrypted Secret</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.RsaEncryptedSecret
	 * @generated
	 */
	public Adapter createRsaEncryptedSecretAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.EccEncryptedSecret <em>Ecc Encrypted Secret</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.EccEncryptedSecret
	 * @generated
	 */
	public Adapter createEccEncryptedSecretAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Integer <em>Integer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Integer
	 * @generated
	 */
	public Adapter createIntegerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Float <em>Float</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Float
	 * @generated
	 */
	public Adapter createFloatAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.UInteger <em>UInteger</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.UInteger
	 * @generated
	 */
	public Adapter createUIntegerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Duration <em>Duration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Duration
	 * @generated
	 */
	public Adapter createDurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int64 <em>Int64</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int64
	 * @generated
	 */
	public Adapter createInt64Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int32 <em>Int32</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int32
	 * @generated
	 */
	public Adapter createInt32Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int16 <em>Int16</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Int16
	 * @generated
	 */
	public Adapter createInt16Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.SByte <em>SByte</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.SByte
	 * @generated
	 */
	public Adapter createSByteAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Byte <em>Byte</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Byte
	 * @generated
	 */
	public Adapter createByteAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Argument <em>Argument</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Argument
	 * @generated
	 */
	public Adapter createArgumentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.UInt16 <em>UInt16</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.UInt16
	 * @generated
	 */
	public Adapter createUInt16Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.UInt32 <em>UInt32</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.UInt32
	 * @generated
	 */
	public Adapter createUInt32Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.UInt64 <em>UInt64</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.UInt64
	 * @generated
	 */
	public Adapter createUInt64Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Image <em>Image</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.Image
	 * @generated
	 */
	public Adapter createImageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.GIF <em>GIF</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.GIF
	 * @generated
	 */
	public Adapter createGIFAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.BMP <em>BMP</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.BMP
	 * @generated
	 */
	public Adapter createBMPAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.PNG <em>PNG</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.PNG
	 * @generated
	 */
	public Adapter createPNGAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.JPG <em>JPG</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.JPG
	 * @generated
	 */
	public Adapter createJPGAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //OPC_UA_LibraryAdapterFactory
