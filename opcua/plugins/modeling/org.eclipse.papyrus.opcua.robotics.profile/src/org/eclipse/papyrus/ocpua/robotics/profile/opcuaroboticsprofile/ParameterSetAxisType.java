/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter Set Axis Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * � Variable ActualPosition: The ActualPosition variable provides the current position of the axis and may have limits. If the axis has physical limits, the EURange property of the AnalogUnitType shall be provided.
 * � Variable ActualSpeed: The ActualSpeed variable provides the axis speed. Applicable speed limits of the axis shall be provided by the EURange property of the AnalogUnitType
 * � Variable ActualAcceleration: The ActualAcceleration variable provides the axis acceleration. Applicable acceleration limits of the axis shall be provided by the EURange property of the AnalogUnitType.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetAxisType#getActualAcceleration <em>Actual Acceleration</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetAxisType#getActualPosition <em>Actual Position</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetAxisType#getActualSpeed <em>Actual Speed</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetAxisType#getNotCS_AxisState <em>Not CS Axis State</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetAxisType()
 * @model
 * @generated
 */
public interface ParameterSetAxisType extends EObject {
	/**
	 * Returns the value of the '<em><b>Actual Acceleration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actual Acceleration</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actual Acceleration</em>' attribute.
	 * @see #setActualAcceleration(double)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetAxisType_ActualAcceleration()
	 * @model ordered="false"
	 * @generated
	 */
	double getActualAcceleration();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetAxisType#getActualAcceleration <em>Actual Acceleration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Actual Acceleration</em>' attribute.
	 * @see #getActualAcceleration()
	 * @generated
	 */
	void setActualAcceleration(double value);

	/**
	 * Returns the value of the '<em><b>Actual Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actual Position</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actual Position</em>' attribute.
	 * @see #setActualPosition(double)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetAxisType_ActualPosition()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	double getActualPosition();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetAxisType#getActualPosition <em>Actual Position</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Actual Position</em>' attribute.
	 * @see #getActualPosition()
	 * @generated
	 */
	void setActualPosition(double value);

	/**
	 * Returns the value of the '<em><b>Actual Speed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actual Speed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actual Speed</em>' attribute.
	 * @see #setActualSpeed(double)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetAxisType_ActualSpeed()
	 * @model ordered="false"
	 * @generated
	 */
	double getActualSpeed();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetAxisType#getActualSpeed <em>Actual Speed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Actual Speed</em>' attribute.
	 * @see #getActualSpeed()
	 * @generated
	 */
	void setActualSpeed(double value);

	/**
	 * Returns the value of the '<em><b>Not CS Axis State</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisStateEnumeration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not CS Axis State</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not CS Axis State</em>' attribute.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisStateEnumeration
	 * @see #setNotCS_AxisState(AxisStateEnumeration)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetAxisType_NotCS_AxisState()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	AxisStateEnumeration getNotCS_AxisState();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetAxisType#getNotCS_AxisState <em>Not CS Axis State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not CS Axis State</em>' attribute.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisStateEnumeration
	 * @see #getNotCS_AxisState()
	 * @generated
	 */
	void setNotCS_AxisState(AxisStateEnumeration value);

} // ParameterSetAxisType
