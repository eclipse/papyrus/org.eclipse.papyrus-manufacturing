/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.IsDrivenBy;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Is Driven By</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IsDrivenByImpl extends HierarchicalReferencesImpl implements IsDrivenBy {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IsDrivenByImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUARoboticsProfilePackage.Literals.IS_DRIVEN_BY;
	}

} //IsDrivenByImpl
