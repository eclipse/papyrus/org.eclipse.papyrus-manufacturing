/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.opcuaprofile.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.papyrus.opcua.opcuaprofile.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OPCUAProfileFactoryImpl extends EFactoryImpl implements OPCUAProfileFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OPCUAProfileFactory init() {
		try {
			OPCUAProfileFactory theOPCUAProfileFactory = (OPCUAProfileFactory)EPackage.Registry.INSTANCE.getEFactory(OPCUAProfilePackage.eNS_URI);
			if (theOPCUAProfileFactory != null) {
				return theOPCUAProfileFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OPCUAProfileFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPCUAProfileFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case OPCUAProfilePackage.BASE_OBJECT_TYPE: return createBaseObjectType();
			case OPCUAProfilePackage.DATA_TYPE_SYSTEM_TYPE: return createDataTypeSystemType();
			case OPCUAProfilePackage.MODELLING_RULE_TYPE: return createModellingRuleType();
			case OPCUAProfilePackage.SERVER_TYPE: return createServerType();
			case OPCUAProfilePackage.FOLDER_TYPE: return createFolderType();
			case OPCUAProfilePackage.DATA_TYPE_ENCODING_TYPE: return createDataTypeEncodingType();
			case OPCUAProfilePackage.SERVER_CAPABILITIES_TYPE: return createServerCapabilitiesType();
			case OPCUAProfilePackage.SERVER_DIAGNOSTICS_TYPE: return createServerDiagnosticsType();
			case OPCUAProfilePackage.SESSIONS_DIAGNOSTICS_SUMMARY_TYPE: return createSessionsDiagnosticsSummaryType();
			case OPCUAProfilePackage.SESSION_DIAGNOSTICS_OBJECT_TYPE: return createSessionDiagnosticsObjectType();
			case OPCUAProfilePackage.VENDOR_SERVER_INFO_TYPE: return createVendorServerInfoType();
			case OPCUAProfilePackage.SERVER_REDUNDANCY_TYPE: return createServerRedundancyType();
			case OPCUAProfilePackage.FILE_TYPE: return createFileType();
			case OPCUAProfilePackage.NAMESPACES_TYPE: return createNamespacesType();
			case OPCUAProfilePackage.BASE_EVENT_TYPE: return createBaseEventType();
			case OPCUAProfilePackage.AGGREGATE_FUNCTION_TYPE: return createAggregateFunctionType();
			case OPCUAProfilePackage.STATE_MACHINE_TYPE: return createStateMachineType();
			case OPCUAProfilePackage.STATE_TYPE: return createStateType();
			case OPCUAProfilePackage.NAMESPACE_METADATA_TYPE: return createNamespaceMetadataType();
			case OPCUAProfilePackage.WRITER_GROUP_MESSAGE_TYPE: return createWriterGroupMessageType();
			case OPCUAProfilePackage.TRANSITION_TYPE: return createTransitionType();
			case OPCUAProfilePackage.TEMPORARY_FILE_TRANSFER_TYPE: return createTemporaryFileTransferType();
			case OPCUAProfilePackage.ROLE_SET_TYPE: return createRoleSetType();
			case OPCUAProfilePackage.ROLE_TYPE: return createRoleType();
			case OPCUAProfilePackage.DICTIONARY_ENTRY_TYPE: return createDictionaryEntryType();
			case OPCUAProfilePackage.ORDERED_LIST_TYPE: return createOrderedListType();
			case OPCUAProfilePackage.BASE_CONDITION_CLASS_TYPE: return createBaseConditionClassType();
			case OPCUAProfilePackage.ALARM_METRICS_TYPE: return createAlarmMetricsType();
			case OPCUAProfilePackage.HISTORICAL_DATA_CONFIGURATION_TYPE: return createHistoricalDataConfigurationType();
			case OPCUAProfilePackage.HISTORY_SERVER_CAPABILITIES_TYPE: return createHistoryServerCapabilitiesType();
			case OPCUAProfilePackage.CERTIFICATE_GROUP_TYPE: return createCertificateGroupType();
			case OPCUAProfilePackage.CERTIFICATE_TYPE: return createCertificateType();
			case OPCUAProfilePackage.SERVER_CONFIGURATION_TYPE: return createServerConfigurationType();
			case OPCUAProfilePackage.KEY_CREDENTIAL_CONFIGURATION_TYPE: return createKeyCredentialConfigurationType();
			case OPCUAProfilePackage.AUTHORIZATION_SERVICE_CONFIGURATION_TYPE: return createAuthorizationServiceConfigurationType();
			case OPCUAProfilePackage.AGGREGATE_CONFIGURATION_TYPE: return createAggregateConfigurationType();
			case OPCUAProfilePackage.PUB_SUB_KEY_SERVICE_TYPE: return createPubSubKeyServiceType();
			case OPCUAProfilePackage.SECURITY_GROUP_TYPE: return createSecurityGroupType();
			case OPCUAProfilePackage.PUBLISHED_DATA_SET_TYPE: return createPublishedDataSetType();
			case OPCUAProfilePackage.EXTENSION_FIELDS_TYPE: return createExtensionFieldsType();
			case OPCUAProfilePackage.PUB_SUB_CONNECTION_TYPE: return createPubSubConnectionType();
			case OPCUAProfilePackage.CONNECTION_TRANSPORT_TYPE: return createConnectionTransportType();
			case OPCUAProfilePackage.PUB_SUB_GROUP_TYPE: return createPubSubGroupType();
			case OPCUAProfilePackage.WRITER_GROUP_TRANSPORT_TYPE: return createWriterGroupTransportType();
			case OPCUAProfilePackage.READER_GROUP_TRANSPORT_TYPE: return createReaderGroupTransportType();
			case OPCUAProfilePackage.READER_GROUP_MESSAGE_TYPE: return createReaderGroupMessageType();
			case OPCUAProfilePackage.DATA_SET_WRITER_TYPE: return createDataSetWriterType();
			case OPCUAProfilePackage.DATA_SET_WRITER_TRANSPORT_TYPE: return createDataSetWriterTransportType();
			case OPCUAProfilePackage.DATA_SET_WRITER_MESSAGE_TYPE: return createDataSetWriterMessageType();
			case OPCUAProfilePackage.DATA_SET_READER_TYPE: return createDataSetReaderType();
			case OPCUAProfilePackage.DATA_SET_READER_TRANSPORT_TYPE: return createDataSetReaderTransportType();
			case OPCUAProfilePackage.DATA_SET_READER_MESSAGE_TYPE: return createDataSetReaderMessageType();
			case OPCUAProfilePackage.SUBSCRIBED_DATA_SET_TYPE: return createSubscribedDataSetType();
			case OPCUAProfilePackage.PUB_SUB_STATUS_TYPE: return createPubSubStatusType();
			case OPCUAProfilePackage.PUB_SUB_DIAGNOSTICS_TYPE: return createPubSubDiagnosticsType();
			case OPCUAProfilePackage.NETWORK_ADDRESS_TYPE: return createNetworkAddressType();
			case OPCUAProfilePackage.ALIAS_NAME_TYPE: return createAliasNameType();
			case OPCUAProfilePackage.BASE_VARIABLE_TYPE: return createBaseVariableType();
			case OPCUAProfilePackage.BASE_DATA_VARIABLE_TYPE: return createBaseDataVariableType();
			case OPCUAProfilePackage.PROPERTY_TYPE: return createPropertyType();
			case OPCUAProfilePackage.SERVER_VENDOR_CAPABILITY_TYPE: return createServerVendorCapabilityType();
			case OPCUAProfilePackage.SAMPLING_INTERVAL_DIAGNOSTICS_ARRAY_TYPE: return createSamplingIntervalDiagnosticsArrayType();
			case OPCUAProfilePackage.SAMPLING_INTERVAL_DIAGNOSTICS_TYPE: return createSamplingIntervalDiagnosticsType();
			case OPCUAProfilePackage.SUBSCRIPTION_DIAGNOSTICS_ARRAY_TYPE: return createSubscriptionDiagnosticsArrayType();
			case OPCUAProfilePackage.SUBSCRIPTION_DIAGNOSTICS_TYPE: return createSubscriptionDiagnosticsType();
			case OPCUAProfilePackage.SESSION_DIAGNOSTICS_ARRAY_TYPE: return createSessionDiagnosticsArrayType();
			case OPCUAProfilePackage.SESSION_DIAGNOSTICS_VARIABLE_TYPE: return createSessionDiagnosticsVariableType();
			case OPCUAProfilePackage.SESSION_SECURITY_DIAGNOSTICS_ARRAY_TYPE: return createSessionSecurityDiagnosticsArrayType();
			case OPCUAProfilePackage.SESSION_SECURITY_DIAGNOSTICS_TYPE: return createSessionSecurityDiagnosticsType();
			case OPCUAProfilePackage.OPTION_SET_TYPE: return createOptionSetType();
			case OPCUAProfilePackage.SERVER_DIAGNOSTICS_SUMMARY_TYPE: return createServerDiagnosticsSummaryType();
			case OPCUAProfilePackage.BUILD_INFO_TYPE: return createBuildInfoType();
			case OPCUAProfilePackage.SERVER_STATUS_TYPE: return createServerStatusType();
			case OPCUAProfilePackage.BASE_INTERFACE_TYPE: return createBaseInterfaceType();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseObjectType createBaseObjectType() {
		BaseObjectTypeImpl baseObjectType = new BaseObjectTypeImpl();
		return baseObjectType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataTypeSystemType createDataTypeSystemType() {
		DataTypeSystemTypeImpl dataTypeSystemType = new DataTypeSystemTypeImpl();
		return dataTypeSystemType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModellingRuleType createModellingRuleType() {
		ModellingRuleTypeImpl modellingRuleType = new ModellingRuleTypeImpl();
		return modellingRuleType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServerType createServerType() {
		ServerTypeImpl serverType = new ServerTypeImpl();
		return serverType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FolderType createFolderType() {
		FolderTypeImpl folderType = new FolderTypeImpl();
		return folderType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataTypeEncodingType createDataTypeEncodingType() {
		DataTypeEncodingTypeImpl dataTypeEncodingType = new DataTypeEncodingTypeImpl();
		return dataTypeEncodingType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServerCapabilitiesType createServerCapabilitiesType() {
		ServerCapabilitiesTypeImpl serverCapabilitiesType = new ServerCapabilitiesTypeImpl();
		return serverCapabilitiesType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServerDiagnosticsType createServerDiagnosticsType() {
		ServerDiagnosticsTypeImpl serverDiagnosticsType = new ServerDiagnosticsTypeImpl();
		return serverDiagnosticsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SessionsDiagnosticsSummaryType createSessionsDiagnosticsSummaryType() {
		SessionsDiagnosticsSummaryTypeImpl sessionsDiagnosticsSummaryType = new SessionsDiagnosticsSummaryTypeImpl();
		return sessionsDiagnosticsSummaryType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SessionDiagnosticsObjectType createSessionDiagnosticsObjectType() {
		SessionDiagnosticsObjectTypeImpl sessionDiagnosticsObjectType = new SessionDiagnosticsObjectTypeImpl();
		return sessionDiagnosticsObjectType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VendorServerInfoType createVendorServerInfoType() {
		VendorServerInfoTypeImpl vendorServerInfoType = new VendorServerInfoTypeImpl();
		return vendorServerInfoType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServerRedundancyType createServerRedundancyType() {
		ServerRedundancyTypeImpl serverRedundancyType = new ServerRedundancyTypeImpl();
		return serverRedundancyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FileType createFileType() {
		FileTypeImpl fileType = new FileTypeImpl();
		return fileType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NamespacesType createNamespacesType() {
		NamespacesTypeImpl namespacesType = new NamespacesTypeImpl();
		return namespacesType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseEventType createBaseEventType() {
		BaseEventTypeImpl baseEventType = new BaseEventTypeImpl();
		return baseEventType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AggregateFunctionType createAggregateFunctionType() {
		AggregateFunctionTypeImpl aggregateFunctionType = new AggregateFunctionTypeImpl();
		return aggregateFunctionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StateMachineType createStateMachineType() {
		StateMachineTypeImpl stateMachineType = new StateMachineTypeImpl();
		return stateMachineType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StateType createStateType() {
		StateTypeImpl stateType = new StateTypeImpl();
		return stateType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NamespaceMetadataType createNamespaceMetadataType() {
		NamespaceMetadataTypeImpl namespaceMetadataType = new NamespaceMetadataTypeImpl();
		return namespaceMetadataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WriterGroupMessageType createWriterGroupMessageType() {
		WriterGroupMessageTypeImpl writerGroupMessageType = new WriterGroupMessageTypeImpl();
		return writerGroupMessageType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransitionType createTransitionType() {
		TransitionTypeImpl transitionType = new TransitionTypeImpl();
		return transitionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemporaryFileTransferType createTemporaryFileTransferType() {
		TemporaryFileTransferTypeImpl temporaryFileTransferType = new TemporaryFileTransferTypeImpl();
		return temporaryFileTransferType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleSetType createRoleSetType() {
		RoleSetTypeImpl roleSetType = new RoleSetTypeImpl();
		return roleSetType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleType createRoleType() {
		RoleTypeImpl roleType = new RoleTypeImpl();
		return roleType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DictionaryEntryType createDictionaryEntryType() {
		DictionaryEntryTypeImpl dictionaryEntryType = new DictionaryEntryTypeImpl();
		return dictionaryEntryType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrderedListType createOrderedListType() {
		OrderedListTypeImpl orderedListType = new OrderedListTypeImpl();
		return orderedListType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseConditionClassType createBaseConditionClassType() {
		BaseConditionClassTypeImpl baseConditionClassType = new BaseConditionClassTypeImpl();
		return baseConditionClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlarmMetricsType createAlarmMetricsType() {
		AlarmMetricsTypeImpl alarmMetricsType = new AlarmMetricsTypeImpl();
		return alarmMetricsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HistoricalDataConfigurationType createHistoricalDataConfigurationType() {
		HistoricalDataConfigurationTypeImpl historicalDataConfigurationType = new HistoricalDataConfigurationTypeImpl();
		return historicalDataConfigurationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HistoryServerCapabilitiesType createHistoryServerCapabilitiesType() {
		HistoryServerCapabilitiesTypeImpl historyServerCapabilitiesType = new HistoryServerCapabilitiesTypeImpl();
		return historyServerCapabilitiesType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CertificateGroupType createCertificateGroupType() {
		CertificateGroupTypeImpl certificateGroupType = new CertificateGroupTypeImpl();
		return certificateGroupType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CertificateType createCertificateType() {
		CertificateTypeImpl certificateType = new CertificateTypeImpl();
		return certificateType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServerConfigurationType createServerConfigurationType() {
		ServerConfigurationTypeImpl serverConfigurationType = new ServerConfigurationTypeImpl();
		return serverConfigurationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public KeyCredentialConfigurationType createKeyCredentialConfigurationType() {
		KeyCredentialConfigurationTypeImpl keyCredentialConfigurationType = new KeyCredentialConfigurationTypeImpl();
		return keyCredentialConfigurationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AuthorizationServiceConfigurationType createAuthorizationServiceConfigurationType() {
		AuthorizationServiceConfigurationTypeImpl authorizationServiceConfigurationType = new AuthorizationServiceConfigurationTypeImpl();
		return authorizationServiceConfigurationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AggregateConfigurationType createAggregateConfigurationType() {
		AggregateConfigurationTypeImpl aggregateConfigurationType = new AggregateConfigurationTypeImpl();
		return aggregateConfigurationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PubSubKeyServiceType createPubSubKeyServiceType() {
		PubSubKeyServiceTypeImpl pubSubKeyServiceType = new PubSubKeyServiceTypeImpl();
		return pubSubKeyServiceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SecurityGroupType createSecurityGroupType() {
		SecurityGroupTypeImpl securityGroupType = new SecurityGroupTypeImpl();
		return securityGroupType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PublishedDataSetType createPublishedDataSetType() {
		PublishedDataSetTypeImpl publishedDataSetType = new PublishedDataSetTypeImpl();
		return publishedDataSetType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExtensionFieldsType createExtensionFieldsType() {
		ExtensionFieldsTypeImpl extensionFieldsType = new ExtensionFieldsTypeImpl();
		return extensionFieldsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PubSubConnectionType createPubSubConnectionType() {
		PubSubConnectionTypeImpl pubSubConnectionType = new PubSubConnectionTypeImpl();
		return pubSubConnectionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionTransportType createConnectionTransportType() {
		ConnectionTransportTypeImpl connectionTransportType = new ConnectionTransportTypeImpl();
		return connectionTransportType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PubSubGroupType createPubSubGroupType() {
		PubSubGroupTypeImpl pubSubGroupType = new PubSubGroupTypeImpl();
		return pubSubGroupType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WriterGroupTransportType createWriterGroupTransportType() {
		WriterGroupTransportTypeImpl writerGroupTransportType = new WriterGroupTransportTypeImpl();
		return writerGroupTransportType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReaderGroupTransportType createReaderGroupTransportType() {
		ReaderGroupTransportTypeImpl readerGroupTransportType = new ReaderGroupTransportTypeImpl();
		return readerGroupTransportType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReaderGroupMessageType createReaderGroupMessageType() {
		ReaderGroupMessageTypeImpl readerGroupMessageType = new ReaderGroupMessageTypeImpl();
		return readerGroupMessageType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataSetWriterType createDataSetWriterType() {
		DataSetWriterTypeImpl dataSetWriterType = new DataSetWriterTypeImpl();
		return dataSetWriterType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataSetWriterTransportType createDataSetWriterTransportType() {
		DataSetWriterTransportTypeImpl dataSetWriterTransportType = new DataSetWriterTransportTypeImpl();
		return dataSetWriterTransportType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataSetWriterMessageType createDataSetWriterMessageType() {
		DataSetWriterMessageTypeImpl dataSetWriterMessageType = new DataSetWriterMessageTypeImpl();
		return dataSetWriterMessageType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataSetReaderType createDataSetReaderType() {
		DataSetReaderTypeImpl dataSetReaderType = new DataSetReaderTypeImpl();
		return dataSetReaderType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataSetReaderTransportType createDataSetReaderTransportType() {
		DataSetReaderTransportTypeImpl dataSetReaderTransportType = new DataSetReaderTransportTypeImpl();
		return dataSetReaderTransportType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataSetReaderMessageType createDataSetReaderMessageType() {
		DataSetReaderMessageTypeImpl dataSetReaderMessageType = new DataSetReaderMessageTypeImpl();
		return dataSetReaderMessageType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubscribedDataSetType createSubscribedDataSetType() {
		SubscribedDataSetTypeImpl subscribedDataSetType = new SubscribedDataSetTypeImpl();
		return subscribedDataSetType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PubSubStatusType createPubSubStatusType() {
		PubSubStatusTypeImpl pubSubStatusType = new PubSubStatusTypeImpl();
		return pubSubStatusType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PubSubDiagnosticsType createPubSubDiagnosticsType() {
		PubSubDiagnosticsTypeImpl pubSubDiagnosticsType = new PubSubDiagnosticsTypeImpl();
		return pubSubDiagnosticsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NetworkAddressType createNetworkAddressType() {
		NetworkAddressTypeImpl networkAddressType = new NetworkAddressTypeImpl();
		return networkAddressType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AliasNameType createAliasNameType() {
		AliasNameTypeImpl aliasNameType = new AliasNameTypeImpl();
		return aliasNameType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseVariableType createBaseVariableType() {
		BaseVariableTypeImpl baseVariableType = new BaseVariableTypeImpl();
		return baseVariableType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseDataVariableType createBaseDataVariableType() {
		BaseDataVariableTypeImpl baseDataVariableType = new BaseDataVariableTypeImpl();
		return baseDataVariableType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyType createPropertyType() {
		PropertyTypeImpl propertyType = new PropertyTypeImpl();
		return propertyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServerVendorCapabilityType createServerVendorCapabilityType() {
		ServerVendorCapabilityTypeImpl serverVendorCapabilityType = new ServerVendorCapabilityTypeImpl();
		return serverVendorCapabilityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SamplingIntervalDiagnosticsArrayType createSamplingIntervalDiagnosticsArrayType() {
		SamplingIntervalDiagnosticsArrayTypeImpl samplingIntervalDiagnosticsArrayType = new SamplingIntervalDiagnosticsArrayTypeImpl();
		return samplingIntervalDiagnosticsArrayType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SamplingIntervalDiagnosticsType createSamplingIntervalDiagnosticsType() {
		SamplingIntervalDiagnosticsTypeImpl samplingIntervalDiagnosticsType = new SamplingIntervalDiagnosticsTypeImpl();
		return samplingIntervalDiagnosticsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubscriptionDiagnosticsArrayType createSubscriptionDiagnosticsArrayType() {
		SubscriptionDiagnosticsArrayTypeImpl subscriptionDiagnosticsArrayType = new SubscriptionDiagnosticsArrayTypeImpl();
		return subscriptionDiagnosticsArrayType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubscriptionDiagnosticsType createSubscriptionDiagnosticsType() {
		SubscriptionDiagnosticsTypeImpl subscriptionDiagnosticsType = new SubscriptionDiagnosticsTypeImpl();
		return subscriptionDiagnosticsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SessionDiagnosticsArrayType createSessionDiagnosticsArrayType() {
		SessionDiagnosticsArrayTypeImpl sessionDiagnosticsArrayType = new SessionDiagnosticsArrayTypeImpl();
		return sessionDiagnosticsArrayType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SessionDiagnosticsVariableType createSessionDiagnosticsVariableType() {
		SessionDiagnosticsVariableTypeImpl sessionDiagnosticsVariableType = new SessionDiagnosticsVariableTypeImpl();
		return sessionDiagnosticsVariableType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SessionSecurityDiagnosticsArrayType createSessionSecurityDiagnosticsArrayType() {
		SessionSecurityDiagnosticsArrayTypeImpl sessionSecurityDiagnosticsArrayType = new SessionSecurityDiagnosticsArrayTypeImpl();
		return sessionSecurityDiagnosticsArrayType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SessionSecurityDiagnosticsType createSessionSecurityDiagnosticsType() {
		SessionSecurityDiagnosticsTypeImpl sessionSecurityDiagnosticsType = new SessionSecurityDiagnosticsTypeImpl();
		return sessionSecurityDiagnosticsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OptionSetType createOptionSetType() {
		OptionSetTypeImpl optionSetType = new OptionSetTypeImpl();
		return optionSetType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServerDiagnosticsSummaryType createServerDiagnosticsSummaryType() {
		ServerDiagnosticsSummaryTypeImpl serverDiagnosticsSummaryType = new ServerDiagnosticsSummaryTypeImpl();
		return serverDiagnosticsSummaryType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BuildInfoType createBuildInfoType() {
		BuildInfoTypeImpl buildInfoType = new BuildInfoTypeImpl();
		return buildInfoType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServerStatusType createServerStatusType() {
		ServerStatusTypeImpl serverStatusType = new ServerStatusTypeImpl();
		return serverStatusType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseInterfaceType createBaseInterfaceType() {
		BaseInterfaceTypeImpl baseInterfaceType = new BaseInterfaceTypeImpl();
		return baseInterfaceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPCUAProfilePackage getOPCUAProfilePackage() {
		return (OPCUAProfilePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static OPCUAProfilePackage getPackage() {
		return OPCUAProfilePackage.eINSTANCE;
	}

} //OPCUAProfileFactoryImpl
