/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Motion Device Category Enum</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * OTHER 		Any motion-device which is not defined by the MotionDeviceCategoryEnumeration 
 * ARTICULATED_ROBOT 		This robot design features rotary joints and can range from simple two joint structures to 10 or more joints. The arm is connected to the base with a twisting joint. The links in the arm are connected by rotary joints. 
 * SCARA_ROBOT 		Robot has two parallel rotary joints to provide compliance in a selected plane 
 * CARTESIAN_ROBOT 		Cartesian robots have three linear joints that use the Cartesian coordinate system (X, Y, and Z). They also may have an attached wrist to allow for rotational movement. The three prismatic joints deliver a linear motion along the axis. 
 * SPHERICAL_ROBOT 		The arm is connected to the base with a twisting joint and a combination of two rotary joints and one linear joint. The axes form a polar coordinate system and create a spherical-shaped work envelope. 
 * PARALLEL_ROBOT 		These spider-like robots are built from jointed parallelograms connected to a common base. The parallelograms move a single end of arm tooling in a dome-shaped work area. 
 * CYLINDRICAL_ROBOT 		The robot has at least one rotary joint at the base and at least one prismatic joint to connect the links. The rotary joint uses a rotational motion along the joint axis, while the prismatic joint moves in a linear motion. Cylindrical robots operate within a cylindrical-shaped work envelope. 
 * 
 * <!-- end-model-doc -->
 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getMotionDeviceCategoryEnum()
 * @model
 * @generated
 */
public enum MotionDeviceCategoryEnum implements Enumerator {
	/**
	 * The '<em><b>OTHER</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OTHER_VALUE
	 * @generated
	 * @ordered
	 */
	OTHER(0, "OTHER", "OTHER"),

	/**
	 * The '<em><b>ARTICULATED ROBOT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ARTICULATED_ROBOT_VALUE
	 * @generated
	 * @ordered
	 */
	ARTICULATED_ROBOT(1, "ARTICULATED_ROBOT", "ARTICULATED_ROBOT"),

	/**
	 * The '<em><b>SCARA ROBOT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SCARA_ROBOT_VALUE
	 * @generated
	 * @ordered
	 */
	SCARA_ROBOT(2, "SCARA_ROBOT", "SCARA_ROBOT"),

	/**
	 * The '<em><b>CARTESIAN ROBOT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CARTESIAN_ROBOT_VALUE
	 * @generated
	 * @ordered
	 */
	CARTESIAN_ROBOT(3, "CARTESIAN_ROBOT", "CARTESIAN_ROBOT"),

	/**
	 * The '<em><b>SPHERICAL ROBOT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SPHERICAL_ROBOT_VALUE
	 * @generated
	 * @ordered
	 */
	SPHERICAL_ROBOT(4, "SPHERICAL_ROBOT", "SPHERICAL_ROBOT"),

	/**
	 * The '<em><b>PARALLEL ROBOT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PARALLEL_ROBOT_VALUE
	 * @generated
	 * @ordered
	 */
	PARALLEL_ROBOT(5, "PARALLEL_ROBOT", "PARALLEL_ROBOT"),

	/**
	 * The '<em><b>CYLINDRICAL ROBOT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CYLINDRICAL_ROBOT_VALUE
	 * @generated
	 * @ordered
	 */
	CYLINDRICAL_ROBOT(6, "CYLINDRICAL_ROBOT", "CYLINDRICAL_ROBOT");

	/**
	 * The '<em><b>OTHER</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OTHER
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int OTHER_VALUE = 0;

	/**
	 * The '<em><b>ARTICULATED ROBOT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ARTICULATED_ROBOT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ARTICULATED_ROBOT_VALUE = 1;

	/**
	 * The '<em><b>SCARA ROBOT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SCARA_ROBOT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SCARA_ROBOT_VALUE = 2;

	/**
	 * The '<em><b>CARTESIAN ROBOT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CARTESIAN_ROBOT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CARTESIAN_ROBOT_VALUE = 3;

	/**
	 * The '<em><b>SPHERICAL ROBOT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SPHERICAL_ROBOT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SPHERICAL_ROBOT_VALUE = 4;

	/**
	 * The '<em><b>PARALLEL ROBOT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PARALLEL_ROBOT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PARALLEL_ROBOT_VALUE = 5;

	/**
	 * The '<em><b>CYLINDRICAL ROBOT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CYLINDRICAL_ROBOT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CYLINDRICAL_ROBOT_VALUE = 6;

	/**
	 * An array of all the '<em><b>Motion Device Category Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MotionDeviceCategoryEnum[] VALUES_ARRAY =
		new MotionDeviceCategoryEnum[] {
			OTHER,
			ARTICULATED_ROBOT,
			SCARA_ROBOT,
			CARTESIAN_ROBOT,
			SPHERICAL_ROBOT,
			PARALLEL_ROBOT,
			CYLINDRICAL_ROBOT,
		};

	/**
	 * A public read-only list of all the '<em><b>Motion Device Category Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MotionDeviceCategoryEnum> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Motion Device Category Enum</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static MotionDeviceCategoryEnum get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MotionDeviceCategoryEnum result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Motion Device Category Enum</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static MotionDeviceCategoryEnum getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MotionDeviceCategoryEnum result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Motion Device Category Enum</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static MotionDeviceCategoryEnum get(int value) {
		switch (value) {
			case OTHER_VALUE: return OTHER;
			case ARTICULATED_ROBOT_VALUE: return ARTICULATED_ROBOT;
			case SCARA_ROBOT_VALUE: return SCARA_ROBOT;
			case CARTESIAN_ROBOT_VALUE: return CARTESIAN_ROBOT;
			case SPHERICAL_ROBOT_VALUE: return SPHERICAL_ROBOT;
			case PARALLEL_ROBOT_VALUE: return PARALLEL_ROBOT;
			case CYLINDRICAL_ROBOT_VALUE: return CYLINDRICAL_ROBOT;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MotionDeviceCategoryEnum(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //MotionDeviceCategoryEnum
