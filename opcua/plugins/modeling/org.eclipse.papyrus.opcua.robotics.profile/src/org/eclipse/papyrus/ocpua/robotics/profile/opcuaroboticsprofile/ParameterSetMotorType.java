/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter Set Motor Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 *  Variable BrakeReleased: The BrakeReleased is an optional variable used only for motors with brakes. If
 * BrakeReleased is TRUE the motor is free to run. FALSE means that the motor shaft is locked by the brake.
 *  Variable MotorTemperature: The MotorTemperature provides the temperature of the motor. If there is no
 * temperature sensor the value is set to null.
 *  Variable EffectiveLoadRate: EffectiveLoadRate is expressed as a percentage of maximum continuous load.
 * The Joule integral is typically used to calculate the current load
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotorType#isBrakeReleased <em>Brake Released</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotorType#getEffectiveLoadRate <em>Effective Load Rate</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotorType#getMotorTemperature <em>Motor Temperature</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotorType#getNotCS_MotorIntensity <em>Not CS Motor Intensity</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetMotorType()
 * @model
 * @generated
 */
public interface ParameterSetMotorType extends EObject {
	/**
	 * Returns the value of the '<em><b>Brake Released</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Brake Released</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Brake Released</em>' attribute.
	 * @see #setBrakeReleased(boolean)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetMotorType_BrakeReleased()
	 * @model dataType="org.eclipse.uml2.types.Boolean" required="true" ordered="false"
	 * @generated
	 */
	boolean isBrakeReleased();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotorType#isBrakeReleased <em>Brake Released</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Brake Released</em>' attribute.
	 * @see #isBrakeReleased()
	 * @generated
	 */
	void setBrakeReleased(boolean value);

	/**
	 * Returns the value of the '<em><b>Effective Load Rate</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Effective Load Rate</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Effective Load Rate</em>' attribute.
	 * @see #setEffectiveLoadRate(int)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetMotorType_EffectiveLoadRate()
	 * @model dataType="org.eclipse.uml2.types.Integer" required="true" ordered="false"
	 * @generated
	 */
	int getEffectiveLoadRate();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotorType#getEffectiveLoadRate <em>Effective Load Rate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Effective Load Rate</em>' attribute.
	 * @see #getEffectiveLoadRate()
	 * @generated
	 */
	void setEffectiveLoadRate(int value);

	/**
	 * Returns the value of the '<em><b>Motor Temperature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Motor Temperature</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Motor Temperature</em>' attribute.
	 * @see #setMotorTemperature(double)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetMotorType_MotorTemperature()
	 * @model dataType="org.eclipse.uml2.types.Real" required="true" ordered="false"
	 * @generated
	 */
	double getMotorTemperature();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotorType#getMotorTemperature <em>Motor Temperature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Motor Temperature</em>' attribute.
	 * @see #getMotorTemperature()
	 * @generated
	 */
	void setMotorTemperature(double value);

	/**
	 * Returns the value of the '<em><b>Not CS Motor Intensity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not CS Motor Intensity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not CS Motor Intensity</em>' attribute.
	 * @see #setNotCS_MotorIntensity(int)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetMotorType_NotCS_MotorIntensity()
	 * @model dataType="org.eclipse.uml2.types.Integer" required="true" ordered="false"
	 * @generated
	 */
	int getNotCS_MotorIntensity();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotorType#getNotCS_MotorIntensity <em>Not CS Motor Intensity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not CS Motor Intensity</em>' attribute.
	 * @see #getNotCS_MotorIntensity()
	 * @generated
	 */
	void setNotCS_MotorIntensity(int value);

} // ParameterSetMotorType
