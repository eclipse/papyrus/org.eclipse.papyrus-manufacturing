/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ModeEnum;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameter Set Motion Device Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetMotionDeviceTypeImpl#isOnPath <em>On Path</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetMotionDeviceTypeImpl#isInControl <em>In Control</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetMotionDeviceTypeImpl#getSpeedOverride <em>Speed Override</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetMotionDeviceTypeImpl#isNotCS_isPowerButtonPressed <em>Not CS is Power Button Pressed</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetMotionDeviceTypeImpl#getNotCS__RobotIntensity <em>Not CS Robot Intensity</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetMotionDeviceTypeImpl#isNotCS_isTeachButtonPressed <em>Not CS is Teach Button Pressed</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetMotionDeviceTypeImpl#isNotCS_isPowerOnRobot <em>Not CS is Power On Robot</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetMotionDeviceTypeImpl#getMode <em>Mode</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ParameterSetMotionDeviceTypeImpl extends MinimalEObjectImpl.Container implements ParameterSetMotionDeviceType {
	/**
	 * The default value of the '{@link #isOnPath() <em>On Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOnPath()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ON_PATH_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isOnPath() <em>On Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOnPath()
	 * @generated
	 * @ordered
	 */
	protected boolean onPath = ON_PATH_EDEFAULT;

	/**
	 * The default value of the '{@link #isInControl() <em>In Control</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInControl()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IN_CONTROL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isInControl() <em>In Control</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInControl()
	 * @generated
	 * @ordered
	 */
	protected boolean inControl = IN_CONTROL_EDEFAULT;

	/**
	 * The default value of the '{@link #getSpeedOverride() <em>Speed Override</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpeedOverride()
	 * @generated
	 * @ordered
	 */
	protected static final double SPEED_OVERRIDE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getSpeedOverride() <em>Speed Override</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpeedOverride()
	 * @generated
	 * @ordered
	 */
	protected double speedOverride = SPEED_OVERRIDE_EDEFAULT;

	/**
	 * The default value of the '{@link #isNotCS_isPowerButtonPressed() <em>Not CS is Power Button Pressed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNotCS_isPowerButtonPressed()
	 * @generated
	 * @ordered
	 */
	protected static final boolean NOT_CS_IS_POWER_BUTTON_PRESSED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isNotCS_isPowerButtonPressed() <em>Not CS is Power Button Pressed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNotCS_isPowerButtonPressed()
	 * @generated
	 * @ordered
	 */
	protected boolean notCS_isPowerButtonPressed = NOT_CS_IS_POWER_BUTTON_PRESSED_EDEFAULT;

	/**
	 * The default value of the '{@link #getNotCS__RobotIntensity() <em>Not CS Robot Intensity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotCS__RobotIntensity()
	 * @generated
	 * @ordered
	 */
	protected static final double NOT_CS_ROBOT_INTENSITY_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getNotCS__RobotIntensity() <em>Not CS Robot Intensity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotCS__RobotIntensity()
	 * @generated
	 * @ordered
	 */
	protected double notCS__RobotIntensity = NOT_CS_ROBOT_INTENSITY_EDEFAULT;

	/**
	 * The default value of the '{@link #isNotCS_isTeachButtonPressed() <em>Not CS is Teach Button Pressed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNotCS_isTeachButtonPressed()
	 * @generated
	 * @ordered
	 */
	protected static final boolean NOT_CS_IS_TEACH_BUTTON_PRESSED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isNotCS_isTeachButtonPressed() <em>Not CS is Teach Button Pressed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNotCS_isTeachButtonPressed()
	 * @generated
	 * @ordered
	 */
	protected boolean notCS_isTeachButtonPressed = NOT_CS_IS_TEACH_BUTTON_PRESSED_EDEFAULT;

	/**
	 * The default value of the '{@link #isNotCS_isPowerOnRobot() <em>Not CS is Power On Robot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNotCS_isPowerOnRobot()
	 * @generated
	 * @ordered
	 */
	protected static final boolean NOT_CS_IS_POWER_ON_ROBOT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isNotCS_isPowerOnRobot() <em>Not CS is Power On Robot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNotCS_isPowerOnRobot()
	 * @generated
	 * @ordered
	 */
	protected boolean notCS_isPowerOnRobot = NOT_CS_IS_POWER_ON_ROBOT_EDEFAULT;

	/**
	 * The default value of the '{@link #getMode() <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMode()
	 * @generated
	 * @ordered
	 */
	protected static final ModeEnum MODE_EDEFAULT = ModeEnum.DISCONNECTED;

	/**
	 * The cached value of the '{@link #getMode() <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMode()
	 * @generated
	 * @ordered
	 */
	protected ModeEnum mode = MODE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParameterSetMotionDeviceTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUARoboticsProfilePackage.Literals.PARAMETER_SET_MOTION_DEVICE_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isOnPath() {
		return onPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOnPath(boolean newOnPath) {
		boolean oldOnPath = onPath;
		onPath = newOnPath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__ON_PATH, oldOnPath, onPath));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isInControl() {
		return inControl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInControl(boolean newInControl) {
		boolean oldInControl = inControl;
		inControl = newInControl;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__IN_CONTROL, oldInControl, inControl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getSpeedOverride() {
		return speedOverride;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpeedOverride(double newSpeedOverride) {
		double oldSpeedOverride = speedOverride;
		speedOverride = newSpeedOverride;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__SPEED_OVERRIDE, oldSpeedOverride, speedOverride));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isNotCS_isPowerButtonPressed() {
		return notCS_isPowerButtonPressed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotCS_isPowerButtonPressed(boolean newNotCS_isPowerButtonPressed) {
		boolean oldNotCS_isPowerButtonPressed = notCS_isPowerButtonPressed;
		notCS_isPowerButtonPressed = newNotCS_isPowerButtonPressed;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_IS_POWER_BUTTON_PRESSED, oldNotCS_isPowerButtonPressed, notCS_isPowerButtonPressed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getNotCS__RobotIntensity() {
		return notCS__RobotIntensity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotCS__RobotIntensity(double newNotCS__RobotIntensity) {
		double oldNotCS__RobotIntensity = notCS__RobotIntensity;
		notCS__RobotIntensity = newNotCS__RobotIntensity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_ROBOT_INTENSITY, oldNotCS__RobotIntensity, notCS__RobotIntensity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isNotCS_isTeachButtonPressed() {
		return notCS_isTeachButtonPressed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotCS_isTeachButtonPressed(boolean newNotCS_isTeachButtonPressed) {
		boolean oldNotCS_isTeachButtonPressed = notCS_isTeachButtonPressed;
		notCS_isTeachButtonPressed = newNotCS_isTeachButtonPressed;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_IS_TEACH_BUTTON_PRESSED, oldNotCS_isTeachButtonPressed, notCS_isTeachButtonPressed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isNotCS_isPowerOnRobot() {
		return notCS_isPowerOnRobot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotCS_isPowerOnRobot(boolean newNotCS_isPowerOnRobot) {
		boolean oldNotCS_isPowerOnRobot = notCS_isPowerOnRobot;
		notCS_isPowerOnRobot = newNotCS_isPowerOnRobot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_IS_POWER_ON_ROBOT, oldNotCS_isPowerOnRobot, notCS_isPowerOnRobot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModeEnum getMode() {
		return mode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMode(ModeEnum newMode) {
		ModeEnum oldMode = mode;
		mode = newMode == null ? MODE_EDEFAULT : newMode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__MODE, oldMode, mode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__ON_PATH:
				return isOnPath();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__IN_CONTROL:
				return isInControl();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__SPEED_OVERRIDE:
				return getSpeedOverride();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_IS_POWER_BUTTON_PRESSED:
				return isNotCS_isPowerButtonPressed();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_ROBOT_INTENSITY:
				return getNotCS__RobotIntensity();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_IS_TEACH_BUTTON_PRESSED:
				return isNotCS_isTeachButtonPressed();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_IS_POWER_ON_ROBOT:
				return isNotCS_isPowerOnRobot();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__MODE:
				return getMode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__ON_PATH:
				setOnPath((Boolean)newValue);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__IN_CONTROL:
				setInControl((Boolean)newValue);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__SPEED_OVERRIDE:
				setSpeedOverride((Double)newValue);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_IS_POWER_BUTTON_PRESSED:
				setNotCS_isPowerButtonPressed((Boolean)newValue);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_ROBOT_INTENSITY:
				setNotCS__RobotIntensity((Double)newValue);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_IS_TEACH_BUTTON_PRESSED:
				setNotCS_isTeachButtonPressed((Boolean)newValue);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_IS_POWER_ON_ROBOT:
				setNotCS_isPowerOnRobot((Boolean)newValue);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__MODE:
				setMode((ModeEnum)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__ON_PATH:
				setOnPath(ON_PATH_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__IN_CONTROL:
				setInControl(IN_CONTROL_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__SPEED_OVERRIDE:
				setSpeedOverride(SPEED_OVERRIDE_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_IS_POWER_BUTTON_PRESSED:
				setNotCS_isPowerButtonPressed(NOT_CS_IS_POWER_BUTTON_PRESSED_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_ROBOT_INTENSITY:
				setNotCS__RobotIntensity(NOT_CS_ROBOT_INTENSITY_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_IS_TEACH_BUTTON_PRESSED:
				setNotCS_isTeachButtonPressed(NOT_CS_IS_TEACH_BUTTON_PRESSED_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_IS_POWER_ON_ROBOT:
				setNotCS_isPowerOnRobot(NOT_CS_IS_POWER_ON_ROBOT_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__MODE:
				setMode(MODE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__ON_PATH:
				return onPath != ON_PATH_EDEFAULT;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__IN_CONTROL:
				return inControl != IN_CONTROL_EDEFAULT;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__SPEED_OVERRIDE:
				return speedOverride != SPEED_OVERRIDE_EDEFAULT;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_IS_POWER_BUTTON_PRESSED:
				return notCS_isPowerButtonPressed != NOT_CS_IS_POWER_BUTTON_PRESSED_EDEFAULT;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_ROBOT_INTENSITY:
				return notCS__RobotIntensity != NOT_CS_ROBOT_INTENSITY_EDEFAULT;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_IS_TEACH_BUTTON_PRESSED:
				return notCS_isTeachButtonPressed != NOT_CS_IS_TEACH_BUTTON_PRESSED_EDEFAULT;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_IS_POWER_ON_ROBOT:
				return notCS_isPowerOnRobot != NOT_CS_IS_POWER_ON_ROBOT_EDEFAULT;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE__MODE:
				return mode != MODE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (onPath: ");
		result.append(onPath);
		result.append(", InControl: ");
		result.append(inControl);
		result.append(", SpeedOverride: ");
		result.append(speedOverride);
		result.append(", notCS_isPowerButtonPressed: ");
		result.append(notCS_isPowerButtonPressed);
		result.append(", notCS__RobotIntensity: ");
		result.append(notCS__RobotIntensity);
		result.append(", notCS_isTeachButtonPressed: ");
		result.append(notCS_isTeachButtonPressed);
		result.append(", notCS_isPowerOnRobot: ");
		result.append(notCS_isPowerOnRobot);
		result.append(", Mode: ");
		result.append(mode);
		result.append(')');
		return result.toString();
	}

} //ParameterSetMotionDeviceTypeImpl
