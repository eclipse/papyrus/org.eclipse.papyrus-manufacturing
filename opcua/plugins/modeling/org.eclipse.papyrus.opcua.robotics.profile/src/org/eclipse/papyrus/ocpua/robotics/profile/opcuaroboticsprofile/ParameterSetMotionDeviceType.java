/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter Set Motion Device Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * � Variable OnPath: The variable OnPath is true if the motion device is on or near enough the planned program path such that program execution can continue. If the MotionDevice deviates too much from this path in case of errors or an emergency stop, this value becomes false. If OnPath is false, the motion device needs repositioning to continue program execution.
 * � Variable InControl: The variable InControl provides the information if the actuators (in most cases a motor) of the motion device are powered up and in control: "true". The motion device might be in a standstill.
 * � Variable SpeedOverride: The SpeedOverride provides the current speed setting in percent of programmed speed (0 - 100%).
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#isOnPath <em>On Path</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#isInControl <em>In Control</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#getSpeedOverride <em>Speed Override</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#isNotCS_isPowerButtonPressed <em>Not CS is Power Button Pressed</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#getNotCS__RobotIntensity <em>Not CS Robot Intensity</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#isNotCS_isTeachButtonPressed <em>Not CS is Teach Button Pressed</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#isNotCS_isPowerOnRobot <em>Not CS is Power On Robot</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#getMode <em>Mode</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetMotionDeviceType()
 * @model
 * @generated
 */
public interface ParameterSetMotionDeviceType extends EObject {
	/**
	 * Returns the value of the '<em><b>On Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>On Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>On Path</em>' attribute.
	 * @see #setOnPath(boolean)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetMotionDeviceType_OnPath()
	 * @model dataType="org.eclipse.uml2.types.Boolean" ordered="false"
	 * @generated
	 */
	boolean isOnPath();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#isOnPath <em>On Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>On Path</em>' attribute.
	 * @see #isOnPath()
	 * @generated
	 */
	void setOnPath(boolean value);

	/**
	 * Returns the value of the '<em><b>In Control</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In Control</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In Control</em>' attribute.
	 * @see #setInControl(boolean)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetMotionDeviceType_InControl()
	 * @model dataType="org.eclipse.uml2.types.Boolean" ordered="false"
	 * @generated
	 */
	boolean isInControl();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#isInControl <em>In Control</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>In Control</em>' attribute.
	 * @see #isInControl()
	 * @generated
	 */
	void setInControl(boolean value);

	/**
	 * Returns the value of the '<em><b>Speed Override</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Speed Override</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Speed Override</em>' attribute.
	 * @see #setSpeedOverride(double)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetMotionDeviceType_SpeedOverride()
	 * @model dataType="org.eclipse.uml2.types.Real" required="true" ordered="false"
	 * @generated
	 */
	double getSpeedOverride();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#getSpeedOverride <em>Speed Override</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Speed Override</em>' attribute.
	 * @see #getSpeedOverride()
	 * @generated
	 */
	void setSpeedOverride(double value);

	/**
	 * Returns the value of the '<em><b>Not CS is Power Button Pressed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not CS is Power Button Pressed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not CS is Power Button Pressed</em>' attribute.
	 * @see #setNotCS_isPowerButtonPressed(boolean)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetMotionDeviceType_NotCS_isPowerButtonPressed()
	 * @model dataType="org.eclipse.uml2.types.Boolean" ordered="false"
	 * @generated
	 */
	boolean isNotCS_isPowerButtonPressed();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#isNotCS_isPowerButtonPressed <em>Not CS is Power Button Pressed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not CS is Power Button Pressed</em>' attribute.
	 * @see #isNotCS_isPowerButtonPressed()
	 * @generated
	 */
	void setNotCS_isPowerButtonPressed(boolean value);

	/**
	 * Returns the value of the '<em><b>Not CS Robot Intensity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not CS Robot Intensity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not CS Robot Intensity</em>' attribute.
	 * @see #setNotCS__RobotIntensity(double)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetMotionDeviceType_NotCS__RobotIntensity()
	 * @model dataType="org.eclipse.uml2.types.Real" required="true" ordered="false"
	 * @generated
	 */
	double getNotCS__RobotIntensity();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#getNotCS__RobotIntensity <em>Not CS Robot Intensity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not CS Robot Intensity</em>' attribute.
	 * @see #getNotCS__RobotIntensity()
	 * @generated
	 */
	void setNotCS__RobotIntensity(double value);

	/**
	 * Returns the value of the '<em><b>Not CS is Teach Button Pressed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not CS is Teach Button Pressed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not CS is Teach Button Pressed</em>' attribute.
	 * @see #setNotCS_isTeachButtonPressed(boolean)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetMotionDeviceType_NotCS_isTeachButtonPressed()
	 * @model dataType="org.eclipse.uml2.types.Boolean" ordered="false"
	 * @generated
	 */
	boolean isNotCS_isTeachButtonPressed();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#isNotCS_isTeachButtonPressed <em>Not CS is Teach Button Pressed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not CS is Teach Button Pressed</em>' attribute.
	 * @see #isNotCS_isTeachButtonPressed()
	 * @generated
	 */
	void setNotCS_isTeachButtonPressed(boolean value);

	/**
	 * Returns the value of the '<em><b>Not CS is Power On Robot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not CS is Power On Robot</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not CS is Power On Robot</em>' attribute.
	 * @see #setNotCS_isPowerOnRobot(boolean)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetMotionDeviceType_NotCS_isPowerOnRobot()
	 * @model dataType="org.eclipse.uml2.types.Boolean" ordered="false"
	 * @generated
	 */
	boolean isNotCS_isPowerOnRobot();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#isNotCS_isPowerOnRobot <em>Not CS is Power On Robot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not CS is Power On Robot</em>' attribute.
	 * @see #isNotCS_isPowerOnRobot()
	 * @generated
	 */
	void setNotCS_isPowerOnRobot(boolean value);

	/**
	 * Returns the value of the '<em><b>Mode</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ModeEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mode</em>' attribute.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ModeEnum
	 * @see #setMode(ModeEnum)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getParameterSetMotionDeviceType_Mode()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	ModeEnum getMode();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType#getMode <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mode</em>' attribute.
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ModeEnum
	 * @see #getMode()
	 * @generated
	 */
	void setMode(ModeEnum value);

} // ParameterSetMotionDeviceType
