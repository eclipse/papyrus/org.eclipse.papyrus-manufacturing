/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotorType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotorType;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ComponentTypeImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Motor Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MotorTypeImpl#getParameterSetMotorType <em>Parameter Set Motor Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MotorTypeImpl extends ComponentTypeImpl implements MotorType {
	/**
	 * The cached value of the '{@link #getParameterSetMotorType() <em>Parameter Set Motor Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterSetMotorType()
	 * @generated
	 * @ordered
	 */
	protected ParameterSetMotorType parameterSetMotorType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MotorTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUARoboticsProfilePackage.Literals.MOTOR_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterSetMotorType getParameterSetMotorType() {
		return parameterSetMotorType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParameterSetMotorType(ParameterSetMotorType newParameterSetMotorType, NotificationChain msgs) {
		ParameterSetMotorType oldParameterSetMotorType = parameterSetMotorType;
		parameterSetMotorType = newParameterSetMotorType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.MOTOR_TYPE__PARAMETER_SET_MOTOR_TYPE, oldParameterSetMotorType, newParameterSetMotorType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterSetMotorType(ParameterSetMotorType newParameterSetMotorType) {
		if (newParameterSetMotorType != parameterSetMotorType) {
			NotificationChain msgs = null;
			if (parameterSetMotorType != null)
				msgs = ((InternalEObject)parameterSetMotorType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.MOTOR_TYPE__PARAMETER_SET_MOTOR_TYPE, null, msgs);
			if (newParameterSetMotorType != null)
				msgs = ((InternalEObject)newParameterSetMotorType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.MOTOR_TYPE__PARAMETER_SET_MOTOR_TYPE, null, msgs);
			msgs = basicSetParameterSetMotorType(newParameterSetMotorType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.MOTOR_TYPE__PARAMETER_SET_MOTOR_TYPE, newParameterSetMotorType, newParameterSetMotorType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.MOTOR_TYPE__PARAMETER_SET_MOTOR_TYPE:
				return basicSetParameterSetMotorType(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.MOTOR_TYPE__PARAMETER_SET_MOTOR_TYPE:
				return getParameterSetMotorType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.MOTOR_TYPE__PARAMETER_SET_MOTOR_TYPE:
				setParameterSetMotorType((ParameterSetMotorType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.MOTOR_TYPE__PARAMETER_SET_MOTOR_TYPE:
				setParameterSetMotorType((ParameterSetMotorType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.MOTOR_TYPE__PARAMETER_SET_MOTOR_TYPE:
				return parameterSetMotorType != null;
		}
		return super.eIsSet(featureID);
	}

} //MotorTypeImpl
