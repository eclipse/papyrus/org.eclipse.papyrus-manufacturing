/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ControllerType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceSystemType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.SafetyStateType;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ComponentTypeImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Motion Device System Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MotionDeviceSystemTypeImpl#getMotionDevices <em>Motion Devices</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MotionDeviceSystemTypeImpl#getControllers <em>Controllers</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.MotionDeviceSystemTypeImpl#getSafetyStates <em>Safety States</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MotionDeviceSystemTypeImpl extends ComponentTypeImpl implements MotionDeviceSystemType {
	/**
	 * The cached value of the '{@link #getMotionDevices() <em>Motion Devices</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMotionDevices()
	 * @generated
	 * @ordered
	 */
	protected EList<MotionDeviceType> motionDevices;

	/**
	 * The cached value of the '{@link #getControllers() <em>Controllers</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getControllers()
	 * @generated
	 * @ordered
	 */
	protected EList<ControllerType> controllers;

	/**
	 * The cached value of the '{@link #getSafetyStates() <em>Safety States</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSafetyStates()
	 * @generated
	 * @ordered
	 */
	protected EList<SafetyStateType> safetyStates;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MotionDeviceSystemTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUARoboticsProfilePackage.Literals.MOTION_DEVICE_SYSTEM_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MotionDeviceType> getMotionDevices() {
		if (motionDevices == null) {
			motionDevices = new EObjectResolvingEList<MotionDeviceType>(MotionDeviceType.class, this, OPCUARoboticsProfilePackage.MOTION_DEVICE_SYSTEM_TYPE__MOTION_DEVICES);
		}
		return motionDevices;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControllerType> getControllers() {
		if (controllers == null) {
			controllers = new EObjectResolvingEList<ControllerType>(ControllerType.class, this, OPCUARoboticsProfilePackage.MOTION_DEVICE_SYSTEM_TYPE__CONTROLLERS);
		}
		return controllers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SafetyStateType> getSafetyStates() {
		if (safetyStates == null) {
			safetyStates = new EObjectResolvingEList<SafetyStateType>(SafetyStateType.class, this, OPCUARoboticsProfilePackage.MOTION_DEVICE_SYSTEM_TYPE__SAFETY_STATES);
		}
		return safetyStates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_SYSTEM_TYPE__MOTION_DEVICES:
				return getMotionDevices();
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_SYSTEM_TYPE__CONTROLLERS:
				return getControllers();
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_SYSTEM_TYPE__SAFETY_STATES:
				return getSafetyStates();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_SYSTEM_TYPE__MOTION_DEVICES:
				getMotionDevices().clear();
				getMotionDevices().addAll((Collection<? extends MotionDeviceType>)newValue);
				return;
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_SYSTEM_TYPE__CONTROLLERS:
				getControllers().clear();
				getControllers().addAll((Collection<? extends ControllerType>)newValue);
				return;
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_SYSTEM_TYPE__SAFETY_STATES:
				getSafetyStates().clear();
				getSafetyStates().addAll((Collection<? extends SafetyStateType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_SYSTEM_TYPE__MOTION_DEVICES:
				getMotionDevices().clear();
				return;
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_SYSTEM_TYPE__CONTROLLERS:
				getControllers().clear();
				return;
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_SYSTEM_TYPE__SAFETY_STATES:
				getSafetyStates().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_SYSTEM_TYPE__MOTION_DEVICES:
				return motionDevices != null && !motionDevices.isEmpty();
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_SYSTEM_TYPE__CONTROLLERS:
				return controllers != null && !controllers.isEmpty();
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_SYSTEM_TYPE__SAFETY_STATES:
				return safetyStates != null && !safetyStates.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //MotionDeviceSystemTypeImpl
