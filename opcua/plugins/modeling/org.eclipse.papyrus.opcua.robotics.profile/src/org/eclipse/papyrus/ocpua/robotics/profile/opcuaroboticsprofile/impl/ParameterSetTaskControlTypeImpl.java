/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ExecutionModeEnumeration;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetTaskControlType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameter Set Task Control Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetTaskControlTypeImpl#getExecutionMode <em>Execution Mode</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetTaskControlTypeImpl#isTaskProgramLoaded <em>Task Program Loaded</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.ParameterSetTaskControlTypeImpl#getTaskProgramName <em>Task Program Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ParameterSetTaskControlTypeImpl extends MinimalEObjectImpl.Container implements ParameterSetTaskControlType {
	/**
	 * The default value of the '{@link #getExecutionMode() <em>Execution Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionMode()
	 * @generated
	 * @ordered
	 */
	protected static final ExecutionModeEnumeration EXECUTION_MODE_EDEFAULT = ExecutionModeEnumeration.CYCLE;

	/**
	 * The cached value of the '{@link #getExecutionMode() <em>Execution Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionMode()
	 * @generated
	 * @ordered
	 */
	protected ExecutionModeEnumeration executionMode = EXECUTION_MODE_EDEFAULT;

	/**
	 * The default value of the '{@link #isTaskProgramLoaded() <em>Task Program Loaded</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTaskProgramLoaded()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TASK_PROGRAM_LOADED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isTaskProgramLoaded() <em>Task Program Loaded</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTaskProgramLoaded()
	 * @generated
	 * @ordered
	 */
	protected boolean taskProgramLoaded = TASK_PROGRAM_LOADED_EDEFAULT;

	/**
	 * The default value of the '{@link #getTaskProgramName() <em>Task Program Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskProgramName()
	 * @generated
	 * @ordered
	 */
	protected static final String TASK_PROGRAM_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTaskProgramName() <em>Task Program Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskProgramName()
	 * @generated
	 * @ordered
	 */
	protected String taskProgramName = TASK_PROGRAM_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParameterSetTaskControlTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUARoboticsProfilePackage.Literals.PARAMETER_SET_TASK_CONTROL_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutionModeEnumeration getExecutionMode() {
		return executionMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExecutionMode(ExecutionModeEnumeration newExecutionMode) {
		ExecutionModeEnumeration oldExecutionMode = executionMode;
		executionMode = newExecutionMode == null ? EXECUTION_MODE_EDEFAULT : newExecutionMode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_TASK_CONTROL_TYPE__EXECUTION_MODE, oldExecutionMode, executionMode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isTaskProgramLoaded() {
		return taskProgramLoaded;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaskProgramLoaded(boolean newTaskProgramLoaded) {
		boolean oldTaskProgramLoaded = taskProgramLoaded;
		taskProgramLoaded = newTaskProgramLoaded;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_TASK_CONTROL_TYPE__TASK_PROGRAM_LOADED, oldTaskProgramLoaded, taskProgramLoaded));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTaskProgramName() {
		return taskProgramName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaskProgramName(String newTaskProgramName) {
		String oldTaskProgramName = taskProgramName;
		taskProgramName = newTaskProgramName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.PARAMETER_SET_TASK_CONTROL_TYPE__TASK_PROGRAM_NAME, oldTaskProgramName, taskProgramName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.PARAMETER_SET_TASK_CONTROL_TYPE__EXECUTION_MODE:
				return getExecutionMode();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_TASK_CONTROL_TYPE__TASK_PROGRAM_LOADED:
				return isTaskProgramLoaded();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_TASK_CONTROL_TYPE__TASK_PROGRAM_NAME:
				return getTaskProgramName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.PARAMETER_SET_TASK_CONTROL_TYPE__EXECUTION_MODE:
				setExecutionMode((ExecutionModeEnumeration)newValue);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_TASK_CONTROL_TYPE__TASK_PROGRAM_LOADED:
				setTaskProgramLoaded((Boolean)newValue);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_TASK_CONTROL_TYPE__TASK_PROGRAM_NAME:
				setTaskProgramName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.PARAMETER_SET_TASK_CONTROL_TYPE__EXECUTION_MODE:
				setExecutionMode(EXECUTION_MODE_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_TASK_CONTROL_TYPE__TASK_PROGRAM_LOADED:
				setTaskProgramLoaded(TASK_PROGRAM_LOADED_EDEFAULT);
				return;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_TASK_CONTROL_TYPE__TASK_PROGRAM_NAME:
				setTaskProgramName(TASK_PROGRAM_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.PARAMETER_SET_TASK_CONTROL_TYPE__EXECUTION_MODE:
				return executionMode != EXECUTION_MODE_EDEFAULT;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_TASK_CONTROL_TYPE__TASK_PROGRAM_LOADED:
				return taskProgramLoaded != TASK_PROGRAM_LOADED_EDEFAULT;
			case OPCUARoboticsProfilePackage.PARAMETER_SET_TASK_CONTROL_TYPE__TASK_PROGRAM_NAME:
				return TASK_PROGRAM_NAME_EDEFAULT == null ? taskProgramName != null : !TASK_PROGRAM_NAME_EDEFAULT.equals(taskProgramName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (ExecutionMode: ");
		result.append(executionMode);
		result.append(", TaskProgramLoaded: ");
		result.append(taskProgramLoaded);
		result.append(", TaskProgramName: ");
		result.append(taskProgramName);
		result.append(')');
		return result.toString();
	}

} //ParameterSetTaskControlTypeImpl
