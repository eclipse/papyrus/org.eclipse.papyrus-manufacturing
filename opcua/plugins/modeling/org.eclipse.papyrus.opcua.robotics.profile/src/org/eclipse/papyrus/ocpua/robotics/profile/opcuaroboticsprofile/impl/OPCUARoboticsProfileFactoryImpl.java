/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OPCUARoboticsProfileFactoryImpl extends EFactoryImpl implements OPCUARoboticsProfileFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OPCUARoboticsProfileFactory init() {
		try {
			OPCUARoboticsProfileFactory theOPCUARoboticsProfileFactory = (OPCUARoboticsProfileFactory)EPackage.Registry.INSTANCE.getEFactory(OPCUARoboticsProfilePackage.eNS_URI);
			if (theOPCUARoboticsProfileFactory != null) {
				return theOPCUARoboticsProfileFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OPCUARoboticsProfileFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPCUARoboticsProfileFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case OPCUARoboticsProfilePackage.LOAD_TYPE: return createLoadType();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTOR_TYPE: return createParameterSetMotorType();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_CONTROLLER_TYPE: return createParameterSetControllerType();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_TASK_CONTROL_TYPE: return createParameterSetTaskControlType();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_SAFETY_STATE_TYPE: return createParameterSetSafetyStateType();
			case OPCUARoboticsProfilePackage.GEAR_TYPE: return createGearType();
			case OPCUARoboticsProfilePackage.MOTOR_TYPE: return createMotorType();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_AXIS_TYPE: return createParameterSetAxisType();
			case OPCUARoboticsProfilePackage.PARAMETER_SET_MOTION_DEVICE_TYPE: return createParameterSetMotionDeviceType();
			case OPCUARoboticsProfilePackage.POWER_TRAIN_TYPE: return createPowerTrainType();
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_SYSTEM_TYPE: return createMotionDeviceSystemType();
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_TYPE: return createMotionDeviceType();
			case OPCUARoboticsProfilePackage.AXIS_TYPE: return createAxisType();
			case OPCUARoboticsProfilePackage.CONTROLLER_TYPE: return createControllerType();
			case OPCUARoboticsProfilePackage.TASK_CONTROL_TYPE: return createTaskControlType();
			case OPCUARoboticsProfilePackage.SAFETY_STATE_TYPE: return createSafetyStateType();
			case OPCUARoboticsProfilePackage.EMERGENCY_STOP_FUNCTION_TYPE: return createEmergencyStopFunctionType();
			case OPCUARoboticsProfilePackage.PROTECTIVE_STOP_FUNCTION_TYPE: return createProtectiveStopFunctionType();
			case OPCUARoboticsProfilePackage.REFERENCES: return createReferences();
			case OPCUARoboticsProfilePackage.HIERARCHICAL_REFERENCES: return createHierarchicalReferences();
			case OPCUARoboticsProfilePackage.CONTROLS: return createControls();
			case OPCUARoboticsProfilePackage.IS_DRIVEN_BY: return createIsDrivenBy();
			case OPCUARoboticsProfilePackage.MOVES: return createMoves();
			case OPCUARoboticsProfilePackage.REQUIRES: return createRequires();
			case OPCUARoboticsProfilePackage.NON_HIERARCHICAL_REFERENCES: return createNonHierarchicalReferences();
			case OPCUARoboticsProfilePackage.HAS_SAFETY_STATES: return createHasSafetyStates();
			case OPCUARoboticsProfilePackage.HAS_SLAVES: return createHasSlaves();
			case OPCUARoboticsProfilePackage.IS_CONNECTED_TO: return createIsConnectedTo();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case OPCUARoboticsProfilePackage.EXECUTION_MODE_ENUMERATION:
				return createExecutionModeEnumerationFromString(eDataType, initialValue);
			case OPCUARoboticsProfilePackage.OPERATIONAL_MODE_ENUMERATION:
				return createOperationalModeEnumerationFromString(eDataType, initialValue);
			case OPCUARoboticsProfilePackage.AXIS_MOTION_PROFILE_ENUMERATION:
				return createAxisMotionProfileEnumerationFromString(eDataType, initialValue);
			case OPCUARoboticsProfilePackage.AXIS_STATE_ENUMERATION:
				return createAxisStateEnumerationFromString(eDataType, initialValue);
			case OPCUARoboticsProfilePackage.MODE_ENUM:
				return createModeEnumFromString(eDataType, initialValue);
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_CATEGORY_ENUM:
				return createMotionDeviceCategoryEnumFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case OPCUARoboticsProfilePackage.EXECUTION_MODE_ENUMERATION:
				return convertExecutionModeEnumerationToString(eDataType, instanceValue);
			case OPCUARoboticsProfilePackage.OPERATIONAL_MODE_ENUMERATION:
				return convertOperationalModeEnumerationToString(eDataType, instanceValue);
			case OPCUARoboticsProfilePackage.AXIS_MOTION_PROFILE_ENUMERATION:
				return convertAxisMotionProfileEnumerationToString(eDataType, instanceValue);
			case OPCUARoboticsProfilePackage.AXIS_STATE_ENUMERATION:
				return convertAxisStateEnumerationToString(eDataType, instanceValue);
			case OPCUARoboticsProfilePackage.MODE_ENUM:
				return convertModeEnumToString(eDataType, instanceValue);
			case OPCUARoboticsProfilePackage.MOTION_DEVICE_CATEGORY_ENUM:
				return convertMotionDeviceCategoryEnumToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LoadType createLoadType() {
		LoadTypeImpl loadType = new LoadTypeImpl();
		return loadType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterSetMotorType createParameterSetMotorType() {
		ParameterSetMotorTypeImpl parameterSetMotorType = new ParameterSetMotorTypeImpl();
		return parameterSetMotorType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterSetControllerType createParameterSetControllerType() {
		ParameterSetControllerTypeImpl parameterSetControllerType = new ParameterSetControllerTypeImpl();
		return parameterSetControllerType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterSetTaskControlType createParameterSetTaskControlType() {
		ParameterSetTaskControlTypeImpl parameterSetTaskControlType = new ParameterSetTaskControlTypeImpl();
		return parameterSetTaskControlType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterSetSafetyStateType createParameterSetSafetyStateType() {
		ParameterSetSafetyStateTypeImpl parameterSetSafetyStateType = new ParameterSetSafetyStateTypeImpl();
		return parameterSetSafetyStateType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GearType createGearType() {
		GearTypeImpl gearType = new GearTypeImpl();
		return gearType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MotorType createMotorType() {
		MotorTypeImpl motorType = new MotorTypeImpl();
		return motorType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterSetAxisType createParameterSetAxisType() {
		ParameterSetAxisTypeImpl parameterSetAxisType = new ParameterSetAxisTypeImpl();
		return parameterSetAxisType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterSetMotionDeviceType createParameterSetMotionDeviceType() {
		ParameterSetMotionDeviceTypeImpl parameterSetMotionDeviceType = new ParameterSetMotionDeviceTypeImpl();
		return parameterSetMotionDeviceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PowerTrainType createPowerTrainType() {
		PowerTrainTypeImpl powerTrainType = new PowerTrainTypeImpl();
		return powerTrainType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MotionDeviceSystemType createMotionDeviceSystemType() {
		MotionDeviceSystemTypeImpl motionDeviceSystemType = new MotionDeviceSystemTypeImpl();
		return motionDeviceSystemType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MotionDeviceType createMotionDeviceType() {
		MotionDeviceTypeImpl motionDeviceType = new MotionDeviceTypeImpl();
		return motionDeviceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AxisType createAxisType() {
		AxisTypeImpl axisType = new AxisTypeImpl();
		return axisType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControllerType createControllerType() {
		ControllerTypeImpl controllerType = new ControllerTypeImpl();
		return controllerType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskControlType createTaskControlType() {
		TaskControlTypeImpl taskControlType = new TaskControlTypeImpl();
		return taskControlType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SafetyStateType createSafetyStateType() {
		SafetyStateTypeImpl safetyStateType = new SafetyStateTypeImpl();
		return safetyStateType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EmergencyStopFunctionType createEmergencyStopFunctionType() {
		EmergencyStopFunctionTypeImpl emergencyStopFunctionType = new EmergencyStopFunctionTypeImpl();
		return emergencyStopFunctionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProtectiveStopFunctionType createProtectiveStopFunctionType() {
		ProtectiveStopFunctionTypeImpl protectiveStopFunctionType = new ProtectiveStopFunctionTypeImpl();
		return protectiveStopFunctionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public References createReferences() {
		ReferencesImpl references = new ReferencesImpl();
		return references;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HierarchicalReferences createHierarchicalReferences() {
		HierarchicalReferencesImpl hierarchicalReferences = new HierarchicalReferencesImpl();
		return hierarchicalReferences;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Controls createControls() {
		ControlsImpl controls = new ControlsImpl();
		return controls;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsDrivenBy createIsDrivenBy() {
		IsDrivenByImpl isDrivenBy = new IsDrivenByImpl();
		return isDrivenBy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Moves createMoves() {
		MovesImpl moves = new MovesImpl();
		return moves;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Requires createRequires() {
		RequiresImpl requires = new RequiresImpl();
		return requires;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NonHierarchicalReferences createNonHierarchicalReferences() {
		NonHierarchicalReferencesImpl nonHierarchicalReferences = new NonHierarchicalReferencesImpl();
		return nonHierarchicalReferences;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasSafetyStates createHasSafetyStates() {
		HasSafetyStatesImpl hasSafetyStates = new HasSafetyStatesImpl();
		return hasSafetyStates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasSlaves createHasSlaves() {
		HasSlavesImpl hasSlaves = new HasSlavesImpl();
		return hasSlaves;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsConnectedTo createIsConnectedTo() {
		IsConnectedToImpl isConnectedTo = new IsConnectedToImpl();
		return isConnectedTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutionModeEnumeration createExecutionModeEnumerationFromString(EDataType eDataType, String initialValue) {
		ExecutionModeEnumeration result = ExecutionModeEnumeration.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertExecutionModeEnumerationToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationalModeEnumeration createOperationalModeEnumerationFromString(EDataType eDataType, String initialValue) {
		OperationalModeEnumeration result = OperationalModeEnumeration.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertOperationalModeEnumerationToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AxisMotionProfileEnumeration createAxisMotionProfileEnumerationFromString(EDataType eDataType, String initialValue) {
		AxisMotionProfileEnumeration result = AxisMotionProfileEnumeration.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAxisMotionProfileEnumerationToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AxisStateEnumeration createAxisStateEnumerationFromString(EDataType eDataType, String initialValue) {
		AxisStateEnumeration result = AxisStateEnumeration.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAxisStateEnumerationToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModeEnum createModeEnumFromString(EDataType eDataType, String initialValue) {
		ModeEnum result = ModeEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertModeEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MotionDeviceCategoryEnum createMotionDeviceCategoryEnumFromString(EDataType eDataType, String initialValue) {
		MotionDeviceCategoryEnum result = MotionDeviceCategoryEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMotionDeviceCategoryEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPCUARoboticsProfilePackage getOPCUARoboticsProfilePackage() {
		return (OPCUARoboticsProfilePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static OPCUARoboticsProfilePackage getPackage() {
		return OPCUARoboticsProfilePackage.eINSTANCE;
	}

} //OPCUARoboticsProfileFactoryImpl
