/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetTaskControlType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.TaskControlType;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ComponentTypeImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Task Control Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.TaskControlTypeImpl#getParameterSetTaskContolType <em>Parameter Set Task Contol Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TaskControlTypeImpl extends ComponentTypeImpl implements TaskControlType {
	/**
	 * The cached value of the '{@link #getParameterSetTaskContolType() <em>Parameter Set Task Contol Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterSetTaskContolType()
	 * @generated
	 * @ordered
	 */
	protected ParameterSetTaskControlType parameterSetTaskContolType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TaskControlTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUARoboticsProfilePackage.Literals.TASK_CONTROL_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterSetTaskControlType getParameterSetTaskContolType() {
		return parameterSetTaskContolType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParameterSetTaskContolType(ParameterSetTaskControlType newParameterSetTaskContolType, NotificationChain msgs) {
		ParameterSetTaskControlType oldParameterSetTaskContolType = parameterSetTaskContolType;
		parameterSetTaskContolType = newParameterSetTaskContolType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.TASK_CONTROL_TYPE__PARAMETER_SET_TASK_CONTOL_TYPE, oldParameterSetTaskContolType, newParameterSetTaskContolType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterSetTaskContolType(ParameterSetTaskControlType newParameterSetTaskContolType) {
		if (newParameterSetTaskContolType != parameterSetTaskContolType) {
			NotificationChain msgs = null;
			if (parameterSetTaskContolType != null)
				msgs = ((InternalEObject)parameterSetTaskContolType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.TASK_CONTROL_TYPE__PARAMETER_SET_TASK_CONTOL_TYPE, null, msgs);
			if (newParameterSetTaskContolType != null)
				msgs = ((InternalEObject)newParameterSetTaskContolType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPCUARoboticsProfilePackage.TASK_CONTROL_TYPE__PARAMETER_SET_TASK_CONTOL_TYPE, null, msgs);
			msgs = basicSetParameterSetTaskContolType(newParameterSetTaskContolType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.TASK_CONTROL_TYPE__PARAMETER_SET_TASK_CONTOL_TYPE, newParameterSetTaskContolType, newParameterSetTaskContolType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.TASK_CONTROL_TYPE__PARAMETER_SET_TASK_CONTOL_TYPE:
				return basicSetParameterSetTaskContolType(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.TASK_CONTROL_TYPE__PARAMETER_SET_TASK_CONTOL_TYPE:
				return getParameterSetTaskContolType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.TASK_CONTROL_TYPE__PARAMETER_SET_TASK_CONTOL_TYPE:
				setParameterSetTaskContolType((ParameterSetTaskControlType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.TASK_CONTROL_TYPE__PARAMETER_SET_TASK_CONTOL_TYPE:
				setParameterSetTaskContolType((ParameterSetTaskControlType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.TASK_CONTROL_TYPE__PARAMETER_SET_TASK_CONTOL_TYPE:
				return parameterSetTaskContolType != null;
		}
		return super.eIsSet(featureID);
	}

} //TaskControlTypeImpl
