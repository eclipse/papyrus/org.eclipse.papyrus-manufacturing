/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisMotionProfileEnumeration;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisStateEnumeration;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.AxisType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ControllerType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.Controls;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.EmergencyStopFunctionType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ExecutionModeEnumeration;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.GearType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.HasSafetyStates;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.HasSlaves;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.HierarchicalReferences;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.IsConnectedTo;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.IsDrivenBy;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.LoadType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ModeEnum;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceCategoryEnum;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceSystemType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotionDeviceType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotorType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.Moves;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.NonHierarchicalReferences;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfileFactory;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.OPC_UA_Robotics_CS_LibraryPackage;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OperationalModeEnumeration;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetAxisType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetControllerType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotionDeviceType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetMotorType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetSafetyStateType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ParameterSetTaskControlType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.PowerTrainType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.ProtectiveStopFunctionType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.References;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.Requires;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.SafetyStateType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.TaskControlType;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.OPCUADIProfilePackage;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.OPC_UA_DI_Library.OPC_UA_DI_LibraryPackage;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.OPC_UA_DI_Library.impl.OPC_UA_DI_LibraryPackageImpl;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.OPCUADIProfilePackageImpl;

import org.eclipse.papyrus.opcua.opcuaprofile.OPCUAProfilePackage;

import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.OPC_UA_LibraryPackage;

import org.eclipse.papyrus.opcua.opcuaprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl;

import org.eclipse.papyrus.opcua.opcuaprofile.impl.OPCUAProfilePackageImpl;

import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OPCUARoboticsProfilePackageImpl extends EPackageImpl implements OPCUARoboticsProfilePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass loadTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterSetMotorTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterSetControllerTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterSetTaskControlTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterSetSafetyStateTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gearTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass motorTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterSetAxisTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterSetMotionDeviceTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass powerTrainTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass motionDeviceSystemTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass motionDeviceTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass axisTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass controllerTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass taskControlTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass safetyStateTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass emergencyStopFunctionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass protectiveStopFunctionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass referencesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hierarchicalReferencesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass controlsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isDrivenByEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass movesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requiresEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nonHierarchicalReferencesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hasSafetyStatesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hasSlavesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isConnectedToEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum executionModeEnumerationEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum operationalModeEnumerationEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum axisMotionProfileEnumerationEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum axisStateEnumerationEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum modeEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum motionDeviceCategoryEnumEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OPCUARoboticsProfilePackageImpl() {
		super(eNS_URI, OPCUARoboticsProfileFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link OPCUARoboticsProfilePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static OPCUARoboticsProfilePackage init() {
		if (isInited) return (OPCUARoboticsProfilePackage)EPackage.Registry.INSTANCE.getEPackage(OPCUARoboticsProfilePackage.eNS_URI);

		// Obtain or create and register package
		Object registeredOPCUARoboticsProfilePackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		OPCUARoboticsProfilePackageImpl theOPCUARoboticsProfilePackage = registeredOPCUARoboticsProfilePackage instanceof OPCUARoboticsProfilePackageImpl ? (OPCUARoboticsProfilePackageImpl)registeredOPCUARoboticsProfilePackage : new OPCUARoboticsProfilePackageImpl();

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OPC_UA_Robotics_CS_LibraryPackage.eNS_URI);
		OPC_UA_Robotics_CS_LibraryPackageImpl theOPC_UA_Robotics_CS_LibraryPackage = (OPC_UA_Robotics_CS_LibraryPackageImpl)(registeredPackage instanceof OPC_UA_Robotics_CS_LibraryPackageImpl ? registeredPackage : OPC_UA_Robotics_CS_LibraryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OPCUADIProfilePackage.eNS_URI);
		OPCUADIProfilePackageImpl theOPCUADIProfilePackage = (OPCUADIProfilePackageImpl)(registeredPackage instanceof OPCUADIProfilePackageImpl ? registeredPackage : OPCUADIProfilePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OPCUAProfilePackage.eNS_URI);
		OPCUAProfilePackageImpl theOPCUAProfilePackage = (OPCUAProfilePackageImpl)(registeredPackage instanceof OPCUAProfilePackageImpl ? registeredPackage : OPCUAProfilePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OPC_UA_LibraryPackage.eNS_URI);
		OPC_UA_LibraryPackageImpl theOPC_UA_LibraryPackage = (OPC_UA_LibraryPackageImpl)(registeredPackage instanceof OPC_UA_LibraryPackageImpl ? registeredPackage : OPC_UA_LibraryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OPC_UA_DI_LibraryPackage.eNS_URI);
		OPC_UA_DI_LibraryPackageImpl theOPC_UA_DI_LibraryPackage = (OPC_UA_DI_LibraryPackageImpl)(registeredPackage instanceof OPC_UA_DI_LibraryPackageImpl ? registeredPackage : OPC_UA_DI_LibraryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Library.OPC_UA_LibraryPackage.eNS_URI);
		org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl theOPC_UA_LibraryPackage_1 = (org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl)(registeredPackage instanceof org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Library.impl.OPC_UA_LibraryPackageImpl ? registeredPackage : org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Library.OPC_UA_LibraryPackage.eINSTANCE);

		// Create package meta-data objects
		theOPCUARoboticsProfilePackage.createPackageContents();
		theOPC_UA_Robotics_CS_LibraryPackage.createPackageContents();
		theOPCUADIProfilePackage.createPackageContents();
		theOPCUAProfilePackage.createPackageContents();
		theOPC_UA_LibraryPackage.createPackageContents();
		theOPC_UA_DI_LibraryPackage.createPackageContents();
		theOPC_UA_LibraryPackage_1.createPackageContents();

		// Initialize created meta-data
		theOPCUARoboticsProfilePackage.initializePackageContents();
		theOPC_UA_Robotics_CS_LibraryPackage.initializePackageContents();
		theOPCUADIProfilePackage.initializePackageContents();
		theOPCUAProfilePackage.initializePackageContents();
		theOPC_UA_LibraryPackage.initializePackageContents();
		theOPC_UA_DI_LibraryPackage.initializePackageContents();
		theOPC_UA_LibraryPackage_1.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theOPCUARoboticsProfilePackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(OPCUARoboticsProfilePackage.eNS_URI, theOPCUARoboticsProfilePackage);
		return theOPCUARoboticsProfilePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLoadType() {
		return loadTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLoadType_Mass() {
		return (EReference)loadTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLoadType_CenterOfMass() {
		return (EReference)loadTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLoadType_Inertia() {
		return (EReference)loadTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParameterSetMotorType() {
		return parameterSetMotorTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetMotorType_BrakeReleased() {
		return (EAttribute)parameterSetMotorTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetMotorType_EffectiveLoadRate() {
		return (EAttribute)parameterSetMotorTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetMotorType_MotorTemperature() {
		return (EAttribute)parameterSetMotorTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetMotorType_NotCS_MotorIntensity() {
		return (EAttribute)parameterSetMotorTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParameterSetControllerType() {
		return parameterSetControllerTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetControllerType_TotalPowerOnTime() {
		return (EAttribute)parameterSetControllerTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetControllerType_StartUpTime() {
		return (EAttribute)parameterSetControllerTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetControllerType_UpsState() {
		return (EAttribute)parameterSetControllerTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetControllerType_TotalEnergyConsumption() {
		return (EAttribute)parameterSetControllerTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetControllerType_CabinetFanSpeed() {
		return (EAttribute)parameterSetControllerTypeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetControllerType_CPUFanSpeed() {
		return (EAttribute)parameterSetControllerTypeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetControllerType_InputVoltage() {
		return (EAttribute)parameterSetControllerTypeEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetControllerType_Temperature() {
		return (EAttribute)parameterSetControllerTypeEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParameterSetTaskControlType() {
		return parameterSetTaskControlTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetTaskControlType_ExecutionMode() {
		return (EAttribute)parameterSetTaskControlTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetTaskControlType_TaskProgramLoaded() {
		return (EAttribute)parameterSetTaskControlTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetTaskControlType_TaskProgramName() {
		return (EAttribute)parameterSetTaskControlTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParameterSetSafetyStateType() {
		return parameterSetSafetyStateTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetSafetyStateType_EmergencyStop() {
		return (EAttribute)parameterSetSafetyStateTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetSafetyStateType_OperationalMode() {
		return (EAttribute)parameterSetSafetyStateTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetSafetyStateType_ProtectiveStop() {
		return (EAttribute)parameterSetSafetyStateTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGearType() {
		return gearTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGearType_GearRatio() {
		return (EReference)gearTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGearType_Pitch() {
		return (EReference)gearTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGearType_IsConnectedTo() {
		return (EReference)gearTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMotorType() {
		return motorTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMotorType_ParameterSetMotorType() {
		return (EReference)motorTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParameterSetAxisType() {
		return parameterSetAxisTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetAxisType_ActualAcceleration() {
		return (EAttribute)parameterSetAxisTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetAxisType_ActualPosition() {
		return (EAttribute)parameterSetAxisTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetAxisType_ActualSpeed() {
		return (EAttribute)parameterSetAxisTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetAxisType_NotCS_AxisState() {
		return (EAttribute)parameterSetAxisTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParameterSetMotionDeviceType() {
		return parameterSetMotionDeviceTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetMotionDeviceType_OnPath() {
		return (EAttribute)parameterSetMotionDeviceTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetMotionDeviceType_InControl() {
		return (EAttribute)parameterSetMotionDeviceTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetMotionDeviceType_SpeedOverride() {
		return (EAttribute)parameterSetMotionDeviceTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetMotionDeviceType_NotCS_isPowerButtonPressed() {
		return (EAttribute)parameterSetMotionDeviceTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetMotionDeviceType_NotCS__RobotIntensity() {
		return (EAttribute)parameterSetMotionDeviceTypeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetMotionDeviceType_NotCS_isTeachButtonPressed() {
		return (EAttribute)parameterSetMotionDeviceTypeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetMotionDeviceType_NotCS_isPowerOnRobot() {
		return (EAttribute)parameterSetMotionDeviceTypeEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterSetMotionDeviceType_Mode() {
		return (EAttribute)parameterSetMotionDeviceTypeEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPowerTrainType() {
		return powerTrainTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPowerTrainType_Motor() {
		return (EReference)powerTrainTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPowerTrainType_Gear() {
		return (EReference)powerTrainTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMotionDeviceSystemType() {
		return motionDeviceSystemTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMotionDeviceSystemType_MotionDevices() {
		return (EReference)motionDeviceSystemTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMotionDeviceSystemType_Controllers() {
		return (EReference)motionDeviceSystemTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMotionDeviceSystemType_SafetyStates() {
		return (EReference)motionDeviceSystemTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMotionDeviceType() {
		return motionDeviceTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMotionDeviceType_MotionDeviceCategory() {
		return (EAttribute)motionDeviceTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMotionDeviceType_ParameterSetMotionDeviceType() {
		return (EReference)motionDeviceTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMotionDeviceType_Axes() {
		return (EReference)motionDeviceTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMotionDeviceType_PowerTrains() {
		return (EReference)motionDeviceTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMotionDeviceType_AdditionalComponents() {
		return (EReference)motionDeviceTypeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMotionDeviceType_FlangeLoad() {
		return (EReference)motionDeviceTypeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAxisType() {
		return axisTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAxisType_MotionProfile() {
		return (EAttribute)axisTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAxisType_AdditionalLoad() {
		return (EReference)axisTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAxisType_ParameterSetAxisType() {
		return (EReference)axisTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getControllerType() {
		return controllerTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getControllerType_ParameterSetControllerType() {
		return (EReference)controllerTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getControllerType_Components() {
		return (EReference)controllerTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getControllerType_Software() {
		return (EReference)controllerTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getControllerType_TaskControls() {
		return (EReference)controllerTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTaskControlType() {
		return taskControlTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTaskControlType_ParameterSetTaskContolType() {
		return (EReference)taskControlTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSafetyStateType() {
		return safetyStateTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSafetyStateType_EmergencyStopFunctions() {
		return (EReference)safetyStateTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSafetyStateType_ProtectiveStopFunctions() {
		return (EReference)safetyStateTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSafetyStateType_ParameterSetSafetyStateType() {
		return (EReference)safetyStateTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEmergencyStopFunctionType() {
		return emergencyStopFunctionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEmergencyStopFunctionType_Name() {
		return (EAttribute)emergencyStopFunctionTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEmergencyStopFunctionType_Active() {
		return (EAttribute)emergencyStopFunctionTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEmergencyStopFunctionType_Base_Class() {
		return (EReference)emergencyStopFunctionTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProtectiveStopFunctionType() {
		return protectiveStopFunctionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProtectiveStopFunctionType_Name() {
		return (EAttribute)protectiveStopFunctionTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProtectiveStopFunctionType_Enabled() {
		return (EAttribute)protectiveStopFunctionTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProtectiveStopFunctionType_Active() {
		return (EAttribute)protectiveStopFunctionTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProtectiveStopFunctionType_Base_Class() {
		return (EReference)protectiveStopFunctionTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReferences() {
		return referencesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferences_Base_Association() {
		return (EReference)referencesEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferences_Base_Dependency() {
		return (EReference)referencesEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHierarchicalReferences() {
		return hierarchicalReferencesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getControls() {
		return controlsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIsDrivenBy() {
		return isDrivenByEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMoves() {
		return movesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRequires() {
		return requiresEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNonHierarchicalReferences() {
		return nonHierarchicalReferencesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHasSafetyStates() {
		return hasSafetyStatesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHasSlaves() {
		return hasSlavesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIsConnectedTo() {
		return isConnectedToEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getExecutionModeEnumeration() {
		return executionModeEnumerationEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getOperationalModeEnumeration() {
		return operationalModeEnumerationEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAxisMotionProfileEnumeration() {
		return axisMotionProfileEnumerationEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAxisStateEnumeration() {
		return axisStateEnumerationEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getModeEnum() {
		return modeEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMotionDeviceCategoryEnum() {
		return motionDeviceCategoryEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPCUARoboticsProfileFactory getOPCUARoboticsProfileFactory() {
		return (OPCUARoboticsProfileFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		loadTypeEClass = createEClass(LOAD_TYPE);
		createEReference(loadTypeEClass, LOAD_TYPE__MASS);
		createEReference(loadTypeEClass, LOAD_TYPE__CENTER_OF_MASS);
		createEReference(loadTypeEClass, LOAD_TYPE__INERTIA);

		parameterSetMotorTypeEClass = createEClass(PARAMETER_SET_MOTOR_TYPE);
		createEAttribute(parameterSetMotorTypeEClass, PARAMETER_SET_MOTOR_TYPE__BRAKE_RELEASED);
		createEAttribute(parameterSetMotorTypeEClass, PARAMETER_SET_MOTOR_TYPE__EFFECTIVE_LOAD_RATE);
		createEAttribute(parameterSetMotorTypeEClass, PARAMETER_SET_MOTOR_TYPE__MOTOR_TEMPERATURE);
		createEAttribute(parameterSetMotorTypeEClass, PARAMETER_SET_MOTOR_TYPE__NOT_CS_MOTOR_INTENSITY);

		parameterSetControllerTypeEClass = createEClass(PARAMETER_SET_CONTROLLER_TYPE);
		createEAttribute(parameterSetControllerTypeEClass, PARAMETER_SET_CONTROLLER_TYPE__TOTAL_POWER_ON_TIME);
		createEAttribute(parameterSetControllerTypeEClass, PARAMETER_SET_CONTROLLER_TYPE__START_UP_TIME);
		createEAttribute(parameterSetControllerTypeEClass, PARAMETER_SET_CONTROLLER_TYPE__UPS_STATE);
		createEAttribute(parameterSetControllerTypeEClass, PARAMETER_SET_CONTROLLER_TYPE__TOTAL_ENERGY_CONSUMPTION);
		createEAttribute(parameterSetControllerTypeEClass, PARAMETER_SET_CONTROLLER_TYPE__CABINET_FAN_SPEED);
		createEAttribute(parameterSetControllerTypeEClass, PARAMETER_SET_CONTROLLER_TYPE__CPU_FAN_SPEED);
		createEAttribute(parameterSetControllerTypeEClass, PARAMETER_SET_CONTROLLER_TYPE__INPUT_VOLTAGE);
		createEAttribute(parameterSetControllerTypeEClass, PARAMETER_SET_CONTROLLER_TYPE__TEMPERATURE);

		parameterSetTaskControlTypeEClass = createEClass(PARAMETER_SET_TASK_CONTROL_TYPE);
		createEAttribute(parameterSetTaskControlTypeEClass, PARAMETER_SET_TASK_CONTROL_TYPE__EXECUTION_MODE);
		createEAttribute(parameterSetTaskControlTypeEClass, PARAMETER_SET_TASK_CONTROL_TYPE__TASK_PROGRAM_LOADED);
		createEAttribute(parameterSetTaskControlTypeEClass, PARAMETER_SET_TASK_CONTROL_TYPE__TASK_PROGRAM_NAME);

		parameterSetSafetyStateTypeEClass = createEClass(PARAMETER_SET_SAFETY_STATE_TYPE);
		createEAttribute(parameterSetSafetyStateTypeEClass, PARAMETER_SET_SAFETY_STATE_TYPE__EMERGENCY_STOP);
		createEAttribute(parameterSetSafetyStateTypeEClass, PARAMETER_SET_SAFETY_STATE_TYPE__OPERATIONAL_MODE);
		createEAttribute(parameterSetSafetyStateTypeEClass, PARAMETER_SET_SAFETY_STATE_TYPE__PROTECTIVE_STOP);

		gearTypeEClass = createEClass(GEAR_TYPE);
		createEReference(gearTypeEClass, GEAR_TYPE__GEAR_RATIO);
		createEReference(gearTypeEClass, GEAR_TYPE__PITCH);
		createEReference(gearTypeEClass, GEAR_TYPE__IS_CONNECTED_TO);

		motorTypeEClass = createEClass(MOTOR_TYPE);
		createEReference(motorTypeEClass, MOTOR_TYPE__PARAMETER_SET_MOTOR_TYPE);

		parameterSetAxisTypeEClass = createEClass(PARAMETER_SET_AXIS_TYPE);
		createEAttribute(parameterSetAxisTypeEClass, PARAMETER_SET_AXIS_TYPE__ACTUAL_ACCELERATION);
		createEAttribute(parameterSetAxisTypeEClass, PARAMETER_SET_AXIS_TYPE__ACTUAL_POSITION);
		createEAttribute(parameterSetAxisTypeEClass, PARAMETER_SET_AXIS_TYPE__ACTUAL_SPEED);
		createEAttribute(parameterSetAxisTypeEClass, PARAMETER_SET_AXIS_TYPE__NOT_CS_AXIS_STATE);

		parameterSetMotionDeviceTypeEClass = createEClass(PARAMETER_SET_MOTION_DEVICE_TYPE);
		createEAttribute(parameterSetMotionDeviceTypeEClass, PARAMETER_SET_MOTION_DEVICE_TYPE__ON_PATH);
		createEAttribute(parameterSetMotionDeviceTypeEClass, PARAMETER_SET_MOTION_DEVICE_TYPE__IN_CONTROL);
		createEAttribute(parameterSetMotionDeviceTypeEClass, PARAMETER_SET_MOTION_DEVICE_TYPE__SPEED_OVERRIDE);
		createEAttribute(parameterSetMotionDeviceTypeEClass, PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_IS_POWER_BUTTON_PRESSED);
		createEAttribute(parameterSetMotionDeviceTypeEClass, PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_ROBOT_INTENSITY);
		createEAttribute(parameterSetMotionDeviceTypeEClass, PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_IS_TEACH_BUTTON_PRESSED);
		createEAttribute(parameterSetMotionDeviceTypeEClass, PARAMETER_SET_MOTION_DEVICE_TYPE__NOT_CS_IS_POWER_ON_ROBOT);
		createEAttribute(parameterSetMotionDeviceTypeEClass, PARAMETER_SET_MOTION_DEVICE_TYPE__MODE);

		powerTrainTypeEClass = createEClass(POWER_TRAIN_TYPE);
		createEReference(powerTrainTypeEClass, POWER_TRAIN_TYPE__MOTOR);
		createEReference(powerTrainTypeEClass, POWER_TRAIN_TYPE__GEAR);

		motionDeviceSystemTypeEClass = createEClass(MOTION_DEVICE_SYSTEM_TYPE);
		createEReference(motionDeviceSystemTypeEClass, MOTION_DEVICE_SYSTEM_TYPE__MOTION_DEVICES);
		createEReference(motionDeviceSystemTypeEClass, MOTION_DEVICE_SYSTEM_TYPE__CONTROLLERS);
		createEReference(motionDeviceSystemTypeEClass, MOTION_DEVICE_SYSTEM_TYPE__SAFETY_STATES);

		motionDeviceTypeEClass = createEClass(MOTION_DEVICE_TYPE);
		createEAttribute(motionDeviceTypeEClass, MOTION_DEVICE_TYPE__MOTION_DEVICE_CATEGORY);
		createEReference(motionDeviceTypeEClass, MOTION_DEVICE_TYPE__PARAMETER_SET_MOTION_DEVICE_TYPE);
		createEReference(motionDeviceTypeEClass, MOTION_DEVICE_TYPE__AXES);
		createEReference(motionDeviceTypeEClass, MOTION_DEVICE_TYPE__POWER_TRAINS);
		createEReference(motionDeviceTypeEClass, MOTION_DEVICE_TYPE__ADDITIONAL_COMPONENTS);
		createEReference(motionDeviceTypeEClass, MOTION_DEVICE_TYPE__FLANGE_LOAD);

		axisTypeEClass = createEClass(AXIS_TYPE);
		createEAttribute(axisTypeEClass, AXIS_TYPE__MOTION_PROFILE);
		createEReference(axisTypeEClass, AXIS_TYPE__ADDITIONAL_LOAD);
		createEReference(axisTypeEClass, AXIS_TYPE__PARAMETER_SET_AXIS_TYPE);

		controllerTypeEClass = createEClass(CONTROLLER_TYPE);
		createEReference(controllerTypeEClass, CONTROLLER_TYPE__PARAMETER_SET_CONTROLLER_TYPE);
		createEReference(controllerTypeEClass, CONTROLLER_TYPE__COMPONENTS);
		createEReference(controllerTypeEClass, CONTROLLER_TYPE__SOFTWARE);
		createEReference(controllerTypeEClass, CONTROLLER_TYPE__TASK_CONTROLS);

		taskControlTypeEClass = createEClass(TASK_CONTROL_TYPE);
		createEReference(taskControlTypeEClass, TASK_CONTROL_TYPE__PARAMETER_SET_TASK_CONTOL_TYPE);

		safetyStateTypeEClass = createEClass(SAFETY_STATE_TYPE);
		createEReference(safetyStateTypeEClass, SAFETY_STATE_TYPE__EMERGENCY_STOP_FUNCTIONS);
		createEReference(safetyStateTypeEClass, SAFETY_STATE_TYPE__PROTECTIVE_STOP_FUNCTIONS);
		createEReference(safetyStateTypeEClass, SAFETY_STATE_TYPE__PARAMETER_SET_SAFETY_STATE_TYPE);

		emergencyStopFunctionTypeEClass = createEClass(EMERGENCY_STOP_FUNCTION_TYPE);
		createEAttribute(emergencyStopFunctionTypeEClass, EMERGENCY_STOP_FUNCTION_TYPE__NAME);
		createEAttribute(emergencyStopFunctionTypeEClass, EMERGENCY_STOP_FUNCTION_TYPE__ACTIVE);
		createEReference(emergencyStopFunctionTypeEClass, EMERGENCY_STOP_FUNCTION_TYPE__BASE_CLASS);

		protectiveStopFunctionTypeEClass = createEClass(PROTECTIVE_STOP_FUNCTION_TYPE);
		createEAttribute(protectiveStopFunctionTypeEClass, PROTECTIVE_STOP_FUNCTION_TYPE__NAME);
		createEAttribute(protectiveStopFunctionTypeEClass, PROTECTIVE_STOP_FUNCTION_TYPE__ENABLED);
		createEAttribute(protectiveStopFunctionTypeEClass, PROTECTIVE_STOP_FUNCTION_TYPE__ACTIVE);
		createEReference(protectiveStopFunctionTypeEClass, PROTECTIVE_STOP_FUNCTION_TYPE__BASE_CLASS);

		referencesEClass = createEClass(REFERENCES);
		createEReference(referencesEClass, REFERENCES__BASE_ASSOCIATION);
		createEReference(referencesEClass, REFERENCES__BASE_DEPENDENCY);

		hierarchicalReferencesEClass = createEClass(HIERARCHICAL_REFERENCES);

		controlsEClass = createEClass(CONTROLS);

		isDrivenByEClass = createEClass(IS_DRIVEN_BY);

		movesEClass = createEClass(MOVES);

		requiresEClass = createEClass(REQUIRES);

		nonHierarchicalReferencesEClass = createEClass(NON_HIERARCHICAL_REFERENCES);

		hasSafetyStatesEClass = createEClass(HAS_SAFETY_STATES);

		hasSlavesEClass = createEClass(HAS_SLAVES);

		isConnectedToEClass = createEClass(IS_CONNECTED_TO);

		// Create enums
		executionModeEnumerationEEnum = createEEnum(EXECUTION_MODE_ENUMERATION);
		operationalModeEnumerationEEnum = createEEnum(OPERATIONAL_MODE_ENUMERATION);
		axisMotionProfileEnumerationEEnum = createEEnum(AXIS_MOTION_PROFILE_ENUMERATION);
		axisStateEnumerationEEnum = createEEnum(AXIS_STATE_ENUMERATION);
		modeEnumEEnum = createEEnum(MODE_ENUM);
		motionDeviceCategoryEnumEEnum = createEEnum(MOTION_DEVICE_CATEGORY_ENUM);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		OPC_UA_Robotics_CS_LibraryPackage theOPC_UA_Robotics_CS_LibraryPackage = (OPC_UA_Robotics_CS_LibraryPackage)EPackage.Registry.INSTANCE.getEPackage(OPC_UA_Robotics_CS_LibraryPackage.eNS_URI);
		OPCUADIProfilePackage theOPCUADIProfilePackage = (OPCUADIProfilePackage)EPackage.Registry.INSTANCE.getEPackage(OPCUADIProfilePackage.eNS_URI);
		org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Library.OPC_UA_LibraryPackage theOPC_UA_LibraryPackage_1 = (org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Library.OPC_UA_LibraryPackage)EPackage.Registry.INSTANCE.getEPackage(org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Library.OPC_UA_LibraryPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theOPC_UA_Robotics_CS_LibraryPackage);
		getESubpackages().add(theOPCUADIProfilePackage);
		getESubpackages().add(theOPC_UA_LibraryPackage_1);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		gearTypeEClass.getESuperTypes().add(theOPCUADIProfilePackage.getComponentType());
		motorTypeEClass.getESuperTypes().add(theOPCUADIProfilePackage.getComponentType());
		powerTrainTypeEClass.getESuperTypes().add(theOPCUADIProfilePackage.getComponentType());
		motionDeviceSystemTypeEClass.getESuperTypes().add(theOPCUADIProfilePackage.getComponentType());
		motionDeviceTypeEClass.getESuperTypes().add(theOPCUADIProfilePackage.getComponentType());
		axisTypeEClass.getESuperTypes().add(theOPCUADIProfilePackage.getComponentType());
		controllerTypeEClass.getESuperTypes().add(theOPCUADIProfilePackage.getComponentType());
		taskControlTypeEClass.getESuperTypes().add(theOPCUADIProfilePackage.getComponentType());
		safetyStateTypeEClass.getESuperTypes().add(theOPCUADIProfilePackage.getComponentType());
		hierarchicalReferencesEClass.getESuperTypes().add(this.getReferences());
		controlsEClass.getESuperTypes().add(this.getHierarchicalReferences());
		isDrivenByEClass.getESuperTypes().add(this.getHierarchicalReferences());
		movesEClass.getESuperTypes().add(this.getHierarchicalReferences());
		requiresEClass.getESuperTypes().add(this.getHierarchicalReferences());
		nonHierarchicalReferencesEClass.getESuperTypes().add(this.getReferences());
		hasSafetyStatesEClass.getESuperTypes().add(this.getHierarchicalReferences());
		hasSlavesEClass.getESuperTypes().add(this.getHierarchicalReferences());
		isConnectedToEClass.getESuperTypes().add(this.getNonHierarchicalReferences());

		// Initialize classes, features, and operations; add parameters
		initEClass(loadTypeEClass, LoadType.class, "LoadType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLoadType_Mass(), theOPC_UA_Robotics_CS_LibraryPackage.getDouble(), null, "Mass", null, 1, 1, LoadType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getLoadType_CenterOfMass(), theOPC_UA_Robotics_CS_LibraryPackage.get_3DFrameType(), null, "CenterOfMass", null, 1, 1, LoadType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getLoadType_Inertia(), theOPC_UA_Robotics_CS_LibraryPackage.get_3DVectorType(), null, "Inertia", null, 1, 1, LoadType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(parameterSetMotorTypeEClass, ParameterSetMotorType.class, "ParameterSetMotorType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getParameterSetMotorType_BrakeReleased(), theTypesPackage.getBoolean(), "BrakeReleased", null, 1, 1, ParameterSetMotorType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getParameterSetMotorType_EffectiveLoadRate(), theTypesPackage.getInteger(), "EffectiveLoadRate", null, 1, 1, ParameterSetMotorType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getParameterSetMotorType_MotorTemperature(), theTypesPackage.getReal(), "MotorTemperature", null, 1, 1, ParameterSetMotorType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getParameterSetMotorType_NotCS_MotorIntensity(), theTypesPackage.getInteger(), "notCS_MotorIntensity", null, 1, 1, ParameterSetMotorType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(parameterSetControllerTypeEClass, ParameterSetControllerType.class, "ParameterSetControllerType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getParameterSetControllerType_TotalPowerOnTime(), theTypesPackage.getString(), "TotalPowerOnTime", null, 0, 1, ParameterSetControllerType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getParameterSetControllerType_StartUpTime(), theTypesPackage.getString(), "StartUpTime", null, 0, 1, ParameterSetControllerType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getParameterSetControllerType_UpsState(), theTypesPackage.getString(), "UpsState", null, 0, 1, ParameterSetControllerType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getParameterSetControllerType_TotalEnergyConsumption(), ecorePackage.getEDouble(), "TotalEnergyConsumption", null, 0, 1, ParameterSetControllerType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getParameterSetControllerType_CabinetFanSpeed(), ecorePackage.getEDouble(), "CabinetFanSpeed", null, 0, 1, ParameterSetControllerType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getParameterSetControllerType_CPUFanSpeed(), ecorePackage.getEDouble(), "CPUFanSpeed", null, 0, 1, ParameterSetControllerType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getParameterSetControllerType_InputVoltage(), ecorePackage.getEDouble(), "InputVoltage", null, 0, 1, ParameterSetControllerType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getParameterSetControllerType_Temperature(), ecorePackage.getEDouble(), "Temperature", null, 0, 1, ParameterSetControllerType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(parameterSetTaskControlTypeEClass, ParameterSetTaskControlType.class, "ParameterSetTaskControlType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getParameterSetTaskControlType_ExecutionMode(), this.getExecutionModeEnumeration(), "ExecutionMode", null, 1, 1, ParameterSetTaskControlType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getParameterSetTaskControlType_TaskProgramLoaded(), theTypesPackage.getBoolean(), "TaskProgramLoaded", null, 1, 1, ParameterSetTaskControlType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getParameterSetTaskControlType_TaskProgramName(), theTypesPackage.getString(), "TaskProgramName", null, 1, 1, ParameterSetTaskControlType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(parameterSetSafetyStateTypeEClass, ParameterSetSafetyStateType.class, "ParameterSetSafetyStateType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getParameterSetSafetyStateType_EmergencyStop(), theTypesPackage.getBoolean(), "EmergencyStop", null, 1, 1, ParameterSetSafetyStateType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getParameterSetSafetyStateType_OperationalMode(), this.getOperationalModeEnumeration(), "OperationalMode", null, 1, 1, ParameterSetSafetyStateType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getParameterSetSafetyStateType_ProtectiveStop(), theTypesPackage.getBoolean(), "ProtectiveStop", null, 1, 1, ParameterSetSafetyStateType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(gearTypeEClass, GearType.class, "GearType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGearType_GearRatio(), theOPC_UA_Robotics_CS_LibraryPackage.getRationalNumber(), null, "GearRatio", null, 1, 1, GearType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getGearType_Pitch(), theOPC_UA_Robotics_CS_LibraryPackage.getDouble(), null, "Pitch", null, 1, 1, GearType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getGearType_IsConnectedTo(), this.getMotorType(), null, "isConnectedTo", null, 1, 1, GearType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(motorTypeEClass, MotorType.class, "MotorType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMotorType_ParameterSetMotorType(), this.getParameterSetMotorType(), null, "ParameterSetMotorType", null, 1, 1, MotorType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(parameterSetAxisTypeEClass, ParameterSetAxisType.class, "ParameterSetAxisType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getParameterSetAxisType_ActualAcceleration(), ecorePackage.getEDouble(), "ActualAcceleration", null, 0, 1, ParameterSetAxisType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getParameterSetAxisType_ActualPosition(), ecorePackage.getEDouble(), "ActualPosition", null, 1, 1, ParameterSetAxisType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getParameterSetAxisType_ActualSpeed(), ecorePackage.getEDouble(), "ActualSpeed", null, 0, 1, ParameterSetAxisType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getParameterSetAxisType_NotCS_AxisState(), this.getAxisStateEnumeration(), "notCS_AxisState", null, 1, 1, ParameterSetAxisType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(parameterSetMotionDeviceTypeEClass, ParameterSetMotionDeviceType.class, "ParameterSetMotionDeviceType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getParameterSetMotionDeviceType_OnPath(), theTypesPackage.getBoolean(), "onPath", null, 0, 1, ParameterSetMotionDeviceType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getParameterSetMotionDeviceType_InControl(), theTypesPackage.getBoolean(), "InControl", null, 0, 1, ParameterSetMotionDeviceType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getParameterSetMotionDeviceType_SpeedOverride(), theTypesPackage.getReal(), "SpeedOverride", null, 1, 1, ParameterSetMotionDeviceType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getParameterSetMotionDeviceType_NotCS_isPowerButtonPressed(), theTypesPackage.getBoolean(), "notCS_isPowerButtonPressed", null, 0, 1, ParameterSetMotionDeviceType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getParameterSetMotionDeviceType_NotCS__RobotIntensity(), theTypesPackage.getReal(), "notCS__RobotIntensity", null, 1, 1, ParameterSetMotionDeviceType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getParameterSetMotionDeviceType_NotCS_isTeachButtonPressed(), theTypesPackage.getBoolean(), "notCS_isTeachButtonPressed", null, 0, 1, ParameterSetMotionDeviceType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getParameterSetMotionDeviceType_NotCS_isPowerOnRobot(), theTypesPackage.getBoolean(), "notCS_isPowerOnRobot", null, 0, 1, ParameterSetMotionDeviceType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getParameterSetMotionDeviceType_Mode(), this.getModeEnum(), "Mode", null, 1, 1, ParameterSetMotionDeviceType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(powerTrainTypeEClass, PowerTrainType.class, "PowerTrainType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPowerTrainType_Motor(), this.getMotorType(), null, "Motor", null, 1, 1, PowerTrainType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getPowerTrainType_Gear(), this.getGearType(), null, "Gear", null, 0, 1, PowerTrainType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(motionDeviceSystemTypeEClass, MotionDeviceSystemType.class, "MotionDeviceSystemType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMotionDeviceSystemType_MotionDevices(), this.getMotionDeviceType(), null, "MotionDevices", null, 1, -1, MotionDeviceSystemType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getMotionDeviceSystemType_Controllers(), this.getControllerType(), null, "Controllers", null, 1, -1, MotionDeviceSystemType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getMotionDeviceSystemType_SafetyStates(), this.getSafetyStateType(), null, "SafetyStates", null, 1, -1, MotionDeviceSystemType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(motionDeviceTypeEClass, MotionDeviceType.class, "MotionDeviceType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMotionDeviceType_MotionDeviceCategory(), this.getMotionDeviceCategoryEnum(), "MotionDeviceCategory", null, 1, 1, MotionDeviceType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getMotionDeviceType_ParameterSetMotionDeviceType(), this.getParameterSetMotionDeviceType(), null, "ParameterSetMotionDeviceType", null, 1, 1, MotionDeviceType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getMotionDeviceType_Axes(), this.getAxisType(), null, "Axes", null, 1, -1, MotionDeviceType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getMotionDeviceType_PowerTrains(), this.getPowerTrainType(), null, "PowerTrains", null, 1, -1, MotionDeviceType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getMotionDeviceType_AdditionalComponents(), theOPCUADIProfilePackage.getComponentType(), null, "AdditionalComponents", null, 0, -1, MotionDeviceType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getMotionDeviceType_FlangeLoad(), this.getLoadType(), null, "FlangeLoad", null, 0, 1, MotionDeviceType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(axisTypeEClass, AxisType.class, "AxisType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAxisType_MotionProfile(), this.getAxisMotionProfileEnumeration(), "MotionProfile", null, 1, 1, AxisType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAxisType_AdditionalLoad(), this.getLoadType(), null, "AdditionalLoad", null, 0, 1, AxisType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAxisType_ParameterSetAxisType(), this.getParameterSetAxisType(), null, "parameterSetAxisType", null, 1, 1, AxisType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(controllerTypeEClass, ControllerType.class, "ControllerType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getControllerType_ParameterSetControllerType(), this.getParameterSetControllerType(), null, "parameterSetControllerType", null, 1, 1, ControllerType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getControllerType_Components(), theOPCUADIProfilePackage.getComponentType(), null, "Components", null, 0, -1, ControllerType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getControllerType_Software(), theOPCUADIProfilePackage.getSoftwareType(), null, "Software", null, 1, 1, ControllerType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getControllerType_TaskControls(), this.getTaskControlType(), null, "TaskControls", null, 1, -1, ControllerType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(taskControlTypeEClass, TaskControlType.class, "TaskControlType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTaskControlType_ParameterSetTaskContolType(), this.getParameterSetTaskControlType(), null, "parameterSetTaskContolType", null, 1, 1, TaskControlType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(safetyStateTypeEClass, SafetyStateType.class, "SafetyStateType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSafetyStateType_EmergencyStopFunctions(), this.getEmergencyStopFunctionType(), null, "EmergencyStopFunctions", null, 0, -1, SafetyStateType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSafetyStateType_ProtectiveStopFunctions(), this.getProtectiveStopFunctionType(), null, "ProtectiveStopFunctions", null, 0, -1, SafetyStateType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSafetyStateType_ParameterSetSafetyStateType(), this.getParameterSetSafetyStateType(), null, "parameterSetSafetyStateType", null, 1, 1, SafetyStateType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(emergencyStopFunctionTypeEClass, EmergencyStopFunctionType.class, "EmergencyStopFunctionType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEmergencyStopFunctionType_Name(), theTypesPackage.getString(), "Name", null, 1, 1, EmergencyStopFunctionType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getEmergencyStopFunctionType_Active(), theTypesPackage.getBoolean(), "Active", null, 1, 1, EmergencyStopFunctionType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getEmergencyStopFunctionType_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 0, 1, EmergencyStopFunctionType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(protectiveStopFunctionTypeEClass, ProtectiveStopFunctionType.class, "ProtectiveStopFunctionType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getProtectiveStopFunctionType_Name(), theTypesPackage.getString(), "Name", null, 1, 1, ProtectiveStopFunctionType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getProtectiveStopFunctionType_Enabled(), theTypesPackage.getBoolean(), "Enabled", null, 1, 1, ProtectiveStopFunctionType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getProtectiveStopFunctionType_Active(), theTypesPackage.getBoolean(), "Active", null, 1, 1, ProtectiveStopFunctionType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getProtectiveStopFunctionType_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 0, 1, ProtectiveStopFunctionType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(referencesEClass, References.class, "References", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReferences_Base_Association(), theUMLPackage.getAssociation(), null, "base_Association", null, 0, 1, References.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getReferences_Base_Dependency(), theUMLPackage.getDependency(), null, "base_Dependency", null, 0, 1, References.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(hierarchicalReferencesEClass, HierarchicalReferences.class, "HierarchicalReferences", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(controlsEClass, Controls.class, "Controls", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(isDrivenByEClass, IsDrivenBy.class, "IsDrivenBy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(movesEClass, Moves.class, "Moves", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(requiresEClass, Requires.class, "Requires", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(nonHierarchicalReferencesEClass, NonHierarchicalReferences.class, "NonHierarchicalReferences", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(hasSafetyStatesEClass, HasSafetyStates.class, "HasSafetyStates", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(hasSlavesEClass, HasSlaves.class, "HasSlaves", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(isConnectedToEClass, IsConnectedTo.class, "IsConnectedTo", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(executionModeEnumerationEEnum, ExecutionModeEnumeration.class, "ExecutionModeEnumeration");
		addEEnumLiteral(executionModeEnumerationEEnum, ExecutionModeEnumeration.CYCLE);
		addEEnumLiteral(executionModeEnumerationEEnum, ExecutionModeEnumeration.CONTINUOUS);
		addEEnumLiteral(executionModeEnumerationEEnum, ExecutionModeEnumeration.STEP);

		initEEnum(operationalModeEnumerationEEnum, OperationalModeEnumeration.class, "OperationalModeEnumeration");
		addEEnumLiteral(operationalModeEnumerationEEnum, OperationalModeEnumeration.OTHER);
		addEEnumLiteral(operationalModeEnumerationEEnum, OperationalModeEnumeration.MANUAL_REDUCED_SPEED);
		addEEnumLiteral(operationalModeEnumerationEEnum, OperationalModeEnumeration.MANUAL_HIGH_SPEED);
		addEEnumLiteral(operationalModeEnumerationEEnum, OperationalModeEnumeration.AUTOMATIC);
		addEEnumLiteral(operationalModeEnumerationEEnum, OperationalModeEnumeration.AUTOMATIC_EXTERNAL);

		initEEnum(axisMotionProfileEnumerationEEnum, AxisMotionProfileEnumeration.class, "AxisMotionProfileEnumeration");
		addEEnumLiteral(axisMotionProfileEnumerationEEnum, AxisMotionProfileEnumeration.OTHER);
		addEEnumLiteral(axisMotionProfileEnumerationEEnum, AxisMotionProfileEnumeration.ROTARY);
		addEEnumLiteral(axisMotionProfileEnumerationEEnum, AxisMotionProfileEnumeration.ROTARY_ENDLESS);
		addEEnumLiteral(axisMotionProfileEnumerationEEnum, AxisMotionProfileEnumeration.LINEAR);
		addEEnumLiteral(axisMotionProfileEnumerationEEnum, AxisMotionProfileEnumeration.LINEAR_ENDLESS);

		initEEnum(axisStateEnumerationEEnum, AxisStateEnumeration.class, "AxisStateEnumeration");
		addEEnumLiteral(axisStateEnumerationEEnum, AxisStateEnumeration.JOINT_SHUTTING_DOWN_MODE);
		addEEnumLiteral(axisStateEnumerationEEnum, AxisStateEnumeration.JOINT_PART_DCALIBRATION_MODE);
		addEEnumLiteral(axisStateEnumerationEEnum, AxisStateEnumeration._JOINT_BACKDRIVE_MODE);
		addEEnumLiteral(axisStateEnumerationEEnum, AxisStateEnumeration.JOINT_POWER_OFF_MODE);
		addEEnumLiteral(axisStateEnumerationEEnum, AxisStateEnumeration.JOINT_NOT_RESPONDING_MODE);
		addEEnumLiteral(axisStateEnumerationEEnum, AxisStateEnumeration.JOINT_MOTOR_INITIALISATION_MODE);
		addEEnumLiteral(axisStateEnumerationEEnum, AxisStateEnumeration.JOINT_BOOTING_MODE);
		addEEnumLiteral(axisStateEnumerationEEnum, AxisStateEnumeration.JOINT_PART_DCALIBRATION_ERROR_MODE);
		addEEnumLiteral(axisStateEnumerationEEnum, AxisStateEnumeration.JOINT_BOOTLOADER_MODE);
		addEEnumLiteral(axisStateEnumerationEEnum, AxisStateEnumeration.JOINT_CALIBRATION_MODE);
		addEEnumLiteral(axisStateEnumerationEEnum, AxisStateEnumeration.JOINT_FAULT_MODE);
		addEEnumLiteral(axisStateEnumerationEEnum, AxisStateEnumeration.JOINT_RUNNING_MODE);
		addEEnumLiteral(axisStateEnumerationEEnum, AxisStateEnumeration.JOINT_IDLE_MODE);

		initEEnum(modeEnumEEnum, ModeEnum.class, "ModeEnum");
		addEEnumLiteral(modeEnumEEnum, ModeEnum.DISCONNECTED);
		addEEnumLiteral(modeEnumEEnum, ModeEnum.CONFIRM_SAFETY);
		addEEnumLiteral(modeEnumEEnum, ModeEnum.BOOTING);
		addEEnumLiteral(modeEnumEEnum, ModeEnum.POWER_OFF);
		addEEnumLiteral(modeEnumEEnum, ModeEnum.POWER_ON);
		addEEnumLiteral(modeEnumEEnum, ModeEnum.IDLE);
		addEEnumLiteral(modeEnumEEnum, ModeEnum.BACKDRIVE);
		addEEnumLiteral(modeEnumEEnum, ModeEnum.RUNNING);

		initEEnum(motionDeviceCategoryEnumEEnum, MotionDeviceCategoryEnum.class, "MotionDeviceCategoryEnum");
		addEEnumLiteral(motionDeviceCategoryEnumEEnum, MotionDeviceCategoryEnum.OTHER);
		addEEnumLiteral(motionDeviceCategoryEnumEEnum, MotionDeviceCategoryEnum.ARTICULATED_ROBOT);
		addEEnumLiteral(motionDeviceCategoryEnumEEnum, MotionDeviceCategoryEnum.SCARA_ROBOT);
		addEEnumLiteral(motionDeviceCategoryEnumEEnum, MotionDeviceCategoryEnum.CARTESIAN_ROBOT);
		addEEnumLiteral(motionDeviceCategoryEnumEEnum, MotionDeviceCategoryEnum.SPHERICAL_ROBOT);
		addEEnumLiteral(motionDeviceCategoryEnumEEnum, MotionDeviceCategoryEnum.PARALLEL_ROBOT);
		addEEnumLiteral(motionDeviceCategoryEnumEEnum, MotionDeviceCategoryEnum.CYLINDRICAL_ROBOT);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/uml2/2.0.0/UML
		createUMLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/uml2/2.0.0/UML</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createUMLAnnotations() {
		String source = "http://www.eclipse.org/uml2/2.0.0/UML";
		addAnnotation
		  (this,
		   source,
		   new String[] {
			   "originalName", "OPC_UA_Robotics_CS"
		   });
		addAnnotation
		  (axisStateEnumerationEEnum.getELiterals().get(2),
		   source,
		   new String[] {
			   "originalName", " JOINT_BACKDRIVE_MODE"
		   });
	}

} //OPCUARoboticsProfilePackageImpl
