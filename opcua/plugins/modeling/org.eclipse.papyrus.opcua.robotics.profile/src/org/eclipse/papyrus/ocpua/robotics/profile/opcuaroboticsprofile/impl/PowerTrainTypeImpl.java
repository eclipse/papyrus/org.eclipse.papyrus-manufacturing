/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.GearType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.MotorType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.PowerTrainType;

import org.eclipse.papyrus.opcua.di.opcuadiprofile.impl.ComponentTypeImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Power Train Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.PowerTrainTypeImpl#getMotor <em>Motor</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.impl.PowerTrainTypeImpl#getGear <em>Gear</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PowerTrainTypeImpl extends ComponentTypeImpl implements PowerTrainType {
	/**
	 * The cached value of the '{@link #getMotor() <em>Motor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMotor()
	 * @generated
	 * @ordered
	 */
	protected MotorType motor;

	/**
	 * The cached value of the '{@link #getGear() <em>Gear</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGear()
	 * @generated
	 * @ordered
	 */
	protected GearType gear;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PowerTrainTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPCUARoboticsProfilePackage.Literals.POWER_TRAIN_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MotorType getMotor() {
		if (motor != null && motor.eIsProxy()) {
			InternalEObject oldMotor = (InternalEObject)motor;
			motor = (MotorType)eResolveProxy(oldMotor);
			if (motor != oldMotor) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OPCUARoboticsProfilePackage.POWER_TRAIN_TYPE__MOTOR, oldMotor, motor));
			}
		}
		return motor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MotorType basicGetMotor() {
		return motor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMotor(MotorType newMotor) {
		MotorType oldMotor = motor;
		motor = newMotor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.POWER_TRAIN_TYPE__MOTOR, oldMotor, motor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GearType getGear() {
		if (gear != null && gear.eIsProxy()) {
			InternalEObject oldGear = (InternalEObject)gear;
			gear = (GearType)eResolveProxy(oldGear);
			if (gear != oldGear) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OPCUARoboticsProfilePackage.POWER_TRAIN_TYPE__GEAR, oldGear, gear));
			}
		}
		return gear;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GearType basicGetGear() {
		return gear;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGear(GearType newGear) {
		GearType oldGear = gear;
		gear = newGear;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPCUARoboticsProfilePackage.POWER_TRAIN_TYPE__GEAR, oldGear, gear));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.POWER_TRAIN_TYPE__MOTOR:
				if (resolve) return getMotor();
				return basicGetMotor();
			case OPCUARoboticsProfilePackage.POWER_TRAIN_TYPE__GEAR:
				if (resolve) return getGear();
				return basicGetGear();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.POWER_TRAIN_TYPE__MOTOR:
				setMotor((MotorType)newValue);
				return;
			case OPCUARoboticsProfilePackage.POWER_TRAIN_TYPE__GEAR:
				setGear((GearType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.POWER_TRAIN_TYPE__MOTOR:
				setMotor((MotorType)null);
				return;
			case OPCUARoboticsProfilePackage.POWER_TRAIN_TYPE__GEAR:
				setGear((GearType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPCUARoboticsProfilePackage.POWER_TRAIN_TYPE__MOTOR:
				return motor != null;
			case OPCUARoboticsProfilePackage.POWER_TRAIN_TYPE__GEAR:
				return gear != null;
		}
		return super.eIsSet(featureID);
	}

} //PowerTrainTypeImpl
