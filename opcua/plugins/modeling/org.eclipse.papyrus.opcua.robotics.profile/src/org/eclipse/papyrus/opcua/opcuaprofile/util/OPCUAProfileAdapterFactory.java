/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.opcua.opcuaprofile.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.papyrus.opcua.opcuaprofile.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.opcua.opcuaprofile.OPCUAProfilePackage
 * @generated
 */
public class OPCUAProfileAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static OPCUAProfilePackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPCUAProfileAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = OPCUAProfilePackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OPCUAProfileSwitch<Adapter> modelSwitch =
		new OPCUAProfileSwitch<Adapter>() {
			@Override
			public Adapter caseBaseObjectType(BaseObjectType object) {
				return createBaseObjectTypeAdapter();
			}
			@Override
			public Adapter caseDataTypeSystemType(DataTypeSystemType object) {
				return createDataTypeSystemTypeAdapter();
			}
			@Override
			public Adapter caseModellingRuleType(ModellingRuleType object) {
				return createModellingRuleTypeAdapter();
			}
			@Override
			public Adapter caseServerType(ServerType object) {
				return createServerTypeAdapter();
			}
			@Override
			public Adapter caseFolderType(FolderType object) {
				return createFolderTypeAdapter();
			}
			@Override
			public Adapter caseDataTypeEncodingType(DataTypeEncodingType object) {
				return createDataTypeEncodingTypeAdapter();
			}
			@Override
			public Adapter caseServerCapabilitiesType(ServerCapabilitiesType object) {
				return createServerCapabilitiesTypeAdapter();
			}
			@Override
			public Adapter caseServerDiagnosticsType(ServerDiagnosticsType object) {
				return createServerDiagnosticsTypeAdapter();
			}
			@Override
			public Adapter caseSessionsDiagnosticsSummaryType(SessionsDiagnosticsSummaryType object) {
				return createSessionsDiagnosticsSummaryTypeAdapter();
			}
			@Override
			public Adapter caseSessionDiagnosticsObjectType(SessionDiagnosticsObjectType object) {
				return createSessionDiagnosticsObjectTypeAdapter();
			}
			@Override
			public Adapter caseVendorServerInfoType(VendorServerInfoType object) {
				return createVendorServerInfoTypeAdapter();
			}
			@Override
			public Adapter caseServerRedundancyType(ServerRedundancyType object) {
				return createServerRedundancyTypeAdapter();
			}
			@Override
			public Adapter caseFileType(FileType object) {
				return createFileTypeAdapter();
			}
			@Override
			public Adapter caseNamespacesType(NamespacesType object) {
				return createNamespacesTypeAdapter();
			}
			@Override
			public Adapter caseBaseEventType(BaseEventType object) {
				return createBaseEventTypeAdapter();
			}
			@Override
			public Adapter caseAggregateFunctionType(AggregateFunctionType object) {
				return createAggregateFunctionTypeAdapter();
			}
			@Override
			public Adapter caseStateMachineType(StateMachineType object) {
				return createStateMachineTypeAdapter();
			}
			@Override
			public Adapter caseStateType(StateType object) {
				return createStateTypeAdapter();
			}
			@Override
			public Adapter caseNamespaceMetadataType(NamespaceMetadataType object) {
				return createNamespaceMetadataTypeAdapter();
			}
			@Override
			public Adapter caseWriterGroupMessageType(WriterGroupMessageType object) {
				return createWriterGroupMessageTypeAdapter();
			}
			@Override
			public Adapter caseTransitionType(TransitionType object) {
				return createTransitionTypeAdapter();
			}
			@Override
			public Adapter caseTemporaryFileTransferType(TemporaryFileTransferType object) {
				return createTemporaryFileTransferTypeAdapter();
			}
			@Override
			public Adapter caseRoleSetType(RoleSetType object) {
				return createRoleSetTypeAdapter();
			}
			@Override
			public Adapter caseRoleType(RoleType object) {
				return createRoleTypeAdapter();
			}
			@Override
			public Adapter caseDictionaryEntryType(DictionaryEntryType object) {
				return createDictionaryEntryTypeAdapter();
			}
			@Override
			public Adapter caseOrderedListType(OrderedListType object) {
				return createOrderedListTypeAdapter();
			}
			@Override
			public Adapter caseBaseConditionClassType(BaseConditionClassType object) {
				return createBaseConditionClassTypeAdapter();
			}
			@Override
			public Adapter caseAlarmMetricsType(AlarmMetricsType object) {
				return createAlarmMetricsTypeAdapter();
			}
			@Override
			public Adapter caseHistoricalDataConfigurationType(HistoricalDataConfigurationType object) {
				return createHistoricalDataConfigurationTypeAdapter();
			}
			@Override
			public Adapter caseHistoryServerCapabilitiesType(HistoryServerCapabilitiesType object) {
				return createHistoryServerCapabilitiesTypeAdapter();
			}
			@Override
			public Adapter caseCertificateGroupType(CertificateGroupType object) {
				return createCertificateGroupTypeAdapter();
			}
			@Override
			public Adapter caseCertificateType(CertificateType object) {
				return createCertificateTypeAdapter();
			}
			@Override
			public Adapter caseServerConfigurationType(ServerConfigurationType object) {
				return createServerConfigurationTypeAdapter();
			}
			@Override
			public Adapter caseKeyCredentialConfigurationType(KeyCredentialConfigurationType object) {
				return createKeyCredentialConfigurationTypeAdapter();
			}
			@Override
			public Adapter caseAuthorizationServiceConfigurationType(AuthorizationServiceConfigurationType object) {
				return createAuthorizationServiceConfigurationTypeAdapter();
			}
			@Override
			public Adapter caseAggregateConfigurationType(AggregateConfigurationType object) {
				return createAggregateConfigurationTypeAdapter();
			}
			@Override
			public Adapter casePubSubKeyServiceType(PubSubKeyServiceType object) {
				return createPubSubKeyServiceTypeAdapter();
			}
			@Override
			public Adapter caseSecurityGroupType(SecurityGroupType object) {
				return createSecurityGroupTypeAdapter();
			}
			@Override
			public Adapter casePublishedDataSetType(PublishedDataSetType object) {
				return createPublishedDataSetTypeAdapter();
			}
			@Override
			public Adapter caseExtensionFieldsType(ExtensionFieldsType object) {
				return createExtensionFieldsTypeAdapter();
			}
			@Override
			public Adapter casePubSubConnectionType(PubSubConnectionType object) {
				return createPubSubConnectionTypeAdapter();
			}
			@Override
			public Adapter caseConnectionTransportType(ConnectionTransportType object) {
				return createConnectionTransportTypeAdapter();
			}
			@Override
			public Adapter casePubSubGroupType(PubSubGroupType object) {
				return createPubSubGroupTypeAdapter();
			}
			@Override
			public Adapter caseWriterGroupTransportType(WriterGroupTransportType object) {
				return createWriterGroupTransportTypeAdapter();
			}
			@Override
			public Adapter caseReaderGroupTransportType(ReaderGroupTransportType object) {
				return createReaderGroupTransportTypeAdapter();
			}
			@Override
			public Adapter caseReaderGroupMessageType(ReaderGroupMessageType object) {
				return createReaderGroupMessageTypeAdapter();
			}
			@Override
			public Adapter caseDataSetWriterType(DataSetWriterType object) {
				return createDataSetWriterTypeAdapter();
			}
			@Override
			public Adapter caseDataSetWriterTransportType(DataSetWriterTransportType object) {
				return createDataSetWriterTransportTypeAdapter();
			}
			@Override
			public Adapter caseDataSetWriterMessageType(DataSetWriterMessageType object) {
				return createDataSetWriterMessageTypeAdapter();
			}
			@Override
			public Adapter caseDataSetReaderType(DataSetReaderType object) {
				return createDataSetReaderTypeAdapter();
			}
			@Override
			public Adapter caseDataSetReaderTransportType(DataSetReaderTransportType object) {
				return createDataSetReaderTransportTypeAdapter();
			}
			@Override
			public Adapter caseDataSetReaderMessageType(DataSetReaderMessageType object) {
				return createDataSetReaderMessageTypeAdapter();
			}
			@Override
			public Adapter caseSubscribedDataSetType(SubscribedDataSetType object) {
				return createSubscribedDataSetTypeAdapter();
			}
			@Override
			public Adapter casePubSubStatusType(PubSubStatusType object) {
				return createPubSubStatusTypeAdapter();
			}
			@Override
			public Adapter casePubSubDiagnosticsType(PubSubDiagnosticsType object) {
				return createPubSubDiagnosticsTypeAdapter();
			}
			@Override
			public Adapter caseNetworkAddressType(NetworkAddressType object) {
				return createNetworkAddressTypeAdapter();
			}
			@Override
			public Adapter caseAliasNameType(AliasNameType object) {
				return createAliasNameTypeAdapter();
			}
			@Override
			public Adapter caseBaseVariableType(BaseVariableType object) {
				return createBaseVariableTypeAdapter();
			}
			@Override
			public Adapter caseBaseDataVariableType(BaseDataVariableType object) {
				return createBaseDataVariableTypeAdapter();
			}
			@Override
			public Adapter casePropertyType(PropertyType object) {
				return createPropertyTypeAdapter();
			}
			@Override
			public Adapter caseServerVendorCapabilityType(ServerVendorCapabilityType object) {
				return createServerVendorCapabilityTypeAdapter();
			}
			@Override
			public Adapter caseSamplingIntervalDiagnosticsArrayType(SamplingIntervalDiagnosticsArrayType object) {
				return createSamplingIntervalDiagnosticsArrayTypeAdapter();
			}
			@Override
			public Adapter caseSamplingIntervalDiagnosticsType(SamplingIntervalDiagnosticsType object) {
				return createSamplingIntervalDiagnosticsTypeAdapter();
			}
			@Override
			public Adapter caseSubscriptionDiagnosticsArrayType(SubscriptionDiagnosticsArrayType object) {
				return createSubscriptionDiagnosticsArrayTypeAdapter();
			}
			@Override
			public Adapter caseSubscriptionDiagnosticsType(SubscriptionDiagnosticsType object) {
				return createSubscriptionDiagnosticsTypeAdapter();
			}
			@Override
			public Adapter caseSessionDiagnosticsArrayType(SessionDiagnosticsArrayType object) {
				return createSessionDiagnosticsArrayTypeAdapter();
			}
			@Override
			public Adapter caseSessionDiagnosticsVariableType(SessionDiagnosticsVariableType object) {
				return createSessionDiagnosticsVariableTypeAdapter();
			}
			@Override
			public Adapter caseSessionSecurityDiagnosticsArrayType(SessionSecurityDiagnosticsArrayType object) {
				return createSessionSecurityDiagnosticsArrayTypeAdapter();
			}
			@Override
			public Adapter caseSessionSecurityDiagnosticsType(SessionSecurityDiagnosticsType object) {
				return createSessionSecurityDiagnosticsTypeAdapter();
			}
			@Override
			public Adapter caseOptionSetType(OptionSetType object) {
				return createOptionSetTypeAdapter();
			}
			@Override
			public Adapter caseServerDiagnosticsSummaryType(ServerDiagnosticsSummaryType object) {
				return createServerDiagnosticsSummaryTypeAdapter();
			}
			@Override
			public Adapter caseBuildInfoType(BuildInfoType object) {
				return createBuildInfoTypeAdapter();
			}
			@Override
			public Adapter caseServerStatusType(ServerStatusType object) {
				return createServerStatusTypeAdapter();
			}
			@Override
			public Adapter caseBaseInterfaceType(BaseInterfaceType object) {
				return createBaseInterfaceTypeAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType <em>Base Object Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BaseObjectType
	 * @generated
	 */
	public Adapter createBaseObjectTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.DataTypeSystemType <em>Data Type System Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.DataTypeSystemType
	 * @generated
	 */
	public Adapter createDataTypeSystemTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ModellingRuleType <em>Modelling Rule Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ModellingRuleType
	 * @generated
	 */
	public Adapter createModellingRuleTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ServerType <em>Server Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ServerType
	 * @generated
	 */
	public Adapter createServerTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.FolderType <em>Folder Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.FolderType
	 * @generated
	 */
	public Adapter createFolderTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.DataTypeEncodingType <em>Data Type Encoding Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.DataTypeEncodingType
	 * @generated
	 */
	public Adapter createDataTypeEncodingTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ServerCapabilitiesType <em>Server Capabilities Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ServerCapabilitiesType
	 * @generated
	 */
	public Adapter createServerCapabilitiesTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ServerDiagnosticsType <em>Server Diagnostics Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ServerDiagnosticsType
	 * @generated
	 */
	public Adapter createServerDiagnosticsTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.SessionsDiagnosticsSummaryType <em>Sessions Diagnostics Summary Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.SessionsDiagnosticsSummaryType
	 * @generated
	 */
	public Adapter createSessionsDiagnosticsSummaryTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.SessionDiagnosticsObjectType <em>Session Diagnostics Object Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.SessionDiagnosticsObjectType
	 * @generated
	 */
	public Adapter createSessionDiagnosticsObjectTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.VendorServerInfoType <em>Vendor Server Info Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.VendorServerInfoType
	 * @generated
	 */
	public Adapter createVendorServerInfoTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ServerRedundancyType <em>Server Redundancy Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ServerRedundancyType
	 * @generated
	 */
	public Adapter createServerRedundancyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.FileType <em>File Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.FileType
	 * @generated
	 */
	public Adapter createFileTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.NamespacesType <em>Namespaces Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.NamespacesType
	 * @generated
	 */
	public Adapter createNamespacesTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseEventType <em>Base Event Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BaseEventType
	 * @generated
	 */
	public Adapter createBaseEventTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.AggregateFunctionType <em>Aggregate Function Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.AggregateFunctionType
	 * @generated
	 */
	public Adapter createAggregateFunctionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.StateMachineType <em>State Machine Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.StateMachineType
	 * @generated
	 */
	public Adapter createStateMachineTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.StateType <em>State Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.StateType
	 * @generated
	 */
	public Adapter createStateTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.NamespaceMetadataType <em>Namespace Metadata Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.NamespaceMetadataType
	 * @generated
	 */
	public Adapter createNamespaceMetadataTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.WriterGroupMessageType <em>Writer Group Message Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.WriterGroupMessageType
	 * @generated
	 */
	public Adapter createWriterGroupMessageTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.TransitionType <em>Transition Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.TransitionType
	 * @generated
	 */
	public Adapter createTransitionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.TemporaryFileTransferType <em>Temporary File Transfer Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.TemporaryFileTransferType
	 * @generated
	 */
	public Adapter createTemporaryFileTransferTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.RoleSetType <em>Role Set Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.RoleSetType
	 * @generated
	 */
	public Adapter createRoleSetTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.RoleType <em>Role Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.RoleType
	 * @generated
	 */
	public Adapter createRoleTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.DictionaryEntryType <em>Dictionary Entry Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.DictionaryEntryType
	 * @generated
	 */
	public Adapter createDictionaryEntryTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OrderedListType <em>Ordered List Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OrderedListType
	 * @generated
	 */
	public Adapter createOrderedListTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseConditionClassType <em>Base Condition Class Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BaseConditionClassType
	 * @generated
	 */
	public Adapter createBaseConditionClassTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.AlarmMetricsType <em>Alarm Metrics Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.AlarmMetricsType
	 * @generated
	 */
	public Adapter createAlarmMetricsTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.HistoricalDataConfigurationType <em>Historical Data Configuration Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.HistoricalDataConfigurationType
	 * @generated
	 */
	public Adapter createHistoricalDataConfigurationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.HistoryServerCapabilitiesType <em>History Server Capabilities Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.HistoryServerCapabilitiesType
	 * @generated
	 */
	public Adapter createHistoryServerCapabilitiesTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.CertificateGroupType <em>Certificate Group Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.CertificateGroupType
	 * @generated
	 */
	public Adapter createCertificateGroupTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.CertificateType <em>Certificate Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.CertificateType
	 * @generated
	 */
	public Adapter createCertificateTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ServerConfigurationType <em>Server Configuration Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ServerConfigurationType
	 * @generated
	 */
	public Adapter createServerConfigurationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.KeyCredentialConfigurationType <em>Key Credential Configuration Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.KeyCredentialConfigurationType
	 * @generated
	 */
	public Adapter createKeyCredentialConfigurationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.AuthorizationServiceConfigurationType <em>Authorization Service Configuration Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.AuthorizationServiceConfigurationType
	 * @generated
	 */
	public Adapter createAuthorizationServiceConfigurationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.AggregateConfigurationType <em>Aggregate Configuration Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.AggregateConfigurationType
	 * @generated
	 */
	public Adapter createAggregateConfigurationTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.PubSubKeyServiceType <em>Pub Sub Key Service Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.PubSubKeyServiceType
	 * @generated
	 */
	public Adapter createPubSubKeyServiceTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.SecurityGroupType <em>Security Group Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.SecurityGroupType
	 * @generated
	 */
	public Adapter createSecurityGroupTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.PublishedDataSetType <em>Published Data Set Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.PublishedDataSetType
	 * @generated
	 */
	public Adapter createPublishedDataSetTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ExtensionFieldsType <em>Extension Fields Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ExtensionFieldsType
	 * @generated
	 */
	public Adapter createExtensionFieldsTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.PubSubConnectionType <em>Pub Sub Connection Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.PubSubConnectionType
	 * @generated
	 */
	public Adapter createPubSubConnectionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ConnectionTransportType <em>Connection Transport Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ConnectionTransportType
	 * @generated
	 */
	public Adapter createConnectionTransportTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.PubSubGroupType <em>Pub Sub Group Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.PubSubGroupType
	 * @generated
	 */
	public Adapter createPubSubGroupTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.WriterGroupTransportType <em>Writer Group Transport Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.WriterGroupTransportType
	 * @generated
	 */
	public Adapter createWriterGroupTransportTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ReaderGroupTransportType <em>Reader Group Transport Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ReaderGroupTransportType
	 * @generated
	 */
	public Adapter createReaderGroupTransportTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ReaderGroupMessageType <em>Reader Group Message Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ReaderGroupMessageType
	 * @generated
	 */
	public Adapter createReaderGroupMessageTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.DataSetWriterType <em>Data Set Writer Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.DataSetWriterType
	 * @generated
	 */
	public Adapter createDataSetWriterTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.DataSetWriterTransportType <em>Data Set Writer Transport Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.DataSetWriterTransportType
	 * @generated
	 */
	public Adapter createDataSetWriterTransportTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.DataSetWriterMessageType <em>Data Set Writer Message Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.DataSetWriterMessageType
	 * @generated
	 */
	public Adapter createDataSetWriterMessageTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.DataSetReaderType <em>Data Set Reader Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.DataSetReaderType
	 * @generated
	 */
	public Adapter createDataSetReaderTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.DataSetReaderTransportType <em>Data Set Reader Transport Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.DataSetReaderTransportType
	 * @generated
	 */
	public Adapter createDataSetReaderTransportTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.DataSetReaderMessageType <em>Data Set Reader Message Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.DataSetReaderMessageType
	 * @generated
	 */
	public Adapter createDataSetReaderMessageTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.SubscribedDataSetType <em>Subscribed Data Set Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.SubscribedDataSetType
	 * @generated
	 */
	public Adapter createSubscribedDataSetTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.PubSubStatusType <em>Pub Sub Status Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.PubSubStatusType
	 * @generated
	 */
	public Adapter createPubSubStatusTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.PubSubDiagnosticsType <em>Pub Sub Diagnostics Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.PubSubDiagnosticsType
	 * @generated
	 */
	public Adapter createPubSubDiagnosticsTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.NetworkAddressType <em>Network Address Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.NetworkAddressType
	 * @generated
	 */
	public Adapter createNetworkAddressTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.AliasNameType <em>Alias Name Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.AliasNameType
	 * @generated
	 */
	public Adapter createAliasNameTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseVariableType <em>Base Variable Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BaseVariableType
	 * @generated
	 */
	public Adapter createBaseVariableTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseDataVariableType <em>Base Data Variable Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BaseDataVariableType
	 * @generated
	 */
	public Adapter createBaseDataVariableTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.PropertyType <em>Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.PropertyType
	 * @generated
	 */
	public Adapter createPropertyTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ServerVendorCapabilityType <em>Server Vendor Capability Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ServerVendorCapabilityType
	 * @generated
	 */
	public Adapter createServerVendorCapabilityTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.SamplingIntervalDiagnosticsArrayType <em>Sampling Interval Diagnostics Array Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.SamplingIntervalDiagnosticsArrayType
	 * @generated
	 */
	public Adapter createSamplingIntervalDiagnosticsArrayTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.SamplingIntervalDiagnosticsType <em>Sampling Interval Diagnostics Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.SamplingIntervalDiagnosticsType
	 * @generated
	 */
	public Adapter createSamplingIntervalDiagnosticsTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.SubscriptionDiagnosticsArrayType <em>Subscription Diagnostics Array Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.SubscriptionDiagnosticsArrayType
	 * @generated
	 */
	public Adapter createSubscriptionDiagnosticsArrayTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.SubscriptionDiagnosticsType <em>Subscription Diagnostics Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.SubscriptionDiagnosticsType
	 * @generated
	 */
	public Adapter createSubscriptionDiagnosticsTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.SessionDiagnosticsArrayType <em>Session Diagnostics Array Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.SessionDiagnosticsArrayType
	 * @generated
	 */
	public Adapter createSessionDiagnosticsArrayTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.SessionDiagnosticsVariableType <em>Session Diagnostics Variable Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.SessionDiagnosticsVariableType
	 * @generated
	 */
	public Adapter createSessionDiagnosticsVariableTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.SessionSecurityDiagnosticsArrayType <em>Session Security Diagnostics Array Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.SessionSecurityDiagnosticsArrayType
	 * @generated
	 */
	public Adapter createSessionSecurityDiagnosticsArrayTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.SessionSecurityDiagnosticsType <em>Session Security Diagnostics Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.SessionSecurityDiagnosticsType
	 * @generated
	 */
	public Adapter createSessionSecurityDiagnosticsTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.OptionSetType <em>Option Set Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.OptionSetType
	 * @generated
	 */
	public Adapter createOptionSetTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ServerDiagnosticsSummaryType <em>Server Diagnostics Summary Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ServerDiagnosticsSummaryType
	 * @generated
	 */
	public Adapter createServerDiagnosticsSummaryTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.BuildInfoType <em>Build Info Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BuildInfoType
	 * @generated
	 */
	public Adapter createBuildInfoTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.ServerStatusType <em>Server Status Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.ServerStatusType
	 * @generated
	 */
	public Adapter createServerStatusTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.opcua.opcuaprofile.BaseInterfaceType <em>Base Interface Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.opcua.opcuaprofile.BaseInterfaceType
	 * @generated
	 */
	public Adapter createBaseInterfaceTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //OPCUAProfileAdapterFactory
