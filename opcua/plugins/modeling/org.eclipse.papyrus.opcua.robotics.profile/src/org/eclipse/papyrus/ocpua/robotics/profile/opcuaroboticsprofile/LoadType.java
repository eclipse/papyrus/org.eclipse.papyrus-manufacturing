/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library._3DFrameType;
import org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library._3DVectorType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Load Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The LoadType is for describing loads mounted on the motion device.
 * A very common mounting point is the flange of a motion device.
 * Typically a motion device has additional mounting points on some of the axis. The provided values can either
 * be determined by the robot controller or can be set up by an operator.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.LoadType#getMass <em>Mass</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.LoadType#getCenterOfMass <em>Center Of Mass</em>}</li>
 *   <li>{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.LoadType#getInertia <em>Inertia</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getLoadType()
 * @model
 * @generated
 */
public interface LoadType extends EObject {
	/**
	 * Returns the value of the '<em><b>Mass</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mass</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mass</em>' containment reference.
	 * @see #setMass(org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.Double)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getLoadType_Mass()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.Double getMass();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.LoadType#getMass <em>Mass</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mass</em>' containment reference.
	 * @see #getMass()
	 * @generated
	 */
	void setMass(org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPC_UA_Robotics_CS_Library.Double value);

	/**
	 * Returns the value of the '<em><b>Center Of Mass</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Center Of Mass</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Center Of Mass</em>' containment reference.
	 * @see #setCenterOfMass(_3DFrameType)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getLoadType_CenterOfMass()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	_3DFrameType getCenterOfMass();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.LoadType#getCenterOfMass <em>Center Of Mass</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Center Of Mass</em>' containment reference.
	 * @see #getCenterOfMass()
	 * @generated
	 */
	void setCenterOfMass(_3DFrameType value);

	/**
	 * Returns the value of the '<em><b>Inertia</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inertia</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inertia</em>' containment reference.
	 * @see #setInertia(_3DVectorType)
	 * @see org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.OPCUARoboticsProfilePackage#getLoadType_Inertia()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	_3DVectorType getInertia();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.ocpua.robotics.profile.opcuaroboticsprofile.LoadType#getInertia <em>Inertia</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inertia</em>' containment reference.
	 * @see #getInertia()
	 * @generated
	 */
	void setInertia(_3DVectorType value);

} // LoadType
