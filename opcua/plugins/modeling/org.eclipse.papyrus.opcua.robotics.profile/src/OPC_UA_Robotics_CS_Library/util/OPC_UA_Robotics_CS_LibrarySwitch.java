/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package OPC_UA_Robotics_CS_Library.util;

import OPC_UA_Robotics_CS_Library.AnalogUnitType;
import OPC_UA_Robotics_CS_Library.BaseDataVariableType;
import OPC_UA_Robotics_CS_Library.DateTime;
import OPC_UA_Robotics_CS_Library.DurationString;
import OPC_UA_Robotics_CS_Library.LocalizedText;
import OPC_UA_Robotics_CS_Library.OPC_UA_Robotics_CS_LibraryPackage;
import OPC_UA_Robotics_CS_Library.RationalNumber;
import OPC_UA_Robotics_CS_Library._3DCartesianCoordinates;
import OPC_UA_Robotics_CS_Library._3DFrameType;
import OPC_UA_Robotics_CS_Library._3DOrientation;
import OPC_UA_Robotics_CS_Library._3DVector;
import OPC_UA_Robotics_CS_Library._3DVectorType;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see OPC_UA_Robotics_CS_Library.OPC_UA_Robotics_CS_LibraryPackage
 * @generated
 */
public class OPC_UA_Robotics_CS_LibrarySwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static OPC_UA_Robotics_CS_LibraryPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OPC_UA_Robotics_CS_LibrarySwitch() {
		if (modelPackage == null) {
			modelPackage = OPC_UA_Robotics_CS_LibraryPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case OPC_UA_Robotics_CS_LibraryPackage._3D_FRAME_TYPE: {
				_3DFrameType _3DFrameType = (_3DFrameType)theEObject;
				T result = case_3DFrameType(_3DFrameType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_Robotics_CS_LibraryPackage._3D_CARTESIAN_COORDINATES: {
				_3DCartesianCoordinates _3DCartesianCoordinates = (_3DCartesianCoordinates)theEObject;
				T result = case_3DCartesianCoordinates(_3DCartesianCoordinates);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_Robotics_CS_LibraryPackage._3D_ORIENTATION: {
				_3DOrientation _3DOrientation = (_3DOrientation)theEObject;
				T result = case_3DOrientation(_3DOrientation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_Robotics_CS_LibraryPackage.ANALOG_UNIT_TYPE: {
				AnalogUnitType analogUnitType = (AnalogUnitType)theEObject;
				T result = caseAnalogUnitType(analogUnitType);
				if (result == null) result = caseDouble(analogUnitType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_Robotics_CS_LibraryPackage.DOUBLE: {
				OPC_UA_Robotics_CS_Library.Double double_ = (OPC_UA_Robotics_CS_Library.Double)theEObject;
				T result = caseDouble(double_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_Robotics_CS_LibraryPackage.BASE_DATA_VARIABLE_TYPE: {
				BaseDataVariableType baseDataVariableType = (BaseDataVariableType)theEObject;
				T result = caseBaseDataVariableType(baseDataVariableType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_Robotics_CS_LibraryPackage.DURATION_STRING: {
				DurationString durationString = (DurationString)theEObject;
				T result = caseDurationString(durationString);
				if (result == null) result = caseBaseDataVariableType(durationString);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_Robotics_CS_LibraryPackage.DATE_TIME: {
				DateTime dateTime = (DateTime)theEObject;
				T result = caseDateTime(dateTime);
				if (result == null) result = caseBaseDataVariableType(dateTime);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_Robotics_CS_LibraryPackage._3D_VECTOR: {
				_3DVector _3DVector = (_3DVector)theEObject;
				T result = case_3DVector(_3DVector);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_Robotics_CS_LibraryPackage._3D_VECTOR_TYPE: {
				_3DVectorType _3DVectorType = (_3DVectorType)theEObject;
				T result = case_3DVectorType(_3DVectorType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_Robotics_CS_LibraryPackage.LOCALIZED_TEXT: {
				LocalizedText localizedText = (LocalizedText)theEObject;
				T result = caseLocalizedText(localizedText);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OPC_UA_Robotics_CS_LibraryPackage.RATIONAL_NUMBER: {
				RationalNumber rationalNumber = (RationalNumber)theEObject;
				T result = caseRationalNumber(rationalNumber);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>3D Frame Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>3D Frame Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T case_3DFrameType(_3DFrameType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>3D Cartesian Coordinates</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>3D Cartesian Coordinates</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T case_3DCartesianCoordinates(_3DCartesianCoordinates object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>3D Orientation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>3D Orientation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T case_3DOrientation(_3DOrientation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Analog Unit Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Analog Unit Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnalogUnitType(AnalogUnitType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Double</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Double</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDouble(OPC_UA_Robotics_CS_Library.Double object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Data Variable Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Data Variable Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseDataVariableType(BaseDataVariableType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Duration String</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Duration String</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDurationString(DurationString object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Date Time</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Date Time</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDateTime(DateTime object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>3D Vector</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>3D Vector</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T case_3DVector(_3DVector object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>3D Vector Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>3D Vector Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T case_3DVectorType(_3DVectorType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Localized Text</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Localized Text</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLocalizedText(LocalizedText object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rational Number</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rational Number</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRationalNumber(RationalNumber object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //OPC_UA_Robotics_CS_LibrarySwitch
