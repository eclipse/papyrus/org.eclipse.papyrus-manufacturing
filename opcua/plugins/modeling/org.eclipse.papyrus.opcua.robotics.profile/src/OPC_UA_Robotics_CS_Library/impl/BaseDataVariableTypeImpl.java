/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package OPC_UA_Robotics_CS_Library.impl;

import OPC_UA_Robotics_CS_Library.BaseDataVariableType;
import OPC_UA_Robotics_CS_Library.OPC_UA_Robotics_CS_LibraryPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Base Data Variable Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BaseDataVariableTypeImpl extends MinimalEObjectImpl.Container implements BaseDataVariableType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BaseDataVariableTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPC_UA_Robotics_CS_LibraryPackage.Literals.BASE_DATA_VARIABLE_TYPE;
	}

} //BaseDataVariableTypeImpl
