/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package OPC_UA_Robotics_CS_Library.impl;

import OPC_UA_Robotics_CS_Library.OPC_UA_Robotics_CS_LibraryPackage;
import OPC_UA_Robotics_CS_Library._3DCartesianCoordinates;
import OPC_UA_Robotics_CS_Library._3DFrameType;
import OPC_UA_Robotics_CS_Library._3DOrientation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>3D Frame Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OPC_UA_Robotics_CS_Library.impl._3DFrameTypeImpl#getCartesianCoordinates <em>Cartesian Coordinates</em>}</li>
 *   <li>{@link OPC_UA_Robotics_CS_Library.impl._3DFrameTypeImpl#getOrientation <em>Orientation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class _3DFrameTypeImpl extends MinimalEObjectImpl.Container implements _3DFrameType {
	/**
	 * The cached value of the '{@link #getCartesianCoordinates() <em>Cartesian Coordinates</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCartesianCoordinates()
	 * @generated
	 * @ordered
	 */
	protected _3DCartesianCoordinates cartesianCoordinates;

	/**
	 * The cached value of the '{@link #getOrientation() <em>Orientation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrientation()
	 * @generated
	 * @ordered
	 */
	protected _3DOrientation orientation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected _3DFrameTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPC_UA_Robotics_CS_LibraryPackage.Literals._3D_FRAME_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public _3DCartesianCoordinates getCartesianCoordinates() {
		return cartesianCoordinates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCartesianCoordinates(_3DCartesianCoordinates newCartesianCoordinates, NotificationChain msgs) {
		_3DCartesianCoordinates oldCartesianCoordinates = cartesianCoordinates;
		cartesianCoordinates = newCartesianCoordinates;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPC_UA_Robotics_CS_LibraryPackage._3D_FRAME_TYPE__CARTESIAN_COORDINATES, oldCartesianCoordinates, newCartesianCoordinates);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCartesianCoordinates(_3DCartesianCoordinates newCartesianCoordinates) {
		if (newCartesianCoordinates != cartesianCoordinates) {
			NotificationChain msgs = null;
			if (cartesianCoordinates != null)
				msgs = ((InternalEObject)cartesianCoordinates).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPC_UA_Robotics_CS_LibraryPackage._3D_FRAME_TYPE__CARTESIAN_COORDINATES, null, msgs);
			if (newCartesianCoordinates != null)
				msgs = ((InternalEObject)newCartesianCoordinates).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPC_UA_Robotics_CS_LibraryPackage._3D_FRAME_TYPE__CARTESIAN_COORDINATES, null, msgs);
			msgs = basicSetCartesianCoordinates(newCartesianCoordinates, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPC_UA_Robotics_CS_LibraryPackage._3D_FRAME_TYPE__CARTESIAN_COORDINATES, newCartesianCoordinates, newCartesianCoordinates));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public _3DOrientation getOrientation() {
		return orientation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOrientation(_3DOrientation newOrientation, NotificationChain msgs) {
		_3DOrientation oldOrientation = orientation;
		orientation = newOrientation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OPC_UA_Robotics_CS_LibraryPackage._3D_FRAME_TYPE__ORIENTATION, oldOrientation, newOrientation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrientation(_3DOrientation newOrientation) {
		if (newOrientation != orientation) {
			NotificationChain msgs = null;
			if (orientation != null)
				msgs = ((InternalEObject)orientation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OPC_UA_Robotics_CS_LibraryPackage._3D_FRAME_TYPE__ORIENTATION, null, msgs);
			if (newOrientation != null)
				msgs = ((InternalEObject)newOrientation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OPC_UA_Robotics_CS_LibraryPackage._3D_FRAME_TYPE__ORIENTATION, null, msgs);
			msgs = basicSetOrientation(newOrientation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPC_UA_Robotics_CS_LibraryPackage._3D_FRAME_TYPE__ORIENTATION, newOrientation, newOrientation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OPC_UA_Robotics_CS_LibraryPackage._3D_FRAME_TYPE__CARTESIAN_COORDINATES:
				return basicSetCartesianCoordinates(null, msgs);
			case OPC_UA_Robotics_CS_LibraryPackage._3D_FRAME_TYPE__ORIENTATION:
				return basicSetOrientation(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPC_UA_Robotics_CS_LibraryPackage._3D_FRAME_TYPE__CARTESIAN_COORDINATES:
				return getCartesianCoordinates();
			case OPC_UA_Robotics_CS_LibraryPackage._3D_FRAME_TYPE__ORIENTATION:
				return getOrientation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPC_UA_Robotics_CS_LibraryPackage._3D_FRAME_TYPE__CARTESIAN_COORDINATES:
				setCartesianCoordinates((_3DCartesianCoordinates)newValue);
				return;
			case OPC_UA_Robotics_CS_LibraryPackage._3D_FRAME_TYPE__ORIENTATION:
				setOrientation((_3DOrientation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPC_UA_Robotics_CS_LibraryPackage._3D_FRAME_TYPE__CARTESIAN_COORDINATES:
				setCartesianCoordinates((_3DCartesianCoordinates)null);
				return;
			case OPC_UA_Robotics_CS_LibraryPackage._3D_FRAME_TYPE__ORIENTATION:
				setOrientation((_3DOrientation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPC_UA_Robotics_CS_LibraryPackage._3D_FRAME_TYPE__CARTESIAN_COORDINATES:
				return cartesianCoordinates != null;
			case OPC_UA_Robotics_CS_LibraryPackage._3D_FRAME_TYPE__ORIENTATION:
				return orientation != null;
		}
		return super.eIsSet(featureID);
	}

} //_3DFrameTypeImpl
