/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package OPC_UA_Robotics_CS_Library.impl;

import OPC_UA_Robotics_CS_Library.OPC_UA_Robotics_CS_LibraryPackage;
import OPC_UA_Robotics_CS_Library._3DVectorType;

import java.lang.Double;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>3D Vector Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OPC_UA_Robotics_CS_Library.impl._3DVectorTypeImpl#getX <em>X</em>}</li>
 *   <li>{@link OPC_UA_Robotics_CS_Library.impl._3DVectorTypeImpl#getY <em>Y</em>}</li>
 *   <li>{@link OPC_UA_Robotics_CS_Library.impl._3DVectorTypeImpl#getZ <em>Z</em>}</li>
 * </ul>
 *
 * @generated
 */
public class _3DVectorTypeImpl extends MinimalEObjectImpl.Container implements _3DVectorType {
	/**
	 * The default value of the '{@link #getX() <em>X</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getX()
	 * @generated
	 * @ordered
	 */
	protected static final double X_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getX() <em>X</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getX()
	 * @generated
	 * @ordered
	 */
	protected double x = X_EDEFAULT;

	/**
	 * The default value of the '{@link #getY() <em>Y</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getY()
	 * @generated
	 * @ordered
	 */
	protected static final double Y_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getY() <em>Y</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getY()
	 * @generated
	 * @ordered
	 */
	protected double y = Y_EDEFAULT;

	/**
	 * The default value of the '{@link #getZ() <em>Z</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getZ()
	 * @generated
	 * @ordered
	 */
	protected static final double Z_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getZ() <em>Z</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getZ()
	 * @generated
	 * @ordered
	 */
	protected double z = Z_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected _3DVectorTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OPC_UA_Robotics_CS_LibraryPackage.Literals._3D_VECTOR_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getX() {
		return x;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setX(double newX) {
		double oldX = x;
		x = newX;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPC_UA_Robotics_CS_LibraryPackage._3D_VECTOR_TYPE__X, oldX, x));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getY() {
		return y;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setY(double newY) {
		double oldY = y;
		y = newY;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPC_UA_Robotics_CS_LibraryPackage._3D_VECTOR_TYPE__Y, oldY, y));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getZ() {
		return z;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setZ(double newZ) {
		double oldZ = z;
		z = newZ;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OPC_UA_Robotics_CS_LibraryPackage._3D_VECTOR_TYPE__Z, oldZ, z));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OPC_UA_Robotics_CS_LibraryPackage._3D_VECTOR_TYPE__X:
				return getX();
			case OPC_UA_Robotics_CS_LibraryPackage._3D_VECTOR_TYPE__Y:
				return getY();
			case OPC_UA_Robotics_CS_LibraryPackage._3D_VECTOR_TYPE__Z:
				return getZ();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OPC_UA_Robotics_CS_LibraryPackage._3D_VECTOR_TYPE__X:
				setX((Double)newValue);
				return;
			case OPC_UA_Robotics_CS_LibraryPackage._3D_VECTOR_TYPE__Y:
				setY((Double)newValue);
				return;
			case OPC_UA_Robotics_CS_LibraryPackage._3D_VECTOR_TYPE__Z:
				setZ((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OPC_UA_Robotics_CS_LibraryPackage._3D_VECTOR_TYPE__X:
				setX(X_EDEFAULT);
				return;
			case OPC_UA_Robotics_CS_LibraryPackage._3D_VECTOR_TYPE__Y:
				setY(Y_EDEFAULT);
				return;
			case OPC_UA_Robotics_CS_LibraryPackage._3D_VECTOR_TYPE__Z:
				setZ(Z_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OPC_UA_Robotics_CS_LibraryPackage._3D_VECTOR_TYPE__X:
				return x != X_EDEFAULT;
			case OPC_UA_Robotics_CS_LibraryPackage._3D_VECTOR_TYPE__Y:
				return y != Y_EDEFAULT;
			case OPC_UA_Robotics_CS_LibraryPackage._3D_VECTOR_TYPE__Z:
				return z != Z_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (X: ");
		result.append(x);
		result.append(", Y: ");
		result.append(y);
		result.append(", Z: ");
		result.append(z);
		result.append(')');
		return result.toString();
	}

} //_3DVectorTypeImpl
