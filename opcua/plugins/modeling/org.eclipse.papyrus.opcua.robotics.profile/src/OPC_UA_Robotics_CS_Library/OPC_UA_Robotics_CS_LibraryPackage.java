/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *    Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr
 *    Fadwa Tmar (CEA LIST) fadwa.tmar@cea.fr 
 *******************************************************************************/
/**
 */
package OPC_UA_Robotics_CS_Library;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see OPC_UA_Robotics_CS_Library.OPC_UA_Robotics_CS_LibraryFactory
 * @model kind="package"
 * @generated
 */
public interface OPC_UA_Robotics_CS_LibraryPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "OPC_UA_Robotics_CS_Library";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///OPC_UA_Robotics_CS_Library.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "OPC_UA_Robotics_CS_Library";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OPC_UA_Robotics_CS_LibraryPackage eINSTANCE = OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl.init();

	/**
	 * The meta object id for the '{@link OPC_UA_Robotics_CS_Library.impl._3DFrameTypeImpl <em>3D Frame Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see OPC_UA_Robotics_CS_Library.impl._3DFrameTypeImpl
	 * @see OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl#get_3DFrameType()
	 * @generated
	 */
	int _3D_FRAME_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Cartesian Coordinates</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int _3D_FRAME_TYPE__CARTESIAN_COORDINATES = 0;

	/**
	 * The feature id for the '<em><b>Orientation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int _3D_FRAME_TYPE__ORIENTATION = 1;

	/**
	 * The number of structural features of the '<em>3D Frame Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int _3D_FRAME_TYPE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>3D Frame Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int _3D_FRAME_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link OPC_UA_Robotics_CS_Library.impl._3DCartesianCoordinatesImpl <em>3D Cartesian Coordinates</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see OPC_UA_Robotics_CS_Library.impl._3DCartesianCoordinatesImpl
	 * @see OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl#get_3DCartesianCoordinates()
	 * @generated
	 */
	int _3D_CARTESIAN_COORDINATES = 1;

	/**
	 * The number of structural features of the '<em>3D Cartesian Coordinates</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int _3D_CARTESIAN_COORDINATES_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>3D Cartesian Coordinates</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int _3D_CARTESIAN_COORDINATES_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link OPC_UA_Robotics_CS_Library.impl._3DOrientationImpl <em>3D Orientation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see OPC_UA_Robotics_CS_Library.impl._3DOrientationImpl
	 * @see OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl#get_3DOrientation()
	 * @generated
	 */
	int _3D_ORIENTATION = 2;

	/**
	 * The number of structural features of the '<em>3D Orientation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int _3D_ORIENTATION_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>3D Orientation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int _3D_ORIENTATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link OPC_UA_Robotics_CS_Library.impl.DoubleImpl <em>Double</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see OPC_UA_Robotics_CS_Library.impl.DoubleImpl
	 * @see OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl#getDouble()
	 * @generated
	 */
	int DOUBLE = 4;

	/**
	 * The number of structural features of the '<em>Double</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Double</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link OPC_UA_Robotics_CS_Library.impl.AnalogUnitTypeImpl <em>Analog Unit Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see OPC_UA_Robotics_CS_Library.impl.AnalogUnitTypeImpl
	 * @see OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl#getAnalogUnitType()
	 * @generated
	 */
	int ANALOG_UNIT_TYPE = 3;

	/**
	 * The number of structural features of the '<em>Analog Unit Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALOG_UNIT_TYPE_FEATURE_COUNT = DOUBLE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Analog Unit Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALOG_UNIT_TYPE_OPERATION_COUNT = DOUBLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link OPC_UA_Robotics_CS_Library.impl.BaseDataVariableTypeImpl <em>Base Data Variable Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see OPC_UA_Robotics_CS_Library.impl.BaseDataVariableTypeImpl
	 * @see OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl#getBaseDataVariableType()
	 * @generated
	 */
	int BASE_DATA_VARIABLE_TYPE = 5;

	/**
	 * The number of structural features of the '<em>Base Data Variable Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_DATA_VARIABLE_TYPE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Base Data Variable Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_DATA_VARIABLE_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link OPC_UA_Robotics_CS_Library.impl.DurationStringImpl <em>Duration String</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see OPC_UA_Robotics_CS_Library.impl.DurationStringImpl
	 * @see OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl#getDurationString()
	 * @generated
	 */
	int DURATION_STRING = 6;

	/**
	 * The number of structural features of the '<em>Duration String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DURATION_STRING_FEATURE_COUNT = BASE_DATA_VARIABLE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Duration String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DURATION_STRING_OPERATION_COUNT = BASE_DATA_VARIABLE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link OPC_UA_Robotics_CS_Library.impl.DateTimeImpl <em>Date Time</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see OPC_UA_Robotics_CS_Library.impl.DateTimeImpl
	 * @see OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl#getDateTime()
	 * @generated
	 */
	int DATE_TIME = 7;

	/**
	 * The number of structural features of the '<em>Date Time</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATE_TIME_FEATURE_COUNT = BASE_DATA_VARIABLE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Date Time</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATE_TIME_OPERATION_COUNT = BASE_DATA_VARIABLE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link OPC_UA_Robotics_CS_Library.impl._3DVectorImpl <em>3D Vector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see OPC_UA_Robotics_CS_Library.impl._3DVectorImpl
	 * @see OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl#get_3DVector()
	 * @generated
	 */
	int _3D_VECTOR = 8;

	/**
	 * The number of structural features of the '<em>3D Vector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int _3D_VECTOR_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>3D Vector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int _3D_VECTOR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link OPC_UA_Robotics_CS_Library.impl._3DVectorTypeImpl <em>3D Vector Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see OPC_UA_Robotics_CS_Library.impl._3DVectorTypeImpl
	 * @see OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl#get_3DVectorType()
	 * @generated
	 */
	int _3D_VECTOR_TYPE = 9;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int _3D_VECTOR_TYPE__X = 0;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int _3D_VECTOR_TYPE__Y = 1;

	/**
	 * The feature id for the '<em><b>Z</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int _3D_VECTOR_TYPE__Z = 2;

	/**
	 * The number of structural features of the '<em>3D Vector Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int _3D_VECTOR_TYPE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>3D Vector Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int _3D_VECTOR_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link OPC_UA_Robotics_CS_Library.impl.LocalizedTextImpl <em>Localized Text</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see OPC_UA_Robotics_CS_Library.impl.LocalizedTextImpl
	 * @see OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl#getLocalizedText()
	 * @generated
	 */
	int LOCALIZED_TEXT = 10;

	/**
	 * The number of structural features of the '<em>Localized Text</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCALIZED_TEXT_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Localized Text</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCALIZED_TEXT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link OPC_UA_Robotics_CS_Library.impl.RationalNumberImpl <em>Rational Number</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see OPC_UA_Robotics_CS_Library.impl.RationalNumberImpl
	 * @see OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl#getRationalNumber()
	 * @generated
	 */
	int RATIONAL_NUMBER = 11;

	/**
	 * The number of structural features of the '<em>Rational Number</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RATIONAL_NUMBER_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Rational Number</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RATIONAL_NUMBER_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link OPC_UA_Robotics_CS_Library._3DFrameType <em>3D Frame Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>3D Frame Type</em>'.
	 * @see OPC_UA_Robotics_CS_Library._3DFrameType
	 * @generated
	 */
	EClass get_3DFrameType();

	/**
	 * Returns the meta object for the containment reference '{@link OPC_UA_Robotics_CS_Library._3DFrameType#getCartesianCoordinates <em>Cartesian Coordinates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Cartesian Coordinates</em>'.
	 * @see OPC_UA_Robotics_CS_Library._3DFrameType#getCartesianCoordinates()
	 * @see #get_3DFrameType()
	 * @generated
	 */
	EReference get_3DFrameType_CartesianCoordinates();

	/**
	 * Returns the meta object for the containment reference '{@link OPC_UA_Robotics_CS_Library._3DFrameType#getOrientation <em>Orientation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Orientation</em>'.
	 * @see OPC_UA_Robotics_CS_Library._3DFrameType#getOrientation()
	 * @see #get_3DFrameType()
	 * @generated
	 */
	EReference get_3DFrameType_Orientation();

	/**
	 * Returns the meta object for class '{@link OPC_UA_Robotics_CS_Library._3DCartesianCoordinates <em>3D Cartesian Coordinates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>3D Cartesian Coordinates</em>'.
	 * @see OPC_UA_Robotics_CS_Library._3DCartesianCoordinates
	 * @generated
	 */
	EClass get_3DCartesianCoordinates();

	/**
	 * Returns the meta object for class '{@link OPC_UA_Robotics_CS_Library._3DOrientation <em>3D Orientation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>3D Orientation</em>'.
	 * @see OPC_UA_Robotics_CS_Library._3DOrientation
	 * @generated
	 */
	EClass get_3DOrientation();

	/**
	 * Returns the meta object for class '{@link OPC_UA_Robotics_CS_Library.AnalogUnitType <em>Analog Unit Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Analog Unit Type</em>'.
	 * @see OPC_UA_Robotics_CS_Library.AnalogUnitType
	 * @generated
	 */
	EClass getAnalogUnitType();

	/**
	 * Returns the meta object for class '{@link OPC_UA_Robotics_CS_Library.Double <em>Double</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Double</em>'.
	 * @see OPC_UA_Robotics_CS_Library.Double
	 * @generated
	 */
	EClass getDouble();

	/**
	 * Returns the meta object for class '{@link OPC_UA_Robotics_CS_Library.BaseDataVariableType <em>Base Data Variable Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Data Variable Type</em>'.
	 * @see OPC_UA_Robotics_CS_Library.BaseDataVariableType
	 * @generated
	 */
	EClass getBaseDataVariableType();

	/**
	 * Returns the meta object for class '{@link OPC_UA_Robotics_CS_Library.DurationString <em>Duration String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Duration String</em>'.
	 * @see OPC_UA_Robotics_CS_Library.DurationString
	 * @generated
	 */
	EClass getDurationString();

	/**
	 * Returns the meta object for class '{@link OPC_UA_Robotics_CS_Library.DateTime <em>Date Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Date Time</em>'.
	 * @see OPC_UA_Robotics_CS_Library.DateTime
	 * @generated
	 */
	EClass getDateTime();

	/**
	 * Returns the meta object for class '{@link OPC_UA_Robotics_CS_Library._3DVector <em>3D Vector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>3D Vector</em>'.
	 * @see OPC_UA_Robotics_CS_Library._3DVector
	 * @generated
	 */
	EClass get_3DVector();

	/**
	 * Returns the meta object for class '{@link OPC_UA_Robotics_CS_Library._3DVectorType <em>3D Vector Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>3D Vector Type</em>'.
	 * @see OPC_UA_Robotics_CS_Library._3DVectorType
	 * @generated
	 */
	EClass get_3DVectorType();

	/**
	 * Returns the meta object for the attribute '{@link OPC_UA_Robotics_CS_Library._3DVectorType#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X</em>'.
	 * @see OPC_UA_Robotics_CS_Library._3DVectorType#getX()
	 * @see #get_3DVectorType()
	 * @generated
	 */
	EAttribute get_3DVectorType_X();

	/**
	 * Returns the meta object for the attribute '{@link OPC_UA_Robotics_CS_Library._3DVectorType#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Y</em>'.
	 * @see OPC_UA_Robotics_CS_Library._3DVectorType#getY()
	 * @see #get_3DVectorType()
	 * @generated
	 */
	EAttribute get_3DVectorType_Y();

	/**
	 * Returns the meta object for the attribute '{@link OPC_UA_Robotics_CS_Library._3DVectorType#getZ <em>Z</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Z</em>'.
	 * @see OPC_UA_Robotics_CS_Library._3DVectorType#getZ()
	 * @see #get_3DVectorType()
	 * @generated
	 */
	EAttribute get_3DVectorType_Z();

	/**
	 * Returns the meta object for class '{@link OPC_UA_Robotics_CS_Library.LocalizedText <em>Localized Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Localized Text</em>'.
	 * @see OPC_UA_Robotics_CS_Library.LocalizedText
	 * @generated
	 */
	EClass getLocalizedText();

	/**
	 * Returns the meta object for class '{@link OPC_UA_Robotics_CS_Library.RationalNumber <em>Rational Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rational Number</em>'.
	 * @see OPC_UA_Robotics_CS_Library.RationalNumber
	 * @generated
	 */
	EClass getRationalNumber();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OPC_UA_Robotics_CS_LibraryFactory getOPC_UA_Robotics_CS_LibraryFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link OPC_UA_Robotics_CS_Library.impl._3DFrameTypeImpl <em>3D Frame Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see OPC_UA_Robotics_CS_Library.impl._3DFrameTypeImpl
		 * @see OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl#get_3DFrameType()
		 * @generated
		 */
		EClass _3D_FRAME_TYPE = eINSTANCE.get_3DFrameType();

		/**
		 * The meta object literal for the '<em><b>Cartesian Coordinates</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference _3D_FRAME_TYPE__CARTESIAN_COORDINATES = eINSTANCE.get_3DFrameType_CartesianCoordinates();

		/**
		 * The meta object literal for the '<em><b>Orientation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference _3D_FRAME_TYPE__ORIENTATION = eINSTANCE.get_3DFrameType_Orientation();

		/**
		 * The meta object literal for the '{@link OPC_UA_Robotics_CS_Library.impl._3DCartesianCoordinatesImpl <em>3D Cartesian Coordinates</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see OPC_UA_Robotics_CS_Library.impl._3DCartesianCoordinatesImpl
		 * @see OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl#get_3DCartesianCoordinates()
		 * @generated
		 */
		EClass _3D_CARTESIAN_COORDINATES = eINSTANCE.get_3DCartesianCoordinates();

		/**
		 * The meta object literal for the '{@link OPC_UA_Robotics_CS_Library.impl._3DOrientationImpl <em>3D Orientation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see OPC_UA_Robotics_CS_Library.impl._3DOrientationImpl
		 * @see OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl#get_3DOrientation()
		 * @generated
		 */
		EClass _3D_ORIENTATION = eINSTANCE.get_3DOrientation();

		/**
		 * The meta object literal for the '{@link OPC_UA_Robotics_CS_Library.impl.AnalogUnitTypeImpl <em>Analog Unit Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see OPC_UA_Robotics_CS_Library.impl.AnalogUnitTypeImpl
		 * @see OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl#getAnalogUnitType()
		 * @generated
		 */
		EClass ANALOG_UNIT_TYPE = eINSTANCE.getAnalogUnitType();

		/**
		 * The meta object literal for the '{@link OPC_UA_Robotics_CS_Library.impl.DoubleImpl <em>Double</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see OPC_UA_Robotics_CS_Library.impl.DoubleImpl
		 * @see OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl#getDouble()
		 * @generated
		 */
		EClass DOUBLE = eINSTANCE.getDouble();

		/**
		 * The meta object literal for the '{@link OPC_UA_Robotics_CS_Library.impl.BaseDataVariableTypeImpl <em>Base Data Variable Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see OPC_UA_Robotics_CS_Library.impl.BaseDataVariableTypeImpl
		 * @see OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl#getBaseDataVariableType()
		 * @generated
		 */
		EClass BASE_DATA_VARIABLE_TYPE = eINSTANCE.getBaseDataVariableType();

		/**
		 * The meta object literal for the '{@link OPC_UA_Robotics_CS_Library.impl.DurationStringImpl <em>Duration String</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see OPC_UA_Robotics_CS_Library.impl.DurationStringImpl
		 * @see OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl#getDurationString()
		 * @generated
		 */
		EClass DURATION_STRING = eINSTANCE.getDurationString();

		/**
		 * The meta object literal for the '{@link OPC_UA_Robotics_CS_Library.impl.DateTimeImpl <em>Date Time</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see OPC_UA_Robotics_CS_Library.impl.DateTimeImpl
		 * @see OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl#getDateTime()
		 * @generated
		 */
		EClass DATE_TIME = eINSTANCE.getDateTime();

		/**
		 * The meta object literal for the '{@link OPC_UA_Robotics_CS_Library.impl._3DVectorImpl <em>3D Vector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see OPC_UA_Robotics_CS_Library.impl._3DVectorImpl
		 * @see OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl#get_3DVector()
		 * @generated
		 */
		EClass _3D_VECTOR = eINSTANCE.get_3DVector();

		/**
		 * The meta object literal for the '{@link OPC_UA_Robotics_CS_Library.impl._3DVectorTypeImpl <em>3D Vector Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see OPC_UA_Robotics_CS_Library.impl._3DVectorTypeImpl
		 * @see OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl#get_3DVectorType()
		 * @generated
		 */
		EClass _3D_VECTOR_TYPE = eINSTANCE.get_3DVectorType();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute _3D_VECTOR_TYPE__X = eINSTANCE.get_3DVectorType_X();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute _3D_VECTOR_TYPE__Y = eINSTANCE.get_3DVectorType_Y();

		/**
		 * The meta object literal for the '<em><b>Z</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute _3D_VECTOR_TYPE__Z = eINSTANCE.get_3DVectorType_Z();

		/**
		 * The meta object literal for the '{@link OPC_UA_Robotics_CS_Library.impl.LocalizedTextImpl <em>Localized Text</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see OPC_UA_Robotics_CS_Library.impl.LocalizedTextImpl
		 * @see OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl#getLocalizedText()
		 * @generated
		 */
		EClass LOCALIZED_TEXT = eINSTANCE.getLocalizedText();

		/**
		 * The meta object literal for the '{@link OPC_UA_Robotics_CS_Library.impl.RationalNumberImpl <em>Rational Number</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see OPC_UA_Robotics_CS_Library.impl.RationalNumberImpl
		 * @see OPC_UA_Robotics_CS_Library.impl.OPC_UA_Robotics_CS_LibraryPackageImpl#getRationalNumber()
		 * @generated
		 */
		EClass RATIONAL_NUMBER = eINSTANCE.getRationalNumber();

	}

} //OPC_UA_Robotics_CS_LibraryPackage
