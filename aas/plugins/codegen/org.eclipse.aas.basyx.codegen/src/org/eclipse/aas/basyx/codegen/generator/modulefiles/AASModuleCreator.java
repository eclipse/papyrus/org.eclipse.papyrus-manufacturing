/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.basyx.codegen.generator.modulefiles;

import java.util.Calendar;
import java.util.List;

import org.eclipse.aas.api.aas.AssetAdministrationShell;
import org.eclipse.aas.api.asset.Asset;
import org.eclipse.aas.api.communications.Endpoint;
import org.eclipse.aas.api.submodel.SubModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AASModuleCreator {

	private static final Logger logger = LoggerFactory.getLogger(AASModuleCreator.class);
	private int year = Calendar.getInstance().get(Calendar.YEAR);
	private String namespace;
	private AssetAdministrationShell aas;
	private Asset asset;
	
	/**
	 * Constructor for the ASModuleCreator Class. It receives: 
	 * 	1. An AAS Instance as parameter
	 * 	2. An Asset Instance as parameter
	 * 	3. A Namespace String Instance as parameter
	 * 
	 * and initializes the class. 
	 * 
	 * @param aas
	 * @param asset
	 * @param namespace
	 */
	public AASModuleCreator(AssetAdministrationShell aas, Asset asset, String namespace) {
		this.aas = aas;
		this.asset = asset;
		this.namespace = namespace;
		
		logger.info("AASModuleCreator Initialised with AAS: " + aas.getIdShort()
		+ ", Asset: " + asset.getIdShort() + " and namespace: " + this.namespace);
		
	}
	
	/**
	 * Generates the Code for AASServer class as String. 
	 * 
	 * @return Returns a String with the generated Code for the AASServer class. 
	 */
	public String createAASServer() {
		
		String text = 
				"/*******************************************************************************\r\n"
				+ " * Copyright (c) " + year +" DFKI.\n"
				+ " *\r\n"
				+ " * This program and the accompanying materials are made\r\n"
				+ " * available under the terms of the Eclipse Public License 2.0\r\n"
				+ " * which is available at https://www.eclipse.org/legal/epl-2.0/\r\n"
				+ " *\r\n"
				+ " * SPDX-License-Identifier: EPL-2.0\r\n"
				+ " * Contributor:\r\n"
				+ " * 		DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>\r\n"
				+ "	*		FESTO - Moritz Marseu <moritz.marseu@festo.com>\r\n"
				+ " ******************************************************************************/\r\n"
				+ "package " + this.namespace + ".module;\r\n"
				+ "\r\n"
				+ "import java.io.IOException;\r\n"
				+ "import org.apache.commons.cli.CommandLine;\r\n"
				+ "import org.apache.commons.cli.DefaultParser;\r\n"
				+ "import org.apache.commons.cli.HelpFormatter;\r\n"
				+ "import org.apache.commons.cli.Options;\r\n"
				+ "import org.apache.commons.cli.ParseException;\r\n"
				+ "\r\n"
				+ "import org.eclipse.basyx.vab.protocol.http.server.BaSyxContext;\r\n"
				+ "import org.eclipse.basyx.vab.protocol.http.server.BaSyxHTTPServer;\r\n"
				+ "import com.festo.aas.p4m.configuration.CommandLineOptions;\r\n"
				+ "\r\n"
				+ "import org.slf4j.Logger;\r\n"
				+ "import org.slf4j.LoggerFactory;\r\n"
				+ "\r\n"
				+ "import " + this.namespace + ".connection.ConnectedDevices;\r\n"
				+ "\r\n"
				+ "/**\r\n"
				+ " * The central class of this AAS application. This class contains the main method which createst the AAS Model\r\n"
				+ " * and launches the server which exposes the model on the network. \r\n"
				+ " * \r\n"
				+ " * @author DFKI\r\n"
				+ " *\r\n"
				+ " */\r\n"
				+ "public class AASServer extends BaSyxHTTPServer {\r\n"
				+ "	\r\n"
				+ "	private static final Logger logger = LoggerFactory.getLogger(AASServer.class);\r\n"
				+ "	private static Settings configuration;\r\n"
				+ "	private static AASModels models;\r\n"
				+ "	private static ConnectedDevices connectors;\r\n"
				+ "	\r\n"
				+ "	/**\r\n"
				+ "	 * Creates a new instance of this application. \r\n"
				+ "	 * \r\n"
				+ "	 * @param context	A context object which contains data needed for the server. \r\n"
				+ "	 * \r\n"
				+ "	 */\r\n"
				+ "	private AASServer(BaSyxContext context) {\r\n"
				+ "		super(context);\r\n"
				+ "	}\r\n"
				+ "\r\n"
				+ "	/**\r\n"
				+ "	 * This main method creates all required objects and launches a server exposing this AAS Model on the network. \r\n"
				+ "	 * \r\n"
				+ "	 * @param args\r\n"
				+ "	 * @throws IOException \r\n"
				+ "	 */\r\n"
				+ "	public static void main(String[] args) throws Exception {\r\n"
				+ "		\r\n"
				+ "		CommandLine cmd = parseCommandLine(args);\r\n"
				+ "		// Load application settings\r\n"
				+ "		if(cmd.hasOption(\"p\")) {\r\n"
				+ "			configuration = new Settings(cmd.getOptionValue(\"p\"));\r\n"
				+ "		}\r\n"
				+ "		\r\n"
				+ "		else { \r\n"
				+ "			configuration = new Settings();\r\n"
				+ "		}\r\n"
				+ "		\r\n"
				+ "		configuration.load();\r\n"
				+ "		\r\n"
				+ "		connectors = ConnectedDevices.getInstance();\r\n"
				+ "		models = new AASModels();\r\n"
				+ "		\r\n"
				+ "		BaSyxContext context = DeviceContext.forModels(models);\r\n"
				+ "		AASServer app = new AASServer(context);\r\n"
				+ "		app.start();\r\n"
				+ "		\r\n"
				+ "		if (app.hasEnded()) {\r\n"
				+ "            logger.error(\"AAS server failed to start. Process is terminating.\");\r\n"
				+ "            System.exit(1);\r\n"
				+ "        } \r\n"
				+ "		else {\r\n"
				+ "        	String contextPath = \"/aas\";\r\n"
				+ "            String serverUrl = String.format(\"http://%s:%d%s\", configuration.applicationHostname.get(), configuration.applicationPort.get(),\r\n"
				+ "                    contextPath);\r\n"
				+ "            logger.info(\"AAS '{}' is listening at: {}\", configuration.aasName.get(), serverUrl);\r\n"
				+ "            logger.info(\r\n"
				+ "                    \"You can find the API documentation at https://app.swaggerhub.com/apis/BaSyx/basyx_submodel_repository_http_rest_api/v1#/\");\r\n"
				+ "        }\r\n"
				+ "\r\n"
				+ "	}\r\n"
				+ "	\r\n"
				+ "	/**\r\n"
				+ "	 * Gets the AAS models this application is serving. \r\n"
				+ "	 * \r\n"
				+ "	 * @return The models this AAS is serving. \r\n"
				+ "	 */\r\n"
				+ "	public static AASModels getModels() {\r\n"
				+ "		return models;\r\n"
				+ "	}\r\n"
				+ "	\r\n"
				+ "	/**\r\n"
				+ "	 * Gets an object which contains all configured connectors to external servers. \r\n"
				+ "	 * \r\n"
				+ "	 * @return An object with all configured connectors. \r\n"
				+ "	 */\r\n"
				+ "	public static ConnectedDevices getConnectors() {\r\n"
				+ "		return connectors;\r\n"
				+ "	}\r\n"
				+ "	\r\n"
				+ "	/**\r\n"
				+ "	 * Gets the object which contains the externalised configuration properties.\r\n"
				+ "	 * \r\n"
				+ "	 * <p>\r\n"
				+ "	 * Some settings for this application can be easily changed in a text file without touching the \r\n"
				+ "	 * source code. These settings are available from this object. \r\n"
				+ "	 * \r\n"
				+ "	 * @return The configuration object.\r\n"
				+ "	 */\r\n"
				+ "	public static Settings getSettings() {\r\n"
				+ "		return configuration;\r\n"
				+ "	}\r\n"
				+ "	\r\n"
				+ "	/**\r\n"
				+ "	 * Parses the command line arguments passed to this application.\r\n"
				+ "	 * \r\n"
				+ "	 * <p>\r\n"
				+ "	 * A set of default options are taken from {@link CommandLineOptions#getDefaultOptions()}. This\r\n"
				+ "	 * application could add its own additional options, if required.\r\n"
				+ "	 * \r\n"
				+ "	 * <p>\r\n"
				+ "	 * If the command line can't be parsed, a help text is displayed on STDOUT and the application\r\n"
				+ "	 * exits.\r\n"
				+ "	 * \r\n"
				+ "	 * @param args The command line arguments passed to this application.\r\n"
				+ "	 * \r\n"
				+ "	 * @author Moritz Marseu\r\n"
				+ "	 * \r\n"
				+ "	 * @return The parsed command line.\r\n"
				+ "	 */\r\n"
				+ "	private static CommandLine parseCommandLine(String[] args) {\r\n"
				+ "  \r\n"
				+ "		Options options = CommandLineOptions.getDefaultOptions();\r\n"
				+ "\r\n"
				+ "		DefaultParser parser = new DefaultParser();\r\n"
				+ "     \r\n"
				+ "		try {\r\n"
				+ "         \r\n"
				+ "			return parser.parse(options, args);\r\n"
				+ "     \r\n"
				+ "		} catch (ParseException e) {\r\n"
				+ "         \r\n"
				+ "			HelpFormatter help = new HelpFormatter();\r\n"
				+ "         \r\n"
				+ "			help.printHelp(\"java -jar <jar_file>\", options);\r\n"
				+ "         \r\n"
				+ "			System.exit(1);\r\n"
				+ "         \r\n"
				+ "			return null;\r\n"
				+ "		}\r\n"
				+ "	}\r\n"
				+ "\r\n"
				+ "}\r\n"
				+ "\r\n"
				+ "\r\n"
				+ "";
		
		logger.info("AASServer code generated successfully!");
		
		return text;
		
	}
	
	/**
	 * Generates the Code for DeviceContext class as String. 
	 * 
	 * @return Returns a String with the generated Code for the DeviceContext class. 
	 */
	public String createDeviceContext() {
		
		String text = 
				"/*******************************************************************************\r\n"
				+ " * Copyright (c) " + year +" DFKI.\n"
				+ " *\r\n"
				+ " * This program and the accompanying materials are made\r\n"
				+ " * available under the terms of the Eclipse Public License 2.0\r\n"
				+ " * which is available at https://www.eclipse.org/legal/epl-2.0/\r\n"
				+ " *\r\n"
				+ " * SPDX-License-Identifier: EPL-2.0\r\n"
				+ " * Contributor:\r\n"
				+ " * 		DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>\r\n"
				+ " ******************************************************************************/\r\n"
				+ "package " + this.namespace + ".module;\r\n"
				+ "\r\n"
				+ "import javax.servlet.http.HttpServlet;\r\n"
				+ "import org.eclipse.basyx.aas.restapi.AASModelProvider;\r\n"
				+ "import org.eclipse.basyx.aas.restapi.MultiSubmodelProvider;\r\n"
				+ "import org.eclipse.basyx.submodel.restapi.SubmodelProvider;\r\n"
				+ "import org.eclipse.basyx.vab.modelprovider.api.IModelProvider;\r\n"
				+ "import org.eclipse.basyx.vab.protocol.http.server.BaSyxContext;\r\n"
				+ "import org.eclipse.basyx.vab.protocol.http.server.VABHTTPInterface;\r\n"
				+ "\r\n"
				+ "/**\r\n"
				+ " * Device Context returns an Object of Type 'BaSyxContext' for each SubModels. \r\n"
				+ " */\r\n"
				+ "public final class DeviceContext {\r\n"
				+ "	\r\n"
				+ "	public static BaSyxContext forModels(AASModels models) {\r\n"
				+ "		\r\n"
				+ "		String hostName = AASServer.getSettings().applicationHostname.get();\r\n"
				+ "		int port = AASServer.getSettings().applicationPort.get();\r\n"
				+ "		BaSyxContext context = new BaSyxContext(\"\", \"\", hostName, port);\r\n"
				+ "		\r\n"
				+ "		MultiSubmodelProvider aasProvider = new MultiSubmodelProvider();\r\n"
				+ "		aasProvider.setAssetAdministrationShell(new AASModelProvider(models.aas));\r\n"
				+ "		\r\n"
				+ "		models.getSubmodels().forEach((sm) -> {\r\n"
				+ "			aasProvider.addSubmodel(new SubmodelProvider(sm.getSubmodel()));\r\n"
				+ "		});\r\n"
				+ "		\r\n"
				+ "		HttpServlet servlet = new VABHTTPInterface<IModelProvider>(aasProvider);\r\n"
				+ "		context.addServletMapping(\"/*\", servlet);\r\n"
				+ "		\r\n"
				+ "		return context;\r\n"
				+ "		\r\n"
				+ "	}\r\n"
				+ "}";
		
		logger.info("DeviceContext code generated successfully!");
		
		return text;
	}
	
	/**
	 * Generates the Code for AASModels class as String. 
	 * 
	 * @return Returns a String with the generated Code for the AASModels class. 
	 */
	public String createAASModels() {
		
		String subModelImports = "";
		String fieldBody = "";
		String constructorBody = "";
		
		List<SubModel> subModels = aas.getSubModels();
		
		for (SubModel subModel : subModels) {
			
			subModelImports += "import " + this.namespace + ".module.submodels." + 
					subModel.getIdShort().toLowerCase() + "." + subModel.getIdShort() + "; \r\n";
			
			fieldBody += "	/** The submodel '" + subModel.getIdShort() +"'. */ \r\n"
					+ "	public final SubmodelWrapper " + subModel.getIdShort() + ";\r\n"
					+ "	\r\n";
			
			constructorBody += 
					"		" + subModel.getIdShort() + " = new SubmodelWrapper(new " + subModel.getIdShort() + "());\r\n"
					+ "		aas.addSubmodel(" + subModel.getIdShort() + ".getSubmodel());\r\n"
					+ "		listSubmodels.add(" + subModel.getIdShort() + "); \r\n";
		}
		
		String text = 
				"/*******************************************************************************\r\n"
				+ " * Copyright (c) " + year +" DFKI.\n"
				+ " *\r\n"
				+ " * This program and the accompanying materials\r\n"
				+ " * are made available under the terms of the Eclipse Public License 2.0\r\n"
				+ " * which accompanies this distribution, and is available at\r\n"
				+ " * https://www.eclipse.org/legal/epl-2.0/\r\n"
				+ " *\r\n"
				+ " * SPDX-License-Identifier: EPL-2.0\r\n"
				+ " *\r\n"
				+ " * Contributors:\r\n"
				+ " *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>\r\n"
				+ " *******************************************************************************/\r\n"
				+ "package " + this.namespace + ".module;\r\n"
				+ "\r\n"
				+ "import java.net.URI;\r\n"
				+ "import java.util.Arrays;\r\n"
				+ "import java.util.HashSet;\r\n"
				+ "import java.util.Set;\r\n"
				+ "\r\n"
				+ "import org.eclipse.basyx.aas.metamodel.api.parts.asset.AssetKind;\r\n"
				+ "import org.eclipse.basyx.aas.metamodel.map.AssetAdministrationShell;\r\n"
				+ "import org.eclipse.basyx.aas.metamodel.map.parts.Asset;\r\n"
				+ "import org.eclipse.basyx.submodel.metamodel.api.identifier.IdentifierType;\r\n"
				+ "import com.festo.aas.p4m.connection.SubmodelWrapper;\r\n"
				+ "\r\n"
				+ subModelImports 
				+ "\r\n"
				+ "/**\r\n"
				+ " * Contains the major components of the AAS Model.\r\n"
				+ " * \r\n"
				+ " * <p>\r\n"
				+ " * 'AAS Model' - includes the AAS itself the assets and the submodels. \r\n"
				+ " * \r\n"
				+ " * <p>\r\n"
				+ " * This class creates and exposes the AAS itself as well as its asset, concept descriptions and submodels. \r\n"
				+ " * \r\n"
				+ " * @author DFKI\r\n"
				+ " *\r\n"
				+ " */ \r\n"
				+ "public class AASModels { \r\n"
				+ "\r\n"
				+ "	/** The asset represented by this AAS. */\r\n"
				+ "	public final Asset asset;\r\n"
				+ "	\r\n"
				+ "	/** The AAS object itself. */\r\n"
				+ "	public final AssetAdministrationShell aas;\r\n"
				+ "	\r\n"
				+ "	/** Holds all concept descriptions. */\r\n"
				+ "	public final ConceptDescriptions conceptDescriptions;\r\n"
				+ "\r\n"
				+ "	/** List of Submodels. **/\r\n"
				+ "	public final Set<SubmodelWrapper> listSubmodels = new HashSet<>();\r\n"
				+ "	\r\n"
				+ "	" + fieldBody
				+ "	\r\n"
				+ "	/**\r\n"
				+ "	 * Constructor Method.\r\n"
				+ "	 * Creates all components of this AAS model and stores references to them in fields. \r\n"
				+ "	 */\r\n"
				+ "	public AASModels() {\r\n"
				+ "		\r\n"
				+ "		Settings settings = AASServer.getSettings();	\r\n"
				+ "		String assetIdShort = settings.assetName.get();\r\n"
				+ "		URI assetId = settings.assetUri.get();			\r\n"
				+ "		String aasIdShort = settings.aasName.get();\r\n"
				+ "		URI aasId = settings.aasUri.get();\r\n"
				+ "		\r\n"
				+ "		\r\n"
				+ "		aas = createAas(aasIdShort, aasId);\r\n"
				+ "		\r\n"
				+ "		asset = createAsset(assetIdShort, assetId);\r\n"
				+ "		aas.setAsset(asset);\r\n"
				+ "		\r\n"
				+ "		conceptDescriptions = new ConceptDescriptions();\r\n"
				+ "		aas.setConceptDictionary(Arrays.asList(conceptDescriptions));\r\n"
				+ "		\r\n"
				+ constructorBody
				+ "	}\r\n"
				+ "	\r\n"
				+ "	/**\r\n"
				+ "	 * Creates an asset object with the given idShort and id. \r\n"
				+ "	 * \r\n"
				+ "	 * @param idShort	The asset's idShort. \r\n"
				+ "	 * @param id		The asset's globally unique id. Must be an IRI. (i.e., an URI)\r\n"
				+ "	 * \r\n"
				+ "	 * @return			The created asset. \r\n"
				+ "	 */\r\n"
				+ "	private Asset createAsset(String idShort, URI id) {\r\n"
				+ "		\r\n"
				+ "		Asset asset = new Asset();\r\n"
				+ "		asset.setAssetKind(AssetKind.INSTANCE);\r\n"
				+ "		asset.setIdShort(idShort);\r\n"
				+ "		asset.setIdentification(IdentifierType.IRI, id.toString());\r\n"
				+ "		\r\n"
				+ "		return asset;\r\n"
				+ "		\r\n"
				+ "	}\r\n"
				+ "	\r\n"
				+ "	/**\r\n"
				+ "	 * Creates an AAS object with the given idShort and id. \r\n"
				+ "	 * \r\n"
				+ "	 * @param idShort	The AAS' idShort.\r\n"
				+ "	 * @param id		The AAS' globally unique id. Must be an IRI (i.e., an URI)\r\n"
				+ "	 * \r\n"
				+ "	 * @return			The created AAS.\r\n"
				+ "	 */\r\n"
				+ "	private AssetAdministrationShell createAas(String idShort, URI id) {\r\n"
				+ "		AssetAdministrationShell aas = new AssetAdministrationShell();\r\n"
				+ "		aas.setIdShort(idShort);\r\n"
				+ "		aas.setIdentification(IdentifierType.IRI, id.toString());\r\n"
				+ "		\r\n"
				+ "		return aas;\r\n"
				+ "	}\r\n"
				+ "	\r\n"
				+ "	\r\n"
				+ "	public Set<SubmodelWrapper> getSubmodels() {\r\n"
				+ "		return listSubmodels;\r\n"
				+ "	}\r\n"
				+ "\r\n"
				+ "}";
		
		logger.info("AASModels code generated successfully!");
		
		return text;
	}
	
	/**
	 * Generates the Code for Settings class as String. 
	 * 
	 * @return Returns a String with the generated Code for the Settings class. 
	 */
	public String createSettings() {
		
		String text = "";
		String serverFields = "";
		String subModelFields = "";
		
		List<Endpoint> endpoints = asset.getEndpoints();
		List<SubModel> subModels = aas.getSubModels();
		
		for (Endpoint endpoint : endpoints) {
			serverFields += "/**\r\n"
					+ "	 * The address of the endpoint named '" + endpoint.getName() + "'.\r\n"
					+ "	 */\r\n"
					+ "	@LoadableProperty\r\n"
					+ "	public final StringProperty " + endpoint.getName() + " = new StringProperty(\"asset.endpoint." + endpoint.getName() + "\", new UrlValidator());\r\n"
					+ "	\r\n";
		}
		
		for (SubModel subModel : subModels) {
			
			if (subModel.getKind().toString() == "INSTANCE") {
				
				if (subModel.getIdentification().getIdType().toString() == "IRDI") {
					
					throw new IllegalStateException("Submodel of ModelingKind - INSTANCE "
							+ "does not support IRDI as IdentifierType "
							+ "according to the Details of the "
							+ "Asset Administration Shell (Page 36). \r\n" 
							+ "Set SubModel IdentifierType to IRI or CUSTOM.");
					
				}
				
				else if (subModel.getIdentification().getIdType().toString() == "IRI") {
					
					subModelFields += 
							"	@LoadableProperty\r\n"
							+ "	public final UriProperty SUBMODEL_" + subModel.getIdShort().toUpperCase() + "_IRI = new UriProperty(\"submodel." + subModel.getIdShort() + ".uri\");\r\n"
							+ "\r\n";
					
				}
				
				else if (subModel.getIdentification().getIdType().toString() == "CUSTOM") {
					
					subModelFields += 
							"	@LoadableProperty\r\n"
							+ "	public final StringProperty SUBMODEL_" + subModel.getIdShort().toUpperCase() + "_CUSTOM = new StringProperty(\"submodel." + subModel.getIdShort() + ".custom\");\r\n"
							+ "\r\n";
					
				}
				
			}
			
			else if (subModel.getKind().toString() == "TEMPLATE") {
				
				if (subModel.getIdentification().getIdType().toString() == "IRDI") {
					
					subModelFields += 
							"	@LoadableProperty\r\n"
							+ "	public final StringProperty SUBMODEL_" + subModel.getIdShort().toUpperCase() + "_IRDI = new StringProperty(\"submodel." + subModel.getIdShort() + ".irdi\");\r\n"
							+ "\r\n";
					
				}
				
				else if (subModel.getIdentification().getIdType().toString() == "IRI") {
					
					subModelFields += 
							"	@LoadableProperty\r\n"
							+ "	public final UriProperty SUBMODEL_" + subModel.getIdShort().toUpperCase() + "_IRI = new UriProperty(\"submodel." + subModel.getIdShort() + ".uri\");\r\n"
							+ "\r\n";					
				}
				
				else if (subModel.getIdentification().getIdType().toString() == "CUSTOM") {
					
					throw new IllegalStateException("Submodel of ModelingKind - TEMPLATE "
							+ "does not support CUSTOM as IdentifierType "
							+ "according to the Details of the "
							+ "Asset Administration Shell (Page 36). \r\n" 
							+ "Set SubModel IdentifierType to IRI or IRDI.");
					
				}
				
			}
			
		}
		
		text = 
				"/*******************************************************************************\r\n"
				+ " * Copyright (c) " + year +" DFKI.\n"
				+ " *\r\n"
				+ " * This program and the accompanying materials\r\n"
				+ " * are made available under the terms of the Eclipse Public License 2.0\r\n"
				+ " * which accompanies this distribution, and is available at\r\n"
				+ " * https://www.eclipse.org/legal/epl-2.0/\r\n"
				+ " *\r\n"
				+ " * SPDX-License-Identifier: EPL-2.0\r\n"
				+ " *\r\n"
				+ " * Contributors:\r\n"
				+ " *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>\r\n"
				+ " *******************************************************************************/\r\n"
				+ "package " + this.namespace + ".module;\r\n"
				+ "\r\n"
				+ "import java.io.IOException;\r\n"
				+ "\r\n"
				+ "import com.festo.aas.p4m.configuration.AasProperties;\r\n"
				+ "import com.festo.aas.p4m.configuration.LoadableProperty;\r\n"
				+ "import com.festo.aas.p4m.validators.UrlValidator;\r\n"
				+ "\r\n"
				+ "/**\r\n"
				+ " * Holds the settings for this AAS application. For more details, refer to {@link AasProperties}\r\n"
				+ " * @author DFKI\r\n"
				+ " *\r\n"
				+ " */\r\n"
				+ "\r\n"
				+ "public final class Settings extends AasProperties {\r\n"
				+ "	\r\n"
				+ "	" + serverFields
				+ "	\r\n"
				+ "	/**\r\n"
				+ "	 * The URI identifying the Submodels. This serves as the submodel's global unique id.\r\n"
				+ "	 * \r\n"
				+ "	 * <p>\r\n"
				+ "	 * This is can be an URL[Uniform Resource Locator] (e.g. <code>http://www.example.com/myAAS1234/</code>), an URN\r\n"
				+ "	 * [Uniform Resource Name] (e.g. <code>urn:uuid:6e8bc430-9c3a-11d9-9669-0800200c9a66</code>)\r\n"
				+ "	 * \r\n"
				+ "	 * <p>\r\n"
				+ "	 * This is merely an identifier. Even if an URL is specified, this URL isn't required to be accessible on any network. \r\n"
				+ "	 * \r\n"
				+ "	 */ \r\n"
				+ subModelFields
				+ "\r\n"
				+ "	/**\r\n"
				+ "	 * Creates a new settings object using the default properties file path. \r\n"
				+ "	 * \r\n"
				+ "	 *  <p>\r\n"
				+ "	 *  Default values are automatically loaded from the resource file\r\n"
				+ "	 *  {@link #DEFAULT_PROPERTIES_RESOURCE} within this constructor. User-defined values are loaded in \r\n"
				+ "	 *  {@link #load()} instead, which must be called, before any property values can be accessed. \r\n"
				+ "	 *  \r\n"
				+ "	 * @throws IOException if the resource file with the default values can't be loaded.\r\n"
				+ "	 * \r\n"
				+ "	 */\r\n"
				+ "	protected Settings() throws IOException {\r\n"
				+ "		super();\r\n"
				+ "	}\r\n"
				+ "	\r\n"
				+ "	/**\r\n"
				+ "	 * Creates a new settings object using the specified properties in the file path. \r\n"
				+ "	 * \r\n"
				+ "	 *  <p>\r\n"
				+ "	 *  Default values are automatically loaded from the resource file\r\n"
				+ "	 *  {@link #DEFAULT_PROPERTIES_RESOURCE} within this constructor. User-defined values are loaded in \r\n"
				+ "	 *  {@link #load()} instead, which must be called, before any property values can be accessed. \r\n"
				+ "	 *  \r\n"
				+ "	 * @throws IOException if the resource file with the default values can't be loaded.\r\n"
				+ "	 * \r\n"
				+ "	 */\r\n"
				+ "	public Settings(String userFilepath) throws IOException {\r\n"
				+ "		super(userFilepath);\r\n"
				+ "	}\r\n"
				+ "\r\n"
				+ "}\r\n"
				+ "";
		
		logger.info("Settings code generated successfully!");
		
		return text;
	}
}
