/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.basyx.codegen.generator.submodel.submodelelements;

import java.util.List;

import org.eclipse.aas.api.reference.Key;
import org.eclipse.aas.api.submodel.parts.ConceptDescription;
import org.eclipse.aas.api.submodel.submodelelement.Operation;
import org.eclipse.aas.api.submodel.submodelelement.SubModelElementCollection;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.File;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.MultiLanguageProperty;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.Property;
import org.eclipse.aas.basyx.codegen.generator.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SECGenerator {

	private static final Logger logger = LoggerFactory.getLogger(SECGenerator.class);
	private SubModelElementCollection secInstance;
	private List<Key> semanticKeys;
	private List<Property> properties;
	private List<File> files;
	private List<Operation> operations;
	private List<MultiLanguageProperty> mlps;
	private List<SubModelElementCollection> subSECs;
	private String parent;
	private ConceptDescription conceptDesc;
	
	/**
	 * Constructor for the SECGenerator(SubmodelElementCollection Generator) Class.
	 * It receives a single instance of the submodelelementCollection Instance to generate
	 * BaSyx Code for the same. 
	 * 
	 * @param secInstance
	 */
	public SECGenerator(SubModelElementCollection secInstance) {
		
		this.secInstance = secInstance;
		
		this.properties = secInstance.getProperties();
		this.files = secInstance.getFiles();
		this.operations = secInstance.getOperations();
		this.mlps = secInstance.getMultiLanguageProperties();
		this.subSECs = secInstance.getSubModelElementCollections();
		
		try {
			this.semanticKeys = secInstance.getSemanticIdentifier().getKeys();
		}
		
		catch (NullPointerException e) {
			logger.error("Null Pointer Exception in Semantic ID Declaration while initialising " + 
											this.getClass().getSimpleName());
		}
		
		logger.info("SECGenerator Initialised for the SubModelElementCollection : "
				 + secInstance.getIdShort());

		// Checking for Parents.
		if (secInstance.getParentSEC()!=null) {
			this.parent = secInstance.getParentSEC().getIdShort();
		}

		else if (secInstance.getParentSub()!=null) {
			this.parent = secInstance.getParentSub().getIdShort();
		}

		else {
			logger.error("SubModelElementCollection: " + secInstance.getIdShort() 
			+ "has no Parent defined");
		}
		
	}
	
	/**
	 * Generates a SubModelElementCollection either in a SubModel or inside 
	 * it's parent SubModelElementCollection. This function generates:
	 * 
	 * 	- Duplicate Info (If the SEC allows Duplicates).
	 * 	- Ordered Info (If the SEC is Ordered).
	 * 	- ModelingInfo (What the ModelingKind of the SEC is.)
	 * 	- Parental Information for this SEC. (Where the SEC belongs to. Either SubModel / Another SEC)
	 * 	- Semantic References for the SubModelElementCollection under consideration.
	 * 	- Required instances and method calls if the SubModelElementCollection is Dynamic. 
	 */
	public String generateSubModelElementCollection() {
		
		String secText = "";
		String propText = "";
		String fileText = "";
		String opText = "";
		String multiLangPropText = "";
		String subSECText = "";
		
		// Print the SEC Creation here. 
		secText = "		SubmodelElementCollection " + parent + "_" + secInstance.getIdShort() + " = new SubmodelElementCollection();\r\n" + 
				"		Collection<ISubmodelElement> " + parent + "_" + secInstance.getIdShort() + "value = new ArrayList<ISubmodelElement>();\r\n" +
				"		" + parent + "_" + secInstance.getIdShort() + ".setIdShort(\"" + secInstance.getIdShort() + "\");\r\n" + 
				generateDuplicateInfo() + 
				generateOrderedInfo() + 
				generateModelingInfo() +
				generateParentalRelation() +
				generateSemanticReference() +
				generateValueDelegates() + 
				"\r\n\r\n";
		
		/** 
		 * The content of the SubModelElementCollection is generated only if the
		 * SubModelElementCollection is not dynamic. If SubModelElementCollection
		 * is dynamic the contents are not generated. 
		 */
		
		// Checking if the SubModelElementCollection is dynamic. 
		if (this.secInstance.isDynamic()) {

			logger.info("SubmodelElementCollection Code generated for Dynamic SubmodelElementCollection: " + 
					secInstance.getIdShort());

			return secText;
		}
		
		else {
			
			if (this.properties!=null) {
				for (Property property : properties) {
					PropertyGenerator propGen = new PropertyGenerator(property);
					propText += propGen.generateProperty();
				}
			}
			
			else {
				propText += "";
			}
			
			if (this.files!=null) {
				for (File file : this.files) {
					FileGenerator fileGen = new FileGenerator(file);
					fileText += fileGen.generateFile();
				}
			}
			
			else {
					fileText += "";
			}
			
			if (this.operations!=null) {
				for (Operation operation : operations) {
					OperationGenerator opGen = new OperationGenerator(operation);
					opText += opGen.generateOperation();
				}
			}
			
			else {
				opText += "";
			}
			
			if (this.mlps!=null) {
				for (MultiLanguageProperty mlp : mlps) {
					MLPGenerator mlpGen = new MLPGenerator(mlp);
					multiLangPropText += mlpGen.generateMultiLanguageProperty();
				}
			}
			
			else {
				multiLangPropText += "";
			}
			
			if (this.subSECs!=null) {
				for (SubModelElementCollection subSEC : subSECs) {
					SECGenerator secGen = new SECGenerator(subSEC);
					subSECText += secGen.generateSubModelElementCollection();
				}
			}
			
			else {
				subSECText += "";
			}
			
			secText += subSECText
					+ opText
					+ propText
					+ fileText
					+ multiLangPropText
					+ generateSetValue();
			
			logger.info("SubmodelElementCollection Code generated for Non-Dynamic SubmodelElementCollection: " + 
					secInstance.getIdShort());

			
			return secText;
		}		
		


	}
	
	/**
	 * Generates the ModelingInfo for the respective SubModelElementCollection.
	 * 
	 * @return	Returns a String with the Modeling Information for the respective SubModelElementCollection. 
	 */
	private String generateModelingInfo() {
		
		String setKind = "     	" + parent + "_" + secInstance.getIdShort() 
		+ ".setKind(ModelingKind." 
		+ secInstance.getKind() + ");\r\n";

		return setKind;
	}
	
	/**
	 * Generates the Duplicates Information for the SubModelElementCollection, stating if the
	 * respective SubModelElementCollection allows duplicate.
	 * 
	 * @return Returns a String with the Duplicate Information. 
	 */
	private String generateDuplicateInfo() {
		
		String setAllowDuplicates = "		" + parent + "_" + secInstance.getIdShort() 
									+ ".setAllowDuplicates(" 
									+ secInstance.getAllowDuplicates() + ");\r\n";
		
		return setAllowDuplicates;
		
	}
	
	/**
	 * Generates the "Ordered" information for the respective SubModelElementCollection.
	 * 
	 * @return	Returns a String with the Ordered Information for the respective SubModelElementCollection. 
	 */
	private String generateOrderedInfo() {
		
		String setOrdered = "		" + parent + "_" + secInstance.getIdShort() + ".setOrdered(" 
				+ secInstance.getOrdered() + ");\r\n";

		return setOrdered;
		
	}
	
	/**
	 * Generates the code line that sets the value of the 
	 * <code>SubModelElementCollection</code> being generated. 
	 * 
	 * @return Returns a String that sets the value of the 
	 * <code>SubModelElementCollection</code> in the generated code. 
	 */
	private String generateSetValue() {
		
		String comment4SetValue = "		// Adding the SubmodelElements that belong to SubmodelElementCollection - " + 
									  secInstance.getIdShort() + ". \r\n";
			
		String parentQualifiedName = parent + "_" + secInstance.getIdShort();
		String valueQualifiedName = parentQualifiedName + "value";
		String setValue = "		" + parentQualifiedName + ".setElements("
				+ valueQualifiedName + ".stream().collect(Collectors.toMap(ISubmodelElement::getIdShort, Function.identity())));\r\n \r\n";
			
		return comment4SetValue + setValue;
	}
	
	/**
	 * Generates the parental relation information for the SubModelElementCollection. 
	 * It indicates where the respective SubModelElementCollection belongs to. 
	 * 
	 * @return Returns a String with the Parental Information. 
	 */
	private String generateParentalRelation() {
		
		// Thread Check: To Understand who has called the function.
		// Test out this function of stackTrace.
		StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
		boolean isSubModelSEC = true;
		
		if(stackTraceElements[3].getMethodName() == "generateSubModelElementCollection") {
			isSubModelSEC = false;
		}
		
		// Defines where the Property belongs to. 
		String addSEC = "";
		if (isSubModelSEC) {
			addSEC = "		addSubmodelElement(" + parent + "_" + secInstance.getIdShort() + ");\r\n";
		}
		else {
			SubModelElementCollection parentSEC = this.secInstance.getParentSEC();
			String immediateParent = "";
			
			// Checking for Parents.
			if (parentSEC.getParentSEC()!=null) {
				immediateParent = parentSEC.getParentSEC().getIdShort();
			}
			
			else if (parentSEC.getParentSub()!=null) {
				immediateParent = parentSEC.getParentSub().getIdShort();
			}
			
			else {
				logger.error("SubModelElementCollection: " + parentSEC.getIdShort() 
				+ "has no Parent defined");
			}
			
			addSEC = "		" + immediateParent + "_" + parentSEC.getIdShort() + "value.add(" 
					+ parent + "_" + secInstance.getIdShort() + ");\r\n\r\n";  
		}
		
		return addSEC;
		
	} 
	
	/**
	 * Generates the Semantic References for the respective SubModelElementCollection. 
	 * 
	 * @return	Returns the String with the Semantic References for the respective 
	 * 			SubModelElementCollection. 
	 */
	private String generateSemanticReference() {
		
		// Adding the Semantic Reference.
		String semanticStr = "";	
		if (this.semanticKeys!=null && this.conceptDesc==null) {
			
			semanticStr += "		List<IKey> " + parent + "_" + secInstance.getIdShort().toLowerCase() 
					+ "Keys= new ArrayList<IKey>();\r\n";
			
			for (Key key : this.semanticKeys) {
				
				String basyxKeyElement = FileUtils.removeUnderScore(key.getType().toString());  
						
				semanticStr += "		" + parent + "_" + secInstance.getIdShort().toLowerCase() 
								+ "Keys.add(" + "new Key(KeyElements." + basyxKeyElement
								+ ", " + key.isLocal() + ", " + "\"" + key.getValue() + "\""
								+ ", " + "KeyType." + key.getIdType() + ")); \r\n";
			}
			
			semanticStr += "		Reference " + parent + "_" + secInstance.getIdShort() 
							+ "Ref = new Reference(" + parent + "_" + secInstance.getIdShort().toLowerCase() 
							+ "Keys" + ");\r\n"
							+ "		" + parent + "_" + secInstance.getIdShort() 
							+ ".setSemanticId(" + parent + "_" + secInstance.getIdShort() + "Ref); \r\n \r\n";
			
			logger.info("Semantic Id reference for SubModelElementCollection: " + secInstance.getIdShort() + "generated.");
		}
		
		else if (this.conceptDesc!=null && this.semanticKeys==null) {
			
			semanticStr += "		" + parent + "_" + secInstance.getIdShort() 
						+  ".setSemanticId(conceptDescriptions." + 
							this.conceptDesc.getIdShort() + ".getReference()); \r\n \r\n";
			
		}
		
		else {
			
			logger.debug("No Semantic Id reference for SubModelElementCollection: " + secInstance.getIdShort() + "found. Thus, not generated.");
			
		}
		
		return semanticStr;
		
	}
	
	/**
	 * Generates the Value Delegates for the SubModelElementCollection under 
	 * consideration in case the SubModelElementCollection is declared as "Dynamic". 
	 * 
	 * @return	Returns a String with generated code containing the ValueDelegates
	 * 			installed on the respective SubModelElementCollection. 
	 */
	private String generateValueDelegates() {
		
		String valueDelegate = "";
		
		String getterCode = "";
		String setterCode = "";
		
		if (this.secInstance.isDynamic()) {
			
			valueDelegate += 
					"		ValueDelegate<" + "Collection<ISubmodelElement>"
					+ "> valDel" + parent + "_" + this.secInstance.getIdShort() 
					+ " = ValueDelegate.installOn(" + parent + "_" + this.secInstance.getIdShort() 
					+ "); \r\n";
			
			getterCode += 
					"		valDel" + parent + "_" + this.secInstance.getIdShort() 
					+ ".setGetHandler(dew::get_" + parent + "_" + this.secInstance.getIdShort() + "); \r\n";
			
			setterCode += "";
			
		}
		
		valueDelegate += getterCode + setterCode;
		
		return valueDelegate;
		
	}

}
