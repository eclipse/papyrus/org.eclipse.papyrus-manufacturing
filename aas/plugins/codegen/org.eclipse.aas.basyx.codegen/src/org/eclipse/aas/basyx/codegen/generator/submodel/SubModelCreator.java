/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.basyx.codegen.generator.submodel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.eclipse.aas.basyx.codegen.generator.FileUtils;
import org.eclipse.aas.basyx.codegen.generator.submodel.submodelelements.FileGenerator;
import org.eclipse.aas.basyx.codegen.generator.submodel.submodelelements.MLPGenerator;
import org.eclipse.aas.basyx.codegen.generator.submodel.submodelelements.OperationGenerator;
import org.eclipse.aas.basyx.codegen.generator.submodel.submodelelements.PropertyGenerator;
import org.eclipse.aas.basyx.codegen.generator.submodel.submodelelements.SECGenerator;
import org.eclipse.aas.api.reference.IKey;
import org.eclipse.aas.api.reference.Key;
import org.eclipse.aas.api.submodel.SubModel;
import org.eclipse.aas.api.submodel.parts.ConceptDescription;
import org.eclipse.aas.api.submodel.submodelelement.Operation;
import org.eclipse.aas.api.submodel.submodelelement.SubModelElementCollection;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.File;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.IOperationVariable;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.MultiLanguageProperty;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.Property;

import io.adminshell.aas.v3.model.LangString;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SubModelCreator {
	
	private static final Logger logger = LoggerFactory.getLogger(SubModelCreator.class);
	private int year = Calendar.getInstance().get(Calendar.YEAR);
	private SubModel subModel;
	private String nameSpace;
	private List<Property> properties;
	private List<File> files;
	private List<Operation> operations;
	private List<MultiLanguageProperty> multiLanguageProperties;
	private List<SubModelElementCollection> subModelElementCollections;
	private List<Key> semanticKeys;
	private ConceptDescription conceptDesc;
	
	/**
	 * Constructor of the SubModelCreator Class. It receives a single submodel
	 * instance and nameSpace to generate BaSyx Code for the same. 
	 * 
	 * @param subModel
	 * @param nameSpace
	 */
	public SubModelCreator(SubModel subModel, String nameSpace) {
		this.subModel = subModel;
		this.nameSpace = nameSpace;
		
		this.properties = this.subModel.getProperties();
		this.files = this.subModel.getFiles();
		this.operations = this.subModel.getOperations();
		this.multiLanguageProperties = this.subModel.getMultiLanguageProperties();
		this.subModelElementCollections = this.subModel.getSubModelElementCollections();

		try {
			this.semanticKeys = this.subModel.getSemanticIdentifier().getKeys();
		}
		
		catch (NullPointerException e) {
			logger.error("Null Pointer Exception in Semantic ID Declaration while initialising " + 
											this.getClass().getSimpleName());
		}
		
		try {
			this.conceptDesc = this.subModel.getSemanticDescription();
		}
		
		catch (NullPointerException e) {
			logger.error("Null Pointer Exception while fetching ConceptDescription for " +
					"the semantic Id definition of the SubModel: " +
									this.getClass().getSimpleName());
		}
		
		logger.info("SubModelCreator Initialised for SubModel : " + subModel.getIdShort()
		+ " in namespace: " + nameSpace);
	}
	
	/**
	 * Generates the SubModel File in the BaSyx generated code. 
	 * 	-	Generates the Identification Text.
	 * 	- 	Generates Field Variables required.
	 * 	-	Generates the Modeling Information & Semantic References. 
	 *  -	Generates all the Submodel elements that belongs to this 
	 *  	respective Submodel.  
	 * 
	 * @return	A String containing the BaSyx Code for a Submodel Creation. 
	 */
	public String createSubModelFile() {
				
		String subModelFileHeader = "";
		String subModelFileBody = "";
		
		String identificationStr = "";
		if(!subModel.getIdentification().getIdType().toString().isEmpty() && !subModel.getIdentification().getIdentifier().isEmpty()) {
			identificationStr = "		setIdentification(IdentifierType." + 
					subModel.getIdentification().getIdType().toString() + ", AASServer.getSettings().SUBMODEL_" + 
					subModel.getIdShort().toUpperCase() + "_" + subModel.getIdentification().getIdType().toString() + ".get().toString()); \r\n";
		}
		
		else {
			logger.error("No Submodel identification information found. Or Identification information incomplete.");
		}
		
		// Retrieving Operations from all SubModelElementCollections in the Submodel
		List<Operation> retrievedOps = new ArrayList<Operation>();
		for (SubModelElementCollection sec : this.subModelElementCollections) {
			retrievedOps = retrieveOperations(sec);
		}
		
		// Generating the Operation Field Variables.
		String operationFieldVariables = generateOperationFields(this.operations);
			   operationFieldVariables += generateOperationFields(retrievedOps);

		
		
		String idShortStr = "		setIdShort(\"" + subModel.getIdShort() + "\");\r\n";
		
		
		subModelFileHeader = "/*******************************************************************************\n"
				+ " * Copyright (c) " + year +" DFKI.\n"
				+ " *\n"
				+ " * This program and the accompanying materials\n"
				+ " * are made available under the terms of the Eclipse Public License 2.0\n"
				+ " * which accompanies this distribution, and is available at\n"
				+ " * https://www.eclipse.org/legal/epl-2.0/\n"
				+ " *\n"
				+ " * SPDX-License-Identifier: EPL-2.0\n"
				+ " *\n"
				+ " * Contributors:\n"
				+ " *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>\n"
				+ " *******************************************************************************/ \r\n" + 
				"package " + this.nameSpace + ".module.submodels." + subModel.getIdShort().toLowerCase() + ";\r\n" + 
				"\r\nimport java.util.function.Function;\r\n" +
				"import java.util.stream.Collectors;\r\n" +
				"import java.util.ArrayList;\r\n" + 
				"import java.util.Collection;\r\n" + 
				"import java.util.HashMap;\r\n" + 
				"import java.util.LinkedHashMap;\r\n" +
				"import java.util.Map;\r\n" + 
				"import java.util.List;\r\n" + 
				"\r\n" +
				"import java.math.BigInteger; \r\n" +
				"\r\n" +
				"import javax.xml.datatype.XMLGregorianCalendar;\r\n" +
				"import javax.xml.datatype.Duration;\r\n" +
				"import javax.xml.namespace.QName;\r\n" +
				"\r\n" +
				"import " + this.nameSpace + ".connection.ConnectedDevices;\r\n" + 
				"//import " + this.nameSpace + ".connection.DataCrawler;\r\n" + 
				"import " + this.nameSpace + ".module.AASServer;\r\n" +
				"import " + this.nameSpace + ".module.ConceptDescriptions;\r\n" +
				"import " + this.nameSpace + ".connection.OpcUaVariable;\r\n" +
				"import org.eclipse.basyx.submodel.metamodel.map.submodelelement.operation.Operation;\r\n" + 
				"import org.eclipse.basyx.submodel.metamodel.map.submodelelement.SubmodelElementCollection;\r\n" +
				"import org.eclipse.basyx.submodel.metamodel.map.submodelelement.dataelement.property.valuetype.ValueType;\r\n" +
				"import org.eclipse.basyx.submodel.metamodel.map.submodelelement.dataelement.File;\r\n" +
				"import org.eclipse.basyx.submodel.metamodel.map.submodelelement.dataelement.property.Property;\r\n" + 	
				"import org.eclipse.basyx.submodel.metamodel.map.submodelelement.dataelement.MultiLanguageProperty;\r\n" +
				"import org.eclipse.basyx.submodel.metamodel.map.qualifier.LangStrings;\r\n" + 
				"import org.eclipse.basyx.submodel.metamodel.map.reference.Key; \r\n" +
				"import org.eclipse.basyx.submodel.metamodel.map.reference.Reference; \r\n" +
				"import org.eclipse.basyx.submodel.metamodel.api.reference.IKey;\r\n" + 
				"import org.eclipse.basyx.submodel.metamodel.api.reference.enums.KeyElements; \r\n" +
				"import org.eclipse.basyx.submodel.metamodel.api.reference.enums.KeyType; \r\n" +
				"import org.eclipse.basyx.submodel.metamodel.api.identifier.IdentifierType;\r\n" + 
				"import org.eclipse.basyx.submodel.metamodel.api.qualifier.haskind.ModelingKind;\r\n" +
				"import org.eclipse.basyx.submodel.metamodel.api.submodelelement.ISubmodelElement;\r\n" +
				"import org.eclipse.basyx.submodel.metamodel.map.Submodel;\r\n" +
				"import org.eclipse.basyx.submodel.metamodel.map.submodelelement.operation.OperationVariable;\r\n" +
				"import com.festo.aas.p4m.connection.ValueDelegate; \r\n"+ 
				"import org.eclipse.basyx.vab.protocol.opcua.types.NodeId;\r\n" +
				"import org.eclipse.basyx.vab.protocol.opcua.types.UnsignedShort;\r\n" + 
				"import com.festo.aas.p4m.connection.ConnectedProperty;\r\n" + 
				"\r\n" + 
				"/**\r\n" + 
				" * \r\n" + 
				" * @author DFKI\r\n" + 
				" * \r\n" + 
				" * Do not edit this file for changing operation behaviours. \r\n" + 
				" */\r\n" + 
				"\r\n" + 
				"public class " + this.subModel.getIdShort() + " extends Submodel {\r\n" + 
				"\r\n" +
				"	/**\r\n" +
				"	 * This field variable holds all configured connectors to the asset. \r\n" +
				"	 * Use these connectors to communicate with your asset.\r\n" +
				"	 */" + 
				"\r\n" +
				"	private ConnectedDevices connectedDevices;\r\n" + 
				"\r\n" +
				"	private ConceptDescriptions conceptDescriptions;\r\n" + 
				"\r\n" +
				"	/**\r\n" + 
				"     * This class contains all user-provided code for operations.\r\n" + 
				"     */\r\n" +
				"	private final " + "DynamicElementsWorkspace " + "dew; \r\n" +
				"\r\n" +
				"	 /**\r\n" + 
				"     * This class contains all user-provided code for value delegates.\r\n" + 
				"     */\r\n" +
				"\r\n" +
				"	 /**\r\n" + 
				"     * The following contains the declaration of the operation variables as field variables so that they can be accessed in their true sense.\r\n" + 
				"     */\r\n" +
				operationFieldVariables + 
				"\r\n" +
				"\r\n" +
				"	public " + this.subModel.getIdShort() + "() {\r\n" + 
				"\r\n" + 
				"		try {\r\n" + 
				"			connectedDevices = ConnectedDevices.getInstance();\r\n" +
				"		} \r\n" +
				"		catch (Exception e) {\r\n" +
				"\r\n" +
				"			e.printStackTrace();\r\n" +
				"		}\r\n" +
				"		\r\n" +
				"		" + "dew = new " + "DynamicElementsWorkspace(connectedDevices);\r\n" +
				"		" + "conceptDescriptions = new " + "ConceptDescriptions();\r\n" +
				"\r\n";
		
		subModelFileBody = 
				idShortStr + 
				identificationStr
				+ generateModelingInfo() 
				+ generateSemanticReference();
		
		for (Operation operation : this.operations) {
			OperationGenerator opGen = new OperationGenerator(operation);
			subModelFileBody += opGen.generateOperation();
		}
		
		for (Property property : this.properties) {
			PropertyGenerator propGen = new PropertyGenerator(property);
			subModelFileBody += propGen.generateProperty();
		}
		
		for (File file : this.files) {
			FileGenerator fileGen = new FileGenerator(file);
			subModelFileBody += fileGen.generateFile();
		}
		
		for (MultiLanguageProperty multiLangProperty : this.multiLanguageProperties) {
			MLPGenerator mlpGen = new MLPGenerator(multiLangProperty);
			subModelFileBody += mlpGen.generateMultiLanguageProperty();
		}
		
		for (SubModelElementCollection subModelElementCollection : this.subModelElementCollections) {
			SECGenerator secGen = new SECGenerator(subModelElementCollection);
			subModelFileBody += secGen.generateSubModelElementCollection();
		}
		
		subModelFileBody += "	}\r\n" + 
				"	\r\n" + 
				"}\r\n" + 
				"";
		
		String subModelText = subModelFileHeader + subModelFileBody;
		
		logger.info("SubModel Code for SubModel: " + subModel.getIdShort() + " generated.");
		
		return subModelText;
		
	}
	
	/**
	 * Generates the ModelingInfo for the respective Submodel. 
	 * 
	 * @return 	Returns a String with the Modeling Information for the 
	 * 			respective Submodel.
	 */
	private String generateModelingInfo() {
		
		String setKind = "";
		
		if (!subModel.getKind().toString().isEmpty()) {
			setKind = "		setKind(ModelingKind." + subModel.getKind().toString() + "); \r\n";
		}
		
		else {
			logger.error("No Submodel ModelingKind information found.");
		}
		
		return setKind;
		
	}
	
	/**
	 * Generates the Semantic References for the respective Submodel. 
	 * 
	 * @return 	Returns a String with the Semantic references for the respective
	 * 			Submodel. 
	 */
	private String generateSemanticReference() {
		
		String semanticStr = "";	
		if (this.semanticKeys!=null && this.conceptDesc==null) {
			
			semanticStr += "		List<IKey> " + subModel.getIdShort().toLowerCase() 
					+ "Keys= new ArrayList<IKey>();\r\n";
			
			for (Key key : this.semanticKeys) {
				
				if(key.getType()!=null) {
					
					String basyxKeyElement = FileUtils.removeUnderScore(key.getType().toString());  
						
					semanticStr += "		" + subModel.getIdShort().toLowerCase() 
							+ "Keys.add(" + "new Key(KeyElements." + basyxKeyElement
							+ ", " + key.isLocal() + ", " + "\"" + key.getValue() + "\""
							+ ", " + "KeyType." + key.getIdType() + ")); \r\n";
				}
				
				/**
				 *  If the Key Value Type within a <code>Key</code>, i.e., <code>KeyElements</code> is not defined.
				 *  We consider a fallback option for <code>KeyElements</code> as "GLOBALREFERENCE".
				 *  This is done because BaSyx makes it compulsory for the creation of 
				 *  an object of class "Key", a parameter that chooses from an enum
				 *  <code>KeyElements</code> its key value type. 
				 */
				
				// Therefore, writing the fallback case in the <code>else</code> block.
				else {
					
					semanticStr += "		" + subModel.getIdShort().toLowerCase() 
							+ "Keys.add(" + "new Key(KeyElements." + "GLOBALREFERENCE"
							+ ", " + key.isLocal() + ", " + "\"" + key.getValue() + "\""
							+ ", " + "KeyType." + key.getIdType() + ")); \r\n";					
					
				}
				
			}
			
			semanticStr += "		Reference " + subModel.getIdShort() 
							+ "Ref = new Reference(" + subModel.getIdShort().toLowerCase() 
							+ "Keys" + ");\r\n"
							+ "		setSemanticId(" + subModel.getIdShort() + "Ref); \r\n \r\n";
			
			logger.info("Semantic Id reference for Submodel: " + subModel.getIdShort() + "generated.");
		}
		
		else if (this.conceptDesc!=null && this.semanticKeys==null) {
			
			semanticStr += "		setSemanticId(conceptDescriptions." + 
							this.conceptDesc.getIdShort() + ".getReference()); \r\n \r\n";
			
		}
		
		else {
			logger.debug("No Semantic Id reference for Submodel: " + subModel.getIdShort() + "found. Thus, not generated.");
		}
		
		return semanticStr;
		
	}
	
	/**
	 * Generates the field variables for all the Operations (also for the ones)
	 * inside a SubmodelElementCollection. 
	 * 
	 * @param operations List of Operations for which the field variables are to 
	 * 					 be generated. 
	 * @return			 
	 */
	private String generateOperationFields(List<Operation> operations) {
		
		String operationFieldVariables = "";
		
		// Operations and SubModelElementCollections individual to SubModelElementCollection. 
		
		
		for (Operation operation : operations) {
			OperationGenerator opGen = new OperationGenerator(operation);
			operationFieldVariables += opGen.generateOperationFieldVariables();
		}
		
		return operationFieldVariables;
	}
	
	/**
	 * Retrieves all the Operations from the SubmodelElementCollection passed in
	 * that is defined in a submodel.
	 * 
	 * @param sec SubModelElementCollection from which all underlying Operations
	 * 				are to be retrieved. 
	 * 
	 * @return Returns a List of Operations.
	 */
	private List<Operation> retrieveOperations(SubModelElementCollection sec) {
		
		List<Operation> retrievedOps = new ArrayList<Operation>();
				
		if (sec.getOperations()!=null) {
			for (Operation operation : sec.getOperations()) {
				retrievedOps.add(operation);
			}
		}
		
		if (sec.getSubModelElementCollections()!=null) {
			for (SubModelElementCollection collection : sec.getSubModelElementCollections()) {
				List<Operation> ops = retrieveOperations(collection);
				
				for (Operation op : ops) {
					retrievedOps.add(op);
				}
			}
		}
		
		return retrievedOps;
		
	}

}

