/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.basyx.codegen;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.aas.basyx.codegen.generator.Project;
import org.eclipse.aas.api.aas.AssetAdministrationShell;
import org.eclipse.aas.api.aas.parts.ConceptDictionary;
import org.eclipse.aas.api.asset.Asset;
import org.eclipse.aas.api.communications.AASEndpoint;
import org.eclipse.aas.api.communications.Endpoint;
import org.eclipse.aas.api.communications.ProtocolKind;
import org.eclipse.aas.api.reference.Key;
import org.eclipse.aas.api.reference.Reference;
import org.eclipse.aas.api.submodel.SubModel;
import org.eclipse.aas.api.submodel.parts.Category;
import org.eclipse.aas.api.submodel.parts.ConceptDescription;
import org.eclipse.aas.api.submodel.submodelelement.Operation;
import org.eclipse.aas.api.submodel.submodelelement.SubModelElementCollection;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.File;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.MultiLanguageProperty;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.OperationVariable;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.Property;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.valuetypes.ValueType;

import io.adminshell.aas.v3.model.AssetKind;
import io.adminshell.aas.v3.model.Identifier;
import io.adminshell.aas.v3.model.IdentifierType;
import io.adminshell.aas.v3.model.KeyElements;
import io.adminshell.aas.v3.model.KeyType;
import io.adminshell.aas.v3.model.LangString;
import io.adminshell.aas.v3.model.ModelingKind;
//import io.adminshell.aas.v3.model.MultiLanguageProperty;
import io.adminshell.aas.v3.model.Submodel;
import io.adminshell.aas.v3.model.SubmodelElement;
import io.adminshell.aas.v3.model.impl.DefaultIdentifier;
import io.adminshell.aas.v3.model.impl.DefaultMultiLanguageProperty;
import io.adminshell.aas.v3.model.impl.DefaultSubmodel;


/**
 * 
 *
 */
public class Test {	

    public static void main(String[] args) {
        
    	
    	test7();
        
        
    }
    
    public static void test1() {
    	
        Endpoint assetEndpoint1 = new Endpoint("CalculatorServer", 
				ProtocolKind.OPCUA, "opc.tcp://127.0.0.1:4840/");

        Endpoint assetEndpoint2 = new Endpoint("MinimalServer", 
				ProtocolKind.OPCUA, "opc.tcp://127.0.0.1:4841/");

    	
    	// Declaring the 3 Submodels.
    	SubModel documentation = new SubModel("Documentation");
    	SubModel controlSubModel = new SubModel("Control");
    	SubModel calculationSubModel = new SubModel("Calculation");
    	
    	// Setting the Identifier Details of all these SubModels. 
    	// "Documentation"
    	Identifier idDoc = new DefaultIdentifier();
        idDoc.setIdType(IdentifierType.IRI);
        idDoc.setIdentifier("https://www.dfki.de/ids/sm/8c47c091-195e-43c5-bfb2-7248b6a31bf1");
        documentation.setIdentification(idDoc);
         
        // "Control"
        Identifier idControl = new DefaultIdentifier();
        idControl.setIdType(IdentifierType.IRI);
        idControl.setIdentifier("https://www.dfki.de/ids/sm/9a1bdba6-eb24-418a-a56e-f9d0ae621941");
        controlSubModel.setIdentification(idControl);
         
        // "Calculation"
        Identifier idCalculation = new DefaultIdentifier();
        idCalculation.setIdType(IdentifierType.IRI);
        idCalculation.setIdentifier("https://www.dfki.de/ids/sm/79c44263-30ad-4296-a51e-2df3a4aed4ae");
        calculationSubModel.setIdentification(idCalculation);
        
        // Setting ModelingKind of the SubModels.
        documentation.setKind(ModelingKind.INSTANCE);
        controlSubModel.setKind(ModelingKind.INSTANCE);
        calculationSubModel.setKind(ModelingKind.INSTANCE);
        
        // Setting Semantic Ids for SubModels. 
        Key docKey = new Key(KeyElements.CONCEPT_DESCRIPTION, true, 
        				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
        Reference docRef = new Reference(docKey);
        documentation.setSemanticIdentifier(docRef);
        
        Key controlKey = new Key(KeyElements.CONCEPT_DESCRIPTION, true, 
				"0112/2///61360_4#AAF891#002", KeyType.IRDI);
        Reference controlRef = new Reference(controlKey);
        controlSubModel.setSemanticIdentifier(controlRef);
        
        Key calculationKey = new Key(KeyElements.CONCEPT_DESCRIPTION, true, 
				"0112/2///61360_4#AAF891#002", KeyType.IRDI);
        Reference calculationRef = new Reference(calculationKey);
        calculationSubModel.setSemanticIdentifier(calculationRef);

        // Adding a File to the Submodel - "Documentation"
        File exampleFile = new File();
        exampleFile.setIdShort("exampleFile");
        exampleFile.setKind(ModelingKind.INSTANCE);
        exampleFile.setMimeType("audio/aac");
        exampleFile.setValue("example.com/myexample");
        documentation.setFile(exampleFile);
        
        // Populating the Submodel - "Documentation"
        MultiLanguageProperty manufacName = new MultiLanguageProperty("ManufacturerName");
        manufacName.setKind(ModelingKind.INSTANCE);
        manufacName.setValue("en", "Smartfactory KL");
        documentation.setMultiLanguageProperty(manufacName);
        
        MultiLanguageProperty prodName = new MultiLanguageProperty("ManufacturerProductDesignation");
        prodName.setKind(ModelingKind.INSTANCE);
        prodName.setValue("de", "Rechner");
        prodName.setValue("en", "Calculator");
        prodName.setValue("fr", "Calculatrice");
        documentation.setMultiLanguageProperty(prodName);
        // ---------------------------------------------------------------------
        
        // Populating the Submodel - "Control".
        SubModelElementCollection controlSEC = new SubModelElementCollection("ControlParameters");
        controlSEC.setAllowDuplicates(false);
        controlSEC.setOrdered(true);
        controlSEC.setKind(ModelingKind.INSTANCE);
        
        // Creating a demo SubModelElementCollection - "Example" & adding it to the SEC - "ControlParameters".
        SubModelElementCollection exampleSEC = new SubModelElementCollection("Example");
        exampleSEC.setAllowDuplicates(false);
        exampleSEC.setOrdered(true);
        exampleSEC.setKind(ModelingKind.INSTANCE);
        controlSEC.setSubModelElementCollection(exampleSEC);
        
//        // Adding a File to the SEC - "Example"
//        File anotherFile = new File();
//        anotherFile.setIdShort("anotherFile");
//        anotherFile.setKind(ModelingKind.INSTANCE);
//        anotherFile.setMimeType("audio/aac");
//        anotherFile.setValue("another.com/myanotherexample");
//        exampleSEC.setFile(anotherFile);
        
        Property opVolt = new Property("OperatingVoltage");
        opVolt.setValueType(ValueType.Integer);
        opVolt.setDynamic(true);
        opVolt.setValue(5);
        controlSEC.setProperty(opVolt);
        
        Property unit = new Property("Unit");
        unit.setValueType(ValueType.String);
        unit.setKind(ModelingKind.INSTANCE);
        controlSEC.setProperty(unit);
        
        Property opTemp = new Property("OperatingTemperature");
        opTemp.setValueType(ValueType.Integer);
        opTemp.setDynamic(true);
        opTemp.setEndpoint(assetEndpoint1);
        opTemp.setNameSpaceIndex(2);
        opTemp.setIdentifier(5);
        controlSubModel.setProperty(opTemp);
        
        // Adding the SubModelelementCollection to the Submodel.
        controlSubModel.setSubModelElementCollection(controlSEC);
        
        // Declaring in Submodel "Control" - an Operation "ChangeManufacturerName".
        OperationVariable manuNameInp = new OperationVariable("ManufacturerName",
        								ValueType.String);
        Operation changeManuName = new Operation("changeManufacturerName");
        changeManuName.setInputVariable(manuNameInp);
        controlSubModel.setOperation(changeManuName);
        
        // Declaring in Submodel "Control" - an Operation "Status".
        OperationVariable statusOut = new OperationVariable("Status", 
        								ValueType.Boolean);
        Operation statusCheck = new Operation("checkStatus");
        statusCheck.setOutputVariable(statusOut);
        controlSubModel.setOperation(statusCheck);
        // ---------------------------------------------------------------------
        
        // Populating the Submodel - "Calculation".
        OperationVariable operand1 = new OperationVariable("Operand1", ValueType.Integer);
        OperationVariable operand2 = new OperationVariable("Operand2", ValueType.Integer);
        OperationVariable result = new OperationVariable("Result", ValueType.Integer);
        
        Operation errorReset = new Operation("errorReset");
        OperationVariable errorToReset = new OperationVariable("ErrorToReset", ValueType.Int64);
        OperationVariable done = new OperationVariable("Done", ValueType.Boolean);
        errorReset.setInputVariable(errorToReset);
        errorReset.setOutputVariable(done);
        calculationSubModel.setOperation(errorReset);
        
        
        // Adding Operations. 
        Operation add = new Operation("Add");
        add.setKind(ModelingKind.INSTANCE);
        add.setInputVariables(operand1, operand2);
        add.setOutputVariable(result);
        calculationSubModel.setOperation(add);
        
        Operation sub = new Operation("Subtract");
        sub.setKind(ModelingKind.INSTANCE);
        sub.setInputVariables(operand1, operand2);
        sub.setOutputVariable(result);
        calculationSubModel.setOperation(sub);
        
        Operation multiply = new Operation("Multiply");
        multiply.setKind(ModelingKind.INSTANCE);
        multiply.setInputVariables(operand1, operand2);
        multiply.setOutputVariable(result);
        calculationSubModel.setOperation(multiply);
        
        Operation divide = new Operation("Divide");
        divide.setKind(ModelingKind.INSTANCE);
        divide.setInputVariables(operand1, operand2);
        divide.setOutputVariable(result);
        calculationSubModel.setOperation(divide);
        
        SubModelElementCollection calSEC = new SubModelElementCollection("CalculationCollection");
        calSEC.setAllowDuplicates(false);
        calSEC.setOrdered(true);
        calSEC.setDynamic(true);
        calSEC.setKind(ModelingKind.INSTANCE);
        calculationSubModel.setSubModelElementCollection(calSEC);
        
        SubModelElementCollection trialSEC = new SubModelElementCollection("DETrialCollection");
        trialSEC.setAllowDuplicates(false);
        trialSEC.setOrdered(true);
        trialSEC.setDynamic(true);
        trialSEC.setKind(ModelingKind.INSTANCE);
        calSEC.setSubModelElementCollection(trialSEC);
        
        SubModelElementCollection trialSEC1 = new SubModelElementCollection("DETrial1Collection");
        trialSEC1.setAllowDuplicates(false);
        trialSEC1.setOrdered(true);
        trialSEC1.setDynamic(true);
        trialSEC1.setKind(ModelingKind.INSTANCE);
        trialSEC.setSubModelElementCollection(trialSEC1);
        
        
        Asset calculatorAsset = new Asset("Calculator", AssetKind.INSTANCE);
        calculatorAsset.setEndpoints(assetEndpoint1, assetEndpoint2);
        
        Identifier idAsset = new DefaultIdentifier();
        idAsset.setIdType(IdentifierType.IRI);
        idAsset.setIdentifier("https://www.dfki.de/ids/assets/15c9c860-34f3-4fc3-a292-66fd0442c1f9");
        calculatorAsset.setIdentification(idAsset);
        
        
        AASEndpoint aasEndpoint = new AASEndpoint("localhost", 2000);
        
        // Creating AssetAdministrationShell.
        AssetAdministrationShell Papyrus4Manufacturing = new AssetAdministrationShell("Papyrus4Manufacturing",
        									aasEndpoint);
        Papyrus4Manufacturing.setSubModels(documentation, controlSubModel, calculationSubModel);
        
        Identifier idAAS = new DefaultIdentifier();
        idAAS.setIdType(IdentifierType.IRI);
        idAAS.setIdentifier("https://www.dfki.de/Papyrus4Manufacturingworkshop/ids/aas/"
				+ "23012dfa-0c1c-4408-bd06-dc3d9dcfcf4f");
        Papyrus4Manufacturing.setIdentification(idAAS);
        
        // Setting the AAS for the Asset.
        Papyrus4Manufacturing.setAsset(calculatorAsset);
        
        // Initialising the Generator.
        Project testAAS = new Project(Papyrus4Manufacturing);
        testAAS.setProjectName("firstTrial");
        testAAS.setNamespaceFromProjectName();
        testAAS.setProjectFolder("/home/tapanta/Documents/tuleap-eclipse-workspace/Second_Trial/");

        testAAS.createProject();
    	
    }
    
    public static void test2() {
    	
        Endpoint assetEndpoint1 = new Endpoint("CalculatorServer", 
				ProtocolKind.OPCUA, "opc.tcp://127.0.0.1:4840/");

        Endpoint assetEndpoint2 = new Endpoint("MinimalServer", 
				ProtocolKind.OPCUA, "opc.tcp://127.0.0.1:4841/");
     	
     	// Declaring the 3 Submodels.
     	SubModel documentation = new SubModel("Documentation");
     	SubModel controlSubModel = new SubModel("Control");
     	SubModel calculationSubModel = new SubModel("Calculation");
     	
     	// Setting the Identifier Details of all these SubModels. 
     	// "Documentation"
     	Identifier idDoc = new DefaultIdentifier();
         idDoc.setIdType(IdentifierType.IRI);
         idDoc.setIdentifier("https://www.dfki.de/ids/sm/8c47c091-195e-43c5-bfb2-7248b6a31bf1");
         documentation.setIdentification(idDoc);
          
         // "Control"
         Identifier idControl = new DefaultIdentifier();
         idControl.setIdType(IdentifierType.IRI);
         idControl.setIdentifier("https://www.dfki.de/ids/sm/9a1bdba6-eb24-418a-a56e-f9d0ae621941");
         controlSubModel.setIdentification(idControl);
          
         // "Calculation"
         Identifier idCalculation = new DefaultIdentifier();
         idCalculation.setIdType(IdentifierType.IRI);
         idCalculation.setIdentifier("https://www.dfki.de/ids/sm/79c44263-30ad-4296-a51e-2df3a4aed4ae");
         calculationSubModel.setIdentification(idCalculation);
         
         // Setting ModelingKind of the SubModels.
         documentation.setKind(ModelingKind.INSTANCE);
         controlSubModel.setKind(ModelingKind.INSTANCE);
         calculationSubModel.setKind(ModelingKind.INSTANCE);
         
         // Setting Semantic Ids for SubModels. 
         Key docKey = new Key(KeyElements.CONCEPT_DESCRIPTION, true, 
         				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
         Reference docRef = new Reference(docKey);
         documentation.setSemanticIdentifier(docRef);
         
         Key controlKey = new Key(KeyElements.CONCEPT_DESCRIPTION, true, 
 				"0112/2///61360_4#AAF891#002", KeyType.IRDI);
         Reference controlRef = new Reference(controlKey);
         controlSubModel.setSemanticIdentifier(controlRef);
         
         Key calculationKey = new Key(KeyElements.CONCEPT_DESCRIPTION, true, 
 				"0112/2///61360_4#AAF891#002", KeyType.IRDI);
         Reference calculationRef = new Reference(calculationKey);
         calculationSubModel.setSemanticIdentifier(calculationRef);

         // Adding a File to the Submodel - "Documentation"
         File exampleFile = new File();
         exampleFile.setIdShort("exampleFile");
         exampleFile.setKind(ModelingKind.INSTANCE);
         exampleFile.setMimeType("audio/aac");
         exampleFile.setValue("example.com/myexample");
         documentation.setFile(exampleFile);
         
         // Populating the Submodel - "Documentation"
         MultiLanguageProperty manufacName = new MultiLanguageProperty("ManufacturerName");
         manufacName.setKind(ModelingKind.INSTANCE);
         manufacName.setValue("en", "Smartfactory KL");
         
         Key manufacKey = new Key(KeyElements.GLOBAL_REFERENCE, false, "0112/2///61360_4#MYNAME891#005", KeyType.IRDI);
         Reference manufNameRef = new Reference(manufacKey);
         manufacName.setSemanticIdentifier(manufNameRef);
         
         documentation.setMultiLanguageProperty(manufacName);
         
         MultiLanguageProperty prodName = new MultiLanguageProperty("ManufacturerProductDesignation");
         prodName.setKind(ModelingKind.INSTANCE);
         prodName.setValue("de", "Rechner");
         prodName.setValue("en", "Calculator");
         prodName.setValue("fr", "Calculatrice");
         documentation.setMultiLanguageProperty(prodName);
         
         
         SubModelElementCollection addressSEC = new SubModelElementCollection("Address");
         documentation.setSubModelElementCollection(addressSEC);
         
         //Adding to the parent SEC "Address" - "Phone". 
         
         SubModelElementCollection phoneSEC = new SubModelElementCollection("Phone");
         addressSEC.setSubModelElementCollection(phoneSEC);
         
         MultiLanguageProperty telephoneNumer = new MultiLanguageProperty("TelephoneNumber");
         phoneSEC.setMultiLanguageProperty(telephoneNumer);
         
         Property typeTel = new Property("TypeOfTelephone");
         phoneSEC.setProperty(typeTel);
         
         
         
         
         
         // ---------------------------------------------------------------------
         
         // Populating the Submodel - "Control".
         SubModelElementCollection controlSEC = new SubModelElementCollection("ControlParameters");
         controlSEC.setAllowDuplicates(false);
         controlSEC.setOrdered(true);
         controlSEC.setKind(ModelingKind.INSTANCE);
         
         // Creating a demo SubModelElementCollection - "Example" & adding it to the SEC - "ControlParameters".
         SubModelElementCollection exampleSEC = new SubModelElementCollection("Example");
         exampleSEC.setAllowDuplicates(false);
         exampleSEC.setOrdered(true);
         exampleSEC.setKind(ModelingKind.INSTANCE);
         controlSEC.setSubModelElementCollection(exampleSEC);
         System.out.println("Parent of Example: " + exampleSEC.getParentSEC().getIdShort());
         
         // Adding a File to the SEC - "Example"
         File anotherFile = new File();
         anotherFile.setIdShort("anotherFile");
         anotherFile.setKind(ModelingKind.INSTANCE);
         anotherFile.setMimeType("audio/aac");
         anotherFile.setValue("another.com/myanotherexample");
         anotherFile.setDynamic(true);
         exampleSEC.setFile(anotherFile);
         
         Property opVolt = new Property("OperatingVoltage");
         opVolt.setValueType(ValueType.Integer);
         opVolt.setDynamic(true);
         opVolt.setValue(5);
         controlSEC.setProperty(opVolt);
         
         Property unit = new Property("Unit");
         unit.setValueType(ValueType.String);
         unit.setKind(ModelingKind.INSTANCE);
         unit.setValue("Volts");
         controlSEC.setProperty(unit);
         
         Property opTemp = new Property("OperatingTemperature");
         opTemp.setValueType(ValueType.Integer);
         opTemp.setDynamic(true);
         opTemp.setEndpoint(assetEndpoint1);
         opTemp.setNameSpaceIndex(2);
         opTemp.setIdentifier(5);
         controlSubModel.setProperty(opTemp);
         
         // Adding the SubModelelementCollection to the Submodel.
         controlSubModel.setSubModelElementCollection(controlSEC);
         
         // Declaring in Submodel "Control" - an Operation "ChangeManufacturerName".
         OperationVariable manuNameInp = new OperationVariable("ManufacturerName",
         								ValueType.String);
         Operation changeManuName = new Operation("changeManufacturerName");
         changeManuName.setInputVariable(manuNameInp);
         controlSubModel.setOperation(changeManuName);
         
         // Declaring in Submodel "Control" - an Operation "Status".
         OperationVariable statusOut = new OperationVariable("Status", 
         								ValueType.Boolean);
         Operation statusCheck = new Operation("checkStatus");
         statusCheck.setOutputVariable(statusOut);
         controlSubModel.setOperation(statusCheck);
         // ---------------------------------------------------------------------
         
         // Populating the Submodel - "Calculation".
         OperationVariable operand1 = new OperationVariable("Operand1", ValueType.Integer);
         OperationVariable operand2 = new OperationVariable("Operand2", ValueType.Integer);
         OperationVariable result = new OperationVariable("Result", ValueType.Integer);
         
         // Adding Operations. 
         Operation add = new Operation("Add");
         add.setKind(ModelingKind.INSTANCE);
         add.setInputVariables(operand1, operand2);
         add.setOutputVariable(result);
         calculationSubModel.setOperation(add);
         
         Operation sub = new Operation("Subtract");
         sub.setKind(ModelingKind.INSTANCE);
         sub.setInputVariables(operand1, operand2);
         sub.setOutputVariable(result);
         calculationSubModel.setOperation(sub);
         
         Operation multiply = new Operation("Multiply");
         multiply.setKind(ModelingKind.INSTANCE);
         multiply.setInputVariables(operand1, operand2);
         multiply.setOutputVariable(result);
         calculationSubModel.setOperation(multiply);
         
         Operation divide = new Operation("Divide");
         divide.setKind(ModelingKind.INSTANCE);
         divide.setInputVariables(operand1, operand2);
         divide.setOutputVariable(result);
         calculationSubModel.setOperation(divide);
         
         SubModelElementCollection calSEC = new SubModelElementCollection("CalculationCollection");
         calSEC.setAllowDuplicates(false);
         calSEC.setOrdered(true);
         calSEC.setDynamic(true);
         calSEC.setKind(ModelingKind.INSTANCE);
         calculationSubModel.setSubModelElementCollection(calSEC);
//         System.out.println("Parent of Example: " + calSEC.getParentSEC().getIdShort());
         
         SubModelElementCollection trialSEC = new SubModelElementCollection("DETrialCollection");
         trialSEC.setAllowDuplicates(false);
         trialSEC.setOrdered(true);
         trialSEC.setDynamic(true);
         trialSEC.setKind(ModelingKind.INSTANCE);
         calSEC.setSubModelElementCollection(trialSEC);
         
         SubModelElementCollection trialSEC1 = new SubModelElementCollection("DETrial1Collection");
         trialSEC1.setAllowDuplicates(false);
         trialSEC1.setOrdered(true);
         trialSEC1.setDynamic(true);
         trialSEC1.setKind(ModelingKind.INSTANCE);
         trialSEC.setSubModelElementCollection(trialSEC1);
         
         
         // Creating Asset Endpoints.
//         Endpoint assetEndpoint1 = new Endpoint("CalculatorServer", 
//         						ProtocolKind.OPCUA, "127.0.0.1", 4840);
//         
//         Endpoint assetEndpoint2 = new Endpoint("MinimalServer", 
//         						ProtocolKind.OPCUA, "127.0.0.1", 4841);
         
         Asset calculatorAsset = new Asset("Calculator", AssetKind.INSTANCE);
         calculatorAsset.setEndpoints(assetEndpoint1, assetEndpoint2);
         
         Identifier idAsset = new DefaultIdentifier();
         idAsset.setIdType(IdentifierType.IRI);
         idAsset.setIdentifier("https://www.dfki.de/ids/assets/15c9c860-34f3-4fc3-a292-66fd0442c1f9");
         calculatorAsset.setIdentification(idAsset);
         
         
         AASEndpoint aasEndpoint = new AASEndpoint("localhost", 2000);
         
         // Creating AssetAdministrationShell.
         AssetAdministrationShell Papyrus4Manufacturing = new AssetAdministrationShell("Papyrus4Manufacturing",
         									aasEndpoint);
         Papyrus4Manufacturing.setSubModels(documentation, controlSubModel, calculationSubModel);
         
         Identifier idAAS = new DefaultIdentifier();
         idAAS.setIdType(IdentifierType.IRI);
         idAAS.setIdentifier("https://www.dfki.de/Papyrus4Manufacturingworkshop/ids/aas/"
 				+ "23012dfa-0c1c-4408-bd06-dc3d9dcfcf4f");
         Papyrus4Manufacturing.setIdentification(idAAS);
         
         // Setting the AAS for the Asset.
         Papyrus4Manufacturing.setAsset(calculatorAsset);
         
         // Initialising the Generator.
         Project testAAS = new Project(Papyrus4Manufacturing);
         testAAS.setProjectName("secondTrial");
         testAAS.setNamespaceFromProjectName();
         testAAS.setProjectFolder("/home/tapanta/Documents/tuleap-eclipse-workspace/Second_Trial/");
         testAAS.createProject();
    }
    
    /**
     * Generating the Test with ConceptDescription and ConceptDictionary. 
     */
    public static void test3() {
    	
        Endpoint assetEndpoint1 = new Endpoint("CalculatorServer", 
				ProtocolKind.OPCUA, "opc.tcp://127.0.0.1:4840/");

        Endpoint assetEndpoint2 = new Endpoint("MinimalServer", 
				ProtocolKind.OPCUA, "opc.tcp://127.0.0.1:4841/");
         
         // Initializing ConceptDictionary
         ConceptDictionary conceptDict = new ConceptDictionary();
         
         // Creating ConceptDescriptions 
         // "Documentation"
         Identifier idDocumentation = new DefaultIdentifier();
         idDocumentation.setIdType(IdentifierType.IRI);
         idDocumentation.setIdentifier("https://www.dfki.de/ids/documentation/79c44263-30ad-4296-a51e-2df3a4aed4ae");
 
         ConceptDescription documentationConcept = new ConceptDescription("Documentation", idDocumentation);
         conceptDict.setConceptDescription(documentationConcept); 
         
         // "Add"
         Identifier idAdd = new DefaultIdentifier();
         idAdd.setIdType(IdentifierType.IRI);
         idAdd.setIdentifier("https://www.dfki.de/ids/add/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
 
         ConceptDescription addConcept = new ConceptDescription("Add", idAdd);
         addConcept.setCategory(Category.FUNCTION);
         conceptDict.setConceptDescription(addConcept); 
         
         // "Subtract"
         Identifier idSub = new DefaultIdentifier();
         idSub.setIdType(IdentifierType.IRI);
         idSub.setIdentifier("https://www.dfki.de/ids/sub/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
 
         ConceptDescription subtractConcept = new ConceptDescription("Subtract", idSub);
         subtractConcept.setCategory(Category.FUNCTION);
         conceptDict.setConceptDescription(subtractConcept); 
         
         // "Multiply"
         Identifier idMul = new DefaultIdentifier();
         idMul.setIdType(IdentifierType.IRI);
         idMul.setIdentifier("https://www.dfki.de/ids/multiply/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
 
         ConceptDescription multiplyConcept = new ConceptDescription("Multiply", idMul);
         multiplyConcept.setCategory(Category.FUNCTION);
         conceptDict.setConceptDescription(multiplyConcept); 
         
         // "Divide"
         Identifier idDivide = new DefaultIdentifier();
         idDivide.setIdType(IdentifierType.IRI);
         idDivide.setIdentifier("https://www.dfki.de/ids/divide/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
 
         ConceptDescription divideConcept = new ConceptDescription("Divide", idDivide);
         divideConcept.setCategory(Category.FUNCTION);
         conceptDict.setConceptDescription(divideConcept); 

     	
     	 // Declaring the 3 Submodels.
     	 SubModel documentation = new SubModel("Documentation");
     	 SubModel controlSubModel = new SubModel("Control");
     	 SubModel calculationSubModel = new SubModel("Calculation");
     	
     	 // Setting the Identifier Details of all these SubModels. 
     	 // "Documentation"
     	 Identifier idDoc = new DefaultIdentifier();
         idDoc.setIdType(IdentifierType.IRI);
         idDoc.setIdentifier("https://www.dfki.de/ids/sm/8c47c091-195e-43c5-bfb2-7248b6a31bf1");
         documentation.setIdentification(idDoc);
          
         // "Control"
         Identifier idControl = new DefaultIdentifier();
         idControl.setIdType(IdentifierType.IRI);
         idControl.setIdentifier("https://www.dfki.de/ids/sm/9a1bdba6-eb24-418a-a56e-f9d0ae621941");
         controlSubModel.setIdentification(idControl);
          
         // "Calculation"
         Identifier idCalculation = new DefaultIdentifier();
         idCalculation.setIdType(IdentifierType.IRI);
         idCalculation.setIdentifier("https://www.dfki.de/ids/sm/79c44263-30ad-4296-a51e-2df3a4aed4ae");
         calculationSubModel.setIdentification(idCalculation);
         
         // Setting ModelingKind of the SubModels.
         documentation.setKind(ModelingKind.INSTANCE);
         controlSubModel.setKind(ModelingKind.INSTANCE);
         calculationSubModel.setKind(ModelingKind.INSTANCE);
         
         // Setting Semantic Ids for SubModels. 
         Key docKey = new Key(KeyElements.CONCEPT_DESCRIPTION, true, 
         				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
         Reference docRef = new Reference(docKey);
         documentation.setSemanticIdentifier(docRef);
         
         Key controlKey = new Key(KeyElements.CONCEPT_DESCRIPTION, true, 
 				"0112/2///61360_4#AAF891#002", KeyType.IRDI);
         Reference controlRef = new Reference(controlKey);
         controlSubModel.setSemanticIdentifier(controlRef);
         
         Key calculationKey = new Key(KeyElements.CONCEPT_DESCRIPTION, true, 
 				"0112/2///61360_4#AAF891#002", KeyType.IRDI);
         Reference calculationRef = new Reference(calculationKey);
         calculationSubModel.setSemanticIdentifier(calculationRef);

         // Adding a File to the Submodel - "Documentation"
         File exampleFile = new File();
         exampleFile.setIdShort("exampleFile");
         exampleFile.setKind(ModelingKind.INSTANCE);
         exampleFile.setMimeType("audio/aac");
         exampleFile.setValue("example.com/myexample");
         documentation.setFile(exampleFile);
         
         // Populating the Submodel - "Documentation"
         MultiLanguageProperty manufacName = new MultiLanguageProperty("ManufacturerName");
         manufacName.setKind(ModelingKind.INSTANCE);
         manufacName.setValue("en", "Smartfactory KL");
         
         Key manufacKey = new Key(KeyElements.GLOBAL_REFERENCE, false, "0112/2///61360_4#MYNAME891#005", KeyType.IRDI);
         Reference manufNameRef = new Reference(manufacKey);
         manufacName.setSemanticIdentifier(manufNameRef);
         
         documentation.setMultiLanguageProperty(manufacName);
         
         MultiLanguageProperty prodName = new MultiLanguageProperty("ManufacturerProductDesignation");
         prodName.setKind(ModelingKind.INSTANCE);
         prodName.setValue("de", "Rechner");
         prodName.setValue("en", "Calculator");
         prodName.setValue("fr", "Calculatrice");
         documentation.setMultiLanguageProperty(prodName);
         
         
         SubModelElementCollection addressSEC = new SubModelElementCollection("Address");
         documentation.setSubModelElementCollection(addressSEC);
         
         //Adding to the parent SEC "Address" - "Phone". 
         
         SubModelElementCollection phoneSEC = new SubModelElementCollection("Phone");
         addressSEC.setSubModelElementCollection(phoneSEC);
         
         MultiLanguageProperty telephoneNumer = new MultiLanguageProperty("TelephoneNumber");
         phoneSEC.setMultiLanguageProperty(telephoneNumer);
         
         Property typeTel = new Property("TypeOfTelephone");
         phoneSEC.setProperty(typeTel);
         
         
         // ---------------------------------------------------------------------
         
         // Populating the Submodel - "Control".
         SubModelElementCollection controlSEC = new SubModelElementCollection("ControlParameters");
         controlSEC.setAllowDuplicates(false);
         controlSEC.setOrdered(true);
         controlSEC.setKind(ModelingKind.INSTANCE);
         
         // Creating a demo SubModelElementCollection - "Example" & adding it to the SEC - "ControlParameters".
         SubModelElementCollection exampleSEC = new SubModelElementCollection("Example");
         exampleSEC.setAllowDuplicates(false);
         exampleSEC.setOrdered(true);
         exampleSEC.setKind(ModelingKind.INSTANCE);
         controlSEC.setSubModelElementCollection(exampleSEC);
         System.out.println("Parent of Example: " + exampleSEC.getParentSEC().getIdShort());
         
         // Adding a File to the SEC - "Example"
         File anotherFile = new File();
         anotherFile.setIdShort("anotherFile");
         anotherFile.setKind(ModelingKind.INSTANCE);
         anotherFile.setMimeType("audio/aac");
         anotherFile.setValue("another.com/myanotherexample");
         anotherFile.setDynamic(true);
         exampleSEC.setFile(anotherFile);
         
         Property opVolt = new Property("OperatingVoltage");
         opVolt.setValueType(ValueType.Integer);
         opVolt.setDynamic(true);
         opVolt.setValue(5);
         controlSEC.setProperty(opVolt);
         
         Property unit = new Property("Unit");
         unit.setValueType(ValueType.String);
         unit.setKind(ModelingKind.INSTANCE);
         unit.setValue("Volts");
         controlSEC.setProperty(unit);
         
         Property opTemp = new Property("OperatingTemperature");
         opTemp.setValueType(ValueType.Integer);
         opTemp.setDynamic(true);
         opTemp.setEndpoint(assetEndpoint1);
         opTemp.setNameSpaceIndex(2);
         opTemp.setIdentifier(5);
         controlSubModel.setProperty(opTemp);
         
         // Adding the SubModelelementCollection to the Submodel.
         controlSubModel.setSubModelElementCollection(controlSEC);
         
         // Declaring in Submodel "Control" - an Operation "ChangeManufacturerName".
         OperationVariable manuNameInp = new OperationVariable("ManufacturerName",
         								ValueType.String);
         Operation changeManuName = new Operation("changeManufacturerName");
         changeManuName.setInputVariable(manuNameInp);
         controlSubModel.setOperation(changeManuName);
         
         // Declaring in Submodel "Control" - an Operation "Status".
         OperationVariable statusOut = new OperationVariable("Status", 
         								ValueType.Boolean);
         Operation statusCheck = new Operation("checkStatus");
         statusCheck.setOutputVariable(statusOut);
         controlSubModel.setOperation(statusCheck);
         // ---------------------------------------------------------------------
         
         // Populating the Submodel - "Calculation".
         OperationVariable operand1 = new OperationVariable("Operand1", ValueType.Integer);
         OperationVariable operand2 = new OperationVariable("Operand2", ValueType.Integer);
         OperationVariable result = new OperationVariable("Result", ValueType.Integer);
         
         // Adding Operations. 
         Operation add = new Operation("Add");
         add.setSemanticDescription(addConcept);
         
//         add.setSemanticIdentifier(xyz);
         
         add.setKind(ModelingKind.INSTANCE);
         add.setInputVariables(operand1, operand2);
         add.setOutputVariable(result);
         calculationSubModel.setOperation(add);
         
         Operation sub = new Operation("Subtract");
         sub.setSemanticDescription(subtractConcept);
         sub.setKind(ModelingKind.INSTANCE);
         sub.setInputVariables(operand1, operand2);
         sub.setOutputVariable(result);
         calculationSubModel.setOperation(sub);
         
         Operation multiply = new Operation("Multiply");
         multiply.setSemanticDescription(multiplyConcept);
         multiply.setKind(ModelingKind.INSTANCE);
         multiply.setInputVariables(operand1, operand2);
         multiply.setOutputVariable(result);
         calculationSubModel.setOperation(multiply);
         
         Operation divide = new Operation("Divide");
         divide.setSemanticDescription(divideConcept);
         divide.setKind(ModelingKind.INSTANCE);
         divide.setInputVariables(operand1, operand2);
         divide.setOutputVariable(result);
         calculationSubModel.setOperation(divide);
         
         SubModelElementCollection calSEC = new SubModelElementCollection("CalculationCollection");
         calSEC.setAllowDuplicates(false);
         calSEC.setOrdered(true);
         calSEC.setDynamic(true);
         calSEC.setKind(ModelingKind.INSTANCE);
         calculationSubModel.setSubModelElementCollection(calSEC);
//         System.out.println("Parent of Example: " + calSEC.getParentSEC().getIdShort());
         
         SubModelElementCollection trialSEC = new SubModelElementCollection("DETrialCollection");
         trialSEC.setAllowDuplicates(false);
         trialSEC.setOrdered(true);
         trialSEC.setDynamic(true);
         trialSEC.setKind(ModelingKind.INSTANCE);
         calSEC.setSubModelElementCollection(trialSEC);
         
         SubModelElementCollection trialSEC1 = new SubModelElementCollection("DETrial1Collection");
         trialSEC1.setAllowDuplicates(false);
         trialSEC1.setOrdered(true);
         trialSEC1.setDynamic(true);
         trialSEC1.setKind(ModelingKind.INSTANCE);
         trialSEC.setSubModelElementCollection(trialSEC1);
         
         
         Asset calculatorAsset = new Asset("Calculator", AssetKind.INSTANCE);
         calculatorAsset.setEndpoints(assetEndpoint1, assetEndpoint2);
         
         Identifier idAsset = new DefaultIdentifier();
         idAsset.setIdType(IdentifierType.IRI);
         idAsset.setIdentifier("https://www.dfki.de/ids/assets/15c9c860-34f3-4fc3-a292-66fd0442c1f9");
         calculatorAsset.setIdentification(idAsset);
         
         
         AASEndpoint aasEndpoint = new AASEndpoint("localhost", 2000);
         
         // Creating AssetAdministrationShell.
         AssetAdministrationShell Papyrus4Manufacturing = new AssetAdministrationShell("Papyrus4Manufacturing",
         									aasEndpoint);
         Papyrus4Manufacturing.setSubModels(documentation, controlSubModel, calculationSubModel);
         
         // Adding the ConceptDictionary to Asset Administration Shell
         Papyrus4Manufacturing.setConceptDictionary(conceptDict);
         
         Identifier idAAS = new DefaultIdentifier();
         idAAS.setIdType(IdentifierType.IRI);
         idAAS.setIdentifier("https://www.dfki.de/Papyrus4Manufacturingworkshop/ids/aas/"
 				+ "23012dfa-0c1c-4408-bd06-dc3d9dcfcf4f");
         Papyrus4Manufacturing.setIdentification(idAAS);
         
         // Setting the AAS for the Asset.
         Papyrus4Manufacturing.setAsset(calculatorAsset);
         
         // Initializing the Generator.
         Project testAAS = new Project(Papyrus4Manufacturing);
         testAAS.setProjectName("firstTrial");
         testAAS.setNamespaceFromProjectName();
         testAAS.setProjectFolder("/home/tapanta/Documents/tuleap-eclipse-workspace/Third_Trial/");
         testAAS.createProject();
    }
    
    /**
     * Generating the Test with ConceptDescription and ConceptDictionary.
     * Using the isCaseOf Function here. 
     * 
     *  Write case for:
     *  ConceptDescription with:
     *  	- One Reference in isCaseOf.
     *  	- One Key in this one reference.
     *  	- Multiple Keys in this one Reference.
     *  	- Multiple References in isCaseOf with single Key.
     *  	- Multiple References in isCaseOf with multiple Key.
     */
    public static void test4() {
    	
        Endpoint assetEndpoint1 = new Endpoint("CalculatorServer", 
				ProtocolKind.OPCUA, "opc.tcp://127.0.0.1:4840/");

        Endpoint assetEndpoint2 = new Endpoint("MinimalServer", 
				ProtocolKind.OPCUA, "opc.tcp://127.0.0.1:4841/");
         
         // Initializing ConceptDictionary
         ConceptDictionary conceptDict = new ConceptDictionary();
         
         // Creating ConceptDescriptions 
         // "Documentation"
         Identifier idDocumentation = new DefaultIdentifier();
         idDocumentation.setIdType(IdentifierType.IRI);
         idDocumentation.setIdentifier("https://www.dfki.de/ids/documentation/79c44263-30ad-4296-a51e-2df3a4aed4ae");
 
         ConceptDescription documentationConcept = new ConceptDescription("Documentation", idDocumentation);
         conceptDict.setConceptDescription(documentationConcept); 
         
         // "Add"
         Identifier idAdd = new DefaultIdentifier();
         idAdd.setIdType(IdentifierType.IRI);
         idAdd.setIdentifier("https://www.dfki.de/ids/add/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
 
         ConceptDescription addConcept = new ConceptDescription("Add", idAdd);
         addConcept.setCategory(Category.FUNCTION);
         conceptDict.setConceptDescription(addConcept); 
         
         // "Subtract"
         Identifier idSub = new DefaultIdentifier();
         idSub.setIdType(IdentifierType.IRI);
         idSub.setIdentifier("https://www.dfki.de/ids/sub/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
 
         ConceptDescription subtractConcept = new ConceptDescription("Subtract", idSub);
         subtractConcept.setCategory(Category.FUNCTION);
         conceptDict.setConceptDescription(subtractConcept); 
         
         // "Multiply"
         Identifier idMul = new DefaultIdentifier();
         idMul.setIdType(IdentifierType.IRI);
         idMul.setIdentifier("https://www.dfki.de/ids/multiply/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
 
         ConceptDescription multiplyConcept = new ConceptDescription("Multiply", idMul);
         multiplyConcept.setCategory(Category.FUNCTION);
         conceptDict.setConceptDescription(multiplyConcept); 
         
         // "Divide"
         Identifier idDivide = new DefaultIdentifier();
         idDivide.setIdType(IdentifierType.IRI);
         idDivide.setIdentifier("https://www.dfki.de/ids/divide/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
 
         ConceptDescription divideConcept = new ConceptDescription("Divide", idDivide);
         divideConcept.setCategory(Category.FUNCTION);
         conceptDict.setConceptDescription(divideConcept); 
         
/*1.*/    /*
          * Trying out ConceptDescription with:
          * 	-  One Reference in isCaseOf
          * 	- One Key in this one reference.
          */
         Identifier idTrial = new DefaultIdentifier();
         idTrial.setIdType(IdentifierType.IRDI);
         idTrial.setIdentifier("565#//256641//45//pa8");
         
         ConceptDescription trialConcept = new ConceptDescription("Trial", idTrial);
         Key trialKey = new Key(KeyElements.PROPERTY, true, 
  				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
         Reference trialRef = new Reference(trialKey);
         trialConcept.setIsCaseOf(trialRef);
         conceptDict.setConceptDescription(trialConcept);
         
/*2.*/    /*
          * Trying out ConceptDescription with:
          * 	-  One Reference in isCaseOf
          * 	- Multiple Keys in this one reference.
          */
//         Identifier idTrial1 = new DefaultIdentifier();
//         idTrial1.setIdType(IdentifierType.IRDI);
//         idTrial1.setIdentifier("565#//256641//45//pa8");
//         
//         ConceptDescription trial1Concept = new ConceptDescription("Trial1", idTrial1);
//         Key trial1Key1 = new Key(KeyElements.PROPERTY, true, 
//  				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
//         Key trial1Key2 = new Key(KeyElements.SUBMODEL_ELEMENT, true, 
//  				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
//
//         Reference trial1Ref = new Reference(trial1Key1);
//         trial1Ref.setKey(trial1Key2);
//         trial1Concept.setIsCaseOf(trial1Ref);
//         conceptDict.setConceptDescription(trial1Concept);
         
/*3.*/    /*
          * Trying out ConceptDescription with:
          * 	-  Multiple Reference in isCaseOf
          * 	- Multiple Keys in these multiple references.
          */
//         Identifier idTrial2 = new DefaultIdentifier();
//         idTrial2.setIdType(IdentifierType.IRDI);
//         idTrial2.setIdentifier("003#//256641//45//pa8");
//         
//         ConceptDescription trial2Concept = new ConceptDescription("Trial2", idTrial2);
//         Key trial2Key1 = new Key(KeyElements.PROPERTY, true, 
//  				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
//         Key trial2Key2 = new Key(KeyElements.SUBMODEL_ELEMENT, true, 
//  				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
//
//         Reference trial2Ref1 = new Reference(trial2Key1);
//         trial2Ref1.setKey(trial2Key2);
//         
//         Key trial2Key3 = new Key(KeyElements.OPERATION, true, 
//   				"0112/2///61360_4#AASDF891#001", KeyType.IRDI);
//         
//         Key trial2Key4 = new Key(KeyElements.SUBMODEL_ELEMENT_COLLECTION, true, 
//   				"0112/2///61360_4sdf#AAF891#001", KeyType.IRDI);
//         
//         Reference trial2Ref2 = new Reference(trial2Key3);
//         trial2Ref2.setKey(trial2Key4);
//         
//         trial2Concept.setIsCaseOf(trial2Ref1, trial2Ref2);
//         conceptDict.setConceptDescription(trial2Concept);


     	
     	 // Declaring the 3 Submodels.
     	 SubModel documentation = new SubModel("Documentation");
     	 SubModel controlSubModel = new SubModel("Control");
     	 SubModel calculationSubModel = new SubModel("Calculation");
     	
     	 // Setting the Identifier Details of all these SubModels. 
     	 // "Documentation"
     	 Identifier idDoc = new DefaultIdentifier();
         idDoc.setIdType(IdentifierType.IRI);
         idDoc.setIdentifier("https://www.dfki.de/ids/sm/8c47c091-195e-43c5-bfb2-7248b6a31bf1");
         documentation.setIdentification(idDoc);
          
         // "Control"
         Identifier idControl = new DefaultIdentifier();
         idControl.setIdType(IdentifierType.IRI);
         idControl.setIdentifier("https://www.dfki.de/ids/sm/9a1bdba6-eb24-418a-a56e-f9d0ae621941");
         controlSubModel.setIdentification(idControl);
          
         // "Calculation"
         Identifier idCalculation = new DefaultIdentifier();
         idCalculation.setIdType(IdentifierType.IRI);
         idCalculation.setIdentifier("https://www.dfki.de/ids/sm/79c44263-30ad-4296-a51e-2df3a4aed4ae");
         calculationSubModel.setIdentification(idCalculation);
         
         // Setting ModelingKind of the SubModels.
         documentation.setKind(ModelingKind.INSTANCE);
         controlSubModel.setKind(ModelingKind.INSTANCE);
         calculationSubModel.setKind(ModelingKind.INSTANCE);
         
         // Setting Semantic Ids for SubModels. 
         Key docKey = new Key(KeyElements.CONCEPT_DESCRIPTION, true, 
         				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
         Reference docRef = new Reference(docKey);
         documentation.setSemanticIdentifier(docRef);
         
         Key controlKey = new Key(KeyElements.CONCEPT_DESCRIPTION, true, 
 				"0112/2///61360_4#AAF891#002", KeyType.IRDI);
         Reference controlRef = new Reference(controlKey);
         controlSubModel.setSemanticIdentifier(controlRef);
         
         Key calculationKey = new Key(KeyElements.CONCEPT_DESCRIPTION, true, 
 				"0112/2///61360_4#AAF891#002", KeyType.IRDI);
         Reference calculationRef = new Reference(calculationKey);
         calculationSubModel.setSemanticIdentifier(calculationRef);

         // Adding a File to the Submodel - "Documentation"
         File exampleFile = new File();
         exampleFile.setIdShort("exampleFile");
         exampleFile.setKind(ModelingKind.INSTANCE);
         exampleFile.setMimeType("audio/aac");
         exampleFile.setValue("example.com/myexample");
         documentation.setFile(exampleFile);
         
         // Populating the Submodel - "Documentation"
         MultiLanguageProperty manufacName = new MultiLanguageProperty("ManufacturerName");
         manufacName.setKind(ModelingKind.INSTANCE);
         manufacName.setValue("en", "Smartfactory KL");
         
         Key manufacKey = new Key(KeyElements.GLOBAL_REFERENCE, false, "0112/2///61360_4#MYNAME891#005", KeyType.IRDI);
         Reference manufNameRef = new Reference(manufacKey);
         manufacName.setSemanticIdentifier(manufNameRef);
         
         documentation.setMultiLanguageProperty(manufacName);
         
         MultiLanguageProperty prodName = new MultiLanguageProperty("ManufacturerProductDesignation");
         prodName.setKind(ModelingKind.INSTANCE);
         prodName.setValue("de", "Rechner");
         prodName.setValue("en", "Calculator");
         prodName.setValue("fr", "Calculatrice");
         documentation.setMultiLanguageProperty(prodName);
         
         
         SubModelElementCollection addressSEC = new SubModelElementCollection("Address");
         documentation.setSubModelElementCollection(addressSEC);
         
         //Adding to the parent SEC "Address" - "Phone". 
         
         SubModelElementCollection phoneSEC = new SubModelElementCollection("Phone");
         addressSEC.setSubModelElementCollection(phoneSEC);
         
         MultiLanguageProperty telephoneNumer = new MultiLanguageProperty("TelephoneNumber");
         phoneSEC.setMultiLanguageProperty(telephoneNumer);
         
         Property typeTel = new Property("TypeOfTelephone");
         phoneSEC.setProperty(typeTel);
         
         
         // ---------------------------------------------------------------------
         
         // Populating the Submodel - "Control".
         SubModelElementCollection controlSEC = new SubModelElementCollection("ControlParameters");
         controlSEC.setAllowDuplicates(false);
         controlSEC.setOrdered(true);
         controlSEC.setKind(ModelingKind.INSTANCE);
         
         // Creating a demo SubModelElementCollection - "Example" & adding it to the SEC - "ControlParameters".
         SubModelElementCollection exampleSEC = new SubModelElementCollection("Example");
         exampleSEC.setAllowDuplicates(false);
         exampleSEC.setOrdered(true);
         exampleSEC.setKind(ModelingKind.INSTANCE);
         controlSEC.setSubModelElementCollection(exampleSEC);
         System.out.println("Parent of Example: " + exampleSEC.getParentSEC().getIdShort());
         
         // Adding a File to the SEC - "Example"
         File anotherFile = new File();
         anotherFile.setIdShort("anotherFile");
         anotherFile.setKind(ModelingKind.INSTANCE);
         anotherFile.setMimeType("audio/aac");
         anotherFile.setValue("another.com/myanotherexample");
         anotherFile.setDynamic(true);
         exampleSEC.setFile(anotherFile);
         
         Property opVolt = new Property("OperatingVoltage");
         opVolt.setValueType(ValueType.Integer);
         opVolt.setDynamic(true);
         opVolt.setValue(5);
         controlSEC.setProperty(opVolt);
         
         Property unit = new Property("Unit");
         unit.setValueType(ValueType.String);
         unit.setKind(ModelingKind.INSTANCE);
         unit.setValue("Volts");
         controlSEC.setProperty(unit);
         
         Property opTemp = new Property("OperatingTemperature");
         opTemp.setValueType(ValueType.Integer);
         opTemp.setDynamic(true);
         opTemp.setEndpoint(assetEndpoint1);
         opTemp.setNameSpaceIndex(2);
         opTemp.setIdentifier(5);
         controlSubModel.setProperty(opTemp);
         
         // Adding the SubModelelementCollection to the Submodel.
         controlSubModel.setSubModelElementCollection(controlSEC);
         
         // Declaring in Submodel "Control" - an Operation "ChangeManufacturerName".
         OperationVariable manuNameInp = new OperationVariable("ManufacturerName",
         								ValueType.String);
         Operation changeManuName = new Operation("changeManufacturerName");
         changeManuName.setInputVariable(manuNameInp);
         controlSubModel.setOperation(changeManuName);
         
         // Declaring in Submodel "Control" - an Operation "Status".
         OperationVariable statusOut = new OperationVariable("Status", 
         								ValueType.Boolean);
         Operation statusCheck = new Operation("checkStatus");
         statusCheck.setOutputVariable(statusOut);
         controlSubModel.setOperation(statusCheck);
         // ---------------------------------------------------------------------
         
         // Populating the Submodel - "Calculation".
         OperationVariable operand1 = new OperationVariable("Operand1", ValueType.Integer);
         OperationVariable operand2 = new OperationVariable("Operand2", ValueType.Integer);
         OperationVariable result = new OperationVariable("Result", ValueType.Integer);
         
         // Adding Operations. 
         Operation add = new Operation("Add");
         add.setSemanticDescription(addConcept);
         
//         add.setSemanticIdentifier(xyz);
         
         add.setKind(ModelingKind.INSTANCE);
         add.setInputVariables(operand1, operand2);
         add.setOutputVariable(result);
         calculationSubModel.setOperation(add);
         
         Operation sub = new Operation("Subtract");
         sub.setSemanticDescription(subtractConcept);
         sub.setKind(ModelingKind.INSTANCE);
         sub.setInputVariables(operand1, operand2);
         sub.setOutputVariable(result);
         calculationSubModel.setOperation(sub);
         
         Operation multiply = new Operation("Multiply");
         multiply.setSemanticDescription(multiplyConcept);
         multiply.setKind(ModelingKind.INSTANCE);
         multiply.setInputVariables(operand1, operand2);
         multiply.setOutputVariable(result);
         calculationSubModel.setOperation(multiply);
         
         Operation divide = new Operation("Divide");
         divide.setSemanticDescription(divideConcept);
         divide.setKind(ModelingKind.INSTANCE);
         divide.setInputVariables(operand1, operand2);
         divide.setOutputVariable(result);
         calculationSubModel.setOperation(divide);
         
         SubModelElementCollection calSEC = new SubModelElementCollection("CalculationCollection");
         calSEC.setAllowDuplicates(false);
         calSEC.setOrdered(true);
         calSEC.setDynamic(true);
         calSEC.setKind(ModelingKind.INSTANCE);
         calculationSubModel.setSubModelElementCollection(calSEC);
//         System.out.println("Parent of Example: " + calSEC.getParentSEC().getIdShort());
         
         SubModelElementCollection trialSEC = new SubModelElementCollection("DETrialCollection");
         trialSEC.setAllowDuplicates(false);
         trialSEC.setOrdered(true);
         trialSEC.setDynamic(true);
         trialSEC.setKind(ModelingKind.INSTANCE);
         calSEC.setSubModelElementCollection(trialSEC);
         
         SubModelElementCollection trialSEC1 = new SubModelElementCollection("DETrial1Collection");
         trialSEC1.setAllowDuplicates(false);
         trialSEC1.setOrdered(true);
         trialSEC1.setDynamic(true);
         trialSEC1.setKind(ModelingKind.INSTANCE);
         trialSEC.setSubModelElementCollection(trialSEC1);
         
         
         Asset calculatorAsset = new Asset("Calculator", AssetKind.INSTANCE);
         calculatorAsset.setEndpoints(assetEndpoint1, assetEndpoint2);
         
         Identifier idAsset = new DefaultIdentifier();
         idAsset.setIdType(IdentifierType.IRI);
         idAsset.setIdentifier("https://www.dfki.de/ids/assets/15c9c860-34f3-4fc3-a292-66fd0442c1f9");
         calculatorAsset.setIdentification(idAsset);
         
         
         AASEndpoint aasEndpoint = new AASEndpoint("localhost", 2000);
         
         // Creating AssetAdministrationShell.
         AssetAdministrationShell Papyrus4Manufacturing = new AssetAdministrationShell("Papyrus4Manufacturing",
         									aasEndpoint);
         Papyrus4Manufacturing.setSubModels(documentation, controlSubModel, calculationSubModel);
         
         // Adding the ConceptDictionary to Asset Administration Shell
         Papyrus4Manufacturing.setConceptDictionary(conceptDict);
         
         Identifier idAAS = new DefaultIdentifier();
         idAAS.setIdType(IdentifierType.IRI);
         idAAS.setIdentifier("https://www.dfki.de/Papyrus4Manufacturingworkshop/ids/aas/"
 				+ "23012dfa-0c1c-4408-bd06-dc3d9dcfcf4f");
         Papyrus4Manufacturing.setIdentification(idAAS);
         
         // Setting the AAS for the Asset.
         Papyrus4Manufacturing.setAsset(calculatorAsset);
         
         // Initializing the Generator.
         Project testAAS = new Project(Papyrus4Manufacturing);
         testAAS.setProjectName("firstTrial");
         testAAS.setNamespaceFromProjectName();
         testAAS.setProjectFolder("/home/tapanta/Documents/tuleap-eclipse-workspace/Fourth_Trial/");
         testAAS.createProject();
    }
    
    /**
     * Copied from test4()
     * 
     * Generating the Test for Submodels with different kinds of Identifier Types.  
     * 
     *  Write case for:
     *  Submodel with:
     *  	- ModelingKind - INSTANCE :-
     *  		- Identifier Type - IRDI 	- 	Should receive an IllegalStateExcception.
     *  		- Identifier Type - IRI  	- 	Should work.
     *  		- Identifier Type - CUSTOM 	- 	Should work.
     *  		[TEST: PASSED]
     *  
     *		- ModelingKind - TEMPLATE :-
     *  		- Identifier Type - IRDI 	- 	Should work.
     *  		- Identifier Type - IRI  	- 	Should work.
     *  		- Identifier Type - CUSTOM 	- 	Should receive an IllegalStateExcception.
     * 	 		[TEST: PASSED]
  	 *
     */
    public static void test5() {
    	
        Endpoint assetEndpoint1 = new Endpoint("CalculatorServer", 
				ProtocolKind.OPCUA, "opc.tcp://127.0.0.1:4840/");

        Endpoint assetEndpoint2 = new Endpoint("MinimalServer", 
				ProtocolKind.OPCUA, "opc.tcp://127.0.0.1:4841/");
         
         // Initializing ConceptDictionary
         ConceptDictionary conceptDict = new ConceptDictionary();
         
         // Creating ConceptDescriptions 
         // "Documentation"
         Identifier idDocumentation = new DefaultIdentifier();
         idDocumentation.setIdType(IdentifierType.IRI);
         idDocumentation.setIdentifier("https://www.dfki.de/ids/documentation/79c44263-30ad-4296-a51e-2df3a4aed4ae");
 
         ConceptDescription documentationConcept = new ConceptDescription("Documentation", idDocumentation);
         conceptDict.setConceptDescription(documentationConcept); 
         
         // "Add"
         Identifier idAdd = new DefaultIdentifier();
         idAdd.setIdType(IdentifierType.IRI);
         idAdd.setIdentifier("https://www.dfki.de/ids/add/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
 
         ConceptDescription addConcept = new ConceptDescription("Add", idAdd);
         addConcept.setCategory(Category.FUNCTION);
         conceptDict.setConceptDescription(addConcept); 
         
         // "Subtract"
         Identifier idSub = new DefaultIdentifier();
         idSub.setIdType(IdentifierType.IRI);
         idSub.setIdentifier("https://www.dfki.de/ids/sub/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
 
         ConceptDescription subtractConcept = new ConceptDescription("Subtract", idSub);
         subtractConcept.setCategory(Category.FUNCTION);
         conceptDict.setConceptDescription(subtractConcept); 
         
         // "Multiply"
         Identifier idMul = new DefaultIdentifier();
         idMul.setIdType(IdentifierType.IRI);
         idMul.setIdentifier("https://www.dfki.de/ids/multiply/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
 
         ConceptDescription multiplyConcept = new ConceptDescription("Multiply", idMul);
         multiplyConcept.setCategory(Category.FUNCTION);
         conceptDict.setConceptDescription(multiplyConcept); 
         
         // "Divide"
         Identifier idDivide = new DefaultIdentifier();
         idDivide.setIdType(IdentifierType.IRI);
         idDivide.setIdentifier("https://www.dfki.de/ids/divide/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
 
         ConceptDescription divideConcept = new ConceptDescription("Divide", idDivide);
         divideConcept.setCategory(Category.FUNCTION);
         conceptDict.setConceptDescription(divideConcept); 
         
/*1.*/    /*
          * Trying out ConceptDescription with:
          * 	-  One Reference in isCaseOf
          * 	- One Key in this one reference.
          */
         Identifier idTrial = new DefaultIdentifier();
         idTrial.setIdType(IdentifierType.IRDI);
         idTrial.setIdentifier("565#//256641//45//pa8");
         
         ConceptDescription trialConcept = new ConceptDescription("Trial", idTrial);
         Key trialKey = new Key(KeyElements.PROPERTY, true, 
  				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
         Reference trialRef = new Reference(trialKey);
         trialConcept.setIsCaseOf(trialRef);
         conceptDict.setConceptDescription(trialConcept);
         
/*2.*/    /*
          * Trying out ConceptDescription with:
          * 	-  One Reference in isCaseOf
          * 	- Multiple Keys in this one reference.
          */
//         Identifier idTrial1 = new DefaultIdentifier();
//         idTrial1.setIdType(IdentifierType.IRDI);
//         idTrial1.setIdentifier("565#//256641//45//pa8");
//         
//         ConceptDescription trial1Concept = new ConceptDescription("Trial1", idTrial1);
//         Key trial1Key1 = new Key(KeyElements.PROPERTY, true, 
//  				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
//         Key trial1Key2 = new Key(KeyElements.SUBMODEL_ELEMENT, true, 
//  				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
//
//         Reference trial1Ref = new Reference(trial1Key1);
//         trial1Ref.setKey(trial1Key2);
//         trial1Concept.setIsCaseOf(trial1Ref);
//         conceptDict.setConceptDescription(trial1Concept);
         
/*3.*/    /*
          * Trying out ConceptDescription with:
          * 	-  Multiple Reference in isCaseOf
          * 	- Multiple Keys in these multiple references.
          */
//         Identifier idTrial2 = new DefaultIdentifier();
//         idTrial2.setIdType(IdentifierType.IRDI);
//         idTrial2.setIdentifier("003#//256641//45//pa8");
//         
//         ConceptDescription trial2Concept = new ConceptDescription("Trial2", idTrial2);
//         Key trial2Key1 = new Key(KeyElements.PROPERTY, true, 
//  				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
//         Key trial2Key2 = new Key(KeyElements.SUBMODEL_ELEMENT, true, 
//  				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
//
//         Reference trial2Ref1 = new Reference(trial2Key1);
//         trial2Ref1.setKey(trial2Key2);
//         
//         Key trial2Key3 = new Key(KeyElements.OPERATION, true, 
//   				"0112/2///61360_4#AASDF891#001", KeyType.IRDI);
//         
//         Key trial2Key4 = new Key(KeyElements.SUBMODEL_ELEMENT_COLLECTION, true, 
//   				"0112/2///61360_4sdf#AAF891#001", KeyType.IRDI);
//         
//         Reference trial2Ref2 = new Reference(trial2Key3);
//         trial2Ref2.setKey(trial2Key4);
//         
//         trial2Concept.setIsCaseOf(trial2Ref1, trial2Ref2);
//         conceptDict.setConceptDescription(trial2Concept);


     	
     	 // Declaring the 3 Submodels.
     	 SubModel documentation = new SubModel("Documentation");
     	 SubModel controlSubModel = new SubModel("Control");
     	 SubModel calculationSubModel = new SubModel("Calculation");
     	
     	 // Setting the Identifier Details of all these SubModels. 
     	 // "Documentation"
     	 Identifier idDoc = new DefaultIdentifier();
         idDoc.setIdType(IdentifierType.IRDI);
//         idDoc.setIdentifier("https://www.dfki.de/ids/sub/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
         idDoc.setIdentifier("0112/2///61360_4#AAF891#002");
         documentation.setIdentification(idDoc);
          
         // "Control"
         Identifier idControl = new DefaultIdentifier();
         idControl.setIdType(IdentifierType.IRDI);
//         idControl.setIdentifier("https://www.dfki.de/ids/sub/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
         idControl.setIdentifier("0112/2///61360_4#AAF891#002");
         controlSubModel.setIdentification(idControl);
          
         // "Calculation"
         Identifier idCalculation = new DefaultIdentifier();
         idCalculation.setIdType(IdentifierType.IRDI);
//         idCalculation.setIdentifier("https://www.dfki.de/ids/sub/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
         idCalculation.setIdentifier("0112/2///61360_4#AAF891#002");
         calculationSubModel.setIdentification(idCalculation);
         
         // Setting ModelingKind of the SubModels.
         documentation.setKind(ModelingKind.TEMPLATE);
         controlSubModel.setKind(ModelingKind.TEMPLATE);
         calculationSubModel.setKind(ModelingKind.TEMPLATE);
         
         // Setting Semantic Ids for SubModels. 
         Key docKey = new Key(KeyElements.CONCEPT_DESCRIPTION, true, 
         				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
         Reference docRef = new Reference(docKey);
         documentation.setSemanticIdentifier(docRef);
         
         Key controlKey = new Key(KeyElements.CONCEPT_DESCRIPTION, true, 
 				"0112/2///61360_4#AAF891#002", KeyType.IRDI);
         Reference controlRef = new Reference(controlKey);
         controlSubModel.setSemanticIdentifier(controlRef);
         
         Key calculationKey = new Key(KeyElements.CONCEPT_DESCRIPTION, true, 
 				"0112/2///61360_4#AAF891#002", KeyType.IRDI);
         Reference calculationRef = new Reference(calculationKey);
         calculationSubModel.setSemanticIdentifier(calculationRef);

         // Adding a File to the Submodel - "Documentation"
         File exampleFile = new File();
         exampleFile.setIdShort("exampleFile");
         exampleFile.setKind(ModelingKind.INSTANCE);
         exampleFile.setMimeType("audio/aac");
         exampleFile.setValue("example.com/myexample");
         documentation.setFile(exampleFile);
         
         // Populating the Submodel - "Documentation"
         MultiLanguageProperty manufacName = new MultiLanguageProperty("ManufacturerName");
         manufacName.setKind(ModelingKind.INSTANCE);
         manufacName.setValue("en", "Smartfactory KL");
         
         Key manufacKey = new Key(KeyElements.GLOBAL_REFERENCE, false, "0112/2///61360_4#MYNAME891#005", KeyType.IRDI);
         Reference manufNameRef = new Reference(manufacKey);
         manufacName.setSemanticIdentifier(manufNameRef);
         
         documentation.setMultiLanguageProperty(manufacName);
         
         MultiLanguageProperty prodName = new MultiLanguageProperty("ManufacturerProductDesignation");
         prodName.setKind(ModelingKind.INSTANCE);
         prodName.setValue("de", "Rechner");
         prodName.setValue("en", "Calculator");
         prodName.setValue("fr", "Calculatrice");
         documentation.setMultiLanguageProperty(prodName);
         
         
         SubModelElementCollection addressSEC = new SubModelElementCollection("Address");
         documentation.setSubModelElementCollection(addressSEC);
         
         //Adding to the parent SEC "Address" - "Phone". 
         
         SubModelElementCollection phoneSEC = new SubModelElementCollection("Phone");
         addressSEC.setSubModelElementCollection(phoneSEC);
         
         MultiLanguageProperty telephoneNumer = new MultiLanguageProperty("TelephoneNumber");
         phoneSEC.setMultiLanguageProperty(telephoneNumer);
         
         Property typeTel = new Property("TypeOfTelephone");
         phoneSEC.setProperty(typeTel);
         
         
         // ---------------------------------------------------------------------
         
         // Populating the Submodel - "Control".
         SubModelElementCollection controlSEC = new SubModelElementCollection("ControlParameters");
         controlSEC.setAllowDuplicates(false);
         controlSEC.setOrdered(true);
         controlSEC.setKind(ModelingKind.INSTANCE);
         
         // Creating a demo SubModelElementCollection - "Example" & adding it to the SEC - "ControlParameters".
         SubModelElementCollection exampleSEC = new SubModelElementCollection("Example");
         exampleSEC.setAllowDuplicates(false);
         exampleSEC.setOrdered(true);
         exampleSEC.setKind(ModelingKind.INSTANCE);
         controlSEC.setSubModelElementCollection(exampleSEC);
         System.out.println("Parent of Example: " + exampleSEC.getParentSEC().getIdShort());
         
         // Adding a File to the SEC - "Example"
         File anotherFile = new File();
         anotherFile.setIdShort("anotherFile");
         anotherFile.setKind(ModelingKind.INSTANCE);
         anotherFile.setMimeType("audio/aac");
         anotherFile.setValue("another.com/myanotherexample");
         anotherFile.setDynamic(true);
         exampleSEC.setFile(anotherFile);
         
         Property opVolt = new Property("OperatingVoltage");
         opVolt.setValueType(ValueType.Integer);
         opVolt.setDynamic(true);
         opVolt.setValue(5);
         controlSEC.setProperty(opVolt);
         
         Property unit = new Property("Unit");
         unit.setValueType(ValueType.String);
         unit.setKind(ModelingKind.INSTANCE);
         unit.setValue("Volts");
         controlSEC.setProperty(unit);
         
         Property opTemp = new Property("OperatingTemperature");
         opTemp.setValueType(ValueType.Integer);
         opTemp.setDynamic(true);
         opTemp.setEndpoint(assetEndpoint1);
         opTemp.setNameSpaceIndex(2);
         opTemp.setIdentifier(5);
         controlSubModel.setProperty(opTemp);
         
         // Adding the SubModelelementCollection to the Submodel.
         controlSubModel.setSubModelElementCollection(controlSEC);
         
         // Declaring in Submodel "Control" - an Operation "ChangeManufacturerName".
         OperationVariable manuNameInp = new OperationVariable("ManufacturerName",
         								ValueType.String);
         Operation changeManuName = new Operation("changeManufacturerName");
         changeManuName.setInputVariable(manuNameInp);
         controlSubModel.setOperation(changeManuName);
         
         // Declaring in Submodel "Control" - an Operation "Status".
         OperationVariable statusOut = new OperationVariable("Status", 
         								ValueType.Boolean);
         Operation statusCheck = new Operation("checkStatus");
         statusCheck.setOutputVariable(statusOut);
         controlSubModel.setOperation(statusCheck);
         // ---------------------------------------------------------------------
         
         // Populating the Submodel - "Calculation".
         OperationVariable operand1 = new OperationVariable("Operand1", ValueType.Integer);
         OperationVariable operand2 = new OperationVariable("Operand2", ValueType.Integer);
         OperationVariable result = new OperationVariable("Result", ValueType.Integer);
         
         // Adding Operations. 
         Operation add = new Operation("Add");
         add.setSemanticDescription(addConcept);
         
//         add.setSemanticIdentifier(xyz);
         
         add.setKind(ModelingKind.INSTANCE);
         add.setInputVariables(operand1, operand2);
         add.setOutputVariable(result);
         calculationSubModel.setOperation(add);
         
         Operation sub = new Operation("Subtract");
         sub.setSemanticDescription(subtractConcept);
         sub.setKind(ModelingKind.INSTANCE);
         sub.setInputVariables(operand1, operand2);
         sub.setOutputVariable(result);
         calculationSubModel.setOperation(sub);
         
         Operation multiply = new Operation("Multiply");
         multiply.setSemanticDescription(multiplyConcept);
         multiply.setKind(ModelingKind.INSTANCE);
         multiply.setInputVariables(operand1, operand2);
         multiply.setOutputVariable(result);
         calculationSubModel.setOperation(multiply);
         
         Operation divide = new Operation("Divide");
         divide.setSemanticDescription(divideConcept);
         divide.setKind(ModelingKind.INSTANCE);
         divide.setInputVariables(operand1, operand2);
         divide.setOutputVariable(result);
         calculationSubModel.setOperation(divide);
         
         SubModelElementCollection calSEC = new SubModelElementCollection("CalculationCollection");
         calSEC.setAllowDuplicates(false);
         calSEC.setOrdered(true);
         calSEC.setDynamic(true);
         calSEC.setKind(ModelingKind.INSTANCE);
         calculationSubModel.setSubModelElementCollection(calSEC);
//         System.out.println("Parent of Example: " + calSEC.getParentSEC().getIdShort());
         
         SubModelElementCollection trialSEC = new SubModelElementCollection("DETrialCollection");
         trialSEC.setAllowDuplicates(false);
         trialSEC.setOrdered(true);
         trialSEC.setDynamic(true);
         trialSEC.setKind(ModelingKind.INSTANCE);
         calSEC.setSubModelElementCollection(trialSEC);
         
         SubModelElementCollection trialSEC1 = new SubModelElementCollection("DETrial1Collection");
         trialSEC1.setAllowDuplicates(false);
         trialSEC1.setOrdered(true);
         trialSEC1.setDynamic(true);
         trialSEC1.setKind(ModelingKind.INSTANCE);
         trialSEC.setSubModelElementCollection(trialSEC1);
         
         
         Asset calculatorAsset = new Asset("Calculator", AssetKind.INSTANCE);
         calculatorAsset.setEndpoints(assetEndpoint1, assetEndpoint2);
         
         Identifier idAsset = new DefaultIdentifier();
         idAsset.setIdType(IdentifierType.IRI);
         idAsset.setIdentifier("https://www.dfki.de/ids/assets/15c9c860-34f3-4fc3-a292-66fd0442c1f9");
         calculatorAsset.setIdentification(idAsset);
         
         
         AASEndpoint aasEndpoint = new AASEndpoint("localhost", 2000);
         
         // Creating AssetAdministrationShell.
         AssetAdministrationShell Papyrus4Manufacturing = new AssetAdministrationShell("Papyrus4Manufacturing",
         									aasEndpoint);
         Papyrus4Manufacturing.setSubModels(documentation, controlSubModel, calculationSubModel);
         
         // Adding the ConceptDictionary to Asset Administration Shell
         Papyrus4Manufacturing.setConceptDictionary(conceptDict);
         
         Identifier idAAS = new DefaultIdentifier();
         idAAS.setIdType(IdentifierType.IRI);
         idAAS.setIdentifier("https://www.dfki.de/Papyrus4Manufacturingworkshop/ids/aas/"
 				+ "23012dfa-0c1c-4408-bd06-dc3d9dcfcf4f");
         Papyrus4Manufacturing.setIdentification(idAAS);
         
         // Setting the AAS for the Asset.
         Papyrus4Manufacturing.setAsset(calculatorAsset);
         
         // Initializing the Generator.
         Project testAAS = new Project(Papyrus4Manufacturing);
         testAAS.setProjectName("firstTrial");
         testAAS.setNamespaceFromProjectName();
         testAAS.setProjectFolder("/home/tapanta/Documents/tuleap-eclipse-workspace/Fifth_Trial/");
         testAAS.createProject();
    }
    
    /**
     * Copied from test5()
     * 
     * Generating the Tests for different ValueType mappings BaSyx -> Java Types. 
  	 *
     */
    public static void test6() {
    	
        Endpoint assetEndpoint1 = new Endpoint("CalculatorServer", 
				ProtocolKind.OPCUA, "opc.tcp://127.0.0.1:4840/");

        Endpoint assetEndpoint2 = new Endpoint("MinimalServer", 
				ProtocolKind.OPCUA, "opc.tcp://127.0.0.1:4841/");
         
         // Initializing ConceptDictionary
         ConceptDictionary conceptDict = new ConceptDictionary();
         
         // Creating ConceptDescriptions 
         // "Documentation"
         Identifier idDocumentation = new DefaultIdentifier();
         idDocumentation.setIdType(IdentifierType.IRI);
         idDocumentation.setIdentifier("https://www.dfki.de/ids/documentation/79c44263-30ad-4296-a51e-2df3a4aed4ae");
 
         ConceptDescription documentationConcept = new ConceptDescription("Documentation", idDocumentation);
         conceptDict.setConceptDescription(documentationConcept); 
         
         // "Add"
         Identifier idAdd = new DefaultIdentifier();
         idAdd.setIdType(IdentifierType.IRI);
         idAdd.setIdentifier("https://www.dfki.de/ids/add/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
 
         ConceptDescription addConcept = new ConceptDescription("Add", idAdd);
         addConcept.setCategory(Category.FUNCTION);
         conceptDict.setConceptDescription(addConcept); 
         
         // "Subtract"
         Identifier idSub = new DefaultIdentifier();
         idSub.setIdType(IdentifierType.IRI);
         idSub.setIdentifier("https://www.dfki.de/ids/sub/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
 
         ConceptDescription subtractConcept = new ConceptDescription("Subtract", idSub);
         subtractConcept.setCategory(Category.FUNCTION);
         conceptDict.setConceptDescription(subtractConcept); 
         
         // "Multiply"
         Identifier idMul = new DefaultIdentifier();
         idMul.setIdType(IdentifierType.IRI);
         idMul.setIdentifier("https://www.dfki.de/ids/multiply/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
 
         ConceptDescription multiplyConcept = new ConceptDescription("Multiply", idMul);
         multiplyConcept.setCategory(Category.FUNCTION);
         conceptDict.setConceptDescription(multiplyConcept); 
         
         // "Divide"
         Identifier idDivide = new DefaultIdentifier();
         idDivide.setIdType(IdentifierType.IRI);
         idDivide.setIdentifier("https://www.dfki.de/ids/divide/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
 
         ConceptDescription divideConcept = new ConceptDescription("Divide", idDivide);
         divideConcept.setCategory(Category.FUNCTION);
         conceptDict.setConceptDescription(divideConcept); 
         
/*1.*/    /*
          * Trying out ConceptDescription with:
          * 	-  One Reference in isCaseOf
          * 	- One Key in this one reference.
          */
         Identifier idTrial = new DefaultIdentifier();
         idTrial.setIdType(IdentifierType.IRDI);
         idTrial.setIdentifier("565#//256641//45//pa8");
         
         ConceptDescription trialConcept = new ConceptDescription("Trial", idTrial);
         Key trialKey = new Key(KeyElements.PROPERTY, true, 
  				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
         Reference trialRef = new Reference(trialKey);
         trialConcept.setIsCaseOf(trialRef);
         conceptDict.setConceptDescription(trialConcept);
         
/*2.*/    /*
          * Trying out ConceptDescription with:
          * 	-  One Reference in isCaseOf
          * 	- Multiple Keys in this one reference.
          */
//         Identifier idTrial1 = new DefaultIdentifier();
//         idTrial1.setIdType(IdentifierType.IRDI);
//         idTrial1.setIdentifier("565#//256641//45//pa8");
//         
//         ConceptDescription trial1Concept = new ConceptDescription("Trial1", idTrial1);
//         Key trial1Key1 = new Key(KeyElements.PROPERTY, true, 
//  				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
//         Key trial1Key2 = new Key(KeyElements.SUBMODEL_ELEMENT, true, 
//  				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
//
//         Reference trial1Ref = new Reference(trial1Key1);
//         trial1Ref.setKey(trial1Key2);
//         trial1Concept.setIsCaseOf(trial1Ref);
//         conceptDict.setConceptDescription(trial1Concept);
         
/*3.*/    /*
          * Trying out ConceptDescription with:
          * 	-  Multiple Reference in isCaseOf
          * 	- Multiple Keys in these multiple references.
          */
//         Identifier idTrial2 = new DefaultIdentifier();
//         idTrial2.setIdType(IdentifierType.IRDI);
//         idTrial2.setIdentifier("003#//256641//45//pa8");
//         
//         ConceptDescription trial2Concept = new ConceptDescription("Trial2", idTrial2);
//         Key trial2Key1 = new Key(KeyElements.PROPERTY, true, 
//  				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
//         Key trial2Key2 = new Key(KeyElements.SUBMODEL_ELEMENT, true, 
//  				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
//
//         Reference trial2Ref1 = new Reference(trial2Key1);
//         trial2Ref1.setKey(trial2Key2);
//         
//         Key trial2Key3 = new Key(KeyElements.OPERATION, true, 
//   				"0112/2///61360_4#AASDF891#001", KeyType.IRDI);
//         
//         Key trial2Key4 = new Key(KeyElements.SUBMODEL_ELEMENT_COLLECTION, true, 
//   				"0112/2///61360_4sdf#AAF891#001", KeyType.IRDI);
//         
//         Reference trial2Ref2 = new Reference(trial2Key3);
//         trial2Ref2.setKey(trial2Key4);
//         
//         trial2Concept.setIsCaseOf(trial2Ref1, trial2Ref2);
//         conceptDict.setConceptDescription(trial2Concept);


     	
     	 // Declaring the 3 Submodels.
     	 SubModel documentation = new SubModel("Documentation");
     	 SubModel controlSubModel = new SubModel("Control");
     	 SubModel calculationSubModel = new SubModel("Calculation");
     	
     	 // Setting the Identifier Details of all these SubModels. 
     	 // "Documentation"
     	 Identifier idDoc = new DefaultIdentifier();
         idDoc.setIdType(IdentifierType.IRDI);
//         idDoc.setIdentifier("https://www.dfki.de/ids/sub/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
         idDoc.setIdentifier("0112/2///61360_4#AAF891#002");
         documentation.setIdentification(idDoc);
          
         // "Control"
         Identifier idControl = new DefaultIdentifier();
         idControl.setIdType(IdentifierType.IRDI);
//         idControl.setIdentifier("https://www.dfki.de/ids/sub/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
         idControl.setIdentifier("0112/2///61360_4#AAF891#002");
         controlSubModel.setIdentification(idControl);
          
         // "Calculation"
         Identifier idCalculation = new DefaultIdentifier();
         idCalculation.setIdType(IdentifierType.IRDI);
//         idCalculation.setIdentifier("https://www.dfki.de/ids/sub/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
         idCalculation.setIdentifier("0112/2///61360_4#AAF891#002");
         calculationSubModel.setIdentification(idCalculation);
         
         // Setting ModelingKind of the SubModels.
         documentation.setKind(ModelingKind.TEMPLATE);
         controlSubModel.setKind(ModelingKind.TEMPLATE);
         calculationSubModel.setKind(ModelingKind.TEMPLATE);
         
         // Setting Semantic Ids for SubModels. 
         Key docKey = new Key(KeyElements.CONCEPT_DESCRIPTION, true, 
         				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
         Reference docRef = new Reference(docKey);
         documentation.setSemanticIdentifier(docRef);
         
         Key controlKey = new Key(KeyElements.CONCEPT_DESCRIPTION, true, 
 				"0112/2///61360_4#AAF891#002", KeyType.IRDI);
         Reference controlRef = new Reference(controlKey);
         controlSubModel.setSemanticIdentifier(controlRef);
         
         Key calculationKey = new Key(KeyElements.CONCEPT_DESCRIPTION, true, 
 				"0112/2///61360_4#AAF891#002", KeyType.IRDI);
         Reference calculationRef = new Reference(calculationKey);
         calculationSubModel.setSemanticIdentifier(calculationRef);

         // Adding a File to the Submodel - "Documentation"
         File exampleFile = new File();
         exampleFile.setIdShort("exampleFile");
         exampleFile.setKind(ModelingKind.INSTANCE);
         exampleFile.setMimeType("audio/aac");
         exampleFile.setValue("example.com/myexample");
         documentation.setFile(exampleFile);
         
         // Populating the Submodel - "Documentation"
         MultiLanguageProperty manufacName = new MultiLanguageProperty("ManufacturerName");
         manufacName.setKind(ModelingKind.INSTANCE);
         manufacName.setValue("en", "Smartfactory KL");
         
         Key manufacKey = new Key(KeyElements.GLOBAL_REFERENCE, false, "0112/2///61360_4#MYNAME891#005", KeyType.IRDI);
         Reference manufNameRef = new Reference(manufacKey);
         manufacName.setSemanticIdentifier(manufNameRef);
         
         documentation.setMultiLanguageProperty(manufacName);
         
         MultiLanguageProperty prodName = new MultiLanguageProperty("ManufacturerProductDesignation");
         prodName.setKind(ModelingKind.INSTANCE);
         prodName.setValue("de", "Rechner");
         prodName.setValue("en", "Calculator");
         prodName.setValue("fr", "Calculatrice");
         documentation.setMultiLanguageProperty(prodName);
         
         
         SubModelElementCollection addressSEC = new SubModelElementCollection("Address");
         documentation.setSubModelElementCollection(addressSEC);
         
         //Adding to the parent SEC "Address" - "Phone". 
         
         SubModelElementCollection phoneSEC = new SubModelElementCollection("Phone");
         addressSEC.setSubModelElementCollection(phoneSEC);
         
         MultiLanguageProperty telephoneNumer = new MultiLanguageProperty("TelephoneNumber");
         phoneSEC.setMultiLanguageProperty(telephoneNumer);
         
         Property typeTel = new Property("TypeOfTelephone");
         phoneSEC.setProperty(typeTel);
         
         
         // ---------------------------------------------------------------------
         
         // Populating the Submodel - "Control".
         SubModelElementCollection controlSEC = new SubModelElementCollection("ControlParameters");
         controlSEC.setAllowDuplicates(false);
         controlSEC.setOrdered(true);
         controlSEC.setKind(ModelingKind.INSTANCE);
         
         // Creating a demo SubModelElementCollection - "Example" & adding it to the SEC - "ControlParameters".
         SubModelElementCollection exampleSEC = new SubModelElementCollection("Example");
         exampleSEC.setAllowDuplicates(false);
         exampleSEC.setOrdered(true);
         exampleSEC.setKind(ModelingKind.INSTANCE);
         controlSEC.setSubModelElementCollection(exampleSEC);
         System.out.println("Parent of Example: " + exampleSEC.getParentSEC().getIdShort());
         
         // Adding a File to the SEC - "Example"
         File anotherFile = new File();
         anotherFile.setIdShort("anotherFile");
         anotherFile.setKind(ModelingKind.INSTANCE);
         anotherFile.setMimeType("audio/aac");
         anotherFile.setValue("another.com/myanotherexample");
         anotherFile.setDynamic(true);
         exampleSEC.setFile(anotherFile);
         
         Property opVolt = new Property("OperatingVoltage");
         opVolt.setValueType(ValueType.Integer);
         opVolt.setDynamic(true);
         opVolt.setValue(5);
         controlSEC.setProperty(opVolt);
         
         Property unit = new Property("Unit");
         unit.setValueType(ValueType.String);
         unit.setKind(ModelingKind.INSTANCE);
         unit.setValue("Volts");
         controlSEC.setProperty(unit);
         
         Property opTemp = new Property("OperatingTemperature");
         opTemp.setValueType(ValueType.Integer);
         opTemp.setDynamic(true);
         opTemp.setEndpoint(assetEndpoint1);
         opTemp.setNameSpaceIndex(2);
         opTemp.setIdentifier(5);
         controlSubModel.setProperty(opTemp);
         
         // Adding the SubModelelementCollection to the Submodel.
         controlSubModel.setSubModelElementCollection(controlSEC);
         
         // Declaring in Submodel "Control" - an Operation "ChangeManufacturerName".
         OperationVariable manuNameInp = new OperationVariable("ManufacturerName",
         								ValueType.String);
         Operation changeManuName = new Operation("changeManufacturerName");
         changeManuName.setInputVariable(manuNameInp);
//         controlSubModel.setOperation(changeManuName);
         exampleSEC.setOperation(changeManuName);
         
         // Declaring in Submodel "Control" - an Operation "Status".
         OperationVariable statusOut = new OperationVariable("Status", 
         								ValueType.Boolean);
         Operation statusCheck = new Operation("checkStatus");
         statusCheck.setOutputVariable(statusOut);
         controlSEC.setOperation(statusCheck);
         
         // ------------- Test Area for Variable Types Mapping. --------------
         // Declaring in Submodel "Control" - an Operation "Testing Variable Type".
         OperationVariable test1Input1 = new OperationVariable("Test1Input1", ValueType.Int8);
         OperationVariable test1Input2 = new OperationVariable("Test1Input2", ValueType.Int16);
         OperationVariable resultTest1 = new OperationVariable("ResultTest1", ValueType.UInt8);
         
         Operation test1Check1 = new Operation("Test1Check");
         test1Check1.setInputVariable(test1Input1);
         test1Check1.setInputVariable(test1Input2);
         test1Check1.setOutputVariable(resultTest1);
         controlSubModel.setOperation(test1Check1);
        
         OperationVariable test2Input1 = new OperationVariable("Test2Input1", ValueType.NonNegativeInteger);
         OperationVariable test2Input2 = new OperationVariable("TestInput2", ValueType.NonPositiveInteger);
         OperationVariable resultTest2 = new OperationVariable("ResultTest", ValueType.PositiveInteger);
         
         Operation test2Check1 = new Operation("Test2Check");
         test2Check1.setInputVariable(test2Input1);
         test2Check1.setInputVariable(test2Input2);
         test2Check1.setOutputVariable(resultTest2);
         controlSubModel.setOperation(test2Check1);

         
         // ---------------------------------------------------------------------
         
         // Populating the Submodel - "Calculation".
         OperationVariable operand1 = new OperationVariable("Operand1", ValueType.Integer);
         OperationVariable operand2 = new OperationVariable("Operand2", ValueType.Integer);
         OperationVariable result = new OperationVariable("Result", ValueType.Integer);
         
         // Adding Operations. 
         Operation add = new Operation("Add");
         add.setSemanticDescription(addConcept);
         
//         add.setSemanticIdentifier(xyz);
         
         add.setKind(ModelingKind.INSTANCE);
         add.setInputVariables(operand1, operand2);
         add.setOutputVariable(result);
         calculationSubModel.setOperation(add);
         
         Operation sub = new Operation("Subtract");
         sub.setSemanticDescription(subtractConcept);
         sub.setKind(ModelingKind.INSTANCE);
         sub.setInputVariables(operand1, operand2);
         sub.setOutputVariable(result);
         calculationSubModel.setOperation(sub);
         
         Operation multiply = new Operation("Multiply");
         multiply.setSemanticDescription(multiplyConcept);
         multiply.setKind(ModelingKind.INSTANCE);
         multiply.setInputVariables(operand1, operand2);
         multiply.setOutputVariable(result);
         calculationSubModel.setOperation(multiply);
         
         Operation divide = new Operation("Divide");
         divide.setSemanticDescription(divideConcept);
         divide.setKind(ModelingKind.INSTANCE);
         divide.setInputVariables(operand1, operand2);
         divide.setOutputVariable(result);
         calculationSubModel.setOperation(divide);
         
         SubModelElementCollection calSEC = new SubModelElementCollection("CalculationCollection");
         calSEC.setAllowDuplicates(false);
         calSEC.setOrdered(true);
         calSEC.setDynamic(true);
         calSEC.setKind(ModelingKind.INSTANCE);
         calculationSubModel.setSubModelElementCollection(calSEC);
//         System.out.println("Parent of Example: " + calSEC.getParentSEC().getIdShort());
         
         SubModelElementCollection trialSEC = new SubModelElementCollection("DETrialCollection");
         trialSEC.setAllowDuplicates(false);
         trialSEC.setOrdered(true);
         trialSEC.setDynamic(false);
         trialSEC.setKind(ModelingKind.INSTANCE);
         calSEC.setSubModelElementCollection(trialSEC);
         
         SubModelElementCollection trialSEC1 = new SubModelElementCollection("DETrial1Collection");
         trialSEC1.setAllowDuplicates(false);
         trialSEC1.setOrdered(true);
         trialSEC1.setDynamic(false);
         trialSEC1.setKind(ModelingKind.INSTANCE);
         trialSEC.setSubModelElementCollection(trialSEC1);
         
         
         Asset calculatorAsset = new Asset("Calculator", AssetKind.INSTANCE);
         calculatorAsset.setEndpoints(assetEndpoint1, assetEndpoint2);
         
         Identifier idAsset = new DefaultIdentifier();
         idAsset.setIdType(IdentifierType.IRI);
         idAsset.setIdentifier("https://www.dfki.de/ids/assets/15c9c860-34f3-4fc3-a292-66fd0442c1f9");
         calculatorAsset.setIdentification(idAsset);
         
         
         AASEndpoint aasEndpoint = new AASEndpoint("localhost", 2000);
         
         // Creating AssetAdministrationShell.
         AssetAdministrationShell Papyrus4Manufacturing = new AssetAdministrationShell("Papyrus4Manufacturing",
         									aasEndpoint);
         Papyrus4Manufacturing.setSubModels(documentation, controlSubModel, calculationSubModel);
         
         // Adding the ConceptDictionary to Asset Administration Shell
         Papyrus4Manufacturing.setConceptDictionary(conceptDict);
         
         Identifier idAAS = new DefaultIdentifier();
         idAAS.setIdType(IdentifierType.IRI);
         idAAS.setIdentifier("https://www.dfki.de/Papyrus4Manufacturingworkshop/ids/aas/"
 				+ "23012dfa-0c1c-4408-bd06-dc3d9dcfcf4f");
         Papyrus4Manufacturing.setIdentification(idAAS);
         
         // Setting the AAS for the Asset.
         Papyrus4Manufacturing.setAsset(calculatorAsset);
         
         // Initializing the Generator.
         Project testAAS = new Project(Papyrus4Manufacturing);
         testAAS.setProjectName("AnotherQualifiedTrial");
         testAAS.setNamespaceFromProjectName();
         testAAS.setProjectFolder("/home/tapanta/Documents/tuleap-eclipse-workspace/AnotherQualified_Trial/");
         testAAS.createProject();
    }
    
    /**
     * Testing out the UTF-8 encoding. 
  	 *
     */
    public static void test7() {
    	
        Endpoint assetEndpoint1 = new Endpoint("CalculatorServer", 
				ProtocolKind.OPCUA, "opc.tcp://127.0.0.1:4840/");

        Endpoint assetEndpoint2 = new Endpoint("MinimalServer", 
				ProtocolKind.OPCUA, "opc.tcp://127.0.0.1:4841/");
         
         // Initializing ConceptDictionary
         ConceptDictionary conceptDict = new ConceptDictionary();
         
         // Creating ConceptDescriptions 
         // "Documentation"
         Identifier idDocumentation = new DefaultIdentifier();
         idDocumentation.setIdType(IdentifierType.IRI);
         idDocumentation.setIdentifier("https://www.dfki.de/ids/documentation/79c44263-30ad-4296-a51e-2df3a4aed4ae");
 
         ConceptDescription documentationConcept = new ConceptDescription("Documentation", idDocumentation);
         conceptDict.setConceptDescription(documentationConcept); 
         
         // "Add"
         Identifier idAdd = new DefaultIdentifier();
         idAdd.setIdType(IdentifierType.IRI);
         idAdd.setIdentifier("https://www.dfki.de/ids/add/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
 
         ConceptDescription addConcept = new ConceptDescription("Add", idAdd);
         addConcept.setCategory(Category.FUNCTION);
         conceptDict.setConceptDescription(addConcept); 
         
         // "Subtract"
         Identifier idSub = new DefaultIdentifier();
         idSub.setIdType(IdentifierType.IRI);
         idSub.setIdentifier("https://www.dfki.de/ids/sub/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
 
         ConceptDescription subtractConcept = new ConceptDescription("Subtract", idSub);
         subtractConcept.setCategory(Category.FUNCTION);
         conceptDict.setConceptDescription(subtractConcept); 
         
         // "Multiply"
         Identifier idMul = new DefaultIdentifier();
         idMul.setIdType(IdentifierType.IRI);
         idMul.setIdentifier("https://www.dfki.de/ids/multiply/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
 
         ConceptDescription multiplyConcept = new ConceptDescription("Multiply", idMul);
         multiplyConcept.setCategory(Category.FUNCTION);
         conceptDict.setConceptDescription(multiplyConcept); 
         
         // "Divide"
         Identifier idDivide = new DefaultIdentifier();
         idDivide.setIdType(IdentifierType.IRI);
         idDivide.setIdentifier("https://www.dfki.de/ids/divide/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
 
         ConceptDescription divideConcept = new ConceptDescription("Divide", idDivide);
         divideConcept.setCategory(Category.FUNCTION);
         conceptDict.setConceptDescription(divideConcept); 
         
/*1.*/    /*
          * Trying out ConceptDescription with:
          * 	-  One Reference in isCaseOf
          * 	- One Key in this one reference.
          */
         Identifier idTrial = new DefaultIdentifier();
         idTrial.setIdType(IdentifierType.IRDI);
         idTrial.setIdentifier("565#//256641//45//pa8");
         
         ConceptDescription trialConcept = new ConceptDescription("Trial", idTrial);
         Key trialKey = new Key(KeyElements.PROPERTY, true, 
  				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
         Reference trialRef = new Reference(trialKey);
         trialConcept.setIsCaseOf(trialRef);
         conceptDict.setConceptDescription(trialConcept);
         
/*2.*/    /*
          * Trying out ConceptDescription with:
          * 	-  One Reference in isCaseOf
          * 	- Multiple Keys in this one reference.
          */
//         Identifier idTrial1 = new DefaultIdentifier();
//         idTrial1.setIdType(IdentifierType.IRDI);
//         idTrial1.setIdentifier("565#//256641//45//pa8");
//         
//         ConceptDescription trial1Concept = new ConceptDescription("Trial1", idTrial1);
//         Key trial1Key1 = new Key(KeyElements.PROPERTY, true, 
//  				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
//         Key trial1Key2 = new Key(KeyElements.SUBMODEL_ELEMENT, true, 
//  				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
//
//         Reference trial1Ref = new Reference(trial1Key1);
//         trial1Ref.setKey(trial1Key2);
//         trial1Concept.setIsCaseOf(trial1Ref);
//         conceptDict.setConceptDescription(trial1Concept);
         
/*3.*/    /*
          * Trying out ConceptDescription with:
          * 	-  Multiple Reference in isCaseOf
          * 	- Multiple Keys in these multiple references.
          */
//         Identifier idTrial2 = new DefaultIdentifier();
//         idTrial2.setIdType(IdentifierType.IRDI);
//         idTrial2.setIdentifier("003#//256641//45//pa8");
//         
//         ConceptDescription trial2Concept = new ConceptDescription("Trial2", idTrial2);
//         Key trial2Key1 = new Key(KeyElements.PROPERTY, true, 
//  				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
//         Key trial2Key2 = new Key(KeyElements.SUBMODEL_ELEMENT, true, 
//  				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
//
//         Reference trial2Ref1 = new Reference(trial2Key1);
//         trial2Ref1.setKey(trial2Key2);
//         
//         Key trial2Key3 = new Key(KeyElements.OPERATION, true, 
//   				"0112/2///61360_4#AASDF891#001", KeyType.IRDI);
//         
//         Key trial2Key4 = new Key(KeyElements.SUBMODEL_ELEMENT_COLLECTION, true, 
//   				"0112/2///61360_4sdf#AAF891#001", KeyType.IRDI);
//         
//         Reference trial2Ref2 = new Reference(trial2Key3);
//         trial2Ref2.setKey(trial2Key4);
//         
//         trial2Concept.setIsCaseOf(trial2Ref1, trial2Ref2);
//         conceptDict.setConceptDescription(trial2Concept);


     	
     	 // Declaring the 3 Submodels.
     	 SubModel documentation = new SubModel("Documentation");
     	 SubModel controlSubModel = new SubModel("Control");
     	 SubModel calculationSubModel = new SubModel("Calculation");
     	
     	 // Setting the Identifier Details of all these SubModels. 
     	 // "Documentation"
     	 Identifier idDoc = new DefaultIdentifier();
         idDoc.setIdType(IdentifierType.IRDI);
//         idDoc.setIdentifier("https://www.dfki.de/ids/sub/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
         idDoc.setIdentifier("0112/2///61360_4#AAF891#002");
         documentation.setIdentification(idDoc);
          
         // "Control"
         Identifier idControl = new DefaultIdentifier();
         idControl.setIdType(IdentifierType.IRDI);
//         idControl.setIdentifier("https://www.dfki.de/ids/sub/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
         idControl.setIdentifier("0112/2///61360_4#AAF891#002");
         controlSubModel.setIdentification(idControl);
          
         // "Calculation"
         Identifier idCalculation = new DefaultIdentifier();
         idCalculation.setIdType(IdentifierType.IRDI);
//         idCalculation.setIdentifier("https://www.dfki.de/ids/sub/79c44263-30ad-4296-a51e-2df3a4tbc4ae");
         idCalculation.setIdentifier("0112/2///61360_4#AAF891#002");
         calculationSubModel.setIdentification(idCalculation);
         
         // Setting ModelingKind of the SubModels.
         documentation.setKind(ModelingKind.TEMPLATE);
         controlSubModel.setKind(ModelingKind.TEMPLATE);
         calculationSubModel.setKind(ModelingKind.TEMPLATE);
         
         // Setting Semantic Ids for SubModels. 
         Key docKey = new Key(KeyElements.CONCEPT_DESCRIPTION, true, 
         				"0112/2///61360_4#AAF891#001", KeyType.IRDI);
         Reference docRef = new Reference(docKey);
         documentation.setSemanticIdentifier(docRef);
         
         Key controlKey = new Key(KeyElements.CONCEPT_DESCRIPTION, true, 
 				"0112/2///61360_4#AAF891#002", KeyType.IRDI);
         Reference controlRef = new Reference(controlKey);
         controlSubModel.setSemanticIdentifier(controlRef);
         
         Key calculationKey = new Key(KeyElements.CONCEPT_DESCRIPTION, true, 
 				"0112/2///61360_4#AAF891#002", KeyType.IRDI);
         Reference calculationRef = new Reference(calculationKey);
         calculationSubModel.setSemanticIdentifier(calculationRef);

         // Adding a File to the Submodel - "Documentation"
         File exampleFile = new File();
         exampleFile.setIdShort("exampleFile");
         exampleFile.setKind(ModelingKind.INSTANCE);
         exampleFile.setMimeType("audio/aac");
         exampleFile.setValue("example.com/myexample");
         documentation.setFile(exampleFile);
         
         // Populating the Submodel - "Documentation"
         MultiLanguageProperty manufacName = new MultiLanguageProperty("ManufacturerName");
         manufacName.setKind(ModelingKind.INSTANCE);
         manufacName.setValue("en", "Smartfactory KL");
         
         Key manufacKey = new Key(KeyElements.GLOBAL_REFERENCE, false, "0112/2///61360_4#MYNAME891#005", KeyType.IRDI);
         Reference manufNameRef = new Reference(manufacKey);
         manufacName.setSemanticIdentifier(manufNameRef);
         
         documentation.setMultiLanguageProperty(manufacName);
         
         MultiLanguageProperty prodName = new MultiLanguageProperty("ManufacturerProductDesignation");
         prodName.setKind(ModelingKind.INSTANCE);
         prodName.setValue("de", "Rechner");
         prodName.setValue("en", "Calculator");
         prodName.setValue("fr", "Calculatrice");
         prodName.setValue("ch", "电脑");
         prodName.setValue("ben", "কম্পিউটার");
         documentation.setMultiLanguageProperty(prodName);
         
         
         SubModelElementCollection addressSEC = new SubModelElementCollection("Address");
         documentation.setSubModelElementCollection(addressSEC);
         
         //Adding to the parent SEC "Address" - "Phone". 
         
         SubModelElementCollection phoneSEC = new SubModelElementCollection("Phone");
         addressSEC.setSubModelElementCollection(phoneSEC);
         
         MultiLanguageProperty telephoneNumer = new MultiLanguageProperty("TelephoneNumber");
         phoneSEC.setMultiLanguageProperty(telephoneNumer);
         
         Property typeTel = new Property("TypeOfTelephone");
         phoneSEC.setProperty(typeTel);
         
         
         // ---------------------------------------------------------------------
         
         // Populating the Submodel - "Control".
         SubModelElementCollection controlSEC = new SubModelElementCollection("ControlParameters");
         controlSEC.setAllowDuplicates(false);
         controlSEC.setOrdered(true);
         controlSEC.setKind(ModelingKind.INSTANCE);
         
         // Creating a demo SubModelElementCollection - "Example" & adding it to the SEC - "ControlParameters".
         SubModelElementCollection exampleSEC = new SubModelElementCollection("Example");
         exampleSEC.setAllowDuplicates(false);
         exampleSEC.setOrdered(true);
         exampleSEC.setKind(ModelingKind.INSTANCE);
         controlSEC.setSubModelElementCollection(exampleSEC);
         System.out.println("Parent of Example: " + exampleSEC.getParentSEC().getIdShort());
         
         // Adding a File to the SEC - "Example"
         File anotherFile = new File();
         anotherFile.setIdShort("anotherFile");
         anotherFile.setKind(ModelingKind.INSTANCE);
         anotherFile.setMimeType("audio/aac");
         anotherFile.setValue("another.com/myanotherexample");
         anotherFile.setDynamic(true);
         exampleSEC.setFile(anotherFile);
         
         Property opVolt = new Property("OperatingVoltage");
         opVolt.setValueType(ValueType.Integer);
         opVolt.setDynamic(true);
         opVolt.setValue(5);
         controlSEC.setProperty(opVolt);
         
         Property unit = new Property("Unit");
         unit.setValueType(ValueType.String);
         unit.setKind(ModelingKind.INSTANCE);
         unit.setValue("Volts");
         controlSEC.setProperty(unit);
         
         Property opTemp = new Property("OperatingTemperature");
         opTemp.setValueType(ValueType.Integer);
         opTemp.setDynamic(true);
         opTemp.setEndpoint(assetEndpoint1);
         opTemp.setNameSpaceIndex(2);
         opTemp.setIdentifier(5);
         controlSubModel.setProperty(opTemp);
         
         // Adding the SubModelelementCollection to the Submodel.
         controlSubModel.setSubModelElementCollection(controlSEC);
         
         // Declaring in Submodel "Control" - an Operation "ChangeManufacturerName".
         OperationVariable manuNameInp = new OperationVariable("ManufacturerName",
         								ValueType.String);
         Operation changeManuName = new Operation("changeManufacturerName");
         changeManuName.setInputVariable(manuNameInp);
//         controlSubModel.setOperation(changeManuName);
         exampleSEC.setOperation(changeManuName);
         
         // Declaring in Submodel "Control" - an Operation "Status".
         OperationVariable statusOut = new OperationVariable("Status", 
         								ValueType.Boolean);
         Operation statusCheck = new Operation("checkStatus");
         statusCheck.setOutputVariable(statusOut);
         controlSEC.setOperation(statusCheck);
         
         // ------------- Test Area for Variable Types Mapping. --------------
         // Declaring in Submodel "Control" - an Operation "Testing Variable Type".
         OperationVariable test1Input1 = new OperationVariable("Test1Input1", ValueType.Int8);
         OperationVariable test1Input2 = new OperationVariable("Test1Input2", ValueType.Int16);
         OperationVariable resultTest1 = new OperationVariable("ResultTest1", ValueType.UInt8);
         
         Operation test1Check1 = new Operation("Test1Check");
         test1Check1.setInputVariable(test1Input1);
         test1Check1.setInputVariable(test1Input2);
         test1Check1.setOutputVariable(resultTest1);
         controlSubModel.setOperation(test1Check1);
        
         OperationVariable test2Input1 = new OperationVariable("Test2Input1", ValueType.NonNegativeInteger);
         OperationVariable test2Input2 = new OperationVariable("TestInput2", ValueType.NonPositiveInteger);
         OperationVariable resultTest2 = new OperationVariable("ResultTest", ValueType.PositiveInteger);
         
         Operation test2Check1 = new Operation("Test2Check");
         test2Check1.setInputVariable(test2Input1);
         test2Check1.setInputVariable(test2Input2);
         test2Check1.setOutputVariable(resultTest2);
         controlSubModel.setOperation(test2Check1);

         
         // ---------------------------------------------------------------------
         
         // Populating the Submodel - "Calculation".
         OperationVariable operand1 = new OperationVariable("Operand1", ValueType.Integer);
         OperationVariable operand2 = new OperationVariable("Operand2", ValueType.Integer);
         OperationVariable result = new OperationVariable("Result", ValueType.Integer);
         
         // Adding Operations. 
         Operation add = new Operation("Add");
         add.setSemanticDescription(addConcept);
         
//         add.setSemanticIdentifier(xyz);
         
         add.setKind(ModelingKind.INSTANCE);
         add.setInputVariables(operand1, operand2);
         add.setOutputVariable(result);
         calculationSubModel.setOperation(add);
         
         Operation sub = new Operation("Süüüüübtract");
         sub.setSemanticDescription(subtractConcept);
         sub.setKind(ModelingKind.INSTANCE);
         sub.setInputVariables(operand1, operand2);
         sub.setOutputVariable(result);
         calculationSubModel.setOperation(sub);
         
         Operation multiply = new Operation("Multiply");
         multiply.setSemanticDescription(multiplyConcept);
         multiply.setKind(ModelingKind.INSTANCE);
         multiply.setInputVariables(operand1, operand2);
         multiply.setOutputVariable(result);
         calculationSubModel.setOperation(multiply);
         
         Operation divide = new Operation("Diśide");
         divide.setSemanticDescription(divideConcept);
         divide.setKind(ModelingKind.INSTANCE);
         divide.setInputVariables(operand1, operand2);
         divide.setOutputVariable(result);
         calculationSubModel.setOperation(divide);
         
         Property testProp1 = new Property("TestPropertyQualified");
         testProp1.setValueType(ValueType.String);
         testProp1.setDynamic(true);
         testProp1.setEndpoint(assetEndpoint1);
         testProp1.setNameSpaceIndex(2);
         testProp1.setIdentifier(23);
         calculationSubModel.setProperty(testProp1);
         
         SubModelElementCollection calSEC = new SubModelElementCollection("CalculationCollection");
         calSEC.setAllowDuplicates(false);
         calSEC.setOrdered(true);
         calSEC.setDynamic(false);
         calSEC.setKind(ModelingKind.INSTANCE);
         calculationSubModel.setSubModelElementCollection(calSEC);
         
         Property calcProp1 = new Property("CalculationProperty1");
         calcProp1.setValueType(ValueType.String);
         calcProp1.setDynamic(true);
//         calcProp1.setEndpoint(assetEndpoint1);
//         calcProp1.setNameSpaceIndex(2);
//         calcProp1.setIdentifier(23);
         calSEC.setProperty(calcProp1);
//         System.out.println("Parent of Example: " + calSEC.getParentSEC().getIdShort());
         
         SubModelElementCollection trialSEC = new SubModelElementCollection("NestedSEC1");
         trialSEC.setAllowDuplicates(false);
         trialSEC.setOrdered(true);
         trialSEC.setDynamic(false);
         trialSEC.setKind(ModelingKind.INSTANCE);
         calSEC.setSubModelElementCollection(trialSEC);
         
         Property calcProp2 = new Property("CalculationProperty2");
         calcProp2.setValueType(ValueType.String);
         calcProp2.setDynamic(false);
//         calcProp2.setEndpoint(assetEndpoint1);
//         calcProp2.setNameSpaceIndex(2);
//         calcProp2.setIdentifier(23);
         trialSEC.setProperty(calcProp2);
         
         SubModelElementCollection trialSEC1 = new SubModelElementCollection("NestedSEC2");
         trialSEC1.setAllowDuplicates(false);
         trialSEC1.setOrdered(true);
         trialSEC1.setDynamic(false);
         trialSEC1.setKind(ModelingKind.INSTANCE);
         trialSEC.setSubModelElementCollection(trialSEC1);
         
         Property calcProp3 = new Property("CalculationProperty3");
         calcProp3.setValueType(ValueType.String);
         calcProp3.setDynamic(true);
         calcProp3.setEndpoint(assetEndpoint1);
         calcProp3.setNameSpaceIndex(2);
         calcProp3.setIdentifier(24);
         trialSEC1.setProperty(calcProp3);
         
         SubModelElementCollection trialSEC2 = new SubModelElementCollection("NestedSEC3");
         trialSEC2.setAllowDuplicates(false);
         trialSEC2.setOrdered(true);
         trialSEC2.setDynamic(false);
         trialSEC2.setKind(ModelingKind.INSTANCE);
         trialSEC1.setSubModelElementCollection(trialSEC2);
         
         
         Asset calculatorAsset = new Asset("Calculator", AssetKind.INSTANCE);
         calculatorAsset.setEndpoints(assetEndpoint1, assetEndpoint2);
         
         Identifier idAsset = new DefaultIdentifier();
         idAsset.setIdType(IdentifierType.IRI);
         idAsset.setIdentifier("https://www.dfki.de/ids/assets/15c9c860-34f3-4fc3-a292-66fd0442c1f9");
         calculatorAsset.setIdentification(idAsset);
         
         
         AASEndpoint aasEndpoint = new AASEndpoint("localhost", 2000);
         
         // Creating AssetAdministrationShell.
         AssetAdministrationShell canvaas = new AssetAdministrationShell("CanvAAS",
         									aasEndpoint);
         canvaas.setSubModels(documentation, controlSubModel, calculationSubModel);
         
         // Adding the ConceptDictionary to Asset Administration Shell
         canvaas.setConceptDictionary(conceptDict);
         
         Identifier idAAS = new DefaultIdentifier();
         idAAS.setIdType(IdentifierType.IRI);
         idAAS.setIdentifier("https://www.dfki.de/canvaasworkshop/ids/aas/"
 				+ "23012dfa-0c1c-4408-bd06-dc3d9dcfcf4f");
         canvaas.setIdentification(idAAS);
         
         // Setting the AAS for the Asset.
         canvaas.setAsset(calculatorAsset);
         
         // Initializing the Generator.
         Project testAAS = new Project(canvaas);
         testAAS.setProjectName("Test1_Bug579656");
         testAAS.setNamespaceFromProjectName();
         testAAS.setProjectFolder("/home/tapanta/Documents/eclipse-workspace/Test1_Bug579656/");
         testAAS.createProject();
    }
}
