/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.basyx.codegen.generator.submodel.submodelelements;

import java.util.List;

import org.eclipse.aas.api.reference.Key;
import org.eclipse.aas.api.submodel.parts.ConceptDescription;
import org.eclipse.aas.api.submodel.submodelelement.SubModelElementCollection;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.MultiLanguageProperty;
import org.eclipse.aas.basyx.codegen.generator.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.adminshell.aas.v3.model.LangString;

public class MLPGenerator {
	
	private static final Logger logger = LoggerFactory.getLogger(MLPGenerator.class);
	private MultiLanguageProperty mlpInstance;
	private List<Key> semanticKeys;
	private String parent;
	private ConceptDescription conceptDesc;
	
	/**
	 * Constructor for the MultiLanguagePropertyGenerator class. It receives a 
	 * single instance of MultiLanguageProperty to generate its respective and 
	 * necessary BaSyx Code. 
	 *  
	 * @param mlpInstance
	 */
	public MLPGenerator(MultiLanguageProperty mlpInstance) {
		
		this.mlpInstance = mlpInstance;
		
		try {

			this.semanticKeys = mlpInstance.getSemanticIdentifier().getKeys();
		
		}
		
		catch (NullPointerException e) {
			logger.error("Null Pointer Exception in Semantic ID Declaration "
					+ "while initialising " + this.getClass().getSimpleName());
		}
		
		try {
			this.conceptDesc = this.mlpInstance.getSemanticDescription();
		}
		
		catch (NullPointerException e) {
			logger.error("Null Pointer Exception while fetching ConceptDescription for " +
					"the semantic Id definition of the MultiLanguageProperty: " +
									this.getClass().getSimpleName());
		}
		
		// Checking for Parents.
		if (mlpInstance.getParentSEC()!=null) {
			this.parent = mlpInstance.getParentSEC().getIdShort();
		}
		
		else if (mlpInstance.getParentSub()!=null) {
			this.parent = mlpInstance.getParentSub().getIdShort();
		}
		
		else {
			logger.error("MultiLanguageProperty: " + mlpInstance.getIdShort() 
						+ "has no Parent defined");
		}
		
		logger.info("MLPGenerator Initialised for MLP : " + mlpInstance.getIdShort());

	}
	
	/**
	 * Generates a MultiLanguageProperty either in a Submodel or inside its 
	 * Parent SubModelElementCollection. 
	 * 
	 * This function generates:
	 * 
	 * -	Semantic Reference for the MultiLanguage Property in consideration. 
	 * -	Modeling Info for the MultiLanguage Property in consideration. 
	 * -	Parental Relation of the MultiLanguage Property. 
	 * 
	 * @return
	 */
	public String generateMultiLanguageProperty() {
		
		String multiLangPropText = ""
				+ "		MultiLanguageProperty " + parent + "_" + mlpInstance.getIdShort() + "= new MultiLanguageProperty();\r\n"
				+ "		" + parent + "_" + mlpInstance.getIdShort() + ".setIdShort(\"" + mlpInstance.getIdShort() +"\");\r\n" 
				+ generateSemanticReference()
				+ "		" + parent + "_" + mlpInstance.getIdShort() + ".setValue(LangStrings.fromStringPairs(" + generateLangStringContent() + "));\r\n"
				+ generateModelingInfo()
				+ generateParentalRelation()
				+ "\r\n\r\n";
		
		logger.info("MultiLanguageProperty Code generated for MultiLanguageProperty: " + 
				mlpInstance.getIdShort());
		
		return multiLangPropText;
		
		
	}
	
	/**
	 * Generates the Modeling Info for the MultiLanguageProperty. 
	 * 
	 * @return	Returns a String with generated code containing the Modeling Info. 
	 */
	private String generateModelingInfo() {
		
		String setKind = "";
		
		if (mlpInstance.getKind()!=null) {
			setKind = "		" + parent + "_" + mlpInstance.getIdShort() + 
					".setKind(ModelingKind." + mlpInstance.getKind() + ");\r\n";
			
			logger.info("Generated ModelingKind Info for Operation: " + 
					mlpInstance.getIdShort());
		}
		
		else {
			logger.info("Generated ModelingKind Info for Operation: " + 
					mlpInstance.getIdShort() + " not generated.");
		}
		
		return setKind;
	}
	
	/**
	 * Generates the LangString Content for the respective MultiLanguageProperty. 
	 * 
	 * @return	Returns a String with generated code containing the LangString 
	 * 			for the MultiLanguageProperty. 
	 */
	private String generateLangStringContent() {
		
		// Preparing the content for LangString. 
		List<LangString> langStringValues = mlpInstance.getValues();
		String langStringContent = "";
		
		for (LangString langStringValue : langStringValues) {
			langStringContent += "\"" + langStringValue.getLanguage() + "\", \"" 
								+ langStringValue.getValue() + "\", ";
		}
		// Stripping off the last comma and space. 
		if (langStringContent!=null && langStringContent.length() > 1) {
			langStringContent = langStringContent.substring(0, langStringContent.length()-2);			
		}
		
		else {
			//TODO: Find something to do 
		}
		
		if (!langStringContent.isEmpty()) {
			logger.info("LangString Content for " + mlpInstance.getIdShort() 
													+ " generated");
		}
		
		else {
			logger.debug("LangString Content for " + mlpInstance.getIdShort() 
													+ " not generated");
		}
		
		return langStringContent;
	}
	
	/**
	 * Generates the Parental Relation for the MultiLanguageProperty. 
	 * 
	 * @return	Returns a String with the generated code containing the parental
	 * 			information for the MultilanguageProperty.
	 */
	private String generateParentalRelation() {
		
		// Thread Check: To Understand who has called the function.
		// Test out this function of stackTrace.
		StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
		boolean isSubModelMultiLangProperty = true;
		
		if(stackTraceElements[3].getMethodName() == "generateSubModelElementCollection") {
			isSubModelMultiLangProperty = false;
		}
		
		// Defines where the MultiLanguageProperty belongs to. 
		String addMultiLangProperty = "";
		if (isSubModelMultiLangProperty) {
			addMultiLangProperty = "		addSubmodelElement(" 
									+ parent + "_" + mlpInstance.getIdShort() + ");\r\n";
			
			logger.info("MultiLanguageProperty " + mlpInstance.getIdShort() 
									+ " added to Submodel: " 
									+ mlpInstance.getParentSub().getIdShort());
		}
		
		else {
			SubModelElementCollection parentSEC = mlpInstance.getParentSEC();
			String immediateParent = "";
			
			// Checking for Parents.
			if (parentSEC.getParentSEC()!=null) {
				immediateParent = parentSEC.getParentSEC().getIdShort();
			}
			
			else if (parentSEC.getParentSub()!=null) {
				immediateParent = parentSEC.getParentSub().getIdShort();
			}
			
			else {
				logger.error("Property: " + parentSEC.getIdShort() 
				+ "has no Parent defined");
			}
			
			addMultiLangProperty = "		" + immediateParent + "_" + parentSEC.getIdShort() 
									+ "value.add(" + parent + "_" + mlpInstance.getIdShort() 
									+ ");\r\n\r\n"; 	 
		
			logger.info("MultiLanguageProperty " + mlpInstance.getIdShort() 
									+ " added to SubmodelElementCollection: " 
									+ mlpInstance.getParentSEC().getIdShort());
		}
		
		return addMultiLangProperty;
		
	}
	
	/**
	 * Generates the Semantic Reference for the MultiLanguageProperty in consideration. 
	 * 
	 * @return	Returns a String with generated code containing the semantic 
	 * 			references for the MultiLanguageProperty.
	 */
	private String generateSemanticReference() {
		
		// Adding the Semantic Reference.
		
		String semanticStr = "";	
		if (this.semanticKeys!=null && this.conceptDesc==null) {
			
			semanticStr += "		List<IKey> " + parent + "_" + mlpInstance.getIdShort().toLowerCase() 
					+ "Keys= new ArrayList<IKey>();\r\n";
			
			for (Key key : this.semanticKeys) {
				
				String basyxKeyElement = FileUtils.removeUnderScore(key.getType().toString());  
						
				semanticStr += "		" + parent + "_" + mlpInstance.getIdShort().toLowerCase() 
								+ "Keys.add(" + "new Key(KeyElements." + basyxKeyElement
								+ ", " + key.isLocal() + ", " + "\"" + key.getValue() + "\""
								+ ", " + "KeyType." + key.getIdType() + ")); \r\n";
			}
			
			semanticStr += "		Reference " + parent + "_" + mlpInstance.getIdShort() 
							+ "Ref = new Reference(" + parent + "_" + mlpInstance.getIdShort().toLowerCase() 
							+ "Keys" + ");\r\n"
							+ "		" + parent + "_" + mlpInstance.getIdShort() 
							+ ".setSemanticId(" + parent + "_" + mlpInstance.getIdShort() + "Ref); \r\n \r\n";
			
			logger.info("Semantic Id reference for Submodel: " + mlpInstance.getIdShort() + "generated.");
		}
		
		else if (this.conceptDesc!=null && this.semanticKeys==null) {
			
			semanticStr += "		" + parent + "_" + mlpInstance.getIdShort() 
						+  ".setSemanticId(conceptDescriptions." + 
							this.conceptDesc.getIdShort() + ".getReference()); \r\n \r\n";
			
		}
		
		else {
			
			logger.debug("No Semantic Id reference for Submodel: " + mlpInstance.getIdShort() + "found. Thus, not generated.");
			
		}
		
		return semanticStr;
	}

}

