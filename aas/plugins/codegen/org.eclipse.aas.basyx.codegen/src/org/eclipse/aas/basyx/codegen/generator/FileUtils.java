/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * 	   DFKI - Volkan Gezer <volkan.gezer@dfki.de>
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.basyx.codegen.generator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides static methods to perform file operations.
 *
 * @author Volkan, Tapanta
 *
 */

public class FileUtils {

	private static final Logger logger = LoggerFactory.getLogger(FileUtils.class);

	/**
	 * Removes file contents.
	 *
	 * @param fileName to clear
	 */
	public static void clearFile(final String fileName) {
		PrintWriter writer;
		try {
			writer = new PrintWriter(fileName);
			writer.close();
			logger.debug("Successfully cleared contents in " + fileName);
		} catch (final FileNotFoundException e) {
			logger.error("File " + fileName + "not found!");
			e.printStackTrace();
		}

	}

	/**
	 * Reads file contents.
	 *
	 * @param fileName to read
	 */
	public static String readFile(final String fileName) {

		try {
			File file = new File(fileName);
			FileInputStream fis = new FileInputStream(file);
			byte[] data = new byte[(int) file.length()];
			fis.read(data);
			fis.close();

			logger.debug("Successfully read contents of file: " + fileName);

			return new String(data, "UTF-8");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error("Error while reading file content.");
		}
		return "File Error!";

	}


	/**
	 * Removes a line from the given file.
	 *
	 * @param fileName     to open for writing
	 * @param lineToRemove line to search for.
	 * @return {@code true if successful}, {@code false} if not.
	 */
	public static boolean removeLine(final String fileName, final String lineToRemove) {
		RandomAccessFile file;
		boolean success = false;
		try {
			file = new RandomAccessFile(fileName, "rw");
			String delete;
			String task = "";
			while ((delete = file.readLine()) != null) {
				if (delete.startsWith(lineToRemove)) {
					success = true;
					continue;
				}
				task += delete + "\n";
			}
			final BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
			writer.write(task);
			file.close();
			writer.close();
			logger.debug("Successfully Line removed!");
		} catch (final Exception e) {
			logger.error("Error during line removal!");
			e.printStackTrace();
		}
		return success;

	}

	/**
	 * Writes the given text into the given file. Creates if file not exists. Closes
	 * after writing. It does not clear the existing data.
	 *
	 * @param fileName to open for writing.
	 * @param data     to write as a new line.
	 *
	 */
	public static void writeData(final String fileName, final String data) {
		try {
			clearFile(fileName);
			final BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, StandardCharsets.UTF_8, true));
			writer.write(data);
			writer.newLine();
			writer.close();

			logger.debug("Successfully written into file: " + fileName);

		} catch (final IOException e) {
			logger.error("Error while opening the file!");
		}
	}

	/**
	 * Creates the given {@code fullPath}, if some of the folders do not exist, it
	 * creates them.
	 *
	 * @param fullPath
	 * @return {@code true} if created, {@code false} if not
	 */
	public static boolean createDirectories(String fullPath) {
		File directory = new File(fullPath);

		logger.debug("Creating folder: " + directory.getAbsolutePath());
		if (!directory.exists()) {

			directory.mkdirs();
			logger.debug(directory.getAbsolutePath()+ " successfully created.");
			return true;
		}

		logger.error(directory.getAbsolutePath()+ " not successfully created.");

		return false;
	}

	/**
	 * Static method that helps fetch the current working directory as String.
	 *
	 * @return Returns the current working directory as String.
	 */
	public static String getCurrentWorkingDirectory() {
		File directory = new File("");
		return directory.getAbsolutePath();
	}

	/**
	 * Static method that helps fetch the Full Path from Namespace.
	 *
	 * @param namespace
	 * @return Returns the Full Path from Namespace as String.
	 */
	public static String getFullPathFromNamespace(String namespace) {
		// convert each dot into a folder
		return namespace.replace(".", "/");
	}

	/**
	 * Removes underscore from any String Input and converts the entire String into UPPERCASE.
	 * This static function is used for converting Enums of AAS java models to BaSyx models.
	 * This static function is written only for this project.
	 *
	 * @param input
	 * @return Returns a String with no Underscore and having it entirely in UPPERCASE.
	 */
	public static String removeUnderScore(String input) {
		String refinedInput = "";

		refinedInput = input.replaceAll("_", "").toUpperCase();

		return refinedInput;
	}

//	/**
//	 * Converts the passed in ASCII encoded text into UTF-8 encoded text.
//	 *
//	 * @param textASCII String encoded in ASCII format
//	 *
//	 * @return String encoded in UTF-8 format.
//	 */
//	public static String encodeText2UTF8(String textASCII) {
//
//		String textUTF8 = "";
//
//		byte[] bytesTextASCI = textASCII.getBytes(StandardCharsets.UTF_8);
//
//		textUTF8 = new String(bytesTextASCI, StandardCharsets.UTF_8);
//
//		return textUTF8;
//
//	}
}

