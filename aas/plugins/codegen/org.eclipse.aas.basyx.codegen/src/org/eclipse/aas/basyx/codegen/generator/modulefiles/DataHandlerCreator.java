/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.basyx.codegen.generator.modulefiles;

import java.util.Calendar;
import java.util.List;

import org.eclipse.aas.api.aas.AssetAdministrationShell;
import org.eclipse.aas.api.asset.Asset;
import org.eclipse.aas.api.communications.Endpoint;
import org.eclipse.aas.api.submodel.SubModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DataHandlerCreator {
	
	private static final Logger logger = LoggerFactory.getLogger(DataHandlerCreator.class);
	private int year = Calendar.getInstance().get(Calendar.YEAR);
	private AssetAdministrationShell aas;
	private Asset asset;
	private String namespace;
	
	/**
	 * Constructor for the DataHandlerCreator Class. It receives: 
	 * 	1. An AAS Instance as parameter
	 * 	2, An Asset Instance as parameter
	 * 	3. A Namepsace String Instance as parameter 
	 * 
	 * and initializes the class. 
	 * 
	 * @param aas
	 * @param asset
	 * @param namespace
	 */
	public DataHandlerCreator(AssetAdministrationShell aas, Asset asset, 
			String namespace) {
		
		this.aas = aas;
		this.asset = asset;
		this.namespace = namespace;
		if(aas!=null && asset!= null && aas.getIdShort()!=null && asset.getIdShort()!=null )
		logger.info("DataHandlerCreator Initialised with AAS: " + aas.getIdShort()
		+ ", Asset: " + asset.getIdShort() + " and namespace: " + this.namespace);
	}
	
	/**
	 * Generates the Code for ConnectedDevices Class as String and returns it as String. 
	 * 
	 * @return	Returns a String with the generated Code for the ConnectedDevices Class. 
	 */
	public String createConnectedDevices() {
		
		String wrapperField = "";
		String wrapperInitialisation = "";
		String connectedDevicesText = "";
		
		List<Endpoint> endpoints = asset.getEndpoints();
		
		for (Endpoint endpoint : endpoints) {
			wrapperField +=
					"	public final " + endpoint.getProtocol().toString().toUpperCase()
					+ "ConnectorWrapper " + endpoint.getName() + ";\r\n" 
					+ "	\r\n"
					+ "	";
			
			if (endpoint.getProtocol().toString() == "OPCUA") {
				wrapperInitialisation += 
						"			" + endpoint.getName() + " = new " + endpoint.getProtocol().toString().toUpperCase() 
						+ "ConnectorWrapper(create" + endpoint.getProtocol().toString().toUpperCase() 
						+ "Client(AASServer.getSettings()." + endpoint.getName() 
						+ ".get(), opcUaClientConfig));\r\n";
			}
			
			else if (endpoint.getProtocol().toString() == "HTTP") {
				wrapperInitialisation += 
						"			" + endpoint.getName() + " = new " + endpoint.getProtocol().toString().toUpperCase() 
						+ "ConnectorWrapper(create" + endpoint.getProtocol().toString().toUpperCase() 
						+ "Client(AASServer.getSettings()." + endpoint.getName() 
						+ ".get()));\r\n";
			}
		}
		
		connectedDevicesText += 
				"/*******************************************************************************\r\n"
				+ " * Copyright (c) " + year +" DFKI.\n"
				+ " *\r\n"
				+ " * This program and the accompanying materials\r\n"
				+ " * are made available under the terms of the Eclipse Public License 2.0\r\n"
				+ " * which accompanies this distribution, and is available at\r\n"
				+ " * https://www.eclipse.org/legal/epl-2.0/\r\n"
				+ " *\r\n"
				+ " * SPDX-License-Identifier: EPL-2.0\r\n"
				+ " *\r\n"
				+ " * Contributors:\r\n"
				+ " *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>\r\n"
				+ " *	  FESTO - Moritz Marseu <moritz.marseu@festo.com>\r\n"
				+ " *******************************************************************************/\r\n"
				+ "\r\n"
				+ "/**\r\n"
				+ " * ConnectedDevices is a general class provided for the AAS and its submodel to \r\n"
				+ " * connect and communicate to real-world assets over different server protocols.\r\n"
				+ " * OPCUA and HTTP Protocols are currently supported with the corresponding wrappers\r\n"
				+ " * (OPCUAConnectorWrapper and HTTPConnectorWrapper).\r\n"
				+ " */\r\n"
				+ " \r\n"
				+ "package " + this.namespace + ".connection;\r\n"
				+ "\r\n"
				+ "import java.security.GeneralSecurityException;\r\n"
				+ "\r\n"
				+ "import org.eclipse.basyx.vab.protocol.http.connector.HTTPConnector;\r\n"
				+ "import org.eclipse.basyx.vab.protocol.opcua.connector.ClientConfiguration;\r\n"
				+ "import org.eclipse.basyx.vab.protocol.opcua.connector.IOpcUaClient;\r\n"
				+ "import org.eclipse.basyx.vab.protocol.opcua.connector.milo.MiloOpcUaClient;\r\n"
				+ "\r\n"
				+ "import " + this.namespace + ".module.AASServer;\r\n"
				+ "import " + this.namespace + ".module.Settings;\r\n"
				+ "\r\n"
				+ "import org.eclipse.basyx.vab.protocol.api.IBaSyxConnector;\r\n"
				+ "import com.festo.aas.p4m.security.SelfSignedCertificateProvider;\r\n"
				+ "\r\n"
				+ "public class ConnectedDevices {\r\n"
				+ "	\r\n"				
				+ wrapperField
				+ "\r\n"
				+ "private static ConnectedDevices single_instance = null;\r\n"
				+ "\r\n"
				+ "public static ConnectedDevices getInstance() throws Exception {\r\n"
				+ "		if (single_instance == null)\r\n" 
				+ " 		single_instance = new ConnectedDevices();\r\n"
				+ "		return single_instance;\r\n"
				+ "}\r\n"
				+ "\r\n"
				+ "	/**\r\n"
				+ "	 * Constructor. Initializes the available servers endpoints declared as field variables respective to their Connectors.\r\n"
				+ "	 *  \r\n"
				+ "	 * @throws Exception If any exception occurs during the creation of the connectors. \r\n"
				+ "	 */\r\n"
				+ "public ConnectedDevices() throws Exception {\r\n"
				+ "		\r\n"
				+ "		try {\r\n"
				+ "			\r\n"
				+ "			ClientConfiguration opcUaClientConfig = createOpcUaClientConfiguration();\r\n"
				+ wrapperInitialisation
				+ "		} catch (GeneralSecurityException e) {\r\n"
				+ "			throw new Exception(\"Failed to create application certificate for the OPCUA Client.\", e);\r\n"
				+ "		}\r\n"
				+ "		\r\n"
				+ "	}\r\n"
				+ "	\r\n"
				+ "	/**\r\n"
				+ "	 * Creates a new OPCUAClient for the given endpoint. \r\n"
				+ "	 * \r\n"
				+ "	 * @param endpointUrl			The endpoint the client will connect to. \r\n"
				+ "	 * @param opcUaClientConfig		The configuration for the OPCUA client. \r\n"
				+ "	 * @return						A preconfigured {@link IOpcUaClient}.\r\n"
				+ "	 */\r\n"
				+ "	private IOpcUaClient createOPCUAClient(String endpointUrl, ClientConfiguration opcUaClientConfig) {\r\n"
				+ "		\r\n"
				+ "		IOpcUaClient client = new MiloOpcUaClient(endpointUrl);\r\n"
				+ "		client.setConfiguration(opcUaClientConfig);\r\n"
				+ "		\r\n"
				+ "		return client;\r\n"
				+ "	}\r\n"
				+ "	\r\n"
				+ "    /**\r\n"
				+ "     * Creates a configuration object for OPC UA clients.\r\n"
				+ "     *\r\n"
				+ "     * <p>\r\n"
				+ "     * You can make modifications to this method if you need to make other settings for your OPC UA\r\n"
				+ "     * clients or if you'd like to use a different source for your application certificate.\r\n"
				+ "     *\r\n"
				+ "     * @return A new client configuration which can be applied to {@link IOpcUaClient} instances.\r\n"
				+ "     *\r\n"
				+ "     * @throws GeneralSecurityException if the creation of the application certificate for the OPC UA\r\n"
				+ "     *                                  client fails.\r\n"
				+ "     */\r\n"
				+ "	private ClientConfiguration createOpcUaClientConfiguration() throws GeneralSecurityException {\r\n"
				+ "		\r\n"
				+ "		Settings settings = AASServer.getSettings();\r\n"
				+ "		\r\n"
				+ "		SelfSignedCertificateProvider certificateProvider = new SelfSignedCertificateProvider(settings.aasName.get(), null, null, null, null, null, settings.aasUri.get().toString());\r\n"
				+ "		certificateProvider.load();\r\n"
				+ "	       \r\n"
				+ "	    ClientConfiguration config = new ClientConfiguration();\r\n"
				+ "	    config.setKeyPairAndCertificate(certificateProvider.getKeyPair(), \r\n"
				+ "	    		certificateProvider.getCertificate());\r\n"
				+ "	       \r\n"
				+ "	    config.setApplicationName(settings.aasName.get());\r\n"
				+ "	    config.setApplicationUri(settings.aasUri.get().toString());\r\n"
				+ "	       \r\n"
				+ "	    return config;\r\n"
				+ "	    \r\n"
				+ "	}\r\n"
				+ "	\r\n"
				+ "	/**\r\n"
				+ "	 * Creates a new HTTPClient. \r\n"
				+ "	 * \r\n"
				+ "	 * @param endpointUrl		The HTTP endpoint, the client wants to connect to. \r\n"
				+ "	 * @return					A pre-configured {@link IBaSyxConnector}.\r\n"
				+ "	 */\r\n"
				+ "	private IBaSyxConnector createHTTPClient(String endpointUrl) {\r\n"
				+ "		return new HTTPConnector(endpointUrl);\r\n"
				+ "	}\r\n"
				+ "	\r\n"
				+ "}";
		
		logger.info("ConnectedDevices code generated successfully!");
		
		return connectedDevicesText;
		
	}
	
	/**
	 * Generates the Code for OPCUAConnectorWrapper Class as String and returns it as String. 
	 * 
	 * @return	Returns a String with the generated Code for the OPCUAConnectorWrapper Class. 
	 */
	public String createOPCUAConnectorWrapper() {
		
		String text = 
				"/*******************************************************************************\n"
				+ " * Copyright (C) " + year + "Festo Didactic SE\r\n"
				+ " *\r\n"
				+ " * This program and the accompanying materials are made\r\n"
				+ " * available under the terms of the Eclipse Public License 2.0\r\n"
				+ " * which is available at https://www.eclipse.org/legal/epl-2.0/\r\n"
				+ " *\r\n"
				+ " * SPDX-License-Identifier: EPL-2.0\r\n"
				+ " * Contributor:\r\n"
				+ " * 		Festo - Moritz Marseu <moritz.marseu@festo.com>\r\n"
				+ " ******************************************************************************/\n"
				+ "\r\n"
				+ "package " + this.namespace + ".connection;\r\n"
				+ "import java.util.List;\r\n"
				+ "import org.eclipse.basyx.vab.protocol.opcua.connector.IOpcUaClient;\r\n"
				+ "import org.eclipse.basyx.vab.protocol.opcua.exception.OpcUaException;\r\n"
				+ "import org.eclipse.basyx.vab.protocol.opcua.types.NodeId;\r\n"
				+ "\r\n"
				+ "import com.festo.aas.p4m.connection.OpcUaClient;\r\n"
				+ "\r\n"
				+ "/**\r\n"
				+ " * Wrapper around BaSyx' OPC UA client with a simpler API.\r\n"
				+ " *\r\n"
				+ " * <h2>Introduction</h2>\r\n"
				+ " *\r\n"
				+ " * This class is intended for novice users who are not very experienced with Java programming or the\r\n"
				+ " * OPC UA protocol. More advanced users may want to use {@link IOpcUaClient} directly.\r\n"
				+ " *\r\n"
				+ " * You can use this class to perform three basic operations from OPC UA's feature set:\r\n"
				+ " * <ul>\r\n"
				+ " * <li>Read variable nodes\r\n"
				+ " * <li>Write variable nodes\r\n"
				+ " * <li>Invoke methods\r\n"
				+ " * </ul>\r\n"
				+ " *\r\n"
				+ " * All of these actions require you to specify so-called <i>NodeIds</i>. This is an OPC UA concept\r\n"
				+ " * used to uniquely identify a single node on an OPC UA server.\r\n"
				+ " *\r\n"
				+ " * <h2>How to work with NodeIds</h2>\r\n"
				+ " *\r\n"
				+ " * A <i>NodeId</i> is a structure made up of two components:\r\n"
				+ " * <ul>\r\n"
				+ " * <li>A <b>namespace index</b> which is a number that identifies the namespace (think of this like\r\n"
				+ " * a group) that the node belongs to.\r\n"
				+ " * <li>An <b>identifier</b> which must be unique within the namespace. The identifier can be a\r\n"
				+ " * number, a <code>String</code>, a {@link java.util.UUID} or an array of <code>byte</code>.\r\n"
				+ " * </ul>\r\n"
				+ " *\r\n"
				+ " * Sometimes, when a NodeId is presented to a human audience, it is written in a special format\r\n"
				+ " * which is easy to copy & paste. This <b>string-format</b> looks like this:\r\n"
				+ " * <code>ns=1;i=4211</code>, where the <b>n</b>ame<b>s</b>pace index is <code>1</code> and the\r\n"
				+ " * identifier is an <b>i</b>nteger with the value <code>4211</code>.\r\n"
				+ " *\r\n"
				+ " * <p>\r\n"
				+ " * Often, the easiest way to find out the NodeId of the node you're interested in is to use a\r\n"
				+ " * generic OPC UA client (a quick web search will help you find a free solution if you don't have\r\n"
				+ " * one already) that allows you to connect to any OPC UA server, browse it and display the\r\n"
				+ " * attributes of all of its nodes. It should display the NodeId.\r\n"
				+ " *\r\n"
				+ " * <p>\r\n"
				+ " * Once you have the NodeId, it is a simple matter to create a {@link NodeId} object for use with\r\n"
				+ " * this client. You can either use one of the constructors {@link NodeId#NodeId} or\r\n"
				+ " * {@link NodeId#parse(String)} if you have the NodeId in string-format.\r\n"
				+ " *\r\n"
				+ " * <pre>\r\n"
				+ " * <code>\r\n"
				+ " * NodeId nodeId1 = new NodeId(1, 4211);\r\n"
				+ " * NodeId nodeId2 = NodeId.parse(\"ns=1;i=4211\");\r\n"
				+ " * </code>\r\n"
				+ " * </pre>\r\n"
				+ " */\r\n"
				+ "\r\n"
				+ "public final class OPCUAConnectorWrapper {\r\n"
				+ "\r\n"
				+ "    /**\r\n"
				+ "     * The BaSyx OPC UA client object. Use this if you need more advanced OPA UA features than this\r\n"
				+ "     * class provides.\r\n"
				+ "     */\r\n"
				+ "    public final IOpcUaClient baSyxClient;\r\n"
				+ "\r\n"
				+ "    /**\r\n"
				+ "     * The endpoint this client connects to.\r\n"
				+ "     */\r\n"
				+ "    public final String endpoint;\r\n"
				+ "\r\n"
				+ "    /**\r\n"
				+ "     * Creates a new OPC UA client for the given endpoint.\r\n"
				+ "     */\r\n"
				+ "    public OPCUAConnectorWrapper(IOpcUaClient baSyxClient) {\r\n"
				+ "        this.baSyxClient = baSyxClient;\r\n"
				+ "        endpoint = baSyxClient.getEndpointUrl();\r\n"
				+ "    }\r\n"
				+ "\r\n"
				+ "    /**\r\n"
				+ "     * Reads a value from the OPC UA server.\r\n"
				+ "     *\r\n"
				+ "     * <p>\r\n"
				+ "     * Using this method you can read a value from a single variable node on an OPC UA server. You need\r\n"
				+ "     * to know the NodeId and the expected data type.\r\n"
				+ "     *\r\n"
				+ "     * <h2>Example</h2>\r\n"
				+ "     *\r\n"
				+ "     * This example shows how to read an integer value from the server.<br>\r\n"
				+ "     * The node id has the namespace index 1 and the String identifier \"SomeIntegerNode\". For more\r\n"
				+ "     * information on NodeIds, see {@link OpcUaClient here}.\r\n"
				+ "     *\r\n"
				+ "     * <pre>\r\n"
				+ "     * <code>\r\n"
				+ "     * NodeId integerNodeId = new NodeId(1, \"SomeIntegerNode\");\r\n"
				+ "     * Object readValue = opcUaClient.readValue(integerNodeId);\r\n"
				+ "     * Integer myInteger = (Integer) readValue;\r\n"
				+ "     * </code>\r\n"
				+ "     * </pre>\r\n"
				+ "     *\r\n"
				+ "     * @param nodeId The id of the variable to read.\r\n"
				+ "     *\r\n"
				+ "     * @return The value which was read. You have to know what data type to expect. See\r\n"
				+ "     *         {@link IOpcUaClient here} for details on types.\r\n"
				+ "     *\r\n"
				+ "     * @throws OpcUaException if there is any error during the OPC UA communication. If you don't catch\r\n"
				+ "     *                        this exception, it will simply be returned to the client which called this\r\n"
				+ "     *                        AAS.\r\n"
				+ "     */\r\n"
				+ "    public Object readValue(NodeId nodeId) {\r\n"
				+ "        return baSyxClient.readValue(nodeId);\r\n"
				+ "    }\r\n"
				+ "\r\n"
				+ "    /**\r\n"
				+ "     * Writes a value to the OPC UA server.\r\n"
				+ "     *\r\n"
				+ "     * <p>\r\n"
				+ "     * Using this method you can set the value of a single variable node on an OPC UA server. You need\r\n"
				+ "     * to know the NodeId and must provide a value of the correct data type.\r\n"
				+ "     *\r\n"
				+ "     * <h2>Example</h2>\r\n"
				+ "     *\r\n"
				+ "     * This example shows how to write an integer value on the server.<br>\r\n"
				+ "     * The node id has the namespace index 1 and the String identifier \"SomeIntegerNode\". For more\r\n"
				+ "     * information on NodeIds, see {@link OpcUaClient here}.\r\n"
				+ "     *\r\n"
				+ "     * <pre>\r\n"
				+ "     * <code>\r\n"
				+ "     * NodeId integerNodeId = new NodeId(1, \"SomeIntegerNode\");\r\n"
				+ "     * opcUaClient.writeValue(integerNodeId, 5);\r\n"
				+ "     * </code>\r\n"
				+ "     * </pre>\r\n"
				+ "     *\r\n"
				+ "     * @param nodeId The id of the variable to write.\r\n"
				+ "     * @param value  The new value to write.\r\n"
				+ "     *\r\n"
				+ "     * @throws OpcUaException if there is any error during the OPC UA communication. If you don't catch\r\n"
				+ "     *                        this exception, it will simply be returned to the client which called this\r\n"
				+ "     *                        AAS.\r\n"
				+ "     */\r\n"
				+ "    public void writeValue(NodeId nodeId, Object value) {\r\n"
				+ "        baSyxClient.writeValue(nodeId, value);\r\n"
				+ "    }\r\n"
				+ "\r\n"
				+ "    /**\r\n"
				+ "     * Invokes a method on the OPC UA server.\r\n"
				+ "     *\r\n"
				+ "     * <p>\r\n"
				+ "     * Using this method you can invoke an OPC UA method on a server. You need to know the NodeId of the\r\n"
				+ "     * method itself as well as the one of the object on which to invoke the method. Additionally,\r\n"
				+ "     * should the method require any arguments, you must provide a value of the correct data type for each\r\n"
				+ "     * of those.<br>\r\n"
				+ "     * Lastly, if the method returns some data, you should know the data type to expect.\r\n"
				+ "     *\r\n"
				+ "     * <h2>Example</h2>\r\n"
				+ "     *\r\n"
				+ "     * This example shows how to invoke a method which takes an integer and a boolean value and returns\r\n"
				+ "     * a string.<br>\r\n"
				+ "     * The method itself has namespace index 2 and the UUID identifier\r\n"
				+ "     * <code>3ac02706-286a-4bd8-b0ec-3917a26fe589</code>.<br>\r\n"
				+ "     * It will be invoked on the object with the namespace index 2 and the integer identifier\r\n"
				+ "     * <code>300</code>.<br>\r\n"
				+ "     * For more information on NodeIds, see {@link OpcUaClient here}.\r\n"
				+ "     *\r\n"
				+ "     * <pre>\r\n"
				+ "     * <code>\r\n"
				+ "     * NodeId objectNodeId = new NodeId(2, 300);\r\n"
				+ "     * NodeId methodNodeId = new NodeId(2, UUID.fromString(\"3ac02706-286a-4bd8-b0ec-3917a26fe589\"));\r\n"
				+ "     * int firstArgument = 5;\r\n"
				+ "     * boolean secondArgument = false;\r\n"
				+ "     * List<Object> returnedList = opcUaClient.invokeMethod(objectNodeId, methodNodeId,\r\n"
				+ "     *     firstArgument, secondArgument);\r\n"
				+ "     * String returnedString = (String) returnedList.get(0);\r\n"
				+ "     * </code>\r\n"
				+ "     * </pre>\r\n"
				+ "     *\r\n"
				+ "     * @param ownerId    The id of the object on which to invoke the method.\r\n"
				+ "     * @param methodId   The id of the method itself.\r\n"
				+ "     * @param parameters The input arguments to the method.\r\n"
				+ "     *\r\n"
				+ "     * @return The values returned by the OPC UA method.\r\n"
				+ "     *\r\n"
				+ "     * @throws OpcUaException if there is any error during the OPC UA communication. If you don't catch\r\n"
				+ "     *                        this exception, it will simply be returned to the client which called this\r\n"
				+ "     *                        AAS.\r\n"
				+ "     */\r\n"
				+ "    public List<Object> invokeMethod(NodeId ownerId, NodeId methodId, Object... parameters) {\r\n"
				+ "        return baSyxClient.invokeMethod(ownerId, methodId, parameters);\r\n"
				+ "    }\r\n"
				+ "}\r\n";
		
		logger.info("OPCUAConnectorWrapper code generated successfully!");
		
		return text;
	}
	
	/**
	 * Generates the Code for HTTPConnectorWrapper Class as String and returns it as String. 
	 * 
	 * @return	Returns a String with the generated Code for the HTTPConnectorWrapper Class. 
	 */
	public String createHTTPConnectorWrapper() {
		String text =
				"/*******************************************************************************\n"
				+ " * Copyright (c) " + year +" DFKI.\n"
				+ " *\r\n"
				+ " * This program and the accompanying materials are made\r\n"
				+ " * available under the terms of the Eclipse Public License 2.0\r\n"
				+ " * which is available at https://www.eclipse.org/legal/epl-2.0/\r\n"
				+ " *\r\n"
				+ " * SPDX-License-Identifier: EPL-2.0\r\n"
				+ " * Contributor:\r\n"
				+ " * 		DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>\r\n"
				+ " ******************************************************************************/\n"
				+ "package " + this.namespace + ".connection;\r\n"
				+ "import org.eclipse.basyx.vab.protocol.api.IBaSyxConnector;\r\n"
				+ "\r\n"
				+ "/**\r\n"
				+ " * Wrapper around BaSyx' HTTP Connector with a simpler API.\r\n"
				+ " * \r\n"
				+ " * <h2>Introduction</h2>\r\n"
				+ " * This class encapsulates the HTTPConnector class provided by BaSyx. This helps\r\n"
				+ " * in providing the user not only with better documented methods, but also helps\r\n"
				+ " * abstract methods or functionalities a basic user does not need.\r\n"
				+ " * \r\n"
				+ " *  The user can use this class to perform the following:\r\n"
				+ " *  <ul>\r\n"
				+ " *  <li> Read values from a HTTP Server Endpoint.\r\n"
				+ " *  <li> Write values to a HTTP Server Endpoint.\r\n"
				+ " *  <li> Invoke Methods on a HTTP Server at the given Endpoint. \r\n"
				+ " *  </ul>\r\n"
				+ " *  \r\n"
				+ " *  All of these requires the user to specify the URL where the server is hosted \r\n"
				+ " *  along with the routing to the Endpoint where a value could be written to, \r\n"
				+ " *  read from or methods could be invoked. \r\n"
				+ " *  \r\n"
				+ " *  An Endpoint URL, is how resources are located on the HTTP Server. This \r\n"
				+ " *  endpoint is broadly divided into two main components. \r\n"
				+ " *  \r\n"
				+ " *   <ul>\r\n"
				+ " *   <li> The IP Address of the server, where the resources are being hosted. \r\n"
				+ " *   This might be either in the form\r\n"
				+ " *   http://example.com/ or http://127.0.0.1:5000/ or as the server was defined. \r\n"
				+ " *   This must be known by the user. \r\n"
				+ " *   \r\n"
				+ " *   <li> The navigation path to the required resource. The forward slash (/) is \r\n"
				+ " *   used here to indicate hierarchical relationships in the resource. \r\n"
				+ " *   Thus using the forward slash (/) one could navigate inside the resource to \r\n"
				+ " *   other hierarchically related resources.  \r\n"
				+ " *   </ul>\r\n"
				+ " *   \r\n"
				+ " *   An example for this would be - The HTTP server end point \r\n"
				+ " *   'http://127.0.0.1:5000/' hosting a resource named 'login'  would be \r\n"
				+ " *   written as:\r\n"
				+ " *   \r\n"
				+ " *   urlPath = http://127.0.0.1:5000/login\r\n"
				+ " *\r\n"
				+ " */\r\n"
				+ "\r\n"
				+ "\r\n"
				+ "public final class HTTPConnectorWrapper {\r\n"
				+ "	\r\n"
				+ "	/**\r\n"
				+ "	 * The BaSyx HTTP Client. One could use this final field/member variable \r\n"
				+ "	 * directly to access the Connector provided by BaSyx, with more \r\n"
				+ "	 * advanced methods. \r\n"
				+ "	 */\r\n"
				+ "	\r\n"
				+ "	public final IBaSyxConnector baSyxHTTPClient;\r\n"
				+ "	\r\n"
				+ "	/**\r\n"
				+ "	 * Constructor method for the class HTTPConnectorWrapper. It requires as \r\n"
				+ "	 * parameters within the parenthesis an instance of the IBaSyxConnector \r\n"
				+ "	 * interface. The class {@link ConnectorFactory.java} provides the\r\n"
				+ "	 * \r\n"
				+ "	 * @param baSyxHTTPClient\r\n"
				+ "	 */\r\n"
				+ "\r\n"
				+ "	public HTTPConnectorWrapper(IBaSyxConnector baSyxHTTPClient) {\r\n"
				+ "		\r\n"
				+ "		this.baSyxHTTPClient = baSyxHTTPClient;\r\n"
				+ "		\r\n"
				+ "	}\r\n"
				+ "	\r\n"
				+ "	/**\r\n"
				+ "	 *	Performs a HTTP GET.\r\n"
				+ "	 *\r\n"
				+ "	 *	The GET method means retrieve whatever information (in the form of an\r\n"
				+ "	 *	entity) is identified by the Request-URI. If the Request-URI refers\r\n"
				+ "	 *	to a data-producing process, it is the produced data which shall be\r\n"
				+ "	 *	returned as the entity in the response and not the source text of the\r\n"
				+ "	 *	process, unless that text happens to be the output of the process. \r\n"
				+ "	 *  \r\n"
				+ "	 *  [For further info, refer - RFC 2616, � 9.3 \r\n"
				+ "	 *  Link: https://datatracker.ietf.org/doc/html/rfc2616#section-9.3]\r\n"
				+ "	 *   \r\n"
				+ "	 * @param urlPath	The URL Path (Request-URI) where the resource being \r\n"
				+ "	 * 					requested is located.\r\n"
				+ "	 * @return			String representation of the resource available under\r\n"
				+ "	 * 					the mentioned urlPath. \r\n"
				+ "	 */\r\n"
				+ "	public String readValue(String urlPath) {\r\n"
				+ "		return baSyxHTTPClient.getValue(urlPath);\r\n"
				+ "	}\r\n"
				+ "	\r\n"
				+ "	/**\r\n"
				+ "	 * Performs a HTTP PUT (or) a HTTP POST.\r\n"
				+ "	 * \r\n"
				+ "	 * The PUT method requests that the enclosed entity be stored under the\r\n"
				+ "	 * supplied Request-URI. If the Request-URI refers to an already\r\n"
				+ "	 * existing resource, the enclosed entity SHOULD be considered as a\r\n"
				+ "	 * modified version of the one residing on the origin server. If the\r\n"
				+ "	 * Request-URI does not point to an existing resource, and that URI is\r\n"
				+ "	 * capable of being defined as a new resource by the requesting user\r\n"
				+ "	 * agent, the origin server can create the resource with that URI.\r\n"
				+ "	 * \r\n"
				+ "	 * [For more details and further info, refer - RFC 2616 � 9.6\r\n"
				+ "	 * Link: https://datatracker.ietf.org/doc/html/rfc2616#section-9.6]\r\n"
				+ "	 * \r\n"
				+ "	 * The POST method is used to request that the origin server accept the\r\n"
				+ "	 * entity enclosed in the request as a new subordinate of the resource\r\n"
				+ "	 * identified by the Request-URI in the Request-Line. POST is designed\r\n"
				+ "	 * to allow a uniform method to cover varied functions. The actual function \r\n"
				+ "	 * performed by the POST method is determined by the server and is usually \r\n"
				+ "	 * dependent on the Request-URI.\r\n"
				+ "	 * \r\n"
				+ "	 * [For more details on varied functions and further info, refer - RFC 2616 \r\n"
				+ "	 * � 9.5\r\n"
				+ "	 * Link: https://datatracker.ietf.org/doc/html/rfc2616#section-9.5]\r\n"
				+ "	 * \r\n"
				+ "	 * @param urlPath		The URL Path (Request-URI) where the resource being \r\n"
				+ "	 * 						requested is located.\r\n"
				+ "	 * @param value			The \"entity\" (as mentioned in the definitions above) \r\n"
				+ "	 * 						that is to be used for 'PUT' and 'POST' requests. \r\n"
				+ "	 * @param requestType	Type of the request to be made using the function. \r\n"
				+ "	 * 						Should be mentioned either \"PUT\" (or) \"POST\".\r\n"
				+ "	 * @return				Returns the feedback from the respective HTTP \r\n"
				+ "	 * 						requests that have been made. \r\n"
				+ "	 * 						In case an unrecognized requestType is passed in by\r\n"
				+ "	 * 						user, the method returns \"Invalid Request\".\r\n"
				+ "	 */\r\n"
				+ "	public String writeValue(String urlPath, Object value, String requestType) {\r\n"
				+ "		\r\n"
				+ "		if (requestType==\"PUT\") {\r\n"
				+ "			return baSyxHTTPClient.setValue(urlPath, (String) value);\r\n"
				+ "		}\r\n"
				+ "		\r\n"
				+ "		else if (requestType == \"POST\") {\r\n"
				+ "			return baSyxHTTPClient.createValue(urlPath, (String) value);\r\n"
				+ "		}\r\n"
				+ "		\r\n"
				+ "		else {\r\n"
				+ "			return \"Invalid Request\";\r\n"
				+ "		}\r\n"
				+ "	}\r\n"
				+ "	\r\n"
				+ "	/**\r\n"
				+ "	 * Performs a HTTP POST Operation. \r\n"
				+ "	 * \r\n"
				+ "	 * The POST method is used to request that the origin server accept the\r\n"
				+ "	 * entity enclosed in the request as a new subordinate of the resource\r\n"
				+ "	 * identified by the Request-URI in the Request-Line. POST is designed\r\n"
				+ "	 * to allow a uniform method to cover varied functions. The actual function \r\n"
				+ "	 * performed by the POST method is determined by the server and is usually \r\n"
				+ "	 * dependent on the Request-URI.\r\n"
				+ "	 * \r\n"
				+ "	 * [For more details on varied functions and further info, refer - RFC 2616 \r\n"
				+ "	 * � 9.5\r\n"
				+ "	 * Link: https://datatracker.ietf.org/doc/html/rfc2616#section-9.5]\r\n"
				+ "	 * \r\n"
				+ "	 * @param urlPath		The URL Path (Request-URI) where the resource being \r\n"
				+ "	 * 						requested is located.\r\n"
				+ "	 * @param value			The \"entity\" (as mentioned in the definitions above) \r\n"
				+ "	 * 						that is to be used for 'PUT' and 'POST' requests.\r\n"
				+ "	 * @return				Returns the feedback from the HTTP POST request. \r\n"
				+ "	 */\r\n"
				+ "	public String invokeMethod(String urlPath, Object value) {\r\n"
				+ "		return baSyxHTTPClient.invokeOperation(urlPath, (String) value);\r\n"
				+ "		\r\n"
				+ "	}\r\n"
				+ "\r\n"
				+ "}\r\n";
		
		logger.info("HTTPConnectorWrapper code generated successfully!");
		
		return text;
	}
	
	/**
	 * Generates the Code for OPCUAVariable Class as String and returns it as String. 
	 * 
	 * @return	Returns a String with the generated Code for the OPCUAVariable Class. 
	 */
	public String createOPCUAVariable() {
		String text = 
				"/*******************************************************************************\r\n"
				+ " * Copyright (C) " + year + "Festo Didactic SE\r\n"
				+ " *\r\n"
				+ " * This program and the accompanying materials are made\r\n"
				+ " * available under the terms of the Eclipse Public License 2.0\r\n"
				+ " * which is available at https://www.eclipse.org/legal/epl-2.0/\r\n"
				+ " *\r\n"
				+ " * SPDX-License-Identifier: EPL-2.0\r\n"
				+ " * Contributor:\r\n"
				+ " * 		Festo - Moritz Marseu <moritz.marseu@festo.com>\r\n"
				+ " ******************************************************************************/\r\n"
				+ "package " + this.namespace + ".connection;\r\n"
				+ "\r\n"
				+ "import java.time.Duration;\r\n"
				+ "import java.time.Instant;\r\n"
				+ "\r\n"
				+ "import org.eclipse.basyx.vab.exception.provider.ProviderException;\r\n"
				+ "import org.eclipse.basyx.vab.protocol.opcua.connector.IOpcUaClient;\r\n"
				+ "import org.eclipse.basyx.vab.protocol.opcua.exception.OpcUaException;\r\n"
				+ "import org.eclipse.basyx.vab.protocol.opcua.types.NodeId;\r\n"
				+ "import org.eclipse.basyx.vab.protocol.opcua.types.UnsignedByte;\r\n"
				+ "import org.eclipse.basyx.vab.protocol.opcua.types.UnsignedInteger;\r\n"
				+ "import org.eclipse.basyx.vab.protocol.opcua.types.UnsignedLong;\r\n"
				+ "import org.eclipse.basyx.vab.protocol.opcua.types.UnsignedShort;\r\n"
				+ "import com.festo.aas.p4m.connection.PropertyValueConsumer;\r\n"
				+ "import com.festo.aas.p4m.connection.PropertyValueSupplier;\r\n"
				+ "import org.slf4j.Logger;\r\n"
				+ "import org.slf4j.LoggerFactory;\r\n"
				+ "\r\n"
				+ "/**\r\n"
				+ " * This class supplies the current value from a single remote OPC UA variable or writes its value.\r\n"
				+ " *\r\n"
				+ " * <p>\r\n"
				+ " * The value can optionally be cached for a specified amount of time after fetching to increase\r\n"
				+ " * performance. The cache duration is set during object initialization and can not be changed\r\n"
				+ " * afterwards.\r\n"
				+ " */\r\n"
				+ "public class OpcUaVariable implements PropertyValueConsumer, PropertyValueSupplier {\r\n"
				+ "    private final Logger logger = LoggerFactory.getLogger(this.getClass());\r\n"
				+ "    private final OPCUAConnectorWrapper client;\r\n"
				+ "    private final NodeId nodeId;\r\n"
				+ "    private final Duration cacheDuration;\r\n"
				+ "    private final Class<?> dataType;\r\n"
				+ "\r\n"
				+ "    private Instant cacheTimestamp = Instant.MIN;\r\n"
				+ "    private Object cachedValue;\r\n"
				+ "\r\n"
				+ "    /**\r\n"
				+ "     * Creates a new OPC UA variable connecting to the given node using the given client.\r\n"
				+ "     *\r\n"
				+ "     * <p>\r\n"
				+ "     * When using this constructor, the caching functionality is disabled.\r\n"
				+ "     *\r\n"
				+ "     * @param client   The client object to use for communication.\r\n"
				+ "     * @param nodeId   The node whose value to read or write.\r\n"
				+ "     * @param dataType The class matching the type of the OPC UA variable. See table at\r\n"
				+ "     *                 {@link IOpcUaClient}.\r\n"
				+ "     */\r\n"
				+ "    public OpcUaVariable(OPCUAConnectorWrapper client, NodeId nodeId, Class<?> dataType) {\r\n"
				+ "        this(client, nodeId, dataType, Duration.ZERO);\r\n"
				+ "    }\r\n"
				+ "\r\n"
				+ "    /**\r\n"
				+ "     * Creates a new OPC UA variable connecting to the given node using the given client.\r\n"
				+ "     *\r\n"
				+ "     * @param client        The client object to use for communication.\r\n"
				+ "     * @param nodeId        The node whose value to read or write.\r\n"
				+ "     * @param dataType      The class matching the type of the OPC UA variable. See table at\r\n"
				+ "     *                      {@link IOpcUaClient}.\r\n"
				+ "     * @param cacheDuration The maximum age of the cached value before it will be refetched during the\r\n"
				+ "     *                      next call to {@link #getValue()}.\r\n"
				+ "     */\r\n"
				+ "    public OpcUaVariable(OPCUAConnectorWrapper client, NodeId nodeId, Class<?> dataType, Duration cacheDuration) {\r\n"
				+ "        this.client = client;\r\n"
				+ "        this.nodeId = nodeId;\r\n"
				+ "        this.cacheDuration = cacheDuration;\r\n"
				+ "        this.dataType = dataType;\r\n"
				+ "    }\r\n"
				+ "\r\n"
				+ "    /**\r\n"
				+ "     * Gets the node id of the variable that this object reads or writes.\r\n"
				+ "     *\r\n"
				+ "     * @return The variable's node id.\r\n"
				+ "     */\r\n"
				+ "    public NodeId getNodeId() {\r\n"
				+ "        return nodeId;\r\n"
				+ "    }\r\n"
				+ "\r\n"
				+ "    @Override\r\n"
				+ "    public Object getValue() throws ProviderException {\r\n"
				+ "        if (!cacheValid()) {\r\n"
				+ "            logger.debug(\"Property '{}' not cached.\", nodeId);\r\n"
				+ "            fetchValue();\r\n"
				+ "        }\r\n"
				+ "\r\n"
				+ "        return cachedValue;\r\n"
				+ "    }\r\n"
				+ "\r\n"
				+ "    @Override\r\n"
				+ "    public void applyValue(Object value) throws ProviderException {\r\n"
				+ "        logger.debug(\"Writing '{}' to {} on {}.\", value, nodeId, client.endpoint);\r\n"
				+ "\r\n"
				+ "        if (!isCorrectType(value)) {\r\n"
				+ "            String exceptionMessage = String.format(\r\n"
				+ "                    \"Mismatch between configured type (%s) and type of given value (%s)\", dataType, value.getClass());\r\n"
				+ "            throw new IllegalArgumentException(exceptionMessage);\r\n"
				+ "        }\r\n"
				+ "\r\n"
				+ "        value = mapBaSyxToUnsigned(value);\r\n"
				+ "\r\n"
				+ "        client.writeValue(nodeId, value);\r\n"
				+ "        cacheTimestamp = Instant.now();\r\n"
				+ "    }\r\n"
				+ "\r\n"
				+ "    private boolean cacheValid() {\r\n"
				+ "        return cacheTimestamp.plus(cacheDuration).isAfter(Instant.now());\r\n"
				+ "    }\r\n"
				+ "\r\n"
				+ "    private void fetchValue() throws OpcUaException {\r\n"
				+ "        logger.debug(\"Reading value for {} from {}.\", nodeId, client.endpoint);\r\n"
				+ "        Object value = client.readValue(nodeId);\r\n"
				+ "\r\n"
				+ "        if (!isCorrectType(value)) {\r\n"
				+ "            String exceptionMessage = String.format(\r\n"
				+ "                    \"Mismatch between configured type (%s) and type received from OPC UA server (%s)\", dataType, value\r\n"
				+ "                            .getClass());\r\n"
				+ "            throw new ProviderException(exceptionMessage);\r\n"
				+ "        }\r\n"
				+ "\r\n"
				+ "        cachedValue = mapUnsignedToBaSyx(value);\r\n"
				+ "        cacheTimestamp = Instant.now();\r\n"
				+ "    }\r\n"
				+ "\r\n"
				+ "    private Object mapUnsignedToBaSyx(Object value) {\r\n"
				+ "        if (dataType == UnsignedByte.class) {\r\n"
				+ "            return ((UnsignedByte) value).toShort();\r\n"
				+ "        } else if (dataType == UnsignedShort.class) {\r\n"
				+ "            return ((UnsignedShort) value).toInt();\r\n"
				+ "        } else if (dataType == UnsignedInteger.class) {\r\n"
				+ "            return ((UnsignedInteger) value).toLong();\r\n"
				+ "        } else if (dataType == UnsignedLong.class) {\r\n"
				+ "            return ((UnsignedLong) value).toBigInteger();\r\n"
				+ "        } else {\r\n"
				+ "            return value;\r\n"
				+ "        }\r\n"
				+ "    }\r\n"
				+ "\r\n"
				+ "    private Object mapBaSyxToUnsigned(Object value) {\r\n"
				+ "        if (dataType == UnsignedByte.class) {\r\n"
				+ "            return ((UnsignedByte) value).toShort();\r\n"
				+ "        } else if (dataType == UnsignedShort.class) {\r\n"
				+ "            return ((UnsignedShort) value).toInt();\r\n"
				+ "        } else if (dataType == UnsignedInteger.class) {\r\n"
				+ "            return ((UnsignedInteger) value).toLong();\r\n"
				+ "        } else if (dataType == UnsignedLong.class) {\r\n"
				+ "            return ((UnsignedLong) value).toBigInteger();\r\n"
				+ "        } else {\r\n"
				+ "            return value;\r\n"
				+ "        }\r\n"
				+ "    }\r\n"
				+ "\r\n"
				+ "    private boolean isCorrectType(Object value) {\r\n"
				+ "        return value.getClass() == dataType;\r\n"
				+ "    }\r\n"
				+ "}\r\n"
				+ "\r\n"
				+ "";
		
		logger.info("OPCUAVariable code generated successfully!");
		
		return text;
	}
	
	/**
	 * Generates the Code for the default.properties file as String. 
	 * 
	 * @return	Returns a String with the generated "default.properties" file code.
	 */
	public String propertiesFile() {
		
		// Creating the endpoint codes.
		String endpointsDeclaration = "";
				
		// Creating the submodel Identification codes.
		String subModelIdentification = "";
				
		List<Endpoint> endpoints = asset.getEndpoints();
				
		List<SubModel> subModels = aas.getSubModels();
				
		for (Endpoint endpoint : endpoints) {
					
			if (endpoint.getProtocol().toString() == "OPCUA") {
				endpointsDeclaration += "asset.endpoint." + endpoint.getName() + " = " + endpoint.getAddress() + "\r\n";
			}
					
			else if (endpoint.getProtocol().toString() == "HTTP") {
				endpointsDeclaration += "asset.endpoint." + endpoint.getName() + " = " + endpoint.getAddress() + "\r\n";
			}
					
			else if (endpoint.getProtocol().toString() == "MQTT") {
				System.err.println("MQTT not yet supported.");
				System.out.println("Not adding anything to the default.properties file.");
				endpointsDeclaration += "";
			}
		}
				
		// Creating the SubModel URI. 
		for (SubModel subModel : subModels) {
					
			if (subModel.getKind().toString() == "INSTANCE") {
						
				if (subModel.getIdentification().getIdType().toString() == "IRDI") {
					
					throw new IllegalStateException("Submodel of ModelingKind - INSTANCE "
							+ "does not support IRDI as IdentifierType "
							+ "according to the Details of the "
							+ "Asset Administration Shell (Page 36). \r\n" 
							+ "Set SubModel IdentifierType to IRI or CUSTOM.");

				}
						
				else if (subModel.getIdentification().getIdType().toString() == "IRI") {
					subModelIdentification += "submodel."+subModel.getIdShort()+".uri = " + subModel.getIdentification().getIdentifier() + "\r\n";
				}
				
				else if (subModel.getIdentification().getIdType().toString() == "CUSTOM") {
					subModelIdentification += "submodel."+subModel.getIdShort()+".custom = " + subModel.getIdentification().getIdentifier() + "\r\n";
				}
						
			}
					
			else if (subModel.getKind().toString() == "TEMPLATE") {
						
				if (subModel.getIdentification().getIdType().toString() == "IRDI") {
					subModelIdentification += "submodel."+subModel.getIdShort()+".irdi = " + subModel.getIdentification().getIdentifier() + "\r\n";
				}
						
				else if (subModel.getIdentification().getIdType().toString() == "IRI") {
					subModelIdentification += "submodel."+subModel.getIdShort()+".uri = " + subModel.getIdentification().getIdentifier() + "\r\n";
				}
				
				else if (subModel.getIdentification().getIdType().toString() == "CUSTOM") {
					
					throw new IllegalStateException("Submodel of ModelingKind - TEMPLATE "
							+ "does not support CUSTOM as IdentifierType "
							+ "according to the Details of the "
							+ "Asset Administration Shell (Page 36). \r\n" 
							+ "Set SubModel IdentifierType to IRI or IRDI.");
				
				}
			}
		}
				
		String text = 
				"# This file is automatically generated with default property values. DO NOT EDIT.\r\n"
				+ "# If changes are needed to be made to these given settings, create a file named 'application.properties'\r\n"
				+ "# in the working directory where you execute the application. There, you can customize these\r\n"
				+ "# properties.\r\n"
				+ "aas.name = " + aas.getIdShort() + "\r\n"
				+ "aas.uri = " + aas.getId() + "\r\n"
				+ "aas.listening_port = " + aas.getAASEndpoint().getPort() + "\r\n"
				+ "aas.hostname = " + aas.getAASEndpoint().getAddress() + "\r\n"
				+ "asset.name = " + asset.getIdShort() + "\r\n"							// Have to think how to handle in case we model multiple Assets to an Asset Administration Shell. 
				+ "asset.uri = " + asset.getId() + "\r\n"
				+ endpointsDeclaration
				+ subModelIdentification;
		
		logger.info("Properties file code generated successfully!");
		
		return text;
		
	}

}
