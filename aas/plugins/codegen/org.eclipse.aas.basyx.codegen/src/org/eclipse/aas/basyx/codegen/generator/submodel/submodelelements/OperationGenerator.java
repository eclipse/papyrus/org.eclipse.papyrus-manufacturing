/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.basyx.codegen.generator.submodel.submodelelements;

import java.util.List;

import org.eclipse.aas.api.reference.Key;
import org.eclipse.aas.api.submodel.parts.ConceptDescription;
import org.eclipse.aas.api.submodel.submodelelement.Operation;
import org.eclipse.aas.api.submodel.submodelelement.SubModelElementCollection;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.IOperationVariable;
import org.eclipse.aas.basyx.codegen.generator.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OperationGenerator {
	
	private static final Logger logger = LoggerFactory.getLogger(OperationGenerator.class);
	private Operation opInstance;
	private List<IOperationVariable> inputVariables;
	private List<IOperationVariable> inoutputVariables;
	private List<IOperationVariable> outputVariables;
	private List<Key> semanticKeys;
	private String parent;
	private ConceptDescription conceptDesc;
	
	/**
	 * Constructor for the OperationGenerator Class. It receives a single instance
	 * of Operation as parameter to generate its respective and necessary BaSyx Code.
	 *  
	 * @param opInstance
	 */
	public OperationGenerator(Operation opInstance) {
		
		this.opInstance = opInstance;
		this.inputVariables = opInstance.getInputVars();
		this.inoutputVariables = opInstance.getInoutputVars();
		this.outputVariables = opInstance.getOutputVars();
		
		try {
			this.semanticKeys = opInstance.getSemanticIdentifier().getKeys();
		} 
		
		catch (NullPointerException e) {
			logger.error("Null Pointer Exception in Semantic ID Declaration "
					+ "while initialising " + this.getClass().getSimpleName());
		}
		
		try {
			this.conceptDesc = this.opInstance.getSemanticDescription();
		}
		
		catch (NullPointerException e) {
			logger.error("Null Pointer Exception while fetching ConceptDescription for " +
					"the semantic Id definition of the Operation: " +
									this.getClass().getSimpleName());
		}
		
		// Checking for Parents.
		if (opInstance.getParentSEC()!=null) {
			this.parent = opInstance.getParentSEC().getIdShort();
		}
		
		else if (opInstance.getParentSub()!=null) {
			this.parent = opInstance.getParentSub().getIdShort();
		}
		
		else {
			logger.error("Operation: " + opInstance.getIdShort() 
						+ "has no Parent defined");
		}
		
		logger.info("OperationGenerator Initialised for Operation : " + 
						opInstance.getIdShort());

	}
	
	/**
	 * Generates an Operation either in a SubModel or inside it's parent 
	 * SubModelElementCollection. This function generates:
	 * 
	 * 	- Lambda Content for the Operation. 
	 * 	- Modeling Info for the Operation.
	 * 	- Semantic References for the Operation in consideration. 
	 * 	- Parental information for the Operation in consideration. 
	 * 	- Input Variables for the Operation.
	 * 	- Inoutput Variables for the Operation.
	 * 	- Output Variables for the Operation. 
	 * 
	 * @return Returns a String with the generated Operation Code. 
	 */
	public String generateOperation() {
		
		String opText = generateLambdaContent()
				+ "		Operation " + parent + "_" + opInstance.getIdShort() + "= new Operation();\r\n"
				+ "		" + parent + "_" + opInstance.getIdShort() + ".setIdShort(\"" + opInstance.getIdShort() + "\");\r\n"
				+ "		" + parent + "_" + opInstance.getIdShort() + ".setInvokable(call" + parent + "_" + opInstance.getIdShort() + ");\r\n"
				+ generateModelingInfo()
				+ generateSemanticReference()
				+ generateParentalRelation()
				+ "\r\n"
				+ generateInputVariables()
				+ "\r\n"
				+ generateInOutputVariables()
				+ "\r\n"
				+ generateOutputVariables()
				+ "\r\n\r\n";
		
		logger.info("Operation Code generated for Operation: " + opInstance.getIdShort());

		return opText;


	}
	
	/**
	 * Generates the Field Variables for the OperationVariables contained in an Operation. 
	 * 
	 * @return	Returns a String with the code for declaration of all the 
	 * 			generated Operation Field Variables. 
	 */
	public String generateOperationFieldVariables() {
		
		String fieldOpVar = "";
		
		if (!inputVariables.isEmpty()) {
			
			for (IOperationVariable inputVariable : inputVariables) {
				fieldOpVar += "	protected static Property " + parent + "_"
							+ inputVariable.getValue().getIdShort() + "_"
							+ opInstance.getIdShort() + "_" + "Input" 
							+ " = new Property(\"" + inputVariable.getValue().getIdShort() 
							+ "\", ValueType." + inputVariable.getValueType() 
							+ ");\r\n" ;
			}
			fieldOpVar += "		\r\n\r\n";
			
			logger.info("Input Variables as Field for Operation: " 
							+ opInstance.getIdShort() + " generated.");
		}
		
		else {
			logger.debug("No Input Variables found for Operation: " 
							+ opInstance.getIdShort() 
							+ ". Or an exception has occured.");
		}
		
		if (!inoutputVariables.isEmpty()) {
			
			for (IOperationVariable inoutputVariable : inoutputVariables) {
				fieldOpVar += "	protected static Property " + parent + "_"
							+ inoutputVariable.getValue().getIdShort() + "_"
							+ opInstance.getIdShort() + "_" + "InOutput" 
							+ " = new Property(\"" + inoutputVariable.getValue().getIdShort() 
							+ "\", ValueType." + inoutputVariable.getValueType() 
							+ ");\r\n";
			}
			fieldOpVar += "		\r\n\r\n";
			
			logger.info("Inoutput Variables as Field for Operation: " 
					+ opInstance.getIdShort() + " generated.");	
		}
		
		else {
			logger.debug("No Inoutput Variables found for Operation: " 
							+ opInstance.getIdShort() 
							+ ". Or an exception has occured.");
		}
		
		if (!outputVariables.isEmpty()) {
			
			for (IOperationVariable outputVariable : outputVariables) {
				fieldOpVar += "	protected static Property " + parent + "_"
							+ outputVariable.getValue().getIdShort() + "_"
							+ opInstance.getIdShort() + "_" + "Output" 
							+ " = new Property(\"" + outputVariable.getValue().getIdShort() 
							+ "\", ValueType." + outputVariable.getValueType() 
							+ ");\r\n";
			}
			fieldOpVar += "		\r\n\r\n";
			
			logger.info("Output Variables as Field for Operation: " 
					+ opInstance.getIdShort() + " generated.");	
		}
		
		else {
			logger.debug("No Output Variables found for Operation: " 
							+ this.opInstance.getIdShort() 
							+ ". Or an exception has occured.");
		}
		
		return fieldOpVar;
		
	}
	
	/**
	 * Generates the Lambda Content for the Operation.  
	 * 
	 * @return Returns a String containing the Code for the Lambda definition of the Operation. 
	 */
	private String generateLambdaContent() {
		
		String lambdaFuncBody = "";		// Writes the lambda function content. 
		String argsFormulation = "";	// Formulates arguments for operation call to SubmodelOp.java
		if (!inputVariables.isEmpty()) {
			
			// Variable Counter
			int varcount = 0;
			
			for (IOperationVariable inputVariable : inputVariables) {
				
				lambdaFuncBody += "			" + parent + "_" + inputVariable.getValue().getIdShort() + "_"
						+ opInstance.getIdShort() + "_" + "Input" + ".setValue((" 
						+ "arguments[" + varcount + "]"
						+ "));\r\n";
				
				argsFormulation += "(" + convertBaSyxToJavaTypes(inputVariable.getValueType()) + ") " + 
									parent + "_" + inputVariable.getValue().getIdShort() + "_" +
									opInstance.getIdShort() + "_" + "Input" + 
									".getValue()" + ",";
				
				varcount += 1;
				
			}
			
			// Deleting the comma used to separate args for the last argument. 
			if (argsFormulation != null && argsFormulation.length() > 0 && 
					argsFormulation.charAt(argsFormulation.length()-1) == ',') {
				argsFormulation = argsFormulation.substring(0, argsFormulation.length()-1);
			}
		}
		
		else {
			logger.debug("No InputVariables found for Operation: " + 
												opInstance.getIdShort());
		}
		
		
		String lambdaFuncReturn = "";	// Writes the return statement for the Lambda Function. 
		if (!inputVariables.isEmpty()) {
			
			if (outputVariables.isEmpty()) {
				
				lambdaFuncReturn = "			" + "dew." + parent + "_" + opInstance.getIdShort() + "(" + 
						argsFormulation	 + ");\r\n"
						+ "			return null;\r\n";
				
			}
			
			else {
				
				lambdaFuncReturn = "			return " + "dew." + parent + "_" + opInstance.getIdShort() + "(" + 
						argsFormulation	 + ");\r\n";
			}
			
		}
		
		else {
			if (outputVariables.isEmpty()) {
				
				lambdaFuncReturn = "			" + "dew." + parent + "_" + opInstance.getIdShort() + "();\r\n"
						+ "			return null;\r\n";
			}
			else {
				lambdaFuncReturn = "			return " + "dew." + parent + "_" + opInstance.getIdShort() + "();\r\n";
			}
		}		
		
		String lambdaFuncContent = lambdaFuncBody + lambdaFuncReturn;
		
		// Generating Just the Lambda Function for each Operation of the Submodel. 
		String lambdaFunc = "" + "		Function<Object[], Object> call" + parent + "_" + 
							opInstance.getIdShort() + " = (arguments) -> "
									+ "{\r\n" + "\r\n" +
							lambdaFuncContent + 
							"\r\n		};\r\n";
		
		return lambdaFunc;
		
	}
	
	/**
	 * Generates the code for the Input Variables for the respective Operation.
	 * 
	 * @return	Returns a String containing the code for the Input Variables for the Operation. 
	 */
	private String generateInputVariables() {
		
		
		// Generating the OperationVariables.
		String inputVariablesText = "		Collection<OperationVariable> " + parent + "_" + opInstance.getIdShort()
					+ "Input" + "s = new ArrayList<OperationVariable>();\r\n";
		
		if (!inputVariables.isEmpty()) {
			for(IOperationVariable inputVariable : inputVariables) { 
				inputVariablesText += "		OperationVariable " + parent + "_" +  inputVariable.getValue().getIdShort() + opInstance.getIdShort() + " = new OperationVariable();\r\n"
						+ "		" + parent + "_" + inputVariable.getValue().getIdShort() + "_" + opInstance.getIdShort() + "_" + "Input" + "." + "setKind(ModelingKind." + inputVariable.getValue().getKind() + ");\r\n"
						+ "		" + parent + "_" + inputVariable.getValue().getIdShort() + opInstance.getIdShort() + ".setValue(" + parent + "_" + inputVariable.getValue().getIdShort() + "_" + opInstance.getIdShort() + "_" + "Input" + ");\r\n"
						+ "		" + parent + "_" + opInstance.getIdShort() + "Input" + "s.add(" + parent + "_" + inputVariable.getValue().getIdShort() + opInstance.getIdShort() +");\r\n"
						+ "		" + parent + "_" + opInstance.getIdShort() + ".setInputVariables(" + parent + "_" + opInstance.getIdShort() + "Input" + "s" + "); \r\n";
			}
		} 
		else {
			inputVariablesText = "";
		}
		
		return inputVariablesText;
	}
	
	/**
	 * Generates the code for the Inoutput Variables for the respective Operation.
	 * 
	 * @return	Returns a String containing the code for the Inoutput Variables for the Operation. 
	 */
	/**
	 * Generates the code for the Inoutput Variables for the respective Operation.
	 * 
	 * @return	Returns a String containing the code for the Inoutput Variables for the Operation. 
	 */
	private String generateInOutputVariables() {
		
		String inoutputVariablesText = "		Collection<OperationVariable> " + parent + "_" + opInstance.getIdShort()
		+ "InOutput" + "s = new ArrayList<OperationVariable>();\r\n";
		
		if (!inoutputVariables.isEmpty()) {
			for(IOperationVariable inoutputVariable : inoutputVariables) {
				inoutputVariablesText += "		OperationVariable " + parent + "_" + inoutputVariable.getValue().getIdShort() + opInstance.getIdShort() + " = new OperationVariable();\r\n"
						+ "		" + parent + "_" + inoutputVariable.getValue().getIdShort() + "_" + opInstance.getIdShort() + "_" + "InOutput" + "." + "setKind(ModelingKind." + inoutputVariable.getValue().getKind() + ");\r\n"
						+ "		" + parent + "_" + inoutputVariable.getValue().getIdShort() + opInstance.getIdShort() + ".setValue(" + parent + "_" + inoutputVariable.getValue().getIdShort() + "_" + opInstance.getIdShort() + "_" + "InOutput" + ");\r\n"
						+ "		" + parent + "_" + opInstance.getIdShort() + "InOutput" + "s.add(" + parent + "_" + inoutputVariable.getValue().getIdShort() + opInstance.getIdShort() +");\r\n"
						+ "		" + parent + "_" + opInstance.getIdShort() + ".setInOutputVariables(" + parent + "_" + opInstance.getIdShort() + "InOutput" + "s" + "); \r\n";
			}
		}
		else {
			inoutputVariablesText = "";
		} 

		return inoutputVariablesText;
	}
	
	/**
	 * Generates the code for the Output Variables for the respective Operation.
	 * 
	 * @return	Returns a String containing the code for the Output Variables for the Operation. 
	 */
	private String generateOutputVariables() {
		
		String outputVariablesText = "		Collection<OperationVariable> " + parent + "_" + opInstance.getIdShort()
		+ "Output" + "s = new ArrayList<OperationVariable>();\r\n";
		
		if (!outputVariables.isEmpty()) {
			for(IOperationVariable outputVariable : outputVariables) {
				outputVariablesText += "		OperationVariable " + parent + "_" + outputVariable.getValue().getIdShort() + opInstance.getIdShort() + " = new OperationVariable();\r\n"
						+ "		" + parent + "_" + outputVariable.getValue().getIdShort() + "_" + opInstance.getIdShort() + "_" + "Output" + "." + "setKind(ModelingKind." + outputVariable.getValue().getKind() + ");\r\n"
						+ "		" + parent + "_" + outputVariable.getValue().getIdShort() + opInstance.getIdShort() + ".setValue(" + parent + "_" + outputVariable.getValue().getIdShort() + "_" + opInstance.getIdShort() + "_" + "Output" + ");\r\n"
						+ "		" + parent + "_" + opInstance.getIdShort() + "Output" + "s.add(" + parent + "_" + outputVariable.getValue().getIdShort() + opInstance.getIdShort() +");\r\n"
						+ "		" + parent + "_" + opInstance.getIdShort() + ".setOutputVariables(" + parent + "_" + opInstance.getIdShort() + "Output" + "s" + "); \r\n";
			}
		}
		
		else {
			outputVariablesText = "";
		} 
		
		return outputVariablesText;
	}
	
	/**
	 * Generates the ModelingInfo for the respective Operation. 
	 * 
	 * @return	Returns a String with the Modeling Information for the respective Operation. 
	 */
	private String generateModelingInfo() {
		
		String setKind = "";
		
		if (opInstance.getKind()!=null) {
			setKind = "		" + parent + "_" + opInstance.getIdShort() + 
					".setKind(ModelingKind." + opInstance.getKind() + ");\r\n";
			
			logger.info("Generated ModelingKind Info for Operation: " + 
					opInstance.getIdShort());
		}
		
		else {
			logger.info("Generated ModelingKind Info for Operation: " + 
					opInstance.getIdShort() + " not generated.");
		}
		
		return setKind;

	}
	
	/**
	 * Generates the Parental Relation Information for the respective Operation. 
	 * 
	 * @return	Returns a String with the parental relation for the respective Operation. 
	 */
	private String generateParentalRelation() {
		
		// Thread Check: To Understand who has called the function.
		// Test out this function of stackTrace.
		StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
		boolean isSubModelOperation = true;
		
		if(stackTraceElements[3].getMethodName() == "generateSubModelElementCollection") {
			isSubModelOperation = false;
		}
		
		// Check where Operation belongs. 
		String addOperation = "";
		if (isSubModelOperation) {
			addOperation = "		addSubmodelElement(" + parent + "_" + opInstance.getIdShort() + ");\r\n";
		}
		
		else {
			SubModelElementCollection parentSEC = opInstance.getParentSEC();
			String immediateParent = "";
			
			// Checking for Parents.
			if (parentSEC.getParentSEC()!=null) {
				immediateParent = parentSEC.getParentSEC().getIdShort();
			}
			
			else if (parentSEC.getParentSub()!=null) {
				immediateParent = parentSEC.getParentSub().getIdShort();
			}
			
			else {
				logger.error("SubModelElementCollection: " + parentSEC.getIdShort() 
				+ "has no Parent defined");
			}
			addOperation = "		" + immediateParent + "_" + parentSEC.getIdShort() + "value.add(" + parent + "_" + opInstance.getIdShort() + ");\r\n\r\n";  
		}

		return addOperation;

	}
	
	/**
	 * Generates the Semantic Reference for the respective Operation. 
	 * 
	 * @return	Returns a String with the Semantic Reference for the respective Operation. 
	 */
	private String generateSemanticReference() {
		
		// Adding the Semantic Reference.
		String semanticStr = "";	
		if (this.semanticKeys!=null && this.conceptDesc==null) {
			
			semanticStr += "		List<IKey> " + parent + "_" + opInstance.getIdShort().toLowerCase() 
					+ "Keys= new ArrayList<IKey>();\r\n";
			
			for (Key key : this.semanticKeys) {
				
				String basyxKeyElement = FileUtils.removeUnderScore(key.getType().toString());  
						
				semanticStr += "		" + parent + "_" + opInstance.getIdShort().toLowerCase() 
								+ "Keys.add(" + "new Key(KeyElements." + basyxKeyElement
								+ ", " + key.isLocal() + ", " + "\"" + key.getValue() + "\""
								+ ", " + "KeyType." + key.getIdType() + ")); \r\n";
			}
			
			semanticStr += "		Reference " + parent + "_" + opInstance.getIdShort() 
							+ "Ref = new Reference(" + parent + "_" + opInstance.getIdShort().toLowerCase() 
							+ "Keys" + ");\r\n"
							+ "		" + parent + "_" + opInstance.getIdShort() 
							+ ".setSemanticId(" + parent + "_" + opInstance.getIdShort() + "Ref); \r\n \r\n";
			
			logger.info("Semantic Id reference for Operation: " + opInstance.getIdShort() + "generated.");
		}
		
		else if (this.conceptDesc!=null && this.semanticKeys==null) {
			
			semanticStr += "		" + parent + "_" + opInstance.getIdShort() 
						+  ".setSemanticId(conceptDescriptions." + 
							this.conceptDesc.getIdShort() + ".getReference()); \r\n \r\n";
			
		}
		
		else {
			
			logger.debug("No Semantic Id reference for Operation: " + opInstance.getIdShort() + "found. Thus, not generated.");
			
		}
		
		return semanticStr;
	}
	
	/**
	 * Converts the BaSyx ValueTypes to Java Types. 
	 * 
	 * @param String		A String of the BaSyx Types.
	 * @return				A String containing the corresponding Java Type
	 * 						of the incoming BaSyx Type.  
	 */
	private String convertBaSyxToJavaTypes(String valueType) {
		
		switch(valueType) {
			
		case "Int8":
			return "Byte";
			
		case "Int16": case "UInt8":
			return "Short";
		
		case "Int32": case "UInt16": case "Integer": 
			return "Integer";
			
		case "NonNegativeInteger": case "NonPositiveInteger":
		case "PositiveInteger": case "NegativeInteger":
			return "BigInteger";
			
		case "Int64": case "UInt32":
			return "Long";
			
		case "UInt64":
			return "BigInteger";
			
		case "Double":
			return "Double";
		
		case "Float":
			return "Float";
		
		case "Boolean":
			return "Boolean";
			
		case "String":
			return "String";
			
		case "Duration": case "DayTimeDuration":
			return "Duration";
			
		case "YearMonthDuration":
			return "Duration";
			
		case "QName":
			return "QName";
			
		case "NOTATION":
			return "QName";
			
		case "AnyURI":
			return "String";
			
		case "LangString":
			return "LangString";
			
		case "Base64Binary": case "HexBinary":  
			return "Byte[]";
			
		case "GDay": case "GMonth": case "DateTime": case "GYearMonth": case "GYear": case "GMonthDay":
			return "XMLGregorianCalendar";
			
		case "None":  case "DateTimeStamp":
		case "AnyType": case "AnySimpleType": case "ID": case "IDREF": case "ENTITY":
			return "String";
			
		default: 
			return "Object";
		}
		
	}
	
}

