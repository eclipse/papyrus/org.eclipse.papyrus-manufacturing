/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.basyx.codegen.generator.modulefiles;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.aas.api.aas.AssetAdministrationShell;
import org.eclipse.aas.api.aas.parts.ConceptDictionary;
import org.eclipse.aas.api.reference.IReference;
import org.eclipse.aas.api.reference.Key;
import org.eclipse.aas.api.reference.Reference;
import org.eclipse.aas.api.submodel.parts.ConceptDescription;
import org.eclipse.aas.basyx.codegen.generator.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConceptDescriptionCreator {
	
	private static final Logger logger = LoggerFactory.getLogger(ConceptDescriptionCreator.class);
	private ConceptDictionary conceptDict;
	private String namespace;
	private List<ConceptDescription> conceptDescriptions;
	
	/**
	 * Constructor for the ConceptDescriptionCreator class. It receives:
	 * 	1. An AAS Instance as parameter.
	 * 	2. An Asset Instance as parameter. 
	 * 
	 * and initializes the class. 
	 * 
	 * @param aas
	 * @param namespace
	 */
	public ConceptDescriptionCreator(AssetAdministrationShell aas, String namespace) {
		
		this.namespace = namespace;
		
		// Try Catch block for initializing the ConceptDictionary. 
		try {
			this.conceptDict = aas.getConceptDictionary();
		}
		
		catch (NullPointerException e) {
			logger.error("Asset Administration Shell does not contain a ConceptDictionary");
		}

		// Try Catch block for initializing the ConceptDescriptions inside the ConceptDictionary. 
		try {
			this.conceptDescriptions = this.conceptDict.getConceptDescriptions();
		}
		
		catch (NullPointerException e) {
			logger.error("ConceptDictionary does not contain any ConceptDescriptions.");
		}
		
		logger.info("ConceptDescriptionCreator Initialised for AAS: " 
				+ aas.getIdShort() + " and namespace: " + this.namespace);
		
	}
	
	/**
	 * Generates the ConceptDescriptions in form of a ConceptDictionary. 
	 * 
	 * [Note:] Usage of the ConceptDictionary is according to the Details of the
	 * Asset Administration Shell Standard V2. The version 3 of the metamodel 
	 * do not contain the ConceptDictionary. 
	 * 
	 * However, since BaSyx supports the V2. This code generator uses the 
	 * ConceptDictionary. As soon as BaSyx is conformant to V3, the code generator
	 * has to / should be changed. 
	 * 
	 * @return	A String containing the entire ConceptDescription class. 
	 */
	public String createConceptDescriptions() {
		
		String licenseHeader = 
				"/*******************************************************************************\n"
				+ " * Copyright (c) 2023 DFKI.\n"
				+ " *\n"
				+ " * This program and the accompanying materials\n"
				+ " * are made available under the terms of the Eclipse Public License 2.0\n"
				+ " * which accompanies this distribution, and is available at\n"
				+ " * https://www.eclipse.org/legal/epl-2.0/\n"
				+ " *\n"
				+ " * SPDX-License-Identifier: EPL-2.0\n"
				+ " *\n"
				+ " * Contributors:\n"
				+ " *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>\n"
				+ " *******************************************************************************/"
				+ "\r\n";
		
		String conceptFields = "";
		String conceptDescriptions = "";
		
		for (ConceptDescription conceptDesc : this.conceptDescriptions) {
			conceptFields += generateConceptField(conceptDesc);
			conceptDescriptions += generateConceptDescription(conceptDesc);
		}
		
		String conceptDescriptionBody = 
				
				"package " + this.namespace + ".module;\r\n"
				+ "import java.util.ArrayList;\r\n"
				+ "import java.util.Collection; \r\n"
				+ "import java.util.List;\r\n"
				+ "import org.eclipse.basyx.aas.metamodel.map.parts.ConceptDictionary;\r\n"
				+ "import org.eclipse.basyx.submodel.metamodel.api.identifier.IdentifierType;\r\n"
				+ "import org.eclipse.basyx.submodel.metamodel.api.reference.IKey;\r\n"
				+ "import org.eclipse.basyx.submodel.metamodel.map.identifier.Identifier;\r\n"
				+ "import org.eclipse.basyx.submodel.metamodel.map.parts.ConceptDescription;\r\n"
				+ "import org.eclipse.basyx.submodel.metamodel.map.reference.Key; \r\n"
				+ "import org.eclipse.basyx.submodel.metamodel.api.reference.enums.KeyElements; \r\n"
				+ "import org.eclipse.basyx.submodel.metamodel.api.reference.enums.KeyType;\r\n"
				+ "import org.eclipse.basyx.submodel.metamodel.map.reference.Reference;\r\n"
				+ "\r\n"
				+ "\r\n"
				+ "/** Holds all concept descriptions known to this AAS. */\r\n"
				+ "public class ConceptDescriptions extends ConceptDictionary {\r\n"
				+ "\r\n"
				+ conceptFields
				+ "    \n"
				+ "    /**\n"
				+ "     * Creates all concept descriptions and stores references to them in fields.\n"
				+ "     */\n"
				+ "    public ConceptDescriptions() {\n"
				+ "        super();\n"
				+ "        \r\n"
				+ conceptDescriptions
				+ "\r\n"
				+ "   }\r\n"
				+ "}\r\n";
		
		String conceptDescription = licenseHeader + conceptDescriptionBody;
		
		return conceptDescription;
		
	}
	
	/**
	 * Generates the field variables for the ConceptDescriptions created later. 
	 * 
	 * @return	A String of field variables for the generated ConceptDescription. 
	 */
	private String generateConceptField(ConceptDescription conceptDesc) {
		
		String conceptField = "";
		
			conceptField += 
					"    public final ConceptDescription " + conceptDesc.getIdShort() + "; \r\n";
		
		return conceptField;
		
	}
	
	/**
	 * Generates the ConceptDescriptions according to the BaSyx syntax.
	 * 
	 * @return	A String of the generated Concept Descriptions. 
	 */
	private String generateConceptDescription(ConceptDescription conceptDesc) {
		
		String conceptDescription = "";
		String isCaseOfText = "";
		
		/*
		 * Create a logic for calling the methods:
		 * 	- generateReferences(ConceptDescription conceptDesc)
		 *  - generateIsCaseOf(ConceptDescription conceptDesc)
		 */
		if (!conceptDesc.getIsCaseOf().isEmpty()) {
			isCaseOfText += generateIsCaseOf(conceptDesc);  
		}
		
		
		conceptDescription += 
				"        " + conceptDesc.getIdShort() + " = new ConceptDescription(\"" + 
							conceptDesc.getIdShort() + "\", new Identifier(IdentifierType." + 
							conceptDesc.getIdentification().getIdType() + ", \"" + 
							conceptDesc.getIdentification().getIdentifier() + "\"));\r\n" + 
				"        " + conceptDesc.getIdShort() + ".setCategory(\"" + 
							conceptDesc.getCategory() +"\");\n" +
							isCaseOfText +
				"        " + "addConceptDescription(" + conceptDesc.getIdShort() + ");\r\n" + 
				"        \r\n";
		
		return conceptDescription;
	}
	
	/**
	 * Generates References inside the ConceptDescriptions Class for the isCaseOf
	 * functionality in ConceptDescription. 
	 * 
	 * @param conceptDesc	An instance of the ConceptDescription class. 
	 * @return	A String containing the references with its keys. 
	 */
	private String generateReferences(ConceptDescription conceptDesc) {
		
		List<IReference> isCaseofReferences = new ArrayList<>();
		String referenceText = "";
		int countRef = 1;
		// Try Catch block for initializing the References for IsCaseOfs. 
		try {
			isCaseofReferences = conceptDesc.getIsCaseOf();
			
			/*
			 * [Problem]: Type mismatch: cannot convert from 
			 * java.util.List<io.adminshell.aas.v3.model.Reference> to 
			 * java.util.List<org.eclipse.aas.api.reference.Reference>.
			 * 
			 * Therefore, change in API needed.
			 * Add isCaseOf Supports in ConceptDescription class in API.
			 */
		}
		
		catch (NullPointerException e) {
			logger.error("This ConceptDescription: " + conceptDesc.getIdShort() + 
						"is not a CaseOf any Reference.");
		}
		
		for (IReference isCaseofReference : isCaseofReferences) {
			
			/*
			 * Bring in the content of generateKeys(IReference reference) here. 
			 */
			List<Key> referenceKeys = isCaseofReference.getKeys();
			String keyText = "";
			
			keyText += "        List<IKey> " + conceptDesc.getIdShort().toLowerCase() 
											+ "Keys" + countRef + " = new ArrayList<IKey>();\r\n";
			
			for (Key referenceKey : referenceKeys) {
				
				String basyxKeyElement = FileUtils.removeUnderScore(referenceKey.getType().toString());  
				
				keyText += "		" + conceptDesc.getIdShort().toLowerCase() 
								+ "Keys" + countRef + ".add(" + "new Key(KeyElements." + basyxKeyElement
								+ ", " + referenceKey.isLocal() + ", " + "\"" + referenceKey.getValue() + "\""
								+ ", " + "KeyType." + referenceKey.getIdType() + ")); \r\n";
				
			}
			
			referenceText += 
					keyText
					+ "        Reference " + conceptDesc.getIdShort().toLowerCase() 
					+ "Ref" + countRef + " = new Reference(" 
					+ conceptDesc.getIdShort().toLowerCase() + "Keys" + countRef
					+ ");\r\n"
					+ "\r\n";
			
			countRef++;
		}
		
		return referenceText;
	
	}
	
	/**
	 * Generates the entire isCaseOf code lines in accordance with BaSyx.  
	 *  
	 * @param conceptDesc	An instance of the ConceptDescription class. 
	 * @return	String containing the respective code lines to enable the 
	 * 			isCaseof functionality in BaSyx. 
	 */
	private String generateIsCaseOf(ConceptDescription conceptDesc) {
		
		List<IReference> isCaseofReferences = new ArrayList<>();
		String text = "";
		
		// Try Catch block for initializing the References for IsCaseOfs. 
		try {
			isCaseofReferences = conceptDesc.getIsCaseOf();
		}
		
		catch (NullPointerException e) {
			logger.error("This ConceptDescription: " + conceptDesc.getIdShort() + 
						"is not a CaseOf any Reference.");
		}
		
		int numberOfReferences = isCaseofReferences.size();
		
		text += "        Collection<Reference> " + conceptDesc.getIdShort() 
						+ "Collection = new ArrayList<Reference>();\r\n"; 
		
		for (int i=1; i<=numberOfReferences; i++) {
			text += "        " + conceptDesc.getIdShort() 
							+ "Collection.add(" + conceptDesc.getIdShort().toLowerCase() + "Ref" + i +");\r\n";
		}
		
		text += "        " + conceptDesc.getIdShort() + ".setIsCaseOf(" 
						+ conceptDesc.getIdShort() + "Collection);\r\n";
		
		return generateReferences(conceptDesc) + text;
		
	} 

}


