/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.basyx.codegen.generator;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.eclipse.aas.api.aas.AssetAdministrationShell;
import org.eclipse.aas.api.communications.AASEndpoint;
import org.eclipse.aas.api.communications.Endpoint;
import org.eclipse.aas.api.submodel.SubModel;
import org.eclipse.aas.basyx.codegen.generator.modulefiles.AASModuleCreator;
import org.eclipse.aas.basyx.codegen.generator.modulefiles.ConceptDescriptionCreator;
import org.eclipse.aas.basyx.codegen.generator.modulefiles.DataHandlerCreator;
import org.eclipse.aas.basyx.codegen.generator.submodel.DEWorkspaceCreator;
import org.eclipse.aas.basyx.codegen.generator.submodel.SubModelCreator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Project {

	private static final Logger logger = LoggerFactory.getLogger(Project.class);

	private AssetAdministrationShell aas;

	private SubModelCreator smc;
	private AASModuleCreator amc;
	private DataHandlerCreator dhc;
	private DEWorkspaceCreator dewc;
	private ConceptDescriptionCreator cdc;

	private Path currentWorkingDirectory = Paths.get(FileUtils.getCurrentWorkingDirectory());
	private String namespace = "de.dfki.Papyrus4Manufacturing.default";
	private String projectName = "Papyrus4ManufacturingProject";
	private Path projectFolder = Paths.get(projectName);

	/**
	 * Constructor for the Project Class. It receives an AAS instance as parameter
	 * and initializes the class.
	 *
	 * @param aas
	 */
	public Project(AssetAdministrationShell aas) {
		this.aas = aas;

		logger.info("Project Initialised for Asset Administration Shell: " + this.aas.getIdShort());
	}

	/**
	 * Returns the Project Name of the Project under consideration.
	 *
	 * @return A String containing the Project name.
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * Sets the project folder to export the files to. If not set, creates a folder with {@code projectName}
	 * in API's folder.
	 * @param projectFolder to export the files to.
	 */
	public void setProjectFolder(String projectFolder) {
		this.projectFolder = Paths.get(projectFolder);
	}

	/**
	 * Gets the {@code projectFolder} that'ss set for the Project.
	 * @return The Path of Project Folder in String Format.
	 */
	public String getProjectFolder() {
		return this.projectFolder.toString();
	}

	/**
	 * Sets the project name. This is also used as asset name.
	 * @param projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * Sets the namespace of the classes using the project name automatically.
	 * If not set, defaults to {@code de.dfki.Papyrus4Manufacturing.default}.
	 */
	public void setNamespaceFromProjectName() {
		this.namespace = projectName.toLowerCase().replace(" ", ".");
	}

	/**
	 * Returns the project name with lowercase and trimmed from spaces.
	 * @return
	 */
	public String getSafeProjectName() {
		return projectName.toLowerCase().replace(" ", "");
	}

	/**
	 * Returns the project name without spaces.
	 * @return
	 */
	public String getProjectNameWithoutSpace() {
		return projectName.replace(" ", "");
	}

	/**
	 * Sets the namespace of the project to be created. See
	 * {@link #setNamespaceFromProjectName()} to create it automatically.
	 * @param namespace to set.
	 */
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	/**
	 * Returns the namespace assigned to the project.
	 *
	 * @return A String containing the namespace assigned to the project.
	 */
	public String getNamespace() {
		return this.namespace;
	}

	/**
	 * Creates Directories for the generated project in the paths dynamically
	 * generated using the location user specifies and also the namespace.
	 * This function internally calls Static Utils function written specifically
	 * for this codebase.
	 *
	 */
	private void createFilePaths() {

//		FileUtils.createDirectories(projectFolder + "/src/" + "main/" + "java/" + FileUtils.getFullPathFromNamespace(namespace) + "/connection");

		FileUtils.createDirectories(projectFolder.resolve(Paths.get("src", "main", "java", FileUtils.getFullPathFromNamespace(namespace), "connection")).toString());

//		FileUtils.createDirectories(projectFolder + "/src/" + "main/" + "java/" + FileUtils.getFullPathFromNamespace(namespace) + "/module/submodels");

		FileUtils.createDirectories(projectFolder.resolve(Paths.get("src", "main", "java", FileUtils.getFullPathFromNamespace(namespace), "module", "submodels")).toString());

		List<SubModel> subModels = this.aas.getSubModels();
		for (SubModel subModel : subModels) {
//			FileUtils.createDirectories(projectFolder + "/src/" + "main/" + "java/" + FileUtils.getFullPathFromNamespace(namespace) + "/module/submodels/" + subModel.getIdShort().toLowerCase());
			FileUtils.createDirectories(projectFolder.resolve(Paths.get("src", "main", "java", FileUtils.getFullPathFromNamespace(namespace), "module", "submodels", subModel.getIdShort().toLowerCase())).toString());
		}

//		FileUtils.createDirectories(projectFolder + "/src/" + "main/" + "resources");

		FileUtils.createDirectories(projectFolder.resolve(Paths.get("src", "main", "resources")).toString());

		logger.info("Creating Filepaths successful!");
	}

	/**
	 * Writes the String of Connection Files Code returned from the respective
	 * functions of the DataHandlerCreator into the blank Connection Files
	 * generated.
	 */
	private void createConnectionFiles() {

		createFilePaths();

		dhc = new DataHandlerCreator(this.aas, this.aas.getAsset(), getNamespace());

//		String connectedDevicesUTF8 = FileUtils.encodeText2UTF8(dhc.createConnectedDevices());
//		String opcuaConnectorWrapperUTF8 = FileUtils.encodeText2UTF8(dhc.createOPCUAConnectorWrapper());
//		String httpConnectorWrapperUTF8 = FileUtils.encodeText2UTF8(dhc.createHTTPConnectorWrapper());
//		String opcuaVariableUTF8 = FileUtils.encodeText2UTF8(dhc.createOPCUAVariable());

		Path connectedDevicesPath = projectFolder.resolve(Paths.get("src", "main", "java", FileUtils.getFullPathFromNamespace(namespace), "connection", "ConnectedDevices.java"));
//		FileUtils.writeData(projectFolder + "/src/" + "main/" + "java/" + FileUtils.getFullPathFromNamespace(namespace) + "/connection/ConnectedDevices.java", connectedDevicesUTF8);
		FileUtils.writeData(connectedDevicesPath.toString(), dhc.createConnectedDevices());

		Path opcuaConnectorWrapperPath = projectFolder.resolve(Paths.get("src", "main", "java", FileUtils.getFullPathFromNamespace(namespace), "connection", "OPCUAConnectorWrapper.java"));
//		FileUtils.writeData(projectFolder + "/src/" + "main/" + "java/" + FileUtils.getFullPathFromNamespace(namespace) + "/connection/OPCUAConnectorWrapper.java", opcuaConnectorWrapperUTF8);
		FileUtils.writeData(opcuaConnectorWrapperPath.toString(), dhc.createOPCUAConnectorWrapper());

		Path httpConnectorWrapperPath = projectFolder.resolve(Paths.get("src", "main", "java", FileUtils.getFullPathFromNamespace(namespace), "connection", "HTTPConnectorWrapper.java"));
//		FileUtils.writeData(projectFolder + "/src/" + "main/" + "java/" + FileUtils.getFullPathFromNamespace(namespace) + "/connection/HTTPConnectorWrapper.java", httpConnectorWrapperUTF8);
		FileUtils.writeData(httpConnectorWrapperPath.toString(), dhc.createHTTPConnectorWrapper());

		Path opcuaVariablePath = projectFolder.resolve(Paths.get("src", "main", "java", FileUtils.getFullPathFromNamespace(namespace), "connection", "OpcUaVariable.java"));
//		FileUtils.writeData(projectFolder + "/src/" + "main/" + "java/" + FileUtils.getFullPathFromNamespace(namespace) + "/connection/OpcUaVariable.java", opcuaVariableUTF8);
		FileUtils.writeData(opcuaVariablePath.toString(), dhc.createOPCUAVariable());

		logger.info("Creating Connection Files successful!");
	}

	/**
	 * Writes the String of the Module files code returned from the respective
	 * functions of the AASModuleCreator into the blank Module Files generated.
	 */
	private void createModuleFiles() {
		createFilePaths();

		amc = new AASModuleCreator(this.aas, this.aas.getAsset(), getNamespace());

		cdc = new ConceptDescriptionCreator(this.aas, getNamespace());

//		String aasServerUTF8 = FileUtils.encodeText2UTF8(amc.createAASServer());
//		String deviceContextUTF8 = FileUtils.encodeText2UTF8(amc.createDeviceContext());
//		String aasModelsUTF8 = FileUtils.encodeText2UTF8(amc.createAASModels());
//		String settingsUTF8 = FileUtils.encodeText2UTF8(amc.createSettings());
//		String conceptDescriptionUTF8 = FileUtils.encodeText2UTF8(cdc.createConceptDescriptions());

		Path aasServerPath = projectFolder.resolve(Paths.get("src", "main", "java", FileUtils.getFullPathFromNamespace(namespace), "module", "AASServer.java"));
//		FileUtils.writeData(projectFolder + "/src/" + "main/" + "java/" + FileUtils.getFullPathFromNamespace(namespace) + "/module/AASServer.java", aasServerUTF8);
		FileUtils.writeData(aasServerPath.toString(), amc.createAASServer());

		Path deviceContextPath = projectFolder.resolve(Paths.get("src", "main", "java", FileUtils.getFullPathFromNamespace(namespace), "module", "DeviceContext.java"));
//		FileUtils.writeData(projectFolder + "/src/" + "main/" + "java/" + FileUtils.getFullPathFromNamespace(namespace) + "/module/DeviceContext.java", deviceContextUTF8);
		FileUtils.writeData(deviceContextPath.toString(), amc.createDeviceContext());

		Path aasModelsPath = projectFolder.resolve(Paths.get("src", "main", "java", FileUtils.getFullPathFromNamespace(namespace), "module", "AASModels.java"));
//		FileUtils.writeData(projectFolder + "/src/" + "main/" + "java/" + FileUtils.getFullPathFromNamespace(namespace) + "/module/AASModels.java", aasModelsUTF8);
		FileUtils.writeData(aasModelsPath.toString(), amc.createAASModels());

		Path settingsPath = projectFolder.resolve(Paths.get("src", "main", "java", FileUtils.getFullPathFromNamespace(namespace), "module", "Settings.java"));
//		FileUtils.writeData(projectFolder + "/src/" + "main/" + "java/" + FileUtils.getFullPathFromNamespace(namespace) + "/module/Settings.java", settingsUTF8);
		FileUtils.writeData(settingsPath.toString(), amc.createSettings());

		Path conceptDescriptionPath = projectFolder.resolve(Paths.get("src", "main", "java", FileUtils.getFullPathFromNamespace(namespace), "module", "ConceptDescriptions.java"));
//		FileUtils.writeData(projectFolder + "/src/" + "main/" + "java/" + FileUtils.getFullPathFromNamespace(namespace) + "/module/ConceptDescriptions.java", conceptDescriptionUTF8);
		FileUtils.writeData(conceptDescriptionPath.toString(), cdc.createConceptDescriptions());

		logger.info("Creating Module Files successful!");
	}

	/**
	 * Writes the String of the SubModel Files code returned from the respective
	 * functions of the SubModelCreator class into the blank SubModel Files
	 * generated.
	 */
	private void createSubModelFiles() {

		// List of Submodels in this Asset Administration Shell.
		List<SubModel> subModels = this.aas.getSubModels();

		for (SubModel subModel : subModels) {

			smc = new SubModelCreator(subModel, getNamespace());
			dewc = new DEWorkspaceCreator(subModel, getNamespace());

			// SubModel Files
//			FileUtils.writeData(projectFolder + "/src/" + "main/" + "java/" + FileUtils.getFullPathFromNamespace(namespace) + "/module/submodels/" + subModel.getIdShort().toLowerCase() + "/" +
//					subModel.getIdShort().replace(" ", "") + ".java", FileUtils.encodeText2UTF8(smc.createSubModelFile()));

//			FileUtils.writeData(projectFolder.resolve(Paths.get("src", "main", "java", FileUtils.getFullPathFromNamespace(namespace), "module", "submodels", subModel.getIdShort().toLowerCase(), subModel.getIdShort().replace(" ", "")+".java")).toString(), FileUtils.encodeText2UTF8(smc.createSubModelFile()));
			FileUtils.writeData(projectFolder.resolve(Paths.get("src", "main", "java", FileUtils.getFullPathFromNamespace(namespace), "module", "submodels", subModel.getIdShort().toLowerCase(), subModel.getIdShort().replace(" ", "")+".java")).toString(), smc.createSubModelFile());

			// SubModel Operation Files
//			FileUtils.writeData(projectFolder + "/src/" + "main/" + "java/" + FileUtils.getFullPathFromNamespace(namespace) + "/module/submodels/" + subModel.getIdShort().toLowerCase() + "/" +
//					"DynamicElementsWorkspace" + ".java", FileUtils.encodeText2UTF8(dewc.generateDEWorkspace()));

//			FileUtils.writeData(projectFolder.resolve(Paths.get("src", "main", "java", FileUtils.getFullPathFromNamespace(namespace), "module", "submodels", subModel.getIdShort().toLowerCase(), "DynamicElementsWorkspace.java")).toString(), FileUtils.encodeText2UTF8(dewc.generateDEWorkspace()));
			FileUtils.writeData(projectFolder.resolve(Paths.get("src", "main", "java", FileUtils.getFullPathFromNamespace(namespace), "module", "submodels", subModel.getIdShort().toLowerCase(), "DynamicElementsWorkspace.java")).toString(), dewc.generateDEWorkspace());

		}

		logger.info("Creating  Submodel Files successful!");
	}

	/**
	 * Generates the code for the Project File in String and writes this code in
	 * String format to the Project File.
	 */
	private void createProjectFile() {

		String text = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" +
				"<projectDescription>\r\n" +
				"	<name>" + getProjectName() + "</name>\r\n" +
				"	<comment></comment>\r\n" +
				"	<projects>\r\n" +
				"	</projects>\r\n" +
				"	<buildSpec>\r\n" +
				"		<buildCommand>\r\n" +
				"			<name>org.eclipse.jdt.core.javabuilder</name>\r\n" +
				"			<arguments>\r\n" +
				"			</arguments>\r\n" +
				"		</buildCommand>\r\n" +
				"   	<buildCommand>\r\n" +
				"			<name>org.eclipse.m2e.core.maven2Builder</name>\r\n" +
				"			<arguments>\r\n" +
				"			</arguments>\r\n" +
				"		</buildCommand>\r\n" +
				"	</buildSpec>\r\n" +
				"	<natures>\r\n" +
				"		<nature>org.eclipse.jdt.core.javanature</nature>\r\n" +
				"	</natures>\r\n" +
				"</projectDescription>\r\n";

//		String projectFileUTF = FileUtils.encodeText2UTF8(text);

//		FileUtils.writeData(projectFolder + ".project", projectFileUTF);
		FileUtils.writeData(projectFolder.resolve(".project").toString(), text);

		logger.info("Creating Project File successful!");

	}

	/**
	 * Generates the code for the POM File in String and writes this code in
	 * String format to the POM File.
	 */
	private void createPOMFile() {

		String text = "<project xmlns=\"http://maven.apache.org/POM/4.0.0\"\r\n" +
				"	xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\r\n" +
				"	xsi:schemaLocation=\"http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd\">\r\n" +
				"	<modelVersion>4.0.0</modelVersion>\r\n" +
				"\r\n" +
				"	<groupId>de.dfki.Papyrus4Manufacturing</groupId>\r\n" +
				"	<artifactId>" + getSafeProjectName() + "</artifactId>\r\n" +
				"	<version>0.1.0-SNAPSHOT</version>\r\n" +
				"	<name>" + getProjectName() + "</name>\r\n" +
				"\r\n" +
				"	<packaging>jar</packaging>\r\n" +
				"\r\n" +
				"  <properties>\r\n"
				+ "    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>\r\n"
				+ "    <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>\r\n"
				+ "  </properties>\r\n"
				+ "\r\n"
				+ "	<build>\r\n"
				+ "		<!-- specify custom source directory (default maven source folder is /src/main/java) -->\r\n"
				+ "		<sourceDirectory>src/main/java</sourceDirectory>\r\n"
				+ "\r\n"
				+ "		<plugins>\r\n"
				+ "			<!-- Add third-party licenses to generated-code -->\n"
				+ "			<plugin>\n"
				+ "				<groupId>org.codehaus.mojo</groupId>\n"
				+ "				<artifactId>license-maven-plugin</artifactId>\n"
				+ "				<version>2.0.0</version>\n"
				+ "				<executions>\n"
				+ "					<execution>\n"
				+ "						<goals>\n"
				+ "							<goal>add-third-party</goal>\n"
				+ "						</goals>\n"
				+ "					</execution>\n"
				+ "				</executions>\n"
				+ "			</plugin>\n"
				+ "			<!-- Compile Sources using Java 17 -->\r\n"
				+ "			<plugin>\r\n"
				+ "				<artifactId>maven-compiler-plugin</artifactId>\r\n"
				+ "				<version>3.8.1</version>\r\n"
				+ "				<configuration>\r\n"
				+ "					<source>17</source>\r\n"
				+ "					<target>17</target>\r\n"
				+ "				</configuration>\r\n"
				+ "				<executions>\r\n"
				+ "                    <!-- Attach the shade into the package phase -->\r\n"
				+ "                    <execution>\r\n"
				+ "                        <phase>package</phase>\r\n"
				+ "                        <configuration>\r\n"
				+ "                            <transformers>\r\n"
				+ "                                <transformer\r\n"
				+ "                                        implementation=\"org.apache.maven.plugins.shade.resource.ManifestResourceTransformer\">\r\n"
				+ "                                    <mainClass>" + getNamespace() + ".module.AASServer</mainClass>\r\n"
				+ "                                </transformer>\r\n"
				+ "                            </transformers>\r\n"
				+ "                        </configuration>\r\n"
				+ "                    </execution>\r\n"
				+ "                </executions>\r\n"
				+ "			</plugin>\r\n"
				+ "			<!-- Executable Jar -->\n"
				+ "			<plugin>\n"
				+ "				<artifactId>maven-assembly-plugin</artifactId>\n"
				+ "        		<version>3.3.0</version>\n"
				+ "        		<executions>\n"
				+ "          			<execution>\n"
				+ "            			<id>make-assembly</id>\n"
				+ "            			<phase>package</phase>\n"
				+ "            			<goals>\n"
				+ "              				<goal>single</goal>\n"
				+ "            			</goals>\n"
				+ "          			</execution>\n"
				+ "        		</executions>\n"
				+ "        		<configuration>\n"
				+ "          			<descriptorRefs>\n"
				+ "            			<descriptorRef>jar-with-dependencies</descriptorRef>\n"
				+ "          			</descriptorRefs>\n"
				+ "          			<finalName>${project.artifactId}</finalName>\n"
				+ "          			<appendAssemblyId>false</appendAssemblyId>\n"
				+ "          			<outputDirectory>${project.basedir}</outputDirectory>\n"
				+ "          			<archive>\n"
				+ "            			<manifest>\n"
				+ "                     	<mainClass>" + getNamespace() + ".module.AASServer</mainClass>\r\n"
				+ "            			</manifest>\n"
				+ "          			</archive>\n"
				+ "        		</configuration>\n"
				+ "			</plugin>\r\n"
				+ "		</plugins>\r\n"
				+ "	</build>\r\n"
				+ "\r\n"
				+ "	<dependencies>\r\n"
				+ "		<dependency>\r\n"
				+ "			<groupId>org.eclipse.basyx</groupId>\r\n"
				+ "			<artifactId>basyx.sdk</artifactId>\r\n"
				+ "			<version>1.2.0</version>\r\n"
				+ "		</dependency>\r\n"
				+ "		<dependency>\n"
				+ "			<groupId>com.festo.aas</groupId>\n"
				+ "			<artifactId>p4m-helpers</artifactId>\n"
				+ "			<version>1.0.4</version>\n"
				+ "		</dependency>\r\n"
				+ "	</dependencies>\r\n"
				+ "</project>";

//		String pomFileUTF = FileUtils.encodeText2UTF8(text);

//		FileUtils.writeData(projectFolder + "pom.xml", pomFileUTF);
		FileUtils.writeData(projectFolder.resolve("pom.xml").toString(), text);

		logger.info("Creating POM File successful!");

	}

	/**
	 * Generates the code for the Class File in String and writes this code in
	 * String format to the Class File.
	 */
	private void createClassFile() {

		Path targetClasses = Paths.get("target", "classes");
		Path targetTestClasses = Paths.get("target", "test-classes");
		Path mainFolder = Paths.get("src", "main", "java");
		Path testFolder = Paths.get("src", "test", "java");
		Path resourcesFolder = Paths.get("src", "main", "resources");
		Path source = Paths.get("src");


		String text = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n"
				+ "<classpath>\r\n"
				+ "	<classpathentry kind=\"con\" path=\"org.eclipse.jdt.launching.JRE_CONTAINER/org.eclipse.jdt.internal.debug.ui.launcher.StandardVMType/JavaSE-17\">\r\n"
				+ "		<attributes>\r\n"
				+ "			<attribute name=\"maven.pomderived\" value=\"true\"/>\r\n"
				+ "		</attributes>\r\n"
				+ "	</classpathentry>\r\n"
				+ "	<classpathentry kind=\"con\" path=\"org.eclipse.m2e.MAVEN2_CLASSPATH_CONTAINER\">\r\n"
				+ "		<attributes>\r\n"
				+ "			<attribute name=\"maven.pomderived\" value=\"true\"/>\r\n"
				+ "		</attributes>\r\n"
				+ "	</classpathentry>\r\n"
				+ "	<classpathentry kind=\"con\" path=\"org.eclipse.jdt.junit.JUNIT_CONTAINER/5\"/>\r\n"
//				+ "	<classpathentry excluding=\"**\" kind=\"src\" output=\"target/classes\" path=\"src/main/resources\">\r\n"
				+ "	<classpathentry excluding=\"**\" kind=\"src\" output=\"" + targetClasses.toString() + "\" path=\"" + resourcesFolder.toString() + "\">\r\n"
				+ "		<attributes>\r\n"
				+ "			<attribute name=\"maven.pomderived\" value=\"true\"/>\r\n"
				+ "		</attributes>\r\n"
				+ "	</classpathentry>\r\n"
//				+ "	<classpathentry kind=\"src\" output=\"target/classes\" path=\"src/main/java\">\r\n"
				+ "	<classpathentry kind=\"src\" output=\"" + targetClasses.toString() + "\" path=\"" + mainFolder.toString() + "\">\r\n"
				+ "		<attributes>\r\n"
				+ "			<attribute name=\"optional\" value=\"true\"/>\r\n"
				+ "			<attribute name=\"maven.pomderived\" value=\"true\"/>\r\n"
				+ "		</attributes>\r\n"
				+ "	</classpathentry>\r\n"
//				+ "	<classpathentry kind=\"src\" output=\"target/test-classes\" path=\"src/test/java\">\r\n"
				+ "	<classpathentry kind=\"src\" output=\"" + targetTestClasses.toString() + "\" path=\"" + testFolder.toString() + "\">\r\n"
				+ "		<attributes>\r\n"
				+ "			<attribute name=\"optional\" value=\"true\"/>\r\n"
				+ "			<attribute name=\"maven.pomderived\" value=\"true\"/>\r\n"
				+ "			<attribute name=\"test\" value=\"true\"/>\r\n"
				+ "		</attributes>\r\n"
				+ "	</classpathentry>\r\n"
//				+ "	<classpathentry kind=\"output\" path=\"target/classes\"/>\r\n"
				+ "	<classpathentry kind=\"output\" path=\"" + targetClasses.toString() + "\"/>\r\n"
				+ "</classpath>\r\n"
				+ "";

//		String classFileUTF = FileUtils.encodeText2UTF8(text);

//		FileUtils.writeData(projectFolder + ".classpath", classFileUTF);

		FileUtils.writeData(projectFolder.resolve(".classpath").toString(), text);

		logger.info("Creating Class File successful!");

	}

	/**
	 * Generates the code for the Launch File in String and writes this code in
	 * String format to the Launch File.
	 */
	private void createLaunchFile() {

		String text = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\r\n" +
				"<launchConfiguration type=\"org.eclipse.jdt.launching.localJavaApplication\"> \r\n" +
				"	<listAttribute key=\"org.eclipse.debug.core.MAPPED_RESOURCE_PATHS\"> \r\n" +
				"		<listEntry value=\"/" + projectName + "/src/" + "main/java/" + projectName.toLowerCase() + "/module/AASServer.java\"/> \r\n" +
				"	</listAttribute> \r\n" +
				"	<listAttribute key=\"org.eclipse.debug.core.MAPPED_RESOURCE_TYPES\"> \r\n" +
				"		<listEntry value=\"1\"/> \r\n" +
				"	</listAttribute> \r\n" +
				"	<booleanAttribute key=\"org.eclipse.jdt.launching.ATTR_EXCLUDE_TEST_CODE\" value=\"true\"/> \r\n" +
				"	<stringAttribute key=\"org.eclipse.jdt.launching.CLASSPATH_PROVIDER\" value=\"org.eclipse.m2e.launchconfig.classpathProvider\"/> \r\n" +
				"	<stringAttribute key=\"org.eclipse.jdt.launching.MAIN_TYPE\" value=\"" + projectName.toLowerCase() + ".module.AASServer\"/> \r\n" +
				"	<stringAttribute key=\"org.eclipse.jdt.launching.MODULE_NAME\" value=\"" + projectName + "\"/> \r\n" +
				"	<stringAttribute key=\"org.eclipse.jdt.launching.PROJECT_ATTR\" value=\"" + projectName + "\"/> \r\n" +
				"	<stringAttribute key=\"org.eclipse.jdt.launching.SOURCE_PATH_PROVIDER\" value=\"org.eclipse.m2e.launchconfig.sourcepathProvider\"/> \r\n" +
				"</launchConfiguration>\r\n" +
				"";

//		String launchFileUTF = FileUtils.encodeText2UTF8(text);

//		FileUtils.writeData(projectFolder + "AASServer.launch", launchFileUTF);
		FileUtils.writeData(projectFolder.resolve("AASServer.launch").toString(), text);

		logger.info("Creating Launch File successful!");
	}

	/**
	 * Generates the code for the ReadMe File in String and writes this code in
	 * String format to the ReadMe File.
	 */
	private void createReadMe() {

		List<SubModel> submodels = aas.getSubModels();
		AASEndpoint aasServer = aas.getAASEndpoint();

		String text = "# Welcome to " + projectName + "!\r\n"
				+ "\r\n"
				+ "This project contains all required files to run your AAS server.\r\n"
				+ "\r\n"
				+ "# Running\r\n"
				+ "To run the AAS without modifications, please right-click on **AASServer.java** in *"
						+ getSafeProjectName() + ".module* package and choose **Run As -> Java Application**. "
						+ "Then, you will see when the server is ready to be accessed on: "
						+ "**http://" + aasServer.getAddress() + ":" + aasServer.getPort() + "/" + "aas/**\r\n"
				+ "\r\n"
				+ "Another option could also be to cut the **AASServer.launch** created in the projectFolder and paste\r\n"
				+ "it in the following path - {PATH to eclipse-workspace}\\.metadata\\.plugins\\org.eclipse.debug.core\\.launches\r\n"
				+ "If the **AASServer.launch** is pasted in the above PATH, a direct click on the green play button in the eclipse\r\n"
				+ "workspace will launch the AAS. Note: the AAS Folder must be selected before clicking on the green play button.\r\n"
				+ "The AAS Server provides a REST interface. You can see the list of commands "
				+ "[here](https://app.swaggerhub.com/apis/BaSyx/basyx_submodel_http_rest_api/v1#/).\r\n"
				+ "Using the interface, you can invoke operations and/or get property values."
				+ "\r\n"
				+ "# Files\r\n"
				+ "\r\n"
				+ "## Sub Models\r\n"
				+ "\r\n"
				+ "The project contains the following submodels:\r\n"
				+ "\r\n";

				for (SubModel subModel : submodels) {
					text += " - " + subModel.getIdShort() + " (URL to access: *[http://" + aasServer.getAddress() + ":" + aasServer.getPort() + "/" +
							"aas/submodels/" + subModel.getIdShort() + "]"
							+ "(http://" + aasServer.getAddress() + ":" + aasServer.getPort() + "/" +
							"aas/submodels/" + subModel.getIdShort() + ")*)\r\n";
				}

				text += "\r\n"
				+ "You can find them inside *" + getSafeProjectName() + ".module.submodel* package.\r\n"
				+ "You can edit these files to tweak properties and operations. You may need to refer to "
				+ "[Basyx Documentation](https://app.swaggerhub.com/apis/BaSyx/basyx_submodel_http_rest_api/v1#/) to work with those files manually.\r\n"
				+ "\r\n"
				+ "Example illustrations of formats to make REST API Calls:\r\n"
				+ "#- POST Call Format (AAS Operations):\r\n"
				+ "\r\n"
				+ "		-	In case, AAS Operation is added to a SubModelElementCollection:\r\n"
				+ "			http://{host}/aas/submodels/{submodelName}/submodel/submodelElements/{submodelCollectionName}/{OperationName}/invoke	\r\n"
				+ "\r\n"
				+ "		- 	In case, AAS Operation is not added to a SubModelElementCollection:\r\n"
				+ "			http://{host}/aas/submodels/{submodelName}/submodel/submodelElements/{OperationName}/invoke \r\n"
				+ "\r\n"
				+ "# Example - POST Call Format (AAS Operations): An Operation with two Input Variables[Operand1 and Operand2] \r\n"
				+ "		-	{\n"
				+ "    		\"requestId\": \"{{$timestamp}}\",\n"
				+ "    		\"inputArguments\": [\n"
				+ "        		{\n"
				+ "            		\"modelType\": {\n"
				+ "                		\"name\": \"OperationVariable\"\n"
				+ "            		},\n"
				+ "            		\"value\": {\n"
				+ "                		\"idShort\": \"Operand1\",\n"
				+ "                		\"modelType\": {\n"
				+ "                    		\"name\": \"Property\"\n"
				+ "                		},\n"
				+ "                		\"kind\": \"Template\",\n"
				+ "                		\"valueType\": \"Integer\",\n"
				+ "                		\"value\": 30\n"
				+ "            		}\n"
				+ "        		},\n"
				+ "        		{\n"
				+ "            		\"modelType\": {\n"
				+ "                		\"name\": \"OperationVariable\"\n"
				+ "            		},\n"
				+ "            		\"value\": {\n"
				+ "                		\"idShort\": \"Operand2\",\n"
				+ "                		\"modelType\": {\n"
				+ "                    		\"name\": \"Property\"\n"
				+ "                		},\n"
				+ "                		\"kind\": \"Template\",\n"
				+ "                		\"valueType\": \"Integer\",\n"
				+ "                		\"value\": 50\n"
				+ "            		}\n"
				+ "        		}\n"
				+ "    		],\n"
				+ "    		\"inoutputArguments\": [],\n"
				+ "    		\"timeout\": 5000\n"
				+ "			}\n"
				+ "\r\n"
				+ "\r\n"
				+ "#- GET Call Format (AAS Properties): \r\n"
				+ "\r\n"
				+ "		- 	In case, AAS Properties is added to a SubModelElementCollection: \r\n"
				+ "			http://{host}/aas/submodels/{submodelIdShort}/submodel/submodelElements/{SubModelElementsCollectionName}/{propertyIdShort}/value \r\n"
				+ "\r\n"
				+ "		- 	In case, AAS Properties is not added to a SubModelElementCollection: \r\n"
				+ "			http://{host}/aas/submodels/{submodelIdShort}/submodel/submodelElements/{propertyIdShort}/value"
				+ "\r\n"
				+ "#- PUT Call Format (AAS Properties): \r\n"
				+ "\r\n"
				+ "		-	In case, AAS Properties is added to a SubModelElementCollection: \r\n"
				+ "			http://{host}/aas/submodels/{submodelIdShort}/submodel/submodelElements/{SubModelElementsCollectionName}/{propertyIdShort}/value \r\n"
				+ "\r\n"
				+ "		- In case, AAS Properties is not added to a SubModelElementCollection: \r\n"
				+ "			http://{host}/aas/submodels/{submodelIdShort}/submodel/submodelElements/{propertyIdShort}/value \r\n"
				+ "\r\n"
				+ "## Link\r\n"
				+ "\r\n"
				+ "If you linked two AAS projects, you can see pre-created methods to access remote API calls inside "
				+ "*" + getSafeProjectName() + ".module.submodel.link* package. You can use these methods using static calls.\r\n";

//			String readMeUTF = FileUtils.encodeText2UTF8(text);

//			FileUtils.writeData(projectFolder + "README.md", readMeUTF);
			FileUtils.writeData(projectFolder.resolve("README.md").toString(), text);

			logger.info("Creating ReadMe File successful!");
	}

	/**
	 * Generates the code for the Properties File in String and writes this code in
	 * String format to the Properties File.
	 */
	private void createPropertiesFile() {

		dhc = new DataHandlerCreator(this.aas, this.aas.getAsset(), getNamespace());

//		FileUtils.writeData(projectFolder + "src/" + "main/" + "resources/" + "default.properties", FileUtils.encodeText2UTF8(dhc.propertiesFile()));

//		FileUtils.writeData(projectFolder.resolve(Paths.get("src", "main", "resources", "default.properties")).toString(), FileUtils.encodeText2UTF8(dhc.propertiesFile()));
		FileUtils.writeData(projectFolder.resolve(Paths.get("src", "main", "resources", "default.properties")).toString(), dhc.propertiesFile());

		logger.info("Creating Properties File successful!");
	}

	/**
	 * Generates the code for the Logback File in String and writes this code in
	 * String format to the Logback File.
	 */
	private void createLogbackFile() {

		String text =
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n"
				+ " \r\n"
				+ "<configuration>\r\n"
				+ "  <appender name=\"STDOUT\" class=\"ch.qos.logback.core.ConsoleAppender\">\r\n"
				+ "    <encoder>\r\n"
				+ "      <pattern>%d{HH:mm:ss.SSS} [%thread] %-5level %logger{5} - %msg%n</pattern>\r\n"
				+ "    </encoder>\r\n"
				+ "  </appender>\r\n"
				+ "  \r\n"
				+ "  <root level=\"info\">          \r\n"
				+ "    <appender-ref ref=\"STDOUT\" />\r\n"
				+ "  </root>\r\n"
				+ "  \r\n"
				+ "  <logger name=\"org.apache\" level=\"info\"></logger>\r\n"
				+ "  <logger name=\"org.eclipse.milo\" level=\"info\"></logger>\r\n"
				+ "  <logger name=\"io.netty\" level=\"info\"></logger>\r\n"
				+ "</configuration>\r\n"
				+ "";

//		String logBackFileUTF = FileUtils.encodeText2UTF8(text);

//		FileUtils.writeData(projectFolder + "src/" + "main/" + "resources/" + "logback.xml", logBackFileUTF);
		FileUtils.writeData(projectFolder.resolve(Paths.get("src", "main", "resources", "logback.xml")).toString(), text);

		logger.info("Creating Logback File successful!");
	}

	/**
	 * The main gateway that the user uses to generates the BaSyx Code for his
	 * Asset Administration Shell that contains all the functionalities it is supposed to
	 * contain.
	 *
	 * When this function is called by the user, it internally calls many other
	 * private methods that triggers the process of code generation based on the
	 * details the user has mentioned.
	 *
	 * The Project Class is initialised by a parameter - The Asset Administration
	 * Shell. This Asset Administration Shell contains all the relevant details
	 * the generator class needs for it to generate the code.
	 *
	 */
	public void createProject() {


		createConnectionFiles();
		createModuleFiles();
		createSubModelFiles();
		createProjectFile();
		createClassFile();
		createLaunchFile();
		createPOMFile();
		createReadMe();
		createPropertiesFile();
		createLogbackFile();
		System.out.println("Done. Files are at: " + projectFolder);

	}
}

