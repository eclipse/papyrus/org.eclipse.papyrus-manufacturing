/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.basyx.codegen.generator.submodel.submodelelements;

import java.util.List;

import org.eclipse.aas.api.reference.Key;
import org.eclipse.aas.api.submodel.parts.ConceptDescription;
import org.eclipse.aas.api.submodel.submodelelement.SubModelElementCollection;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.Property;
import org.eclipse.aas.basyx.codegen.generator.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertyGenerator {
	
	private static final Logger logger = LoggerFactory.getLogger(PropertyGenerator.class);
	private Property propInstance;
	private List<Key> semanticKeys;
	
	private String parent;
	private ConceptDescription conceptDesc;
	
	/**
	 * Constructor for the PropertyGenerator Class. It receives a single instance
	 * of Property as parameter to generate its respective and necessary BaSyx Code. 
	 * 
	 * @param propInstance
	 */
	public PropertyGenerator(Property propInstance) {
		
		this.propInstance = propInstance;
		
		try {
			this.semanticKeys = propInstance.getSemanticIdentifier().getKeys();
		}
		
		catch (NullPointerException e) {
			logger.error("Null Pointer Exception in Semantic ID Declaration "
					+ "while initialising " + this.getClass().getSimpleName());
		}
		
		try {
			this.conceptDesc = this.propInstance.getSemanticDescription();
		}
		
		catch (NullPointerException e) {
			logger.error("Null Pointer Exception while fetching ConceptDescription for " + 
							"the semantic Id definition of the Property: " + 
										this.getClass().getSimpleName());
		}
		
		// Checking for Parents.
		if (propInstance.getParentSEC()!=null) {
			this.parent = propInstance.getParentSEC().getIdShort();
		}
		
		else if (propInstance.getParentSub()!=null) {
			this.parent = propInstance.getParentSub().getIdShort();
		}
		
		else {
			logger.error("Property: " + propInstance.getIdShort() 
						+ "has no Parent defined");
		}
		
		logger.info("PropertyGenerator Initialised for Property : " + propInstance.getIdShort());
		
	}
	
	/**
	 * Generates a property either in a SubModel or inside it's parent SubModelElementCollection. 
	 * This function generates:
	 * 
	 * 	-	Semantic Reference for the Property in consideration. 
	 * 	- 	Modeling Info for the Property in consideration.
	 * 	-	Parental Information for the Property in consideration. 
	 * 	- 	Value Delegates for the Property in case it is Dynamic.  
	 * 
	 * @return	Returns a String with the generated Property Code. 
	 */
	public String generateProperty() {
		
		String propText = ""
				+ "		Property " + parent + "_" + propInstance.getIdShort() + "= new Property();\r\n"
				+ "		" + parent + "_" + propInstance.getIdShort() + ".setIdShort(\"" + propInstance.getIdShort() +"\");\r\n" 
				+ "		" + parent + "_" + propInstance.getIdShort() + ".setValueType(ValueType." + propInstance.getValueType() + ");\r\n"
				+ generateSemanticReference()
				+ generateModelingInfo()
				+ generateParentalRelation()
				+ generateValueDelegates()
				+ "\r\n\r\n";
		
		logger.info("Property Code generated for Property: " + 
				propInstance.getIdShort());
		
		return propText;
		
	}
	
	/**
	 * Generates the Modeling Info for the Property. 
	 * 
	 * @return	Returns a String with generated code containing the Modeling Info. 
	 */
	private String generateModelingInfo() {
		
		String setKind = "";
		
		if (propInstance.getKind()!=null) {
			setKind = "		" + parent + "_" + propInstance.getIdShort() + 
					".setKind(ModelingKind." + propInstance.getKind() + ");\r\n";
			
			logger.info("Generated ModelingKind Info for Property: " + 
					propInstance.getIdShort());
		}
		
		else {
			logger.info("Generated ModelingKind Info for Property: " + 
					propInstance.getIdShort() + " not generated.");
		}
		
		return setKind;

	}
	
	/**
	 * Generates the Parental Relation for the Property. 
	 * 
	 * @return	Returns a String with generated code containing the parental 
	 * 			information for the Property. 
	 */
	private String generateParentalRelation() {
		
		// Thread Check: To Understand who has called the function.
		// Test out this function of stackTrace.
		StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
		boolean isSubModelProperty = true;
				
		if(stackTraceElements[3].getMethodName() == "generateSubModelElementCollection") {
			isSubModelProperty = false;
		}
		
		// Defines where the Property belongs to. 
		String addProperty = "";
		if (isSubModelProperty) {
			addProperty = "		addSubmodelElement(" + parent + "_" + propInstance.getIdShort() + ");\r\n";
			
			logger.info("Property " + propInstance.getIdShort() 
									+ " added to Submodel: " );
		}
		else {
			SubModelElementCollection parentSEC = propInstance.getParentSEC();
			String immediateParent = "";
			
			// Checking for Parents.
			if (parentSEC.getParentSEC()!=null) {
				immediateParent = parentSEC.getParentSEC().getIdShort();
			}
			
			else if (parentSEC.getParentSub()!=null) {
				immediateParent = parentSEC.getParentSub().getIdShort();
			}
			
			else {
				logger.error("Property: " + parentSEC.getIdShort() 
				+ "has no Parent defined");
			}
			
			addProperty = "		" + immediateParent + "_" + parentSEC.getIdShort() + "value.add(" 
								+ parent + "_" + propInstance.getIdShort() + ");\r\n\r\n"; 
			
			logger.info("Property " + propInstance.getIdShort() 
									+ " added to SubmodelElementCollection: " 
									+ propInstance.getParentSEC().getIdShort());
		}

		return addProperty;
		
	}
	
	/**
	 * Generates the Value Information for the {@code Property} in consideration.
	 * 
	 * @return	Returns a String with generated code containing the Value 
	 * 			Information for the {@code Property} in consideration.
	 */
	private String generateValueCode() {
		
		String setValue = "";
		
		if (propInstance.getValue()!=null) {
			setValue = "		" + parent + "_" + propInstance.getIdShort() + 
					".setValue(\"" + propInstance.getValue() + "\");\r\n";
			
			logger.info("Generated Value Info for Property: " + 
					propInstance.getIdShort());
		}
		
		else {
			logger.info("Value Info for Property: " + 
					propInstance.getIdShort() + " not generated.");
		}
		
		return setValue;
		
	}
	
	/**
	 * Generates the Semantic Reference for the Property in consideration. 
	 * 
	 * @return	Returns a String with generated code containing the semantic 
	 * 			references for the property. 
	 */
	private String generateSemanticReference() {
		
		// Adding the Semantic Reference.
		String semanticStr = "";	
		if (this.semanticKeys!=null && this.conceptDesc==null) {
			
			semanticStr += "		List<IKey> " + parent + "_" + propInstance.getIdShort().toLowerCase() 
					+ "Keys= new ArrayList<IKey>();\r\n";
			
			for (Key key : this.semanticKeys) {
				
				String basyxKeyElement = FileUtils.removeUnderScore(key.getType().toString());  
						
				semanticStr += "		" + parent + "_" + propInstance.getIdShort().toLowerCase() 
								+ "Keys.add(" + "new Key(KeyElements." + basyxKeyElement
								+ ", " + key.isLocal() + ", " + "\"" + key.getValue() + "\""
								+ ", " + "KeyType." + key.getIdType() + ")); \r\n";
			}
			
			semanticStr += "		Reference " + parent + "_" + propInstance.getIdShort() 
							+ "Ref = new Reference(" + parent + "_" + propInstance.getIdShort().toLowerCase() 
							+ "Keys" + ");\r\n"
							+ "		" + parent + "_" + propInstance.getIdShort() 
							+ ".setSemanticId(" + parent + "_" + propInstance.getIdShort() + "Ref); \r\n \r\n";
			
			logger.info("Semantic Id reference for Property: " + propInstance.getIdShort() + "generated.");
		}
		
		else if (this.conceptDesc!=null && this.semanticKeys==null) {
			
			semanticStr += "		" + parent + "_" + propInstance.getIdShort() 
						+  ".setSemanticId(conceptDescriptions." + 
					this.conceptDesc.getIdShort() + ".getReference()); \r\n \r\n";
			
		}
		
		else {
			
			logger.debug("No Semantic Id reference for Property: " + propInstance.getIdShort() + "found. Thus, not generated.");
			
		}
		
		return semanticStr;
	}
	
	/**
	 * Generates the Value Delegates for the Property under consideration in case
	 * the Property is declared as "Dynamic". 
	 * 
	 * @return	Returns a String with generated code containing the ValueDelegates
	 * 			installed on the respective Property.  
	 */
	private String generateValueDelegates() {
		
		String valueDelegate = "";
		String valueCode = "";
		
		String getterCode = "";
		String setterCode = "";
		
		if (this.propInstance.isDynamic()) {
			
			valueDelegate += 
					"		ValueDelegate<" + convertBaSyxToJavaTypes(propInstance.getValueType())
					+ "> valDel" + parent + "_" + propInstance.getIdShort() 
					+ " = ValueDelegate.installOn(" + parent + "_" + propInstance.getIdShort() 
					+ "); \r\n";
			
			getterCode += 
					"		valDel" + parent + "_" + this.propInstance.getIdShort() 
					+ ".setGetHandler(dew::get_" + parent + "_" + propInstance.getIdShort() + "); \r\n";
			
			setterCode += "";
			
			valueDelegate += getterCode + setterCode;
			
			return valueDelegate;
			
		}
		
		else {
			
			valueCode += "		" + parent + "_" + propInstance.getIdShort() + ".setValue(\"" + propInstance.getValue() +"\");\r\n"; 
			
			return valueCode;
		
		}
		
	}
	
	/**
	 * Converts the BaSyx ValueTypes to Java Types. 
	 * 
	 * @param String		A String of the BaSyx Types.
	 * @return				A String containing the corresponding Java Type
	 * 						of the incoming BaSyx Type.  
	 */
	private String convertBaSyxToJavaTypes(String valueType) {
		
		switch(valueType) {
			
		case "Int8":
			return "Byte";
			
		case "Int16": case "UInt8":
			return "Short";
		
		case "Int32": case "UInt16": case "Integer": 
			return "Integer";
			
		case "NonNegativeInteger": case "NonPositiveInteger":
		case "PositiveInteger": case "NegativeInteger":
			return "BigInteger";
			
		case "Int64": case "UInt32":
			return "Long";
			
		case "UInt64":
			return "BigInteger";
			
		case "Double":
			return "Double";
		
		case "Float":
			return "Float";
		
		case "Boolean":
			return "Boolean";
			
		case "String":
			return "String";
			
		case "Duration": case "DayTimeDuration":
			return "Duration";
			
		case "YearMonthDuration":
			return "Duration";
			
		case "QName":
			return "QName";
			
		case "NOTATION":
			return "QName";
			
		case "AnyURI":
			return "String";
			
		case "LangString":
			return "LangString";
			
		case "Base64Binary": case "HexBinary":  
			return "Byte[]";
			
		case "GDay": case "GMonth": case "DateTime": case "GYearMonth": case "GYear": case "GMonthDay":
			return "XMLGregorianCalendar";
			
		case "None":  case "DateTimeStamp":
		case "AnyType": case "AnySimpleType": case "ID": case "IDREF": case "ENTITY":
			return "String";
			
		default: 
			return "Object";
		}
		
	}

}
