/*****************************************************************************
 * Copyright (c) 2023 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Vincent Lorenzo (CEA LIST) vincent.lorenzo@cea.fr - Initial API and implementation
  *****************************************************************************/

package org.eclipse.papyrus.aas.tables.configurations.editors.cell;

import java.util.Collections;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.nebula.widgets.nattable.NatTable;
import org.eclipse.nebula.widgets.nattable.config.IConfigRegistry;
import org.eclipse.nebula.widgets.nattable.grid.GridRegion;
import org.eclipse.nebula.widgets.nattable.style.DisplayMode;
import org.eclipse.nebula.widgets.nattable.ui.action.IMouseAction;
import org.eclipse.nebula.widgets.nattable.ui.binding.UiBindingRegistry;
import org.eclipse.nebula.widgets.nattable.ui.matcher.IMouseEventMatcher;
import org.eclipse.nebula.widgets.nattable.ui.matcher.MouseEventMatcher;
import org.eclipse.papyrus.aas.Key;
import org.eclipse.papyrus.aas.Reference;
//import org.eclipse.papyrus.aas.ui.utils.IAASElementTypes;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.papyrus.infra.nattable.celleditor.config.ICellAxisConfiguration;
import org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager;
import org.eclipse.papyrus.infra.nattable.model.nattable.Table;
import org.eclipse.papyrus.infra.nattable.model.nattable.nattableaxis.IAxis;
import org.eclipse.papyrus.infra.nattable.model.nattable.nattableaxis.ITreeItemAxis;
import org.eclipse.papyrus.infra.nattable.model.nattable.nattableaxisconfiguration.AxisManagerRepresentation;
import org.eclipse.papyrus.infra.nattable.model.nattable.nattableaxisconfiguration.TableHeaderAxisConfiguration;
import org.eclipse.papyrus.infra.nattable.model.nattable.nattableaxisconfiguration.TreeFillingConfiguration;
import org.eclipse.papyrus.infra.nattable.model.nattable.nattableaxisprovider.AbstractAxisProvider;
import org.eclipse.papyrus.infra.nattable.model.nattable.nattableaxisprovider.NattableaxisproviderPackage;
import org.eclipse.papyrus.infra.nattable.mouse.action.AbstractCellMouseAction;
import org.eclipse.papyrus.infra.nattable.mouse.action.EmptyLineRowHeaderEventMatch;
import org.eclipse.papyrus.infra.nattable.tree.ITreeItemAxisHelper;
import org.eclipse.papyrus.infra.nattable.utils.AxisUtils;
import org.eclipse.papyrus.infra.nattable.utils.HeaderAxisConfigurationManagementUtils;
import org.eclipse.papyrus.infra.nattable.utils.NattableConfigAttributes;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * We created this configuration to respect the Papyrus Table Pattern, but we are not able to configure the
 * table with a customization registered on header
 *
 * This configuration allows to create a new element from a Single LeftClick on the RowHeader area
 */
public class CreateKeyFromHeaderCellEditorConfiguration implements ICellAxisConfiguration {


	public NatTable natTable;

	/**
	 * @see org.eclipse.papyrus.infra.nattable.configuration.IPapyrusNatTableConfiguration#getConfigurationId()
	 *
	 * @return
	 */
	@Override
	public String getConfigurationId() {
		return "org.eclipse.papyrus.aas.tables.configurations.editors.cell.CreateKeyFromHeaderCellEditorConfiguration"; //$NON-NLS-1$
	}

	/**
	 * @see org.eclipse.papyrus.infra.nattable.configuration.IPapyrusNatTableConfiguration#getConfigurationDescription()
	 *
	 * @return
	 */
	@Override
	public String getConfigurationDescription() {
		return "Create new Key on empty line"; //$NON-NLS-1$
	}

	/**
	 * @see org.eclipse.papyrus.infra.nattable.celleditor.config.ICellAxisConfiguration#handles(org.eclipse.papyrus.infra.nattable.model.nattable.Table, java.lang.Object)
	 *
	 * @param table
	 * @param axisElement
	 * @return
	 */
	@Override
	public boolean handles(Table table, Object axisElement) {
		// return Activator.SCHEDULER_TABLE_KIND_ID.equals(table.getTableKindId());
		// return false, because the framework is not able to register cell editor on header area.
		return false;
	}

	/**
	 * @see org.eclipse.papyrus.infra.nattable.celleditor.config.ICellAxisConfiguration#configureCellEditor(org.eclipse.nebula.widgets.nattable.config.IConfigRegistry, java.lang.Object, java.lang.String)
	 *
	 * @param configRegistry
	 * @param axis
	 * @param configLabel
	 */
	@Override
	public void configureCellEditor(IConfigRegistry configRegistry, Object axis, String configLabel) {
		final INattableModelManager manager = configRegistry.getConfigAttribute(NattableConfigAttributes.NATTABLE_MODEL_MANAGER_CONFIG_ATTRIBUTE, DisplayMode.NORMAL, NattableConfigAttributes.NATTABLE_MODEL_MANAGER_ID);
		final NatTable natTable = manager.getAdapter(NatTable.class);
		this.natTable = natTable;
		final UiBindingRegistry uiBindingRegistry = natTable.getUiBindingRegistry();
		uiBindingRegistry.registerSingleClickBinding(getMouseEventMatcher(), getMouseAction());
	}

	/**
	 * @return
	 */
	private IMouseEventMatcher getMouseEventMatcher() {
		return new EmptyLineRowHeaderEventMatch(SWT.None, GridRegion.ROW_HEADER, MouseEventMatcher.LEFT_BUTTON);
	}

	/**
	 * @return
	 */
	private IMouseAction getMouseAction() {
		return new CreateElementCellMouseAction();
	}


	public static Reference getStereotypeApplication(Element element) {
		Reference ref = null;
		if (element != null) {
			ref = UMLUtil.getStereotypeApplication(element, Reference.class);
		}
		return ref;

	}

	private Key getKeyApplication(Object commandresult) {
		Key key = null;
		if (commandresult != null) {
			key = UMLUtil.getStereotypeApplication((Element) commandresult, Key.class);
		}
		return key;
	}

	private class CreateElementCellMouseAction extends AbstractCellMouseAction {

		/**
		 * @see org.eclipse.papyrus.infra.nattable.mouse.action.AbstractCellMouseAction#doRun(org.eclipse.nebula.widgets.nattable.NatTable, org.eclipse.swt.events.MouseEvent, java.lang.Object, java.lang.Object)
		 *
		 * @param natTable
		 * @param event
		 * @param rowElement
		 * @param columnElement
		 */
		@Override
		public void doRun(NatTable natTable, MouseEvent event, final Object rowElement, final Object columnElement) {
			if (rowElement instanceof ITreeItemAxis && ((ITreeItemAxis) rowElement).getElement() == null) {
				final ITreeItemAxis currentAxis = (ITreeItemAxis) rowElement;
				final ITreeItemAxis parentAxis = currentAxis.getParent();

				final Object parentElement = parentAxis.getElement();
				if (parentElement instanceof TreeFillingConfiguration) {
					IAxis axisProvider = ((TreeFillingConfiguration) parentElement).getAxisUsedAsAxisProvider();
					Object representedElement = AxisUtils.getRepresentedElement(axisProvider);

					// for a real usage, a check about the filter configuration could be better to be sure the created class
					// will appears in the table

					// we will create a new class
					final INattableModelManager manager = natTable.getConfigRegistry().getConfigAttribute(NattableConfigAttributes.NATTABLE_MODEL_MANAGER_CONFIG_ATTRIBUTE, DisplayMode.NORMAL, NattableConfigAttributes.NATTABLE_MODEL_MANAGER_ID);
					final EObject creationParent = manager.getTable().getContext();
					Reference reference = getStereotypeApplication((Element) creationParent);
					if (reference != null) {
						final CreateElementRequest request = new CreateElementRequest(creationParent, ElementTypeRegistry.getInstance().getType("org.eclipse.papyrus.aAS.Key"));
						final IElementEditService service = ElementEditServiceUtils.getCommandProvider(reference);

						final ICommand cmd = service.getEditCommand(request);
						request.getEditingDomain().getCommandStack().execute(new GMFtoEMFCommandWrapper(cmd));
						Object commandresult = cmd.getCommandResult().getReturnValue();
						// Stereotype stereotype = ((Element) commandresult).getApplicableStereotype("AAS::Key");
						// ApplyStereotypeCommand applyCommand = new ApplyStereotypeCommand((Element) commandresult, stereotype, domain);
						// domain.getCommandStack().execute(applyCommand);
						// Object commandresult2 = applyCommand.getResult();
						Key key = getKeyApplication(commandresult);
						AbstractTransactionalCommand c = new AbstractTransactionalCommand(request.getEditingDomain(), "Reference and set its key2", null) {

							@Override
							protected CommandResult doExecuteWithResult(IProgressMonitor arg0, IAdaptable arg1)
									throws ExecutionException {

								// Reference reference = AASFactory.eINSTANCE.createReference();
								// Stereotype ster = org.eclipse.papyrus.uml.tools.utils.UMLUtil.getAppliedSuperstereotype(propertyElement, "AAS::HasSemantics");
								// propertyElement.setValue(ster, "semanticId", reference);
								Table table = manager.getTable();
								reference.getKey().add(key);

								final AbstractAxisProvider axisProvider = table.getCurrentRowAxisProvider();
								TableHeaderAxisConfiguration conf = (TableHeaderAxisConfiguration) HeaderAxisConfigurationManagementUtils.getRowAbstractHeaderAxisInTableConfiguration(manager.getTable());
								AxisManagerRepresentation rep = conf.getAxisManagers().get(0);
								// final IAxis axis = ITreeItemAxisHelper.createITreeItemAxis(null, null, commandresult, rep);
								// // IAxis axis1111 = ITreeItemAxisHelper.createITreeItemAxis(request.getEditingDomain(), parentAxis, commandresult, rep);
								// // NattableModelManager manager2 = new NattableModelManager(manager.getTable(), new EObjectSelectionExtractor(), true);
								// //
								// // IAxisManager rowMgr = manager.getRowAxisManager();
								// // if (rowMgr instanceof ICompositeAxisManager) {
								// // ((ICompositeAxisManager) rowMgr).updateAxisContents();
								// // }
								// // NattablePackage.eINSTANCE.getTable_CurrentColumnAxisProvider()
								// // Command setContextCommand = SetCommand.create(request.getEditingDomain(), manager.getTable(), NattablePackage.eINSTANCE.getTable_CurrentColumnAxisProvider(), manager.getTable().getColumnAxisProvidersHistory());
								//
								// Command addCommand = AddCommand.create(request.getEditingDomain(), parentAxis, NattableaxisPackage.eINSTANCE.getITreeItemAxis_Children(), Collections.singleton(axis));
								// // Command addCommand = AddCommand.create(request.getEditingDomain(), axisProvider, NattableaxisproviderPackage.eINSTANCE.getAxisProvider_Axis(), Collections.singleton(axis));
								// // NattableModelManagerFactory.INSTANCE.createNatTableModelManager(manager.getTable(), new EObjectSelectionExtractor());
								//
								//
								// // Command addCommand2 = AddCommand.create(request.getEditingDomain(), parentAxis, NattableaxisproviderPackage.eINSTANCE.getAxisProvider_Axis(), Collections.singleton(axis));
								// Command setContextCommand = SetCommand.create(request.getEditingDomain(), manager.getTable(), NattablePackage.eINSTANCE.getTable_Context(), creationParent);
								//
								// if (!setContextCommand.canExecute()) {
								// System.out.print("can not set context");//$NON-NLS-1$
								//
								// } else {
								// setContextCommand.execute();
								//
								// }
								// if (!addCommand.canExecute()) {
								// System.out.print("can not add command");//$NON-NLS-1$
								//
								// } else {
								// addCommand.execute();
								//
								// }
								return CommandResult.newOKCommandResult();
							}
						};





						// CompoundCommand cc = new CompoundCommand();
						// addTreeItemAxis(request.getEditingDomain(), axisProvider2, rep, commandresult, cc);

						if (c != null && c.canExecute()) {
							try {
								c.execute(null, null);

								// NattableModelManager manager = NattableModelManagerFactory.INSTANCE.createNatTableModelManager(this.table, new EObjectSelectionExtractor());
								// if (TableHelper.isTreeTable(manager.getTable())) {

								// manager2.refreshInUIThread();
								// }
							} catch (ExecutionException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							// ICommand command2 = new SetValueCommand(new SetRequest(hassematics, AASPackage.eINSTANCE.getHasSemantics_SemanticId(), reference));
							// domain.getCommandStack().execute(new GMFtoEMFCommandWrapper(command2));
							// }
						}

						// AbstractTransactionalCommand cc = new AbstractTransactionalCommand(request.getEditingDomain(), "Reference and set its key2", null) {
						//
						// @Override
						// protected CommandResult doExecuteWithResult(IProgressMonitor arg0, IAdaptable arg1)
						// throws ExecutionException {
						//
						// final AbstractAxisProvider axisProvider2 = manager.getTable().getCurrentRowAxisProvider();
						// TableHeaderAxisConfiguration conf = (TableHeaderAxisConfiguration) HeaderAxisConfigurationManagementUtils.getRowAbstractHeaderAxisInTableConfiguration(manager.getTable());
						// AxisManagerRepresentation rep = conf.getAxisManagers().get(0);
						// final IAxis axis = ITreeItemAxisHelper.createITreeItemAxis(request.getEditingDomain(), parentAxis, commandresult, rep);
						// // IAxis axis1111 = ITreeItemAxisHelper.createITreeItemAxis(request.getEditingDomain(), parentAxis, commandresult, rep);
						// Command addCommand = AddCommand.create(request.getEditingDomain(), axisProvider2, NattableaxisproviderPackage.eINSTANCE.getAxisProvider_Axis(), Collections.singleton(axis));
						//
						// if (!addCommand.canExecute()) {
						// System.out.print("The table can't be initialized");//$NON-NLS-1$
						//
						// }
						//
						// // final ResourceSet resourceSet = request.getEditingDomain().getResourceSet();
						// // // Bug 502160: Remove the resource from the resource set to execute the command without using the editing command stack
						// // Table table = manager.getTable();
						// //
						// // Resource resource = table.eResource();
						// // resourceSet.getResources().remove(resource);
						// // try {
						// // GMFUnsafe.write(request.getEditingDomain(), addCommand);
						// // } catch (InterruptedException e) {
						// // System.out.print(e);
						// // } catch (RollbackException e) {
						// // System.out.print(e);
						// // } finally {
						// // // Bug 502160: Re-add the removed resource before the command execute
						// // resourceSet.getResources().add(resource);
						// // }
						//
						//
						// return CommandResult.newOKCommandResult();
						// }
						// };
						//
						// if (cc != null && cc.canExecute()) {
						// try {
						// cc.execute(null, null);
						//
						// } catch (ExecutionException e) {
						// // TODO Auto-generated catch block
						// e.printStackTrace();
						// }
						// // ICommand command2 = new SetValueCommand(new SetRequest(hassematics, AASPackage.eINSTANCE.getHasSemantics_SemanticId(), reference));
						// // domain.getCommandStack().execute(new GMFtoEMFCommandWrapper(command2));
						// // }
						// }
						//
					}
				}

			}
		}
	}





	/**
	 * This allow to add the tree item axis.
	 *
	 * @param axisProvider
	 *            The axis provider.
	 * @param rep
	 *            The axis manager representation.
	 * @param object
	 *            The object to add.
	 */
	protected void addTreeItemAxis(final TransactionalEditingDomain domain, final AbstractAxisProvider axisProvider, final AxisManagerRepresentation rep, final Object object, final CompoundCommand command) {
		final IAxis axis = ITreeItemAxisHelper.createITreeItemAxis(null, null, object, rep);
		Command addCommand = AddCommand.create(domain, axisProvider, NattableaxisproviderPackage.eINSTANCE.getAxisProvider_Axis(), Collections.singleton(axis));
		command.append(addCommand);
	}
}
