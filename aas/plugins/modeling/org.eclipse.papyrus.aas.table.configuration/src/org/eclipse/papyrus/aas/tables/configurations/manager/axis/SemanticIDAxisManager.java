/*****************************************************************************
 * Copyright (c) 2015, 2016 CEA LIST, Christian W. Damus, and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Celine Janssens (ALL4TEC) celine.janssens@all4tec.net - Initial API and implementation
 *  Christian W. Damus - bug 476984
 *
 *****************************************************************************/
package org.eclipse.papyrus.aas.tables.configurations.manager.axis;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.infra.nattable.model.nattable.nattableaxis.ITreeItemAxis;
import org.eclipse.papyrus.infra.nattable.model.nattable.nattableaxisconfiguration.TreeFillingConfiguration;
import org.eclipse.papyrus.uml.nattable.manager.axis.UMLElementTreeAxisManagerForEventList;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Axis Manager for the Parameter Table in Papyrus RT
 * 
 * @author Céline JANSSENS
 *
 */
public class SemanticIDAxisManager extends UMLElementTreeAxisManagerForEventList {

	/**
	 * @see org.eclipse.papyrus.infra.emf.nattable.manager.axis.EObjectTreeAxisManagerForEventList#isAllowedContents(java.lang.Object, Object, TreeFillingConfiguration, int)
	 *
	 * @param objectToTest
	 * @param depth
	 * @return
	 */
	@Override
	public boolean isAllowedContents(Object objectToTest, Object semanticParent, TreeFillingConfiguration conf, int depth) {
		if (objectToTest instanceof EObject && UMLUtil.getBaseElement((EObject) objectToTest) != null) {
			return super.isAllowedContents(UMLUtil.getBaseElement((EObject) objectToTest), semanticParent, conf, depth);
		}
		return super.isAllowedContents(objectToTest, semanticParent, conf, depth);
	}

	/**
	 * @see org.eclipse.papyrus.infra.emf.nattable.manager.axis.EObjectTreeAxisManagerForEventList#createITreeItemAxis(org.eclipse.papyrus.infra.nattable.model.nattable.nattableaxis.ITreeItemAxis, java.lang.Object)
	 *
	 * @param parentAxis
	 * @param objectToAdd
	 * @return
	 */
	@Override
	protected ITreeItemAxis createITreeItemAxis(ITreeItemAxis parentAxis, Object objectToAdd) {
		if (objectToAdd instanceof EObject && UMLUtil.getBaseElement((EObject) objectToAdd) != null) {
			return super.createITreeItemAxis(parentAxis, UMLUtil.getBaseElement((EObject) objectToAdd));
		}
		return super.createITreeItemAxis(parentAxis, objectToAdd);
	}
}
