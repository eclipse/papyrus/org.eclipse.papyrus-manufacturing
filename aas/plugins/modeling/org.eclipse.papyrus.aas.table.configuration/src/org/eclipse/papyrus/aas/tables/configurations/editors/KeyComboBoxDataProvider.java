/*****************************************************************************
 * Copyright (c) 2021 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.aas.tables.configurations.editors;

/**
 * @author AS247872
 *
 */
/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are the property of CEA LIST, their use is subject to specific
 * agreement with the CEA LIST.
 *
 * Contributors:
 *  Vincent Lorenzo (CEA LIST) <vincent.lorenzo@cea.fr>
 *
 *****************************************************************************/



import java.util.Comparator;
import java.util.List;

import org.eclipse.nebula.widgets.nattable.data.convert.IDisplayConverter;
import org.eclipse.nebula.widgets.nattable.edit.editor.IComboBoxDataProvider;
import org.eclipse.papyrus.infra.nattable.model.nattable.Table;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Type;


/**
 * Data Provider used to get all available concepts
 */
public class KeyComboBoxDataProvider implements IComboBoxDataProvider {

	/**
	 * the context table
	 */
	protected final Table table;

	/**
	 * compartor used to sort elements proposed in the combo box
	 */
	protected final Comparator<? super Type> comparator;

	/**
	 *
	 * Constructor.
	 *
	 * @param table
	 *            the table on which this provider is working
	 * @param converter
	 *            the converter used to get a label for each element, in order to be able to sort them by alphabetic order
	 */
	public KeyComboBoxDataProvider(final Table table, final IDisplayConverter converter) {
		this.table = table;
		this.comparator = new TypeComparator(converter);
	}

	/**
	 * @see org.eclipse.nebula.widgets.nattable.edit.editor.IComboBoxDataProvider#getValues(int, int)
	 *
	 * @param columnIndex
	 * @param rowIndex
	 * @return
	 */
	@Override
	public List<?> getValues(int columnIndex, int rowIndex) {
		// list of all types taken from the table context.

		List<Type> types = KeyUtils.getAllTypes((Element) this.table.getContext());

		types.sort(this.comparator);
		return types;
	}

	/**
	 * This comparator use the display converter to sort elements proposed in the combo box
	 */
	private final class TypeComparator implements Comparator<Type> {

		/**
		 * The display converter used to get a label for each element
		 */
		private IDisplayConverter converter;

		private TypeComparator(final IDisplayConverter converter) {
			this.converter = converter;
		}

		/**
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 *
		 * @param o1
		 * @param o2
		 * @return
		 */
		@Override
		public int compare(Type o1, Type o2) {
			final String name1 = converter.canonicalToDisplayValue(o1).toString();
			final String name2 = converter.canonicalToDisplayValue(o2).toString();
			return name1.compareTo(name2);
		}
	}

}

