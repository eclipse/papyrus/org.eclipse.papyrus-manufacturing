package org.eclipse.papyrus.aas.tables.configurations.editors;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.nebula.widgets.nattable.config.IConfigRegistry;
import org.eclipse.nebula.widgets.nattable.edit.EditConfigAttributes;
import org.eclipse.nebula.widgets.nattable.edit.editor.IComboBoxDataProvider;
import org.eclipse.nebula.widgets.nattable.style.DisplayMode;
import org.eclipse.papyrus.aas.AASPackage;
import org.eclipse.papyrus.aas.tables.configurations.manager.provider.KeyTypeEnumComboBoxDataProvider;
import org.eclipse.papyrus.infra.nattable.celleditor.ActionComboBoxCellEditor;
import org.eclipse.papyrus.infra.nattable.celleditor.action.CompositeCellEditorButtonAction;
import org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager;
import org.eclipse.papyrus.infra.nattable.model.nattable.Table;
import org.eclipse.papyrus.infra.nattable.model.nattable.nattableaxisprovider.AbstractAxisProvider;
import org.eclipse.papyrus.infra.nattable.utils.AxisUtils;
import org.eclipse.papyrus.infra.nattable.utils.NattableConfigAttributes;
import org.eclipse.papyrus.uml.nattable.config.SingleUMLReferenceCellEditorConfiguration;
import org.eclipse.papyrus.uml.nattable.utils.UMLTableUtils;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;

/**
 * CellEditor Configuration for column representing the methods getSource and getTarget from {@link StickerTreeItemAxis}
 */
public class KeyTypeCellEditorConfiguration extends SingleUMLReferenceCellEditorConfiguration {

	/**
	 * The id of this editor.
	 */
	public static final String ID = "org.eclipse.papyrus.aas.tables.configurations.KeyTypeCellEditorConfiguration";//$NON-NLS-1$

	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.papyrus.infra.nattable.configuration.IPapyrusNatTableConfiguration#getConfigurationId()
	 */
	@Override
	public String getConfigurationId() {
		return ID;
	}

	/**
	 * @see org.eclipse.papyrus.uml.nattable.config.SingleUMLReferenceCellEditorConfiguration#getConfigurationDescription()
	 *
	 * @return
	 */
	@Override
	public String getConfigurationDescription() {
		return "Cell editor for Key Type modification"; //$NON-NLS-1$
	}

	/**
	 * @see org.eclipse.papyrus.uml.nattable.config.SingleUMLReferenceCellEditorConfiguration#handles(org.eclipse.papyrus.infra.nattable.model.nattable.Table, java.lang.Object)
	 *
	 * @param table
	 * @param axisElement
	 * @return
	 */
	@Override
	public boolean handles(Table table, Object axisElement) {
		Object object = AxisUtils.getRepresentedElement(axisElement);
		Boolean res = false;
		String id = object.toString();
		if (id.startsWith(UMLTableUtils.PROPERTY_OF_STEREOTYPE_PREFIX)) {
			Property prop = UMLTableUtils.getRealStereotypeProperty(table.getContext(), id);
			if (prop != null && prop.getOwner() != null && prop.getOwner() instanceof Stereotype) {
				Stereotype current = (Stereotype) prop.getOwner();
				EClass stereotypeDef = (EClass) current.getProfile().getDefinition(current);
				EStructuralFeature feature = stereotypeDef.getEStructuralFeature(prop.getName());
				res = feature == AASPackage.eINSTANCE.getKey_Type();
			}
		}

		return res;
		// return object != null;
	}

	/**
	 * @see org.eclipse.papyrus.uml.nattable.config.SingleUMLReferenceCellEditorConfiguration#configureCellEditor(org.eclipse.nebula.widgets.nattable.config.IConfigRegistry, java.lang.Object, java.lang.String)
	 *
	 * @param configRegistry
	 * @param axis
	 * @param configLabel
	 */
	@Override
	public void configureCellEditor(IConfigRegistry configRegistry, Object axis, String configLabel) {
		super.configureCellEditor(configRegistry, axis, configLabel);
		final Object axisElement = AxisUtils.getRepresentedElement(axis);
		final INattableModelManager modelManager = configRegistry.getConfigAttribute(NattableConfigAttributes.NATTABLE_MODEL_MANAGER_CONFIG_ATTRIBUTE, DisplayMode.NORMAL, NattableConfigAttributes.NATTABLE_MODEL_MANAGER_ID);
		final int selectedAxisIndex = AxisUtils.getUniqueSelectedAxisIndex(modelManager);
		// Always get the column axis provider for invert or non-invert table
		final AbstractAxisProvider axisProvider = modelManager.getTable().getCurrentColumnAxisProvider();
		EObject context = modelManager.getTable().getContext();
		modelManager.getColumnElement(selectedAxisIndex);
		// boolean isSource = AxisUtils.getRepresentedElement(axis) == SNCFNatTableParameterPackage.eINSTANCE.getStickerTreeItemAxis__GetSource();
		final IComboBoxDataProvider dataProvider;


		dataProvider = new KeyTypeEnumComboBoxDataProvider(AASPackage.eINSTANCE.getKeyElements());

		final List<IElementType> creatableElementType = new ArrayList<>();

		creatableElementType.add(AASElementTypesEnumerator.CONCEPTDESCRIPTION);
		// creatableElementType.add(AASElementTypesEnumerator.SUBMODEL);
		// creatableElementType.add(AASElementTypesEnumerator.ASSET);



		final ActionComboBoxCellEditor comboBoxCellEditor = new ActionComboBoxCellEditor(dataProvider);
		final CompositeCellEditorButtonAction actions = new CompositeCellEditorButtonAction();
		comboBoxCellEditor.setCellEditorButtonAction(actions);

		// create action
		final CreateKeyTypeCellEditorButtonAction createNewElement = new CreateKeyTypeCellEditorButtonAction(creatableElementType, false, context);
		createNewElement.setTooltipText("Create a new reference"); //$NON-NLS-1$
		actions.addAction(createNewElement);

		// select action
		final SelectKeyTypeCellEditorButtonAction selectexistingElement = new SelectKeyTypeCellEditorButtonAction(creatableElementType, false, context);
		selectexistingElement.setTooltipText("Select an existing reference"); //$NON-NLS-1$
		actions.addAction(selectexistingElement);

		// unset action
		// final UnsetCellEditorButtonAction unsetAction = new UnsetCellEditorButtonAction();
		// unsetAction.setTooltipText("Delete the semantic id"); //$NON-NLS-1$
		// actions.addAction(unsetAction);

		configRegistry.registerConfigAttribute(EditConfigAttributes.CELL_EDITOR, comboBoxCellEditor, DisplayMode.EDIT, configLabel);
	}

}
