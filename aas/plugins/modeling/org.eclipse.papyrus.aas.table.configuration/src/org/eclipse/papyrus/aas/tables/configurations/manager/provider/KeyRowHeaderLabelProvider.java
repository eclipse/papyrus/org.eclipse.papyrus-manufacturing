/*****************************************************************************
 * Copyright (c) 2023 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Vincent Lorenzo (CEA LIST) vincent.lorenzo@cea.fr - Initial API and implementation
  *****************************************************************************/

package org.eclipse.papyrus.aas.tables.configurations.manager.provider;


import org.eclipse.papyrus.aas.tables.configurations.Activator;
import org.eclipse.papyrus.infra.emf.nattable.provider.AbstractEmptyLineRowHeaderLabelProvider;
import org.eclipse.papyrus.infra.nattable.model.nattable.nattableaxisconfiguration.TreeFillingConfiguration;

/**
 * @author Vincent Lorenzo (CEA LIST) <vincent.lorenzo@cea.fr>
 *
 */
public class KeyRowHeaderLabelProvider extends AbstractEmptyLineRowHeaderLabelProvider {

	/**
	 * Constructor.
	 *
	 * @param tableKind
	 */
	public KeyRowHeaderLabelProvider() {
		super(Activator.TABLE_KIND_ID);
	}

	/**
	 * @see org.eclipse.papyrus.infra.emf.nattable.provider.AbstractEmptyLineRowHeaderLabelProvider#getCreationHeaderMessage(org.eclipse.papyrus.infra.nattable.model.nattable.nattableaxisconfiguration.TreeFillingConfiguration)
	 *
	 * @param configuration
	 * @return
	 */
	@Override
	protected String getCreationHeaderMessage(final TreeFillingConfiguration configuration) {

		if (configuration.getDepth() == 0) {// a check about the listen feature and the filter expression will be great in a real usage
			// configuration.get
			return "Create a new Reference"; //$NON-NLS-1$
		}
		return ""; //$NON-NLS-1$
	}

}
