/*****************************************************************************
 * Copyright (c) 2023 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Vincent Lorenzo (CEA LIST) vincent.lorenzo@cea.fr - Initial API and implementation
  *****************************************************************************/

package org.eclipse.papyrus.aas.tables.configurations.manager.cell;


import org.eclipse.papyrus.aas.tables.configurations.Activator;
import org.eclipse.papyrus.infra.nattable.manager.cell.AbstractEmptyAxisCellManager;

/**
 * @author Vincent Lorenzo (CEA LIST) <vincent.lorenzo@cea.fr>
 *
 */
public class KeyEmptyAxisCellManager extends AbstractEmptyAxisCellManager {

	/**
	 * Constructor.
	 *
	 * @param tableKindId
	 */
	public KeyEmptyAxisCellManager() {
		super(Activator.TABLE_KIND_ID);
	}

}
