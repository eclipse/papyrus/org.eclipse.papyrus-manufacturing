/*****************************************************************************
 * Copyright (c) 2015, 2016 CEA LIST, Christian W. Damus, and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Celine Janssens (ALL4TEC) celine.janssens@all4tec.net - Initial API and implementation
 *  Christian W. Damus - bug 476984
 *
 *****************************************************************************/
package org.eclipse.papyrus.aas.tables.configurations.manager.axis;

import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.nebula.widgets.nattable.NatTable;
import org.eclipse.papyrus.aas.tables.configurations.editors.cell.CreateKeyFromHeaderCellEditorConfiguration;
import org.eclipse.papyrus.infra.nattable.celleditor.config.ICellAxisConfiguration;
import org.eclipse.papyrus.infra.nattable.manager.axis.ITreeItemAxisComparator;
import org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager;
import org.eclipse.papyrus.infra.nattable.model.nattable.nattableaxis.ITreeItemAxis;
import org.eclipse.papyrus.infra.nattable.model.nattable.nattableaxisconfiguration.AxisManagerRepresentation;
import org.eclipse.papyrus.infra.nattable.model.nattable.nattableaxisconfiguration.TreeFillingConfiguration;
import org.eclipse.papyrus.infra.nattable.model.nattable.nattableaxisprovider.AbstractAxisProvider;
import org.eclipse.papyrus.infra.nattable.model.nattable.nattableaxisprovider.NattableaxisproviderPackage;
import org.eclipse.papyrus.infra.nattable.utils.FillingConfigurationUtils;
import org.eclipse.papyrus.uml.nattable.manager.axis.EmptyLineUMLElementTreeAxisManagerForEventList;
import org.eclipse.uml2.uml.util.UMLUtil;

public class SemanticIDAxisManager2 extends EmptyLineUMLElementTreeAxisManagerForEventList {


	/**
	 * the action used to create element from single left click on row header
	 */
	private static final ICellAxisConfiguration conf = new CreateKeyFromHeaderCellEditorConfiguration();


	@Override
	public void init(INattableModelManager manager, AxisManagerRepresentation rep, AbstractAxisProvider provider) {
		super.init(manager, rep, provider);
		this.currentFillingConfiguration = FillingConfigurationUtils.getTreeFillingConfiguration(getTable(), representedAxisManager);
		this.comparator = new ITreeItemAxisComparator(this.tableManager, this);
	}

	/**
	 *
	 * Constructor.
	 *
	 */
	public SemanticIDAxisManager2() {
		setCreateEmptyRow(true);
	}

	/**
	 * @see org.eclipse.papyrus.infra.emf.nattable.manager.axis.EObjectTreeAxisManagerForEventList#isAllowedContents(java.lang.Object, Object, TreeFillingConfiguration, int)
	 *
	 * @param objectToTest
	 * @param depth
	 * @return
	 */
	@Override
	public boolean isAllowedContents(Object objectToTest, Object semanticParent, TreeFillingConfiguration conf, int depth) {
		if (objectToTest instanceof EObject && UMLUtil.getBaseElement((EObject) objectToTest) != null) {
			return super.isAllowedContents(UMLUtil.getBaseElement((EObject) objectToTest), semanticParent, conf, depth);
		}
		return super.isAllowedContents(objectToTest, semanticParent, conf, depth);
	}

	/**
	 * @see org.eclipse.papyrus.infra.emf.nattable.manager.axis.EObjectTreeAxisManagerForEventList#createITreeItemAxis(org.eclipse.papyrus.infra.nattable.model.nattable.nattableaxis.ITreeItemAxis, java.lang.Object)
	 *
	 * @param parentAxis
	 * @param objectToAdd
	 * @return
	 */
	@Override
	protected ITreeItemAxis createITreeItemAxis(ITreeItemAxis parentAxis, Object objectToAdd) {
		if (objectToAdd instanceof EObject && UMLUtil.getBaseElement((EObject) objectToAdd) != null) {
			return super.createITreeItemAxis(parentAxis, UMLUtil.getBaseElement((EObject) objectToAdd));
		}
		return super.createITreeItemAxis(parentAxis, objectToAdd);
	}



	/**
	 * @see org.eclipse.papyrus.infra.nattable.manager.axis.AbstractTreeAxisManagerForEventList#managedHideShowCategoriesForDepth(java.util.List, java.util.List)
	 *
	 * @param depthToHide
	 * @param depthToShow
	 */
	@Override
	public void managedHideShowCategoriesForDepth(List<Integer> depthToHide, List<Integer> depthToShow) {
		super.managedHideShowCategoriesForDepth(depthToHide, depthToShow);
		// trick to be able to register action on row header on single left click
		// warning, it requires to hide categories in the table, if not, this method is not called
		conf.configureCellEditor(((NatTable) getTableManager().getAdapter(NatTable.class)).getConfigRegistry(), null, "");
	}

	/**
	 * @param notification
	 */
	@Override
	protected void manageAddNotification(Notification notification) {
		Object notifier = notification.getNotifier();
		Object newValue = notification.getNewValue();
		Object feature = notification.getFeature();
		// case 1 - the user added a new root axis by DnD
		if (feature == NattableaxisproviderPackage.eINSTANCE.getAxisProvider_Axis() && newValue instanceof ITreeItemAxis && ((ITreeItemAxis) newValue).getElement() instanceof EObject) {
			manageAddITreeItemAxisForSemanticElement((ITreeItemAxis) newValue);
			return;
		}

		// case 2 : the notifier is the context of the table and we have filling configuration for level 0
		if (FillingConfigurationUtils.hasTreeFillingConfigurationForDepth(getTable(), 0)) {
			for (final TreeFillingConfiguration current : FillingConfigurationUtils.getTreeFillingConfigurationForDepth(getTable(), representedAxisManager, 0)) {
				if (isProvidedByTreeFillingConfiguration(newValue, current, notifier)) {
					Set<ITreeItemAxis> parentRepresentation = this.managedElements.get(current);
					// we are on the root, so the set is null or its size is 1;
					Assert.isTrue(parentRepresentation == null || parentRepresentation.size() == 1);
					ITreeItemAxis parentConf = null;
					if (parentRepresentation == null) {
						parentConf = addObject(null, current);
					} else {
						parentConf = parentRepresentation.iterator().next();
					}
					// this allows to avoid the double same entry on the same level (especially for the stereotyped elements)
					if (!managedElements.containsKey(newValue)) {
						addObject(parentConf, newValue);
					}
				}
			}
			return;
		}

		// case 3 - the notifier is a known element
		if (this.managedElements.containsKey(notifier) && !this.managedElements.containsKey(newValue)) {
			final Set<ITreeItemAxis> itemAxisRepresentations = this.managedElements.get(notifier);
			// we need to add a child for each representation of its parent in the table
			for (final ITreeItemAxis curr : itemAxisRepresentations) {

				// we update the conf only when the parent of the representation has already been filled (so expanded)
				if (curr.getParent() == null || this.alreadyExpanded.contains(curr.getParent())) {
					// 1.1 : get the depth for the new value
					int nextDepth = getSemanticDepth(curr) + 1;

					// 1.2 verify than the new value is allowed
					if (FillingConfigurationUtils.hasTreeFillingConfigurationForDepth(getTable(), representedAxisManager, nextDepth)) {

						// we cross the possible confs for this level
						for (TreeFillingConfiguration conf : FillingConfigurationUtils.getTreeFillingConfigurationForDepth(getTable(), representedAxisManager, nextDepth)) {
							if (isProvidedByTreeFillingConfiguration(newValue, conf, notifier)) {
								// we must add the value
								ITreeItemAxis confRep = getITreeItemAxisRepresentingObject(curr.getChildren(), conf);


								if (confRep == null) {
									confRep = addObject(curr, conf);
								}
								// if the representation of the notifier has already been expanded, we need to add the new value to the list, if not it will be done during the expand of the notifier
								if (this.alreadyExpanded.contains(curr)) {
									addObject(confRep, newValue);
								}
							}
						}
					}
				}
			}
		}
	}

}
