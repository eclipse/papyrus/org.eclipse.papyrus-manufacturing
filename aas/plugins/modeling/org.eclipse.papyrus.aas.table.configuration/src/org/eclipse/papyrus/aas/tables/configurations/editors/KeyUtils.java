/*****************************************************************************
 * Copyright (c) 2021 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.aas.tables.configurations.editors;

import java.util.List;

import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.papyrus.aas.AASPackage;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Type;

/**
 * @author AS247872
 *
 */
public class KeyUtils {

	/**
	 * @param context
	 * @return
	 */
	public static List<Type> getAllTypes(Element context) {
		// getKeyElementsLiteral
		EEnum keyElements = AASPackage.eINSTANCE.getKeyElements();
		List<EEnumLiteral> literals = keyElements.getELiterals();
		return null;
	}

}
