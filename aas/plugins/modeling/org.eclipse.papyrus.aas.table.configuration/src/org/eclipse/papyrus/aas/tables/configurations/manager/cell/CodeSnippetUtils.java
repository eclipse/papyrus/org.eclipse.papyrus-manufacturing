/*******************************************************************************
 * Copyright (c) 2016 Zeligsoft (2009) Limited and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *  Young-Soo Roh - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.aas.tables.configurations.manager.cell;

import java.util.Collections;
import java.util.List;

import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.Trigger;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * Utility class for handing user code related actions.
 * 
 * @author Young-Soo Roh
 *
 */
public final class CodeSnippetUtils {

	/**
	 * 
	 * Constructor.
	 *
	 */
	private CodeSnippetUtils() {

	}



	/**
	 * Create guard.
	 * 
	 * @param owner
	 *            owner
	 * @return guard
	 */
	@SuppressWarnings("unchecked")
	public static Constraint createGuard(Namespace owner) {
		return createGuard(owner, Collections.EMPTY_LIST);
	}

	/**
	 * Ceate Guard.
	 * 
	 * @param owner
	 *            owner
	 * @param constrainedElements
	 *            constrained elements
	 * @return guard
	 */
	public static Constraint createGuard(Namespace owner, List<Element> constrainedElements) {
		Constraint result = owner.createOwnedRule(null);
		result.getConstrainedElements().addAll(constrainedElements);
		result.setSpecification(createOpaqueExpressionWithDefaultLanguage(owner));
		return result;
	}

	/**
	 * Get trigger guard.
	 * 
	 * @param trigger
	 *            trigger
	 * @return Trigger guard or null if not found
	 */
	@SuppressWarnings("unchecked")
	public static Constraint getTriggerGuard(Trigger trigger) {
		List<Constraint> rules = (List<Constraint>) trigger.eContainer().eGet(UMLPackage.Literals.NAMESPACE__OWNED_RULE);
		return rules.stream().filter(c -> c.getConstrainedElements().contains(trigger)).findFirst().orElseGet(() -> null);
	}

	/**
	 * Create Opauqe Expression with default language.
	 * 
	 * @param context
	 *            context
	 * @return Opaque Expression
	 */
	public static OpaqueExpression createOpaqueExpressionWithDefaultLanguage(Element context) {
		OpaqueExpression result = UMLFactory.eINSTANCE.createOpaqueExpression();
		result.getBodies().add("");

		return result;
	}


	/**
	 * Create Opauqe Expression with default language.
	 * 
	 * @param context
	 *            context
	 * @return Opaque Expression
	 */
	public static OpaqueBehavior createOpaqueBehaviorWithDefaultLanguage(Element context) {
		OpaqueBehavior result = UMLFactory.eINSTANCE.createOpaqueBehavior();
		result.getBodies().add("");

		return result;
	}
}
