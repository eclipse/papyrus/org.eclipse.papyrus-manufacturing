/*****************************************************************************
 * Copyright (c) 2021 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.aas.tables.configurations.editors;

import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.Window;
import org.eclipse.nebula.widgets.nattable.edit.gui.AbstractDialogCellEditor;
import org.eclipse.nebula.widgets.nattable.layer.cell.ILayerCell;
import org.eclipse.nebula.widgets.nattable.style.DisplayMode;
import org.eclipse.papyrus.aas.Asset;
import org.eclipse.papyrus.aas.ConceptDescription;
import org.eclipse.papyrus.aas.IdentifierType;
import org.eclipse.papyrus.aas.Key;
import org.eclipse.papyrus.aas.KeyElements;
import org.eclipse.papyrus.aas.KeyType;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.papyrus.infra.emf.utils.EMFHelper;
import org.eclipse.papyrus.infra.nattable.celleditor.action.AbstractOpenDialogCellEditorButtonAction;
import org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager;
import org.eclipse.papyrus.infra.nattable.model.nattable.nattableaxis.EObjectTreeItemAxis;
import org.eclipse.papyrus.infra.nattable.utils.NattableConfigAttributes;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.papyrus.infra.widgets.Activator;
import org.eclipse.papyrus.infra.widgets.editors.TreeSelectorDialog;
import org.eclipse.papyrus.uml.tools.providers.UMLContentProvider;
import org.eclipse.papyrus.uml.tools.providers.UMLFilteredLabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * @author AS247872
 *
 */
public class SelectKeyTypeCellEditorButtonAction extends AbstractOpenDialogCellEditorButtonAction {

	/**
	 * The elements we propose for the creation
	 */
	private List<IElementType> elementsForCreation;

	/**
	 * The selected element type for creation
	 */
	private IElementType selectedElementType = null;

	private SelectPropertyDialogCellEditor editor;
	/**
	 * The context to use for creation
	 */
	private EObject creationContext;

	/**
	 * Constructor.
	 *
	 * @param creatableElementType
	 * @param b
	 * @param context
	 */
	public SelectKeyTypeCellEditorButtonAction(List<IElementType> creatableElementType, boolean b, EObject context) {
		super();
		setImage(Activator.getDefault().getImage("/icons/browse_12x12.gif")); //$NON-NLS-1$
		setText(""); //$NON-NLS-1$
		setTooltipText("Select an existing Semantic id");
		this.elementsForCreation = creatableElementType;
		this.creationContext = context;
	}

	public static ConceptDescription getConceptDescritpion(Element element) {
		ConceptDescription conceptDescription = null;
		if (element != null) {
			conceptDescription = UMLUtil.getStereotypeApplication(element, ConceptDescription.class);
		}
		return conceptDescription;

	}

	public static Asset getAsset(Element element) {
		Asset conceptDescription = null;
		if (element != null) {
			conceptDescription = UMLUtil.getStereotypeApplication(element, Asset.class);
		}
		return conceptDescription;

	}

	public static Key getStereotypeApplication(Element element) {
		Key key = null;
		if (element != null) {
			key = UMLUtil.getStereotypeApplication(element, Key.class);
		}
		return key;

	}


	/**
	 * @see org.eclipse.papyrus.infra.nattable.celleditor.action.AbstractOpenDialogCellEditorButtonAction#createDialogCellEditor()
	 *
	 * @return
	 */
	@Override
	public AbstractDialogCellEditor createDialogCellEditor() {
		// this was to create and edit a new value using new PropertyEditorFactory();
		// return new PropertyDialogCellEditor();
		// we should rather propose an dialog to select an existing property.
		this.editor = new SelectPropertyDialogCellEditor(this.elementsForCreation, this.creationContext);
		return this.editor;

	}

	@Override
	public int runAction(SelectionEvent e) {

		openMenuForCreation(parent);
		if (this.selectedElementType != null) {
			final INattableModelManager modelManager = configRegistry.getConfigAttribute(NattableConfigAttributes.NATTABLE_MODEL_MANAGER_CONFIG_ATTRIBUTE, DisplayMode.NORMAL, NattableConfigAttributes.NATTABLE_MODEL_MANAGER_ID);
			final ILayerCell cell = this.cell;
			cell.getColumnIndex();
			cell.getRowIndex();
			Object key = modelManager.getRowElement(getRowIndex());
			this.creationContext = modelManager.getTable().getContext();
			final TransactionalEditingDomain editingDomain = getEditingDomain();
			final IElementEditService provider = ElementEditServiceUtils.getCommandProvider(this.creationContext);
			if (provider != null) {


				final AbstractTransactionalCommand abs = new AbstractTransactionalCommand(editingDomain, "Select an existing element", null) { //$NON-NLS-1$

					@Override
					protected CommandResult doExecuteWithResult(IProgressMonitor arg0, IAdaptable arg1) throws ExecutionException {


						Object previousCanonicalValue = SelectKeyTypeCellEditorButtonAction.this.originalCanonicalValue;

						int resIntermediare = SelectKeyTypeCellEditorButtonAction.super.runAction(e);
						Object value = SelectKeyTypeCellEditorButtonAction.this.editor.editedElement;
						SelectKeyTypeCellEditorButtonAction.this.originalCanonicalValue = value;
						if (resIntermediare == Window.OK) {
							// change the current type of the current key
							if (key instanceof EObjectTreeItemAxis) {
								EObject element = ((EObjectTreeItemAxis) key).getElement();
								if (element instanceof Element) {
									Key key2 = getStereotypeApplication((Element) element);
									ConceptDescription cd = null;
									Asset asset = null;
									if (value instanceof Element && getConceptDescritpion((Element) value) != null) {
										cd = getConceptDescritpion((Element) value);
										if (cd != null && cd.getIdentification() != null) {
											key2.setType(KeyElements.CONCEPT_DESCRIPTION);
											key2.setIdType(getKeyType(cd.getIdentification().getIdType()));
											key2.setValue(cd.getIdentification().getId());
										}
									}
									// if (value instanceof Element && getAsset((Element) value) != null) {
									// asset = getAsset((Element) value);
									// if (asset != null && asset.getIdentification() != null) {
									// key2.setType(KeyElements.ASSET);
									// key2.setIdType(getKeyType(asset.getIdentification().getIdType()));
									// key2.setValue(asset.getIdentification().getId());
									// }
									// }
								}

							}
							return CommandResult.newOKCommandResult(value);
						} else {
							setResult(CommandResult.newCancelledCommandResult());
							SelectKeyTypeCellEditorButtonAction.this.originalCanonicalValue = previousCanonicalValue;
							return getCommandResult();

						}
					}
				};
				getEditingDomain().getCommandStack().execute(GMFtoEMFCommandWrapper.wrap(abs));

				CommandResult res = abs.getCommandResult();


				if (res.getStatus().isOK()) {
					this.selectedElementType = null;
					return Window.OK;
				}
			}
		}
		this.selectedElementType = null;
		return Window.CANCEL;

	}

	private KeyType getKeyType(IdentifierType idType) {

		switch (idType.getValue()) {

		case IdentifierType.CUSTOM_VALUE:
			return KeyType.CUSTOM;
		case IdentifierType.IRI_VALUE:
			return KeyType.IRI;
		case IdentifierType.IRDI_VALUE:
			return KeyType.IRDI;
		}
		return KeyType.CUSTOM;
	}

	/**
	 *
	 * @param elementType
	 *            the element type to use for creation
	 */
	private void setSelectedElementType(IElementType elementType) {
		selectedElementType = elementType;
	}


	private void openMenuForCreation(final Composite parent) {
		if (elementsForCreation.size() == 0) {
			return;
		}
		if (elementsForCreation.size() == 1) {
			this.selectedElementType = elementsForCreation.get(0);
			return;
		}
		final Menu menu = new Menu(parent);
		final String ELEMENT_TYPE = "elementTypeId"; //$NON-NLS-1$

		for (IElementType current : this.elementsForCreation) {
			final MenuItem item = new MenuItem(menu, SWT.NONE);
			item.setText(current.getDisplayName());
			ImageDescriptor imgDesc = ImageDescriptor.createFromURL(current.getIconURL());
			item.setImage(org.eclipse.papyrus.infra.widgets.Activator.getDefault().getImage(imgDesc));

			// item.setImage(current.getImage());
			item.setData(ELEMENT_TYPE, current);
			item.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					setSelectedElementType((IElementType) item.getData(ELEMENT_TYPE));
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					// nothing to do
				}
			});
		}

		menu.setVisible(true);

		// The menu is blocking the thread
		Display display = Display.getDefault();
		while (menu.isVisible()) {
			try {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			} catch (Throwable ex) {
				// Activator.log.error(ex);
			}
		}
		if (!display.isDisposed()) {
			display.update();
		}
	}

	public class SelectPropertyDialogCellEditor extends AbstractDialogCellEditor {

		/**
		 * The edited element
		 */
		private Object editedElement;

		/**
		 * boolean indicating if the cell editor is closed or not
		 */
		private boolean isClosed = false;

		private List<IElementType> elementsForCreation;

		private EObject creationcontext;

		/**
		 * Constructor.
		 *
		 * @param elementsForCreation
		 * @param creationContext
		 */
		public SelectPropertyDialogCellEditor(List<IElementType> elementsForCreation, EObject creationContext) {
			this.elementsForCreation = elementsForCreation;
			this.creationcontext = creationContext;
		}



		protected Object createDialog() {

			UMLContentProvider contentProvider = null;
			String title = "";
			if (selectedElementType.equals(AASElementTypesEnumerator.CONCEPTDESCRIPTION)) {
				contentProvider = new KeyTypeContentProvider();
				title = "Select a ConceptDescription";
			}
			// if (selectedElementType.equals(AASElementTypesEnumerator.ASSET)) {
			// contentProvider = new KeyTypeContentProviderAsset();
			// title = "Select an Asset";
			// }
			ResourceSet resSet = EMFHelper.getResourceSet(this.creationcontext);
			TreeSelectorDialog dialog = new TreeSelectorDialog(Display.getCurrent().getActiveShell());
			dialog.setInput(resSet);
			if (this.elementsForCreation.size() > 0) {

				dialog.setContentProvider(contentProvider);
				dialog.setLabelProvider(new UMLFilteredLabelProvider());
				dialog.setTitle(title);



			}
			this.dialog = dialog;
			return dialog;
		}



		/**
		 * @see org.eclipse.nebula.widgets.nattable.edit.gui.AbstractDialogCellEditor#open()
		 *
		 * @return
		 */
		@Override
		public int open() {

			Object[] selected = null;
			if (((TreeSelectorDialog) dialog).open() == Window.OK) {
				Object oneSelected;
				TreeSelectorDialog mydialog = (TreeSelectorDialog) dialog;
				selected = mydialog.getResult();
				if ((selected != null) && (selected.length > 0)) {
					oneSelected = selected[0];

					if (oneSelected instanceof Classifier) {
						Classifier result = (Classifier) oneSelected;
						this.editedElement = result;
					}
				}
			}

			this.isClosed = true;
			if (selected != null) {
				return Window.OK;
			}
			return Window.CANCEL;
		}

		/**
		 * @see org.eclipse.nebula.widgets.nattable.edit.gui.AbstractDialogCellEditor#createDialogInstance()
		 *
		 * @return
		 */
		@Override
		public Object createDialogInstance() {

			this.dialog = createDialog();

			return this.dialog;
		}

		/**
		 * @see org.eclipse.nebula.widgets.nattable.edit.gui.AbstractDialogCellEditor#getDialogInstance()
		 *
		 * @return
		 */
		@Override
		public Object getDialogInstance() {
			return dialog;
		}

		/**
		 * @see org.eclipse.nebula.widgets.nattable.edit.gui.AbstractDialogCellEditor#getEditorValue()
		 *
		 * @return
		 */
		@Override
		public Object getEditorValue() {
			return this.editedElement;
		}


		/**
		 * @see org.eclipse.nebula.widgets.nattable.edit.gui.AbstractDialogCellEditor#setEditorValue(java.lang.Object)
		 *
		 * @param value
		 */
		@Override
		public void setEditorValue(Object value) {
			// no usage here
		}

		/**
		 * @see org.eclipse.nebula.widgets.nattable.edit.gui.AbstractDialogCellEditor#close()
		 *
		 */
		@Override
		public void close() {
			// nothing to do
		}

		/**
		 * @see org.eclipse.nebula.widgets.nattable.edit.gui.AbstractDialogCellEditor#isClosed()
		 *
		 * @return
		 */
		@Override
		public boolean isClosed() {
			return this.isClosed;
		}

	}
}
