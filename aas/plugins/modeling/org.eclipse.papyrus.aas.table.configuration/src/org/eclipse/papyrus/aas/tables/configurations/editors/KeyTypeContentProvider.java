/*****************************************************************************
 * Copyright (c) 2016 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Celine JANSSENS (ALL4TEC) celine.janssens@all4tec.net - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.aas.tables.configurations.editors;

import java.util.stream.Stream;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.aas.ConceptDescription;
import org.eclipse.papyrus.infra.emf.utils.EMFHelper;
import org.eclipse.papyrus.uml.tools.providers.UMLContentProvider;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.ElementImport;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLSwitch;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * 
 * Content Provider For Parameter Type
 * It filters the basic content to keep only the rootModel and add the list of default language primitives
 * Required when adding a new parameter to a Protocol Message
 * 
 * @author Céline JANSSENS
 *
 */
public class KeyTypeContentProvider extends UMLContentProvider {


	/**
	 * @see org.eclipse.papyrus.infra.widgets.providers.EncapsulatedContentProvider#getChildren(java.lang.Object)
	 *
	 * @param parentElement
	 * @return The list of Children
	 */
	@Override
	public Object[] getChildren(final Object parentElement) {
		return Stream.of(super.getChildren(parentElement))
				.filter(this::isNavigable)
				.toArray();
	}

	public static ConceptDescription getConceptDescritpion(Element element) {
		ConceptDescription conceptDescription = null;
		if (element != null) {
			conceptDescription = UMLUtil.getStereotypeApplication(element, ConceptDescription.class);
		}
		return conceptDescription;

	}

	@Override
	public boolean isValidValue(Object object) {
		EObject element = EMFHelper.getEObject(object);

		if ((element instanceof Classifier) && getConceptDescritpion((Classifier) element) != null) {
			return true;
		} else {
			return false;
		}
	}


	private boolean isNavigable(Object object) {
		EObject element = EMFHelper.getEObject(object);

		return element != null && NavigableElementSwitch.NAVIGABLE_ELEMENT.doSwitch(element);
	}

	private static class NavigableElementSwitch extends UMLSwitch<Boolean> {
		static final NavigableElementSwitch NAVIGABLE_ELEMENT = new NavigableElementSwitch();

		@Override
		public Boolean defaultCase(EObject object) {
			return false;
		}

		@Override
		public Boolean caseProperty(Property object) {
			return false;
		}

		@Override
		public Boolean casePackage(Package object) {
			return true;
		}

		@Override
		public Boolean caseClassifier(Classifier object) {
			return true;
		}

		@Override
		public Boolean casePackageImport(PackageImport object) {
			return true;
		}

		@Override
		public Boolean caseElementImport(ElementImport object) {
			return false;
		}
	}




}


