package org.eclipse.papyrus.aas.tables.configurations.editors;

import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.Window;
import org.eclipse.nebula.widgets.nattable.config.IConfigRegistry;
import org.eclipse.nebula.widgets.nattable.edit.gui.AbstractDialogCellEditor;
import org.eclipse.nebula.widgets.nattable.layer.cell.ILayerCell;
import org.eclipse.nebula.widgets.nattable.style.DisplayMode;
import org.eclipse.papyrus.aas.Asset;
import org.eclipse.papyrus.aas.ConceptDescription;
import org.eclipse.papyrus.aas.IdentifierType;
import org.eclipse.papyrus.aas.Key;
import org.eclipse.papyrus.aas.KeyElements;
import org.eclipse.papyrus.aas.KeyType;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.papyrus.infra.nattable.celleditor.IActionCellEditor;
import org.eclipse.papyrus.infra.nattable.celleditor.action.AbstractOpenDialogCellEditorButtonAction;
import org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager;
import org.eclipse.papyrus.infra.nattable.model.nattable.nattableaxis.EObjectTreeItemAxis;
import org.eclipse.papyrus.infra.nattable.properties.celleditor.PropertyDialogCellEditor;
import org.eclipse.papyrus.infra.nattable.utils.NattableConfigAttributes;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.papyrus.infra.widgets.Activator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.util.UMLUtil;



/**
 * Custom Button Action for concept creation
 */
public class CreateKeyTypeCellEditorButtonAction extends AbstractOpenDialogCellEditorButtonAction {

	/**
	 * The elements we propose for the creation
	 */
	private List<IElementType> elementsForCreation;

	/**
	 * The selected element type for creation
	 */
	private IElementType selectedElementType = null;

	/**
	 * The context to use for creation
	 */
	private EObject creationContext;

	/**
	 * if <code>true</code> the creation the button is action is available only when the cell is empty
	 */
	private boolean onlyOnEmptyCell;


	/**
	 *
	 * Constructor.
	 *
	 * @param elementsForCreation
	 *            The elements we propose for the creation
	 * @param onlyOnEmptyCell
	 *            if <code>true</code> the action is proposed only on empty cell
	 */
	public CreateKeyTypeCellEditorButtonAction(final List<IElementType> elementsForCreation, final boolean onlyOnEmptyCell, EObject context) {
		super();
		setImage(Activator.getDefault().getImage("/icons/Add_12x12.gif")); //$NON-NLS-1$
		setText(""); //$NON-NLS-1$
		setTooltipText("Create a new Reference");
		this.elementsForCreation = elementsForCreation;
		this.onlyOnEmptyCell = onlyOnEmptyCell;
		this.creationContext = context;

	}

	/**
	 * @see org.eclipse.papyrus.infra.nattable.celleditor.AbstractOpenDialogCellEditorButtonAction#createDialogCellEditor()
	 *
	 * @return
	 */
	@Override
	public AbstractDialogCellEditor createDialogCellEditor() {
		return new PropertyDialogCellEditor();
	}

	public static Key getStereotypeApplication(Element element) {
		Key key = null;
		if (element != null) {
			key = UMLUtil.getStereotypeApplication(element, Key.class);
		}
		return key;

	}

	public static Asset getAsset(Element element) {
		Asset conceptDescription = null;
		if (element != null) {
			conceptDescription = UMLUtil.getStereotypeApplication(element, Asset.class);
		}
		return conceptDescription;

	}

	public static ConceptDescription getConceptDescritpion(Element element) {
		ConceptDescription conceptDescription = null;
		if (element != null) {
			conceptDescription = UMLUtil.getStereotypeApplication(element, ConceptDescription.class);
		}
		return conceptDescription;

	}

	/**
	 * @see org.eclipse.papyrus.infra.nattable.celleditor.action.AbstractOpenDialogCellEditorButtonAction#runAction(org.eclipse.swt.events.SelectionEvent)
	 *
	 * @param e
	 * @return
	 */
	@Override
	public int runAction(SelectionEvent e) {
		openMenuForCreation(parent);
		if (this.selectedElementType != null) {
			final INattableModelManager modelManager = configRegistry.getConfigAttribute(NattableConfigAttributes.NATTABLE_MODEL_MANAGER_CONFIG_ATTRIBUTE, DisplayMode.NORMAL, NattableConfigAttributes.NATTABLE_MODEL_MANAGER_ID);
			final ILayerCell cell = this.cell;
			cell.getColumnIndex();
			cell.getRowIndex();
			Object key = modelManager.getRowElement(getRowIndex());
			this.creationContext = modelManager.getTable().getContext();
			final TransactionalEditingDomain editingDomain = getEditingDomain();
			final CreateElementRequest request = createCreateElementRequest(editingDomain, this.creationContext, this.selectedElementType, null);
			final IElementEditService provider = ElementEditServiceUtils.getCommandProvider(this.creationContext);
			if (provider != null) {
				final ICommand cmd = provider.getEditCommand(request);

				final AbstractTransactionalCommand abs = new AbstractTransactionalCommand(editingDomain, "Create and Edit new element", null) { //$NON-NLS-1$

					@Override
					protected CommandResult doExecuteWithResult(IProgressMonitor arg0, IAdaptable arg1) throws ExecutionException {
						cmd.execute(arg0, arg1);
						CommandResult res = cmd.getCommandResult();
						Object value = res.getReturnValue();
						Object previousCanonicalValue = CreateKeyTypeCellEditorButtonAction.this.originalCanonicalValue;
						CreateKeyTypeCellEditorButtonAction.this.originalCanonicalValue = value;
						int resIntermediare = CreateKeyTypeCellEditorButtonAction.super.runAction(e);
						if (resIntermediare == Window.OK) {
							// change the current type of the current key
							if (key instanceof EObjectTreeItemAxis) {
								EObject element = ((EObjectTreeItemAxis) key).getElement();
								if (element instanceof Element) {
									Key key2 = getStereotypeApplication((Element) element);
									ConceptDescription cd = null;
									Asset asset = null;
									if (value instanceof Element && getConceptDescritpion((Element) value) != null) {
										cd = getConceptDescritpion((Element) value);
										if (cd != null && cd.getIdentification() != null) {
											key2.setType(KeyElements.CONCEPT_DESCRIPTION);
											key2.setIdType(getKeyType(cd.getIdentification().getIdType()));
											key2.setValue(cd.getIdentification().getId());
										}
									}
									if (value instanceof Element && getAsset((Element) value) != null) {
										asset = getAsset((Element) value);
										if (asset != null && asset.getIdentification() != null) {
											key2.setType(KeyElements.ASSET);
											key2.setIdType(getKeyType(asset.getIdentification().getIdType()));
											key2.setValue(asset.getIdentification().getId());
										}
									}
								}

							}
							return CommandResult.newOKCommandResult(value);
						} else {
							setResult(CommandResult.newCancelledCommandResult());
							CreateKeyTypeCellEditorButtonAction.this.originalCanonicalValue = previousCanonicalValue;
							return getCommandResult();

						}
					}



				};
				getEditingDomain().getCommandStack().execute(GMFtoEMFCommandWrapper.wrap(abs));

				CommandResult res = abs.getCommandResult();
				if (res.getStatus().isOK()) {
					this.selectedElementType = null;
					return Window.OK;
				}
			}
		}
		this.selectedElementType = null;
		return Window.CANCEL;
	}




	/**
	 * This method opens an intermediate menu to choose the element to create (if {@link #elementsForCreation}'s size > 1)
	 *
	 * @param parent
	 *            the parent composite
	 */

	private KeyType getKeyType(IdentifierType idType) {

		switch (idType.getValue()) {

		case IdentifierType.CUSTOM_VALUE:
			return KeyType.CUSTOM;
		case IdentifierType.IRI_VALUE:
			return KeyType.IRI;
		case IdentifierType.IRDI_VALUE:
			return KeyType.IRDI;
		}
		return KeyType.CUSTOM;
	}

	private void openMenuForCreation(final Composite parent) {
		if (elementsForCreation.size() == 0) {
			return;
		}
		if (elementsForCreation.size() == 1) {
			this.selectedElementType = elementsForCreation.get(0);
			return;
		}
		final Menu menu = new Menu(parent);
		final String ELEMENT_TYPE = "elementTypeId"; //$NON-NLS-1$

		for (IElementType current : this.elementsForCreation) {
			final MenuItem item = new MenuItem(menu, SWT.NONE);
			item.setText(current.getDisplayName());
			ImageDescriptor imgDesc = ImageDescriptor.createFromURL(current.getIconURL());
			item.setImage(org.eclipse.papyrus.infra.widgets.Activator.getDefault().getImage(imgDesc));

			// item.setImage(current.getImage());
			item.setData(ELEMENT_TYPE, current);
			item.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					setSelectedElementType((IElementType) item.getData(ELEMENT_TYPE));
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					// nothing to do
				}
			});
		}

		menu.setVisible(true);

		// The menu is blocking the thread
		Display display = Display.getDefault();
		while (menu.isVisible()) {
			try {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			} catch (Throwable ex) {
				// Activator.log.error(ex);
			}
		}
		if (!display.isDisposed()) {
			display.update();
		}
	}

	/**
	 *
	 * @param elementType
	 *            the element type to use for creation
	 */
	private void setSelectedElementType(IElementType elementType) {
		selectedElementType = elementType;
	}

	/**
	 *
	 * @param creationContext
	 *            the element to use to create the new element
	 */
	protected void setCreationContext(final EObject creationContext) {
		this.creationContext = creationContext;
	}


	/**
	 * @see org.eclipse.papyrus.infra.nattable.properties.celleditor.action.CreateElementCellEditorButtonAction#createCreateElementRequest(org.eclipse.emf.transaction.TransactionalEditingDomain, org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.gmf.runtime.emf.type.core.IElementType, org.eclipse.emf.ecore.EReference)
	 *
	 * @param editingDomain
	 * @param container
	 * @param elementType
	 * @param containmentFeature
	 * @return
	 */

	protected CreateElementRequest createCreateElementRequest(TransactionalEditingDomain editingDomain, EObject container, IElementType elementType, EReference containmentFeature) {
		return new CreateElementRequest(editingDomain, container, elementType, containmentFeature);
	}

	@Override
	public boolean configureAction(final IActionCellEditor editor, final Composite parent, final Object originalCanonicalValue, final ILayerCell cell, final IConfigRegistry configRegistry) {
		super.configureAction(editor, parent, originalCanonicalValue, cell, configRegistry);
		if (getNattableModelManager() != null && getNattableModelManager().getTable() != null) {
			setCreationContext(getNattableModelManager().getTable().getOwner());
		}
		return isEnabled();
	}

	@Override
	public boolean isEnabled() {
		return true;
	}


}
