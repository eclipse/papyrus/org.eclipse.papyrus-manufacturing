/*****************************************************************************
 * Copyright (c) 2015, 2016 CEA LIST, Christian W. Damus, and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Celine Janssens (ALL4TEC) celine.janssens@all4tec.net - Initial API and implementation
 *  Christian W. Damus - bug 476984
 *
 *****************************************************************************/
package org.eclipse.papyrus.aas.tables.configurations.manager.axis;

import java.util.List;

import org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager;
import org.eclipse.papyrus.infra.nattable.utils.CellHelper;
import org.eclipse.papyrus.uml.nattable.manager.cell.StereotypePropertyCellManager;
import org.eclipse.papyrus.uml.nattable.utils.UMLTableUtils;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;

public class SemanticIDStereotypeProperty extends StereotypePropertyCellManager {


	@Override
	protected Object doGetValue(final Object columnElement, final Object rowElement, final INattableModelManager tableManager) {
		final List<Object> umlObjects = organizeAndResolvedObjects(columnElement, rowElement, null);
		if (umlObjects.size() == 2) {
			final Element el = (Element) umlObjects.get(0);
			final String id = (String) umlObjects.get(1);
			final Property prop = UMLTableUtils.getRealStereotypeProperty(el, id);
			final List<Stereotype> stereotypesWithThisProperty = UMLTableUtils.getAppliedStereotypesWithThisProperty(el, id);
			if (stereotypesWithThisProperty.size() == 1) {
				return el.getValue(stereotypesWithThisProperty.get(0), prop.getName());
			} else if (stereotypesWithThisProperty.size() > 1) {
				return SEVERAL_STEREOTYPES_WITH_THIS_FEATURE_ARE_APPLIED;
			}
		}
		return CellHelper.getUnsupportedCellContentsText();
	}
}
