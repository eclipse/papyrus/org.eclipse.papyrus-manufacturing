/*******************************************************************************
 * Copyright (c) 2016 Zeligsoft (2009) Limited and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *   Young-Soo Roh - Initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.aas.tables.configurations.manager.cell;

import java.util.Collections;

import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.papyrus.infra.nattable.manager.cell.AbstractCellManager;
import org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager;
import org.eclipse.papyrus.infra.nattable.utils.AxisUtils;
import org.eclipse.uml2.common.util.UML2Util;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Trigger;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.ValueSpecification;

/**
 * Trigger guard cell manager.
 * 
 * @author ysroh
 *
 */
public class TriggerGuardCellManager extends AbstractCellManager {

	/**
	 * @see org.eclipse.papyrus.infra.nattable.manager.cell.ICellManager#handles(java.lang.Object, java.lang.Object)
	 *
	 * @param columnElement
	 * @param rowElement
	 * @return
	 */
	@Override
	public boolean handles(Object columnElement, Object rowElement, INattableModelManager tableManager) {
		final Object row = AxisUtils.getRepresentedElement(rowElement);
		final Object column = AxisUtils.getRepresentedElement(columnElement);
		if (row instanceof Trigger && UMLPackage.Literals.TRANSITION__GUARD.equals(column)) {
			return true;
		}
		return false;
	}

	/**
	 * @see org.eclipse.papyrus.infra.nattable.manager.cell.AbstractCellManager#isCellEditable(java.lang.Object, java.lang.Object)
	 *
	 * @param columnElement
	 * @param rowElement
	 * @return
	 */
	@Override
	public boolean isCellEditable(Object columnElement, Object rowElement, INattableModelManager tableManager) {
		final Object row = AxisUtils.getRepresentedElement(rowElement);
		Object column = AxisUtils.getRepresentedElement(columnElement);
		if (row instanceof Trigger && UMLPackage.Literals.TRANSITION__GUARD.equals(column)) {
			return true;
		}
		return super.isCellEditable(columnElement, rowElement, tableManager);
	}

	/**
	 * @see org.eclipse.papyrus.infra.nattable.manager.cell.AbstractCellManager#doGetValue(java.lang.Object, java.lang.Object, org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager)
	 *
	 * @param columnElement
	 * @param rowElement
	 * @param tableManager
	 * @return
	 */
	@Override
	protected Object doGetValue(Object columnElement, Object rowElement, INattableModelManager tableManager) {
		Trigger trigger = (Trigger) AxisUtils.getRepresentedElement(rowElement);
		Constraint guard = getTriggerGuard(trigger, false);
		if (guard != null && guard.getSpecification() != null) {
			ValueSpecification spec = guard.getSpecification();
			if (spec instanceof OpaqueExpression) {
				if (!((OpaqueExpression) spec).getBodies().isEmpty()) {
					return ((OpaqueExpression) spec).getBodies().get(0);
				}
			}
		}
		return "";
	}

	/**
	 * Get or create trigger guard.
	 * 
	 * @param rowElement
	 *            row element
	 * @param createOnDemand
	 *            create if true
	 * @return trigger guard
	 */
	private Constraint getTriggerGuard(Trigger trigger, boolean createOnDemand) {
		Transition transition = (Transition) trigger.eContainer();
		Constraint result = CodeSnippetUtils.getTriggerGuard(trigger);
		if (result == null && createOnDemand) {
			result = CodeSnippetUtils.createGuard(transition, Collections.singletonList(trigger));
		}
		return result;
	}

	/**
	 * @see org.eclipse.papyrus.infra.nattable.manager.cell.AbstractCellManager#setValue(org.eclipse.emf.transaction.TransactionalEditingDomain, java.lang.Object, java.lang.Object, java.lang.Object,
	 *      org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager)
	 *
	 * @param domain
	 * @param columnElement
	 * @param rowElement
	 * @param newValue
	 * @param tableManager
	 */
	@Override
	public void setValue(TransactionalEditingDomain domain, Object columnElement, Object rowElement, Object newValue, INattableModelManager tableManager) {
		Trigger trigger = (Trigger) AxisUtils.getRepresentedElement(rowElement);
		if (UML2Util.isEmpty(newValue.toString())) {
			// remove guard if empty
			Constraint guard = getTriggerGuard(trigger, false);
			if (guard != null) {
				guard.destroy();
			}
		} else {
			Constraint guard = getTriggerGuard(trigger, true);
			ValueSpecification valueSpec = guard.getSpecification();
			if (valueSpec != null && !(valueSpec instanceof OpaqueExpression)) {
				// trigger guard spec should be opaque expression
				valueSpec.destroy();
				valueSpec = null;
			}
			if (valueSpec == null) {
				valueSpec = CodeSnippetUtils.createOpaqueExpressionWithDefaultLanguage(trigger);
				guard.setSpecification(valueSpec);
			}
			OpaqueExpression op = (OpaqueExpression) valueSpec;
			if (op.getBodies().isEmpty()) {
				op.getBodies().add(newValue.toString());
			} else {
				String oldValue = op.getBodies().get(0);
				if (!oldValue.equals(newValue)) {
					op.getBodies().clear();
					op.getBodies().add(newValue.toString());
				}
			}
		}
	}
}
