/**
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 */
package org.eclipse.papyrus.designer.languages.java.codegen.xtend;

import com.google.common.base.Objects;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.papyrus.designer.languages.common.base.ClassUtils;
import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.NoCodeGen;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.ExternLibrary;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.External;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class JavaClassImportClassDeclaration {
  public static Iterable<String> javaClassAllImports(final Classifier classifier, final EList<Classifier> list, final String prefix) {
    List<String> newList = new UniqueEList<String>();
    for (final Classifier cl : list) {
      if (((((!Objects.equal(cl, classifier)) && (!GenUtils.hasStereotype(cl, NoCodeGen.class))) || GenUtils.hasStereotype(cl, External.class)) || GenUtils.hasStereotypeTree(cl, ExternLibrary.class))) {
        if ((!(cl instanceof PrimitiveType))) {
          final List<String> imports = JavaClassImportDeclaration.javaClassImports(cl, classifier);
          if ((GenUtils.hasStereotype(cl, External.class) || GenUtils.hasStereotypeTree(cl, ExternLibrary.class))) {
            newList.addAll(JavaClassImportDeclaration.javaClassImports(cl, classifier));
          } else {
            for (final String theImport : imports) {
              newList.add((prefix + theImport));
            }
          }
        }
      }
    }
    final Function1<String, Boolean> _function = (String str) -> {
      return Boolean.valueOf((str != null));
    };
    return IterableExtensions.<String>filter(newList, _function);
  }

  public static Iterable<String> javaClassAllImports(final Classifier clazz, final String prefix) {
    return JavaClassImportClassDeclaration.javaClassAllImports(clazz, ClassUtils.requiredClassifiers(clazz), prefix);
  }
}
