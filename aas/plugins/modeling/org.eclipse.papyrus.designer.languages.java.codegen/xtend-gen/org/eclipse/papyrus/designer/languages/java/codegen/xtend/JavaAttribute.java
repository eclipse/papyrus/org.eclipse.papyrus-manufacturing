/**
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 */
package org.eclipse.papyrus.designer.languages.java.codegen.xtend;

import java.util.Collection;
import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.designer.languages.java.codegen.utils.JavaGenUtils;
import org.eclipse.papyrus.designer.languages.java.codegen.utils.Modifier;
import org.eclipse.uml2.uml.AttributeOwner;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.ValueSpecification;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@SuppressWarnings("all")
public class JavaAttribute {
  public static Collection<Property> getOwnedAttributes(final Classifier cl) {
    Collection<Property> _xblockexpression = null;
    {
      final EList<Property> attributes = JavaAttribute.getOwnedAttributesWNull(cl);
      Collection<Property> _xifexpression = null;
      if ((attributes == null)) {
        _xifexpression = CollectionLiterals.<Property>emptySet();
      } else {
        _xifexpression = attributes;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }

  /**
   * @return a list of owned attributes, since this is not supported directly on a classifier, null if not available
   */
  public static EList<Property> getOwnedAttributesWNull(final Classifier cl) {
    EList<Property> _xifexpression = null;
    if ((cl instanceof AttributeOwner)) {
      _xifexpression = ((AttributeOwner) cl).getOwnedAttributes();
    } else {
      return null;
    }
    return _xifexpression;
  }

  public static String defaultValue(final Property attribute) {
    String _xifexpression = null;
    ValueSpecification _defaultValue = attribute.getDefaultValue();
    boolean _tripleNotEquals = (_defaultValue != null);
    if (_tripleNotEquals) {
      String _stringValue = attribute.getDefaultValue().stringValue();
      _xifexpression = (" =" + _stringValue);
    }
    return _xifexpression;
  }

  public static CharSequence javaAttributeDeclaration(final Property attribute) {
    StringConcatenation _builder = new StringConcatenation();
    CharSequence _javaElementDoc = JavaDocumentation.javaElementDoc(attribute);
    _builder.append(_javaElementDoc);
    _builder.newLineIfNotEmpty();
    String _attributeModifiers = Modifier.attributeModifiers(attribute);
    _builder.append(_attributeModifiers);
    String _javaType = JavaTypedElement.javaType(attribute);
    _builder.append(_javaType);
    _builder.append(" ");
    String _name = attribute.getName();
    _builder.append(_name);
    String _defaultValue = JavaAttribute.defaultValue(attribute);
    _builder.append(_defaultValue);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    return _builder;
  }

  public static CharSequence javaPropertyAttributeDeclaration(final Property attribute) {
    StringConcatenation _builder = new StringConcatenation();
    CharSequence _javaGetPropertyDoc = JavaDocumentation.javaGetPropertyDoc();
    _builder.append(_javaGetPropertyDoc);
    _builder.newLineIfNotEmpty();
    String _attributeModifiers = Modifier.attributeModifiers(attribute);
    _builder.append(_attributeModifiers);
    String _javaType = JavaTypedElement.javaType(attribute);
    _builder.append(_javaType);
    _builder.newLineIfNotEmpty();
    return _builder;
  }

  public static CharSequence javaPropertyAttributeBody(final Property attribute) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("// Work with your Dynamic Property here. ");
    _builder.newLine();
    {
      if ((((JavaGenUtils.hasEndPoint(attribute)).booleanValue() && (JavaGenUtils.hasNodeId(attribute)).booleanValue()) && JavaGenUtils.getEndPointProtocol(attribute).equals("OPCUA"))) {
        _builder.append("\t");
        String _javaType = JavaTypedElement.javaType(attribute);
        _builder.append(_javaType, "\t");
        _builder.append(" defaultVar = (");
        String _javaType_1 = JavaTypedElement.javaType(attribute);
        _builder.append(_javaType_1, "\t");
        _builder.append(") this.connectedDevices.");
        String _endPointName = JavaGenUtils.getEndPointName(attribute);
        _builder.append(_endPointName, "\t");
        _builder.append(".readValue(new NodeId(");
        Integer _nameSpace = JavaGenUtils.getNameSpace(attribute);
        _builder.append(_nameSpace, "\t");
        _builder.append(", ");
        String _identifier = JavaGenUtils.getIdentifier(attribute);
        _builder.append(_identifier, "\t");
        _builder.append("));");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("return defaultVar;");
        _builder.newLine();
      } else {
        {
          String _propertyValue = JavaGenUtils.getPropertyValue(attribute);
          boolean _tripleNotEquals = (_propertyValue != null);
          if (_tripleNotEquals) {
            _builder.append("\t");
            _builder.newLine();
            _builder.append("\t");
            String _javaType_2 = JavaTypedElement.javaType(attribute);
            _builder.append(_javaType_2, "\t");
            _builder.append(" defaultVar =  ");
            String _propertyValue_1 = JavaGenUtils.getPropertyValue(attribute);
            _builder.append(_propertyValue_1, "\t");
            _builder.append(";");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            _builder.append("return defaultVar;");
            _builder.newLine();
          } else {
            _builder.append("\t");
            String _javaType_3 = JavaTypedElement.javaType(attribute);
            _builder.append(_javaType_3, "\t");
            _builder.append(" defaultVar = null;");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            _builder.append("return defaultVar;");
            _builder.newLine();
          }
        }
      }
    }
    return _builder;
  }
}
