/**
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 */
package org.eclipse.papyrus.designer.languages.java.codegen.xtend;

import java.util.List;
import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.papyrus.designer.languages.java.codegen.utils.JavaGenUtils;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class JavaClassOperationsDeclaration {
  public static CharSequence javaClassOperationsDeclaration(final Classifier clazz) {
    StringConcatenation _builder = new StringConcatenation();
    {
      List<Operation> _allownedOperations = JavaGenUtils.getAllownedOperations(clazz);
      for(final Operation op : _allownedOperations) {
        {
          boolean _hasStereotype = GenUtils.hasStereotype(op, org.eclipse.papyrus.aas.Operation.class);
          if (_hasStereotype) {
            CharSequence _javaAASOperationDeclaration = JavaOperations.javaAASOperationDeclaration(op);
            _builder.append(_javaAASOperationDeclaration);
          }
        }
        _builder.newLineIfNotEmpty();
        {
          if (((!GenUtils.hasStereotype(op, org.eclipse.papyrus.aas.Operation.class)) && (JavaGenUtils.isOperationCreatedByUser(op, clazz)).booleanValue())) {
            CharSequence _javaOperationDeclaration = JavaOperations.javaOperationDeclaration(op);
            _builder.append(_javaOperationDeclaration);
          }
        }
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }

  public static CharSequence javaClassPropertyOperationsDeclaration(final Classifier clazz) {
    StringConcatenation _builder = new StringConcatenation();
    {
      List<Operation> _allownedOperations = JavaGenUtils.getAllownedOperations(clazz);
      for(final Operation op : _allownedOperations) {
        {
          boolean _hasStereotype = GenUtils.hasStereotype(op, org.eclipse.papyrus.aas.Operation.class);
          boolean _not = (!_hasStereotype);
          if (_not) {
            CharSequence _javaOperationDeclaration = JavaOperations.javaOperationDeclaration(op);
            _builder.append(_javaOperationDeclaration);
          }
        }
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }

  public static String javaClassConstructorDeclaration(final Classifier clazz) {
    String result2 = (" public DynamicElementsWorkspace " + " (ConnectedDevices connectedDevices) {\n\t\tthis.connectedDevices = connectedDevices;}");
    return result2;
  }

  public static String javaClassPropertyConstructorDeclaration(final Classifier clazz) {
    String _name = clazz.getName();
    String _plus = ("public " + _name);
    String _plus_1 = (_plus + "ValueDelegatesJDT");
    String result = (_plus_1 + " (ConnectedDevices connectedDevices, \n\t\t\tConceptDescriptions conceptDescriptions) {\n\t\tthis.connectedDevices = connectedDevices;\n\t\tthis.conceptDescriptions = conceptDescriptions;\n\t}");
    return result;
  }
}
