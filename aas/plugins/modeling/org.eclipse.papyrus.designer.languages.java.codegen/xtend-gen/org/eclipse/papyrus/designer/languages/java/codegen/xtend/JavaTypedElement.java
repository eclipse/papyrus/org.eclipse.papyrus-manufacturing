/**
 * Copyright (c) 2019 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     CEA LIST - initial API and implementation
 */
package org.eclipse.papyrus.designer.languages.java.codegen.xtend;

import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.ListHint;
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.TemplateBinding;
import org.eclipse.papyrus.designer.languages.java.codegen.Activator;
import org.eclipse.papyrus.designer.languages.java.codegen.utils.JavaGenUtils;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.MultiplicityElement;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.TypedElement;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.eclipse.xtend2.lib.StringConcatenation;

/**
 * Produce the string for a typed element, taking the ListHint and TemplateBinding stereotype
 * into account
 */
@SuppressWarnings("all")
public class JavaTypedElement {
  private static final String TYPE_NAME_TAG = "[typeName]";

  private String string;

  /**
   * Handle list types
   * @param propertyOrParameter
   *            a property or parameter (in both cases a typed element)
   * @return a string representation of the type of a property or parameter. The function takes
   *         list hints into account in case of non 1 multiplicity
   */
  public static String javaType(final TypedElement propertyOrParameter) {
    String type = null;
    if ((((propertyOrParameter == null) || (propertyOrParameter.getType() == null)) || (propertyOrParameter.getType().getQualifiedName() == null))) {
      if ((propertyOrParameter.getName().contains("::") && (propertyOrParameter.getName().length() > 5))) {
        boolean _contains = propertyOrParameter.getName().contains(".");
        boolean _not = (!_contains);
        if (_not) {
          return propertyOrParameter.getName().substring(5);
        } else {
          return JavaGenUtils.getXMLTypes(propertyOrParameter);
        }
      } else {
        return JavaGenUtils.getXMLTypes(propertyOrParameter);
      }
    }
    Activator.log.debug("javaType: propertpropertyOrParameter is null");
    Classifier ns = null;
    if ((propertyOrParameter instanceof Parameter)) {
      ns = ((Parameter)propertyOrParameter).getOperation().getClass_();
    } else {
      if ((propertyOrParameter instanceof Property)) {
        ns = ((Property)propertyOrParameter).getClass_();
      }
    }
    String defaultType = JavaGenUtils.javaQualifiedName(propertyOrParameter.getType(), ns);
    final TemplateBinding binding = UMLUtil.<TemplateBinding>getStereotypeApplication(propertyOrParameter, TemplateBinding.class);
    if ((binding != null)) {
      String _defaultType = defaultType;
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("<");
      {
        EList<Type> _actuals = binding.getActuals();
        boolean _hasElements = false;
        for(final Type actual : _actuals) {
          if (!_hasElements) {
            _hasElements = true;
          } else {
            _builder.appendImmediate(",", "");
          }
          String _qualifiedName = actual.getQualifiedName();
          _builder.append(_qualifiedName);
        }
      }
      _builder.append(">");
      defaultType = (_defaultType + _builder);
    }
    if (((defaultType != null) && defaultType.contains("."))) {
      defaultType = JavaGenUtils.removedot(defaultType);
    }
    final ListHint listHint = GenUtils.<ListHint>getApplicationTree(propertyOrParameter, ListHint.class);
    final int lower = ((MultiplicityElement) propertyOrParameter).getLower();
    final int upper = ((MultiplicityElement) propertyOrParameter).getUpper();
    if ((listHint != null)) {
      if ((upper == (-1))) {
        type = listHint.getVariable();
      } else {
        if ((upper > 1)) {
          if ((upper == lower)) {
            type = listHint.getFixed();
          } else {
            type = listHint.getBounded();
          }
        }
      }
    }
    if (((type == null) || (type.length() == 0))) {
      if ((upper != 1)) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append(JavaTypedElement.TYPE_NAME_TAG);
        _builder_1.append("[]");
        type = _builder_1.toString();
      }
    }
    if ((type != null)) {
      return type.replace(JavaTypedElement.TYPE_NAME_TAG, defaultType);
    } else {
      return defaultType;
    }
  }

  public static String javaSecType(final TypedElement propertyOrParameter) {
    String type = null;
    if ((((propertyOrParameter == null) || (propertyOrParameter.getType() == null)) || (propertyOrParameter.getType().getQualifiedName() == null))) {
      Activator.log.debug("javaType: propertpropertyOrParameter is null");
      return "undef";
    }
    Classifier ns = null;
    if ((propertyOrParameter instanceof Parameter)) {
      ns = ((Parameter)propertyOrParameter).getOperation().getClass_();
    } else {
      if ((propertyOrParameter instanceof Property)) {
        ns = ((Property)propertyOrParameter).getClass_();
      }
    }
    String defaultType = JavaGenUtils.javaQualifiedName(propertyOrParameter.getType(), ns);
    final TemplateBinding binding = UMLUtil.<TemplateBinding>getStereotypeApplication(propertyOrParameter, TemplateBinding.class);
    if ((binding != null)) {
      String _defaultType = defaultType;
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("<");
      {
        EList<Type> _actuals = binding.getActuals();
        boolean _hasElements = false;
        for(final Type actual : _actuals) {
          if (!_hasElements) {
            _hasElements = true;
          } else {
            _builder.appendImmediate(",", "");
          }
          String _qualifiedName = actual.getQualifiedName();
          _builder.append(_qualifiedName);
        }
      }
      _builder.append(">");
      defaultType = (_defaultType + _builder);
    }
    return "Collection<ISubmodelElement>";
  }
}
