/**
 * Copyright (c) 2014 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     CEA LIST - initial API and implementation
 */
package org.eclipse.papyrus.designer.languages.java.codegen.xtend;

import com.google.common.base.Objects;
import java.util.Collection;
import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.papyrus.designer.languages.java.codegen.Constants;
import org.eclipse.papyrus.designer.languages.java.codegen.utils.JavaGenUtils;
import org.eclipse.papyrus.designer.languages.java.codegen.utils.Modifier;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.Default;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.profile.standard.Create;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class JavaOperations {
  public static CharSequence javaOperationImplementation(final Operation operation) {
    StringConcatenation _builder = new StringConcatenation();
    String _body = GenUtils.getBody(operation, Constants.supportedLanguages);
    _builder.append(_body);
    _builder.newLineIfNotEmpty();
    return _builder;
  }

  public static String javaReturnSpec(final Operation operation) {
    String _xifexpression = null;
    boolean _isConstructor = JavaOperations.isConstructor(operation);
    if (_isConstructor) {
      _xifexpression = JavaOperations.constructorOrVoid(operation);
    } else {
      final Function1<Parameter, Boolean> _function = (Parameter it) -> {
        ParameterDirectionKind _direction = it.getDirection();
        return Boolean.valueOf(Objects.equal(_direction, ParameterDirectionKind.OUT_LITERAL));
      };
      String _javaTypeOrVoid = JavaOperations.javaTypeOrVoid(IterableExtensions.<Parameter>head(IterableExtensions.<Parameter>filter(operation.getOwnedParameters(), _function)));
      _xifexpression = (_javaTypeOrVoid + " ");
    }
    return _xifexpression;
  }

  public static String javaSECReturnSpec(final Operation operation) {
    String _xifexpression = null;
    boolean _isConstructor = JavaOperations.isConstructor(operation);
    if (_isConstructor) {
      _xifexpression = JavaOperations.constructorOrVoid(operation);
    } else {
      final Function1<Parameter, Boolean> _function = (Parameter it) -> {
        ParameterDirectionKind _direction = it.getDirection();
        return Boolean.valueOf(Objects.equal(_direction, ParameterDirectionKind.OUT_LITERAL));
      };
      String _javaSECTypeOrVoid = JavaOperations.javaSECTypeOrVoid(IterableExtensions.<Parameter>head(IterableExtensions.<Parameter>filter(operation.getOwnedParameters(), _function)));
      _xifexpression = (_javaSECTypeOrVoid + " ");
    }
    return _xifexpression;
  }

  public static String javaTypeOrVoid(final Parameter parameter) {
    if ((parameter != null)) {
      return JavaTypedElement.javaType(parameter);
    } else {
      return "void";
    }
  }

  public static String javaSECTypeOrVoid(final Parameter parameter) {
    if ((parameter != null)) {
      return parameter.getName().substring(5);
    } else {
      return "void";
    }
  }

  public static CharSequence throwss(final Operation operation) {
    StringConcatenation _builder = new StringConcatenation();
    {
      int _length = ((Object[])Conversions.unwrapArray(operation.getRaisedExceptions(), Object.class)).length;
      boolean _greaterThan = (_length > 0);
      if (_greaterThan) {
        _builder.append("throws ");
        {
          EList<Type> _raisedExceptions = operation.getRaisedExceptions();
          boolean _hasElements = false;
          for(final Type re : _raisedExceptions) {
            if (!_hasElements) {
              _hasElements = true;
            } else {
              _builder.appendImmediate(",", "");
            }
            String _name = re.getName();
            _builder.append(_name);
          }
        }
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }

  public static String constructorOrVoid(final Operation operation) {
    String _xifexpression = null;
    boolean _isConstructor = JavaOperations.isConstructor(operation);
    if (_isConstructor) {
      _xifexpression = "";
    } else {
      _xifexpression = "void ";
    }
    return _xifexpression;
  }

  public static boolean isConstructor(final Operation operation) {
    return GenUtils.hasStereotype(operation, Create.class);
  }

  public static Collection<Operation> getOwnedOperations(final Classifier cl) {
    Collection<Operation> _xblockexpression = null;
    {
      final EList<Operation> operations = JavaOperations.getOwnedOperationsWNull(cl);
      Collection<Operation> _xifexpression = null;
      if ((operations == null)) {
        _xifexpression = CollectionLiterals.<Operation>emptySet();
      } else {
        _xifexpression = operations;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }

  public static EList<Operation> getOwnedOperationsWNull(final Classifier cl) {
    EList<Operation> _xifexpression = null;
    if ((cl instanceof org.eclipse.uml2.uml.Class)) {
      _xifexpression = ((org.eclipse.uml2.uml.Class) cl).getOwnedOperations();
    } else {
      EList<Operation> _xifexpression_1 = null;
      if ((cl instanceof DataType)) {
        _xifexpression_1 = ((DataType) cl).getOwnedOperations();
      } else {
        EList<Operation> _xifexpression_2 = null;
        if ((cl instanceof Interface)) {
          _xifexpression_2 = ((Interface) cl).getOwnedOperations();
        } else {
          _xifexpression_2 = null;
        }
        _xifexpression_1 = _xifexpression_2;
      }
      _xifexpression = _xifexpression_1;
    }
    return _xifexpression;
  }

  public static CharSequence javaOperationDeclaration(final Operation operation) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.append("\t");
    String _methodModifiers = Modifier.methodModifiers(operation);
    _builder.append(_methodModifiers, "\t");
    String _javaReturnSpec = JavaOperations.javaReturnSpec(operation);
    _builder.append(_javaReturnSpec, "\t");
    String _name = operation.getName();
    _builder.append(_name, "\t");
    _builder.append("(");
    CharSequence _javaOperationParameters = JavaParameter.javaOperationParameters(operation);
    _builder.append(_javaOperationParameters, "\t");
    _builder.append(") ");
    CharSequence _throwss = JavaOperations.throwss(operation);
    _builder.append(_throwss, "\t");
    {
      boolean _mustGenerateBody = JavaOperations.mustGenerateBody(operation);
      if (_mustGenerateBody) {
        _builder.append("{");
      } else {
        _builder.append(";");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    {
      boolean _mustGenerateBody_1 = JavaOperations.mustGenerateBody(operation);
      if (_mustGenerateBody_1) {
        CharSequence _javaOperationImplementation = JavaOperations.javaOperationImplementation(operation);
        _builder.append(_javaOperationImplementation, "\t\t");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    {
      boolean _mustGenerateBody_2 = JavaOperations.mustGenerateBody(operation);
      if (_mustGenerateBody_2) {
        _builder.append("}");
      }
    }
    _builder.newLineIfNotEmpty();
    return _builder;
  }

  public static CharSequence javaAASOperationDeclaration(final Operation operation) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.append("\t");
    CharSequence _javaAASOperationDoc = JavaDocumentation.javaAASOperationDoc(operation);
    _builder.append(_javaAASOperationDoc, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    String _methodModifiers = Modifier.methodModifiers(operation);
    _builder.append(_methodModifiers, "\t");
    String _javaReturnSpec = JavaOperations.javaReturnSpec(operation);
    _builder.append(_javaReturnSpec, "\t");
    Element _owner = operation.getOwner();
    String _name = ((NamedElement) _owner).getName();
    _builder.append(_name, "\t");
    _builder.append("_");
    String _name_1 = operation.getName();
    _builder.append(_name_1, "\t");
    _builder.append("(");
    CharSequence _javaOperationParameters = JavaParameter.javaOperationParameters(operation);
    _builder.append(_javaOperationParameters, "\t");
    _builder.append(") ");
    CharSequence _throwss = JavaOperations.throwss(operation);
    _builder.append(_throwss, "\t");
    {
      boolean _mustGenerateBody = JavaOperations.mustGenerateBody(operation);
      if (_mustGenerateBody) {
        _builder.append("{");
      } else {
        _builder.append(";");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    {
      Boolean _hasOpaqueBehavior = JavaGenUtils.hasOpaqueBehavior(operation);
      if ((_hasOpaqueBehavior).booleanValue()) {
        CharSequence _javaOperationImplementation = JavaOperations.javaOperationImplementation(operation);
        _builder.append(_javaOperationImplementation, "\t\t");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t");
      } else {
        CharSequence _javaAASOperationImplementation = JavaOperations.javaAASOperationImplementation(operation);
        _builder.append(_javaAASOperationImplementation, "\t\t");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    {
      boolean _mustGenerateBody_1 = JavaOperations.mustGenerateBody(operation);
      if (_mustGenerateBody_1) {
        _builder.append("}");
      }
    }
    _builder.newLineIfNotEmpty();
    return _builder;
  }

  public static CharSequence javaAASOperationImplementation(final Operation operation) {
    StringConcatenation _builder = new StringConcatenation();
    {
      boolean _hasReturnType = JavaOperations.hasReturnType(operation);
      if (_hasReturnType) {
        _builder.append("return (");
        String _returnType = JavaOperations.getReturnType(operation);
        _builder.append(_returnType);
        _builder.append(") ");
        Element _owner = operation.getOwner();
        String _name = ((Classifier) _owner).getName();
        _builder.append(_name);
        _builder.append(".");
        Element _owner_1 = operation.getOwner();
        String _name_1 = ((Classifier) _owner_1).getName();
        _builder.append(_name_1);
        _builder.append("_");
        final Function1<Parameter, Boolean> _function = (Parameter it) -> {
          ParameterDirectionKind _direction = it.getDirection();
          return Boolean.valueOf(Objects.equal(_direction, ParameterDirectionKind.OUT_LITERAL));
        };
        String _name_2 = IterableExtensions.<Parameter>head(IterableExtensions.<Parameter>filter(operation.getOwnedParameters(), _function)).getName();
        _builder.append(_name_2);
        _builder.append("_");
        String _name_3 = operation.getName();
        _builder.append(_name_3);
        _builder.append("_Output.getValue();\t\t\t");
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }

  public static CharSequence javaGetOperationDeclaration(final Operation operation) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.append("\t");
    CharSequence _javaGetOperationDoc = JavaDocumentation.javaGetOperationDoc(operation);
    _builder.append(_javaGetOperationDoc, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    String _methodModifiers = Modifier.methodModifiers(operation);
    _builder.append(_methodModifiers, "\t");
    String _javaReturnSpec = JavaOperations.javaReturnSpec(operation);
    _builder.append(_javaReturnSpec, "\t");
    String _name = operation.getName();
    _builder.append(_name, "\t");
    _builder.append("(");
    CharSequence _javaOperationParameters = JavaParameter.javaOperationParameters(operation);
    _builder.append(_javaOperationParameters, "\t");
    _builder.append(") ");
    CharSequence _throwss = JavaOperations.throwss(operation);
    _builder.append(_throwss, "\t");
    {
      boolean _mustGenerateBody = JavaOperations.mustGenerateBody(operation);
      if (_mustGenerateBody) {
        _builder.append("{");
      } else {
        _builder.append(";");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    {
      boolean _mustGenerateBody_1 = JavaOperations.mustGenerateBody(operation);
      if (_mustGenerateBody_1) {
        CharSequence _javaOperationImplementation = JavaOperations.javaOperationImplementation(operation);
        _builder.append(_javaOperationImplementation, "\t\t");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    {
      boolean _mustGenerateBody_2 = JavaOperations.mustGenerateBody(operation);
      if (_mustGenerateBody_2) {
        _builder.append("}");
      }
    }
    _builder.newLineIfNotEmpty();
    return _builder;
  }

  public static CharSequence javagetOperationDeclaration(final Property property, final Classifier c) {
    StringConcatenation _builder = new StringConcatenation();
    {
      Boolean _isOperationExists = JavaGenUtils.isOperationExists(c, property);
      if ((_isOperationExists).booleanValue()) {
        CharSequence _javaGetOperationDeclaration = JavaOperations.javaGetOperationDeclaration(JavaGenUtils.getOperation(c, property));
        _builder.append(_javaGetOperationDeclaration);
        _builder.newLineIfNotEmpty();
      } else {
        CharSequence _javaPropertyAttributeDeclaration = JavaAttribute.javaPropertyAttributeDeclaration(property);
        _builder.append(_javaPropertyAttributeDeclaration);
        _builder.append(" get_");
        Element _owner = property.getOwner();
        String _name = ((NamedElement) _owner).getName();
        _builder.append(_name);
        _builder.append("_");
        String _name_1 = property.getName();
        _builder.append(_name_1);
        _builder.append("() {");
        _builder.newLineIfNotEmpty();
        CharSequence _javaPropertyAttributeBody = JavaAttribute.javaPropertyAttributeBody(property);
        _builder.append(_javaPropertyAttributeBody);
        _builder.append("}");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    return _builder;
  }

  public static boolean isAbstract(final Operation operation) {
    return (((operation.isAbstract() && (operation.getOwner() instanceof Classifier)) && (((Classifier) operation.getOwner()).isAbstract() || (operation.getOwner() instanceof Enumeration))) || 
      (operation.getInterface() != null));
  }

  public static boolean isDefault(final Operation operation) {
    return ((operation.getInterface() != null) && (UMLUtil.<Default>getStereotypeApplication(operation, Default.class) != null));
  }

  public static boolean mustGenerateBody(final Operation operation) {
    boolean _isAbstract = JavaOperations.isAbstract(operation);
    boolean _not = (!_isAbstract);
    if (_not) {
      return true;
    } else {
      Interface _interface = operation.getInterface();
      boolean _tripleEquals = (_interface == null);
      if (_tripleEquals) {
        boolean _isStatic = operation.isStatic();
        if (_isStatic) {
          return true;
        } else {
          return false;
        }
      } else {
        Interface _interface_1 = operation.getInterface();
        boolean _tripleNotEquals = (_interface_1 != null);
        if (_tripleNotEquals) {
          return (operation.isStatic() || (UMLUtil.<Default>getStereotypeApplication(operation, Default.class) != null));
        }
      }
    }
    return true;
  }

  public static boolean hasReturnType(final Operation operation) {
    final Function1<Parameter, Boolean> _function = (Parameter it) -> {
      ParameterDirectionKind _direction = it.getDirection();
      return Boolean.valueOf(Objects.equal(_direction, ParameterDirectionKind.OUT_LITERAL));
    };
    boolean _equals = JavaOperations.javaTypeOrVoid(IterableExtensions.<Parameter>head(IterableExtensions.<Parameter>filter(operation.getOwnedParameters(), _function))).equals("void");
    return (!_equals);
  }

  public static String getReturnType(final Operation operation) {
    final Function1<Parameter, Boolean> _function = (Parameter it) -> {
      ParameterDirectionKind _direction = it.getDirection();
      return Boolean.valueOf(Objects.equal(_direction, ParameterDirectionKind.OUT_LITERAL));
    };
    return JavaOperations.javaTypeOrVoid(IterableExtensions.<Parameter>head(IterableExtensions.<Parameter>filter(operation.getOwnedParameters(), _function)));
  }

  public static CharSequence javaOperationSECDeclaration(final Operation operation) {
    StringConcatenation _builder = new StringConcatenation();
    {
      if ((operation != null)) {
        CharSequence _javaGetSECOperationDoc = JavaDocumentation.javaGetSECOperationDoc(operation);
        _builder.append(_javaGetSECOperationDoc);
        _builder.newLineIfNotEmpty();
        String _methodModifiers = Modifier.methodModifiers(operation);
        _builder.append(_methodModifiers);
        String _javaSECReturnSpec = JavaOperations.javaSECReturnSpec(operation);
        _builder.append(_javaSECReturnSpec);
        String _name = operation.getName();
        _builder.append(_name);
        _builder.append("(");
        CharSequence _javaOperationParameters = JavaParameter.javaOperationParameters(operation);
        _builder.append(_javaOperationParameters);
        _builder.append(") ");
        CharSequence _throwss = JavaOperations.throwss(operation);
        _builder.append(_throwss);
        {
          boolean _mustGenerateBody = JavaOperations.mustGenerateBody(operation);
          if (_mustGenerateBody) {
            _builder.append("{");
          } else {
            _builder.append(";");
          }
        }
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        {
          boolean _mustGenerateBody_1 = JavaOperations.mustGenerateBody(operation);
          if (_mustGenerateBody_1) {
            CharSequence _javaOperationImplementation = JavaOperations.javaOperationImplementation(operation);
            _builder.append(_javaOperationImplementation, "\t");
          }
        }
        _builder.newLineIfNotEmpty();
        {
          boolean _mustGenerateBody_2 = JavaOperations.mustGenerateBody(operation);
          if (_mustGenerateBody_2) {
            _builder.append("}");
          }
        }
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }
}
