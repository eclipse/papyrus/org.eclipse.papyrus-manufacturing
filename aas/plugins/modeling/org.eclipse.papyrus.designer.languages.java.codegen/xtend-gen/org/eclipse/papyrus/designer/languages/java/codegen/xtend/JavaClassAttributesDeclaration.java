/**
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 */
package org.eclipse.papyrus.designer.languages.java.codegen.xtend;

import java.util.Collection;
import java.util.List;
import org.eclipse.papyrus.designer.languages.java.codegen.utils.JavaGenUtils;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Property;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class JavaClassAttributesDeclaration {
  public static CharSequence javaClassAttributesDeclaration(final Classifier clazz) {
    StringConcatenation _builder = new StringConcatenation();
    {
      Collection<Property> _ownedAttributes = JavaAttribute.getOwnedAttributes(clazz);
      for(final Property oa : _ownedAttributes) {
        CharSequence _javaAttributeDeclaration = JavaAttribute.javaAttributeDeclaration(oa);
        _builder.append(_javaAttributeDeclaration);
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }

  public static CharSequence javaClassPropertyAttributesDeclaration(final Classifier clazz) {
    StringConcatenation _builder = new StringConcatenation();
    {
      List<Property> _allownedAttributes = JavaGenUtils.getAllownedAttributes(clazz);
      for(final Property oa : _allownedAttributes) {
        {
          Boolean _isConnectProperty = JavaGenUtils.isConnectProperty(oa);
          if ((_isConnectProperty).booleanValue()) {
            CharSequence _javagetOperationDeclaration = JavaOperations.javagetOperationDeclaration(oa, clazz);
            _builder.append(_javagetOperationDeclaration);
          }
        }
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    return _builder;
  }

  public static String javaClassprivateAttributesDeclaration() {
    return "private ConnectedDevices connectedDevices;";
  }

  public static String javaClassPropertyprivateAttributesDeclaration() {
    return (("private ConnectedDevices connectedDevices;" + "\n") + "final ConceptDescriptions conceptDescriptions;");
  }
}
