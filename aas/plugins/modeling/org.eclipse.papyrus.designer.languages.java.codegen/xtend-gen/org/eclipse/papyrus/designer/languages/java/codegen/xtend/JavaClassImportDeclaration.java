/**
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 */
package org.eclipse.papyrus.designer.languages.java.codegen.xtend;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.papyrus.designer.languages.java.codegen.utils.JavaGenUtils;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.ExternLibrary;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.External;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.Template;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.TemplateParameterSubstitution;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.util.UMLUtil;

@SuppressWarnings("all")
public class JavaClassImportDeclaration {
  public static List<String> javaClassImports(final NamedElement ne, final Namespace ns) {
    List<String> result = new ArrayList<String>();
    boolean _hasStereotypeTree = GenUtils.hasStereotypeTree(ne, ExternLibrary.class);
    if (_hasStereotypeTree) {
      result = GenUtils.<ExternLibrary>getApplicationTree(ne, ExternLibrary.class).getImports();
    } else {
      result.add(JavaClassImportDeclaration.importName(ne, ns));
    }
    return result;
  }

  /**
   * @param ne the element to import
   * @param ns the element in which we import ne
   */
  public static String importName(final NamedElement ne, final Namespace ns) {
    boolean _hasStereotypeTree = GenUtils.hasStereotypeTree(ne, Template.class);
    if (_hasStereotypeTree) {
      return UMLUtil.<Template>getStereotypeApplication(ne, Template.class).getDeclaration();
    } else {
      boolean _hasStereotypeTree_1 = GenUtils.hasStereotypeTree(ne, External.class);
      if (_hasStereotypeTree_1) {
        return UMLUtil.<External>getStereotypeApplication(ne, External.class).getName();
      } else {
        Element _owner = ne.getOwner();
        if ((_owner instanceof TemplateParameterSubstitution)) {
          return "";
        }
        EList<Element> _ownedElements = ns.getOwnedElements();
        for (final Element element : _ownedElements) {
          if ((((element instanceof Enumeration) || (element instanceof Interface)) || element.eClass().equals(UMLFactory.eINSTANCE.getUMLPackage().getClass_()))) {
            boolean _equals = ((Classifier) element).getName().equals(ne.getName());
            if (_equals) {
              return "";
            }
          }
        }
        Namespace classifier = ns;
        Element _owner_1 = ns.getOwner();
        boolean _not = (!(_owner_1 instanceof org.eclipse.uml2.uml.Package));
        if (_not) {
          classifier = JavaClassImportDeclaration.getPackagedClassifier(ns);
          if ((classifier == null)) {
            return "";
          }
        }
        EList<String> importsOfClassifier = JavaGenUtils.imports.get(classifier);
        if ((importsOfClassifier == null)) {
          UniqueEList<String> _uniqueEList = new UniqueEList<String>();
          importsOfClassifier = _uniqueEList;
          JavaGenUtils.imports.put(classifier, importsOfClassifier);
        } else {
          for (final String import_ : importsOfClassifier) {
            {
              final String[] parts = import_.split("\\.");
              int _length = parts.length;
              boolean _greaterThan = (_length > 0);
              if (_greaterThan) {
                int _length_1 = parts.length;
                int _minus = (_length_1 - 1);
                final String lastPart = parts[_minus];
                boolean _equals_1 = ne.getName().equals(lastPart);
                if (_equals_1) {
                  return "";
                }
              }
            }
          }
        }
        final String qName = GenUtils.getFullName(ne, ".", false);
        importsOfClassifier.add(qName);
        return qName;
      }
    }
  }

  private static Classifier getPackagedClassifier(final Element ns) {
    if ((ns instanceof Classifier)) {
      return ((Classifier) ns);
    } else {
      Element _owner = ns.getOwner();
      boolean _tripleEquals = (_owner == null);
      if (_tripleEquals) {
        return null;
      }
      return JavaClassImportDeclaration.getPackagedClassifier(ns.getOwner());
    }
  }
}
