/**
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 */
package org.eclipse.papyrus.designer.languages.java.codegen.xtend;

import com.google.common.base.Objects;
import java.util.List;
import org.eclipse.core.resources.IProject;
import org.eclipse.papyrus.designer.languages.java.codegen.utils.JavaGenUtils;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.StaticClassifier;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.Strictfp;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.VisibilityKind;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class JavaClassifierGenerator {
  public static List<String> getSortedIncludePathList(final Classifier classifier, final String prefix) {
    List<String> importPathList = IterableExtensions.<String>sort(JavaClassImportClassDeclaration.javaClassAllImports(classifier, prefix));
    return importPathList;
  }

  public static CharSequence generateClassCode(final Classifier classifier, final IProject project, final String prefix) {
    StringConcatenation _builder = new StringConcatenation();
    String _packageDeclaration = JavaClassifierGenerator.packageDeclaration(classifier, project, prefix);
    _builder.append(_packageDeclaration);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t\t\t\t\t\t\t");
    _builder.newLine();
    String _javaImport = JavaImportUtil.javaImport(classifier, project);
    _builder.append(_javaImport);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    JavaGenUtils.openNS(classifier);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t\t\t\t\t\t\t\t");
    String _classVisibility = JavaClassifierGenerator.classVisibility(classifier);
    _builder.append(_classVisibility, "\t\t\t\t\t\t\t\t");
    _builder.append(" ");
    String _classModifiers = JavaClassifierGenerator.classModifiers(classifier);
    _builder.append(_classModifiers, "\t\t\t\t\t\t\t\t");
    String _classifierType = JavaClassifierGenerator.classifierType(classifier);
    _builder.append(_classifierType, "\t\t\t\t\t\t\t\t");
    _builder.append(" DynamicElementsWorkspace");
    _builder.newLineIfNotEmpty();
    _builder.append(" ");
    _builder.append("{");
    _builder.newLine();
    _builder.append("\t");
    {
      if ((classifier instanceof Enumeration)) {
        CharSequence _javaEnumerationLiterals = JavaEnumerations.javaEnumerationLiterals(((Enumeration) classifier));
        _builder.append(_javaEnumerationLiterals, "\t");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t\t\t\t\t");
    String _javaClassprivateAttributesDeclaration = JavaClassAttributesDeclaration.javaClassprivateAttributesDeclaration();
    _builder.append(_javaClassprivateAttributesDeclaration, "\t\t\t\t\t\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    String _javaClassConstructorDeclaration = JavaClassOperationsDeclaration.javaClassConstructorDeclaration(classifier);
    _builder.append(_javaClassConstructorDeclaration, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    CharSequence _javaClassOperationsDeclaration = JavaClassOperationsDeclaration.javaClassOperationsDeclaration(classifier);
    _builder.append(_javaClassOperationsDeclaration, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    CharSequence _javaClassPropertyAttributesDeclaration = JavaClassAttributesDeclaration.javaClassPropertyAttributesDeclaration(classifier);
    _builder.append(_javaClassPropertyAttributesDeclaration, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    CharSequence _javaInnerSECClassDefinition = JavaInnerClassifiers.javaInnerSECClassDefinition(classifier);
    _builder.append(_javaInnerSECClassDefinition, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }

  public static String classifierType(final Classifier classifier) {
    if ((classifier instanceof Interface)) {
      return "interface";
    } else {
      if ((classifier instanceof Enumeration)) {
        return "enum";
      } else {
        return "class";
      }
    }
  }

  public static String classVisibility(final Classifier classifier) {
    Element _owner = classifier.getOwner();
    if ((_owner instanceof Classifier)) {
      return classifier.getVisibility().toString().toLowerCase();
    } else {
      VisibilityKind _visibility = classifier.getVisibility();
      boolean _equals = Objects.equal(_visibility, VisibilityKind.PUBLIC_LITERAL);
      if (_equals) {
        return "public";
      }
      return "";
    }
  }

  public static String classModifiers(final Classifier classifier) {
    String result = "";
    if (((!(classifier instanceof Enumeration)) && (!(classifier instanceof Interface)))) {
      boolean _isAbstract = classifier.isAbstract();
      if (_isAbstract) {
        String _result = result;
        result = (_result + "abstract ");
      } else {
        boolean _isLeaf = classifier.isLeaf();
        if (_isLeaf) {
          String _result_1 = result;
          result = (_result_1 + "final ");
        }
      }
    }
    Element _owner = classifier.getOwner();
    if ((_owner instanceof Classifier)) {
      StaticClassifier _stereotypeApplication = UMLUtil.<StaticClassifier>getStereotypeApplication(classifier, StaticClassifier.class);
      boolean _tripleNotEquals = (_stereotypeApplication != null);
      if (_tripleNotEquals) {
        String _result_2 = result;
        result = (_result_2 + "static ");
      }
    }
    Strictfp _stereotypeApplication_1 = UMLUtil.<Strictfp>getStereotypeApplication(classifier, Strictfp.class);
    boolean _tripleNotEquals_1 = (_stereotypeApplication_1 != null);
    if (_tripleNotEquals_1) {
      String _result_3 = result;
      result = (_result_3 + "strictfp ");
    }
    return result;
  }

  public static String packageDeclaration(final Classifier classifier, final IProject project, final String prefix) {
    String _fullPath = JavaGenUtils.getFullPath(classifier, project, ".", false);
    String qName = (prefix + _fullPath);
    boolean _endsWith = qName.endsWith(".");
    if (_endsWith) {
      int _length = qName.length();
      int _minus = (_length - 1);
      qName = qName.substring(0, _minus);
    }
    boolean _isEmpty = qName.isEmpty();
    if (_isEmpty) {
      return "";
    }
    String _lowerCase = classifier.getName().toLowerCase();
    String _plus = ((("package " + qName) + ".") + _lowerCase);
    return (_plus + ";");
  }
}
