/**
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 */
package org.eclipse.papyrus.designer.languages.java.codegen.xtend;

import org.eclipse.core.resources.IProject;
import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.Import;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class JavaImportUtil {
  public static String importDirective(final String path) {
    if (((path != null) && (path.length() > 0))) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("import ");
      String _plus = (_builder.toString() + path);
      return (_plus + ";");
    }
    return null;
  }

  public static String javaImport(final NamedElement ne, final IProject project) {
    boolean _hasStereotype = GenUtils.hasStereotype(ne, Import.class);
    if (_hasStereotype) {
      final Import import_ = UMLUtil.<Import>getStereotypeApplication(ne, Import.class);
      String header = import_.getManualImports();
      if (((header != null) && (header.length() > 0))) {
        String _cleanCR = GenUtils.cleanCR(header);
        String includeHeader = (_cleanCR + "\n");
        return includeHeader;
      }
    } else {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("import ");
      String _lowerCase = project.getName().toLowerCase();
      String _plus = (_builder.toString() + _lowerCase);
      String _plus_1 = (_plus + ".connection.ConnectedDevices;");
      String _plus_2 = (_plus_1 + "\n");
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("import ");
      String _plus_3 = (_plus_2 + _builder_1);
      String _plus_4 = (_plus_3 + "java.util.Collection;");
      String _plus_5 = (_plus_4 + "\n");
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append("import ");
      String _plus_6 = (_plus_5 + _builder_2);
      String _plus_7 = (_plus_6 + "java.util.LinkedList;");
      String _plus_8 = (_plus_7 + "\n");
      StringConcatenation _builder_3 = new StringConcatenation();
      _builder_3.append("import ");
      String _plus_9 = (_plus_8 + _builder_3);
      String _plus_10 = (_plus_9 + "java.util.List;");
      String _plus_11 = (_plus_10 + "\n");
      StringConcatenation _builder_4 = new StringConcatenation();
      _builder_4.append("import ");
      String _plus_12 = (_plus_11 + _builder_4);
      String _plus_13 = (_plus_12 + "java.math.BigInteger;");
      String _plus_14 = (_plus_13 + "\n");
      StringConcatenation _builder_5 = new StringConcatenation();
      _builder_5.append("import ");
      String _plus_15 = (_plus_14 + _builder_5);
      String _plus_16 = (_plus_15 + "javax.xml.datatype.XMLGregorianCalendar;");
      String _plus_17 = (_plus_16 + "\n");
      StringConcatenation _builder_6 = new StringConcatenation();
      _builder_6.append("import ");
      String _plus_18 = (_plus_17 + _builder_6);
      String _plus_19 = (_plus_18 + "javax.xml.datatype.Duration;");
      String _plus_20 = (_plus_19 + "\n");
      StringConcatenation _builder_7 = new StringConcatenation();
      _builder_7.append("import ");
      String _plus_21 = (_plus_20 + _builder_7);
      String _plus_22 = (_plus_21 + "javax.xml.namespace.QName;");
      String _plus_23 = (_plus_22 + "\n");
      StringConcatenation _builder_8 = new StringConcatenation();
      _builder_8.append("import ");
      String _plus_24 = (_plus_23 + _builder_8);
      String _plus_25 = (_plus_24 + "javax.xml.datatype.Duration;");
      String _plus_26 = (_plus_25 + "\n");
      StringConcatenation _builder_9 = new StringConcatenation();
      _builder_9.append("import ");
      String _plus_27 = (_plus_26 + _builder_9);
      String _plus_28 = (_plus_27 + "org.eclipse.basyx.vab.exception.provider.ProviderException;");
      String _plus_29 = (_plus_28 + "\n");
      StringConcatenation _builder_10 = new StringConcatenation();
      _builder_10.append("import ");
      String _plus_30 = (_plus_29 + _builder_10);
      String _plus_31 = (_plus_30 + "org.eclipse.basyx.vab.protocol.opcua.types.NodeId;");
      String _plus_32 = (_plus_31 + "\n");
      StringConcatenation _builder_11 = new StringConcatenation();
      _builder_11.append("import ");
      String _plus_33 = (_plus_32 + _builder_11);
      String _plus_34 = (_plus_33 + "org.eclipse.basyx.submodel.metamodel.api.submodelelement.ISubmodelElement;");
      String _plus_35 = (_plus_34 + "\n");
      StringConcatenation _builder_12 = new StringConcatenation();
      _builder_12.append("import ");
      String _plus_36 = (_plus_35 + _builder_12);
      String _plus_37 = (_plus_36 + "org.eclipse.basyx.submodel.metamodel.api.reference.enums.KeyType;");
      String _plus_38 = (_plus_37 + "\n");
      StringConcatenation _builder_13 = new StringConcatenation();
      _builder_13.append("import ");
      String _plus_39 = (_plus_38 + _builder_13);
      String _plus_40 = (_plus_39 + "org.eclipse.basyx.submodel.metamodel.map.reference.Key;");
      String _plus_41 = (_plus_40 + "\n");
      StringConcatenation _builder_14 = new StringConcatenation();
      _builder_14.append("import ");
      String _plus_42 = (_plus_41 + _builder_14);
      String _plus_43 = (_plus_42 + "org.eclipse.basyx.submodel.metamodel.map.reference.Reference;");
      String _plus_44 = (_plus_43 + "\n");
      StringConcatenation _builder_15 = new StringConcatenation();
      _builder_15.append("import ");
      String _plus_45 = (_plus_44 + _builder_15);
      String _plus_46 = (_plus_45 + "org.eclipse.basyx.submodel.metamodel.map.submodelelement.dataelement.property.Property;");
      String includeHeader_1 = (_plus_46 + "\n");
      return includeHeader_1;
    }
    return null;
  }

  public static CharSequence constImportStart() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("// Manual imports");
    _builder.newLine();
    return _builder;
  }

  public static CharSequence constImportEnd() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("// End of manual imports");
    _builder.newLine();
    return _builder;
  }
}
