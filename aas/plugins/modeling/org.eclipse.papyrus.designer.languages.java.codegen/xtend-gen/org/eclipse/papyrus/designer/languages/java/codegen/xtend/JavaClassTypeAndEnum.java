/**
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 */
package org.eclipse.papyrus.designer.languages.java.codegen.xtend;

import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.NoCodeGen;
import org.eclipse.papyrus.designer.languages.java.codegen.utils.JavaGenUtils;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class JavaClassTypeAndEnum {
  public static CharSequence javaClassTypeAndEnum(final Classifier clazz) {
    StringConcatenation _builder = new StringConcatenation();
    {
      EList<Element> _allOwnedElements = clazz.allOwnedElements();
      for(final Element ownedElement : _allOwnedElements) {
        CharSequence _typeAndEnum = JavaClassTypeAndEnum.typeAndEnum(ownedElement);
        _builder.append(_typeAndEnum);
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }

  public static CharSequence javaClassTypeSEC(final Classifier clazz) {
    StringConcatenation _builder = new StringConcatenation();
    {
      EList<Element> _allOwnedElements = clazz.allOwnedElements();
      for(final Element ownedElement : _allOwnedElements) {
        CharSequence _typeAndSEC = JavaClassTypeAndEnum.typeAndSEC(ownedElement);
        _builder.append(_typeAndSEC);
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }

  public static CharSequence typeAndEnum(final Element element) {
    if (((!GenUtils.hasStereotype(element, NoCodeGen.class)) && (element instanceof Classifier))) {
      if (((((element instanceof Enumeration) || (element instanceof Interface)) || element.eClass().equals(UMLPackage.eINSTANCE.getClass_())) && (!(element.getOwner() instanceof org.eclipse.uml2.uml.Package)))) {
        return JavaInnerClassifiers.javaInnerClassDefinition(((Classifier) element));
      }
    }
    return null;
  }

  public static CharSequence typeAndSEC(final Element element) {
    if (((!GenUtils.hasStereotype(element, NoCodeGen.class)) && (element instanceof Classifier))) {
      if (((((element instanceof Enumeration) || (element instanceof Interface)) || element.eClass().equals(UMLPackage.eINSTANCE.getClass_())) && (!(element.getOwner() instanceof org.eclipse.uml2.uml.Package)))) {
        Boolean _isConnectSEC = JavaGenUtils.isConnectSEC(((Classifier) element));
        if ((_isConnectSEC).booleanValue()) {
          return JavaInnerClassifiers.javaInnerClassDefinitionSECM(((Classifier) element));
        }
      }
    }
    return null;
  }
}
