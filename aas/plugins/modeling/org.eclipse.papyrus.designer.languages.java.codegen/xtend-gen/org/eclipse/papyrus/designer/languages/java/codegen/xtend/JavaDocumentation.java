/**
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 */
package org.eclipse.papyrus.designer.languages.java.codegen.xtend;

import com.google.common.base.Objects;
import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class JavaDocumentation {
  public static CharSequence javaElementDoc(final Element argument) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("/**");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("* ");
    String _replaceAll = GenUtils.getComments(argument).replaceAll("\n", "\n * ");
    _builder.append(_replaceAll, " ");
    _builder.newLineIfNotEmpty();
    _builder.append(" ");
    _builder.append("*/");
    _builder.newLine();
    return _builder;
  }

  public static CharSequence javaGetPropertyDoc() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("/**");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("* Please under no circumstances change/modify this method\'s Signature ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("* and the return statement. ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("* ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("*/");
    _builder.newLine();
    return _builder;
  }

  public static CharSequence javaOperationDoc(final Operation operation) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("/**");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("* ");
    String _replaceAll = GenUtils.getComments(operation).replaceAll("\n", "\n * ");
    _builder.append(_replaceAll, " ");
    _builder.newLineIfNotEmpty();
    _builder.append(" ");
    {
      EList<Parameter> _ownedParameters = operation.getOwnedParameters();
      for(final Parameter op : _ownedParameters) {
        CharSequence _javaParamDoc = JavaDocumentation.javaParamDoc(op);
        _builder.append(_javaParamDoc, " ");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append(" ");
    _builder.append("*/");
    _builder.newLine();
    return _builder;
  }

  public static CharSequence javaGetSECOperationDoc(final Operation operation) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("/**");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("* Please under no circumstances change/modify this method\'s Signature ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("* and the return statement. ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("* ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("*/");
    _builder.newLine();
    return _builder;
  }

  public static CharSequence javaGetOperationDoc(final Operation operation) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("/**");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("* Please under no circumstances change/modify this method\'s Signature ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("* and the return statement. ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("* Any modification in the NodeId is not synchronized with the model. ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("* Please ensure the synchronization manually");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("* ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("*/");
    _builder.newLine();
    return _builder;
  }

  public static CharSequence javaAASOperationDoc(final Operation operation) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("/**");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* [Note:] The method signature, if populated with parameters, are the ");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* Input and InOutput parameters assigned by the user to the Operation. ");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* ");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* Papyrus4Manufacturing handles InOutput parameters as Input Parameters.  ");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* ");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* Please under no circumstances change/modify this method\'s Signature ");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* and the return statement. ");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* ");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* After this methods behaviour has been defined, please use the following ");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* code line (as an example) to assign the output variable of this Operation its value, ");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* if and only if an OutputVariable is defined for this Operation. ");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* ");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* Submodel1.out1_operation2_Output.setValue(<<Value Instance>>);");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*  ");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*/");
    _builder.newLine();
    return _builder;
  }

  public static CharSequence javaParamDoc(final Parameter parameter) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("* ");
    {
      ParameterDirectionKind _direction = parameter.getDirection();
      boolean _equals = Objects.equal(_direction, ParameterDirectionKind.RETURN_LITERAL);
      if (_equals) {
        _builder.append("@return");
      } else {
        _builder.append("@param ");
        String _name = parameter.getName();
        _builder.append(_name);
      }
    }
    _builder.append(" ");
    String _replaceAll = GenUtils.getComments(parameter).replaceAll("\n", "\n *    ");
    _builder.append(_replaceAll);
    _builder.newLineIfNotEmpty();
    return _builder;
  }

  public static CharSequence javaBehaviorDoc(final Behavior behavior) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("/**");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("* ");
    String _replaceAll = GenUtils.getComments(behavior).replaceAll("\n", "\n * ");
    _builder.append(_replaceAll, " ");
    _builder.newLineIfNotEmpty();
    _builder.append(" ");
    _builder.append("* ");
    {
      EList<Parameter> _ownedParameters = behavior.getOwnedParameters();
      for(final Parameter op : _ownedParameters) {
        CharSequence _javaParamDoc = JavaDocumentation.javaParamDoc(op);
        _builder.append(_javaParamDoc, " ");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append(" ");
    _builder.append("*/");
    _builder.newLine();
    return _builder;
  }
}
