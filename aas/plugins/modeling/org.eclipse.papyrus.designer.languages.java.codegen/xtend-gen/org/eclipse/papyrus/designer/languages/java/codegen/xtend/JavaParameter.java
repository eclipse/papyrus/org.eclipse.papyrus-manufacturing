/**
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 */
package org.eclipse.papyrus.designer.languages.java.codegen.xtend;

import com.google.common.base.Objects;
import org.eclipse.papyrus.designer.languages.java.codegen.utils.JavaGenUtils;
import org.eclipse.papyrus.designer.languages.java.codegen.utils.Modifier;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.Array;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.uml2.uml.ValueSpecification;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class JavaParameter {
  public static CharSequence javaOperationParameters(final Operation operation) {
    StringConcatenation _builder = new StringConcatenation();
    {
      final Function1<Parameter, Boolean> _function = (Parameter it) -> {
        return Boolean.valueOf(((!Objects.equal(it.getDirection(), ParameterDirectionKind.RETURN_LITERAL)) && (!Objects.equal(it.getDirection(), ParameterDirectionKind.OUT_LITERAL))));
      };
      Iterable<Parameter> _filter = IterableExtensions.<Parameter>filter(operation.getOwnedParameters(), _function);
      boolean _hasElements = false;
      for(final Parameter ownedParameter : _filter) {
        if (!_hasElements) {
          _hasElements = true;
        } else {
          _builder.appendImmediate(", ", "");
        }
        String _javaParameter = JavaParameter.javaParameter(ownedParameter);
        _builder.append(_javaParameter);
      }
    }
    return _builder;
  }

  /**
   * Java parameter. Default values are added, if parameter showDefault is true (implementation signature
   */
  public static String javaParameter(final Parameter parameter) {
    String _parameterModifiers = Modifier.parameterModifiers(parameter);
    String _javaType = JavaTypedElement.javaType(parameter);
    String _plus = (_parameterModifiers + _javaType);
    String _modVariadic = Modifier.modVariadic(parameter);
    String _plus_1 = (_plus + _modVariadic);
    String _plus_2 = (_plus_1 + " ");
    String _name = parameter.getName();
    return (_plus_2 + _name);
  }

  /**
   * JavaParameterCalculation for JDT
   */
  public static String javaParameterForJDT(final Parameter parameter) {
    String paramStr = JavaGenUtils.javaQualifiedName(parameter.getType(), parameter.getOperation().getOwner());
    Array _stereotypeApplication = UMLUtil.<Array>getStereotypeApplication(parameter, Array.class);
    boolean _tripleNotEquals = (_stereotypeApplication != null);
    if (_tripleNotEquals) {
      String _paramStr = paramStr;
      paramStr = (_paramStr + "[]");
    }
    return paramStr;
  }

  public static String defaultValue(final Parameter parameter) {
    String _xifexpression = null;
    ValueSpecification _defaultValue = parameter.getDefaultValue();
    boolean _tripleNotEquals = (_defaultValue != null);
    if (_tripleNotEquals) {
      String _stringValue = parameter.getDefaultValue().stringValue();
      _xifexpression = (" = " + _stringValue);
    } else {
      _xifexpression = "";
    }
    return _xifexpression;
  }
}
