/**
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 */
package org.eclipse.papyrus.designer.languages.java.codegen.xtend;

import java.util.Collection;
import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.papyrus.designer.languages.java.codegen.utils.JavaGenUtils;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.TemplateParameter;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class JavaTemplates {
  public static CharSequence templateSignature(final Classifier clazz) {
    StringConcatenation _builder = new StringConcatenation();
    {
      boolean _isTemplate = JavaTemplates.isTemplate(clazz);
      if (_isTemplate) {
        _builder.append("<");
        {
          Collection<TemplateParameter> _templateParameters = GenUtils.getTemplateParameters(clazz);
          boolean _hasElements = false;
          for(final TemplateParameter templateParam : _templateParameters) {
            if (!_hasElements) {
              _hasElements = true;
            } else {
              _builder.appendImmediate(", ", "");
            }
            String _templateTypeName = JavaGenUtils.getTemplateTypeName(templateParam);
            _builder.append(_templateTypeName);
          }
        }
        _builder.append(">");
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }

  public static boolean isTemplate(final Classifier clazz) {
    int _size = GenUtils.getTemplateParameters(clazz).size();
    return (_size > 0);
  }
}
