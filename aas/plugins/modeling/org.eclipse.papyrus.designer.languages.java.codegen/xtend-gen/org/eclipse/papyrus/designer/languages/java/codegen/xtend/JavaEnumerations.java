/**
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 */
package org.eclipse.papyrus.designer.languages.java.codegen.xtend;

import org.eclipse.emf.common.util.EList;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.EnumerationLiteral;
import org.eclipse.uml2.uml.ValueSpecification;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class JavaEnumerations {
  public static CharSequence javaEnumerationLiterals(final Enumeration enumeration) {
    StringConcatenation _builder = new StringConcatenation();
    {
      EList<EnumerationLiteral> _ownedLiterals = enumeration.getOwnedLiterals();
      boolean _hasElements = false;
      for(final EnumerationLiteral literal : _ownedLiterals) {
        if (!_hasElements) {
          _hasElements = true;
        } else {
          _builder.appendImmediate(",", "");
        }
        CharSequence _javaElementDoc = JavaDocumentation.javaElementDoc(literal);
        _builder.append(_javaElementDoc);
        _builder.newLineIfNotEmpty();
        String _name = literal.getName();
        _builder.append(_name);
        String _defaultValue = JavaEnumerations.defaultValue(literal);
        _builder.append(_defaultValue);
        _builder.newLineIfNotEmpty();
      }
      if (_hasElements) {
        _builder.append(";");
      }
    }
    return _builder;
  }

  public static String defaultValue(final EnumerationLiteral literal) {
    String _xifexpression = null;
    ValueSpecification _specification = literal.getSpecification();
    boolean _tripleNotEquals = (_specification != null);
    if (_tripleNotEquals) {
      _xifexpression = literal.getSpecification().stringValue();
    }
    return _xifexpression;
  }
}
