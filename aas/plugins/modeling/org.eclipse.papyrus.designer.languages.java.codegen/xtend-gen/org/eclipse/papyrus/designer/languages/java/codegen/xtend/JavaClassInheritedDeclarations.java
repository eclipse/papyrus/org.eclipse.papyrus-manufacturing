/**
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 */
package org.eclipse.papyrus.designer.languages.java.codegen.xtend;

import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.NoCodeGen;
import org.eclipse.papyrus.designer.languages.java.codegen.utils.JavaGenUtils;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.DirectedRelationship;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.InterfaceRealization;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class JavaClassInheritedDeclarations {
  public static CharSequence javaClassInheritedDeclarations(final Classifier clazz) {
    StringConcatenation _builder = new StringConcatenation();
    {
      if (((!(clazz instanceof Enumeration)) && (((Object[])Conversions.unwrapArray(JavaClassInheritedDeclarations.generalizations(clazz), Object.class)).length > 0))) {
        _builder.append(" extends ");
        Element _get = (((DirectedRelationship[])Conversions.unwrapArray(JavaClassInheritedDeclarations.generalizations(clazz), DirectedRelationship.class))[0]).getTargets().get(0);
        Element _get_1 = (((DirectedRelationship[])Conversions.unwrapArray(JavaClassInheritedDeclarations.generalizations(clazz), DirectedRelationship.class))[0]).getTargets().get(0);
        String _javaQualifiedName = JavaGenUtils.javaQualifiedName(((Classifier) _get), ((Classifier) _get_1).getOwner());
        _builder.append(_javaQualifiedName);
      }
    }
    {
      int _length = ((Object[])Conversions.unwrapArray(JavaClassInheritedDeclarations.realizations(clazz), Object.class)).length;
      boolean _greaterThan = (_length > 0);
      if (_greaterThan) {
        _builder.append(" implements ");
      }
    }
    {
      Iterable<DirectedRelationship> _realizations = JavaClassInheritedDeclarations.realizations(clazz);
      boolean _hasElements = false;
      for(final DirectedRelationship fr : _realizations) {
        if (!_hasElements) {
          _hasElements = true;
        } else {
          _builder.appendImmediate(", ", "");
        }
        Element _get_2 = fr.getTargets().get(0);
        Element _get_3 = fr.getTargets().get(0);
        String _javaQualifiedName_1 = JavaGenUtils.javaQualifiedName(((Classifier) _get_2), ((Classifier) _get_3).getOwner());
        _builder.append(_javaQualifiedName_1);
      }
    }
    _builder.newLineIfNotEmpty();
    return _builder;
  }

  /**
   * Return a list of relationships that are Generalizations
   * and the target does not point to a classifier that has the no-code-gen Stereotype
   */
  public static Iterable<DirectedRelationship> generalizations(final Classifier clazz) {
    final Function1<DirectedRelationship, Boolean> _function = (DirectedRelationship it) -> {
      return Boolean.valueOf((((it instanceof Generalization) && (it.getTargets().size() > 0)) && (!GenUtils.hasStereotype(it.getTargets().get(0), NoCodeGen.class))));
    };
    return IterableExtensions.<DirectedRelationship>filter(clazz.getSourceDirectedRelationships(), _function);
  }

  /**
   * Return a list of relationships that are InterfaceRealizations
   * and the target does not point to a classifier that has the no-code-gen Stereotype
   */
  public static Iterable<DirectedRelationship> realizations(final Classifier clazz) {
    final Function1<DirectedRelationship, Boolean> _function = (DirectedRelationship it) -> {
      return Boolean.valueOf((((it instanceof InterfaceRealization) && (it.getTargets().size() > 0)) && (!GenUtils.hasStereotype(it.getTargets().get(0), NoCodeGen.class))));
    };
    return IterableExtensions.<DirectedRelationship>filter(clazz.getSourceDirectedRelationships(), _function);
  }
}
