/**
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 */
package org.eclipse.papyrus.designer.languages.java.codegen.xtend;

import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.papyrus.designer.languages.java.codegen.utils.JavaGenUtils;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.ExternLibrary;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.External;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class JavaInnerClassifiers {
  public static CharSequence javaInnerClassDefinition(final Classifier classifier) {
    StringConcatenation _builder = new StringConcatenation();
    {
      boolean _noCodeGen = JavaInnerClassifiers.noCodeGen(classifier);
      boolean _not = (!_noCodeGen);
      if (_not) {
        CharSequence _javaElementDoc = JavaDocumentation.javaElementDoc(classifier);
        _builder.append(_javaElementDoc);
        _builder.newLineIfNotEmpty();
        String _classVisibility = JavaClassifierGenerator.classVisibility(classifier);
        _builder.append(_classVisibility);
        _builder.append(" ");
        String _classModifiers = JavaClassifierGenerator.classModifiers(classifier);
        _builder.append(_classModifiers);
        String _classifierType = JavaClassifierGenerator.classifierType(classifier);
        _builder.append(_classifierType);
        _builder.append(" ");
        String _name = classifier.getName();
        _builder.append(_name);
        CharSequence _templateSignature = JavaTemplates.templateSignature(classifier);
        _builder.append(_templateSignature);
        CharSequence _javaClassInheritedDeclarations = JavaClassInheritedDeclarations.javaClassInheritedDeclarations(classifier);
        _builder.append(_javaClassInheritedDeclarations);
        _builder.append(" {");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        {
          if ((classifier instanceof Enumeration)) {
            CharSequence _javaEnumerationLiterals = JavaEnumerations.javaEnumerationLiterals(((Enumeration) classifier));
            _builder.append(_javaEnumerationLiterals, "\t");
          }
        }
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        CharSequence _javaClassTypeAndEnum = JavaClassTypeAndEnum.javaClassTypeAndEnum(classifier);
        _builder.append(_javaClassTypeAndEnum, "\t");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t\t\t\t");
        String _string = JavaClassAttributesDeclaration.javaClassAttributesDeclaration(classifier).toString();
        _builder.append(_string, "\t\t\t\t\t");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        String _string_1 = JavaClassOperationsDeclaration.javaClassOperationsDeclaration(classifier).toString();
        _builder.append(_string_1, "\t");
        _builder.newLineIfNotEmpty();
        _builder.append("}");
        _builder.newLine();
      }
    }
    return _builder;
  }

  public static CharSequence javaInnerClassDefinitionSECM(final Classifier classifier) {
    StringConcatenation _builder = new StringConcatenation();
    {
      Element _owner = classifier.getOwner();
      Boolean _isOperationExists = JavaGenUtils.isOperationExists(((Classifier) _owner), classifier);
      if ((_isOperationExists).booleanValue()) {
        Element _owner_1 = classifier.getOwner();
        CharSequence _javaOperationSECDeclaration = JavaOperations.javaOperationSECDeclaration(JavaGenUtils.getOperation(((Classifier) _owner_1), classifier));
        _builder.append(_javaOperationSECDeclaration);
        _builder.newLineIfNotEmpty();
      } else {
        CharSequence _javaGetPropertyDoc = JavaDocumentation.javaGetPropertyDoc();
        _builder.append(_javaGetPropertyDoc);
        _builder.newLineIfNotEmpty();
        String _classVisibility = JavaClassifierGenerator.classVisibility(classifier);
        _builder.append(_classVisibility);
        _builder.append(" Collection<ISubmodelElement>");
        String _classModifiers = JavaClassifierGenerator.classModifiers(classifier);
        _builder.append(_classModifiers);
        _builder.append("get_");
        Element _owner_2 = classifier.getOwner();
        String _name = ((NamedElement) _owner_2).getName();
        _builder.append(_name);
        _builder.append("_");
        String _name_1 = classifier.getName();
        _builder.append(_name_1);
        _builder.append("() {");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("List<ISubmodelElement> ");
        String _name_2 = classifier.getName();
        _builder.append(_name_2, "\t");
        _builder.append(" = new LinkedList<>();");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("// Work with your Dynamic SubModelElementCollection here. ");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("return ");
        String _name_3 = classifier.getName();
        _builder.append(_name_3, "\t");
        _builder.append(";");
        _builder.newLineIfNotEmpty();
        _builder.append("}");
        _builder.newLine();
      }
    }
    return _builder;
  }

  public static CharSequence javaInnerSECClassDefinition(final Classifier classifier) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\t");
    _builder.newLine();
    CharSequence _javaClassTypeSEC = JavaClassTypeAndEnum.javaClassTypeSEC(classifier);
    _builder.append(_javaClassTypeSEC);
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t\t\t\t");
    _builder.newLine();
    return _builder;
  }

  public static boolean noCodeGen(final Element element) {
    return ((GenUtils.hasStereotype(element, External.class) || 
      GenUtils.hasStereotype(element, External.class)) || 
      GenUtils.hasStereotypeTree(element, ExternLibrary.class));
  }
}
