/*******************************************************************************
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.java.codegen.transformation;

@SuppressWarnings("nls")
public class JavaNodeId {
	public static final String PACKAGEDECLARATION= "%";
	public static final String IMPORTDECLARATION= "#";
	public static final String IMPORT_CONTAINER= "<";
	public static final String FIELD= "^";
	public static final String METHOD= "~";
	public static final String INITIALIZER= "|";
	public static final String COMPILATIONUNIT= "{";
	public static final String TYPE= "[";
}
