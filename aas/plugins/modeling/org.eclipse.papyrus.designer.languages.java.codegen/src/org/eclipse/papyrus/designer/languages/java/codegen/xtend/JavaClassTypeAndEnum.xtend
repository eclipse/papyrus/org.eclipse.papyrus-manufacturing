/*******************************************************************************
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 *******************************************************************************/
 
package org.eclipse.papyrus.designer.languages.java.codegen.xtend

import org.eclipse.papyrus.designer.languages.common.base.GenUtils
import org.eclipse.uml2.uml.Classifier
import org.eclipse.uml2.uml.Element
import org.eclipse.uml2.uml.Enumeration
import org.eclipse.uml2.uml.Interface
import org.eclipse.uml2.uml.Package
import org.eclipse.uml2.uml.UMLPackage
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.NoCodeGen
import org.eclipse.papyrus.designer.languages.java.codegen.utils.JavaGenUtils

class JavaClassTypeAndEnum {
	
	static def CharSequence javaClassTypeAndEnum(Classifier clazz) '''
		«FOR ownedElement : clazz.allOwnedElements»
			«JavaClassTypeAndEnum.typeAndEnum(ownedElement)»
		«ENDFOR»
	'''

static def CharSequence javaClassTypeSEC(Classifier clazz) '''
		«FOR ownedElement : clazz.allOwnedElements»
			«JavaClassTypeAndEnum.typeAndSEC(ownedElement)»
		«ENDFOR»
	'''
	
	static def typeAndEnum(Element element) {
		if ((!GenUtils.hasStereotype(element, NoCodeGen)) && (element instanceof Classifier)) {
			if ((element instanceof Enumeration || element instanceof Interface || element.eClass.equals(UMLPackage.eINSTANCE.class_)) && !(element.owner instanceof Package)) {
				return JavaInnerClassifiers.javaInnerClassDefinition(element as Classifier)
			}
		}
	}
	
	static def typeAndSEC(Element element) {
		if ((!GenUtils.hasStereotype(element, NoCodeGen)) && (element instanceof Classifier)) {
			if ((element instanceof Enumeration || element instanceof Interface || element.eClass.equals(UMLPackage.eINSTANCE.class_)) && !(element.owner instanceof Package)) {
				if (JavaGenUtils.isConnectSEC(element as Classifier)) {
					return JavaInnerClassifiers.javaInnerClassDefinitionSECM(element as Classifier)
				}
			}
		}
	}
}
