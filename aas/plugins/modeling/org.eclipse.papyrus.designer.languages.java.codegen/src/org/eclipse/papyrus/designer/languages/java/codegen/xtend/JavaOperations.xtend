/*******************************************************************************
 * Copyright (c) 2014 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.java.codegen.xtend

import org.eclipse.papyrus.designer.languages.common.base.GenUtils
import org.eclipse.papyrus.designer.languages.java.codegen.Constants
import org.eclipse.papyrus.designer.languages.java.codegen.utils.Modifier
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.Default
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.Classifier
import org.eclipse.uml2.uml.Property
import org.eclipse.uml2.uml.DataType
import org.eclipse.uml2.uml.Enumeration
import org.eclipse.uml2.uml.Interface
import org.eclipse.uml2.uml.Operation
import org.eclipse.uml2.uml.profile.standard.Create
import org.eclipse.uml2.uml.util.UMLUtil
import static extension org.eclipse.papyrus.designer.languages.java.codegen.xtend.JavaTypedElement.javaType
import org.eclipse.uml2.uml.Parameter
import org.eclipse.papyrus.designer.languages.java.codegen.utils.JavaGenUtils
import org.eclipse.uml2.uml.ParameterDirectionKind
import org.eclipse.uml2.uml.NamedElement

class JavaOperations {
	
	
	
	static def javaOperationImplementation(Operation operation) '''	
		«GenUtils.getBody(operation, Constants.supportedLanguages)»
	'''

	static def javaReturnSpec(Operation operation) {
		if (isConstructor(operation)) {
			constructorOrVoid(operation)
		} else {
			operation.ownedParameters.filter[it.direction == ParameterDirectionKind.OUT_LITERAL].head.javaTypeOrVoid + ' '
		}
	}


static def javaSECReturnSpec(Operation operation) {
		if (isConstructor(operation)) {
			constructorOrVoid(operation)
		} else {
			operation.ownedParameters.filter[it.direction == ParameterDirectionKind.OUT_LITERAL].head.javaSECTypeOrVoid + ' '
		}
	}
	static def javaTypeOrVoid(Parameter parameter) {
		if (parameter !== null) {
			return parameter.javaType
		} else {
			return "void"	
		}
	}

	static def javaSECTypeOrVoid(Parameter parameter) {
		if (parameter !== null) {
			return parameter.name.substring(5);
		} else {
			return "void"
		}
	}
	static def throwss(Operation operation) '''
		«IF operation.raisedExceptions.length > 0»
			throws «FOR re : operation.raisedExceptions SEPARATOR ','»«re.name»«ENDFOR»
		«ENDIF»
	'''

	static def constructorOrVoid(Operation operation) {
		if (isConstructor(operation)) {
			''
		} else {
			'void '
		}
	}

	static def isConstructor(Operation operation) {
		GenUtils.hasStereotype(operation, Create)
	}

	// return a list of owned operations, return emptyset, if null
	static def getOwnedOperations(Classifier cl) {
		val operations = getOwnedOperationsWNull(cl)
		if (operations === null) {
			emptySet
		} else {
			operations
		}
	}

	// return a list of owned operations, since this is not supported directly on a classifier
	static def getOwnedOperationsWNull(Classifier cl) {
		if (cl instanceof Class) {
			(cl as Class).ownedOperations
		} else {
			if (cl instanceof DataType) {
				(cl as DataType).ownedOperations
			} else {
				if (cl instanceof Interface) {
					(cl as Interface).ownedOperations
				} else {
					// Sequence{}
				}
			}
		}
	}

	static def javaOperationDeclaration(Operation operation) '''
	
		«Modifier.methodModifiers(operation)»«JavaOperations.javaReturnSpec(operation)»«operation.name»(«JavaParameter.javaOperationParameters(operation)») «throwss(operation)»«IF mustGenerateBody(operation)»{«ELSE»;«ENDIF»
			«IF mustGenerateBody(operation)»«JavaOperations.javaOperationImplementation(operation)»«ENDIF»
		«IF mustGenerateBody(operation)»}«ENDIF»
	'''
	
	static def javaAASOperationDeclaration(Operation operation) '''
	
		«JavaDocumentation.javaAASOperationDoc(operation)»
		«Modifier.methodModifiers(operation)»«JavaOperations.javaReturnSpec(operation)»«(operation.getOwner as NamedElement).name»_«operation.name»(«JavaParameter.javaOperationParameters(operation)») «throwss(operation)»«IF mustGenerateBody(operation)»{«ELSE»;«ENDIF»
			«IF JavaGenUtils.hasOpaqueBehavior(operation)»«JavaOperations.javaOperationImplementation(operation)»
			«ELSE»«JavaOperations.javaAASOperationImplementation(operation)»«ENDIF»
		«IF mustGenerateBody(operation)»}«ENDIF»
	'''
	
	static def javaAASOperationImplementation(Operation operation) '''
	«IF hasReturnType(operation)»
		return («getReturnType(operation)») «(operation.owner as Classifier).name».«(operation.owner as Classifier).name»_«operation.ownedParameters.filter[it.direction == ParameterDirectionKind.OUT_LITERAL].head.name»_«operation.name»_Output.getValue();			
	«ENDIF»
	'''
	
	static def javaGetOperationDeclaration(Operation operation) '''
	
		«JavaDocumentation.javaGetOperationDoc(operation)»
		«Modifier.methodModifiers(operation)»«JavaOperations.javaReturnSpec(operation)»«operation.name»(«JavaParameter.javaOperationParameters(operation)») «throwss(operation)»«IF mustGenerateBody(operation)»{«ELSE»;«ENDIF»
			«IF mustGenerateBody(operation)»«JavaOperations.javaOperationImplementation(operation)»«ENDIF»
		«IF mustGenerateBody(operation)»}«ENDIF»
	'''
	
	static def javagetOperationDeclaration(Property property, Classifier c) '''
	«IF JavaGenUtils.isOperationExists(c,property)»
	«javaGetOperationDeclaration(JavaGenUtils.getOperation(c, property))»
	«ELSE»
	«JavaAttribute.javaPropertyAttributeDeclaration(property)» get_«(property.getOwner as NamedElement).name»_«property.name»() {
	«JavaAttribute.javaPropertyAttributeBody(property)»}«ENDIF»

	'''


	static def isAbstract(Operation operation) {
		return (operation.isAbstract && operation.owner instanceof Classifier &&
			((operation.owner as Classifier).isAbstract || operation.owner instanceof Enumeration)) ||
			operation.interface !== null
	}

	static def isDefault(Operation operation) {
		return (operation.interface !== null && UMLUtil.getStereotypeApplication(operation, Default) !== null)
	}

	static def mustGenerateBody(Operation operation) {
		if (!isAbstract(operation)) { // Operation is not abstract (not part of interface and not abstract in abstract class)
			return true;
		} else if (operation.getInterface() === null) { // Operation is abstract and in an abstract class (unless it is static which means it shouldn't pass validation)
			if (operation.isStatic) {
				return true;
			} else {
				return false;
			}
		} else if (operation.getInterface() !== null) { // Operation is in an interface
			return operation.isStatic || UMLUtil.getStereotypeApplication(operation, Default) !== null
		}

		return true;
	}
	
	static def hasReturnType(Operation operation) {
		
	return ! (operation.ownedParameters.filter[it.direction == ParameterDirectionKind.OUT_LITERAL].head.javaTypeOrVoid.equals("void"));
		
	}
	
	static def getReturnType(Operation operation) {
		
	return operation.ownedParameters.filter[it.direction == ParameterDirectionKind.OUT_LITERAL].head.javaTypeOrVoid ;
		
	}
	
	static def javaOperationSECDeclaration(Operation operation) '''
	«IF operation !== null»
		«JavaDocumentation.javaGetSECOperationDoc(operation)»
		«Modifier.methodModifiers(operation)»«JavaOperations.javaSECReturnSpec(operation)»«operation.name»(«JavaParameter.javaOperationParameters(operation)») «throwss(operation)»«IF mustGenerateBody(operation)»{«ELSE»;«ENDIF»
			«IF mustGenerateBody(operation)»«JavaOperations.javaOperationImplementation(operation)»«ENDIF»
		«IF mustGenerateBody(operation)»}«ENDIF»
	«ENDIF»
	'''
	
	
}
