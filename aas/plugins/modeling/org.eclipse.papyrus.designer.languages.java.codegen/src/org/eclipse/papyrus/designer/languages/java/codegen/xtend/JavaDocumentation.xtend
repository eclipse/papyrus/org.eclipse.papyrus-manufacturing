/*******************************************************************************
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 *******************************************************************************/
 
package org.eclipse.papyrus.designer.languages.java.codegen.xtend

import org.eclipse.uml2.uml.Element
import org.eclipse.uml2.uml.Operation
import org.eclipse.uml2.uml.Parameter
import org.eclipse.uml2.uml.Behavior
import org.eclipse.uml2.uml.ParameterDirectionKind
import org.eclipse.papyrus.designer.languages.common.base.GenUtils

class JavaDocumentation {
	static def javaElementDoc(Element argument) '''
		/**
		 * «GenUtils.getComments(argument).replaceAll('\n', '\n * ')»
		 */
	'''
	static def javaGetPropertyDoc() '''
		/**
			* Please under no circumstances change/modify this method's Signature 
			* and the return statement. 
			* 
			*/
	'''
	
	static def javaOperationDoc(Operation operation) '''
		/**
		 * «GenUtils.getComments(operation).replaceAll('\n', '\n * ')»
		 «FOR op: operation.ownedParameters»«javaParamDoc(op)»«ENDFOR»
		 */
	'''
	static def javaGetSECOperationDoc(Operation operation) '''
		/**
			* Please under no circumstances change/modify this method's Signature 
			* and the return statement. 
			* 
			*/
	'''
	
	static def javaGetOperationDoc(Operation operation) '''
		/**
			* Please under no circumstances change/modify this method's Signature 
			* and the return statement. 
			* Any modification in the NodeId is not synchronized with the model. 
			* Please ensure the synchronization manually
			* 
			*/
	'''
	
	static def javaAASOperationDoc(Operation operation) '''
		/**
			 * [Note:] The method signature, if populated with parameters, are the 
			 * Input and InOutput parameters assigned by the user to the Operation. 
			 * 
			 * Papyrus4Manufacturing handles InOutput parameters as Input Parameters.  
			 * 
			 * Please under no circumstances change/modify this method's Signature 
			 * and the return statement. 
			 * 
			 * After this methods behaviour has been defined, please use the following 
			 * code line (as an example) to assign the output variable of this Operation its value, 
			 * if and only if an OutputVariable is defined for this Operation. 
			 * 
			 * Submodel1.out1_operation2_Output.setValue(<<Value Instance>>);
			 *  
			 */
	'''
	
	static def javaParamDoc(Parameter parameter) '''
		* «IF (parameter.direction == ParameterDirectionKind.RETURN_LITERAL)
		  »@return«
		  ELSE
		  »@param «parameter.name»«ENDIF» «GenUtils.getComments(parameter).replaceAll('\n', '\n *    ')»
	'''
	
	static def javaBehaviorDoc(Behavior behavior) '''
		/**
		 * «GenUtils.getComments(behavior).replaceAll('\n', '\n * ')»
		 * «FOR op : behavior.ownedParameters»«javaParamDoc(op)»«ENDFOR»
		 */
	'''
}
