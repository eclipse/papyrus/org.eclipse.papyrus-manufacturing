/*******************************************************************************
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.java.codegen.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IProject;
import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.aas.NodeId;
import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.papyrus.designer.infra.base.StringConstants;
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.NoCodeGen;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.ExternLibrary;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.External;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.ClassifierTemplateParameter;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.LiteralBoolean;
import org.eclipse.uml2.uml.LiteralInteger;
import org.eclipse.uml2.uml.LiteralReal;
import org.eclipse.uml2.uml.LiteralString;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.ParameterableElement;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.TemplateBinding;
import org.eclipse.uml2.uml.TemplateParameter;
import org.eclipse.uml2.uml.TemplateParameterSubstitution;
import org.eclipse.uml2.uml.TemplateSignature;
import org.eclipse.uml2.uml.TypedElement;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.ValueSpecification;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Some utilities specific to Java code generation
 */
public class JavaGenUtils {

	private static NamedElement currentNE;
	public static Map<NamedElement, EList<String>> imports = new HashMap<NamedElement, EList<String>>();

	/**
	 * The standard UML, MARTE, and JavaPrimitive libraries provide some primitives
	 * to be translated as Java primitives
	 *
	 * @param type
	 * @return the name of a standard type
	 */

	public static String getJavaTypeFromUML(String umlTypeName) {
		switch (umlTypeName) {
		/////////
		// UML Primitive Types
		////////
		case "Boolean":
			return "Boolean";
		case "Integer":
			return "Integer";
		case "Real":
			return "Float";
		case "String":
			return "String";
		// map this type to Integer
		case "UnlimitedNatural":
			return "BigInteger";

		/////////
		// Ecore Primitive Types
		////////
		case "EString":
			return "String";
		case "EBoolean":
			return "Boolean";
		case "EDate":
			return "XMLGregorianCalendar";
		case "EDouble":
			return "Double";
		case "EInt":
			return "Integer";
		case "EFloat":
			return "Float";

		/////////
		// XML DataTypes
		/////////
		case "Date":
			return "XMLGregorianCalendar";
		case "AnySimpleType":
			return "String";
		case "IDREFS":
			return "String";
		case "Base64Binary":
			return "Byte []";
		case "AnyURI":
			return "String";
		case "Double":
			return "Double";
		case "Duration":
			return "Duration";
		case "Float":
			return "Float";
		case "GDay":
			return "XMLGregorianCalendar";
		case "GMonthDay":
			return "XMLGregorianCalendar";
		case "GMonth":
			return "XMLGregorianCalendar";
		case "GYear":
			return "XMLGregorianCalendar";
		case "GYearMonth":
			return "XMLGregorianCalendar";
		case "HexBinary":
			return "Byte []";
		case "NOTATION":
			return "QName";
		case "QName":
			return "QName";
		case "NonNegativeInteger":
			return "BigInteger";
		case "NonPositiveInteger":
			return "BigInteger";
		case "Int":
			return "Integer";
		case "Long":
			return "Long";
		case "Short":
			return "Short";
		case "Byte":
			return "Byte";
		case "NegativeInteger":
			return "BigInteger";
		case "PositiveInteger":
			return "BigInteger";
		case "UnsignedLong":
			return "BigInteger";
		case "UnsignedByte":
			return "Short";
		case "UnsignedInt":
			return "Long";
		case "UnsignedShort":
			return "Integer";
		case "ID":
			return "String";
		case "IDREF":
			return "String";
		case "ENTITY":
			return "String";
		// These include java primitives types: int, byte, short, long, float, double,
		// boolean, and char
		case "int":
			return "int";
		case "boolean":
			return "boolean";
		case "long":
			return "long";
		case "short":
			return "short";
		case "byte":
			return "byte";
		case "float":
			return "float";
		case "double":
			return "double";
		case "char":
			return "char";

		default:
			return "Object";

		}
	}

	public static String getStdtypes(PrimitiveType type) {
		Object owner = type.getOwner();
		String owningPkgName = StringConstants.EMPTY;
		if (owner instanceof Package) {
			owningPkgName = ((Package) owner).getName();
		}
		if (owningPkgName.equals("PrimitiveTypes") || // used in UML >= 2.4 //$NON-NLS-1$
				owningPkgName.equals("UMLPrimitiveTypes") || // used in UML < 2.4 //$NON-NLS-1$
				owningPkgName.equals("XMLPrimitiveTypes") || owningPkgName.equals("JavaPrimitiveTypes")) { //$NON-NLS-1$
			String td = StringConstants.EMPTY;
			String name = type.getName();

			td = getJavaTypeFromUML(name);
			// case unrecognized type
			// we should also show a message saying that custom dataTypes, which are not
			// supported in Basyx, are not supported in Papyrus4Manufacturing code
			// generation as well
			return td;
		}
		return StringConstants.EMPTY;
	}

	/**
	 * Return a kind of qualifiedName, except if - The named element has the
	 * stereotype External or NoCodeGen - The named element is part of the Java XML
	 * library - The named element is a primitive type that has no further
	 * definition via a stereotype (TODO: why is this required/useful?)
	 *
	 * @param ne
	 * @param ns scope in which ne is used (classifier)
	 * @return the qualified name
	 */

	public static String javaQualifiedName(NamedElement ne, Element ns) {
		if (ne == null) {
			return "undefined"; //$NON-NLS-1$
		}
		Object owner = ne.getOwner();
		String owningPkgName = StringConstants.EMPTY;
		if (owner instanceof Package) {
			owningPkgName = ((Package) owner).getName();
		}

		if (GenUtils.hasStereotype(ne, External.class) || GenUtils.hasStereotypeTree(ne, NoCodeGen.class)
				|| GenUtils.hasStereotypeTree(ne, ExternLibrary.class)) {
			return ne.getName();
		} else if (ne instanceof PrimitiveType) {
			String stdType = getStdtypes((PrimitiveType) ne);
			if (!stdType.isEmpty()) {
				return stdType;
			}
		} else if (owner instanceof ClassifierTemplateParameter) {
			// return short name for template in Type
			return ne.getName();
		} else if (ne instanceof Classifier && !((Classifier) ne).getTemplateBindings().isEmpty()) { // TODO template
																										// stereotype
			TemplateBinding templateBinding = ((Classifier) ne).getTemplateBindings().get(0);
			TemplateSignature signature = templateBinding.getSignature();

			String specializedTypeName = StringConstants.EMPTY;

			if (signature != null && signature.getTemplate() instanceof Classifier) {
				specializedTypeName = javaQualifiedName((Classifier) signature.getTemplate(), ne);

				List<TemplateParameter> templateParameters = signature.getOwnedParameters();
				Map<TemplateParameter, NamedElement> parameterToClassMap = new HashMap<TemplateParameter, NamedElement>();

				List<TemplateParameterSubstitution> substitutions = templateBinding.getParameterSubstitutions();
				for (TemplateParameterSubstitution substitution : substitutions) {
					if (substitution.getFormal() != null && templateParameters.contains(substitution.getFormal())
							&& substitution.getActual() instanceof NamedElement) {
						parameterToClassMap.put(substitution.getFormal(), (NamedElement) substitution.getActual());
					}
				}

				if (parameterToClassMap.size() == templateParameters.size()) {
					specializedTypeName += "<"; //$NON-NLS-1$

					for (TemplateParameter templateParameter : templateParameters) {
						NamedElement substitutionType = parameterToClassMap.get(templateParameter);
						specializedTypeName += javaQualifiedName(substitutionType, ne) + ", "; //$NON-NLS-1$
					}

					specializedTypeName = specializedTypeName.substring(0, specializedTypeName.length() - 2);
					specializedTypeName += ">"; //$NON-NLS-1$
				}

				return specializedTypeName;
			}
		}

		// Get qualified name and remove root name if root is a <<Project>>
		String qName = GenUtils.getFullName(ne, ".", false); //$NON-NLS-1$

		// First check that the ne is not a direct inner class of ns
		// Also check that ne does not have the same short name as a direct inner class
		// of ns
		if (ns instanceof Classifier) {
			Classifier classifier = (Classifier) ns;
			for (Element directlyOwnedElement : classifier.getOwnedElements()) {
				if (directlyOwnedElement instanceof Enumeration || directlyOwnedElement instanceof Interface
						|| directlyOwnedElement.eClass().equals(UMLFactory.eINSTANCE.getUMLPackage().getClass_())) {
					if (((Classifier) directlyOwnedElement).getQualifiedName().equals(ne.getQualifiedName())) {
						return ne.getName();
					} else if (((Classifier) directlyOwnedElement).getName().equals(ne.getName())) {
						return qName;
					}
				}
			}
		}

		// Then check that ne hasn't been imported
		EList<String> importsOfCurrentNs = imports.get(currentNE);
		if (importsOfCurrentNs != null) {
			for (String importOfCurrentNs : importsOfCurrentNs) {
				if (importOfCurrentNs.equals(qName)) {
					// ne is imported in the current ns, so we use its short name
					return ne.getName();
				}
			}
		}

		// We return the qualified name of ne otherwise
		return qName;
	}

	/**
	 * Returns the string that is used within a Java template declaration, e.g. the
	 * "Class XY" in template<class XY>.
	 *
	 * @return the template type formated as string
	 */
	public static String getTemplateTypeName(TemplateParameter templateParam) {
		String name = StringConstants.EMPTY;

		// Retrieve name of the ParameteredElement (when possible = it is a NamedElement
		ParameterableElement pElt = templateParam.getParameteredElement();
		if ((pElt != null) && (pElt instanceof NamedElement)) {
			name = ((NamedElement) pElt).getName();
		} else {
			name = "undefined"; //$NON-NLS-1$
		}

		return (name);
	}

	/**
	 * Return a Java namespace definition for a named element
	 *
	 * @param ne a named element
	 * @return a Java namespace definition for a named element
	 */
	public static String getNamespace(NamedElement ne) {
		String namespace = StringConstants.EMPTY;
		for (Namespace ns : ne.allNamespaces()) {
			if (ns.getOwner() != null) {
				String nsName = ns.getName();
				if (!namespace.equals(StringConstants.EMPTY)) {
					nsName += "::"; //$NON-NLS-1$
				}
				namespace = nsName + namespace;
			}
		}
		if (!namespace.equals(StringConstants.EMPTY)) {
			namespace = "\n" + "using namespace " + namespace + ";\n"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		return namespace;
	}

	/**
	 * Update the currentNS (used to organize imports and types)
	 * 
	 * @param ne a named element
	 */
	public static void openNS(NamedElement ne) {
		currentNE = ne;
	}

	public static String getFullPath(Classifier classe, IProject project, String separator, boolean withProjectName) {
		String qName = getFullPath(classe, project, separator);

		return qName;
	}

	public static String getFullPath(Classifier classe, IProject project, String seperator) {
		return project.getName().toLowerCase().concat(".module.submodels");

	}

	public static Boolean isOperationExists(Classifier c, Property p) {
		for (Operation o : getAllownedOperations(c)) {
			// get new name concatenated with the parent name
			String name = ((NamedElement) p.getOwner()).getName() + "_" + p.getName();
			if (o.getName().equals("get_" + name))
				return true;
		}
		return false;
	}

	public static Boolean hasOpaqueBehavior(Operation o) {
		Boolean result = false;
		result = o.getMethods() != null ? !o.getMethods().isEmpty() : false;
		return result;
	}

	public static Boolean isOperationCreatedByUser(Operation o, Classifier c) {

		// check if it is a get_PropertyName operation

		for (Property p : getAllownedAttributes(c)) {
			String name = ((NamedElement) p.getOwner()).getName() + "_" + p.getName();
			if (o.getName().equals("get_" + name))
				return false;
		}
		// check if it is a get_SEC operation
		for (Element inneclass : c.getOwnedElements()) {
			if (inneclass instanceof Classifier) {
				String name = ((Classifier) ((Classifier) inneclass).getOwner()).getName() + "_"
						+ ((Classifier) inneclass).getName();
				if (o.getName().equals("get_" + name))
					return false;
			}
		}

		return true;
	}

	public static Boolean isOperationExists(Classifier c, Classifier p) {
		Classifier submodel = (Classifier) JavaGenUtils.getSubmodel(c);
		for (Operation o : getAllownedOperations(submodel)) {
			String newname = ((Classifier) p.getOwner()).getName() + "_" + p.getName();
			if (o.getName().equals("get_" + newname))
				return true;
		}
		return false;
	}

	/**
	 * @param c
	 * @return
	 */
	private static Element getSubmodel(Classifier c) {

		String qualifiedName = "";
		Element owner = c;
		if (isSubmodel(owner)) {
			return c;
		} else {
			while (!isSubmodel(owner)) {

				owner = owner.getOwner();
			}
		}
		return owner;
	}

	public static Boolean isSubmodel(Element c) {
		return GenUtils.hasStereotype(c, org.eclipse.papyrus.aas.Submodel.class);
	}

//	public static Operation getOperation(Classifier c, String name) {
//		// reconstitute the name by concatinating the parent
//		// true only if the parent is the classifier
//		String newName = "get_"+c.getName()+"_"+name;
//		for (Operation o : getAllownedOperations(c)) {
//			if (o.getName().equals(newName))
//				return o;
//		}
//		return null;
//	}
//	
	public static Operation getOperation(Classifier c, Classifier sec) {
		// reconstitute the name by concatinating the parent
		// true only if the parent is the classifier
		String newName = "get_" + ((NamedElement) sec.getOwner()).getName() + "_" + sec.getName();
		for (Operation o : getAllownedOperations(c)) {
			if (o.getName().equals(newName))
				return o;
		}
		return null;
	}

	public static Operation getOperation(Classifier c, Property p) {
		// reconstitute the name by concatinating the parent

		String name = "get_" + ((NamedElement) p.getOwner()).getName() + "_" + p.getName();
		for (Operation o : getAllownedOperations(c)) {
			if (o.getName().equals(name))
				return o;
		}
		return null;
	}

	public static Boolean isConnectProperty(Property property) {
		if (GenUtils.hasStereotype(property, org.eclipse.papyrus.aas.Property.class)) {
			org.eclipse.papyrus.aas.Property aasProp = UMLUtil.getStereotypeApplication(property,
					org.eclipse.papyrus.aas.Property.class);
			if (aasProp != null)
				return aasProp.isDynamic();
		}
		return false;
	}

	public static Boolean isConnectSEC(Classifier classifier) {
		if (GenUtils.hasStereotype(classifier, org.eclipse.papyrus.aas.SubmodelElementCollection.class)) {
			org.eclipse.papyrus.aas.SubmodelElementCollection sec = UMLUtil.getStereotypeApplication(classifier,
					org.eclipse.papyrus.aas.SubmodelElementCollection.class);
			if (sec != null)
				return sec.isDynamic();
		}
		return false;
	}

	public static Boolean hasNodeId(Property property) {
		if (GenUtils.hasStereotype(property, org.eclipse.papyrus.aas.Property.class)) {
			org.eclipse.papyrus.aas.Property aasProp = UMLUtil.getStereotypeApplication(property,
					org.eclipse.papyrus.aas.Property.class);
			if (aasProp != null && aasProp.getNodeId() != null)
				return true;
		}
		return false;
	}

	public static Boolean hasEndPoint(Property property) {
		if (GenUtils.hasStereotype(property, org.eclipse.papyrus.aas.Property.class)) {
			org.eclipse.papyrus.aas.Property aasProp = UMLUtil.getStereotypeApplication(property,
					org.eclipse.papyrus.aas.Property.class);
			if (aasProp != null && aasProp.getEndPoint() != null)
				return true;
		}
		return false;
	}

	public static Integer getNameSpace(Property property) {
		if (hasNodeId(property)) {
			org.eclipse.papyrus.aas.Property aasProp = UMLUtil.getStereotypeApplication(property,
					org.eclipse.papyrus.aas.Property.class);
			return aasProp.getNodeId().getNameSpaceIndex();
		}

		return 0;
	}

	public static String getIdentifier(Property property) {
		if (hasNodeId(property)) {
			org.eclipse.papyrus.aas.Property aasProp = UMLUtil.getStereotypeApplication(property,
					org.eclipse.papyrus.aas.Property.class);
			NodeId nodeid = aasProp.getNodeId();
			if (nodeid != null) {
				if (nodeid.getIdType().getLiteral().equals("String")) {
					return "\"" + aasProp.getNodeId().getIdentifier() + "\"";
				}

				return aasProp.getNodeId().getIdentifier();
			}
		}
		return "";
	}

	public static String getEndPointName(Property property) {
		if (hasEndPoint(property)) {
			org.eclipse.papyrus.aas.Property aasProp = UMLUtil.getStereotypeApplication(property,
					org.eclipse.papyrus.aas.Property.class);
			return aasProp.getEndPoint().getName();
		}
		return "";
	}

	public static String getEndPointProtocol(Property property) {
		if (hasEndPoint(property)) {
			org.eclipse.papyrus.aas.Property aasProp = UMLUtil.getStereotypeApplication(property,
					org.eclipse.papyrus.aas.Property.class);
			return aasProp.getEndPoint().getProtocol().getName();
		}
		return "";
	}

	/**
	 * @param operation         the operation
	 * @param selectedLanguages a pattern containing one or more languages
	 * @return Return the first body of a selected language that is provided by one
	 *         of the operation's methods
	 */
	public static String getBody(Operation operation, Pattern selectedLanguages) {

		for (Behavior behavior : operation.getMethods()) {
			if (behavior instanceof OpaqueBehavior) {
				return GenUtils.getBodyFromOB((OpaqueBehavior) behavior, selectedLanguages);
			}
		}
		if (operation != null && GenUtils.hasStereotype(operation, "AAS::Operation")
				&& ParameterUtils.getOutParameter(operation.getOwnedParameters()) != null) {
			Parameter outparam = ParameterUtils.getOutParameter(operation.getOwnedParameters());
			return "(" + outparam.getType().getName() + ")" + ((Classifier) operation.getOwner()).getName() + "."
					+ outparam.getName() + "_" + operation.getName() + "_Output.getValue();";
		}
		return ""; //$NON-NLS-1$
	}

	public static List<Property> getAllownedAttributes(Classifier classifier) {

		List<Property> allattributes = new ArrayList();
		// direct submodel properties
		allattributes.addAll(classifier.allAttributes());
		for (Element el : classifier.allOwnedElements()) {
			if (el instanceof Classifier) {
				// check that the SEC is not dynamic other wise do not add its attributes
				if (GenUtils.hasStereotype(classifier, org.eclipse.papyrus.aas.SubmodelElementCollection.class)
						&& !JavaGenUtils.isConnectSEC(((Classifier) el))) {
					allattributes.addAll(((Classifier) el).allAttributes());
				}
			}
		}
		return allattributes;
	}

	public static List<Operation> getAllownedOperations(Classifier classifier) {
		List<Operation> alloperations = new ArrayList();
		alloperations.addAll(classifier.getAllOperations());
		for (Element el : classifier.allOwnedElements()) {
			if (el instanceof Classifier) {
				alloperations.addAll(((Classifier) el).getAllOperations());
			}
		}
		return alloperations;
	}

	public static String getXMLTypes(TypedElement element) {

		String td = "Object";
		String[] strings = element.getName().split("\\.");
		if (strings != null && strings.length > 1) {
			String type = strings[1];
			td = getJavaTypeFromUML(type);

		}
		return td;
	}

	public static String removedot(String element) {

		String result = element;
		String[] strings = element.split("\\.");
		if (strings != null && strings.length > 1) {
			result = strings[1];

		}
		return result;
	}

	public static String getPropertyValue(org.eclipse.uml2.uml.Property property) {
		ValueSpecification vs = property.getDefaultValue();
		if (vs == null) {
			if (property.getType() instanceof PrimitiveType) {
				PrimitiveType pt = (PrimitiveType) property.getType();
				if (pt.getName().equals("int")) {
					return "0";
				}
			}
			return "null";
		} else {

			if (vs instanceof LiteralString && property.getType().getName().equals("String")) {
				return "\"" + vs.stringValue() + "\"";
			} else if (vs instanceof LiteralInteger) {
				return vs.integerValue() + "";
			} else if (vs instanceof LiteralBoolean) {
				return vs.booleanValue() + "";
			} else if (vs instanceof LiteralReal) {
				return vs.realValue() + "";
			}
			return vs.stringValue();
		}
	}

}
