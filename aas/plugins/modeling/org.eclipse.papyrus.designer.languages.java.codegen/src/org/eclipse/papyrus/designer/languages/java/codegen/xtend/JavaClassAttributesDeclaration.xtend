/*******************************************************************************
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 *******************************************************************************/
 
package org.eclipse.papyrus.designer.languages.java.codegen.xtend

import org.eclipse.uml2.uml.Classifier
import org.eclipse.papyrus.designer.languages.common.base.GenUtils
import org.eclipse.papyrus.aas.Property
import org.eclipse.papyrus.designer.languages.java.codegen.utils.JavaGenUtils

class JavaClassAttributesDeclaration {
	
	static def javaClassAttributesDeclaration(Classifier clazz) '''
		«FOR oa : JavaAttribute.getOwnedAttributes(clazz)»
			«JavaAttribute.javaAttributeDeclaration(oa)»
		«ENDFOR»
	'''
	
	static def javaClassPropertyAttributesDeclaration(Classifier clazz) '''
		«FOR oa : JavaGenUtils.getAllownedAttributes(clazz)»
		«IF JavaGenUtils.isConnectProperty(oa)»«JavaOperations.javagetOperationDeclaration(oa, clazz)»«ENDIF»
		«ENDFOR»
		
	'''
	static def javaClassprivateAttributesDeclaration() {
	return 'private ConnectedDevices connectedDevices;'
	}
	
	static def javaClassPropertyprivateAttributesDeclaration() {
	return 'private ConnectedDevices connectedDevices;'+'\n'+ 'final ConceptDescriptions conceptDescriptions;'
	}
}