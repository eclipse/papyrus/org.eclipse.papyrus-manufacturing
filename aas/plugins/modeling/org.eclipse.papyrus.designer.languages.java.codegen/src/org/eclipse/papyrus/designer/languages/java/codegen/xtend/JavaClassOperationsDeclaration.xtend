/*******************************************************************************
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 *******************************************************************************/
 
package org.eclipse.papyrus.designer.languages.java.codegen.xtend

import org.eclipse.papyrus.designer.languages.common.base.GenUtils
import org.eclipse.uml2.uml.Classifier
import org.eclipse.papyrus.aas.Operation
import org.eclipse.papyrus.designer.languages.java.codegen.utils.JavaGenUtils

class JavaClassOperationsDeclaration {
	static def javaClassOperationsDeclaration(Classifier clazz) '''
		«FOR op : JavaGenUtils.getAllownedOperations(clazz)»
			«IF GenUtils.hasStereotype(op, Operation)»«JavaOperations.javaAASOperationDeclaration(op)»«ENDIF»
			«IF!GenUtils.hasStereotype(op, Operation) && JavaGenUtils.isOperationCreatedByUser(op, clazz)»«JavaOperations.javaOperationDeclaration(op)»«ENDIF»
		«ENDFOR»
	'''
	static def javaClassPropertyOperationsDeclaration(Classifier clazz) '''
		«FOR op : JavaGenUtils.getAllownedOperations(clazz)»
			«IF ! GenUtils.hasStereotype(op, Operation)»«JavaOperations.javaOperationDeclaration(op)»«ENDIF»
		«ENDFOR»
	'''
	
	static def javaClassConstructorDeclaration(Classifier clazz) {
		//var String result = 'public '+ clazz.getName()+'Operations'+' (ConnectedDevices connectedDevices) {
		//this.connectedDevices = connectedDevices;}'
		var String result2 = ' public DynamicElementsWorkspace '+' (ConnectedDevices connectedDevices) {
		this.connectedDevices = connectedDevices;}'	
		return result2;
		}
	
	static def javaClassPropertyConstructorDeclaration(Classifier clazz) {
		// For AAS we need to return a specific constructor for all submodel classes
		var String result = 'public '+ clazz.getName()+'ValueDelegatesJDT'+' (ConnectedDevices connectedDevices, 
			ConceptDescriptions conceptDescriptions) {
		this.connectedDevices = connectedDevices;
		this.conceptDescriptions = conceptDescriptions;
	}'

		return result;
		}
	
}