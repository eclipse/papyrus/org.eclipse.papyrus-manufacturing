/*******************************************************************************
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.java.codegen.xtend

import org.eclipse.papyrus.designer.languages.common.base.GenUtils
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.Import
import org.eclipse.uml2.uml.NamedElement
import org.eclipse.uml2.uml.util.UMLUtil
import org.eclipse.core.resources.IProject

class JavaImportUtil {
	static def importDirective(String path) {
		if ((path !== null) && (path.length > 0))
			return '''import ''' + path + ';'
	}

	static def javaImport(NamedElement ne, IProject project) {
		if (GenUtils.hasStereotype(ne, Import)) {
			val import = UMLUtil.getStereotypeApplication(ne, Import)
			var header = import.manualImports
			if ((header !== null) && (header.length > 0)) {
				var includeHeader = GenUtils.cleanCR(header)+ '\n'
				return includeHeader
			}
		} else {
			var includeHeader = '''import ''' + project.getName().toLowerCase + '.connection.ConnectedDevices;' + '\n' +
								'''import ''' + 'java.util.Collection;' + '\n' + 
								'''import ''' + 'java.util.LinkedList;' + '\n' + 
								'''import ''' + 'java.util.List;' + '\n' + 
								
								'''import '''+ 'java.math.BigInteger;' + '\n'+ 
								'''import '''+ 'javax.xml.datatype.XMLGregorianCalendar;' + '\n'+ 
								'''import '''+ 'javax.xml.datatype.Duration;' + '\n'+ 
								'''import '''+ 'javax.xml.namespace.QName;' + '\n'+ 
								'''import '''+ 'javax.xml.datatype.Duration;' + '\n'+ 

								'''import ''' + 'org.eclipse.basyx.vab.exception.provider.ProviderException;' + '\n' + 
								'''import ''' + 'org.eclipse.basyx.vab.protocol.opcua.types.NodeId;' + '\n' + 
								'''import ''' + 'org.eclipse.basyx.submodel.metamodel.api.submodelelement.ISubmodelElement;' + '\n'+
								
								'''import ''' + 'org.eclipse.basyx.submodel.metamodel.api.reference.enums.KeyType;' + '\n' + 
								'''import ''' + 'org.eclipse.basyx.submodel.metamodel.map.reference.Key;' + '\n' + 
								'''import ''' + 'org.eclipse.basyx.submodel.metamodel.map.reference.Reference;' + '\n'+
								'''import ''' + 'org.eclipse.basyx.submodel.metamodel.map.submodelelement.dataelement.property.Property;' + '\n'
								
								
										

			return includeHeader;
		}
	}

	static def constImportStart() '''
		// Manual imports
	'''

	static def constImportEnd() '''
		// End of manual imports
	'''
}
