/*******************************************************************************
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.java.codegen.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.papyrus.designer.languages.java.codegen.Activator;


public class JavaCodeGenPreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	@Override
	public void initializeDefaultPreferences() {
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		store.setDefault(JavaCodeGenConstants.P_JAVA_SUFFIX, "java"); //$NON-NLS-1$
		store.setDefault(JavaCodeGenConstants.P_PROJECT_PREFIX, "org.eclipse.papyrus.javagen."); //$NON-NLS-1$

		// Default value for P_COMMENT_HEADER
		String NL = System.getProperties().getProperty("line.separator"); //$NON-NLS-1$
		String defaultValue =
				"/**\r\n" + //$NON-NLS-1$
			    " * Copyright (c) 2023 CEA LIST and others.\r\n"+ //$NON-NLS-1$
				" *  \r\n" + //$NON-NLS-1$
				" * All rights reserved. This program and the accompanying materials\r\n" + //$NON-NLS-1$
				" * are made available under the terms of the Eclipse Public License 2.0\r\n" +//$NON-NLS-1$
				" * which accompanies this distribution, and is available at\r\n" + //$NON-NLS-1$
				" * http://www.eclipse.org/legal/epl-2.0/\r\n" + //$NON-NLS-1$
				" * \r\n"+ //$NON-NLS-1$
				" *   SPDX-License-Identifier: EPL-2.0\r\n" + //$NON-NLS-1$
				" *  \r\n" +//$NON-NLS-1$
				" *  Contributors:\r\n" +  //$NON-NLS-1$
				" *  	CEA LIST - Initial API and implementation\r\n" + //$NON-NLS-1$
				" */" + NL; //$NON-NLS-1$
						
		store.setDefault(JavaCodeGenConstants.P_COMMENT_HEADER, defaultValue);
	}
}
