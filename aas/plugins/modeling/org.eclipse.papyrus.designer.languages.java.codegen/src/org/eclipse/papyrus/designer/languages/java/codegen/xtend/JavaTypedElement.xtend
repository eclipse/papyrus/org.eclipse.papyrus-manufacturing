/*******************************************************************************
 * Copyright (c) 2019 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.java.codegen.xtend

import org.eclipse.papyrus.designer.languages.common.base.GenUtils
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.ListHint
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.TemplateBinding
import org.eclipse.uml2.uml.MultiplicityElement
import org.eclipse.uml2.uml.TypedElement
import org.eclipse.uml2.uml.util.UMLUtil
import org.eclipse.papyrus.designer.languages.java.codegen.utils.JavaGenUtils
import org.eclipse.uml2.uml.Classifier
import org.eclipse.uml2.uml.Parameter
import org.eclipse.uml2.uml.Property
import org.eclipse.papyrus.designer.languages.java.codegen.Activator

/**
 * Produce the string for a typed element, taking the ListHint and TemplateBinding stereotype
 * into account
 */
class JavaTypedElement {
	static final String TYPE_NAME_TAG = "[typeName]"
	
	String string
	
	

	/**
	 * Handle list types
	 * @param propertyOrParameter
	 *            a property or parameter (in both cases a typed element)
	 * @return a string representation of the type of a property or parameter. The function takes
	 *         list hints into account in case of non 1 multiplicity
	 */
	static def String javaType(TypedElement propertyOrParameter) {
		var String type = null
		if (propertyOrParameter === null || propertyOrParameter.type === null || propertyOrParameter.type.qualifiedName === null) {
			if (propertyOrParameter.name.contains("::")&& propertyOrParameter.name.length>5 )
			{
			if (!propertyOrParameter.name.contains("."))
			{
				return propertyOrParameter.name.substring(5);
			} else
			{
				return JavaGenUtils.getXMLTypes(propertyOrParameter);		
			}
			} else 
			{
			return JavaGenUtils.getXMLTypes(propertyOrParameter);	
			}
			}
			
			Activator.log.debug("javaType: propertpropertyOrParameter is null")
				
		
		// default: normal string representation of Java type
		var Classifier ns = null;
		if (propertyOrParameter instanceof Parameter) {
			ns = propertyOrParameter.operation.class_
		}
		else if (propertyOrParameter instanceof Property) {
			ns = propertyOrParameter.class_
		}

		var String defaultType = JavaGenUtils.javaQualifiedName(propertyOrParameter.type, ns)
		val binding =
				UMLUtil.getStereotypeApplication(propertyOrParameter, TemplateBinding)
		if (binding !== null) {
			defaultType += '''<«FOR actual : binding.getActuals() SEPARATOR ','»«actual.qualifiedName»«ENDFOR»>'''
		}

		// remove "." if any
		if (defaultType!==null && defaultType.contains(".")){
			defaultType= JavaGenUtils.removedot(defaultType)
			
		}
	
	
	
		// calculate type from stereotype definition
		val listHint = GenUtils.getApplicationTree(propertyOrParameter, ListHint)
		val lower = (propertyOrParameter as MultiplicityElement).getLower()
		val upper = (propertyOrParameter as MultiplicityElement).getUpper()
	
		if (listHint !== null) {
			if (upper == -1) {
				type = listHint.getVariable()
			} else if (upper > 1) {
				if (upper == lower) {
					type = listHint.getFixed()
				} else {
					type = listHint.getBounded()
				}
			}
		}
		if (type === null || type.length == 0) {
			// no or incomplete list hint
			// Java does not support fixed size array references such as int[n],
			// these must be given in the constructor (e.g. new int[5]) => always
			//  use non-specified array
			if (upper != 1) {
				type = '''«TYPE_NAME_TAG»[]'''
			}
		}
		if (type !== null) {
			return type.replace(TYPE_NAME_TAG, defaultType)
		} else {
			return defaultType
		}
	}
	
	static def String javaSecType(TypedElement propertyOrParameter) {
		var String type = null
		if (propertyOrParameter === null || propertyOrParameter.type === null || propertyOrParameter.type.qualifiedName === null) {
			Activator.log.debug("javaType: propertpropertyOrParameter is null")
			return "undef";
		}
		// default: normal string representation of Java type
		var Classifier ns = null;
		if (propertyOrParameter instanceof Parameter) {
			ns = propertyOrParameter.operation.class_
		}
		else if (propertyOrParameter instanceof Property) {
			ns = propertyOrParameter.class_
		}

		var String defaultType = JavaGenUtils.javaQualifiedName(propertyOrParameter.type, ns)
		val binding =
				UMLUtil.getStereotypeApplication(propertyOrParameter, TemplateBinding)
		if (binding !== null) {
			defaultType += '''<«FOR actual : binding.getActuals() SEPARATOR ','»«actual.qualifiedName»«ENDFOR»>'''
		}

		return "Collection<ISubmodelElement>";
	}
}
