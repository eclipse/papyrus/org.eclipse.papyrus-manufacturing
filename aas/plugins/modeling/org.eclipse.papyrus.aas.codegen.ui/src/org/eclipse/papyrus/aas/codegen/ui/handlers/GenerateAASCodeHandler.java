/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ** Contributors:
 *	Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr - Initial API and implementation
 *  Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.aas.codegen.ui.handlers;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.regex.Pattern;

import org.apache.maven.model.Model;
import org.eclipse.aas.basyx.codegen.generator.Project;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.m2e.core.MavenPlugin;
import org.eclipse.m2e.core.project.IProjectConfigurationManager;
import org.eclipse.m2e.core.project.MavenProjectInfo;
import org.eclipse.m2e.core.project.ProjectImportConfiguration;
import org.eclipse.papyrus.aas.AssetAdministrationShell;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.ILangCodegen;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.LanguageCodegen;

import org.eclipse.papyrus.uml.diagram.common.handlers.CmdHandler;
import org.eclipse.ui.PlatformUI;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.osgi.framework.Bundle;


// TODO: Auto-generated Javadoc
/**
 * The Class GenerateAASCodeHandler.
 */
public class GenerateAASCodeHandler extends CmdHandler {

	/**
	 * Execute.
	 *
	 * @param event the event
	 * @return the object
	 * @throws ExecutionException the execution exception
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		updateSelectedEObject();
		
		IRunnableWithProgress op = new IRunnableWithProgress() {

			@Override
			public void run(IProgressMonitor monitor) {
				
				monitor.beginTask("Generating Basyx Code...", IProgressMonitor.UNKNOWN);
				try {
					executeCodeGen(monitor);
				} catch (Exception e) {
					e.printStackTrace();
					MessageDialog.openError(GenerateAASCodeHelper.getShell(), "Error",
							"Error During Code generation"+(e.getMessage()!=null ?e.getMessage(): e.toString()));
				}
				monitor.done();
			}
		};
		
		try {
			new ProgressMonitorDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell()).run(true, true,
					op);
			MessageDialog.openInformation(GenerateAASCodeHelper.getShell(), "Success",
					"The project " + ((Class) selectedEObject).getName()
							+ " has been successfully generated and imported into your workspace");
			

		} catch (InvocationTargetException e) {
			
			e.printStackTrace();
			MessageDialog.openError(GenerateAASCodeHelper.getShell(), "Error",
					(e.getMessage()!=null ?e.getMessage(): e.toString()));

		} catch (InterruptedException e) {
			
			e.printStackTrace();
			MessageDialog.openError(GenerateAASCodeHelper.getShell(), "Error",
					(e.getMessage()!=null ?e.getMessage(): e.toString()));

		} catch (Exception e) {
			
			e.printStackTrace();
			MessageDialog.openError(GenerateAASCodeHelper.getShell(), "Error",
					(e.getMessage()!=null ?e.getMessage(): e.toString()));

		}
		
	

		return null;
	}
	
	
	/**
	 * Execute basyx code generator.
	 *
	 * @param monitor the monitor
	 * @return null if there are problems
	 * @throws Exception the exception
	 */
	private Object executeCodeGen(IProgressMonitor monitor) throws Exception {
		
		Class selectedClass = (Class) selectedEObject;
		AssetAdministrationShell aas = UMLUtil.getStereotypeApplication(selectedClass, AssetAdministrationShell.class);
		if (aas != null) {

			URI uri = selectedClass.eResource().getURI();

			IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
			if (uri.segmentCount() < 2) {
				return null;
			}
			String projectLocation = uri.segment(1);
			try {
				projectLocation = URLDecoder.decode(uri.segment(1), "UTF-8");
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// System.err.println("projectLocation "+ projectLocation);
			IProject modelProject = root.getProject(projectLocation);
			if (modelProject.exists()) {
				
				
				//get the Basyx AssetAdministration from the UML Class
				org.eclipse.aas.api.aas.AssetAdministrationShell basyxAAS = GenerateAASCodeHelper.getAAS(selectedClass);
				
				// Initialising the Generator.
				String projectName = (selectedClass.getName()!=null) ? selectedClass.getName() : "BaSyxAAS";
		         Project aasproject = new Project(basyxAAS);
		         aasproject.setProjectName(projectName);
		         aasproject.setNamespaceFromProjectName();
		         aasproject.setProjectFolder(root.getLocation().toString() + "/" + projectName + "/");

				// Create the runnable project
				aasproject.createProject();

				
				
				
				
				// Import and open the project in the workspace

				IProjectDescription description;
				try {

					description = ResourcesPlugin.getWorkspace().loadProjectDescription(
							new Path(root.getLocation().toString() + "/" + selectedClass.getName() + "/.project"));

					File pomFile = new File(ResourcesPlugin.getWorkspace().getRoot().getLocation().toString() + "/"
							+ selectedClass.getName() + "/pom.xml");
					importMavenProjects(monitor, pomFile, description.getName(), "aas", "aasmodule", "0.1");
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				
				/////////////////////
				// generate the Class for the Submodel with the JDT Integration 
				/////////////////////
				Pattern java = Pattern.compile("Java"); //$NON-NLS-1$
				final ILangCodegen javaCodeGen =
						LanguageCodegen.getGenerator(java, null);
				IProject generatedProject = root.getProject(projectName);
				
				if (GenerateAASCodeHelper.getUMLSubmodels(selectedClass)!=null && !GenerateAASCodeHelper.getUMLSubmodels(selectedClass).isEmpty() ) {
					for(Class submodelClass: GenerateAASCodeHelper.getUMLSubmodels(selectedClass)) {
						generate(javaCodeGen, generatedProject, submodelClass, new BasicEList<PackageableElement>(), false);
					}
				}
				
				
				
			
		}
		}
		return null;
	}

	

	/**
	 * Generate.
	 *
	 * @param codeGen the code gen
	 * @param genProject the gen project
	 * @param pe the pe
	 * @param alreadyHandled the already handled
	 * @param recurse the recurse
	 */
	public void generate(ILangCodegen codeGen, IProject genProject, PackageableElement pe, EList<PackageableElement> alreadyHandled, boolean recurse) {
		alreadyHandled.add(pe);
		
		codeGen.generateCode(genProject, pe, null);



	}
	
	
	/**
	 * Import maven projects.
	 *
	 * @param monitor the monitor
	 * @param pomFile the pom file
	 * @param projectName the project name
	 * @param groupId the group id
	 * @param artifactId the artifact id
	 * @param version the version
	 * @throws CoreException the core exception
	 */
	private void importMavenProjects(IProgressMonitor monitor, File pomFile, String projectName, String groupId,
			String artifactId, String version) throws CoreException {

		IProjectConfigurationManager manager = MavenPlugin.getProjectConfigurationManager();
		ProjectImportConfiguration config = new ProjectImportConfiguration();
		Collection<MavenProjectInfo> infos = new ArrayList<>();
		Model model = new Model();
		model.setGroupId(groupId);
		model.setArtifactId(artifactId);
		model.setVersion(version);

		model.setPomFile(pomFile);
		MavenProjectInfo info = new MavenProjectInfo(projectName, pomFile, model, null);
		infos.add(info);

		manager.importProjects(infos, config, monitor);
	}
	
	
	

}
