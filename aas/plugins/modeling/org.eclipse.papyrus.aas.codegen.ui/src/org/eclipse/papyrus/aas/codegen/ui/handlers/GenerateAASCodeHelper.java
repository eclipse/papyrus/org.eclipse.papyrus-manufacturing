/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *  Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr - Initial API and implementation
 *  
 *****************************************************************************/
package org.eclipse.papyrus.aas.codegen.ui.handlers;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;

import org.eclipse.aas.api.aas.parts.ConceptDictionary;
import org.eclipse.aas.api.communications.Endpoint;
import org.eclipse.aas.api.communications.ProtocolKind;
import org.eclipse.aas.api.reference.Key;
import org.eclipse.aas.api.reference.Reference;
import org.eclipse.aas.api.submodel.SubModel;
import org.eclipse.aas.api.submodel.submodelelement.Operation;
import org.eclipse.aas.api.submodel.submodelelement.SubModelElementCollection;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.File;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.OperationVariable;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.Property;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.valuetypes.ValueType;
import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.papyrus.aas.AASEndpoint;
import org.eclipse.papyrus.aas.Asset;
import org.eclipse.papyrus.aas.AssetAdministrationShell;
import org.eclipse.papyrus.aas.ConceptDescription;
import org.eclipse.papyrus.aas.IdType;
import org.eclipse.papyrus.aas.LangString;
import org.eclipse.papyrus.aas.MultiLanguageProperty;
import org.eclipse.papyrus.aas.NodeId;
import org.eclipse.papyrus.aas.SubmodelElement;
import org.eclipse.papyrus.aas.impl.AssetAdministrationShellImpl;
import org.eclipse.papyrus.aas.impl.MultiLanguagePropertyImpl;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.ValueSpecification;
import org.eclipse.uml2.uml.util.UMLUtil;

import io.adminshell.aas.v3.model.Identifiable;
import io.adminshell.aas.v3.model.Identifier;
import io.adminshell.aas.v3.model.IdentifierType;
import io.adminshell.aas.v3.model.KeyElements;
import io.adminshell.aas.v3.model.KeyType;
import io.adminshell.aas.v3.model.ModelingKind;
import io.adminshell.aas.v3.model.impl.DefaultConceptDescription;
import io.adminshell.aas.v3.model.impl.DefaultIdentifier;

// TODO: Auto-generated Javadoc
/**
 * The Class GenerateAASCodeHelper.
 */
public class GenerateAASCodeHelper {

	/**
	 * Generate AAS.
	 *
	 * @param aasClass the aas class
	 * @return the aas
	 * @throws Exception the exception
	 */
	public static org.eclipse.aas.api.aas.AssetAdministrationShell getAAS(Class aasClass) throws Exception {
		AssetAdministrationShell aas = UMLUtil.getStereotypeApplication(aasClass, AssetAdministrationShell.class);
		if (aas != null) {

			org.eclipse.aas.api.aas.AssetAdministrationShell basyxAAS = new org.eclipse.aas.api.aas.AssetAdministrationShell(
					(aas.getIdShort() != null) ? aas.getIdShort() : "aas");

			// setting the endpoint
			AASEndpoint endpoint = aas.getEndpoint();
			org.eclipse.aas.api.communications.AASEndpoint aasEndpoint = new org.eclipse.aas.api.communications.AASEndpoint("AASServer", 1503);
			//Wait for Tapanta fix to create the AAS Endpoint
			if (endpoint != null) {
				aasEndpoint = new org.eclipse.aas.api.communications.AASEndpoint(endpoint.getAddress(), endpoint.getPort());
			}
			basyxAAS.setEndpoint(aasEndpoint);

			// to do: manage the port attribute

			// setting the submodels
			List<SubModel> basyxSubmodels = new ArrayList<SubModel>();
			basyxSubmodels = getSubmodels(aasClass);
			try {
				if (basyxSubmodels != null && !basyxSubmodels.isEmpty()) {
					basyxAAS.setSubModels(basyxSubmodels);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			// setting the identification
			generateIdentification(aas, basyxAAS);

			// Setting the AAS for the Asset.
			org.eclipse.aas.api.asset.Asset basyxAsset = new org.eclipse.aas.api.asset.Asset("basyxAsset");
			basyxAsset = getAsset(aasClass);
			try {
				if (basyxAsset != null)
					basyxAAS.setAsset(basyxAsset);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				MessageDialog.openError(getShell(), "Error",
						"You have to set an asset for the AAS before code generation");
			}

			// setting the concept dictionary
			org.eclipse.aas.api.aas.parts.ConceptDictionary conceptDic = GenerateAASCodeHelper
					.getConceptDictionary(aasClass);
			basyxAAS.setConceptDictionary(conceptDic);

			return basyxAAS;

		}

		return null;

	}

	/**
	 * Gets the asset endpoints.
	 *
	 * @param aasClass the aas
	 * @return the asset endpoints
	 */
	public static List<Endpoint> getAssetEndpoints(Class aasClass) {

		// create a list of java endpoints
		List<Endpoint> endpoints = new ArrayList<Endpoint>();
		AssetAdministrationShell aas = UMLUtil.getStereotypeApplication(aasClass, AssetAdministrationShell.class);
		if (aas != null) {
			if (aas.getAsset() != null) {
				Asset asset = ((AssetAdministrationShellImpl) aasClass).getAsset();
				// System.err.println(asset.getBase_Class().getName());

				for (org.eclipse.papyrus.aas.Endpoint endpoint : asset.getEndpoint()) {

					// to do: manage the port attribute
					Endpoint assetEndpoint = new Endpoint(endpoint.getName(), convertProtocol(endpoint.getProtocol()),
							endpoint.getAddress());
					endpoints.add(assetEndpoint);

				}
			}
		}

		return endpoints;

	}

	/**
	 * Gets the asset.
	 *
	 * @param aasClass the aas class
	 * @return the asset
	 * @throws Exception the exception
	 */
	public static org.eclipse.aas.api.asset.Asset getAsset(Class aasClass) throws Exception {

		AssetAdministrationShell aas = UMLUtil.getStereotypeApplication(aasClass, AssetAdministrationShell.class);
		if (aas != null) {
			if (aas.getAsset() != null) {
				Asset asset = ((AssetAdministrationShellImpl) aas).getAsset();

				// setting the asset kind
				org.eclipse.aas.api.asset.Asset basyxAsset = new org.eclipse.aas.api.asset.Asset(asset.getIdShort());
				if (asset.getKind() != null) {
					basyxAsset.setKind(convertAssetKind(asset.getKind()));
				}

				// setting the identification
				generateIdentification(asset, basyxAsset);

				// setting the endpoints
				if (asset.getEndpoint() != null && !asset.getEndpoint().isEmpty()) {

					for (org.eclipse.papyrus.aas.Endpoint endpoint : asset.getEndpoint()) {

						// to do: manage the port attribute
						Endpoint assetEndpoint = new Endpoint(endpoint.getName(),
								convertProtocol(endpoint.getProtocol()), endpoint.getAddress());
						basyxAsset.setEndpoint(assetEndpoint);

					}

				}

				return basyxAsset;
			} else {
				// since it is forbidden to create an AAS without an asset, throw an Exception
				throw new Exception("Set an asset for the AAS before code generation");
			}

		}
		throw new Exception("Code generator has to be  executed on an AAS!");

	}

	/**
	 * Gets the submodels.
	 *
	 * @param aasClass the aas class
	 * @return the submodels
	 * @throws Exception the exception
	 */
	public static List<SubModel> getSubmodels(Class aasClass) throws Exception {

		AssetAdministrationShell aas = UMLUtil.getStereotypeApplication(aasClass, AssetAdministrationShell.class);
		if (aas != null) {
			// create a list of java submodels
			List<SubModel> submodels = new ArrayList<SubModel>();

			if (aas.getSubmodel() != null)

				for (org.eclipse.papyrus.aas.Submodel submodel : aas.getSubmodel()) {
					Class submodelClass = submodel.getBase_Class();
					if (submodelClass != null) {
						String submodelName = submodelClass.getName();
						SubModel basyxSubmodel = new SubModel(submodelName);

						// setting submodel IdType and Identifier
						generateIdentification(submodel, basyxSubmodel);

						
						// setting the semanticId of the Submodel
						if (submodel.getSemanticId() != null) {
							org.eclipse.papyrus.aas.Reference ref = submodel.getSemanticId();
							Reference basyxRef = convertReference(ref);
							if (basyxRef != null)
								basyxSubmodel.setSemanticIdentifier(basyxRef);
						}
						

						// setting submodel Kind
						if (submodel.getKind() != null) {

							basyxSubmodel.setKind(convertModelingKind(submodel.getKind()));
						}

						// Adding submodelElements (properties, Files, MLPs) from class attributes
						generateSubmodelElementsFromClassAttributes(submodelClass, basyxSubmodel, null);

						// Adding Operations to the submodel

						generateSubmodelOperations(submodelClass, basyxSubmodel, null);

						// Adding SubmodelElementCollection
						// generate submodelElementCollections from nested classifiers
						if (submodelClass.getNestedClassifiers() != null
								&& submodelClass.getNestedClassifiers().size() > 0) {
							for (Classifier sec : submodelClass.getNestedClassifiers()) {
								if (sec instanceof Class)
									generateSubmodelElementCollection((Class) sec, basyxSubmodel, null);
							}

						}

						// adding the submodel to the list of submodels
						submodels.add(basyxSubmodel);

					}
				}

			return submodels;

		}

		return null;

	}

	/**
	 * Gets the UML submodels.
	 *
	 * @param aasClass the aas class
	 * @return the UML submodels
	 */
	public static List<Class> getUMLSubmodels(Class aasClass) {

		AssetAdministrationShell aas = UMLUtil.getStereotypeApplication(aasClass, AssetAdministrationShell.class);
		List<Class> umlSubmodels = new ArrayList<Class>();
		if (aas != null) {

			for (org.eclipse.papyrus.aas.Submodel submodel : aas.getSubmodel()) {
				Class submodelClass = submodel.getBase_Class();
				if (submodelClass != null) {
					umlSubmodels.add(submodelClass);
				}
			}
		}
		return umlSubmodels;

	}

	/**
	 * Generate concept descriptions.
	 *
	 * @param aasClass the aas class
	 */
	@SuppressWarnings("deprecation")
	public static org.eclipse.aas.api.aas.parts.ConceptDictionary getConceptDictionary(Class aasClass) {

		/////////////////////////////////////////////////////
		// Parsing UML AAS Model and collecting concept descriptions
		/////////////////////////////////////////////////////

		org.eclipse.uml2.uml.Model rootModel = aasClass.getModel();

		List<org.eclipse.uml2.uml.Package> modelPackages = new ArrayList<>();
		modelPackages = rootModel.getNestedPackages();

		// Initializing a Basyx ConceptDictionary
		ConceptDictionary conceptDict = new ConceptDictionary();

		if (modelPackages != null) {
			for (org.eclipse.uml2.uml.Package pk : modelPackages) {

				//if (UMLUtil.getStereotypeApplication(pk, org.eclipse.papyrus.aas.ConceptDescription.class) != null) {
					if (pk.getOwnedElements() != null) {
						for (Element child : pk.getOwnedElements()) {

							org.eclipse.papyrus.aas.ConceptDescription stConceptDescription = UMLUtil
									.getStereotypeApplication(child, org.eclipse.papyrus.aas.ConceptDescription.class);

							if (stConceptDescription != null) {
								// void
								// org.eclipse.aas.basyx.codegen.util.ConceptDescriptionCollector.addConceptDesc(String
								// conceptName, String idType, String id, String category)
								String conceptName = ((org.eclipse.uml2.uml.NamedElement) child).getName();

								String conceptIdShort = "";
								// setting the idshort
								if (stConceptDescription.getIdShort() != null
										&& !stConceptDescription.getIdShort().isEmpty()) {
									conceptIdShort = stConceptDescription.getIdShort();
								} else {
									conceptIdShort = conceptName.replaceAll("\\s+", "");

								}
								org.eclipse.aas.api.submodel.parts.ConceptDescription basyxCD = new org.eclipse.aas.api.submodel.parts.ConceptDescription(
										conceptIdShort);

								// setting the identification
								generateIdentification(stConceptDescription, basyxCD);

								if (stConceptDescription.getCategory() != null) {
									basyxCD.setCategory(stConceptDescription.getCategory());
								}

								// setting isCaseOf list of references

								if (stConceptDescription.getIsCaseOf() != null) {
									List<org.eclipse.aas.api.reference.Reference> iscaseOfList = new ArrayList<org.eclipse.aas.api.reference.Reference>();
									for (org.eclipse.papyrus.aas.Reference ref : stConceptDescription.getIsCaseOf()) {

										org.eclipse.aas.api.reference.Reference basyxRef = convertReference(ref);

										if (basyxRef != null) {

											iscaseOfList.add(basyxRef);
										}

									}

									for (org.eclipse.aas.api.reference.Reference ref : iscaseOfList) {
										basyxCD.setIsCaseOf(ref);

									}

								}

								conceptDict.setConceptDescription(basyxCD);

							}
						}

					//}

				}
			}

		}
		return conceptDict;
	}

	/**
	 * Generate submodel element collection from a UML nestedClassifier.
	 *
	 * @param umlClass      the UML nestedClassifier that correspond to the SEC
	 * @param basyxSubmodel the basyx submodel to which the basyx SEC will be added
	 * @param parentSEC     the parent SEC
	 */
	public static void generateSubmodelElementCollection(Class umlClass, SubModel basyxSubmodel,
			SubModelElementCollection parentSEC) {
		org.eclipse.papyrus.aas.SubmodelElementCollection stSEC = UMLUtil.getStereotypeApplication(umlClass,
				org.eclipse.papyrus.aas.SubmodelElementCollection.class);
		if (stSEC != null) {

			SubModelElementCollection basyxSEC = new SubModelElementCollection(umlClass.getName());
			basyxSEC.setAllowDuplicates(stSEC.isAllowDuplicates());
			basyxSEC.setOrdered(stSEC.isOrdered());
			basyxSEC.setDynamic(stSEC.isDynamic());

			// setting the idshort, descriptions, kind, displayName, category

			generateSubmodelElementAttributes(stSEC, umlClass, basyxSEC);

			// setting the semanticId of the File
			if (stSEC.getSemanticId() != null) {
				org.eclipse.papyrus.aas.Reference ref = stSEC.getSemanticId();
				Reference basyxRef = convertReference(ref);
				if (basyxRef != null)
					basyxSEC.setSemanticIdentifier(basyxRef);
			}

			// Adding submodelElements (properties, Files, MLPs) to the basyxSEC from class
			// attributes
			generateSubmodelElementsFromClassAttributes(umlClass, null, basyxSEC);

			// generate operations of the submodelelementcollection
			generateSubmodelOperations(umlClass, null, basyxSEC);

			// generate nested SEC
			for (Classifier secChild : umlClass.getNestedClassifiers()) {
				if (secChild instanceof Class) {// nested SEC are UML nested classifiers

					// call generateSubmodelElementCollection to generate the content of the
					// SEC-child and add it to the
					generateSubmodelElementCollection((Class) secChild, null, basyxSEC);

				}
			}

			// add SEC to basyxSubmodel
			if (basyxSubmodel != null)
				basyxSubmodel.setSubModelElementCollection(basyxSEC);

			// add SEC to parentSEC
			if (parentSEC != null)
				parentSEC.setSubModelElementCollection(basyxSEC);

		}

	}

	/**
	 * Adds the submodelElements (properties, Files, MLPs) from UML class
	 * attributes.
	 *
	 * @param submodelClass the submodel class
	 * @param basyxSubmodel Adds the submodelElements to the basyx submodel if
	 *                      basyxSubmodel!=null
	 * @param basyxSEC      Adds the submodelElements to the basyx SEC if
	 *                      basyxSEC!=null
	 * 
	 */
	public static void generateSubmodelElementsFromClassAttributes(Class submodelClass,
			org.eclipse.aas.api.submodel.SubModel basyxSubmodel, SubModelElementCollection basyxSEC) {

		if (submodelClass.getAllAttributes() != null && !submodelClass.getAllAttributes().isEmpty()) {

			// generate submodelElements that corresponds to UML Class attributes
			for (int j = 0; j < submodelClass.getAllAttributes().size(); j++) {
				org.eclipse.uml2.uml.Property submodelElement = submodelClass.getAllAttributes().get(j);

				String attName = submodelElement.getName();

				// Generate a submodel property if there is no stereotype applied on the
				// property
				if ((submodelElement.getAppliedStereotypes() == null)
						|| (submodelElement.getAppliedStereotypes().size() == 0)) {

					Property submodelProperty = new Property(attName);

					// set the type of the property
					if (submodelElement.getType() != null && submodelElement.getType().getName() != null) {
						submodelProperty.setValueType(convertType(submodelElement.getType().toString()));
					}

					// set the default value
					if (submodelElement.getDefaultValue() != null) {

						String attValue = submodelElement.getDefaultValue().stringValue();
						if(attValue!=null)
						submodelProperty.setValue(attValue);

					}

					if (basyxSubmodel != null)
						basyxSubmodel.setProperty(submodelProperty);

					if (basyxSEC != null)
						basyxSEC.setProperty(submodelProperty);

				} else {

					// add submodelElement property
					generateProperty(submodelElement, basyxSubmodel, basyxSEC);
					// add submodelElement File
					generateFile(submodelElement, basyxSubmodel, basyxSEC);
					// add multiLanguageProperties
					generateMLP(submodelElement, basyxSubmodel, basyxSEC);
				}

			}
		}

	}

	/**
	 * Generate property.
	 *
	 * @param submodelElement the submodel element
	 * @param basyxSubmodel   the submodel containing the submodelElement
	 * @param basyxSEC        the basyx SEC
	 */
	public static void generateProperty(org.eclipse.uml2.uml.Property submodelElement, SubModel basyxSubmodel,
			SubModelElementCollection basyxSEC) {

		org.eclipse.papyrus.aas.Property stProperty = UMLUtil.getStereotypeApplication(submodelElement,
				org.eclipse.papyrus.aas.Property.class);

		if (stProperty != null) {
			Property submodelProperty = new Property();

			// setting the idshort, descriptions, kind, displayName, category
			// comment this method call, waiting for DFKI API fix
			generateSubmodelElementAttributes(stProperty, submodelElement, submodelProperty);

			// set the semanticId of the property
			if (stProperty.getSemanticId() != null) {
				org.eclipse.papyrus.aas.Reference ref = stProperty.getSemanticId();
				Reference basyxRef = convertReference(ref);
				if (basyxRef != null)
					submodelProperty.setSemanticIdentifier(basyxRef);
			}

			// set the type of the property
			if (submodelElement.getType() != null) {
				submodelProperty.setValueType(convertType(submodelElement.getType().getName()));
			}

			// setting the attribute isDynamic for properties, files and SECs
			submodelProperty.setDynamic(stProperty.isDynamic());

			// setting the NodeId
			Boolean nodeidGenerated = false;
			NodeId nodeid = stProperty.getNodeId();
			if (nodeid != null) {

				submodelProperty.setNameSpaceIndex(nodeid.getNameSpaceIndex());

				generateNodeId(nodeid, submodelProperty, null);
				nodeidGenerated = true;
			}

			// setting the endpoint
			org.eclipse.papyrus.aas.Endpoint endpoint = stProperty.getEndPoint();
			// Endpoint propEndpoint = new Endpoint("OPCServer", ProtocolKind.OPCUA,
			// "opc.tcp://server", 48010);
			Boolean endpointGenerated = false;
			if (endpoint != null) {
				Endpoint propEndpoint = new Endpoint(endpoint.getName(), convertProtocol(endpoint.getProtocol()),
						endpoint.getAddress());
				endpointGenerated = true;
				submodelProperty.setEndpoint(propEndpoint);
			}

			// setting the default value
			// If isDynamic is true, but no endpoint is defined -> generate the default
			// valueSpecification
			// isDynamic is true and endpoint is defined -> we don’t generate the default
			// valueSpecification because the value will be fetched from the asset

			if (submodelElement.getDefaultValue() != null) {
				if (stProperty.isDynamic() == false) {

					submodelProperty.setValue(getPropertyValue(submodelElement));
				} else
				// verify that endpoint is not defined
				if (endpointGenerated == false) {
					submodelProperty.setValue(getPropertyValue(submodelElement));
				}

			}

			if (basyxSubmodel != null)
				basyxSubmodel.setProperty(submodelProperty);

			if (basyxSEC != null) {
				// Modifying the idshort to contain the namespace. Otherwise, two properties
				// belonging to diffrent SEC will be generated with the same idshort in the java
				// class, this will cause compilation errors

				basyxSEC.setProperty(submodelProperty);
			}

		}

	}

	/**
	 * Generate file submodelElement.
	 *
	 * @param submodelElement the submodel element
	 * @param basyxSubmodel   the basyx submodel
	 * @param basyxSEC        the basyx SEC
	 */
	public static void generateFile(org.eclipse.uml2.uml.Property submodelElement, SubModel basyxSubmodel,
			SubModelElementCollection basyxSEC) {
		// TODO Auto-generated method stub
		org.eclipse.papyrus.aas.File stFile = UMLUtil.getStereotypeApplication(submodelElement,
				org.eclipse.papyrus.aas.File.class);

		if (stFile != null) {
			File submodelFile = new File();

			// setting the idshort, descriptions, kind, displayName, category

			generateSubmodelElementAttributes(stFile, submodelElement, submodelFile);

			// setting the attribute isDynamic for properties, files and SECs
			submodelFile.setDynamic(stFile.isDynamic());

			// setting the semanticId of the File
			if (stFile.getSemanticId() != null) {
				org.eclipse.papyrus.aas.Reference ref = stFile.getSemanticId();
				Reference basyxRef = convertReference(ref);
				if (basyxRef != null)
					submodelFile.setSemanticIdentifier(basyxRef);
			}

			// setting nodeid for properties and files

			NodeId nodeid = stFile.getNodeId();
			if (nodeid != null) {

				submodelFile.setNameSpaceIndex(nodeid.getNameSpaceIndex());

				generateNodeId(nodeid, null, submodelFile);
			}

			// setting the endpoint
			org.eclipse.papyrus.aas.Endpoint endpoint = stFile.getEndPoint();

			if (endpoint != null) {
				Endpoint propEndpoint = new Endpoint(endpoint.getName(), convertProtocol(endpoint.getProtocol()),
						endpoint.getAddress());

				submodelFile.setEndpoint(propEndpoint);
			}

			// setting the mime type

			if (stFile.getMimeType() != null) {
				submodelFile.setMimeType(convertMimeType(stFile.getMimeType()));
			}

			// setting the path
			if (stFile.getPath() != null) {
				submodelFile.setValue(stFile.getPath());
			}

			// Adding a File to the Submodel
			if (basyxSubmodel != null)
				basyxSubmodel.setFile(submodelFile);

			// Adding a File to the SEC
			if (basyxSEC != null)
				basyxSEC.setFile(submodelFile);

		}

	}

	/**
	 * Generate MLP submodelElement.
	 *
	 * @param submodelElement the submodel element
	 * @param basyxSubmodel   the basyx submodel
	 * @param basyxSEC        the basyx SEC
	 */
	public static void generateMLP(org.eclipse.uml2.uml.Property submodelElement, SubModel basyxSubmodel,
			SubModelElementCollection basyxSEC) {

		MultiLanguageProperty stMLP = UMLUtil.getStereotypeApplication(submodelElement, MultiLanguageProperty.class);

		String[] langCodes = new String[0];
		String[] langPropValues = new String[0];
		if (stMLP != null) {
			// value:MultiLang*
			// valueid:Reference

			EList<LangString> values = ((MultiLanguagePropertyImpl) stMLP).getValue();
			if (values != null && values.size() > 0) {
				langPropValues = new String[values.size()];
				langCodes = new String[values.size()];
				for (int i = 0; i < values.size(); i++) {
					langPropValues[i] = values.get(i).getValue();
					langCodes[i] = values.get(i).getLang().toString();
				}
			}

			// basyxSubmodel.addMultiLangProperty(multiLangPropName, langCodes,
			// langPropValues);

			org.eclipse.aas.api.submodel.submodelelement.dataelement.MultiLanguageProperty submodelMLP = new org.eclipse.aas.api.submodel.submodelelement.dataelement.MultiLanguageProperty();

			// setting the idshort, descriptions, kind, displayName, category
			// comment this method call waiting for DFKI API fix
			generateSubmodelElementAttributes(stMLP, submodelElement, submodelMLP);

			// setting the semanticId of the MLP
			if (stMLP.getSemanticId() != null) {
				org.eclipse.papyrus.aas.Reference ref = stMLP.getSemanticId();
				Reference basyxRef = convertReference(ref);
				if (basyxRef != null)
					submodelMLP.setSemanticIdentifier(basyxRef);
			}

			// setting the attribute isDynamic for properties, files and SECs
			// Not yet supported by Basyx codegen API
			// submodelMLP.setDynamic(stMLP.isDynamic());

			// setting the values
			if (stMLP.getValue() != null && !stMLP.getValue().isEmpty()) {

				for (LangString desc : stMLP.getValue()) {

					submodelMLP.setValue((desc.getLang() != null) ? desc.getLang().getLiteral() : "en",
							desc.getValue());

				}

			}

			// Adding MLP to the Submodel
			if (basyxSubmodel != null)
				basyxSubmodel.setMultiLanguageProperty(submodelMLP);

			// Adding MLP to the SEC
			if (basyxSEC != null)
				basyxSEC.setMultiLanguageProperty(submodelMLP);

		}

	}

	/**
	 * Generate submodel element common attributes.
	 *
	 * @param <T1>               the generic type
	 * @param <T2>               the generic type
	 * @param stSubmodelElement  the st submodel element
	 * @param umlSubmodelElement the uml submodel element
	 * @param submodelElement    the submodel element
	 */
	public static <T1 extends SubmodelElement, T2 extends io.adminshell.aas.v3.model.SubmodelElement> void generateSubmodelElementAttributes(
			T1 stSubmodelElement, NamedElement umlSubmodelElement, T2 submodelElement) {
		// setting the idshort of the submodelElement
		if (stSubmodelElement.getIdShort() != null) {
			submodelElement.setIdShort(stSubmodelElement.getIdShort());
		} else {
			String idShort = "setIdShort";
			if (umlSubmodelElement.getName() != null)
				idShort = umlSubmodelElement.getName();
			submodelElement.setIdShort(idShort.replaceAll("\\s+", ""));

		}

		// setting the descriptions of the submodelElement
		if (stSubmodelElement.getDescription() != null && !stSubmodelElement.getDescription().isEmpty()) {

			submodelElement.setDescriptions(convertDescriptions(stSubmodelElement.getDescription()));
		}

		// setting the kind of the submodelElement
		if (stSubmodelElement.getKind() != null) {
			submodelElement.setKind(convertModelingKind(stSubmodelElement.getKind()));
		}

		// setting the display Name
		if (umlSubmodelElement.getName() != null) {
			List<io.adminshell.aas.v3.model.LangString> dNames = new ArrayList<io.adminshell.aas.v3.model.LangString>();

			io.adminshell.aas.v3.model.LangString dName = new io.adminshell.aas.v3.model.LangString(
					umlSubmodelElement.getName(), "en");
			dNames.add(dName);

			submodelElement.setDisplayNames(dNames);

		}

		// setting the category
		if (stSubmodelElement.getCategory() != null)
			submodelElement.setCategory(stSubmodelElement.getCategory());

		// To do: ask Tapanta to use the Reference from Fraunhofer API
		// setting the semanticId of the submodelElement
//		if (stSubmodelElement.getSemanticId() != null) {
//			org.eclipse.papyrus.aas.Reference ref = stSubmodelElement.getSemanticId();
//			Reference basyxRef=convertReference(ref);
//			if(basyxRef!=null)
//				submodelElement.setSemanticId((io.adminshell.aas.v3.model.Reference) basyxRef);
//		}

	}

	/**
	 * Generate identification.
	 *
	 * @param <T1>              the generic type
	 * @param <T2>              the generic type
	 * @param stIdentifiable    the st identifiable
	 * @param basyxIdentifiable the java identifiable object
	 */
	public static <T1 extends org.eclipse.papyrus.aas.Identifiable, T2 extends io.adminshell.aas.v3.model.Identifiable> void generateIdentification(
			T1 stIdentifiable, T2 basyxIdentifiable) {

		if (stIdentifiable.getIdentification() != null) {
			io.adminshell.aas.v3.model.Identifier idAAS = new DefaultIdentifier();
			if (stIdentifiable.getIdentification().getIdType() != null) {
				idAAS.setIdType(convertIdentifierType(stIdentifiable.getIdentification().getIdType()));

			}
			if (stIdentifiable.getIdentification().getId() != null) {
				idAAS.setIdentifier(stIdentifiable.getIdentification().getId().toString());
			}
			basyxIdentifiable.setIdentification(idAAS);
		} else {
			// Identification is mandatory for the aas
			// throw new Exception("Set the Identification of the aas");
			Identifier idAAS = new DefaultIdentifier();
			idAAS.setIdType(IdentifierType.IRI);
			idAAS.setIdentifier("https://institution.com/aas/aasid");
			basyxIdentifiable.setIdentification(idAAS);
		}

	}

	/**
	 * Generate submodel operations.
	 *
	 * @param submodelClass the submodel class
	 * @param basyxSubmodel the basyx submodel
	 * @param basyxSEC      the basyx SEC
	 */
	public static void generateSubmodelOperations(Class submodelClass,
			org.eclipse.aas.api.submodel.SubModel basyxSubmodel,
			org.eclipse.aas.api.submodel.submodelelement.SubModelElementCollection basyxSEC) {
		// add submodel operations
		if (submodelClass.getAllOperations() != null && !submodelClass.getAllOperations().isEmpty()) {
			int index = 0;
			for (org.eclipse.uml2.uml.Operation op : submodelClass.getAllOperations()) {

				index++;

				Operation basyxOperation = new Operation();

				org.eclipse.papyrus.aas.Operation stOperation = UMLUtil.getStereotypeApplication(op,
						org.eclipse.papyrus.aas.Operation.class);

				// generate only operations that have the stereotype Operation applied
				if (stOperation != null) {

					// generate common attributes of submodelElements

					generateSubmodelElementAttributes(stOperation, op, basyxOperation);

					// setting the semanticId of the MLP
					if (stOperation.getSemanticId() != null) {
						org.eclipse.papyrus.aas.Reference ref = stOperation.getSemanticId();
						Reference basyxRef = convertReference(ref);
						if (basyxRef != null)
							basyxOperation.setSemanticIdentifier(basyxRef);
					}

//				else {
//					// use the uml name to set the idshort
//					basyxOperation.setIdShort(
//							(op.getName() != null && op.getName().length() > 0) ? op.getName() : "op" + index);
//				}

					// Setting the OperationVariables

					List<OperationVariable> op_input = getOperationParameters(op, ParameterDirectionKind.IN);
					if (op_input.size() > 0) {

						basyxOperation.setInputVariables(op_input.toArray(new OperationVariable[op_input.size()]));
					}

					
					List<OperationVariable> op_inoutput = getOperationParameters(op, ParameterDirectionKind.INOUT);
					if (op_inoutput.size() > 0) {

						// set inout parameters as In parameters, since Basyx Java does not support
						// inout parameters
						basyxOperation
								.setInputVariables(op_inoutput.toArray(new OperationVariable[op_inoutput.size()]));

					}

					List<OperationVariable> op_output = getOperationParameters(op, ParameterDirectionKind.OUT);
					if (op_output.size() > 0) {

						basyxOperation.setOutputVariable(op_output.get(0));

					}

					// adding the basyxOperation to the basyxSubmodel

					if (basyxSubmodel != null)
						basyxSubmodel.setOperation(basyxOperation);

					// adding the basyxOperation to the basyxSEC

					if (basyxSEC != null)
						basyxSEC.setOperation(basyxOperation);

				}
			}
		}
	}

	/**
	 * Gets the operation parameters.
	 *
	 * @param op        the operation
	 * @param direction the direction of the parameters
	 * @return the list of OperationVariables
	 */
	public static List<OperationVariable> getOperationParameters(org.eclipse.uml2.uml.Operation op, int direction) {
		List<OperationVariable> paramaters = new ArrayList<OperationVariable>();
		if (op.getOwnedParameters() != null && !op.getOwnedParameters().isEmpty()) {

			int index = 0;
			for (org.eclipse.uml2.uml.Parameter param : op.getOwnedParameters()) {
				index++;
				String paramName = "param" + index;
				String paramType = "String";
				if (param != null && param.getName() != null) {
					paramName = param.getName();
					if (param.getDirection().getValue() == direction) {

						System.err.println("found parameter for direction " + direction + "\n");
						if (param.getType() != null && param.getType().getName() != null) {
							paramType = param.getType().getName();
						}
						OperationVariable basyxParam = new OperationVariable(paramName, convertType(paramType));
						paramaters.add(basyxParam);

					}
				}

			}

		}
		return paramaters;

	}

	/**
	 * Convert protocol.
	 *
	 * @param profileProtocol the profile protocol
	 * @return java api ProtocolKind
	 */
	public static ProtocolKind convertProtocol(org.eclipse.papyrus.aas.ProtocolKind profileProtocol) {

		if (profileProtocol.getLiteral() != null) {

			switch (profileProtocol.getLiteral()) {
			case "HTTP":
				return ProtocolKind.HTTP;

			case "MQTT":
				return ProtocolKind.MQTT;

			case "OPCUA":
				return ProtocolKind.OPCUA;

			case "CoAP":
				return ProtocolKind.CoAP;

			case "OTHER":
				return ProtocolKind.OTHER;
			}
		}

		return null;

	}

	/**
	 * Convert identifier type.
	 *
	 * @param idType the id type
	 * @return the identifier type
	 */
	public static IdentifierType convertIdentifierType(org.eclipse.papyrus.aas.IdentifierType idType) {

		if (idType.getLiteral() != null) {
			switch (idType.getLiteral()) {
			case "Custom":
				return IdentifierType.CUSTOM;
			case "IRDI":
				return IdentifierType.IRDI;
			case "IRI":
				return IdentifierType.IRI;

			}
			return null;
		}

		return null;

	}

	/**
	 * Convert modeling kind.
	 *
	 * @param kind the kind
	 * @return the modeling kind
	 */
	public static ModelingKind convertModelingKind(org.eclipse.papyrus.aas.ModelingKind kind) {

		if (kind.getLiteral() != null) {
			switch (kind.getLiteral()) {
			case "Template":
				return ModelingKind.TEMPLATE;
			case "Instance":
				return ModelingKind.INSTANCE;

			}
		}
		// return default value instance
		return ModelingKind.INSTANCE;

	}

	/**
	 * Convert key type.
	 *
	 * @param keyType the key type
	 * @return the key type
	 */
	public static KeyType convertKeyType(org.eclipse.papyrus.aas.KeyType keyType) {

		if (keyType.getLiteral() != null) {
			switch (keyType.getLiteral()) {
			case "Custom":
				return KeyType.CUSTOM;
			case "IRDI":
				return KeyType.IRDI;
			case "IRI":
				return KeyType.IRI;
			case "IdShort":
				return KeyType.ID_SHORT;
			case "FragmentId":
				return KeyType.FRAGMENT_ID;
			}
		}
		return null;

	}

	/**
	 * Convert asset kind.
	 *
	 * @param assetKind the asset kind
	 * @return the io.adminshell.aas.v 3 .model. asset kind
	 */
	public static io.adminshell.aas.v3.model.AssetKind convertAssetKind(org.eclipse.papyrus.aas.AssetKind assetKind) {
		if (assetKind.getLiteral() != null) {
			switch (assetKind.getLiteral()) {
			case "Type":
				return io.adminshell.aas.v3.model.AssetKind.TYPE;
			case "Instance":
				return io.adminshell.aas.v3.model.AssetKind.INSTANCE;

			}
		}

		return null;

	}

	/**
	 * Convert key elements.
	 *
	 * @param keyElements the key elements
	 * @return the key elements
	 */
	public static KeyElements convertKeyElements(org.eclipse.papyrus.aas.KeyElements keyElements) {

		if (keyElements.getLiteral() != null) {
			switch (keyElements.getLiteral()) {
			case "GlobalReference":
				return KeyElements.GLOBAL_REFERENCE;
			case "ConceptDescription":
				return KeyElements.CONCEPT_DESCRIPTION;

			}
		}
		return null;

	}
	
	/**
	 * Convert Mime Types.
	 *
	 * @param mimeType enumeration literal
	 * @return a string
	 */
	
	public static String convertMimeType(org.eclipse.papyrus.aas.MimeType mimeType) {
		
		if (mimeType.getLiteral()!=null) {
			
			switch (mimeType.getLiteral()) {
			case "applicationjson":
				return "application/json";
				
			case "applicationxls":
				return "application/xls";
				
			case "applicationpdf":
				return "application/pdf";
				
			case "applicationzip":
				return "application/zip";
			
			case "applicationstep":
				return "application/step";
				
			case "imagebmp":
				return "image/bmp";
				
			case "imagepng":
				return "image/png";
				
			case "imagejpeg":
				return "image/jpeg";
				
			case "imagegif":
				return "image/gif";
				
			case "textxml":
				return "text/xml";
				
			case "textplain":
				return "text/plain";
				
			case "texthtml":
				return "text/html";
				
			case "applicationxml":
				return "application/xml";
				
			case "applicationiges":
				return "application/iges";
			
			}
		}
		return "other";
		
		
	}

	/**
	 * Convert reference.
	 *
	 * @param ref the ref
	 * @return the reference
	 */
	public static Reference convertReference(org.eclipse.papyrus.aas.Reference ref) {
		Class refClass =ref.getBase_Class();
		if(refClass!=null) {
			List<Key> refKeys = new ArrayList<Key>();
			for(Classifier cl:refClass.getNestedClassifiers()) {
				org.eclipse.papyrus.aas.Key key =UMLUtil.getStereotypeApplication(cl, org.eclipse.papyrus.aas.Key.class);
				if(key!=null) {
					Key basyxKey = new Key(convertKeyElements(key.getType()),
							(key.getType().getLiteral().equals("GlobalReference") ? false : true),
							(key.getValue() != null) ? key.getValue() : "", convertKeyType(key.getIdType()));
					refKeys.add(basyxKey);
				}
				
				
			}
			Reference basyxRef = new Reference();
			basyxRef.setKeys(refKeys);
			return basyxRef;
		}else
			return null;
		
		
//		List<Key> refKeys = new ArrayList<Key>();
//		if (ref.getKey() != null) {
//			for (org.eclipse.papyrus.aas.Key key : ref.getKey()) {
//				Key basyxKey = new Key(convertKeyElements(key.getType()),
//						(key.getType().getLiteral().equals("GlobalReference") ? false : true),
//						(key.getValue() != null) ? key.getValue() : "", convertKeyType(key.getIdType()));
//				refKeys.add(basyxKey);
//			}
//
//			Reference basyxRef = new Reference();
//			basyxRef.setKeys(refKeys);
//			return basyxRef;
//		} else
//			return null;
	}

	/**
	 * Convert type.
	 *
	 * @param umlType the uml type
	 * @return the value type
	 */
	public static ValueType convertType(String umlType) {

		switch (umlType) {
		/////////
		// UML Primitive Types
		////////
		case "Boolean":
			return ValueType.Boolean;

		case "Integer":
			return ValueType.Integer;

		case "Real":
			return ValueType.Float;

		case "String":
			return ValueType.String;

		// map this type to Integer
		case "UnlimitedNatural":
			return ValueType.PositiveInteger;

		/////////
		// Ecore Primitive Types
		////////
		case "EString":
			return ValueType.String;

		case "EBoolean":
			return ValueType.Boolean;

		case "EDate":
			return ValueType.DateTime;

		case "EDouble":
			return ValueType.Double;

		case "EInt":
			return ValueType.Integer;

		case "EFloat":
			return ValueType.Float;

		/////////
		// XML DataTypes
		/////////
		
		case "Date": case "Time": case "DateTime": case "DateTimeStamp":
			return ValueType.DateTime;
		case "AnySimpleType":
			return ValueType.AnySimpleType;
		case "IDREFS":
			return ValueType.IDREF;
		case "Base64Binary":
			return ValueType.Base64Binary;
		case "AnyURI":
			return ValueType.AnyURI;
		case "Double":
			return ValueType.Double;
		case "Duration":
			return ValueType.Duration;
		case "Float":
			return ValueType.Float;
		case "GDay":
			return ValueType.GDay;
		case "GMonthDay":
			return ValueType.GMonthDay;
		case "GMonth":
			return ValueType.GMonth;
		case "GYear":
			return ValueType.GYear;
		case "GYearMonth":
			return ValueType.GYearMonth;
		case "HexBinary":
			return ValueType.HexBinary;
		case "NOTATION":
			return ValueType.NOTATION;
		case "QName":
			return ValueType.QName;	
		case "NonNegativeInteger":
			return ValueType.NonNegativeInteger;
		case "NonPositiveInteger":
			return ValueType.NonPositiveInteger;
		case "Int":
			return ValueType.Int32;
		case "Long":
			return ValueType.Int64;
		case "Short":
			return ValueType.Int16;
		case "Byte":
			return ValueType.Int8;
		case "NegativeInteger":
			return ValueType.NegativeInteger;
		case "PositiveInteger":
			return ValueType.PositiveInteger;
		case "UnsignedLong":
			return ValueType.UInt64;
		case "UnsignedByte":
			return ValueType.UInt8;
		case "UnsignedInt":
			return ValueType.UInt32;
		case "UnsignedShort":
			return ValueType.UInt16;

		case "ID":
			return ValueType.ID;
		case "IDREF":
			return ValueType.IDREF;
		case "ENTITY":
			return ValueType.ENTITY;

		}
		// case unrecognized type
		// we should also show a message saying that custom dataTypes, which are not
		// supported in Basyx, are not supported in Papyrus4Manufacturing code
		// generation as well
		return ValueType.None;

	}

	/**
	 * Convert descriptions.
	 *
	 * @param profileDescriptions the profile descriptions
	 * @return the list
	 */
	public static List<io.adminshell.aas.v3.model.LangString> convertDescriptions(
			List<LangString> profileDescriptions) {
		if (profileDescriptions != null && !profileDescriptions.isEmpty()) {
			List<io.adminshell.aas.v3.model.LangString> descriptions = new ArrayList<io.adminshell.aas.v3.model.LangString>();
			for (LangString desc : profileDescriptions) {
				io.adminshell.aas.v3.model.LangString basyxDesc = new io.adminshell.aas.v3.model.LangString(
						desc.getValue(), (desc.getLang() != null) ? desc.getLang().getLiteral() : "en");
				descriptions.add(basyxDesc);

			}

			return descriptions;
		} else
			return null;

	}

	/**
	 * Gets the shell.
	 *
	 * @return the shell
	 */
	public static Shell getShell() {
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (window == null) {
			IWorkbenchWindow[] windows = PlatformUI.getWorkbench().getWorkbenchWindows();
			if (windows.length > 0) {
				return windows[0].getShell();
			}
		} else {
			return window.getShell();
		}
		return null;
	}

	/**
	 * Generate node id.
	 *
	 * @param umlNodeid        the uml nodeid
	 * @param submodelproperty the submodelproperty
	 * @param submodelFile     the submodel file
	 */
	public static void generateNodeId(NodeId umlNodeid, Property submodelproperty, File submodelFile) {
		IdType idType = umlNodeid.getIdType();
		// submodelProperty.setIdentifier();
		if (idType.getLiteral() != null) {
			switch (idType.getLiteral()) {
			case "String":
				if (submodelproperty != null)
					submodelproperty.setIdentifier(umlNodeid.getIdentifier());

				if (submodelFile != null)
					submodelFile.setIdentifier(umlNodeid.getIdentifier());
				break;
			case "Integer":
				int idint;
				try {
					idint = Integer.parseInt(umlNodeid.getIdentifier());
				} catch (NumberFormatException e) {
					idint = 0;
				}
				if (submodelproperty != null)
					submodelproperty.setIdentifier(idint);

				if (submodelFile != null)
					submodelFile.setIdentifier(idint);
				// System.err.println("Papyrus codegen: \n \n");
				// System.err.println("Setting Nodeid for property:
				// "+submodelproperty.getIdShort()+" identifier:
				// "+submodelproperty.getIdentifier()+ " namespace:
				// "+submodelproperty.getNameSpaceIndex());
				break;

			case "Long":
				long idlong;
				try {
					idlong = Long.parseLong(umlNodeid.getIdentifier());
				} catch (NumberFormatException e) {
					idlong = 0;
				}
				if (submodelproperty != null)
					submodelproperty.setIdentifier(idlong);

				if (submodelFile != null)
					submodelFile.setIdentifier(idlong);

				break;
			case "ByteArray":
				if (convertStringtoByteArray(umlNodeid.getIdentifier()) != null) {
					if (submodelproperty != null)
						submodelproperty.setIdentifier(convertStringtoByteArray(umlNodeid.getIdentifier()));

					if (submodelFile != null)
						submodelFile.setIdentifier(convertStringtoByteArray(umlNodeid.getIdentifier()));

				}
				break;

			case "UUID":
				UUID uid;
				try {
					uid = UUID.fromString(umlNodeid.getIdentifier());

				} catch (IllegalArgumentException e) {
					uid = UUID.randomUUID();

				}
				if (submodelproperty != null)
					submodelproperty.setIdentifier(uid);

				if (submodelFile != null)
					submodelFile.setIdentifier(uid);
				break;

			}
		}
	}

	/**
	 * Convert stringto byte array.
	 *
	 * @param input the input
	 * @return the byte[]
	 */
	public static byte[] convertStringtoByteArray(String input) {
		// convert string to byte array
		if (input != null && input.length() > 1) {
			// remove "[" and "]" from the string:
			// "[0, 1, -2, 3, -4, -5, 6]" -> "0, 1, -2, 3, -4, -5, 6"
			String newString = input.substring(1, input.length() - 1);

			String[] stringArray = newString.split(", ");

			byte[] byte_array = new byte[stringArray.length];
			for (int i = 0; i < stringArray.length; i++) {
				try {
					byte_array[i] = (byte) Integer.parseInt(stringArray[i]);
				} catch (NumberFormatException e) {
					byte_array[i] = 0;
				}
			}
			return byte_array;
		} else
			return null;
	}

	public static String getPropertyValue(org.eclipse.uml2.uml.Property property) {
		ValueSpecification vs = property.getDefaultValue();
		if (vs == null) {
			if (property.getType() instanceof PrimitiveType) {
				PrimitiveType pt = (PrimitiveType) property.getType();
				if (pt.getName().equals("int")) {
					return "0";
				}
			}
			return "null";
		} else {
			if (vs.getType() == null) {
				return vs.stringValue();
			} else if (vs.getType().getName().equals("String")) {
				return vs.stringValue();
			} else if (vs.getType().getName().equals("Integer")) {
				return vs.integerValue() + "";
			} else if (vs.getType().getName().equals("Boolean")) {
				return vs.booleanValue() + "";
			}
			return vs.stringValue();
		}
	}

}
