/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node Id</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.NodeId#getNameSpaceIndex <em>Name Space Index</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.NodeId#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.NodeId#getIdType <em>Id Type</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.aas.AASPackage#getNodeId()
 * @model
 * @generated
 */
public interface NodeId extends EObject {
	/**
	 * Returns the value of the '<em><b>Name Space Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name Space Index</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name Space Index</em>' attribute.
	 * @see #setNameSpaceIndex(int)
	 * @see org.eclipse.papyrus.aas.AASPackage#getNodeId_NameSpaceIndex()
	 * @model dataType="org.eclipse.uml2.types.Integer" ordered="false"
	 * @generated
	 */
	int getNameSpaceIndex();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.NodeId#getNameSpaceIndex <em>Name Space Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name Space Index</em>' attribute.
	 * @see #getNameSpaceIndex()
	 * @generated
	 */
	void setNameSpaceIndex(int value);

	/**
	 * Returns the value of the '<em><b>Identifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Identifier</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identifier</em>' attribute.
	 * @see #setIdentifier(String)
	 * @see org.eclipse.papyrus.aas.AASPackage#getNodeId_Identifier()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getIdentifier();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.NodeId#getIdentifier <em>Identifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Identifier</em>' attribute.
	 * @see #getIdentifier()
	 * @generated
	 */
	void setIdentifier(String value);

	/**
	 * Returns the value of the '<em><b>Id Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.aas.IdType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id Type</em>' attribute.
	 * @see org.eclipse.papyrus.aas.IdType
	 * @see #setIdType(IdType)
	 * @see org.eclipse.papyrus.aas.AASPackage#getNodeId_IdType()
	 * @model ordered="false"
	 * @generated
	 */
	IdType getIdType();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.NodeId#getIdType <em>Id Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id Type</em>' attribute.
	 * @see org.eclipse.papyrus.aas.IdType
	 * @see #getIdType()
	 * @generated
	 */
	void setIdType(IdType value);

} // NodeId
