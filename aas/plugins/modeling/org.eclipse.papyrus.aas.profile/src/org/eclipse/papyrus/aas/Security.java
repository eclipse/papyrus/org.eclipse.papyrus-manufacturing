/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Security</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.Security#getBase_Class <em>Base Class</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.Security#getAccessControlPolicyPoints <em>Access Control Policy Points</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.Security#getCertificate <em>Certificate</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.Security#getRequiredCertificateExtension <em>Required Certificate Extension</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.aas.AASPackage#getSecurity()
 * @model
 * @generated
 */
public interface Security extends EObject {
	/**
	 * Returns the value of the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Class</em>' reference.
	 * @see #setBase_Class(org.eclipse.uml2.uml.Class)
	 * @see org.eclipse.papyrus.aas.AASPackage#getSecurity_Base_Class()
	 * @model ordered="false"
	 * @generated
	 */
	org.eclipse.uml2.uml.Class getBase_Class();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.Security#getBase_Class <em>Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Class</em>' reference.
	 * @see #getBase_Class()
	 * @generated
	 */
	void setBase_Class(org.eclipse.uml2.uml.Class value);

	/**
	 * Returns the value of the '<em><b>Access Control Policy Points</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access Control Policy Points</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access Control Policy Points</em>' containment reference.
	 * @see #setAccessControlPolicyPoints(AccessControlPolicyPoints)
	 * @see org.eclipse.papyrus.aas.AASPackage#getSecurity_AccessControlPolicyPoints()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	AccessControlPolicyPoints getAccessControlPolicyPoints();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.Security#getAccessControlPolicyPoints <em>Access Control Policy Points</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access Control Policy Points</em>' containment reference.
	 * @see #getAccessControlPolicyPoints()
	 * @generated
	 */
	void setAccessControlPolicyPoints(AccessControlPolicyPoints value);

	/**
	 * Returns the value of the '<em><b>Certificate</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.aas.Certificate}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Certificate</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Certificate</em>' containment reference list.
	 * @see org.eclipse.papyrus.aas.AASPackage#getSecurity_Certificate()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Certificate> getCertificate();

	/**
	 * Returns the value of the '<em><b>Required Certificate Extension</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.aas.Reference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Certificate Extension</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Certificate Extension</em>' reference list.
	 * @see org.eclipse.papyrus.aas.AASPackage#getSecurity_RequiredCertificateExtension()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Reference> getRequiredCertificateExtension();

} // Security
