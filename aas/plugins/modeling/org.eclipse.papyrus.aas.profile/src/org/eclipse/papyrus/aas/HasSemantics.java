/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Has Semantics</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * semanticid is present in Basyx but not used
 * semanticid should be typed by an URI or a refereable
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.HasSemantics#getSemanticId <em>Semantic Id</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.HasSemantics#getBase_HasSemantics_Class <em>Base Has Semantics Class</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.aas.AASPackage#getHasSemantics()
 * @model abstract="true"
 * @generated
 */
public interface HasSemantics extends EObject {
	/**
	 * Returns the value of the '<em><b>Semantic Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Semantic Id</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semantic Id</em>' reference.
	 * @see #setSemanticId(Reference)
	 * @see org.eclipse.papyrus.aas.AASPackage#getHasSemantics_SemanticId()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Reference getSemanticId();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.HasSemantics#getSemanticId <em>Semantic Id</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Semantic Id</em>' reference.
	 * @see #getSemanticId()
	 * @generated
	 */
	void setSemanticId(Reference value);

	/**
	 * Returns the value of the '<em><b>Base Has Semantics Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Has Semantics Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Has Semantics Class</em>' reference.
	 * @see #setBase_HasSemantics_Class(org.eclipse.uml2.uml.Class)
	 * @see org.eclipse.papyrus.aas.AASPackage#getHasSemantics_Base_HasSemantics_Class()
	 * @model ordered="false"
	 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='base_Class'"
	 * @generated
	 */
	org.eclipse.uml2.uml.Class getBase_HasSemantics_Class();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.HasSemantics#getBase_HasSemantics_Class <em>Base Has Semantics Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Has Semantics Class</em>' reference.
	 * @see #getBase_HasSemantics_Class()
	 * @generated
	 */
	void setBase_HasSemantics_Class(org.eclipse.uml2.uml.Class value);

} // HasSemantics
