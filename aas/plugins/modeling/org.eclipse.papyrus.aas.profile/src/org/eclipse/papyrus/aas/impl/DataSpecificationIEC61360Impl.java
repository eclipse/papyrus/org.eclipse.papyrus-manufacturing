/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.papyrus.aas.AASPackage;
import org.eclipse.papyrus.aas.DataSpecificationIEC61360;
import org.eclipse.papyrus.aas.LangString;
import org.eclipse.papyrus.aas.LevelType;
import org.eclipse.papyrus.aas.Reference;
import org.eclipse.papyrus.aas.ValueReferencePairType;

import org.eclipse.uml2.uml.ValueSpecification;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Specification IEC61360</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.impl.DataSpecificationIEC61360Impl#getBase_Class <em>Base Class</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.DataSpecificationIEC61360Impl#getPreferredName <em>Preferred Name</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.DataSpecificationIEC61360Impl#getShortName <em>Short Name</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.DataSpecificationIEC61360Impl#getUnit <em>Unit</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.DataSpecificationIEC61360Impl#getUnitId <em>Unit Id</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.DataSpecificationIEC61360Impl#getSourceOfDefinition <em>Source Of Definition</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.DataSpecificationIEC61360Impl#getSymbol <em>Symbol</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.DataSpecificationIEC61360Impl#getDataType <em>Data Type</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.DataSpecificationIEC61360Impl#getDefinition <em>Definition</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.DataSpecificationIEC61360Impl#getValueFormat <em>Value Format</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.DataSpecificationIEC61360Impl#getValueList <em>Value List</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.DataSpecificationIEC61360Impl#getValue <em>Value</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.DataSpecificationIEC61360Impl#getValueId <em>Value Id</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.DataSpecificationIEC61360Impl#getLevelType <em>Level Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataSpecificationIEC61360Impl extends MinimalEObjectImpl.Container implements DataSpecificationIEC61360 {
	/**
	 * The cached value of the '{@link #getBase_Class() <em>Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Class()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Class base_Class;

	/**
	 * The cached value of the '{@link #getPreferredName() <em>Preferred Name</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreferredName()
	 * @generated
	 * @ordered
	 */
	protected EList<LangString> preferredName;

	/**
	 * The cached value of the '{@link #getShortName() <em>Short Name</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShortName()
	 * @generated
	 * @ordered
	 */
	protected EList<LangString> shortName;

	/**
	 * The default value of the '{@link #getUnit() <em>Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnit()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnit() <em>Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnit()
	 * @generated
	 * @ordered
	 */
	protected String unit = UNIT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getUnitId() <em>Unit Id</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitId()
	 * @generated
	 * @ordered
	 */
	protected Reference unitId;

	/**
	 * The default value of the '{@link #getSourceOfDefinition() <em>Source Of Definition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceOfDefinition()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_OF_DEFINITION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSourceOfDefinition() <em>Source Of Definition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceOfDefinition()
	 * @generated
	 * @ordered
	 */
	protected String sourceOfDefinition = SOURCE_OF_DEFINITION_EDEFAULT;

	/**
	 * The default value of the '{@link #getSymbol() <em>Symbol</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSymbol()
	 * @generated
	 * @ordered
	 */
	protected static final String SYMBOL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSymbol() <em>Symbol</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSymbol()
	 * @generated
	 * @ordered
	 */
	protected String symbol = SYMBOL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDataType() <em>Data Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataType()
	 * @generated
	 * @ordered
	 */
	protected DataSpecificationIEC61360 dataType;

	/**
	 * The cached value of the '{@link #getDefinition() <em>Definition</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefinition()
	 * @generated
	 * @ordered
	 */
	protected EList<LangString> definition;

	/**
	 * The default value of the '{@link #getValueFormat() <em>Value Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueFormat()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_FORMAT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValueFormat() <em>Value Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueFormat()
	 * @generated
	 * @ordered
	 */
	protected String valueFormat = VALUE_FORMAT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getValueList() <em>Value List</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueList()
	 * @generated
	 * @ordered
	 */
	protected EList<ValueReferencePairType> valueList;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected ValueSpecification value;

	/**
	 * The cached value of the '{@link #getValueId() <em>Value Id</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueId()
	 * @generated
	 * @ordered
	 */
	protected Reference valueId;

	/**
	 * The cached value of the '{@link #getLevelType() <em>Level Type</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevelType()
	 * @generated
	 * @ordered
	 */
	protected EList<LevelType> levelType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataSpecificationIEC61360Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AASPackage.Literals.DATA_SPECIFICATION_IEC61360;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public org.eclipse.uml2.uml.Class getBase_Class() {
		if (base_Class != null && base_Class.eIsProxy()) {
			InternalEObject oldBase_Class = (InternalEObject)base_Class;
			base_Class = (org.eclipse.uml2.uml.Class)eResolveProxy(oldBase_Class);
			if (base_Class != oldBase_Class) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AASPackage.DATA_SPECIFICATION_IEC61360__BASE_CLASS, oldBase_Class, base_Class));
			}
		}
		return base_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class basicGetBase_Class() {
		return base_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_Class(org.eclipse.uml2.uml.Class newBase_Class) {
		org.eclipse.uml2.uml.Class oldBase_Class = base_Class;
		base_Class = newBase_Class;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.DATA_SPECIFICATION_IEC61360__BASE_CLASS, oldBase_Class, base_Class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<LangString> getPreferredName() {
		if (preferredName == null) {
			preferredName = new EObjectContainmentEList<LangString>(LangString.class, this, AASPackage.DATA_SPECIFICATION_IEC61360__PREFERRED_NAME);
		}
		return preferredName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<LangString> getShortName() {
		if (shortName == null) {
			shortName = new EObjectContainmentEList<LangString>(LangString.class, this, AASPackage.DATA_SPECIFICATION_IEC61360__SHORT_NAME);
		}
		return shortName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getUnit() {
		return unit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUnit(String newUnit) {
		String oldUnit = unit;
		unit = newUnit;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.DATA_SPECIFICATION_IEC61360__UNIT, oldUnit, unit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Reference getUnitId() {
		if (unitId != null && unitId.eIsProxy()) {
			InternalEObject oldUnitId = (InternalEObject)unitId;
			unitId = (Reference)eResolveProxy(oldUnitId);
			if (unitId != oldUnitId) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AASPackage.DATA_SPECIFICATION_IEC61360__UNIT_ID, oldUnitId, unitId));
			}
		}
		return unitId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reference basicGetUnitId() {
		return unitId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUnitId(Reference newUnitId) {
		Reference oldUnitId = unitId;
		unitId = newUnitId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.DATA_SPECIFICATION_IEC61360__UNIT_ID, oldUnitId, unitId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSourceOfDefinition() {
		return sourceOfDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSourceOfDefinition(String newSourceOfDefinition) {
		String oldSourceOfDefinition = sourceOfDefinition;
		sourceOfDefinition = newSourceOfDefinition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.DATA_SPECIFICATION_IEC61360__SOURCE_OF_DEFINITION, oldSourceOfDefinition, sourceOfDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSymbol() {
		return symbol;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSymbol(String newSymbol) {
		String oldSymbol = symbol;
		symbol = newSymbol;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.DATA_SPECIFICATION_IEC61360__SYMBOL, oldSymbol, symbol));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DataSpecificationIEC61360 getDataType() {
		if (dataType != null && dataType.eIsProxy()) {
			InternalEObject oldDataType = (InternalEObject)dataType;
			dataType = (DataSpecificationIEC61360)eResolveProxy(oldDataType);
			if (dataType != oldDataType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AASPackage.DATA_SPECIFICATION_IEC61360__DATA_TYPE, oldDataType, dataType));
			}
		}
		return dataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataSpecificationIEC61360 basicGetDataType() {
		return dataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDataType(DataSpecificationIEC61360 newDataType) {
		DataSpecificationIEC61360 oldDataType = dataType;
		dataType = newDataType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.DATA_SPECIFICATION_IEC61360__DATA_TYPE, oldDataType, dataType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<LangString> getDefinition() {
		if (definition == null) {
			definition = new EObjectContainmentEList<LangString>(LangString.class, this, AASPackage.DATA_SPECIFICATION_IEC61360__DEFINITION);
		}
		return definition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getValueFormat() {
		return valueFormat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setValueFormat(String newValueFormat) {
		String oldValueFormat = valueFormat;
		valueFormat = newValueFormat;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.DATA_SPECIFICATION_IEC61360__VALUE_FORMAT, oldValueFormat, valueFormat));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ValueReferencePairType> getValueList() {
		if (valueList == null) {
			valueList = new EObjectContainmentEList<ValueReferencePairType>(ValueReferencePairType.class, this, AASPackage.DATA_SPECIFICATION_IEC61360__VALUE_LIST);
		}
		return valueList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ValueSpecification getValue() {
		if (value != null && value.eIsProxy()) {
			InternalEObject oldValue = (InternalEObject)value;
			value = (ValueSpecification)eResolveProxy(oldValue);
			if (value != oldValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AASPackage.DATA_SPECIFICATION_IEC61360__VALUE, oldValue, value));
			}
		}
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueSpecification basicGetValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setValue(ValueSpecification newValue) {
		ValueSpecification oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.DATA_SPECIFICATION_IEC61360__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Reference getValueId() {
		if (valueId != null && valueId.eIsProxy()) {
			InternalEObject oldValueId = (InternalEObject)valueId;
			valueId = (Reference)eResolveProxy(oldValueId);
			if (valueId != oldValueId) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AASPackage.DATA_SPECIFICATION_IEC61360__VALUE_ID, oldValueId, valueId));
			}
		}
		return valueId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reference basicGetValueId() {
		return valueId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setValueId(Reference newValueId) {
		Reference oldValueId = valueId;
		valueId = newValueId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.DATA_SPECIFICATION_IEC61360__VALUE_ID, oldValueId, valueId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<LevelType> getLevelType() {
		if (levelType == null) {
			levelType = new EDataTypeUniqueEList<LevelType>(LevelType.class, this, AASPackage.DATA_SPECIFICATION_IEC61360__LEVEL_TYPE);
		}
		return levelType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AASPackage.DATA_SPECIFICATION_IEC61360__PREFERRED_NAME:
				return ((InternalEList<?>)getPreferredName()).basicRemove(otherEnd, msgs);
			case AASPackage.DATA_SPECIFICATION_IEC61360__SHORT_NAME:
				return ((InternalEList<?>)getShortName()).basicRemove(otherEnd, msgs);
			case AASPackage.DATA_SPECIFICATION_IEC61360__DEFINITION:
				return ((InternalEList<?>)getDefinition()).basicRemove(otherEnd, msgs);
			case AASPackage.DATA_SPECIFICATION_IEC61360__VALUE_LIST:
				return ((InternalEList<?>)getValueList()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AASPackage.DATA_SPECIFICATION_IEC61360__BASE_CLASS:
				if (resolve) return getBase_Class();
				return basicGetBase_Class();
			case AASPackage.DATA_SPECIFICATION_IEC61360__PREFERRED_NAME:
				return getPreferredName();
			case AASPackage.DATA_SPECIFICATION_IEC61360__SHORT_NAME:
				return getShortName();
			case AASPackage.DATA_SPECIFICATION_IEC61360__UNIT:
				return getUnit();
			case AASPackage.DATA_SPECIFICATION_IEC61360__UNIT_ID:
				if (resolve) return getUnitId();
				return basicGetUnitId();
			case AASPackage.DATA_SPECIFICATION_IEC61360__SOURCE_OF_DEFINITION:
				return getSourceOfDefinition();
			case AASPackage.DATA_SPECIFICATION_IEC61360__SYMBOL:
				return getSymbol();
			case AASPackage.DATA_SPECIFICATION_IEC61360__DATA_TYPE:
				if (resolve) return getDataType();
				return basicGetDataType();
			case AASPackage.DATA_SPECIFICATION_IEC61360__DEFINITION:
				return getDefinition();
			case AASPackage.DATA_SPECIFICATION_IEC61360__VALUE_FORMAT:
				return getValueFormat();
			case AASPackage.DATA_SPECIFICATION_IEC61360__VALUE_LIST:
				return getValueList();
			case AASPackage.DATA_SPECIFICATION_IEC61360__VALUE:
				if (resolve) return getValue();
				return basicGetValue();
			case AASPackage.DATA_SPECIFICATION_IEC61360__VALUE_ID:
				if (resolve) return getValueId();
				return basicGetValueId();
			case AASPackage.DATA_SPECIFICATION_IEC61360__LEVEL_TYPE:
				return getLevelType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AASPackage.DATA_SPECIFICATION_IEC61360__BASE_CLASS:
				setBase_Class((org.eclipse.uml2.uml.Class)newValue);
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__PREFERRED_NAME:
				getPreferredName().clear();
				getPreferredName().addAll((Collection<? extends LangString>)newValue);
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__SHORT_NAME:
				getShortName().clear();
				getShortName().addAll((Collection<? extends LangString>)newValue);
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__UNIT:
				setUnit((String)newValue);
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__UNIT_ID:
				setUnitId((Reference)newValue);
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__SOURCE_OF_DEFINITION:
				setSourceOfDefinition((String)newValue);
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__SYMBOL:
				setSymbol((String)newValue);
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__DATA_TYPE:
				setDataType((DataSpecificationIEC61360)newValue);
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__DEFINITION:
				getDefinition().clear();
				getDefinition().addAll((Collection<? extends LangString>)newValue);
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__VALUE_FORMAT:
				setValueFormat((String)newValue);
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__VALUE_LIST:
				getValueList().clear();
				getValueList().addAll((Collection<? extends ValueReferencePairType>)newValue);
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__VALUE:
				setValue((ValueSpecification)newValue);
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__VALUE_ID:
				setValueId((Reference)newValue);
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__LEVEL_TYPE:
				getLevelType().clear();
				getLevelType().addAll((Collection<? extends LevelType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AASPackage.DATA_SPECIFICATION_IEC61360__BASE_CLASS:
				setBase_Class((org.eclipse.uml2.uml.Class)null);
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__PREFERRED_NAME:
				getPreferredName().clear();
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__SHORT_NAME:
				getShortName().clear();
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__UNIT:
				setUnit(UNIT_EDEFAULT);
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__UNIT_ID:
				setUnitId((Reference)null);
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__SOURCE_OF_DEFINITION:
				setSourceOfDefinition(SOURCE_OF_DEFINITION_EDEFAULT);
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__SYMBOL:
				setSymbol(SYMBOL_EDEFAULT);
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__DATA_TYPE:
				setDataType((DataSpecificationIEC61360)null);
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__DEFINITION:
				getDefinition().clear();
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__VALUE_FORMAT:
				setValueFormat(VALUE_FORMAT_EDEFAULT);
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__VALUE_LIST:
				getValueList().clear();
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__VALUE:
				setValue((ValueSpecification)null);
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__VALUE_ID:
				setValueId((Reference)null);
				return;
			case AASPackage.DATA_SPECIFICATION_IEC61360__LEVEL_TYPE:
				getLevelType().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AASPackage.DATA_SPECIFICATION_IEC61360__BASE_CLASS:
				return base_Class != null;
			case AASPackage.DATA_SPECIFICATION_IEC61360__PREFERRED_NAME:
				return preferredName != null && !preferredName.isEmpty();
			case AASPackage.DATA_SPECIFICATION_IEC61360__SHORT_NAME:
				return shortName != null && !shortName.isEmpty();
			case AASPackage.DATA_SPECIFICATION_IEC61360__UNIT:
				return UNIT_EDEFAULT == null ? unit != null : !UNIT_EDEFAULT.equals(unit);
			case AASPackage.DATA_SPECIFICATION_IEC61360__UNIT_ID:
				return unitId != null;
			case AASPackage.DATA_SPECIFICATION_IEC61360__SOURCE_OF_DEFINITION:
				return SOURCE_OF_DEFINITION_EDEFAULT == null ? sourceOfDefinition != null : !SOURCE_OF_DEFINITION_EDEFAULT.equals(sourceOfDefinition);
			case AASPackage.DATA_SPECIFICATION_IEC61360__SYMBOL:
				return SYMBOL_EDEFAULT == null ? symbol != null : !SYMBOL_EDEFAULT.equals(symbol);
			case AASPackage.DATA_SPECIFICATION_IEC61360__DATA_TYPE:
				return dataType != null;
			case AASPackage.DATA_SPECIFICATION_IEC61360__DEFINITION:
				return definition != null && !definition.isEmpty();
			case AASPackage.DATA_SPECIFICATION_IEC61360__VALUE_FORMAT:
				return VALUE_FORMAT_EDEFAULT == null ? valueFormat != null : !VALUE_FORMAT_EDEFAULT.equals(valueFormat);
			case AASPackage.DATA_SPECIFICATION_IEC61360__VALUE_LIST:
				return valueList != null && !valueList.isEmpty();
			case AASPackage.DATA_SPECIFICATION_IEC61360__VALUE:
				return value != null;
			case AASPackage.DATA_SPECIFICATION_IEC61360__VALUE_ID:
				return valueId != null;
			case AASPackage.DATA_SPECIFICATION_IEC61360__LEVEL_TYPE:
				return levelType != null && !levelType.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (unit: ");
		result.append(unit);
		result.append(", sourceOfDefinition: ");
		result.append(sourceOfDefinition);
		result.append(", symbol: ");
		result.append(symbol);
		result.append(", valueFormat: ");
		result.append(valueFormat);
		result.append(", levelType: ");
		result.append(levelType);
		result.append(')');
		return result.toString();
	}

} //DataSpecificationIEC61360Impl
