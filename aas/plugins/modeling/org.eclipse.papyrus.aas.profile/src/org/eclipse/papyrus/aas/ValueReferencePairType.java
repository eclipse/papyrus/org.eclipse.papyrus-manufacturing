/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.uml2.uml.ValueSpecification;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value Reference Pair Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.ValueReferencePairType#getValue <em>Value</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.ValueReferencePairType#getValueId <em>Value Id</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.aas.AASPackage#getValueReferencePairType()
 * @model
 * @generated
 */
public interface ValueReferencePairType extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' reference.
	 * @see #setValue(ValueSpecification)
	 * @see org.eclipse.papyrus.aas.AASPackage#getValueReferencePairType_Value()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	ValueSpecification getValue();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.ValueReferencePairType#getValue <em>Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(ValueSpecification value);

	/**
	 * Returns the value of the '<em><b>Value Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Id</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Id</em>' reference.
	 * @see #setValueId(Reference)
	 * @see org.eclipse.papyrus.aas.AASPackage#getValueReferencePairType_ValueId()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Reference getValueId();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.ValueReferencePairType#getValueId <em>Value Id</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Id</em>' reference.
	 * @see #getValueId()
	 * @generated
	 */
	void setValueId(Reference value);

} // ValueReferencePairType
