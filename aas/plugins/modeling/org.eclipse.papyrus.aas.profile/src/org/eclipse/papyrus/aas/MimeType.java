/**
 * Copyright (c) 2023 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 *   SPDX-License-Identifier: EPL-2.0
 *
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Mime Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.aas.AASPackage#getMimeType()
 * @model
 * @generated
 */
public enum MimeType implements Enumerator {
	/**
	 * The '<em><b>Applicationjson</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #APPLICATIONJSON_VALUE
	 * @generated
	 * @ordered
	 */
	APPLICATIONJSON(0, "applicationjson", "applicationjson"),
	/**
	 * The '<em><b>Applicationxls</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #APPLICATIONXLS_VALUE
	 * @generated
	 * @ordered
	 */
	APPLICATIONXLS(1, "applicationxls", "applicationxls"),
	/**
	 * The '<em><b>Applicationpdf</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #APPLICATIONPDF_VALUE
	 * @generated
	 * @ordered
	 */
	APPLICATIONPDF(2, "applicationpdf", "applicationpdf"),
	/**
	 * The '<em><b>Applicationzip</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #APPLICATIONZIP_VALUE
	 * @generated
	 * @ordered
	 */
	APPLICATIONZIP(3, "applicationzip", "applicationzip"),
	/**
	 * The '<em><b>Applicationxml</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #APPLICATIONXML_VALUE
	 * @generated
	 * @ordered
	 */
	APPLICATIONXML(4, "applicationxml", "applicationxml"),
	/**
	 * The '<em><b>Applicationiges</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #APPLICATIONIGES_VALUE
	 * @generated
	 * @ordered
	 */
	APPLICATIONIGES(5, "applicationiges", "applicationiges"), /**
	 * The '<em><b>Applicationstep</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #APPLICATIONSTEP_VALUE
	 * @generated
	 * @ordered
	 */
	APPLICATIONSTEP(6, "applicationstep", "applicationstep"), /**
	 * The '<em><b>Imagepng</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IMAGEPNG_VALUE
	 * @generated
	 * @ordered
	 */
	IMAGEPNG(7, "imagepng", "imagepng"), /**
	 * The '<em><b>Imagebmp</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IMAGEBMP_VALUE
	 * @generated
	 * @ordered
	 */
	IMAGEBMP(8, "imagebmp", "imagebmp"), /**
	 * The '<em><b>Imagejpeg</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IMAGEJPEG_VALUE
	 * @generated
	 * @ordered
	 */
	IMAGEJPEG(9, "imagejpeg", "imagejpeg"), /**
	 * The '<em><b>Imagegif</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IMAGEGIF_VALUE
	 * @generated
	 * @ordered
	 */
	IMAGEGIF(10, "imagegif", "imagegif"), /**
	 * The '<em><b>Textxml</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEXTXML_VALUE
	 * @generated
	 * @ordered
	 */
	TEXTXML(11, "textxml", "textxml"), /**
	 * The '<em><b>Textplain</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEXTPLAIN_VALUE
	 * @generated
	 * @ordered
	 */
	TEXTPLAIN(12, "textplain", "textplain"), /**
	 * The '<em><b>Texthtml</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEXTHTML_VALUE
	 * @generated
	 * @ordered
	 */
	TEXTHTML(13, "texthtml", "texthtml"), /**
	 * The '<em><b>Other</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OTHER_VALUE
	 * @generated
	 * @ordered
	 */
	OTHER(14, "other", "other");

	/**
	 * The '<em><b>Applicationjson</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #APPLICATIONJSON
	 * @model name="applicationjson"
	 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='application-json'"
	 * @generated
	 * @ordered
	 */
	public static final int APPLICATIONJSON_VALUE = 0;

	/**
	 * The '<em><b>Applicationxls</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #APPLICATIONXLS
	 * @model name="applicationxls"
	 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='application-xls'"
	 * @generated
	 * @ordered
	 */
	public static final int APPLICATIONXLS_VALUE = 1;

	/**
	 * The '<em><b>Applicationpdf</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #APPLICATIONPDF
	 * @model name="applicationpdf"
	 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='application-pdf'"
	 * @generated
	 * @ordered
	 */
	public static final int APPLICATIONPDF_VALUE = 2;

	/**
	 * The '<em><b>Applicationzip</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #APPLICATIONZIP
	 * @model name="applicationzip"
	 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='application-zip'"
	 * @generated
	 * @ordered
	 */
	public static final int APPLICATIONZIP_VALUE = 3;

	/**
	 * The '<em><b>Applicationxml</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #APPLICATIONXML
	 * @model name="applicationxml"
	 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='application-xml'"
	 * @generated
	 * @ordered
	 */
	public static final int APPLICATIONXML_VALUE = 4;

	/**
	 * The '<em><b>Applicationiges</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #APPLICATIONIGES
	 * @model name="applicationiges"
	 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='application-iges'"
	 * @generated
	 * @ordered
	 */
	public static final int APPLICATIONIGES_VALUE = 5;

	/**
	 * The '<em><b>Applicationstep</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #APPLICATIONSTEP
	 * @model name="applicationstep"
	 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='application-step'"
	 * @generated
	 * @ordered
	 */
	public static final int APPLICATIONSTEP_VALUE = 6;

	/**
	 * The '<em><b>Imagepng</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IMAGEPNG
	 * @model name="imagepng"
	 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='image-png'"
	 * @generated
	 * @ordered
	 */
	public static final int IMAGEPNG_VALUE = 7;

	/**
	 * The '<em><b>Imagebmp</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IMAGEBMP
	 * @model name="imagebmp"
	 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='image-bmp'"
	 * @generated
	 * @ordered
	 */
	public static final int IMAGEBMP_VALUE = 8;

	/**
	 * The '<em><b>Imagejpeg</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IMAGEJPEG
	 * @model name="imagejpeg"
	 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='image-jpeg'"
	 * @generated
	 * @ordered
	 */
	public static final int IMAGEJPEG_VALUE = 9;

	/**
	 * The '<em><b>Imagegif</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IMAGEGIF
	 * @model name="imagegif"
	 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='image-gif'"
	 * @generated
	 * @ordered
	 */
	public static final int IMAGEGIF_VALUE = 10;

	/**
	 * The '<em><b>Textxml</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEXTXML
	 * @model name="textxml"
	 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='text-xml'"
	 * @generated
	 * @ordered
	 */
	public static final int TEXTXML_VALUE = 11;

	/**
	 * The '<em><b>Textplain</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEXTPLAIN
	 * @model name="textplain"
	 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='text-plain'"
	 * @generated
	 * @ordered
	 */
	public static final int TEXTPLAIN_VALUE = 12;

	/**
	 * The '<em><b>Texthtml</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEXTHTML
	 * @model name="texthtml"
	 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='text-html'"
	 * @generated
	 * @ordered
	 */
	public static final int TEXTHTML_VALUE = 13;

	/**
	 * The '<em><b>Other</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OTHER
	 * @model name="other"
	 * @generated
	 * @ordered
	 */
	public static final int OTHER_VALUE = 14;

	/**
	 * An array of all the '<em><b>Mime Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MimeType[] VALUES_ARRAY = new MimeType[] {
			APPLICATIONJSON,
			APPLICATIONXLS,
			APPLICATIONPDF,
			APPLICATIONZIP,
			APPLICATIONXML,
			APPLICATIONIGES,
			APPLICATIONSTEP,
			IMAGEPNG,
			IMAGEBMP,
			IMAGEJPEG,
			IMAGEGIF,
			TEXTXML,
			TEXTPLAIN,
			TEXTHTML,
			OTHER,
		};

	/**
	 * A public read-only list of all the '<em><b>Mime Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MimeType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Mime Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static MimeType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MimeType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Mime Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static MimeType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MimeType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Mime Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static MimeType get(int value) {
		switch (value) {
			case APPLICATIONJSON_VALUE: return APPLICATIONJSON;
			case APPLICATIONXLS_VALUE: return APPLICATIONXLS;
			case APPLICATIONPDF_VALUE: return APPLICATIONPDF;
			case APPLICATIONZIP_VALUE: return APPLICATIONZIP;
			case APPLICATIONXML_VALUE: return APPLICATIONXML;
			case APPLICATIONIGES_VALUE: return APPLICATIONIGES;
			case APPLICATIONSTEP_VALUE: return APPLICATIONSTEP;
			case IMAGEPNG_VALUE: return IMAGEPNG;
			case IMAGEBMP_VALUE: return IMAGEBMP;
			case IMAGEJPEG_VALUE: return IMAGEJPEG;
			case IMAGEGIF_VALUE: return IMAGEGIF;
			case TEXTXML_VALUE: return TEXTXML;
			case TEXTPLAIN_VALUE: return TEXTPLAIN;
			case TEXTHTML_VALUE: return TEXTHTML;
			case OTHER_VALUE: return OTHER;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MimeType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} // MimeType
