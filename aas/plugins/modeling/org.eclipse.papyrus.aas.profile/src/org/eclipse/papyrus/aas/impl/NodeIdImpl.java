/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.papyrus.aas.AASPackage;
import org.eclipse.papyrus.aas.IdType;
import org.eclipse.papyrus.aas.NodeId;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Node Id</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.impl.NodeIdImpl#getNameSpaceIndex <em>Name Space Index</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.NodeIdImpl#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.NodeIdImpl#getIdType <em>Id Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NodeIdImpl extends MinimalEObjectImpl.Container implements NodeId {
	/**
	 * The default value of the '{@link #getNameSpaceIndex() <em>Name Space Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNameSpaceIndex()
	 * @generated
	 * @ordered
	 */
	protected static final int NAME_SPACE_INDEX_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNameSpaceIndex() <em>Name Space Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNameSpaceIndex()
	 * @generated
	 * @ordered
	 */
	protected int nameSpaceIndex = NAME_SPACE_INDEX_EDEFAULT;

	/**
	 * The default value of the '{@link #getIdentifier() <em>Identifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdentifier()
	 * @generated
	 * @ordered
	 */
	protected static final String IDENTIFIER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIdentifier() <em>Identifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdentifier()
	 * @generated
	 * @ordered
	 */
	protected String identifier = IDENTIFIER_EDEFAULT;

	/**
	 * The default value of the '{@link #getIdType() <em>Id Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdType()
	 * @generated
	 * @ordered
	 */
	protected static final IdType ID_TYPE_EDEFAULT = IdType.STRING;

	/**
	 * The cached value of the '{@link #getIdType() <em>Id Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdType()
	 * @generated
	 * @ordered
	 */
	protected IdType idType = ID_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NodeIdImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AASPackage.Literals.NODE_ID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getNameSpaceIndex() {
		return nameSpaceIndex;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNameSpaceIndex(int newNameSpaceIndex) {
		int oldNameSpaceIndex = nameSpaceIndex;
		nameSpaceIndex = newNameSpaceIndex;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.NODE_ID__NAME_SPACE_INDEX, oldNameSpaceIndex, nameSpaceIndex));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIdentifier(String newIdentifier) {
		String oldIdentifier = identifier;
		identifier = newIdentifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.NODE_ID__IDENTIFIER, oldIdentifier, identifier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IdType getIdType() {
		return idType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIdType(IdType newIdType) {
		IdType oldIdType = idType;
		idType = newIdType == null ? ID_TYPE_EDEFAULT : newIdType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.NODE_ID__ID_TYPE, oldIdType, idType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AASPackage.NODE_ID__NAME_SPACE_INDEX:
				return getNameSpaceIndex();
			case AASPackage.NODE_ID__IDENTIFIER:
				return getIdentifier();
			case AASPackage.NODE_ID__ID_TYPE:
				return getIdType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AASPackage.NODE_ID__NAME_SPACE_INDEX:
				setNameSpaceIndex((Integer)newValue);
				return;
			case AASPackage.NODE_ID__IDENTIFIER:
				setIdentifier((String)newValue);
				return;
			case AASPackage.NODE_ID__ID_TYPE:
				setIdType((IdType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AASPackage.NODE_ID__NAME_SPACE_INDEX:
				setNameSpaceIndex(NAME_SPACE_INDEX_EDEFAULT);
				return;
			case AASPackage.NODE_ID__IDENTIFIER:
				setIdentifier(IDENTIFIER_EDEFAULT);
				return;
			case AASPackage.NODE_ID__ID_TYPE:
				setIdType(ID_TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AASPackage.NODE_ID__NAME_SPACE_INDEX:
				return nameSpaceIndex != NAME_SPACE_INDEX_EDEFAULT;
			case AASPackage.NODE_ID__IDENTIFIER:
				return IDENTIFIER_EDEFAULT == null ? identifier != null : !IDENTIFIER_EDEFAULT.equals(identifier);
			case AASPackage.NODE_ID__ID_TYPE:
				return idType != ID_TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (nameSpaceIndex: ");
		result.append(nameSpaceIndex);
		result.append(", identifier: ");
		result.append(identifier);
		result.append(", idType: ");
		result.append(idType);
		result.append(')');
		return result.toString();
	}

} //NodeIdImpl
