/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Has Kind</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.HasKind#getKind <em>Kind</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.HasKind#getBase_HasKind_Class <em>Base Has Kind Class</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.aas.AASPackage#getHasKind()
 * @model abstract="true"
 * @generated
 */
public interface HasKind extends EObject {
	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.aas.ModelingKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see org.eclipse.papyrus.aas.ModelingKind
	 * @see #setKind(ModelingKind)
	 * @see org.eclipse.papyrus.aas.AASPackage#getHasKind_Kind()
	 * @model ordered="false"
	 * @generated
	 */
	ModelingKind getKind();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.HasKind#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind</em>' attribute.
	 * @see org.eclipse.papyrus.aas.ModelingKind
	 * @see #getKind()
	 * @generated
	 */
	void setKind(ModelingKind value);

	/**
	 * Returns the value of the '<em><b>Base Has Kind Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Has Kind Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Has Kind Class</em>' reference.
	 * @see #setBase_HasKind_Class(org.eclipse.uml2.uml.Class)
	 * @see org.eclipse.papyrus.aas.AASPackage#getHasKind_Base_HasKind_Class()
	 * @model ordered="false"
	 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='base_Class'"
	 * @generated
	 */
	org.eclipse.uml2.uml.Class getBase_HasKind_Class();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.HasKind#getBase_HasKind_Class <em>Base Has Kind Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Has Kind Class</em>' reference.
	 * @see #getBase_HasKind_Class()
	 * @generated
	 */
	void setBase_HasKind_Class(org.eclipse.uml2.uml.Class value);

} // HasKind
