/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.papyrus.aas.AASPackage;
import org.eclipse.papyrus.aas.LangString;
import org.eclipse.papyrus.aas.MultiLanguageProperty;
import org.eclipse.papyrus.aas.Reference;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Multi Language Property</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.impl.MultiLanguagePropertyImpl#getValue <em>Value</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.MultiLanguagePropertyImpl#getValueId <em>Value Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MultiLanguagePropertyImpl extends DataElementImpl implements MultiLanguageProperty {
	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected EList<LangString> value;

	/**
	 * The cached value of the '{@link #getValueId() <em>Value Id</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueId()
	 * @generated
	 * @ordered
	 */
	protected Reference valueId;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MultiLanguagePropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AASPackage.Literals.MULTI_LANGUAGE_PROPERTY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<LangString> getValue() {
		if (value == null) {
			value = new EObjectContainmentEList<LangString>(LangString.class, this, AASPackage.MULTI_LANGUAGE_PROPERTY__VALUE);
		}
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Reference getValueId() {
		if (valueId != null && valueId.eIsProxy()) {
			InternalEObject oldValueId = (InternalEObject)valueId;
			valueId = (Reference)eResolveProxy(oldValueId);
			if (valueId != oldValueId) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AASPackage.MULTI_LANGUAGE_PROPERTY__VALUE_ID, oldValueId, valueId));
			}
		}
		return valueId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reference basicGetValueId() {
		return valueId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setValueId(Reference newValueId) {
		Reference oldValueId = valueId;
		valueId = newValueId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.MULTI_LANGUAGE_PROPERTY__VALUE_ID, oldValueId, valueId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AASPackage.MULTI_LANGUAGE_PROPERTY__VALUE:
				return ((InternalEList<?>)getValue()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AASPackage.MULTI_LANGUAGE_PROPERTY__VALUE:
				return getValue();
			case AASPackage.MULTI_LANGUAGE_PROPERTY__VALUE_ID:
				if (resolve) return getValueId();
				return basicGetValueId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AASPackage.MULTI_LANGUAGE_PROPERTY__VALUE:
				getValue().clear();
				getValue().addAll((Collection<? extends LangString>)newValue);
				return;
			case AASPackage.MULTI_LANGUAGE_PROPERTY__VALUE_ID:
				setValueId((Reference)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AASPackage.MULTI_LANGUAGE_PROPERTY__VALUE:
				getValue().clear();
				return;
			case AASPackage.MULTI_LANGUAGE_PROPERTY__VALUE_ID:
				setValueId((Reference)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AASPackage.MULTI_LANGUAGE_PROPERTY__VALUE:
				return value != null && !value.isEmpty();
			case AASPackage.MULTI_LANGUAGE_PROPERTY__VALUE_ID:
				return valueId != null;
		}
		return super.eIsSet(featureID);
	}

} //MultiLanguagePropertyImpl
