/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Asset Information</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * AssetInformation/globalAssetId either is a reference 
 * to an Asset object or a global reference.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.AssetInformation#getAssetKind <em>Asset Kind</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.AssetInformation#getGlobalAssetId <em>Global Asset Id</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.AssetInformation#getSpecificAssetId <em>Specific Asset Id</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.AssetInformation#getBillOfMaterial <em>Bill Of Material</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.AssetInformation#getDefaultThumbnail <em>Default Thumbnail</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.aas.AASPackage#getAssetInformation()
 * @model
 * @generated
 */
public interface AssetInformation extends EObject {
	/**
	 * Returns the value of the '<em><b>Asset Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.aas.AssetKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asset Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asset Kind</em>' attribute.
	 * @see org.eclipse.papyrus.aas.AssetKind
	 * @see #setAssetKind(AssetKind)
	 * @see org.eclipse.papyrus.aas.AASPackage#getAssetInformation_AssetKind()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	AssetKind getAssetKind();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.AssetInformation#getAssetKind <em>Asset Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Asset Kind</em>' attribute.
	 * @see org.eclipse.papyrus.aas.AssetKind
	 * @see #getAssetKind()
	 * @generated
	 */
	void setAssetKind(AssetKind value);

	/**
	 * Returns the value of the '<em><b>Global Asset Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Global Asset Id</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Global Asset Id</em>' reference.
	 * @see #setGlobalAssetId(Reference)
	 * @see org.eclipse.papyrus.aas.AASPackage#getAssetInformation_GlobalAssetId()
	 * @model ordered="false"
	 * @generated
	 */
	Reference getGlobalAssetId();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.AssetInformation#getGlobalAssetId <em>Global Asset Id</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Global Asset Id</em>' reference.
	 * @see #getGlobalAssetId()
	 * @generated
	 */
	void setGlobalAssetId(Reference value);

	/**
	 * Returns the value of the '<em><b>Specific Asset Id</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.aas.IdentifierKeyValuePair}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Specific Asset Id</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specific Asset Id</em>' reference list.
	 * @see org.eclipse.papyrus.aas.AASPackage#getAssetInformation_SpecificAssetId()
	 * @model ordered="false"
	 * @generated
	 */
	EList<IdentifierKeyValuePair> getSpecificAssetId();

	/**
	 * Returns the value of the '<em><b>Bill Of Material</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.aas.Submodel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bill Of Material</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bill Of Material</em>' reference list.
	 * @see org.eclipse.papyrus.aas.AASPackage#getAssetInformation_BillOfMaterial()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Submodel> getBillOfMaterial();

	/**
	 * Returns the value of the '<em><b>Default Thumbnail</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Thumbnail</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Thumbnail</em>' reference.
	 * @see #setDefaultThumbnail(File)
	 * @see org.eclipse.papyrus.aas.AASPackage#getAssetInformation_DefaultThumbnail()
	 * @model ordered="false"
	 * @generated
	 */
	File getDefaultThumbnail();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.AssetInformation#getDefaultThumbnail <em>Default Thumbnail</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Thumbnail</em>' reference.
	 * @see #getDefaultThumbnail()
	 * @generated
	 */
	void setDefaultThumbnail(File value);

} // AssetInformation
