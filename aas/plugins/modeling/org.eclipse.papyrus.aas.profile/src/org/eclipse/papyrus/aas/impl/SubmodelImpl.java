/**
 * Copyright (c) 2023 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 *   SPDX-License-Identifier: EPL-2.0
 *
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.papyrus.aas.AASPackage;
import org.eclipse.papyrus.aas.HasDataSpecification;
import org.eclipse.papyrus.aas.HasKind;
import org.eclipse.papyrus.aas.HasSemantics;
import org.eclipse.papyrus.aas.ModelingKind;
import org.eclipse.papyrus.aas.Reference;
import org.eclipse.papyrus.aas.Submodel;
import org.eclipse.papyrus.aas.SubmodelElement;
import org.eclipse.papyrus.aas.SubmodelElementCollection;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Property;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Submodel</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.impl.SubmodelImpl#getKind <em>Kind</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.SubmodelImpl#getBase_HasKind_Class <em>Base Has Kind Class</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.SubmodelImpl#getSemanticId <em>Semantic Id</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.SubmodelImpl#getBase_HasSemantics_Class <em>Base Has Semantics Class</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.SubmodelImpl#getDataSpecification <em>Data Specification</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.SubmodelImpl#getSubmodelelement <em>Submodelelement</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubmodelImpl extends IdentifiableImpl implements Submodel {
	/**
	 * The default value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected static final ModelingKind KIND_EDEFAULT = ModelingKind.TEMPLATE;

	/**
	 * The cached value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected ModelingKind kind = KIND_EDEFAULT;

	/**
	 * The cached value of the '{@link #getBase_HasKind_Class() <em>Base Has Kind Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_HasKind_Class()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Class base_HasKind_Class;

	/**
	 * The cached value of the '{@link #getSemanticId() <em>Semantic Id</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemanticId()
	 * @generated
	 * @ordered
	 */
	protected Reference semanticId;

	/**
	 * The cached value of the '{@link #getBase_HasSemantics_Class() <em>Base Has Semantics Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_HasSemantics_Class()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Class base_HasSemantics_Class;

	/**
	 * The cached value of the '{@link #getDataSpecification() <em>Data Specification</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataSpecification()
	 * @generated
	 * @ordered
	 */
	protected EList<Reference> dataSpecification;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubmodelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AASPackage.Literals.SUBMODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ModelingKind getKind() {
		return kind;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setKind(ModelingKind newKind) {
		ModelingKind oldKind = kind;
		kind = newKind == null ? KIND_EDEFAULT : newKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.SUBMODEL__KIND, oldKind, kind));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public org.eclipse.uml2.uml.Class getBase_HasKind_Class() {
		if (base_HasKind_Class != null && base_HasKind_Class.eIsProxy()) {
			InternalEObject oldBase_HasKind_Class = (InternalEObject)base_HasKind_Class;
			base_HasKind_Class = (org.eclipse.uml2.uml.Class)eResolveProxy(oldBase_HasKind_Class);
			if (base_HasKind_Class != oldBase_HasKind_Class) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AASPackage.SUBMODEL__BASE_HAS_KIND_CLASS, oldBase_HasKind_Class, base_HasKind_Class));
			}
		}
		return base_HasKind_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class basicGetBase_HasKind_Class() {
		return base_HasKind_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_HasKind_Class(org.eclipse.uml2.uml.Class newBase_HasKind_Class) {
		org.eclipse.uml2.uml.Class oldBase_HasKind_Class = base_HasKind_Class;
		base_HasKind_Class = newBase_HasKind_Class;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.SUBMODEL__BASE_HAS_KIND_CLASS, oldBase_HasKind_Class, base_HasKind_Class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Reference getSemanticId() {
		if (semanticId != null && semanticId.eIsProxy()) {
			InternalEObject oldSemanticId = (InternalEObject)semanticId;
			semanticId = (Reference)eResolveProxy(oldSemanticId);
			if (semanticId != oldSemanticId) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AASPackage.SUBMODEL__SEMANTIC_ID, oldSemanticId, semanticId));
			}
		}
		return semanticId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reference basicGetSemanticId() {
		return semanticId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSemanticId(Reference newSemanticId) {
		Reference oldSemanticId = semanticId;
		semanticId = newSemanticId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.SUBMODEL__SEMANTIC_ID, oldSemanticId, semanticId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public org.eclipse.uml2.uml.Class getBase_HasSemantics_Class() {
		if (base_HasSemantics_Class != null && base_HasSemantics_Class.eIsProxy()) {
			InternalEObject oldBase_HasSemantics_Class = (InternalEObject)base_HasSemantics_Class;
			base_HasSemantics_Class = (org.eclipse.uml2.uml.Class)eResolveProxy(oldBase_HasSemantics_Class);
			if (base_HasSemantics_Class != oldBase_HasSemantics_Class) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AASPackage.SUBMODEL__BASE_HAS_SEMANTICS_CLASS, oldBase_HasSemantics_Class, base_HasSemantics_Class));
			}
		}
		return base_HasSemantics_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class basicGetBase_HasSemantics_Class() {
		return base_HasSemantics_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_HasSemantics_Class(org.eclipse.uml2.uml.Class newBase_HasSemantics_Class) {
		org.eclipse.uml2.uml.Class oldBase_HasSemantics_Class = base_HasSemantics_Class;
		base_HasSemantics_Class = newBase_HasSemantics_Class;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.SUBMODEL__BASE_HAS_SEMANTICS_CLASS, oldBase_HasSemantics_Class, base_HasSemantics_Class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Reference> getDataSpecification() {
		if (dataSpecification == null) {
			dataSpecification = new EObjectResolvingEList<Reference>(Reference.class, this, AASPackage.SUBMODEL__DATA_SPECIFICATION);
		}
		return dataSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SubmodelElement> getSubmodelelement() {
		// get all SubmodelElementCollection and add them to the list of submodelElements
						EList<SubmodelElement> submodelElements = new org.eclipse.emf.common.util.BasicEList<>();
						if (getBase_Class() != null) {
							org.eclipse.uml2.uml.Class base_class = getBase_Class();
				
							// get all SECs and add them to the list of submodelElements
							EList<Classifier> allNestedClassifier = base_class.getNestedClassifiers();
							java.util.Iterator<org.eclipse.uml2.uml.Classifier> it = allNestedClassifier.iterator();
							while (it.hasNext()) {
								Classifier classifier = it.next();
								if (org.eclipse.uml2.uml.util.UMLUtil.getStereotypeApplication(classifier, org.eclipse.papyrus.aas.SubmodelElementCollection.class) != null) {
				
									SubmodelElementCollection sec = org.eclipse.uml2.uml.util.UMLUtil.getStereotypeApplication(classifier, org.eclipse.papyrus.aas.SubmodelElementCollection.class);
									submodelElements.add(sec);
								}
							}
				
							// get all submodelElements (stereotyped attributes) and add them to the list of submodelElements
							EList<org.eclipse.uml2.uml.Property> allAttributes = base_class.getAllAttributes();
							java.util.Iterator<Property> p_it = allAttributes.iterator();
							while (p_it.hasNext()) {
								Property property = p_it.next();
								if (org.eclipse.papyrus.uml.tools.utils.UMLUtil.getAppliedStereotype(property, "AAS::SubmodelElement", false) != null) {
									SubmodelElement child = org.eclipse.uml2.uml.util.UMLUtil.getStereotypeApplication(property, org.eclipse.papyrus.aas.SubmodelElement.class);
									submodelElements.add(child);
				
								}
							}
				
							// get all Operations and add them to the list of submodelElements
							EList<org.eclipse.uml2.uml.Operation> allOperations = base_class.getAllOperations();
							java.util.Iterator<org.eclipse.uml2.uml.Operation> o_it = allOperations.iterator();
							while (o_it.hasNext()) {
								org.eclipse.uml2.uml.Operation operation = o_it.next();
								if (org.eclipse.uml2.uml.util.UMLUtil.getStereotypeApplication(operation, org.eclipse.papyrus.aas.Operation.class) != null) {
				
									org.eclipse.papyrus.aas.Operation op = org.eclipse.uml2.uml.util.UMLUtil.getStereotypeApplication(operation, org.eclipse.papyrus.aas.Operation.class);
									submodelElements.add(op);
								}
							}
				
				
						}
						return new org.eclipse.emf.ecore.util.EcoreEList.UnmodifiableEList<>(this, AASPackage.eINSTANCE.getSubmodel_Submodelelement(), submodelElements.size(), submodelElements.toArray());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AASPackage.SUBMODEL__KIND:
				return getKind();
			case AASPackage.SUBMODEL__BASE_HAS_KIND_CLASS:
				if (resolve) return getBase_HasKind_Class();
				return basicGetBase_HasKind_Class();
			case AASPackage.SUBMODEL__SEMANTIC_ID:
				if (resolve) return getSemanticId();
				return basicGetSemanticId();
			case AASPackage.SUBMODEL__BASE_HAS_SEMANTICS_CLASS:
				if (resolve) return getBase_HasSemantics_Class();
				return basicGetBase_HasSemantics_Class();
			case AASPackage.SUBMODEL__DATA_SPECIFICATION:
				return getDataSpecification();
			case AASPackage.SUBMODEL__SUBMODELELEMENT:
				return getSubmodelelement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AASPackage.SUBMODEL__KIND:
				setKind((ModelingKind)newValue);
				return;
			case AASPackage.SUBMODEL__BASE_HAS_KIND_CLASS:
				setBase_HasKind_Class((org.eclipse.uml2.uml.Class)newValue);
				return;
			case AASPackage.SUBMODEL__SEMANTIC_ID:
				setSemanticId((Reference)newValue);
				return;
			case AASPackage.SUBMODEL__BASE_HAS_SEMANTICS_CLASS:
				setBase_HasSemantics_Class((org.eclipse.uml2.uml.Class)newValue);
				return;
			case AASPackage.SUBMODEL__DATA_SPECIFICATION:
				getDataSpecification().clear();
				getDataSpecification().addAll((Collection<? extends Reference>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AASPackage.SUBMODEL__KIND:
				setKind(KIND_EDEFAULT);
				return;
			case AASPackage.SUBMODEL__BASE_HAS_KIND_CLASS:
				setBase_HasKind_Class((org.eclipse.uml2.uml.Class)null);
				return;
			case AASPackage.SUBMODEL__SEMANTIC_ID:
				setSemanticId((Reference)null);
				return;
			case AASPackage.SUBMODEL__BASE_HAS_SEMANTICS_CLASS:
				setBase_HasSemantics_Class((org.eclipse.uml2.uml.Class)null);
				return;
			case AASPackage.SUBMODEL__DATA_SPECIFICATION:
				getDataSpecification().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AASPackage.SUBMODEL__KIND:
				return kind != KIND_EDEFAULT;
			case AASPackage.SUBMODEL__BASE_HAS_KIND_CLASS:
				return base_HasKind_Class != null;
			case AASPackage.SUBMODEL__SEMANTIC_ID:
				return semanticId != null;
			case AASPackage.SUBMODEL__BASE_HAS_SEMANTICS_CLASS:
				return base_HasSemantics_Class != null;
			case AASPackage.SUBMODEL__DATA_SPECIFICATION:
				return dataSpecification != null && !dataSpecification.isEmpty();
			case AASPackage.SUBMODEL__SUBMODELELEMENT:
				return !getSubmodelelement().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == HasKind.class) {
			switch (derivedFeatureID) {
				case AASPackage.SUBMODEL__KIND: return AASPackage.HAS_KIND__KIND;
				case AASPackage.SUBMODEL__BASE_HAS_KIND_CLASS: return AASPackage.HAS_KIND__BASE_HAS_KIND_CLASS;
				default: return -1;
			}
		}
		if (baseClass == HasSemantics.class) {
			switch (derivedFeatureID) {
				case AASPackage.SUBMODEL__SEMANTIC_ID: return AASPackage.HAS_SEMANTICS__SEMANTIC_ID;
				case AASPackage.SUBMODEL__BASE_HAS_SEMANTICS_CLASS: return AASPackage.HAS_SEMANTICS__BASE_HAS_SEMANTICS_CLASS;
				default: return -1;
			}
		}
		if (baseClass == HasDataSpecification.class) {
			switch (derivedFeatureID) {
				case AASPackage.SUBMODEL__DATA_SPECIFICATION: return AASPackage.HAS_DATA_SPECIFICATION__DATA_SPECIFICATION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == HasKind.class) {
			switch (baseFeatureID) {
				case AASPackage.HAS_KIND__KIND: return AASPackage.SUBMODEL__KIND;
				case AASPackage.HAS_KIND__BASE_HAS_KIND_CLASS: return AASPackage.SUBMODEL__BASE_HAS_KIND_CLASS;
				default: return -1;
			}
		}
		if (baseClass == HasSemantics.class) {
			switch (baseFeatureID) {
				case AASPackage.HAS_SEMANTICS__SEMANTIC_ID: return AASPackage.SUBMODEL__SEMANTIC_ID;
				case AASPackage.HAS_SEMANTICS__BASE_HAS_SEMANTICS_CLASS: return AASPackage.SUBMODEL__BASE_HAS_SEMANTICS_CLASS;
				default: return -1;
			}
		}
		if (baseClass == HasDataSpecification.class) {
			switch (baseFeatureID) {
				case AASPackage.HAS_DATA_SPECIFICATION__DATA_SPECIFICATION: return AASPackage.SUBMODEL__DATA_SPECIFICATION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (kind: ");
		result.append(kind);
		result.append(')');
		return result.toString();
	}

} // SubmodelImpl
