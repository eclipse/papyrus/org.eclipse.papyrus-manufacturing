/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Identifier Key Value Pair</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.IdentifierKeyValuePair#getKey <em>Key</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.IdentifierKeyValuePair#getValue <em>Value</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.IdentifierKeyValuePair#getExternalSubjectId <em>External Subject Id</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.aas.AASPackage#getIdentifierKeyValuePair()
 * @model
 * @generated
 */
public interface IdentifierKeyValuePair extends HasSemantics {
	/**
	 * Returns the value of the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' attribute.
	 * @see #setKey(String)
	 * @see org.eclipse.papyrus.aas.AASPackage#getIdentifierKeyValuePair_Key()
	 * @model dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
	 * @generated
	 */
	String getKey();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.IdentifierKeyValuePair#getKey <em>Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' attribute.
	 * @see #getKey()
	 * @generated
	 */
	void setKey(String value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see org.eclipse.papyrus.aas.AASPackage#getIdentifierKeyValuePair_Value()
	 * @model dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.IdentifierKeyValuePair#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>External Subject Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External Subject Id</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Subject Id</em>' reference.
	 * @see #setExternalSubjectId(Reference)
	 * @see org.eclipse.papyrus.aas.AASPackage#getIdentifierKeyValuePair_ExternalSubjectId()
	 * @model ordered="false"
	 * @generated
	 */
	Reference getExternalSubjectId();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.IdentifierKeyValuePair#getExternalSubjectId <em>External Subject Id</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>External Subject Id</em>' reference.
	 * @see #getExternalSubjectId()
	 * @generated
	 */
	void setExternalSubjectId(Reference value);

} // IdentifierKeyValuePair
