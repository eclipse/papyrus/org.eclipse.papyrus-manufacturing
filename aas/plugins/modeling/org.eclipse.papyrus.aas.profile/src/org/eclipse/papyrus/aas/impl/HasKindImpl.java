/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.papyrus.aas.AASPackage;
import org.eclipse.papyrus.aas.HasKind;
import org.eclipse.papyrus.aas.ModelingKind;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Has Kind</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.impl.HasKindImpl#getKind <em>Kind</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.HasKindImpl#getBase_HasKind_Class <em>Base Has Kind Class</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class HasKindImpl extends MinimalEObjectImpl.Container implements HasKind {
	/**
	 * The default value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected static final ModelingKind KIND_EDEFAULT = ModelingKind.TEMPLATE;

	/**
	 * The cached value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected ModelingKind kind = KIND_EDEFAULT;

	/**
	 * The cached value of the '{@link #getBase_HasKind_Class() <em>Base Has Kind Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_HasKind_Class()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Class base_HasKind_Class;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HasKindImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AASPackage.Literals.HAS_KIND;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ModelingKind getKind() {
		return kind;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setKind(ModelingKind newKind) {
		ModelingKind oldKind = kind;
		kind = newKind == null ? KIND_EDEFAULT : newKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.HAS_KIND__KIND, oldKind, kind));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public org.eclipse.uml2.uml.Class getBase_HasKind_Class() {
		if (base_HasKind_Class != null && base_HasKind_Class.eIsProxy()) {
			InternalEObject oldBase_HasKind_Class = (InternalEObject)base_HasKind_Class;
			base_HasKind_Class = (org.eclipse.uml2.uml.Class)eResolveProxy(oldBase_HasKind_Class);
			if (base_HasKind_Class != oldBase_HasKind_Class) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AASPackage.HAS_KIND__BASE_HAS_KIND_CLASS, oldBase_HasKind_Class, base_HasKind_Class));
			}
		}
		return base_HasKind_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class basicGetBase_HasKind_Class() {
		return base_HasKind_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_HasKind_Class(org.eclipse.uml2.uml.Class newBase_HasKind_Class) {
		org.eclipse.uml2.uml.Class oldBase_HasKind_Class = base_HasKind_Class;
		base_HasKind_Class = newBase_HasKind_Class;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.HAS_KIND__BASE_HAS_KIND_CLASS, oldBase_HasKind_Class, base_HasKind_Class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AASPackage.HAS_KIND__KIND:
				return getKind();
			case AASPackage.HAS_KIND__BASE_HAS_KIND_CLASS:
				if (resolve) return getBase_HasKind_Class();
				return basicGetBase_HasKind_Class();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AASPackage.HAS_KIND__KIND:
				setKind((ModelingKind)newValue);
				return;
			case AASPackage.HAS_KIND__BASE_HAS_KIND_CLASS:
				setBase_HasKind_Class((org.eclipse.uml2.uml.Class)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AASPackage.HAS_KIND__KIND:
				setKind(KIND_EDEFAULT);
				return;
			case AASPackage.HAS_KIND__BASE_HAS_KIND_CLASS:
				setBase_HasKind_Class((org.eclipse.uml2.uml.Class)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AASPackage.HAS_KIND__KIND:
				return kind != KIND_EDEFAULT;
			case AASPackage.HAS_KIND__BASE_HAS_KIND_CLASS:
				return base_HasKind_Class != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (kind: ");
		result.append(kind);
		result.append(')');
		return result.toString();
	}

} //HasKindImpl
