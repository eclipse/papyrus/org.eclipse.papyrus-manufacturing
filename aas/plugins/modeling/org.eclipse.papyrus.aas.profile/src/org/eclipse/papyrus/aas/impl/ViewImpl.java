/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.papyrus.aas.AASPackage;
import org.eclipse.papyrus.aas.HasDataSpecification;
import org.eclipse.papyrus.aas.HasSemantics;
import org.eclipse.papyrus.aas.Referable;
import org.eclipse.papyrus.aas.Reference;
import org.eclipse.papyrus.aas.View;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>View</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.impl.ViewImpl#getSemanticId <em>Semantic Id</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.ViewImpl#getBase_HasSemantics_Class <em>Base Has Semantics Class</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.ViewImpl#getDataSpecification <em>Data Specification</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.ViewImpl#getContainedElement <em>Contained Element</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ViewImpl extends ReferableImpl implements View {
	/**
	 * The cached value of the '{@link #getSemanticId() <em>Semantic Id</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemanticId()
	 * @generated
	 * @ordered
	 */
	protected Reference semanticId;

	/**
	 * The cached value of the '{@link #getBase_HasSemantics_Class() <em>Base Has Semantics Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_HasSemantics_Class()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Class base_HasSemantics_Class;

	/**
	 * The cached value of the '{@link #getDataSpecification() <em>Data Specification</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataSpecification()
	 * @generated
	 * @ordered
	 */
	protected EList<Reference> dataSpecification;

	/**
	 * The cached value of the '{@link #getContainedElement() <em>Contained Element</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainedElement()
	 * @generated
	 * @ordered
	 */
	protected EList<Referable> containedElement;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ViewImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AASPackage.Literals.VIEW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Reference getSemanticId() {
		if (semanticId != null && semanticId.eIsProxy()) {
			InternalEObject oldSemanticId = (InternalEObject)semanticId;
			semanticId = (Reference)eResolveProxy(oldSemanticId);
			if (semanticId != oldSemanticId) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AASPackage.VIEW__SEMANTIC_ID, oldSemanticId, semanticId));
			}
		}
		return semanticId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reference basicGetSemanticId() {
		return semanticId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSemanticId(Reference newSemanticId) {
		Reference oldSemanticId = semanticId;
		semanticId = newSemanticId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.VIEW__SEMANTIC_ID, oldSemanticId, semanticId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public org.eclipse.uml2.uml.Class getBase_HasSemantics_Class() {
		if (base_HasSemantics_Class != null && base_HasSemantics_Class.eIsProxy()) {
			InternalEObject oldBase_HasSemantics_Class = (InternalEObject)base_HasSemantics_Class;
			base_HasSemantics_Class = (org.eclipse.uml2.uml.Class)eResolveProxy(oldBase_HasSemantics_Class);
			if (base_HasSemantics_Class != oldBase_HasSemantics_Class) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AASPackage.VIEW__BASE_HAS_SEMANTICS_CLASS, oldBase_HasSemantics_Class, base_HasSemantics_Class));
			}
		}
		return base_HasSemantics_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class basicGetBase_HasSemantics_Class() {
		return base_HasSemantics_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_HasSemantics_Class(org.eclipse.uml2.uml.Class newBase_HasSemantics_Class) {
		org.eclipse.uml2.uml.Class oldBase_HasSemantics_Class = base_HasSemantics_Class;
		base_HasSemantics_Class = newBase_HasSemantics_Class;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.VIEW__BASE_HAS_SEMANTICS_CLASS, oldBase_HasSemantics_Class, base_HasSemantics_Class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Reference> getDataSpecification() {
		if (dataSpecification == null) {
			dataSpecification = new EObjectResolvingEList<Reference>(Reference.class, this, AASPackage.VIEW__DATA_SPECIFICATION);
		}
		return dataSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Referable> getContainedElement() {
		if (containedElement == null) {
			containedElement = new EObjectResolvingEList<Referable>(Referable.class, this, AASPackage.VIEW__CONTAINED_ELEMENT);
		}
		return containedElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AASPackage.VIEW__SEMANTIC_ID:
				if (resolve) return getSemanticId();
				return basicGetSemanticId();
			case AASPackage.VIEW__BASE_HAS_SEMANTICS_CLASS:
				if (resolve) return getBase_HasSemantics_Class();
				return basicGetBase_HasSemantics_Class();
			case AASPackage.VIEW__DATA_SPECIFICATION:
				return getDataSpecification();
			case AASPackage.VIEW__CONTAINED_ELEMENT:
				return getContainedElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AASPackage.VIEW__SEMANTIC_ID:
				setSemanticId((Reference)newValue);
				return;
			case AASPackage.VIEW__BASE_HAS_SEMANTICS_CLASS:
				setBase_HasSemantics_Class((org.eclipse.uml2.uml.Class)newValue);
				return;
			case AASPackage.VIEW__DATA_SPECIFICATION:
				getDataSpecification().clear();
				getDataSpecification().addAll((Collection<? extends Reference>)newValue);
				return;
			case AASPackage.VIEW__CONTAINED_ELEMENT:
				getContainedElement().clear();
				getContainedElement().addAll((Collection<? extends Referable>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AASPackage.VIEW__SEMANTIC_ID:
				setSemanticId((Reference)null);
				return;
			case AASPackage.VIEW__BASE_HAS_SEMANTICS_CLASS:
				setBase_HasSemantics_Class((org.eclipse.uml2.uml.Class)null);
				return;
			case AASPackage.VIEW__DATA_SPECIFICATION:
				getDataSpecification().clear();
				return;
			case AASPackage.VIEW__CONTAINED_ELEMENT:
				getContainedElement().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AASPackage.VIEW__SEMANTIC_ID:
				return semanticId != null;
			case AASPackage.VIEW__BASE_HAS_SEMANTICS_CLASS:
				return base_HasSemantics_Class != null;
			case AASPackage.VIEW__DATA_SPECIFICATION:
				return dataSpecification != null && !dataSpecification.isEmpty();
			case AASPackage.VIEW__CONTAINED_ELEMENT:
				return containedElement != null && !containedElement.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == HasSemantics.class) {
			switch (derivedFeatureID) {
				case AASPackage.VIEW__SEMANTIC_ID: return AASPackage.HAS_SEMANTICS__SEMANTIC_ID;
				case AASPackage.VIEW__BASE_HAS_SEMANTICS_CLASS: return AASPackage.HAS_SEMANTICS__BASE_HAS_SEMANTICS_CLASS;
				default: return -1;
			}
		}
		if (baseClass == HasDataSpecification.class) {
			switch (derivedFeatureID) {
				case AASPackage.VIEW__DATA_SPECIFICATION: return AASPackage.HAS_DATA_SPECIFICATION__DATA_SPECIFICATION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == HasSemantics.class) {
			switch (baseFeatureID) {
				case AASPackage.HAS_SEMANTICS__SEMANTIC_ID: return AASPackage.VIEW__SEMANTIC_ID;
				case AASPackage.HAS_SEMANTICS__BASE_HAS_SEMANTICS_CLASS: return AASPackage.VIEW__BASE_HAS_SEMANTICS_CLASS;
				default: return -1;
			}
		}
		if (baseClass == HasDataSpecification.class) {
			switch (baseFeatureID) {
				case AASPackage.HAS_DATA_SPECIFICATION__DATA_SPECIFICATION: return AASPackage.VIEW__DATA_SPECIFICATION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //ViewImpl
