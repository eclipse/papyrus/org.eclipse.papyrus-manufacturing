/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas;

import org.eclipse.uml2.uml.Element;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Basic Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.BasicEvent#getObserved <em>Observed</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.aas.AASPackage#getBasicEvent()
 * @model
 * @generated
 */
public interface BasicEvent extends Event {
	/**
	 * Returns the value of the '<em><b>Observed</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Observed</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Observed</em>' reference.
	 * @see #setObserved(Element)
	 * @see org.eclipse.papyrus.aas.AASPackage#getBasicEvent_Observed()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Element getObserved();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.BasicEvent#getObserved <em>Observed</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Observed</em>' reference.
	 * @see #getObserved()
	 * @generated
	 */
	void setObserved(Element value);

} // BasicEvent
