/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>File</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A File is a data element that represents an address to a file. The value is an URI that can represent an absolute or relative path.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.File#getPath <em>Path</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.File#getMimeType <em>Mime Type</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.aas.AASPackage#getFile()
 * @model
 * @generated
 */
public interface File extends DataElement {
	/**
	 * Returns the value of the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Path</em>' attribute.
	 * @see #setPath(String)
	 * @see org.eclipse.papyrus.aas.AASPackage#getFile_Path()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getPath();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.File#getPath <em>Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Path</em>' attribute.
	 * @see #getPath()
	 * @generated
	 */
	void setPath(String value);

	/**
	 * Returns the value of the '<em><b>Mime Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.aas.MimeType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mime Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mime Type</em>' attribute.
	 * @see org.eclipse.papyrus.aas.MimeType
	 * @see #setMimeType(MimeType)
	 * @see org.eclipse.papyrus.aas.AASPackage#getFile_MimeType()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	MimeType getMimeType();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.File#getMimeType <em>Mime Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mime Type</em>' attribute.
	 * @see org.eclipse.papyrus.aas.MimeType
	 * @see #getMimeType()
	 * @generated
	 */
	void setMimeType(MimeType value);

} // File
