/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.uml2.uml.ValueSpecification;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Specification IEC61360</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getBase_Class <em>Base Class</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getPreferredName <em>Preferred Name</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getShortName <em>Short Name</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getUnit <em>Unit</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getUnitId <em>Unit Id</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getSourceOfDefinition <em>Source Of Definition</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getSymbol <em>Symbol</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getDataType <em>Data Type</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getDefinition <em>Definition</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getValueFormat <em>Value Format</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getValueList <em>Value List</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getValue <em>Value</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getValueId <em>Value Id</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getLevelType <em>Level Type</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.aas.AASPackage#getDataSpecificationIEC61360()
 * @model
 * @generated
 */
public interface DataSpecificationIEC61360 extends EObject {
	/**
	 * Returns the value of the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Class</em>' reference.
	 * @see #setBase_Class(org.eclipse.uml2.uml.Class)
	 * @see org.eclipse.papyrus.aas.AASPackage#getDataSpecificationIEC61360_Base_Class()
	 * @model ordered="false"
	 * @generated
	 */
	org.eclipse.uml2.uml.Class getBase_Class();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getBase_Class <em>Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Class</em>' reference.
	 * @see #getBase_Class()
	 * @generated
	 */
	void setBase_Class(org.eclipse.uml2.uml.Class value);

	/**
	 * Returns the value of the '<em><b>Preferred Name</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.aas.LangString}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preferred Name</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preferred Name</em>' containment reference list.
	 * @see org.eclipse.papyrus.aas.AASPackage#getDataSpecificationIEC61360_PreferredName()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	EList<LangString> getPreferredName();

	/**
	 * Returns the value of the '<em><b>Short Name</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.aas.LangString}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Short Name</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Short Name</em>' containment reference list.
	 * @see org.eclipse.papyrus.aas.AASPackage#getDataSpecificationIEC61360_ShortName()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<LangString> getShortName();

	/**
	 * Returns the value of the '<em><b>Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit</em>' attribute.
	 * @see #setUnit(String)
	 * @see org.eclipse.papyrus.aas.AASPackage#getDataSpecificationIEC61360_Unit()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getUnit();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getUnit <em>Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit</em>' attribute.
	 * @see #getUnit()
	 * @generated
	 */
	void setUnit(String value);

	/**
	 * Returns the value of the '<em><b>Unit Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit Id</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit Id</em>' reference.
	 * @see #setUnitId(Reference)
	 * @see org.eclipse.papyrus.aas.AASPackage#getDataSpecificationIEC61360_UnitId()
	 * @model ordered="false"
	 * @generated
	 */
	Reference getUnitId();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getUnitId <em>Unit Id</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit Id</em>' reference.
	 * @see #getUnitId()
	 * @generated
	 */
	void setUnitId(Reference value);

	/**
	 * Returns the value of the '<em><b>Source Of Definition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Of Definition</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Of Definition</em>' attribute.
	 * @see #setSourceOfDefinition(String)
	 * @see org.eclipse.papyrus.aas.AASPackage#getDataSpecificationIEC61360_SourceOfDefinition()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getSourceOfDefinition();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getSourceOfDefinition <em>Source Of Definition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Of Definition</em>' attribute.
	 * @see #getSourceOfDefinition()
	 * @generated
	 */
	void setSourceOfDefinition(String value);

	/**
	 * Returns the value of the '<em><b>Symbol</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Symbol</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symbol</em>' attribute.
	 * @see #setSymbol(String)
	 * @see org.eclipse.papyrus.aas.AASPackage#getDataSpecificationIEC61360_Symbol()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getSymbol();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getSymbol <em>Symbol</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Symbol</em>' attribute.
	 * @see #getSymbol()
	 * @generated
	 */
	void setSymbol(String value);

	/**
	 * Returns the value of the '<em><b>Data Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Type</em>' reference.
	 * @see #setDataType(DataSpecificationIEC61360)
	 * @see org.eclipse.papyrus.aas.AASPackage#getDataSpecificationIEC61360_DataType()
	 * @model ordered="false"
	 * @generated
	 */
	DataSpecificationIEC61360 getDataType();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getDataType <em>Data Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Type</em>' reference.
	 * @see #getDataType()
	 * @generated
	 */
	void setDataType(DataSpecificationIEC61360 value);

	/**
	 * Returns the value of the '<em><b>Definition</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.aas.LangString}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definition</em>' containment reference list.
	 * @see org.eclipse.papyrus.aas.AASPackage#getDataSpecificationIEC61360_Definition()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<LangString> getDefinition();

	/**
	 * Returns the value of the '<em><b>Value Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Format</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Format</em>' attribute.
	 * @see #setValueFormat(String)
	 * @see org.eclipse.papyrus.aas.AASPackage#getDataSpecificationIEC61360_ValueFormat()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getValueFormat();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getValueFormat <em>Value Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Format</em>' attribute.
	 * @see #getValueFormat()
	 * @generated
	 */
	void setValueFormat(String value);

	/**
	 * Returns the value of the '<em><b>Value List</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.aas.ValueReferencePairType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value List</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value List</em>' containment reference list.
	 * @see org.eclipse.papyrus.aas.AASPackage#getDataSpecificationIEC61360_ValueList()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<ValueReferencePairType> getValueList();

	/**
	 * Returns the value of the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' reference.
	 * @see #setValue(ValueSpecification)
	 * @see org.eclipse.papyrus.aas.AASPackage#getDataSpecificationIEC61360_Value()
	 * @model ordered="false"
	 * @generated
	 */
	ValueSpecification getValue();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getValue <em>Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(ValueSpecification value);

	/**
	 * Returns the value of the '<em><b>Value Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Id</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Id</em>' reference.
	 * @see #setValueId(Reference)
	 * @see org.eclipse.papyrus.aas.AASPackage#getDataSpecificationIEC61360_ValueId()
	 * @model ordered="false"
	 * @generated
	 */
	Reference getValueId();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getValueId <em>Value Id</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Id</em>' reference.
	 * @see #getValueId()
	 * @generated
	 */
	void setValueId(Reference value);

	/**
	 * Returns the value of the '<em><b>Level Type</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.papyrus.aas.LevelType}.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.aas.LevelType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Level Type</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Level Type</em>' attribute list.
	 * @see org.eclipse.papyrus.aas.LevelType
	 * @see org.eclipse.papyrus.aas.AASPackage#getDataSpecificationIEC61360_LevelType()
	 * @model ordered="false"
	 * @generated
	 */
	EList<LevelType> getLevelType();

} // DataSpecificationIEC61360
