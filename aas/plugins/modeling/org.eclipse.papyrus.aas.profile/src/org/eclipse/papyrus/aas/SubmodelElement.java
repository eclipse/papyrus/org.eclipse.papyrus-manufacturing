/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Submodel Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.SubmodelElement#isDynamic <em>Is Dynamic</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.SubmodelElement#getEndPoint <em>End Point</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.SubmodelElement#getNodeId <em>Node Id</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.aas.AASPackage#getSubmodelElement()
 * @model abstract="true"
 * @generated
 */
public interface SubmodelElement extends Referable, HasKind, HasSemantics, HasDataSpecification {
	/**
	 * Returns the value of the '<em><b>Is Dynamic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Dynamic</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Dynamic</em>' attribute.
	 * @see #setIsDynamic(boolean)
	 * @see org.eclipse.papyrus.aas.AASPackage#getSubmodelElement_IsDynamic()
	 * @model dataType="org.eclipse.uml2.types.Boolean" required="true" ordered="false"
	 * @generated
	 */
	boolean isDynamic();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.SubmodelElement#isDynamic <em>Is Dynamic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Dynamic</em>' attribute.
	 * @see #isDynamic()
	 * @generated
	 */
	void setIsDynamic(boolean value);

	/**
	 * Returns the value of the '<em><b>End Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Point</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Point</em>' containment reference.
	 * @see #setEndPoint(Endpoint)
	 * @see org.eclipse.papyrus.aas.AASPackage#getSubmodelElement_EndPoint()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	Endpoint getEndPoint();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.SubmodelElement#getEndPoint <em>End Point</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Point</em>' containment reference.
	 * @see #getEndPoint()
	 * @generated
	 */
	void setEndPoint(Endpoint value);

	/**
	 * Returns the value of the '<em><b>Node Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node Id</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node Id</em>' containment reference.
	 * @see #setNodeId(NodeId)
	 * @see org.eclipse.papyrus.aas.AASPackage#getSubmodelElement_NodeId()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	NodeId getNodeId();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.SubmodelElement#getNodeId <em>Node Id</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Node Id</em>' containment reference.
	 * @see #getNodeId()
	 * @generated
	 */
	void setNodeId(NodeId value);

} // SubmodelElement
