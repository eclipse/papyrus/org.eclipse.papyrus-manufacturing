/**
 * Copyright (c) 2023 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 *   SPDX-License-Identifier: EPL-2.0
 *
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas.impl;

import java.util.Collection;
import java.util.Iterator;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEList.UnmodifiableEList;
import org.eclipse.papyrus.aas.AASEndpoint;
import org.eclipse.papyrus.aas.AASPackage;
import org.eclipse.papyrus.aas.Asset;
import org.eclipse.papyrus.aas.AssetAdministrationShell;
import org.eclipse.papyrus.aas.AssetInformation;
import org.eclipse.papyrus.aas.HasDataSpecification;
import org.eclipse.papyrus.aas.Reference;
import org.eclipse.papyrus.aas.Security;
import org.eclipse.papyrus.aas.Submodel;
import org.eclipse.uml2.uml.Classifier;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Asset Administration Shell</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.impl.AssetAdministrationShellImpl#getDataSpecification <em>Data Specification</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.AssetAdministrationShellImpl#getDerivedFrom <em>Derived From</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.AssetAdministrationShellImpl#getSecurity <em>Security</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.AssetAdministrationShellImpl#getAssetInformation <em>Asset Information</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.AssetAdministrationShellImpl#getAsset <em>Asset</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.AssetAdministrationShellImpl#getSubmodel <em>Submodel</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.AssetAdministrationShellImpl#getEndpoint <em>Endpoint</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AssetAdministrationShellImpl extends IdentifiableImpl implements AssetAdministrationShell {
	/**
	 * The cached value of the '{@link #getDataSpecification() <em>Data Specification</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataSpecification()
	 * @generated
	 * @ordered
	 */
	protected EList<Reference> dataSpecification;

	/**
	 * The cached value of the '{@link #getDerivedFrom() <em>Derived From</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedFrom()
	 * @generated
	 * @ordered
	 */
	protected AssetAdministrationShell derivedFrom;

	/**
	 * The cached value of the '{@link #getSecurity() <em>Security</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSecurity()
	 * @generated
	 * @ordered
	 */
	protected Security security;

	/**
	 * The cached value of the '{@link #getAssetInformation() <em>Asset Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssetInformation()
	 * @generated
	 * @ordered
	 */
	protected AssetInformation assetInformation;

	/**
	 * The cached value of the '{@link #getAsset() <em>Asset</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsset()
	 * @generated
	 * @ordered
	 */
	protected Asset asset;

	/**
	 * The cached value of the '{@link #getEndpoint() <em>Endpoint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndpoint()
	 * @generated
	 * @ordered
	 */
	protected AASEndpoint endpoint;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssetAdministrationShellImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AASPackage.Literals.ASSET_ADMINISTRATION_SHELL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Reference> getDataSpecification() {
		if (dataSpecification == null) {
			dataSpecification = new EObjectResolvingEList<Reference>(Reference.class, this, AASPackage.ASSET_ADMINISTRATION_SHELL__DATA_SPECIFICATION);
		}
		return dataSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AssetAdministrationShell getDerivedFrom() {
		if (derivedFrom != null && derivedFrom.eIsProxy()) {
			InternalEObject oldDerivedFrom = (InternalEObject)derivedFrom;
			derivedFrom = (AssetAdministrationShell)eResolveProxy(oldDerivedFrom);
			if (derivedFrom != oldDerivedFrom) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AASPackage.ASSET_ADMINISTRATION_SHELL__DERIVED_FROM, oldDerivedFrom, derivedFrom));
			}
		}
		return derivedFrom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssetAdministrationShell basicGetDerivedFrom() {
		return derivedFrom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDerivedFrom(AssetAdministrationShell newDerivedFrom) {
		AssetAdministrationShell oldDerivedFrom = derivedFrom;
		derivedFrom = newDerivedFrom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.ASSET_ADMINISTRATION_SHELL__DERIVED_FROM, oldDerivedFrom, derivedFrom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Security getSecurity() {
		if (security != null && security.eIsProxy()) {
			InternalEObject oldSecurity = (InternalEObject)security;
			security = (Security)eResolveProxy(oldSecurity);
			if (security != oldSecurity) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AASPackage.ASSET_ADMINISTRATION_SHELL__SECURITY, oldSecurity, security));
			}
		}
		return security;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Security basicGetSecurity() {
		return security;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSecurity(Security newSecurity) {
		Security oldSecurity = security;
		security = newSecurity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.ASSET_ADMINISTRATION_SHELL__SECURITY, oldSecurity, security));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AssetInformation getAssetInformation() {
		return assetInformation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssetInformation(AssetInformation newAssetInformation, NotificationChain msgs) {
		AssetInformation oldAssetInformation = assetInformation;
		assetInformation = newAssetInformation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AASPackage.ASSET_ADMINISTRATION_SHELL__ASSET_INFORMATION, oldAssetInformation, newAssetInformation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAssetInformation(AssetInformation newAssetInformation) {
		if (newAssetInformation != assetInformation) {
			NotificationChain msgs = null;
			if (assetInformation != null)
				msgs = ((InternalEObject)assetInformation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AASPackage.ASSET_ADMINISTRATION_SHELL__ASSET_INFORMATION, null, msgs);
			if (newAssetInformation != null)
				msgs = ((InternalEObject)newAssetInformation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AASPackage.ASSET_ADMINISTRATION_SHELL__ASSET_INFORMATION, null, msgs);
			msgs = basicSetAssetInformation(newAssetInformation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.ASSET_ADMINISTRATION_SHELL__ASSET_INFORMATION, newAssetInformation, newAssetInformation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Asset getAsset() {
		if (asset != null && asset.eIsProxy()) {
			InternalEObject oldAsset = (InternalEObject)asset;
			asset = (Asset)eResolveProxy(oldAsset);
			if (asset != oldAsset) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AASPackage.ASSET_ADMINISTRATION_SHELL__ASSET, oldAsset, asset));
			}
		}
		return asset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Asset basicGetAsset() {
		return asset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAsset(Asset newAsset) {
		Asset oldAsset = asset;
		asset = newAsset;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.ASSET_ADMINISTRATION_SHELL__ASSET, oldAsset, asset));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Submodel> getSubmodel() {
		// should return all nestedClassifier of a AAS of type Submodel
				EList<Submodel> submodels = new BasicEList<>();
				if (getBase_Class() != null) {
					org.eclipse.uml2.uml.Class base_class = getBase_Class();
					EList<Classifier> allNestedClassifier = base_class.getNestedClassifiers();
					Iterator<Classifier> it = allNestedClassifier.iterator();
					while (it.hasNext()) {
						Classifier classifier = it.next();
						if (org.eclipse.uml2.uml.util.UMLUtil.getStereotypeApplication(classifier, Submodel.class) != null) {
		
							Submodel submodel = org.eclipse.uml2.uml.util.UMLUtil.getStereotypeApplication(classifier, Submodel.class);
							submodels.add(submodel);
						}
					}
				}
				return new UnmodifiableEList<>(this, AASPackage.eINSTANCE.getSubmodel_Submodelelement(), submodels.size(), submodels.toArray());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AASEndpoint getEndpoint() {
		return endpoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEndpoint(AASEndpoint newEndpoint, NotificationChain msgs) {
		AASEndpoint oldEndpoint = endpoint;
		endpoint = newEndpoint;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AASPackage.ASSET_ADMINISTRATION_SHELL__ENDPOINT, oldEndpoint, newEndpoint);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEndpoint(AASEndpoint newEndpoint) {
		if (newEndpoint != endpoint) {
			NotificationChain msgs = null;
			if (endpoint != null)
				msgs = ((InternalEObject)endpoint).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AASPackage.ASSET_ADMINISTRATION_SHELL__ENDPOINT, null, msgs);
			if (newEndpoint != null)
				msgs = ((InternalEObject)newEndpoint).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AASPackage.ASSET_ADMINISTRATION_SHELL__ENDPOINT, null, msgs);
			msgs = basicSetEndpoint(newEndpoint, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.ASSET_ADMINISTRATION_SHELL__ENDPOINT, newEndpoint, newEndpoint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AASPackage.ASSET_ADMINISTRATION_SHELL__ASSET_INFORMATION:
				return basicSetAssetInformation(null, msgs);
			case AASPackage.ASSET_ADMINISTRATION_SHELL__ENDPOINT:
				return basicSetEndpoint(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AASPackage.ASSET_ADMINISTRATION_SHELL__DATA_SPECIFICATION:
				return getDataSpecification();
			case AASPackage.ASSET_ADMINISTRATION_SHELL__DERIVED_FROM:
				if (resolve) return getDerivedFrom();
				return basicGetDerivedFrom();
			case AASPackage.ASSET_ADMINISTRATION_SHELL__SECURITY:
				if (resolve) return getSecurity();
				return basicGetSecurity();
			case AASPackage.ASSET_ADMINISTRATION_SHELL__ASSET_INFORMATION:
				return getAssetInformation();
			case AASPackage.ASSET_ADMINISTRATION_SHELL__ASSET:
				if (resolve) return getAsset();
				return basicGetAsset();
			case AASPackage.ASSET_ADMINISTRATION_SHELL__SUBMODEL:
				return getSubmodel();
			case AASPackage.ASSET_ADMINISTRATION_SHELL__ENDPOINT:
				return getEndpoint();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AASPackage.ASSET_ADMINISTRATION_SHELL__DATA_SPECIFICATION:
				getDataSpecification().clear();
				getDataSpecification().addAll((Collection<? extends Reference>)newValue);
				return;
			case AASPackage.ASSET_ADMINISTRATION_SHELL__DERIVED_FROM:
				setDerivedFrom((AssetAdministrationShell)newValue);
				return;
			case AASPackage.ASSET_ADMINISTRATION_SHELL__SECURITY:
				setSecurity((Security)newValue);
				return;
			case AASPackage.ASSET_ADMINISTRATION_SHELL__ASSET_INFORMATION:
				setAssetInformation((AssetInformation)newValue);
				return;
			case AASPackage.ASSET_ADMINISTRATION_SHELL__ASSET:
				setAsset((Asset)newValue);
				return;
			case AASPackage.ASSET_ADMINISTRATION_SHELL__ENDPOINT:
				setEndpoint((AASEndpoint)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AASPackage.ASSET_ADMINISTRATION_SHELL__DATA_SPECIFICATION:
				getDataSpecification().clear();
				return;
			case AASPackage.ASSET_ADMINISTRATION_SHELL__DERIVED_FROM:
				setDerivedFrom((AssetAdministrationShell)null);
				return;
			case AASPackage.ASSET_ADMINISTRATION_SHELL__SECURITY:
				setSecurity((Security)null);
				return;
			case AASPackage.ASSET_ADMINISTRATION_SHELL__ASSET_INFORMATION:
				setAssetInformation((AssetInformation)null);
				return;
			case AASPackage.ASSET_ADMINISTRATION_SHELL__ASSET:
				setAsset((Asset)null);
				return;
			case AASPackage.ASSET_ADMINISTRATION_SHELL__ENDPOINT:
				setEndpoint((AASEndpoint)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AASPackage.ASSET_ADMINISTRATION_SHELL__DATA_SPECIFICATION:
				return dataSpecification != null && !dataSpecification.isEmpty();
			case AASPackage.ASSET_ADMINISTRATION_SHELL__DERIVED_FROM:
				return derivedFrom != null;
			case AASPackage.ASSET_ADMINISTRATION_SHELL__SECURITY:
				return security != null;
			case AASPackage.ASSET_ADMINISTRATION_SHELL__ASSET_INFORMATION:
				return assetInformation != null;
			case AASPackage.ASSET_ADMINISTRATION_SHELL__ASSET:
				return asset != null;
			case AASPackage.ASSET_ADMINISTRATION_SHELL__SUBMODEL:
				return !getSubmodel().isEmpty();
			case AASPackage.ASSET_ADMINISTRATION_SHELL__ENDPOINT:
				return endpoint != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == HasDataSpecification.class) {
			switch (derivedFeatureID) {
				case AASPackage.ASSET_ADMINISTRATION_SHELL__DATA_SPECIFICATION: return AASPackage.HAS_DATA_SPECIFICATION__DATA_SPECIFICATION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == HasDataSpecification.class) {
			switch (baseFeatureID) {
				case AASPackage.HAS_DATA_SPECIFICATION__DATA_SPECIFICATION: return AASPackage.ASSET_ADMINISTRATION_SHELL__DATA_SPECIFICATION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} // AssetAdministrationShellImpl
