/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Asset Administration Shell</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A project in Basyx
 * Basyx:
 * 
 * identifierType: IRDI (urn:...), URI, custom 
 * identifier (for submodel or operation) ->generated automatically
 * resource name
 * idshort: string -> generated automatically
 * category:string
 * description:string
 * parent: not used
 * administration: not used
 * 
 * 
 * 
 * Security is not implemented, but is an ongoing project
 * 0 < port  <= 65535
 * 
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.AssetAdministrationShell#getDerivedFrom <em>Derived From</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.AssetAdministrationShell#getSecurity <em>Security</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.AssetAdministrationShell#getAssetInformation <em>Asset Information</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.AssetAdministrationShell#getAsset <em>Asset</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.AssetAdministrationShell#getSubmodel <em>Submodel</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.AssetAdministrationShell#getEndpoint <em>Endpoint</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.aas.AASPackage#getAssetAdministrationShell()
 * @model
 * @generated
 */
public interface AssetAdministrationShell extends Identifiable, HasDataSpecification {
	/**
	 * Returns the value of the '<em><b>Derived From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived From</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived From</em>' reference.
	 * @see #setDerivedFrom(AssetAdministrationShell)
	 * @see org.eclipse.papyrus.aas.AASPackage#getAssetAdministrationShell_DerivedFrom()
	 * @model ordered="false"
	 * @generated
	 */
	AssetAdministrationShell getDerivedFrom();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.AssetAdministrationShell#getDerivedFrom <em>Derived From</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Derived From</em>' reference.
	 * @see #getDerivedFrom()
	 * @generated
	 */
	void setDerivedFrom(AssetAdministrationShell value);

	/**
	 * Returns the value of the '<em><b>Security</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Security</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Security</em>' reference.
	 * @see #setSecurity(Security)
	 * @see org.eclipse.papyrus.aas.AASPackage#getAssetAdministrationShell_Security()
	 * @model ordered="false"
	 * @generated
	 */
	Security getSecurity();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.AssetAdministrationShell#getSecurity <em>Security</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Security</em>' reference.
	 * @see #getSecurity()
	 * @generated
	 */
	void setSecurity(Security value);

	/**
	 * Returns the value of the '<em><b>Asset Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asset Information</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asset Information</em>' containment reference.
	 * @see #setAssetInformation(AssetInformation)
	 * @see org.eclipse.papyrus.aas.AASPackage#getAssetAdministrationShell_AssetInformation()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	AssetInformation getAssetInformation();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.AssetAdministrationShell#getAssetInformation <em>Asset Information</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Asset Information</em>' containment reference.
	 * @see #getAssetInformation()
	 * @generated
	 */
	void setAssetInformation(AssetInformation value);

	/**
	 * Returns the value of the '<em><b>Asset</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asset</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asset</em>' reference.
	 * @see #setAsset(Asset)
	 * @see org.eclipse.papyrus.aas.AASPackage#getAssetAdministrationShell_Asset()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Asset getAsset();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.AssetAdministrationShell#getAsset <em>Asset</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Asset</em>' reference.
	 * @see #getAsset()
	 * @generated
	 */
	void setAsset(Asset value);

	/**
	 * Returns the value of the '<em><b>Submodel</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.aas.Submodel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Submodel</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Submodel</em>' reference list.
	 * @see org.eclipse.papyrus.aas.AASPackage#getAssetAdministrationShell_Submodel()
	 * @model transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<Submodel> getSubmodel();

	/**
	 * Returns the value of the '<em><b>Endpoint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Endpoint</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Endpoint</em>' containment reference.
	 * @see #setEndpoint(AASEndpoint)
	 * @see org.eclipse.papyrus.aas.AASPackage#getAssetAdministrationShell_Endpoint()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	AASEndpoint getEndpoint();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.AssetAdministrationShell#getEndpoint <em>Endpoint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Endpoint</em>' containment reference.
	 * @see #getEndpoint()
	 * @generated
	 */
	void setEndpoint(AASEndpoint value);

} // AssetAdministrationShell
