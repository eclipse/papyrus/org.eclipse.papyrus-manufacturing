/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lang String</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.LangString#getLang <em>Lang</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.LangString#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.aas.AASPackage#getLangString()
 * @model
 * @generated
 */
public interface LangString extends EObject {
	/**
	 * Returns the value of the '<em><b>Lang</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.aas.LangEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lang</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lang</em>' attribute.
	 * @see org.eclipse.papyrus.aas.LangEnum
	 * @see #setLang(LangEnum)
	 * @see org.eclipse.papyrus.aas.AASPackage#getLangString_Lang()
	 * @model required="true"
	 * @generated
	 */
	LangEnum getLang();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.LangString#getLang <em>Lang</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lang</em>' attribute.
	 * @see org.eclipse.papyrus.aas.LangEnum
	 * @see #getLang()
	 * @generated
	 */
	void setLang(LangEnum value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see org.eclipse.papyrus.aas.AASPackage#getLangString_Value()
	 * @model dataType="org.eclipse.uml2.types.String" required="true"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.LangString#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

} // LangString
