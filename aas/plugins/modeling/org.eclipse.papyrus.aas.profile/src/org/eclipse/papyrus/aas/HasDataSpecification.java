/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Has Data Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.HasDataSpecification#getDataSpecification <em>Data Specification</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.aas.AASPackage#getHasDataSpecification()
 * @model abstract="true"
 * @generated
 */
public interface HasDataSpecification extends EObject {
	/**
	 * Returns the value of the '<em><b>Data Specification</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.aas.Reference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Specification</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Specification</em>' reference list.
	 * @see org.eclipse.papyrus.aas.AASPackage#getHasDataSpecification_DataSpecification()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Reference> getDataSpecification();

} // HasDataSpecification
