/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Submodel</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A model in Basyx
 * Basyx:
 * 
 * identifierType: IRDI (urn:...), URI, custom 
 * identifier (for submodel or operation) ->generated automatically
 * resource name
 * idshort: string -> generated automatically
 * category:string
 * description:string
 * parent: not used
 * administration: not used
 * 
 * 
 * 
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.Submodel#getSubmodelelement <em>Submodelelement</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.aas.AASPackage#getSubmodel()
 * @model
 * @generated
 */
public interface Submodel extends Identifiable, HasKind, HasSemantics, HasDataSpecification {
	/**
	 * Returns the value of the '<em><b>Submodelelement</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.aas.SubmodelElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Submodelelement</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Submodelelement</em>' reference list.
	 * @see org.eclipse.papyrus.aas.AASPackage#getSubmodel_Submodelelement()
	 * @model transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<SubmodelElement> getSubmodelelement();

} // Submodel
