/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.papyrus.aas.AASPackage;
import org.eclipse.papyrus.aas.AccessControlPolicyPoints;
import org.eclipse.papyrus.aas.Certificate;
import org.eclipse.papyrus.aas.Reference;
import org.eclipse.papyrus.aas.Security;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Security</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.impl.SecurityImpl#getBase_Class <em>Base Class</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.SecurityImpl#getAccessControlPolicyPoints <em>Access Control Policy Points</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.SecurityImpl#getCertificate <em>Certificate</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.SecurityImpl#getRequiredCertificateExtension <em>Required Certificate Extension</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SecurityImpl extends MinimalEObjectImpl.Container implements Security {
	/**
	 * The cached value of the '{@link #getBase_Class() <em>Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Class()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Class base_Class;

	/**
	 * The cached value of the '{@link #getAccessControlPolicyPoints() <em>Access Control Policy Points</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessControlPolicyPoints()
	 * @generated
	 * @ordered
	 */
	protected AccessControlPolicyPoints accessControlPolicyPoints;

	/**
	 * The cached value of the '{@link #getCertificate() <em>Certificate</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCertificate()
	 * @generated
	 * @ordered
	 */
	protected EList<Certificate> certificate;

	/**
	 * The cached value of the '{@link #getRequiredCertificateExtension() <em>Required Certificate Extension</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredCertificateExtension()
	 * @generated
	 * @ordered
	 */
	protected EList<Reference> requiredCertificateExtension;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SecurityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AASPackage.Literals.SECURITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public org.eclipse.uml2.uml.Class getBase_Class() {
		if (base_Class != null && base_Class.eIsProxy()) {
			InternalEObject oldBase_Class = (InternalEObject)base_Class;
			base_Class = (org.eclipse.uml2.uml.Class)eResolveProxy(oldBase_Class);
			if (base_Class != oldBase_Class) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AASPackage.SECURITY__BASE_CLASS, oldBase_Class, base_Class));
			}
		}
		return base_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class basicGetBase_Class() {
		return base_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_Class(org.eclipse.uml2.uml.Class newBase_Class) {
		org.eclipse.uml2.uml.Class oldBase_Class = base_Class;
		base_Class = newBase_Class;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.SECURITY__BASE_CLASS, oldBase_Class, base_Class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AccessControlPolicyPoints getAccessControlPolicyPoints() {
		return accessControlPolicyPoints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccessControlPolicyPoints(AccessControlPolicyPoints newAccessControlPolicyPoints, NotificationChain msgs) {
		AccessControlPolicyPoints oldAccessControlPolicyPoints = accessControlPolicyPoints;
		accessControlPolicyPoints = newAccessControlPolicyPoints;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AASPackage.SECURITY__ACCESS_CONTROL_POLICY_POINTS, oldAccessControlPolicyPoints, newAccessControlPolicyPoints);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAccessControlPolicyPoints(AccessControlPolicyPoints newAccessControlPolicyPoints) {
		if (newAccessControlPolicyPoints != accessControlPolicyPoints) {
			NotificationChain msgs = null;
			if (accessControlPolicyPoints != null)
				msgs = ((InternalEObject)accessControlPolicyPoints).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AASPackage.SECURITY__ACCESS_CONTROL_POLICY_POINTS, null, msgs);
			if (newAccessControlPolicyPoints != null)
				msgs = ((InternalEObject)newAccessControlPolicyPoints).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AASPackage.SECURITY__ACCESS_CONTROL_POLICY_POINTS, null, msgs);
			msgs = basicSetAccessControlPolicyPoints(newAccessControlPolicyPoints, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.SECURITY__ACCESS_CONTROL_POLICY_POINTS, newAccessControlPolicyPoints, newAccessControlPolicyPoints));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Certificate> getCertificate() {
		if (certificate == null) {
			certificate = new EObjectContainmentEList<Certificate>(Certificate.class, this, AASPackage.SECURITY__CERTIFICATE);
		}
		return certificate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Reference> getRequiredCertificateExtension() {
		if (requiredCertificateExtension == null) {
			requiredCertificateExtension = new EObjectResolvingEList<Reference>(Reference.class, this, AASPackage.SECURITY__REQUIRED_CERTIFICATE_EXTENSION);
		}
		return requiredCertificateExtension;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AASPackage.SECURITY__ACCESS_CONTROL_POLICY_POINTS:
				return basicSetAccessControlPolicyPoints(null, msgs);
			case AASPackage.SECURITY__CERTIFICATE:
				return ((InternalEList<?>)getCertificate()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AASPackage.SECURITY__BASE_CLASS:
				if (resolve) return getBase_Class();
				return basicGetBase_Class();
			case AASPackage.SECURITY__ACCESS_CONTROL_POLICY_POINTS:
				return getAccessControlPolicyPoints();
			case AASPackage.SECURITY__CERTIFICATE:
				return getCertificate();
			case AASPackage.SECURITY__REQUIRED_CERTIFICATE_EXTENSION:
				return getRequiredCertificateExtension();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AASPackage.SECURITY__BASE_CLASS:
				setBase_Class((org.eclipse.uml2.uml.Class)newValue);
				return;
			case AASPackage.SECURITY__ACCESS_CONTROL_POLICY_POINTS:
				setAccessControlPolicyPoints((AccessControlPolicyPoints)newValue);
				return;
			case AASPackage.SECURITY__CERTIFICATE:
				getCertificate().clear();
				getCertificate().addAll((Collection<? extends Certificate>)newValue);
				return;
			case AASPackage.SECURITY__REQUIRED_CERTIFICATE_EXTENSION:
				getRequiredCertificateExtension().clear();
				getRequiredCertificateExtension().addAll((Collection<? extends Reference>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AASPackage.SECURITY__BASE_CLASS:
				setBase_Class((org.eclipse.uml2.uml.Class)null);
				return;
			case AASPackage.SECURITY__ACCESS_CONTROL_POLICY_POINTS:
				setAccessControlPolicyPoints((AccessControlPolicyPoints)null);
				return;
			case AASPackage.SECURITY__CERTIFICATE:
				getCertificate().clear();
				return;
			case AASPackage.SECURITY__REQUIRED_CERTIFICATE_EXTENSION:
				getRequiredCertificateExtension().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AASPackage.SECURITY__BASE_CLASS:
				return base_Class != null;
			case AASPackage.SECURITY__ACCESS_CONTROL_POLICY_POINTS:
				return accessControlPolicyPoints != null;
			case AASPackage.SECURITY__CERTIFICATE:
				return certificate != null && !certificate.isEmpty();
			case AASPackage.SECURITY__REQUIRED_CERTIFICATE_EXTENSION:
				return requiredCertificateExtension != null && !requiredCertificateExtension.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SecurityImpl
