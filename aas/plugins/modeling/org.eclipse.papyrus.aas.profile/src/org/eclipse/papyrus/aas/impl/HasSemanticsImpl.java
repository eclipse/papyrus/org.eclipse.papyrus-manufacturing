/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.papyrus.aas.AASPackage;
import org.eclipse.papyrus.aas.HasSemantics;
import org.eclipse.papyrus.aas.Reference;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Has Semantics</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.impl.HasSemanticsImpl#getSemanticId <em>Semantic Id</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.HasSemanticsImpl#getBase_HasSemantics_Class <em>Base Has Semantics Class</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class HasSemanticsImpl extends MinimalEObjectImpl.Container implements HasSemantics {
	/**
	 * The cached value of the '{@link #getSemanticId() <em>Semantic Id</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemanticId()
	 * @generated
	 * @ordered
	 */
	protected Reference semanticId;

	/**
	 * The cached value of the '{@link #getBase_HasSemantics_Class() <em>Base Has Semantics Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_HasSemantics_Class()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Class base_HasSemantics_Class;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HasSemanticsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AASPackage.Literals.HAS_SEMANTICS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Reference getSemanticId() {
		if (semanticId != null && semanticId.eIsProxy()) {
			InternalEObject oldSemanticId = (InternalEObject)semanticId;
			semanticId = (Reference)eResolveProxy(oldSemanticId);
			if (semanticId != oldSemanticId) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AASPackage.HAS_SEMANTICS__SEMANTIC_ID, oldSemanticId, semanticId));
			}
		}
		return semanticId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reference basicGetSemanticId() {
		return semanticId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSemanticId(Reference newSemanticId) {
		Reference oldSemanticId = semanticId;
		semanticId = newSemanticId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.HAS_SEMANTICS__SEMANTIC_ID, oldSemanticId, semanticId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public org.eclipse.uml2.uml.Class getBase_HasSemantics_Class() {
		if (base_HasSemantics_Class != null && base_HasSemantics_Class.eIsProxy()) {
			InternalEObject oldBase_HasSemantics_Class = (InternalEObject)base_HasSemantics_Class;
			base_HasSemantics_Class = (org.eclipse.uml2.uml.Class)eResolveProxy(oldBase_HasSemantics_Class);
			if (base_HasSemantics_Class != oldBase_HasSemantics_Class) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AASPackage.HAS_SEMANTICS__BASE_HAS_SEMANTICS_CLASS, oldBase_HasSemantics_Class, base_HasSemantics_Class));
			}
		}
		return base_HasSemantics_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class basicGetBase_HasSemantics_Class() {
		return base_HasSemantics_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_HasSemantics_Class(org.eclipse.uml2.uml.Class newBase_HasSemantics_Class) {
		org.eclipse.uml2.uml.Class oldBase_HasSemantics_Class = base_HasSemantics_Class;
		base_HasSemantics_Class = newBase_HasSemantics_Class;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.HAS_SEMANTICS__BASE_HAS_SEMANTICS_CLASS, oldBase_HasSemantics_Class, base_HasSemantics_Class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AASPackage.HAS_SEMANTICS__SEMANTIC_ID:
				if (resolve) return getSemanticId();
				return basicGetSemanticId();
			case AASPackage.HAS_SEMANTICS__BASE_HAS_SEMANTICS_CLASS:
				if (resolve) return getBase_HasSemantics_Class();
				return basicGetBase_HasSemantics_Class();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AASPackage.HAS_SEMANTICS__SEMANTIC_ID:
				setSemanticId((Reference)newValue);
				return;
			case AASPackage.HAS_SEMANTICS__BASE_HAS_SEMANTICS_CLASS:
				setBase_HasSemantics_Class((org.eclipse.uml2.uml.Class)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AASPackage.HAS_SEMANTICS__SEMANTIC_ID:
				setSemanticId((Reference)null);
				return;
			case AASPackage.HAS_SEMANTICS__BASE_HAS_SEMANTICS_CLASS:
				setBase_HasSemantics_Class((org.eclipse.uml2.uml.Class)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AASPackage.HAS_SEMANTICS__SEMANTIC_ID:
				return semanticId != null;
			case AASPackage.HAS_SEMANTICS__BASE_HAS_SEMANTICS_CLASS:
				return base_HasSemantics_Class != null;
		}
		return super.eIsSet(featureID);
	}

} //HasSemanticsImpl
