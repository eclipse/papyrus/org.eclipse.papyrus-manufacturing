/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Concept Description</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.ConceptDescription#getIsCaseOf <em>Is Case Of</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.aas.AASPackage#getConceptDescription()
 * @model
 * @generated
 */
public interface ConceptDescription extends Identifiable, HasDataSpecification {
	/**
	 * Returns the value of the '<em><b>Is Case Of</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.aas.Reference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Case Of</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Case Of</em>' reference list.
	 * @see org.eclipse.papyrus.aas.AASPackage#getConceptDescription_IsCaseOf()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Reference> getIsCaseOf();

} // ConceptDescription
