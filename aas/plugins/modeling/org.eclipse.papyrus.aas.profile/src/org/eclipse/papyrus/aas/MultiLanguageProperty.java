/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Multi Language Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.MultiLanguageProperty#getValue <em>Value</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.MultiLanguageProperty#getValueId <em>Value Id</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.aas.AASPackage#getMultiLanguageProperty()
 * @model
 * @generated
 */
public interface MultiLanguageProperty extends DataElement {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.aas.LangString}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference list.
	 * @see org.eclipse.papyrus.aas.AASPackage#getMultiLanguageProperty_Value()
	 * @model containment="true"
	 * @generated
	 */
	EList<LangString> getValue();

	/**
	 * Returns the value of the '<em><b>Value Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Id</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Id</em>' reference.
	 * @see #setValueId(Reference)
	 * @see org.eclipse.papyrus.aas.AASPackage#getMultiLanguageProperty_ValueId()
	 * @model
	 * @generated
	 */
	Reference getValueId();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.aas.MultiLanguageProperty#getValueId <em>Value Id</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Id</em>' reference.
	 * @see #getValueId()
	 * @generated
	 */
	void setValueId(Reference value);

} // MultiLanguageProperty
