/**
 * Copyright (c) 2023 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 *   SPDX-License-Identifier: EPL-2.0
 *
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.papyrus.aas.AASPackage;
import org.eclipse.papyrus.aas.SubmodelElement;
import org.eclipse.papyrus.aas.SubmodelElementCollection;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Property;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Submodel Element Collection</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.impl.SubmodelElementCollectionImpl#getValue <em>Value</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.SubmodelElementCollectionImpl#getBase_Property <em>Base Property</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.SubmodelElementCollectionImpl#getBase_DataType <em>Base Data Type</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.SubmodelElementCollectionImpl#isOrdered <em>Ordered</em>}</li>
 *   <li>{@link org.eclipse.papyrus.aas.impl.SubmodelElementCollectionImpl#isAllowDuplicates <em>Allow Duplicates</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubmodelElementCollectionImpl extends SubmodelElementImpl implements SubmodelElementCollection {
	/**
	 * The cached value of the '{@link #getBase_Property() <em>Base Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Property()
	 * @generated
	 * @ordered
	 */
	protected Property base_Property;

	/**
	 * The cached value of the '{@link #getBase_DataType() <em>Base Data Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_DataType()
	 * @generated
	 * @ordered
	 */
	protected DataType base_DataType;

	/**
	 * The default value of the '{@link #isOrdered() <em>Ordered</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOrdered()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ORDERED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isOrdered() <em>Ordered</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOrdered()
	 * @generated
	 * @ordered
	 */
	protected boolean ordered = ORDERED_EDEFAULT;

	/**
	 * The default value of the '{@link #isAllowDuplicates() <em>Allow Duplicates</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAllowDuplicates()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ALLOW_DUPLICATES_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAllowDuplicates() <em>Allow Duplicates</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAllowDuplicates()
	 * @generated
	 * @ordered
	 */
	protected boolean allowDuplicates = ALLOW_DUPLICATES_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubmodelElementCollectionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AASPackage.Literals.SUBMODEL_ELEMENT_COLLECTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SubmodelElement> getValue() {
			// get all SubmodelElementCollection and add them to the list of submodelElements
				EList<SubmodelElement> submodelElements = new org.eclipse.emf.common.util.BasicEList<>();
				if (getBase_Class() != null) {
					org.eclipse.uml2.uml.Class base_class = getBase_Class();
		
					// get all SECs and add them to the list of submodelElements
					EList<Classifier> allNestedClassifier = base_class.getNestedClassifiers();
					java.util.Iterator<org.eclipse.uml2.uml.Classifier> it = allNestedClassifier.iterator();
					while (it.hasNext()) {
						Classifier classifier = it.next();
						if (org.eclipse.uml2.uml.util.UMLUtil.getStereotypeApplication(classifier, org.eclipse.papyrus.aas.SubmodelElementCollection.class) != null) {
		
							SubmodelElementCollection sec = org.eclipse.uml2.uml.util.UMLUtil.getStereotypeApplication(classifier, org.eclipse.papyrus.aas.SubmodelElementCollection.class);
							submodelElements.add(sec);
						}
					}
		
					// get all submodelElements (stereotyped attributes) and add them to the list of submodelElements
					EList<org.eclipse.uml2.uml.Property> allAttributes = base_class.getAllAttributes();
					java.util.Iterator<Property> p_it = allAttributes.iterator();
					while (p_it.hasNext()) {
						Property property = p_it.next();
						if (org.eclipse.papyrus.uml.tools.utils.UMLUtil.getAppliedStereotype(property, "AAS::SubmodelElement", false) != null) {
							SubmodelElement child = org.eclipse.uml2.uml.util.UMLUtil.getStereotypeApplication(property, org.eclipse.papyrus.aas.SubmodelElement.class);
							submodelElements.add(child);
		
						}
					}
		
					// get all Operations and add them to the list of submodelElements
					EList<org.eclipse.uml2.uml.Operation> allOperations = base_class.getAllOperations();
					java.util.Iterator<org.eclipse.uml2.uml.Operation> o_it = allOperations.iterator();
					while (o_it.hasNext()) {
						org.eclipse.uml2.uml.Operation operation = o_it.next();
						if (org.eclipse.uml2.uml.util.UMLUtil.getStereotypeApplication(operation, org.eclipse.papyrus.aas.Operation.class) != null) {
		
							org.eclipse.papyrus.aas.Operation op = org.eclipse.uml2.uml.util.UMLUtil.getStereotypeApplication(operation, org.eclipse.papyrus.aas.Operation.class);
							submodelElements.add(op);
						}
					}
		
		
				}
				return new org.eclipse.emf.ecore.util.EcoreEList.UnmodifiableEList<>(this, AASPackage.eINSTANCE.getSubmodel_Submodelelement(), submodelElements.size(), submodelElements.toArray());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Property getBase_Property() {
		if (base_Property != null && base_Property.eIsProxy()) {
			InternalEObject oldBase_Property = (InternalEObject)base_Property;
			base_Property = (Property)eResolveProxy(oldBase_Property);
			if (base_Property != oldBase_Property) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AASPackage.SUBMODEL_ELEMENT_COLLECTION__BASE_PROPERTY, oldBase_Property, base_Property));
			}
		}
		return base_Property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property basicGetBase_Property() {
		return base_Property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_Property(Property newBase_Property) {
		Property oldBase_Property = base_Property;
		base_Property = newBase_Property;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.SUBMODEL_ELEMENT_COLLECTION__BASE_PROPERTY, oldBase_Property, base_Property));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DataType getBase_DataType() {
		if (base_DataType != null && base_DataType.eIsProxy()) {
			InternalEObject oldBase_DataType = (InternalEObject)base_DataType;
			base_DataType = (DataType)eResolveProxy(oldBase_DataType);
			if (base_DataType != oldBase_DataType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AASPackage.SUBMODEL_ELEMENT_COLLECTION__BASE_DATA_TYPE, oldBase_DataType, base_DataType));
			}
		}
		return base_DataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataType basicGetBase_DataType() {
		return base_DataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_DataType(DataType newBase_DataType) {
		DataType oldBase_DataType = base_DataType;
		base_DataType = newBase_DataType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.SUBMODEL_ELEMENT_COLLECTION__BASE_DATA_TYPE, oldBase_DataType, base_DataType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isOrdered() {
		return ordered;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOrdered(boolean newOrdered) {
		boolean oldOrdered = ordered;
		ordered = newOrdered;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.SUBMODEL_ELEMENT_COLLECTION__ORDERED, oldOrdered, ordered));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isAllowDuplicates() {
		return allowDuplicates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAllowDuplicates(boolean newAllowDuplicates) {
		boolean oldAllowDuplicates = allowDuplicates;
		allowDuplicates = newAllowDuplicates;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AASPackage.SUBMODEL_ELEMENT_COLLECTION__ALLOW_DUPLICATES, oldAllowDuplicates, allowDuplicates));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AASPackage.SUBMODEL_ELEMENT_COLLECTION__VALUE:
				return getValue();
			case AASPackage.SUBMODEL_ELEMENT_COLLECTION__BASE_PROPERTY:
				if (resolve) return getBase_Property();
				return basicGetBase_Property();
			case AASPackage.SUBMODEL_ELEMENT_COLLECTION__BASE_DATA_TYPE:
				if (resolve) return getBase_DataType();
				return basicGetBase_DataType();
			case AASPackage.SUBMODEL_ELEMENT_COLLECTION__ORDERED:
				return isOrdered();
			case AASPackage.SUBMODEL_ELEMENT_COLLECTION__ALLOW_DUPLICATES:
				return isAllowDuplicates();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AASPackage.SUBMODEL_ELEMENT_COLLECTION__BASE_PROPERTY:
				setBase_Property((Property)newValue);
				return;
			case AASPackage.SUBMODEL_ELEMENT_COLLECTION__BASE_DATA_TYPE:
				setBase_DataType((DataType)newValue);
				return;
			case AASPackage.SUBMODEL_ELEMENT_COLLECTION__ORDERED:
				setOrdered((Boolean)newValue);
				return;
			case AASPackage.SUBMODEL_ELEMENT_COLLECTION__ALLOW_DUPLICATES:
				setAllowDuplicates((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AASPackage.SUBMODEL_ELEMENT_COLLECTION__BASE_PROPERTY:
				setBase_Property((Property)null);
				return;
			case AASPackage.SUBMODEL_ELEMENT_COLLECTION__BASE_DATA_TYPE:
				setBase_DataType((DataType)null);
				return;
			case AASPackage.SUBMODEL_ELEMENT_COLLECTION__ORDERED:
				setOrdered(ORDERED_EDEFAULT);
				return;
			case AASPackage.SUBMODEL_ELEMENT_COLLECTION__ALLOW_DUPLICATES:
				setAllowDuplicates(ALLOW_DUPLICATES_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AASPackage.SUBMODEL_ELEMENT_COLLECTION__VALUE:
				return !getValue().isEmpty();
			case AASPackage.SUBMODEL_ELEMENT_COLLECTION__BASE_PROPERTY:
				return base_Property != null;
			case AASPackage.SUBMODEL_ELEMENT_COLLECTION__BASE_DATA_TYPE:
				return base_DataType != null;
			case AASPackage.SUBMODEL_ELEMENT_COLLECTION__ORDERED:
				return ordered != ORDERED_EDEFAULT;
			case AASPackage.SUBMODEL_ELEMENT_COLLECTION__ALLOW_DUPLICATES:
				return allowDuplicates != ALLOW_DUPLICATES_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (ordered: ");
		result.append(ordered);
		result.append(", allowDuplicates: ");
		result.append(allowDuplicates);
		result.append(')');
		return result.toString();
	}

} // SubmodelElementCollectionImpl
