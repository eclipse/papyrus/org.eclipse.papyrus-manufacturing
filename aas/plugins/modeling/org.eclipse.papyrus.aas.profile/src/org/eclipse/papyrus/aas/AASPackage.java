/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.aas.AASFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='AAS'"
 * @generated
 */
public interface AASPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "aas";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/papyrus/AAS";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "AAS";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AASPackage eINSTANCE = org.eclipse.papyrus.aas.impl.AASPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.ReferableImpl <em>Referable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.ReferableImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getReferable()
	 * @generated
	 */
	int REFERABLE = 2;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERABLE__ID_SHORT = 0;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERABLE__CATEGORY = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERABLE__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERABLE__BASE_CLASS = 3;

	/**
	 * The number of structural features of the '<em>Referable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERABLE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Referable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERABLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.IdentifiableImpl <em>Identifiable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.IdentifiableImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getIdentifiable()
	 * @generated
	 */
	int IDENTIFIABLE = 1;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIABLE__ID_SHORT = REFERABLE__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIABLE__CATEGORY = REFERABLE__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIABLE__DESCRIPTION = REFERABLE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIABLE__BASE_CLASS = REFERABLE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Administration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIABLE__ADMINISTRATION = REFERABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Identification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIABLE__IDENTIFICATION = REFERABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Base Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIABLE__BASE_PACKAGE = REFERABLE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Identifiable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIABLE_FEATURE_COUNT = REFERABLE_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Identifiable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIABLE_OPERATION_COUNT = REFERABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.AssetAdministrationShellImpl <em>Asset Administration Shell</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.AssetAdministrationShellImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getAssetAdministrationShell()
	 * @generated
	 */
	int ASSET_ADMINISTRATION_SHELL = 0;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL__ID_SHORT = IDENTIFIABLE__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL__CATEGORY = IDENTIFIABLE__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL__DESCRIPTION = IDENTIFIABLE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL__BASE_CLASS = IDENTIFIABLE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Administration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL__ADMINISTRATION = IDENTIFIABLE__ADMINISTRATION;

	/**
	 * The feature id for the '<em><b>Identification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL__IDENTIFICATION = IDENTIFIABLE__IDENTIFICATION;

	/**
	 * The feature id for the '<em><b>Base Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL__BASE_PACKAGE = IDENTIFIABLE__BASE_PACKAGE;

	/**
	 * The feature id for the '<em><b>Data Specification</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL__DATA_SPECIFICATION = IDENTIFIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Derived From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL__DERIVED_FROM = IDENTIFIABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Security</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL__SECURITY = IDENTIFIABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Asset Information</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL__ASSET_INFORMATION = IDENTIFIABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Asset</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL__ASSET = IDENTIFIABLE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL__SUBMODEL = IDENTIFIABLE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Endpoint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL__ENDPOINT = IDENTIFIABLE_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Asset Administration Shell</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL_FEATURE_COUNT = IDENTIFIABLE_FEATURE_COUNT + 7;

	/**
	 * The number of operations of the '<em>Asset Administration Shell</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL_OPERATION_COUNT = IDENTIFIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.LangStringImpl <em>Lang String</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.LangStringImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getLangString()
	 * @generated
	 */
	int LANG_STRING = 3;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANG_STRING__LANG = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANG_STRING__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Lang String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANG_STRING_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Lang String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANG_STRING_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.AdministrativeInformationImpl <em>Administrative Information</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.AdministrativeInformationImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getAdministrativeInformation()
	 * @generated
	 */
	int ADMINISTRATIVE_INFORMATION = 4;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMINISTRATIVE_INFORMATION__VERSION = 0;

	/**
	 * The feature id for the '<em><b>Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMINISTRATIVE_INFORMATION__REVISION = 1;

	/**
	 * The number of structural features of the '<em>Administrative Information</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMINISTRATIVE_INFORMATION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Administrative Information</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMINISTRATIVE_INFORMATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.IdentifierImpl <em>Identifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.IdentifierImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getIdentifier()
	 * @generated
	 */
	int IDENTIFIER = 5;

	/**
	 * The feature id for the '<em><b>Id Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIER__ID_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIER__ID = 1;

	/**
	 * The number of structural features of the '<em>Identifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIER_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Identifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.HasDataSpecificationImpl <em>Has Data Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.HasDataSpecificationImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getHasDataSpecification()
	 * @generated
	 */
	int HAS_DATA_SPECIFICATION = 6;

	/**
	 * The feature id for the '<em><b>Data Specification</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_DATA_SPECIFICATION__DATA_SPECIFICATION = 0;

	/**
	 * The number of structural features of the '<em>Has Data Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_DATA_SPECIFICATION_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Has Data Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_DATA_SPECIFICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.ReferenceImpl <em>Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.ReferenceImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getReference()
	 * @generated
	 */
	int REFERENCE = 7;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE__KEY = 0;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE__BASE_CLASS = 1;

	/**
	 * The number of structural features of the '<em>Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.KeyImpl <em>Key</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.KeyImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getKey()
	 * @generated
	 */
	int KEY = 8;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY__TYPE = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY__VALUE = 1;

	/**
	 * The feature id for the '<em><b>Id Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY__ID_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY__BASE_CLASS = 3;

	/**
	 * The number of structural features of the '<em>Key</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Key</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.SecurityImpl <em>Security</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.SecurityImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getSecurity()
	 * @generated
	 */
	int SECURITY = 9;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY__BASE_CLASS = 0;

	/**
	 * The feature id for the '<em><b>Access Control Policy Points</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY__ACCESS_CONTROL_POLICY_POINTS = 1;

	/**
	 * The feature id for the '<em><b>Certificate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY__CERTIFICATE = 2;

	/**
	 * The feature id for the '<em><b>Required Certificate Extension</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY__REQUIRED_CERTIFICATE_EXTENSION = 3;

	/**
	 * The number of structural features of the '<em>Security</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Security</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.AccessControlPolicyPointsImpl <em>Access Control Policy Points</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.AccessControlPolicyPointsImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getAccessControlPolicyPoints()
	 * @generated
	 */
	int ACCESS_CONTROL_POLICY_POINTS = 10;

	/**
	 * The feature id for the '<em><b>Local Access Control</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONTROL_POLICY_POINTS__LOCAL_ACCESS_CONTROL = 0;

	/**
	 * The feature id for the '<em><b>External Access Control</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONTROL_POLICY_POINTS__EXTERNAL_ACCESS_CONTROL = 1;

	/**
	 * The feature id for the '<em><b>External Information Points</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONTROL_POLICY_POINTS__EXTERNAL_INFORMATION_POINTS = 2;

	/**
	 * The feature id for the '<em><b>Internal Information Point</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONTROL_POLICY_POINTS__INTERNAL_INFORMATION_POINT = 3;

	/**
	 * The feature id for the '<em><b>External Policy Decision Points</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONTROL_POLICY_POINTS__EXTERNAL_POLICY_DECISION_POINTS = 4;

	/**
	 * The feature id for the '<em><b>External Policy Enforcement Point</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONTROL_POLICY_POINTS__EXTERNAL_POLICY_ENFORCEMENT_POINT = 5;

	/**
	 * The number of structural features of the '<em>Access Control Policy Points</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONTROL_POLICY_POINTS_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Access Control Policy Points</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONTROL_POLICY_POINTS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.AccessControlImpl <em>Access Control</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.AccessControlImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getAccessControl()
	 * @generated
	 */
	int ACCESS_CONTROL = 11;

	/**
	 * The number of structural features of the '<em>Access Control</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONTROL_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Access Control</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONTROL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.SubmodelImpl <em>Submodel</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.SubmodelImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getSubmodel()
	 * @generated
	 */
	int SUBMODEL = 12;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL__ID_SHORT = IDENTIFIABLE__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL__CATEGORY = IDENTIFIABLE__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL__DESCRIPTION = IDENTIFIABLE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL__BASE_CLASS = IDENTIFIABLE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Administration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL__ADMINISTRATION = IDENTIFIABLE__ADMINISTRATION;

	/**
	 * The feature id for the '<em><b>Identification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL__IDENTIFICATION = IDENTIFIABLE__IDENTIFICATION;

	/**
	 * The feature id for the '<em><b>Base Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL__BASE_PACKAGE = IDENTIFIABLE__BASE_PACKAGE;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL__KIND = IDENTIFIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base Has Kind Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL__BASE_HAS_KIND_CLASS = IDENTIFIABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL__SEMANTIC_ID = IDENTIFIABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Base Has Semantics Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL__BASE_HAS_SEMANTICS_CLASS = IDENTIFIABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Data Specification</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL__DATA_SPECIFICATION = IDENTIFIABLE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Submodelelement</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL__SUBMODELELEMENT = IDENTIFIABLE_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Submodel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_FEATURE_COUNT = IDENTIFIABLE_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Submodel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_OPERATION_COUNT = IDENTIFIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.HasKindImpl <em>Has Kind</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.HasKindImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getHasKind()
	 * @generated
	 */
	int HAS_KIND = 13;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KIND__KIND = 0;

	/**
	 * The feature id for the '<em><b>Base Has Kind Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KIND__BASE_HAS_KIND_CLASS = 1;

	/**
	 * The number of structural features of the '<em>Has Kind</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KIND_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Has Kind</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_KIND_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.HasSemanticsImpl <em>Has Semantics</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.HasSemanticsImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getHasSemantics()
	 * @generated
	 */
	int HAS_SEMANTICS = 14;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_SEMANTICS__SEMANTIC_ID = 0;

	/**
	 * The feature id for the '<em><b>Base Has Semantics Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_SEMANTICS__BASE_HAS_SEMANTICS_CLASS = 1;

	/**
	 * The number of structural features of the '<em>Has Semantics</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_SEMANTICS_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Has Semantics</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_SEMANTICS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.SubmodelElementImpl <em>Submodel Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.SubmodelElementImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getSubmodelElement()
	 * @generated
	 */
	int SUBMODEL_ELEMENT = 15;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT__ID_SHORT = REFERABLE__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT__CATEGORY = REFERABLE__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT__DESCRIPTION = REFERABLE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT__BASE_CLASS = REFERABLE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT__KIND = REFERABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base Has Kind Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT__BASE_HAS_KIND_CLASS = REFERABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT__SEMANTIC_ID = REFERABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Base Has Semantics Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT__BASE_HAS_SEMANTICS_CLASS = REFERABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Data Specification</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT__DATA_SPECIFICATION = REFERABLE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Is Dynamic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT__IS_DYNAMIC = REFERABLE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>End Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT__END_POINT = REFERABLE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT__NODE_ID = REFERABLE_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Submodel Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_FEATURE_COUNT = REFERABLE_FEATURE_COUNT + 8;

	/**
	 * The number of operations of the '<em>Submodel Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_OPERATION_COUNT = REFERABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.EndpointImpl <em>Endpoint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.EndpointImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getEndpoint()
	 * @generated
	 */
	int ENDPOINT = 16;

	/**
	 * The feature id for the '<em><b>Address</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENDPOINT__ADDRESS = 0;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENDPOINT__PROTOCOL = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENDPOINT__NAME = 2;

	/**
	 * The number of structural features of the '<em>Endpoint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENDPOINT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Endpoint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENDPOINT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.NodeIdImpl <em>Node Id</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.NodeIdImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getNodeId()
	 * @generated
	 */
	int NODE_ID = 17;

	/**
	 * The feature id for the '<em><b>Name Space Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_ID__NAME_SPACE_INDEX = 0;

	/**
	 * The feature id for the '<em><b>Identifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_ID__IDENTIFIER = 1;

	/**
	 * The feature id for the '<em><b>Id Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_ID__ID_TYPE = 2;

	/**
	 * The number of structural features of the '<em>Node Id</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_ID_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Node Id</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_ID_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.CertificateImpl <em>Certificate</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.CertificateImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getCertificate()
	 * @generated
	 */
	int CERTIFICATE = 18;

	/**
	 * The number of structural features of the '<em>Certificate</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFICATE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Certificate</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFICATE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.AssetInformationImpl <em>Asset Information</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.AssetInformationImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getAssetInformation()
	 * @generated
	 */
	int ASSET_INFORMATION = 19;

	/**
	 * The feature id for the '<em><b>Asset Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_INFORMATION__ASSET_KIND = 0;

	/**
	 * The feature id for the '<em><b>Global Asset Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_INFORMATION__GLOBAL_ASSET_ID = 1;

	/**
	 * The feature id for the '<em><b>Specific Asset Id</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_INFORMATION__SPECIFIC_ASSET_ID = 2;

	/**
	 * The feature id for the '<em><b>Bill Of Material</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_INFORMATION__BILL_OF_MATERIAL = 3;

	/**
	 * The feature id for the '<em><b>Default Thumbnail</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_INFORMATION__DEFAULT_THUMBNAIL = 4;

	/**
	 * The number of structural features of the '<em>Asset Information</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_INFORMATION_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Asset Information</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_INFORMATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.IdentifierKeyValuePairImpl <em>Identifier Key Value Pair</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.IdentifierKeyValuePairImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getIdentifierKeyValuePair()
	 * @generated
	 */
	int IDENTIFIER_KEY_VALUE_PAIR = 20;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIER_KEY_VALUE_PAIR__SEMANTIC_ID = HAS_SEMANTICS__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Base Has Semantics Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIER_KEY_VALUE_PAIR__BASE_HAS_SEMANTICS_CLASS = HAS_SEMANTICS__BASE_HAS_SEMANTICS_CLASS;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIER_KEY_VALUE_PAIR__KEY = HAS_SEMANTICS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIER_KEY_VALUE_PAIR__VALUE = HAS_SEMANTICS_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>External Subject Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIER_KEY_VALUE_PAIR__EXTERNAL_SUBJECT_ID = HAS_SEMANTICS_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Identifier Key Value Pair</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIER_KEY_VALUE_PAIR_FEATURE_COUNT = HAS_SEMANTICS_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Identifier Key Value Pair</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIER_KEY_VALUE_PAIR_OPERATION_COUNT = HAS_SEMANTICS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.DataElementImpl <em>Data Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.DataElementImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getDataElement()
	 * @generated
	 */
	int DATA_ELEMENT = 22;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT__ID_SHORT = SUBMODEL_ELEMENT__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT__CATEGORY = SUBMODEL_ELEMENT__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT__DESCRIPTION = SUBMODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT__BASE_CLASS = SUBMODEL_ELEMENT__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT__KIND = SUBMODEL_ELEMENT__KIND;

	/**
	 * The feature id for the '<em><b>Base Has Kind Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT__BASE_HAS_KIND_CLASS = SUBMODEL_ELEMENT__BASE_HAS_KIND_CLASS;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT__SEMANTIC_ID = SUBMODEL_ELEMENT__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Base Has Semantics Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT__BASE_HAS_SEMANTICS_CLASS = SUBMODEL_ELEMENT__BASE_HAS_SEMANTICS_CLASS;

	/**
	 * The feature id for the '<em><b>Data Specification</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT__DATA_SPECIFICATION = SUBMODEL_ELEMENT__DATA_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Is Dynamic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT__IS_DYNAMIC = SUBMODEL_ELEMENT__IS_DYNAMIC;

	/**
	 * The feature id for the '<em><b>End Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT__END_POINT = SUBMODEL_ELEMENT__END_POINT;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT__NODE_ID = SUBMODEL_ELEMENT__NODE_ID;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT__BASE_PROPERTY = SUBMODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Data Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT_FEATURE_COUNT = SUBMODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Data Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT_OPERATION_COUNT = SUBMODEL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.FileImpl <em>File</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.FileImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getFile()
	 * @generated
	 */
	int FILE = 21;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__ID_SHORT = DATA_ELEMENT__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__CATEGORY = DATA_ELEMENT__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__DESCRIPTION = DATA_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__BASE_CLASS = DATA_ELEMENT__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__KIND = DATA_ELEMENT__KIND;

	/**
	 * The feature id for the '<em><b>Base Has Kind Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__BASE_HAS_KIND_CLASS = DATA_ELEMENT__BASE_HAS_KIND_CLASS;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__SEMANTIC_ID = DATA_ELEMENT__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Base Has Semantics Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__BASE_HAS_SEMANTICS_CLASS = DATA_ELEMENT__BASE_HAS_SEMANTICS_CLASS;

	/**
	 * The feature id for the '<em><b>Data Specification</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__DATA_SPECIFICATION = DATA_ELEMENT__DATA_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Is Dynamic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__IS_DYNAMIC = DATA_ELEMENT__IS_DYNAMIC;

	/**
	 * The feature id for the '<em><b>End Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__END_POINT = DATA_ELEMENT__END_POINT;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__NODE_ID = DATA_ELEMENT__NODE_ID;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__BASE_PROPERTY = DATA_ELEMENT__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__PATH = DATA_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Mime Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__MIME_TYPE = DATA_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>File</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_FEATURE_COUNT = DATA_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>File</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_OPERATION_COUNT = DATA_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.AssetImpl <em>Asset</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.AssetImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getAsset()
	 * @generated
	 */
	int ASSET = 23;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__ID_SHORT = IDENTIFIABLE__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__CATEGORY = IDENTIFIABLE__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__DESCRIPTION = IDENTIFIABLE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__BASE_CLASS = IDENTIFIABLE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Administration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__ADMINISTRATION = IDENTIFIABLE__ADMINISTRATION;

	/**
	 * The feature id for the '<em><b>Identification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__IDENTIFICATION = IDENTIFIABLE__IDENTIFICATION;

	/**
	 * The feature id for the '<em><b>Base Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__BASE_PACKAGE = IDENTIFIABLE__BASE_PACKAGE;

	/**
	 * The feature id for the '<em><b>Data Specification</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__DATA_SPECIFICATION = IDENTIFIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__KIND = IDENTIFIABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Endpoint</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__ENDPOINT = IDENTIFIABLE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Asset</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_FEATURE_COUNT = IDENTIFIABLE_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Asset</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_OPERATION_COUNT = IDENTIFIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.AASEndpointImpl <em>Endpoint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.AASEndpointImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getAASEndpoint()
	 * @generated
	 */
	int AAS_ENDPOINT = 24;

	/**
	 * The feature id for the '<em><b>Address</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AAS_ENDPOINT__ADDRESS = 0;

	/**
	 * The feature id for the '<em><b>Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AAS_ENDPOINT__PORT = 1;

	/**
	 * The feature id for the '<em><b>Security</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AAS_ENDPOINT__SECURITY = 2;

	/**
	 * The number of structural features of the '<em>Endpoint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AAS_ENDPOINT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Endpoint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AAS_ENDPOINT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.EntityImpl <em>Entity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.EntityImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getEntity()
	 * @generated
	 */
	int ENTITY = 25;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__ID_SHORT = SUBMODEL_ELEMENT__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__CATEGORY = SUBMODEL_ELEMENT__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__DESCRIPTION = SUBMODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__BASE_CLASS = SUBMODEL_ELEMENT__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__KIND = SUBMODEL_ELEMENT__KIND;

	/**
	 * The feature id for the '<em><b>Base Has Kind Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__BASE_HAS_KIND_CLASS = SUBMODEL_ELEMENT__BASE_HAS_KIND_CLASS;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__SEMANTIC_ID = SUBMODEL_ELEMENT__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Base Has Semantics Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__BASE_HAS_SEMANTICS_CLASS = SUBMODEL_ELEMENT__BASE_HAS_SEMANTICS_CLASS;

	/**
	 * The feature id for the '<em><b>Data Specification</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__DATA_SPECIFICATION = SUBMODEL_ELEMENT__DATA_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Is Dynamic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__IS_DYNAMIC = SUBMODEL_ELEMENT__IS_DYNAMIC;

	/**
	 * The feature id for the '<em><b>End Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__END_POINT = SUBMODEL_ELEMENT__END_POINT;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__NODE_ID = SUBMODEL_ELEMENT__NODE_ID;

	/**
	 * The feature id for the '<em><b>Entity Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__ENTITY_TYPE = SUBMODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Asset</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__ASSET = SUBMODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__BASE_PROPERTY = SUBMODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_FEATURE_COUNT = SUBMODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_OPERATION_COUNT = SUBMODEL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.RelationshipElementImpl <em>Relationship Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.RelationshipElementImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getRelationshipElement()
	 * @generated
	 */
	int RELATIONSHIP_ELEMENT = 26;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT__ID_SHORT = SUBMODEL_ELEMENT__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT__CATEGORY = SUBMODEL_ELEMENT__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT__DESCRIPTION = SUBMODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT__BASE_CLASS = SUBMODEL_ELEMENT__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT__KIND = SUBMODEL_ELEMENT__KIND;

	/**
	 * The feature id for the '<em><b>Base Has Kind Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT__BASE_HAS_KIND_CLASS = SUBMODEL_ELEMENT__BASE_HAS_KIND_CLASS;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT__SEMANTIC_ID = SUBMODEL_ELEMENT__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Base Has Semantics Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT__BASE_HAS_SEMANTICS_CLASS = SUBMODEL_ELEMENT__BASE_HAS_SEMANTICS_CLASS;

	/**
	 * The feature id for the '<em><b>Data Specification</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT__DATA_SPECIFICATION = SUBMODEL_ELEMENT__DATA_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Is Dynamic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT__IS_DYNAMIC = SUBMODEL_ELEMENT__IS_DYNAMIC;

	/**
	 * The feature id for the '<em><b>End Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT__END_POINT = SUBMODEL_ELEMENT__END_POINT;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT__NODE_ID = SUBMODEL_ELEMENT__NODE_ID;

	/**
	 * The feature id for the '<em><b>Base Dependency</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT__BASE_DEPENDENCY = SUBMODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Relationship Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT_FEATURE_COUNT = SUBMODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Relationship Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT_OPERATION_COUNT = SUBMODEL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.OperationImpl <em>Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.OperationImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getOperation()
	 * @generated
	 */
	int OPERATION = 27;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__ID_SHORT = SUBMODEL_ELEMENT__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__CATEGORY = SUBMODEL_ELEMENT__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__DESCRIPTION = SUBMODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__BASE_CLASS = SUBMODEL_ELEMENT__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__KIND = SUBMODEL_ELEMENT__KIND;

	/**
	 * The feature id for the '<em><b>Base Has Kind Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__BASE_HAS_KIND_CLASS = SUBMODEL_ELEMENT__BASE_HAS_KIND_CLASS;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__SEMANTIC_ID = SUBMODEL_ELEMENT__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Base Has Semantics Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__BASE_HAS_SEMANTICS_CLASS = SUBMODEL_ELEMENT__BASE_HAS_SEMANTICS_CLASS;

	/**
	 * The feature id for the '<em><b>Data Specification</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__DATA_SPECIFICATION = SUBMODEL_ELEMENT__DATA_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Is Dynamic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__IS_DYNAMIC = SUBMODEL_ELEMENT__IS_DYNAMIC;

	/**
	 * The feature id for the '<em><b>End Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__END_POINT = SUBMODEL_ELEMENT__END_POINT;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__NODE_ID = SUBMODEL_ELEMENT__NODE_ID;

	/**
	 * The feature id for the '<em><b>Base Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__BASE_OPERATION = SUBMODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_FEATURE_COUNT = SUBMODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_OPERATION_COUNT = SUBMODEL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.SubmodelElementCollectionImpl <em>Submodel Element Collection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.SubmodelElementCollectionImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getSubmodelElementCollection()
	 * @generated
	 */
	int SUBMODEL_ELEMENT_COLLECTION = 28;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION__ID_SHORT = SUBMODEL_ELEMENT__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION__CATEGORY = SUBMODEL_ELEMENT__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION__DESCRIPTION = SUBMODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION__BASE_CLASS = SUBMODEL_ELEMENT__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION__KIND = SUBMODEL_ELEMENT__KIND;

	/**
	 * The feature id for the '<em><b>Base Has Kind Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION__BASE_HAS_KIND_CLASS = SUBMODEL_ELEMENT__BASE_HAS_KIND_CLASS;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION__SEMANTIC_ID = SUBMODEL_ELEMENT__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Base Has Semantics Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION__BASE_HAS_SEMANTICS_CLASS = SUBMODEL_ELEMENT__BASE_HAS_SEMANTICS_CLASS;

	/**
	 * The feature id for the '<em><b>Data Specification</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION__DATA_SPECIFICATION = SUBMODEL_ELEMENT__DATA_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Is Dynamic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION__IS_DYNAMIC = SUBMODEL_ELEMENT__IS_DYNAMIC;

	/**
	 * The feature id for the '<em><b>End Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION__END_POINT = SUBMODEL_ELEMENT__END_POINT;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION__NODE_ID = SUBMODEL_ELEMENT__NODE_ID;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION__VALUE = SUBMODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION__BASE_PROPERTY = SUBMODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Base Data Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION__BASE_DATA_TYPE = SUBMODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Ordered</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION__ORDERED = SUBMODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Allow Duplicates</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION__ALLOW_DUPLICATES = SUBMODEL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Submodel Element Collection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION_FEATURE_COUNT = SUBMODEL_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Submodel Element Collection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION_OPERATION_COUNT = SUBMODEL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.PropertyImpl <em>Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.PropertyImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getProperty()
	 * @generated
	 */
	int PROPERTY = 29;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__ID_SHORT = DATA_ELEMENT__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__CATEGORY = DATA_ELEMENT__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__DESCRIPTION = DATA_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__BASE_CLASS = DATA_ELEMENT__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__KIND = DATA_ELEMENT__KIND;

	/**
	 * The feature id for the '<em><b>Base Has Kind Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__BASE_HAS_KIND_CLASS = DATA_ELEMENT__BASE_HAS_KIND_CLASS;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__SEMANTIC_ID = DATA_ELEMENT__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Base Has Semantics Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__BASE_HAS_SEMANTICS_CLASS = DATA_ELEMENT__BASE_HAS_SEMANTICS_CLASS;

	/**
	 * The feature id for the '<em><b>Data Specification</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__DATA_SPECIFICATION = DATA_ELEMENT__DATA_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Is Dynamic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__IS_DYNAMIC = DATA_ELEMENT__IS_DYNAMIC;

	/**
	 * The feature id for the '<em><b>End Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__END_POINT = DATA_ELEMENT__END_POINT;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__NODE_ID = DATA_ELEMENT__NODE_ID;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__BASE_PROPERTY = DATA_ELEMENT__BASE_PROPERTY;

	/**
	 * The number of structural features of the '<em>Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_FEATURE_COUNT = DATA_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_OPERATION_COUNT = DATA_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.ReferenceElementImpl <em>Reference Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.ReferenceElementImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getReferenceElement()
	 * @generated
	 */
	int REFERENCE_ELEMENT = 30;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT__ID_SHORT = DATA_ELEMENT__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT__CATEGORY = DATA_ELEMENT__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT__DESCRIPTION = DATA_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT__BASE_CLASS = DATA_ELEMENT__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT__KIND = DATA_ELEMENT__KIND;

	/**
	 * The feature id for the '<em><b>Base Has Kind Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT__BASE_HAS_KIND_CLASS = DATA_ELEMENT__BASE_HAS_KIND_CLASS;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT__SEMANTIC_ID = DATA_ELEMENT__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Base Has Semantics Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT__BASE_HAS_SEMANTICS_CLASS = DATA_ELEMENT__BASE_HAS_SEMANTICS_CLASS;

	/**
	 * The feature id for the '<em><b>Data Specification</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT__DATA_SPECIFICATION = DATA_ELEMENT__DATA_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Is Dynamic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT__IS_DYNAMIC = DATA_ELEMENT__IS_DYNAMIC;

	/**
	 * The feature id for the '<em><b>End Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT__END_POINT = DATA_ELEMENT__END_POINT;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT__NODE_ID = DATA_ELEMENT__NODE_ID;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT__BASE_PROPERTY = DATA_ELEMENT__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT__VALUE = DATA_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Reference Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT_FEATURE_COUNT = DATA_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Reference Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT_OPERATION_COUNT = DATA_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.EventImpl <em>Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.EventImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getEvent()
	 * @generated
	 */
	int EVENT = 31;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__ID_SHORT = SUBMODEL_ELEMENT__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__CATEGORY = SUBMODEL_ELEMENT__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__DESCRIPTION = SUBMODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__BASE_CLASS = SUBMODEL_ELEMENT__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__KIND = SUBMODEL_ELEMENT__KIND;

	/**
	 * The feature id for the '<em><b>Base Has Kind Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__BASE_HAS_KIND_CLASS = SUBMODEL_ELEMENT__BASE_HAS_KIND_CLASS;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__SEMANTIC_ID = SUBMODEL_ELEMENT__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Base Has Semantics Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__BASE_HAS_SEMANTICS_CLASS = SUBMODEL_ELEMENT__BASE_HAS_SEMANTICS_CLASS;

	/**
	 * The feature id for the '<em><b>Data Specification</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__DATA_SPECIFICATION = SUBMODEL_ELEMENT__DATA_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Is Dynamic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__IS_DYNAMIC = SUBMODEL_ELEMENT__IS_DYNAMIC;

	/**
	 * The feature id for the '<em><b>End Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__END_POINT = SUBMODEL_ELEMENT__END_POINT;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__NODE_ID = SUBMODEL_ELEMENT__NODE_ID;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__BASE_PROPERTY = SUBMODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FEATURE_COUNT = SUBMODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OPERATION_COUNT = SUBMODEL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.BasicEventImpl <em>Basic Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.BasicEventImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getBasicEvent()
	 * @generated
	 */
	int BASIC_EVENT = 32;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT__ID_SHORT = EVENT__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT__CATEGORY = EVENT__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT__DESCRIPTION = EVENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT__BASE_CLASS = EVENT__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT__KIND = EVENT__KIND;

	/**
	 * The feature id for the '<em><b>Base Has Kind Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT__BASE_HAS_KIND_CLASS = EVENT__BASE_HAS_KIND_CLASS;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT__SEMANTIC_ID = EVENT__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Base Has Semantics Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT__BASE_HAS_SEMANTICS_CLASS = EVENT__BASE_HAS_SEMANTICS_CLASS;

	/**
	 * The feature id for the '<em><b>Data Specification</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT__DATA_SPECIFICATION = EVENT__DATA_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Is Dynamic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT__IS_DYNAMIC = EVENT__IS_DYNAMIC;

	/**
	 * The feature id for the '<em><b>End Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT__END_POINT = EVENT__END_POINT;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT__NODE_ID = EVENT__NODE_ID;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT__BASE_PROPERTY = EVENT__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Observed</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT__OBSERVED = EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Basic Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT_FEATURE_COUNT = EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Basic Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT_OPERATION_COUNT = EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.RangeImpl <em>Range</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.RangeImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getRange()
	 * @generated
	 */
	int RANGE = 33;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE__ID_SHORT = DATA_ELEMENT__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE__CATEGORY = DATA_ELEMENT__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE__DESCRIPTION = DATA_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE__BASE_CLASS = DATA_ELEMENT__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE__KIND = DATA_ELEMENT__KIND;

	/**
	 * The feature id for the '<em><b>Base Has Kind Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE__BASE_HAS_KIND_CLASS = DATA_ELEMENT__BASE_HAS_KIND_CLASS;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE__SEMANTIC_ID = DATA_ELEMENT__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Base Has Semantics Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE__BASE_HAS_SEMANTICS_CLASS = DATA_ELEMENT__BASE_HAS_SEMANTICS_CLASS;

	/**
	 * The feature id for the '<em><b>Data Specification</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE__DATA_SPECIFICATION = DATA_ELEMENT__DATA_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Is Dynamic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE__IS_DYNAMIC = DATA_ELEMENT__IS_DYNAMIC;

	/**
	 * The feature id for the '<em><b>End Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE__END_POINT = DATA_ELEMENT__END_POINT;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE__NODE_ID = DATA_ELEMENT__NODE_ID;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE__BASE_PROPERTY = DATA_ELEMENT__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE__MIN = DATA_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE__MAX = DATA_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Range</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_FEATURE_COUNT = DATA_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Range</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_OPERATION_COUNT = DATA_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.CapabilityImpl <em>Capability</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.CapabilityImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getCapability()
	 * @generated
	 */
	int CAPABILITY = 34;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY__ID_SHORT = SUBMODEL_ELEMENT__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY__CATEGORY = SUBMODEL_ELEMENT__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY__DESCRIPTION = SUBMODEL_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY__BASE_CLASS = SUBMODEL_ELEMENT__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY__KIND = SUBMODEL_ELEMENT__KIND;

	/**
	 * The feature id for the '<em><b>Base Has Kind Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY__BASE_HAS_KIND_CLASS = SUBMODEL_ELEMENT__BASE_HAS_KIND_CLASS;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY__SEMANTIC_ID = SUBMODEL_ELEMENT__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Base Has Semantics Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY__BASE_HAS_SEMANTICS_CLASS = SUBMODEL_ELEMENT__BASE_HAS_SEMANTICS_CLASS;

	/**
	 * The feature id for the '<em><b>Data Specification</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY__DATA_SPECIFICATION = SUBMODEL_ELEMENT__DATA_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Is Dynamic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY__IS_DYNAMIC = SUBMODEL_ELEMENT__IS_DYNAMIC;

	/**
	 * The feature id for the '<em><b>End Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY__END_POINT = SUBMODEL_ELEMENT__END_POINT;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY__NODE_ID = SUBMODEL_ELEMENT__NODE_ID;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY__BASE_PROPERTY = SUBMODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Capability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY_FEATURE_COUNT = SUBMODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Capability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY_OPERATION_COUNT = SUBMODEL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.ConceptDescriptionImpl <em>Concept Description</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.ConceptDescriptionImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getConceptDescription()
	 * @generated
	 */
	int CONCEPT_DESCRIPTION = 35;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTION__ID_SHORT = IDENTIFIABLE__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTION__CATEGORY = IDENTIFIABLE__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTION__DESCRIPTION = IDENTIFIABLE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTION__BASE_CLASS = IDENTIFIABLE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Administration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTION__ADMINISTRATION = IDENTIFIABLE__ADMINISTRATION;

	/**
	 * The feature id for the '<em><b>Identification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTION__IDENTIFICATION = IDENTIFIABLE__IDENTIFICATION;

	/**
	 * The feature id for the '<em><b>Base Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTION__BASE_PACKAGE = IDENTIFIABLE__BASE_PACKAGE;

	/**
	 * The feature id for the '<em><b>Data Specification</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTION__DATA_SPECIFICATION = IDENTIFIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Is Case Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTION__IS_CASE_OF = IDENTIFIABLE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Concept Description</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTION_FEATURE_COUNT = IDENTIFIABLE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Concept Description</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTION_OPERATION_COUNT = IDENTIFIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.LangStringSetImpl <em>Lang String Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.LangStringSetImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getLangStringSet()
	 * @generated
	 */
	int LANG_STRING_SET = 36;

	/**
	 * The number of structural features of the '<em>Lang String Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANG_STRING_SET_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Lang String Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANG_STRING_SET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.DataSpecificationContentImpl <em>Data Specification Content</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.DataSpecificationContentImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getDataSpecificationContent()
	 * @generated
	 */
	int DATA_SPECIFICATION_CONTENT = 37;

	/**
	 * The number of structural features of the '<em>Data Specification Content</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_CONTENT_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Data Specification Content</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_CONTENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.DataSpecificationIEC61360Impl <em>Data Specification IEC61360</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.DataSpecificationIEC61360Impl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getDataSpecificationIEC61360()
	 * @generated
	 */
	int DATA_SPECIFICATION_IEC61360 = 38;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61360__BASE_CLASS = 0;

	/**
	 * The feature id for the '<em><b>Preferred Name</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61360__PREFERRED_NAME = 1;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61360__SHORT_NAME = 2;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61360__UNIT = 3;

	/**
	 * The feature id for the '<em><b>Unit Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61360__UNIT_ID = 4;

	/**
	 * The feature id for the '<em><b>Source Of Definition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61360__SOURCE_OF_DEFINITION = 5;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61360__SYMBOL = 6;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61360__DATA_TYPE = 7;

	/**
	 * The feature id for the '<em><b>Definition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61360__DEFINITION = 8;

	/**
	 * The feature id for the '<em><b>Value Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61360__VALUE_FORMAT = 9;

	/**
	 * The feature id for the '<em><b>Value List</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61360__VALUE_LIST = 10;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61360__VALUE = 11;

	/**
	 * The feature id for the '<em><b>Value Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61360__VALUE_ID = 12;

	/**
	 * The feature id for the '<em><b>Level Type</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61360__LEVEL_TYPE = 13;

	/**
	 * The number of structural features of the '<em>Data Specification IEC61360</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61360_FEATURE_COUNT = 14;

	/**
	 * The number of operations of the '<em>Data Specification IEC61360</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61360_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.ValueReferencePairTypeImpl <em>Value Reference Pair Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.ValueReferencePairTypeImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getValueReferencePairType()
	 * @generated
	 */
	int VALUE_REFERENCE_PAIR_TYPE = 39;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_REFERENCE_PAIR_TYPE__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Value Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_REFERENCE_PAIR_TYPE__VALUE_ID = 1;

	/**
	 * The number of structural features of the '<em>Value Reference Pair Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_REFERENCE_PAIR_TYPE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Value Reference Pair Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_REFERENCE_PAIR_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.ViewImpl <em>View</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.ViewImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getView()
	 * @generated
	 */
	int VIEW = 40;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEW__ID_SHORT = REFERABLE__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEW__CATEGORY = REFERABLE__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEW__DESCRIPTION = REFERABLE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEW__BASE_CLASS = REFERABLE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEW__SEMANTIC_ID = REFERABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base Has Semantics Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEW__BASE_HAS_SEMANTICS_CLASS = REFERABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Data Specification</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEW__DATA_SPECIFICATION = REFERABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Contained Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEW__CONTAINED_ELEMENT = REFERABLE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEW_FEATURE_COUNT = REFERABLE_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEW_OPERATION_COUNT = REFERABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.impl.MultiLanguagePropertyImpl <em>Multi Language Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.impl.MultiLanguagePropertyImpl
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getMultiLanguageProperty()
	 * @generated
	 */
	int MULTI_LANGUAGE_PROPERTY = 41;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY__ID_SHORT = DATA_ELEMENT__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY__CATEGORY = DATA_ELEMENT__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY__DESCRIPTION = DATA_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY__BASE_CLASS = DATA_ELEMENT__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY__KIND = DATA_ELEMENT__KIND;

	/**
	 * The feature id for the '<em><b>Base Has Kind Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY__BASE_HAS_KIND_CLASS = DATA_ELEMENT__BASE_HAS_KIND_CLASS;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY__SEMANTIC_ID = DATA_ELEMENT__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Base Has Semantics Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY__BASE_HAS_SEMANTICS_CLASS = DATA_ELEMENT__BASE_HAS_SEMANTICS_CLASS;

	/**
	 * The feature id for the '<em><b>Data Specification</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY__DATA_SPECIFICATION = DATA_ELEMENT__DATA_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Is Dynamic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY__IS_DYNAMIC = DATA_ELEMENT__IS_DYNAMIC;

	/**
	 * The feature id for the '<em><b>End Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY__END_POINT = DATA_ELEMENT__END_POINT;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY__NODE_ID = DATA_ELEMENT__NODE_ID;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY__BASE_PROPERTY = DATA_ELEMENT__BASE_PROPERTY;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY__VALUE = DATA_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY__VALUE_ID = DATA_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Multi Language Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY_FEATURE_COUNT = DATA_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Multi Language Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY_OPERATION_COUNT = DATA_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.IdentifierType <em>Identifier Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.IdentifierType
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getIdentifierType()
	 * @generated
	 */
	int IDENTIFIER_TYPE = 42;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.LangEnum <em>Lang Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.LangEnum
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getLangEnum()
	 * @generated
	 */
	int LANG_ENUM = 43;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.KeyElements <em>Key Elements</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.KeyElements
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getKeyElements()
	 * @generated
	 */
	int KEY_ELEMENTS = 44;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.ReferableElements <em>Referable Elements</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.ReferableElements
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getReferableElements()
	 * @generated
	 */
	int REFERABLE_ELEMENTS = 45;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.IdentifiableElement <em>Identifiable Element</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.IdentifiableElement
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getIdentifiableElement()
	 * @generated
	 */
	int IDENTIFIABLE_ELEMENT = 46;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.KeyType <em>Key Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.KeyType
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getKeyType()
	 * @generated
	 */
	int KEY_TYPE = 47;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.LocalKeyType <em>Local Key Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.LocalKeyType
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getLocalKeyType()
	 * @generated
	 */
	int LOCAL_KEY_TYPE = 48;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.ModelingKind <em>Modeling Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.ModelingKind
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getModelingKind()
	 * @generated
	 */
	int MODELING_KIND = 49;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.ProtocolKind <em>Protocol Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.ProtocolKind
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getProtocolKind()
	 * @generated
	 */
	int PROTOCOL_KIND = 50;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.IdType <em>Id Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.IdType
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getIdType()
	 * @generated
	 */
	int ID_TYPE = 51;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.AssetKind <em>Asset Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.AssetKind
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getAssetKind()
	 * @generated
	 */
	int ASSET_KIND = 52;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.MimeType <em>Mime Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.MimeType
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getMimeType()
	 * @generated
	 */
	int MIME_TYPE = 53;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.SecurityKind <em>Security Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.SecurityKind
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getSecurityKind()
	 * @generated
	 */
	int SECURITY_KIND = 54;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.EntityType <em>Entity Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.EntityType
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getEntityType()
	 * @generated
	 */
	int ENTITY_TYPE = 55;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.LevelType <em>Level Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.LevelType
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getLevelType()
	 * @generated
	 */
	int LEVEL_TYPE = 56;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.aas.DataTypeIEC61360 <em>Data Type IEC61360</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.aas.DataTypeIEC61360
	 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getDataTypeIEC61360()
	 * @generated
	 */
	int DATA_TYPE_IEC61360 = 57;


	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.AssetAdministrationShell <em>Asset Administration Shell</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asset Administration Shell</em>'.
	 * @see org.eclipse.papyrus.aas.AssetAdministrationShell
	 * @generated
	 */
	EClass getAssetAdministrationShell();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.AssetAdministrationShell#getDerivedFrom <em>Derived From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Derived From</em>'.
	 * @see org.eclipse.papyrus.aas.AssetAdministrationShell#getDerivedFrom()
	 * @see #getAssetAdministrationShell()
	 * @generated
	 */
	EReference getAssetAdministrationShell_DerivedFrom();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.AssetAdministrationShell#getSecurity <em>Security</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Security</em>'.
	 * @see org.eclipse.papyrus.aas.AssetAdministrationShell#getSecurity()
	 * @see #getAssetAdministrationShell()
	 * @generated
	 */
	EReference getAssetAdministrationShell_Security();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.aas.AssetAdministrationShell#getAssetInformation <em>Asset Information</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Asset Information</em>'.
	 * @see org.eclipse.papyrus.aas.AssetAdministrationShell#getAssetInformation()
	 * @see #getAssetAdministrationShell()
	 * @generated
	 */
	EReference getAssetAdministrationShell_AssetInformation();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.AssetAdministrationShell#getAsset <em>Asset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Asset</em>'.
	 * @see org.eclipse.papyrus.aas.AssetAdministrationShell#getAsset()
	 * @see #getAssetAdministrationShell()
	 * @generated
	 */
	EReference getAssetAdministrationShell_Asset();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.aas.AssetAdministrationShell#getSubmodel <em>Submodel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Submodel</em>'.
	 * @see org.eclipse.papyrus.aas.AssetAdministrationShell#getSubmodel()
	 * @see #getAssetAdministrationShell()
	 * @generated
	 */
	EReference getAssetAdministrationShell_Submodel();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.aas.AssetAdministrationShell#getEndpoint <em>Endpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Endpoint</em>'.
	 * @see org.eclipse.papyrus.aas.AssetAdministrationShell#getEndpoint()
	 * @see #getAssetAdministrationShell()
	 * @generated
	 */
	EReference getAssetAdministrationShell_Endpoint();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.Identifiable <em>Identifiable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Identifiable</em>'.
	 * @see org.eclipse.papyrus.aas.Identifiable
	 * @generated
	 */
	EClass getIdentifiable();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.aas.Identifiable#getAdministration <em>Administration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Administration</em>'.
	 * @see org.eclipse.papyrus.aas.Identifiable#getAdministration()
	 * @see #getIdentifiable()
	 * @generated
	 */
	EReference getIdentifiable_Administration();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.aas.Identifiable#getIdentification <em>Identification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Identification</em>'.
	 * @see org.eclipse.papyrus.aas.Identifiable#getIdentification()
	 * @see #getIdentifiable()
	 * @generated
	 */
	EReference getIdentifiable_Identification();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.Identifiable#getBase_Package <em>Base Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Package</em>'.
	 * @see org.eclipse.papyrus.aas.Identifiable#getBase_Package()
	 * @see #getIdentifiable()
	 * @generated
	 */
	EReference getIdentifiable_Base_Package();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.Referable <em>Referable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Referable</em>'.
	 * @see org.eclipse.papyrus.aas.Referable
	 * @generated
	 */
	EClass getReferable();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.Referable#getIdShort <em>Id Short</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id Short</em>'.
	 * @see org.eclipse.papyrus.aas.Referable#getIdShort()
	 * @see #getReferable()
	 * @generated
	 */
	EAttribute getReferable_IdShort();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.Referable#getCategory <em>Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Category</em>'.
	 * @see org.eclipse.papyrus.aas.Referable#getCategory()
	 * @see #getReferable()
	 * @generated
	 */
	EAttribute getReferable_Category();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.aas.Referable#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Description</em>'.
	 * @see org.eclipse.papyrus.aas.Referable#getDescription()
	 * @see #getReferable()
	 * @generated
	 */
	EReference getReferable_Description();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.Referable#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.eclipse.papyrus.aas.Referable#getBase_Class()
	 * @see #getReferable()
	 * @generated
	 */
	EReference getReferable_Base_Class();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.LangString <em>Lang String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lang String</em>'.
	 * @see org.eclipse.papyrus.aas.LangString
	 * @generated
	 */
	EClass getLangString();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.LangString#getLang <em>Lang</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lang</em>'.
	 * @see org.eclipse.papyrus.aas.LangString#getLang()
	 * @see #getLangString()
	 * @generated
	 */
	EAttribute getLangString_Lang();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.LangString#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.papyrus.aas.LangString#getValue()
	 * @see #getLangString()
	 * @generated
	 */
	EAttribute getLangString_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.AdministrativeInformation <em>Administrative Information</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Administrative Information</em>'.
	 * @see org.eclipse.papyrus.aas.AdministrativeInformation
	 * @generated
	 */
	EClass getAdministrativeInformation();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.AdministrativeInformation#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.eclipse.papyrus.aas.AdministrativeInformation#getVersion()
	 * @see #getAdministrativeInformation()
	 * @generated
	 */
	EAttribute getAdministrativeInformation_Version();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.AdministrativeInformation#getRevision <em>Revision</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Revision</em>'.
	 * @see org.eclipse.papyrus.aas.AdministrativeInformation#getRevision()
	 * @see #getAdministrativeInformation()
	 * @generated
	 */
	EAttribute getAdministrativeInformation_Revision();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.Identifier <em>Identifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Identifier</em>'.
	 * @see org.eclipse.papyrus.aas.Identifier
	 * @generated
	 */
	EClass getIdentifier();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.Identifier#getIdType <em>Id Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id Type</em>'.
	 * @see org.eclipse.papyrus.aas.Identifier#getIdType()
	 * @see #getIdentifier()
	 * @generated
	 */
	EAttribute getIdentifier_IdType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.Identifier#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.eclipse.papyrus.aas.Identifier#getId()
	 * @see #getIdentifier()
	 * @generated
	 */
	EAttribute getIdentifier_Id();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.HasDataSpecification <em>Has Data Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Has Data Specification</em>'.
	 * @see org.eclipse.papyrus.aas.HasDataSpecification
	 * @generated
	 */
	EClass getHasDataSpecification();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.aas.HasDataSpecification#getDataSpecification <em>Data Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Data Specification</em>'.
	 * @see org.eclipse.papyrus.aas.HasDataSpecification#getDataSpecification()
	 * @see #getHasDataSpecification()
	 * @generated
	 */
	EReference getHasDataSpecification_DataSpecification();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.Reference <em>Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference</em>'.
	 * @see org.eclipse.papyrus.aas.Reference
	 * @generated
	 */
	EClass getReference();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.aas.Reference#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Key</em>'.
	 * @see org.eclipse.papyrus.aas.Reference#getKey()
	 * @see #getReference()
	 * @generated
	 */
	EReference getReference_Key();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.Reference#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.eclipse.papyrus.aas.Reference#getBase_Class()
	 * @see #getReference()
	 * @generated
	 */
	EReference getReference_Base_Class();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.Key <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Key</em>'.
	 * @see org.eclipse.papyrus.aas.Key
	 * @generated
	 */
	EClass getKey();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.Key#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.eclipse.papyrus.aas.Key#getType()
	 * @see #getKey()
	 * @generated
	 */
	EAttribute getKey_Type();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.Key#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.papyrus.aas.Key#getValue()
	 * @see #getKey()
	 * @generated
	 */
	EAttribute getKey_Value();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.Key#getIdType <em>Id Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id Type</em>'.
	 * @see org.eclipse.papyrus.aas.Key#getIdType()
	 * @see #getKey()
	 * @generated
	 */
	EAttribute getKey_IdType();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.Key#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.eclipse.papyrus.aas.Key#getBase_Class()
	 * @see #getKey()
	 * @generated
	 */
	EReference getKey_Base_Class();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.Security <em>Security</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Security</em>'.
	 * @see org.eclipse.papyrus.aas.Security
	 * @generated
	 */
	EClass getSecurity();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.Security#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.eclipse.papyrus.aas.Security#getBase_Class()
	 * @see #getSecurity()
	 * @generated
	 */
	EReference getSecurity_Base_Class();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.aas.Security#getAccessControlPolicyPoints <em>Access Control Policy Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Access Control Policy Points</em>'.
	 * @see org.eclipse.papyrus.aas.Security#getAccessControlPolicyPoints()
	 * @see #getSecurity()
	 * @generated
	 */
	EReference getSecurity_AccessControlPolicyPoints();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.aas.Security#getCertificate <em>Certificate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Certificate</em>'.
	 * @see org.eclipse.papyrus.aas.Security#getCertificate()
	 * @see #getSecurity()
	 * @generated
	 */
	EReference getSecurity_Certificate();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.aas.Security#getRequiredCertificateExtension <em>Required Certificate Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Required Certificate Extension</em>'.
	 * @see org.eclipse.papyrus.aas.Security#getRequiredCertificateExtension()
	 * @see #getSecurity()
	 * @generated
	 */
	EReference getSecurity_RequiredCertificateExtension();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.AccessControlPolicyPoints <em>Access Control Policy Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Access Control Policy Points</em>'.
	 * @see org.eclipse.papyrus.aas.AccessControlPolicyPoints
	 * @generated
	 */
	EClass getAccessControlPolicyPoints();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.aas.AccessControlPolicyPoints#getLocalAccessControl <em>Local Access Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Local Access Control</em>'.
	 * @see org.eclipse.papyrus.aas.AccessControlPolicyPoints#getLocalAccessControl()
	 * @see #getAccessControlPolicyPoints()
	 * @generated
	 */
	EReference getAccessControlPolicyPoints_LocalAccessControl();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.AccessControlPolicyPoints#isExternalAccessControl <em>External Access Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>External Access Control</em>'.
	 * @see org.eclipse.papyrus.aas.AccessControlPolicyPoints#isExternalAccessControl()
	 * @see #getAccessControlPolicyPoints()
	 * @generated
	 */
	EAttribute getAccessControlPolicyPoints_ExternalAccessControl();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.AccessControlPolicyPoints#isExternalInformationPoints <em>External Information Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>External Information Points</em>'.
	 * @see org.eclipse.papyrus.aas.AccessControlPolicyPoints#isExternalInformationPoints()
	 * @see #getAccessControlPolicyPoints()
	 * @generated
	 */
	EAttribute getAccessControlPolicyPoints_ExternalInformationPoints();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.AccessControlPolicyPoints#getInternalInformationPoint <em>Internal Information Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Internal Information Point</em>'.
	 * @see org.eclipse.papyrus.aas.AccessControlPolicyPoints#getInternalInformationPoint()
	 * @see #getAccessControlPolicyPoints()
	 * @generated
	 */
	EReference getAccessControlPolicyPoints_InternalInformationPoint();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.AccessControlPolicyPoints#isExternalPolicyDecisionPoints <em>External Policy Decision Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>External Policy Decision Points</em>'.
	 * @see org.eclipse.papyrus.aas.AccessControlPolicyPoints#isExternalPolicyDecisionPoints()
	 * @see #getAccessControlPolicyPoints()
	 * @generated
	 */
	EAttribute getAccessControlPolicyPoints_ExternalPolicyDecisionPoints();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.AccessControlPolicyPoints#isExternalPolicyEnforcementPoint <em>External Policy Enforcement Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>External Policy Enforcement Point</em>'.
	 * @see org.eclipse.papyrus.aas.AccessControlPolicyPoints#isExternalPolicyEnforcementPoint()
	 * @see #getAccessControlPolicyPoints()
	 * @generated
	 */
	EAttribute getAccessControlPolicyPoints_ExternalPolicyEnforcementPoint();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.AccessControl <em>Access Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Access Control</em>'.
	 * @see org.eclipse.papyrus.aas.AccessControl
	 * @generated
	 */
	EClass getAccessControl();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.Submodel <em>Submodel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Submodel</em>'.
	 * @see org.eclipse.papyrus.aas.Submodel
	 * @generated
	 */
	EClass getSubmodel();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.aas.Submodel#getSubmodelelement <em>Submodelelement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Submodelelement</em>'.
	 * @see org.eclipse.papyrus.aas.Submodel#getSubmodelelement()
	 * @see #getSubmodel()
	 * @generated
	 */
	EReference getSubmodel_Submodelelement();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.HasKind <em>Has Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Has Kind</em>'.
	 * @see org.eclipse.papyrus.aas.HasKind
	 * @generated
	 */
	EClass getHasKind();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.HasKind#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see org.eclipse.papyrus.aas.HasKind#getKind()
	 * @see #getHasKind()
	 * @generated
	 */
	EAttribute getHasKind_Kind();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.HasKind#getBase_HasKind_Class <em>Base Has Kind Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Has Kind Class</em>'.
	 * @see org.eclipse.papyrus.aas.HasKind#getBase_HasKind_Class()
	 * @see #getHasKind()
	 * @generated
	 */
	EReference getHasKind_Base_HasKind_Class();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.HasSemantics <em>Has Semantics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Has Semantics</em>'.
	 * @see org.eclipse.papyrus.aas.HasSemantics
	 * @generated
	 */
	EClass getHasSemantics();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.HasSemantics#getSemanticId <em>Semantic Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Semantic Id</em>'.
	 * @see org.eclipse.papyrus.aas.HasSemantics#getSemanticId()
	 * @see #getHasSemantics()
	 * @generated
	 */
	EReference getHasSemantics_SemanticId();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.HasSemantics#getBase_HasSemantics_Class <em>Base Has Semantics Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Has Semantics Class</em>'.
	 * @see org.eclipse.papyrus.aas.HasSemantics#getBase_HasSemantics_Class()
	 * @see #getHasSemantics()
	 * @generated
	 */
	EReference getHasSemantics_Base_HasSemantics_Class();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.SubmodelElement <em>Submodel Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Submodel Element</em>'.
	 * @see org.eclipse.papyrus.aas.SubmodelElement
	 * @generated
	 */
	EClass getSubmodelElement();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.SubmodelElement#isDynamic <em>Is Dynamic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Dynamic</em>'.
	 * @see org.eclipse.papyrus.aas.SubmodelElement#isDynamic()
	 * @see #getSubmodelElement()
	 * @generated
	 */
	EAttribute getSubmodelElement_IsDynamic();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.aas.SubmodelElement#getEndPoint <em>End Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>End Point</em>'.
	 * @see org.eclipse.papyrus.aas.SubmodelElement#getEndPoint()
	 * @see #getSubmodelElement()
	 * @generated
	 */
	EReference getSubmodelElement_EndPoint();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.aas.SubmodelElement#getNodeId <em>Node Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Node Id</em>'.
	 * @see org.eclipse.papyrus.aas.SubmodelElement#getNodeId()
	 * @see #getSubmodelElement()
	 * @generated
	 */
	EReference getSubmodelElement_NodeId();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.Endpoint <em>Endpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Endpoint</em>'.
	 * @see org.eclipse.papyrus.aas.Endpoint
	 * @generated
	 */
	EClass getEndpoint();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.Endpoint#getAddress <em>Address</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Address</em>'.
	 * @see org.eclipse.papyrus.aas.Endpoint#getAddress()
	 * @see #getEndpoint()
	 * @generated
	 */
	EAttribute getEndpoint_Address();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.Endpoint#getProtocol <em>Protocol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Protocol</em>'.
	 * @see org.eclipse.papyrus.aas.Endpoint#getProtocol()
	 * @see #getEndpoint()
	 * @generated
	 */
	EAttribute getEndpoint_Protocol();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.Endpoint#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.papyrus.aas.Endpoint#getName()
	 * @see #getEndpoint()
	 * @generated
	 */
	EAttribute getEndpoint_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.NodeId <em>Node Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Node Id</em>'.
	 * @see org.eclipse.papyrus.aas.NodeId
	 * @generated
	 */
	EClass getNodeId();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.NodeId#getNameSpaceIndex <em>Name Space Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name Space Index</em>'.
	 * @see org.eclipse.papyrus.aas.NodeId#getNameSpaceIndex()
	 * @see #getNodeId()
	 * @generated
	 */
	EAttribute getNodeId_NameSpaceIndex();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.NodeId#getIdentifier <em>Identifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Identifier</em>'.
	 * @see org.eclipse.papyrus.aas.NodeId#getIdentifier()
	 * @see #getNodeId()
	 * @generated
	 */
	EAttribute getNodeId_Identifier();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.NodeId#getIdType <em>Id Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id Type</em>'.
	 * @see org.eclipse.papyrus.aas.NodeId#getIdType()
	 * @see #getNodeId()
	 * @generated
	 */
	EAttribute getNodeId_IdType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.Certificate <em>Certificate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Certificate</em>'.
	 * @see org.eclipse.papyrus.aas.Certificate
	 * @generated
	 */
	EClass getCertificate();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.AssetInformation <em>Asset Information</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asset Information</em>'.
	 * @see org.eclipse.papyrus.aas.AssetInformation
	 * @generated
	 */
	EClass getAssetInformation();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.AssetInformation#getAssetKind <em>Asset Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Asset Kind</em>'.
	 * @see org.eclipse.papyrus.aas.AssetInformation#getAssetKind()
	 * @see #getAssetInformation()
	 * @generated
	 */
	EAttribute getAssetInformation_AssetKind();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.AssetInformation#getGlobalAssetId <em>Global Asset Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Global Asset Id</em>'.
	 * @see org.eclipse.papyrus.aas.AssetInformation#getGlobalAssetId()
	 * @see #getAssetInformation()
	 * @generated
	 */
	EReference getAssetInformation_GlobalAssetId();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.aas.AssetInformation#getSpecificAssetId <em>Specific Asset Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Specific Asset Id</em>'.
	 * @see org.eclipse.papyrus.aas.AssetInformation#getSpecificAssetId()
	 * @see #getAssetInformation()
	 * @generated
	 */
	EReference getAssetInformation_SpecificAssetId();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.aas.AssetInformation#getBillOfMaterial <em>Bill Of Material</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Bill Of Material</em>'.
	 * @see org.eclipse.papyrus.aas.AssetInformation#getBillOfMaterial()
	 * @see #getAssetInformation()
	 * @generated
	 */
	EReference getAssetInformation_BillOfMaterial();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.AssetInformation#getDefaultThumbnail <em>Default Thumbnail</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Default Thumbnail</em>'.
	 * @see org.eclipse.papyrus.aas.AssetInformation#getDefaultThumbnail()
	 * @see #getAssetInformation()
	 * @generated
	 */
	EReference getAssetInformation_DefaultThumbnail();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.IdentifierKeyValuePair <em>Identifier Key Value Pair</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Identifier Key Value Pair</em>'.
	 * @see org.eclipse.papyrus.aas.IdentifierKeyValuePair
	 * @generated
	 */
	EClass getIdentifierKeyValuePair();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.IdentifierKeyValuePair#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see org.eclipse.papyrus.aas.IdentifierKeyValuePair#getKey()
	 * @see #getIdentifierKeyValuePair()
	 * @generated
	 */
	EAttribute getIdentifierKeyValuePair_Key();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.IdentifierKeyValuePair#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.papyrus.aas.IdentifierKeyValuePair#getValue()
	 * @see #getIdentifierKeyValuePair()
	 * @generated
	 */
	EAttribute getIdentifierKeyValuePair_Value();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.IdentifierKeyValuePair#getExternalSubjectId <em>External Subject Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>External Subject Id</em>'.
	 * @see org.eclipse.papyrus.aas.IdentifierKeyValuePair#getExternalSubjectId()
	 * @see #getIdentifierKeyValuePair()
	 * @generated
	 */
	EReference getIdentifierKeyValuePair_ExternalSubjectId();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.File <em>File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>File</em>'.
	 * @see org.eclipse.papyrus.aas.File
	 * @generated
	 */
	EClass getFile();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.File#getPath <em>Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Path</em>'.
	 * @see org.eclipse.papyrus.aas.File#getPath()
	 * @see #getFile()
	 * @generated
	 */
	EAttribute getFile_Path();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.File#getMimeType <em>Mime Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mime Type</em>'.
	 * @see org.eclipse.papyrus.aas.File#getMimeType()
	 * @see #getFile()
	 * @generated
	 */
	EAttribute getFile_MimeType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.DataElement <em>Data Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Element</em>'.
	 * @see org.eclipse.papyrus.aas.DataElement
	 * @generated
	 */
	EClass getDataElement();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.DataElement#getBase_Property <em>Base Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Property</em>'.
	 * @see org.eclipse.papyrus.aas.DataElement#getBase_Property()
	 * @see #getDataElement()
	 * @generated
	 */
	EReference getDataElement_Base_Property();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.Asset <em>Asset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asset</em>'.
	 * @see org.eclipse.papyrus.aas.Asset
	 * @generated
	 */
	EClass getAsset();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.Asset#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see org.eclipse.papyrus.aas.Asset#getKind()
	 * @see #getAsset()
	 * @generated
	 */
	EAttribute getAsset_Kind();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.aas.Asset#getEndpoint <em>Endpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Endpoint</em>'.
	 * @see org.eclipse.papyrus.aas.Asset#getEndpoint()
	 * @see #getAsset()
	 * @generated
	 */
	EReference getAsset_Endpoint();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.AASEndpoint <em>Endpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Endpoint</em>'.
	 * @see org.eclipse.papyrus.aas.AASEndpoint
	 * @generated
	 */
	EClass getAASEndpoint();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.AASEndpoint#getAddress <em>Address</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Address</em>'.
	 * @see org.eclipse.papyrus.aas.AASEndpoint#getAddress()
	 * @see #getAASEndpoint()
	 * @generated
	 */
	EAttribute getAASEndpoint_Address();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.AASEndpoint#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Port</em>'.
	 * @see org.eclipse.papyrus.aas.AASEndpoint#getPort()
	 * @see #getAASEndpoint()
	 * @generated
	 */
	EAttribute getAASEndpoint_Port();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.AASEndpoint#getSecurity <em>Security</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Security</em>'.
	 * @see org.eclipse.papyrus.aas.AASEndpoint#getSecurity()
	 * @see #getAASEndpoint()
	 * @generated
	 */
	EAttribute getAASEndpoint_Security();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.Entity <em>Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Entity</em>'.
	 * @see org.eclipse.papyrus.aas.Entity
	 * @generated
	 */
	EClass getEntity();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.Entity#getEntityType <em>Entity Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Entity Type</em>'.
	 * @see org.eclipse.papyrus.aas.Entity#getEntityType()
	 * @see #getEntity()
	 * @generated
	 */
	EAttribute getEntity_EntityType();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.Entity#getAsset <em>Asset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Asset</em>'.
	 * @see org.eclipse.papyrus.aas.Entity#getAsset()
	 * @see #getEntity()
	 * @generated
	 */
	EReference getEntity_Asset();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.Entity#getBase_Property <em>Base Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Property</em>'.
	 * @see org.eclipse.papyrus.aas.Entity#getBase_Property()
	 * @see #getEntity()
	 * @generated
	 */
	EReference getEntity_Base_Property();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.RelationshipElement <em>Relationship Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Relationship Element</em>'.
	 * @see org.eclipse.papyrus.aas.RelationshipElement
	 * @generated
	 */
	EClass getRelationshipElement();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.RelationshipElement#getBase_Dependency <em>Base Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Dependency</em>'.
	 * @see org.eclipse.papyrus.aas.RelationshipElement#getBase_Dependency()
	 * @see #getRelationshipElement()
	 * @generated
	 */
	EReference getRelationshipElement_Base_Dependency();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.Operation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation</em>'.
	 * @see org.eclipse.papyrus.aas.Operation
	 * @generated
	 */
	EClass getOperation();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.Operation#getBase_Operation <em>Base Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Operation</em>'.
	 * @see org.eclipse.papyrus.aas.Operation#getBase_Operation()
	 * @see #getOperation()
	 * @generated
	 */
	EReference getOperation_Base_Operation();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.SubmodelElementCollection <em>Submodel Element Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Submodel Element Collection</em>'.
	 * @see org.eclipse.papyrus.aas.SubmodelElementCollection
	 * @generated
	 */
	EClass getSubmodelElementCollection();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.aas.SubmodelElementCollection#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value</em>'.
	 * @see org.eclipse.papyrus.aas.SubmodelElementCollection#getValue()
	 * @see #getSubmodelElementCollection()
	 * @generated
	 */
	EReference getSubmodelElementCollection_Value();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.SubmodelElementCollection#getBase_Property <em>Base Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Property</em>'.
	 * @see org.eclipse.papyrus.aas.SubmodelElementCollection#getBase_Property()
	 * @see #getSubmodelElementCollection()
	 * @generated
	 */
	EReference getSubmodelElementCollection_Base_Property();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.SubmodelElementCollection#getBase_DataType <em>Base Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Data Type</em>'.
	 * @see org.eclipse.papyrus.aas.SubmodelElementCollection#getBase_DataType()
	 * @see #getSubmodelElementCollection()
	 * @generated
	 */
	EReference getSubmodelElementCollection_Base_DataType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.SubmodelElementCollection#isOrdered <em>Ordered</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ordered</em>'.
	 * @see org.eclipse.papyrus.aas.SubmodelElementCollection#isOrdered()
	 * @see #getSubmodelElementCollection()
	 * @generated
	 */
	EAttribute getSubmodelElementCollection_Ordered();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.SubmodelElementCollection#isAllowDuplicates <em>Allow Duplicates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Allow Duplicates</em>'.
	 * @see org.eclipse.papyrus.aas.SubmodelElementCollection#isAllowDuplicates()
	 * @see #getSubmodelElementCollection()
	 * @generated
	 */
	EAttribute getSubmodelElementCollection_AllowDuplicates();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.Property <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property</em>'.
	 * @see org.eclipse.papyrus.aas.Property
	 * @generated
	 */
	EClass getProperty();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.ReferenceElement <em>Reference Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference Element</em>'.
	 * @see org.eclipse.papyrus.aas.ReferenceElement
	 * @generated
	 */
	EClass getReferenceElement();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.ReferenceElement#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see org.eclipse.papyrus.aas.ReferenceElement#getValue()
	 * @see #getReferenceElement()
	 * @generated
	 */
	EReference getReferenceElement_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event</em>'.
	 * @see org.eclipse.papyrus.aas.Event
	 * @generated
	 */
	EClass getEvent();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.Event#getBase_Property <em>Base Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Property</em>'.
	 * @see org.eclipse.papyrus.aas.Event#getBase_Property()
	 * @see #getEvent()
	 * @generated
	 */
	EReference getEvent_Base_Property();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.BasicEvent <em>Basic Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Basic Event</em>'.
	 * @see org.eclipse.papyrus.aas.BasicEvent
	 * @generated
	 */
	EClass getBasicEvent();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.BasicEvent#getObserved <em>Observed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Observed</em>'.
	 * @see org.eclipse.papyrus.aas.BasicEvent#getObserved()
	 * @see #getBasicEvent()
	 * @generated
	 */
	EReference getBasicEvent_Observed();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.Range <em>Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Range</em>'.
	 * @see org.eclipse.papyrus.aas.Range
	 * @generated
	 */
	EClass getRange();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.Range#getMin <em>Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min</em>'.
	 * @see org.eclipse.papyrus.aas.Range#getMin()
	 * @see #getRange()
	 * @generated
	 */
	EAttribute getRange_Min();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.Range#getMax <em>Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max</em>'.
	 * @see org.eclipse.papyrus.aas.Range#getMax()
	 * @see #getRange()
	 * @generated
	 */
	EAttribute getRange_Max();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.Capability <em>Capability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Capability</em>'.
	 * @see org.eclipse.papyrus.aas.Capability
	 * @generated
	 */
	EClass getCapability();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.Capability#getBase_Property <em>Base Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Property</em>'.
	 * @see org.eclipse.papyrus.aas.Capability#getBase_Property()
	 * @see #getCapability()
	 * @generated
	 */
	EReference getCapability_Base_Property();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.ConceptDescription <em>Concept Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Concept Description</em>'.
	 * @see org.eclipse.papyrus.aas.ConceptDescription
	 * @generated
	 */
	EClass getConceptDescription();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.aas.ConceptDescription#getIsCaseOf <em>Is Case Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Is Case Of</em>'.
	 * @see org.eclipse.papyrus.aas.ConceptDescription#getIsCaseOf()
	 * @see #getConceptDescription()
	 * @generated
	 */
	EReference getConceptDescription_IsCaseOf();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.LangStringSet <em>Lang String Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lang String Set</em>'.
	 * @see org.eclipse.papyrus.aas.LangStringSet
	 * @generated
	 */
	EClass getLangStringSet();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.DataSpecificationContent <em>Data Specification Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Specification Content</em>'.
	 * @see org.eclipse.papyrus.aas.DataSpecificationContent
	 * @generated
	 */
	EClass getDataSpecificationContent();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360 <em>Data Specification IEC61360</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Specification IEC61360</em>'.
	 * @see org.eclipse.papyrus.aas.DataSpecificationIEC61360
	 * @generated
	 */
	EClass getDataSpecificationIEC61360();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.eclipse.papyrus.aas.DataSpecificationIEC61360#getBase_Class()
	 * @see #getDataSpecificationIEC61360()
	 * @generated
	 */
	EReference getDataSpecificationIEC61360_Base_Class();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getPreferredName <em>Preferred Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Preferred Name</em>'.
	 * @see org.eclipse.papyrus.aas.DataSpecificationIEC61360#getPreferredName()
	 * @see #getDataSpecificationIEC61360()
	 * @generated
	 */
	EReference getDataSpecificationIEC61360_PreferredName();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getShortName <em>Short Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Short Name</em>'.
	 * @see org.eclipse.papyrus.aas.DataSpecificationIEC61360#getShortName()
	 * @see #getDataSpecificationIEC61360()
	 * @generated
	 */
	EReference getDataSpecificationIEC61360_ShortName();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getUnit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unit</em>'.
	 * @see org.eclipse.papyrus.aas.DataSpecificationIEC61360#getUnit()
	 * @see #getDataSpecificationIEC61360()
	 * @generated
	 */
	EAttribute getDataSpecificationIEC61360_Unit();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getUnitId <em>Unit Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Unit Id</em>'.
	 * @see org.eclipse.papyrus.aas.DataSpecificationIEC61360#getUnitId()
	 * @see #getDataSpecificationIEC61360()
	 * @generated
	 */
	EReference getDataSpecificationIEC61360_UnitId();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getSourceOfDefinition <em>Source Of Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Of Definition</em>'.
	 * @see org.eclipse.papyrus.aas.DataSpecificationIEC61360#getSourceOfDefinition()
	 * @see #getDataSpecificationIEC61360()
	 * @generated
	 */
	EAttribute getDataSpecificationIEC61360_SourceOfDefinition();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getSymbol <em>Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Symbol</em>'.
	 * @see org.eclipse.papyrus.aas.DataSpecificationIEC61360#getSymbol()
	 * @see #getDataSpecificationIEC61360()
	 * @generated
	 */
	EAttribute getDataSpecificationIEC61360_Symbol();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getDataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Data Type</em>'.
	 * @see org.eclipse.papyrus.aas.DataSpecificationIEC61360#getDataType()
	 * @see #getDataSpecificationIEC61360()
	 * @generated
	 */
	EReference getDataSpecificationIEC61360_DataType();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getDefinition <em>Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Definition</em>'.
	 * @see org.eclipse.papyrus.aas.DataSpecificationIEC61360#getDefinition()
	 * @see #getDataSpecificationIEC61360()
	 * @generated
	 */
	EReference getDataSpecificationIEC61360_Definition();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getValueFormat <em>Value Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Format</em>'.
	 * @see org.eclipse.papyrus.aas.DataSpecificationIEC61360#getValueFormat()
	 * @see #getDataSpecificationIEC61360()
	 * @generated
	 */
	EAttribute getDataSpecificationIEC61360_ValueFormat();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getValueList <em>Value List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Value List</em>'.
	 * @see org.eclipse.papyrus.aas.DataSpecificationIEC61360#getValueList()
	 * @see #getDataSpecificationIEC61360()
	 * @generated
	 */
	EReference getDataSpecificationIEC61360_ValueList();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see org.eclipse.papyrus.aas.DataSpecificationIEC61360#getValue()
	 * @see #getDataSpecificationIEC61360()
	 * @generated
	 */
	EReference getDataSpecificationIEC61360_Value();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getValueId <em>Value Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value Id</em>'.
	 * @see org.eclipse.papyrus.aas.DataSpecificationIEC61360#getValueId()
	 * @see #getDataSpecificationIEC61360()
	 * @generated
	 */
	EReference getDataSpecificationIEC61360_ValueId();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.papyrus.aas.DataSpecificationIEC61360#getLevelType <em>Level Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Level Type</em>'.
	 * @see org.eclipse.papyrus.aas.DataSpecificationIEC61360#getLevelType()
	 * @see #getDataSpecificationIEC61360()
	 * @generated
	 */
	EAttribute getDataSpecificationIEC61360_LevelType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.ValueReferencePairType <em>Value Reference Pair Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value Reference Pair Type</em>'.
	 * @see org.eclipse.papyrus.aas.ValueReferencePairType
	 * @generated
	 */
	EClass getValueReferencePairType();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.ValueReferencePairType#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see org.eclipse.papyrus.aas.ValueReferencePairType#getValue()
	 * @see #getValueReferencePairType()
	 * @generated
	 */
	EReference getValueReferencePairType_Value();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.ValueReferencePairType#getValueId <em>Value Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value Id</em>'.
	 * @see org.eclipse.papyrus.aas.ValueReferencePairType#getValueId()
	 * @see #getValueReferencePairType()
	 * @generated
	 */
	EReference getValueReferencePairType_ValueId();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.View <em>View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>View</em>'.
	 * @see org.eclipse.papyrus.aas.View
	 * @generated
	 */
	EClass getView();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.aas.View#getContainedElement <em>Contained Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Contained Element</em>'.
	 * @see org.eclipse.papyrus.aas.View#getContainedElement()
	 * @see #getView()
	 * @generated
	 */
	EReference getView_ContainedElement();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.aas.MultiLanguageProperty <em>Multi Language Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Multi Language Property</em>'.
	 * @see org.eclipse.papyrus.aas.MultiLanguageProperty
	 * @generated
	 */
	EClass getMultiLanguageProperty();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.aas.MultiLanguageProperty#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Value</em>'.
	 * @see org.eclipse.papyrus.aas.MultiLanguageProperty#getValue()
	 * @see #getMultiLanguageProperty()
	 * @generated
	 */
	EReference getMultiLanguageProperty_Value();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.aas.MultiLanguageProperty#getValueId <em>Value Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value Id</em>'.
	 * @see org.eclipse.papyrus.aas.MultiLanguageProperty#getValueId()
	 * @see #getMultiLanguageProperty()
	 * @generated
	 */
	EReference getMultiLanguageProperty_ValueId();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.aas.IdentifierType <em>Identifier Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Identifier Type</em>'.
	 * @see org.eclipse.papyrus.aas.IdentifierType
	 * @generated
	 */
	EEnum getIdentifierType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.aas.LangEnum <em>Lang Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Lang Enum</em>'.
	 * @see org.eclipse.papyrus.aas.LangEnum
	 * @generated
	 */
	EEnum getLangEnum();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.aas.KeyElements <em>Key Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Key Elements</em>'.
	 * @see org.eclipse.papyrus.aas.KeyElements
	 * @generated
	 */
	EEnum getKeyElements();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.aas.ReferableElements <em>Referable Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Referable Elements</em>'.
	 * @see org.eclipse.papyrus.aas.ReferableElements
	 * @generated
	 */
	EEnum getReferableElements();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.aas.IdentifiableElement <em>Identifiable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Identifiable Element</em>'.
	 * @see org.eclipse.papyrus.aas.IdentifiableElement
	 * @generated
	 */
	EEnum getIdentifiableElement();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.aas.KeyType <em>Key Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Key Type</em>'.
	 * @see org.eclipse.papyrus.aas.KeyType
	 * @generated
	 */
	EEnum getKeyType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.aas.LocalKeyType <em>Local Key Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Local Key Type</em>'.
	 * @see org.eclipse.papyrus.aas.LocalKeyType
	 * @generated
	 */
	EEnum getLocalKeyType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.aas.ModelingKind <em>Modeling Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Modeling Kind</em>'.
	 * @see org.eclipse.papyrus.aas.ModelingKind
	 * @generated
	 */
	EEnum getModelingKind();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.aas.ProtocolKind <em>Protocol Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Protocol Kind</em>'.
	 * @see org.eclipse.papyrus.aas.ProtocolKind
	 * @generated
	 */
	EEnum getProtocolKind();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.aas.IdType <em>Id Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Id Type</em>'.
	 * @see org.eclipse.papyrus.aas.IdType
	 * @generated
	 */
	EEnum getIdType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.aas.AssetKind <em>Asset Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Asset Kind</em>'.
	 * @see org.eclipse.papyrus.aas.AssetKind
	 * @generated
	 */
	EEnum getAssetKind();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.aas.MimeType <em>Mime Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Mime Type</em>'.
	 * @see org.eclipse.papyrus.aas.MimeType
	 * @generated
	 */
	EEnum getMimeType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.aas.SecurityKind <em>Security Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Security Kind</em>'.
	 * @see org.eclipse.papyrus.aas.SecurityKind
	 * @generated
	 */
	EEnum getSecurityKind();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.aas.EntityType <em>Entity Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Entity Type</em>'.
	 * @see org.eclipse.papyrus.aas.EntityType
	 * @generated
	 */
	EEnum getEntityType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.aas.LevelType <em>Level Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Level Type</em>'.
	 * @see org.eclipse.papyrus.aas.LevelType
	 * @generated
	 */
	EEnum getLevelType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.aas.DataTypeIEC61360 <em>Data Type IEC61360</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Data Type IEC61360</em>'.
	 * @see org.eclipse.papyrus.aas.DataTypeIEC61360
	 * @generated
	 */
	EEnum getDataTypeIEC61360();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AASFactory getAASFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.AssetAdministrationShellImpl <em>Asset Administration Shell</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.AssetAdministrationShellImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getAssetAdministrationShell()
		 * @generated
		 */
		EClass ASSET_ADMINISTRATION_SHELL = eINSTANCE.getAssetAdministrationShell();

		/**
		 * The meta object literal for the '<em><b>Derived From</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_ADMINISTRATION_SHELL__DERIVED_FROM = eINSTANCE.getAssetAdministrationShell_DerivedFrom();

		/**
		 * The meta object literal for the '<em><b>Security</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_ADMINISTRATION_SHELL__SECURITY = eINSTANCE.getAssetAdministrationShell_Security();

		/**
		 * The meta object literal for the '<em><b>Asset Information</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_ADMINISTRATION_SHELL__ASSET_INFORMATION = eINSTANCE.getAssetAdministrationShell_AssetInformation();

		/**
		 * The meta object literal for the '<em><b>Asset</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_ADMINISTRATION_SHELL__ASSET = eINSTANCE.getAssetAdministrationShell_Asset();

		/**
		 * The meta object literal for the '<em><b>Submodel</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_ADMINISTRATION_SHELL__SUBMODEL = eINSTANCE.getAssetAdministrationShell_Submodel();

		/**
		 * The meta object literal for the '<em><b>Endpoint</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_ADMINISTRATION_SHELL__ENDPOINT = eINSTANCE.getAssetAdministrationShell_Endpoint();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.IdentifiableImpl <em>Identifiable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.IdentifiableImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getIdentifiable()
		 * @generated
		 */
		EClass IDENTIFIABLE = eINSTANCE.getIdentifiable();

		/**
		 * The meta object literal for the '<em><b>Administration</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IDENTIFIABLE__ADMINISTRATION = eINSTANCE.getIdentifiable_Administration();

		/**
		 * The meta object literal for the '<em><b>Identification</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IDENTIFIABLE__IDENTIFICATION = eINSTANCE.getIdentifiable_Identification();

		/**
		 * The meta object literal for the '<em><b>Base Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IDENTIFIABLE__BASE_PACKAGE = eINSTANCE.getIdentifiable_Base_Package();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.ReferableImpl <em>Referable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.ReferableImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getReferable()
		 * @generated
		 */
		EClass REFERABLE = eINSTANCE.getReferable();

		/**
		 * The meta object literal for the '<em><b>Id Short</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REFERABLE__ID_SHORT = eINSTANCE.getReferable_IdShort();

		/**
		 * The meta object literal for the '<em><b>Category</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REFERABLE__CATEGORY = eINSTANCE.getReferable_Category();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERABLE__DESCRIPTION = eINSTANCE.getReferable_Description();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERABLE__BASE_CLASS = eINSTANCE.getReferable_Base_Class();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.LangStringImpl <em>Lang String</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.LangStringImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getLangString()
		 * @generated
		 */
		EClass LANG_STRING = eINSTANCE.getLangString();

		/**
		 * The meta object literal for the '<em><b>Lang</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LANG_STRING__LANG = eINSTANCE.getLangString_Lang();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LANG_STRING__VALUE = eINSTANCE.getLangString_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.AdministrativeInformationImpl <em>Administrative Information</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.AdministrativeInformationImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getAdministrativeInformation()
		 * @generated
		 */
		EClass ADMINISTRATIVE_INFORMATION = eINSTANCE.getAdministrativeInformation();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ADMINISTRATIVE_INFORMATION__VERSION = eINSTANCE.getAdministrativeInformation_Version();

		/**
		 * The meta object literal for the '<em><b>Revision</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ADMINISTRATIVE_INFORMATION__REVISION = eINSTANCE.getAdministrativeInformation_Revision();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.IdentifierImpl <em>Identifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.IdentifierImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getIdentifier()
		 * @generated
		 */
		EClass IDENTIFIER = eINSTANCE.getIdentifier();

		/**
		 * The meta object literal for the '<em><b>Id Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IDENTIFIER__ID_TYPE = eINSTANCE.getIdentifier_IdType();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IDENTIFIER__ID = eINSTANCE.getIdentifier_Id();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.HasDataSpecificationImpl <em>Has Data Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.HasDataSpecificationImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getHasDataSpecification()
		 * @generated
		 */
		EClass HAS_DATA_SPECIFICATION = eINSTANCE.getHasDataSpecification();

		/**
		 * The meta object literal for the '<em><b>Data Specification</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HAS_DATA_SPECIFICATION__DATA_SPECIFICATION = eINSTANCE.getHasDataSpecification_DataSpecification();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.ReferenceImpl <em>Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.ReferenceImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getReference()
		 * @generated
		 */
		EClass REFERENCE = eINSTANCE.getReference();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE__KEY = eINSTANCE.getReference_Key();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE__BASE_CLASS = eINSTANCE.getReference_Base_Class();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.KeyImpl <em>Key</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.KeyImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getKey()
		 * @generated
		 */
		EClass KEY = eINSTANCE.getKey();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute KEY__TYPE = eINSTANCE.getKey_Type();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute KEY__VALUE = eINSTANCE.getKey_Value();

		/**
		 * The meta object literal for the '<em><b>Id Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute KEY__ID_TYPE = eINSTANCE.getKey_IdType();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference KEY__BASE_CLASS = eINSTANCE.getKey_Base_Class();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.SecurityImpl <em>Security</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.SecurityImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getSecurity()
		 * @generated
		 */
		EClass SECURITY = eINSTANCE.getSecurity();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY__BASE_CLASS = eINSTANCE.getSecurity_Base_Class();

		/**
		 * The meta object literal for the '<em><b>Access Control Policy Points</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY__ACCESS_CONTROL_POLICY_POINTS = eINSTANCE.getSecurity_AccessControlPolicyPoints();

		/**
		 * The meta object literal for the '<em><b>Certificate</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY__CERTIFICATE = eINSTANCE.getSecurity_Certificate();

		/**
		 * The meta object literal for the '<em><b>Required Certificate Extension</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY__REQUIRED_CERTIFICATE_EXTENSION = eINSTANCE.getSecurity_RequiredCertificateExtension();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.AccessControlPolicyPointsImpl <em>Access Control Policy Points</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.AccessControlPolicyPointsImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getAccessControlPolicyPoints()
		 * @generated
		 */
		EClass ACCESS_CONTROL_POLICY_POINTS = eINSTANCE.getAccessControlPolicyPoints();

		/**
		 * The meta object literal for the '<em><b>Local Access Control</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESS_CONTROL_POLICY_POINTS__LOCAL_ACCESS_CONTROL = eINSTANCE.getAccessControlPolicyPoints_LocalAccessControl();

		/**
		 * The meta object literal for the '<em><b>External Access Control</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACCESS_CONTROL_POLICY_POINTS__EXTERNAL_ACCESS_CONTROL = eINSTANCE.getAccessControlPolicyPoints_ExternalAccessControl();

		/**
		 * The meta object literal for the '<em><b>External Information Points</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACCESS_CONTROL_POLICY_POINTS__EXTERNAL_INFORMATION_POINTS = eINSTANCE.getAccessControlPolicyPoints_ExternalInformationPoints();

		/**
		 * The meta object literal for the '<em><b>Internal Information Point</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESS_CONTROL_POLICY_POINTS__INTERNAL_INFORMATION_POINT = eINSTANCE.getAccessControlPolicyPoints_InternalInformationPoint();

		/**
		 * The meta object literal for the '<em><b>External Policy Decision Points</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACCESS_CONTROL_POLICY_POINTS__EXTERNAL_POLICY_DECISION_POINTS = eINSTANCE.getAccessControlPolicyPoints_ExternalPolicyDecisionPoints();

		/**
		 * The meta object literal for the '<em><b>External Policy Enforcement Point</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACCESS_CONTROL_POLICY_POINTS__EXTERNAL_POLICY_ENFORCEMENT_POINT = eINSTANCE.getAccessControlPolicyPoints_ExternalPolicyEnforcementPoint();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.AccessControlImpl <em>Access Control</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.AccessControlImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getAccessControl()
		 * @generated
		 */
		EClass ACCESS_CONTROL = eINSTANCE.getAccessControl();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.SubmodelImpl <em>Submodel</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.SubmodelImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getSubmodel()
		 * @generated
		 */
		EClass SUBMODEL = eINSTANCE.getSubmodel();

		/**
		 * The meta object literal for the '<em><b>Submodelelement</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL__SUBMODELELEMENT = eINSTANCE.getSubmodel_Submodelelement();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.HasKindImpl <em>Has Kind</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.HasKindImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getHasKind()
		 * @generated
		 */
		EClass HAS_KIND = eINSTANCE.getHasKind();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAS_KIND__KIND = eINSTANCE.getHasKind_Kind();

		/**
		 * The meta object literal for the '<em><b>Base Has Kind Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HAS_KIND__BASE_HAS_KIND_CLASS = eINSTANCE.getHasKind_Base_HasKind_Class();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.HasSemanticsImpl <em>Has Semantics</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.HasSemanticsImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getHasSemantics()
		 * @generated
		 */
		EClass HAS_SEMANTICS = eINSTANCE.getHasSemantics();

		/**
		 * The meta object literal for the '<em><b>Semantic Id</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HAS_SEMANTICS__SEMANTIC_ID = eINSTANCE.getHasSemantics_SemanticId();

		/**
		 * The meta object literal for the '<em><b>Base Has Semantics Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HAS_SEMANTICS__BASE_HAS_SEMANTICS_CLASS = eINSTANCE.getHasSemantics_Base_HasSemantics_Class();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.SubmodelElementImpl <em>Submodel Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.SubmodelElementImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getSubmodelElement()
		 * @generated
		 */
		EClass SUBMODEL_ELEMENT = eINSTANCE.getSubmodelElement();

		/**
		 * The meta object literal for the '<em><b>Is Dynamic</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUBMODEL_ELEMENT__IS_DYNAMIC = eINSTANCE.getSubmodelElement_IsDynamic();

		/**
		 * The meta object literal for the '<em><b>End Point</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENT__END_POINT = eINSTANCE.getSubmodelElement_EndPoint();

		/**
		 * The meta object literal for the '<em><b>Node Id</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENT__NODE_ID = eINSTANCE.getSubmodelElement_NodeId();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.EndpointImpl <em>Endpoint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.EndpointImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getEndpoint()
		 * @generated
		 */
		EClass ENDPOINT = eINSTANCE.getEndpoint();

		/**
		 * The meta object literal for the '<em><b>Address</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENDPOINT__ADDRESS = eINSTANCE.getEndpoint_Address();

		/**
		 * The meta object literal for the '<em><b>Protocol</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENDPOINT__PROTOCOL = eINSTANCE.getEndpoint_Protocol();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENDPOINT__NAME = eINSTANCE.getEndpoint_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.NodeIdImpl <em>Node Id</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.NodeIdImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getNodeId()
		 * @generated
		 */
		EClass NODE_ID = eINSTANCE.getNodeId();

		/**
		 * The meta object literal for the '<em><b>Name Space Index</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NODE_ID__NAME_SPACE_INDEX = eINSTANCE.getNodeId_NameSpaceIndex();

		/**
		 * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NODE_ID__IDENTIFIER = eINSTANCE.getNodeId_Identifier();

		/**
		 * The meta object literal for the '<em><b>Id Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NODE_ID__ID_TYPE = eINSTANCE.getNodeId_IdType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.CertificateImpl <em>Certificate</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.CertificateImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getCertificate()
		 * @generated
		 */
		EClass CERTIFICATE = eINSTANCE.getCertificate();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.AssetInformationImpl <em>Asset Information</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.AssetInformationImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getAssetInformation()
		 * @generated
		 */
		EClass ASSET_INFORMATION = eINSTANCE.getAssetInformation();

		/**
		 * The meta object literal for the '<em><b>Asset Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET_INFORMATION__ASSET_KIND = eINSTANCE.getAssetInformation_AssetKind();

		/**
		 * The meta object literal for the '<em><b>Global Asset Id</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_INFORMATION__GLOBAL_ASSET_ID = eINSTANCE.getAssetInformation_GlobalAssetId();

		/**
		 * The meta object literal for the '<em><b>Specific Asset Id</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_INFORMATION__SPECIFIC_ASSET_ID = eINSTANCE.getAssetInformation_SpecificAssetId();

		/**
		 * The meta object literal for the '<em><b>Bill Of Material</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_INFORMATION__BILL_OF_MATERIAL = eINSTANCE.getAssetInformation_BillOfMaterial();

		/**
		 * The meta object literal for the '<em><b>Default Thumbnail</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_INFORMATION__DEFAULT_THUMBNAIL = eINSTANCE.getAssetInformation_DefaultThumbnail();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.IdentifierKeyValuePairImpl <em>Identifier Key Value Pair</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.IdentifierKeyValuePairImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getIdentifierKeyValuePair()
		 * @generated
		 */
		EClass IDENTIFIER_KEY_VALUE_PAIR = eINSTANCE.getIdentifierKeyValuePair();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IDENTIFIER_KEY_VALUE_PAIR__KEY = eINSTANCE.getIdentifierKeyValuePair_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IDENTIFIER_KEY_VALUE_PAIR__VALUE = eINSTANCE.getIdentifierKeyValuePair_Value();

		/**
		 * The meta object literal for the '<em><b>External Subject Id</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IDENTIFIER_KEY_VALUE_PAIR__EXTERNAL_SUBJECT_ID = eINSTANCE.getIdentifierKeyValuePair_ExternalSubjectId();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.FileImpl <em>File</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.FileImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getFile()
		 * @generated
		 */
		EClass FILE = eINSTANCE.getFile();

		/**
		 * The meta object literal for the '<em><b>Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FILE__PATH = eINSTANCE.getFile_Path();

		/**
		 * The meta object literal for the '<em><b>Mime Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FILE__MIME_TYPE = eINSTANCE.getFile_MimeType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.DataElementImpl <em>Data Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.DataElementImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getDataElement()
		 * @generated
		 */
		EClass DATA_ELEMENT = eINSTANCE.getDataElement();

		/**
		 * The meta object literal for the '<em><b>Base Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_ELEMENT__BASE_PROPERTY = eINSTANCE.getDataElement_Base_Property();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.AssetImpl <em>Asset</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.AssetImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getAsset()
		 * @generated
		 */
		EClass ASSET = eINSTANCE.getAsset();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET__KIND = eINSTANCE.getAsset_Kind();

		/**
		 * The meta object literal for the '<em><b>Endpoint</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET__ENDPOINT = eINSTANCE.getAsset_Endpoint();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.AASEndpointImpl <em>Endpoint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.AASEndpointImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getAASEndpoint()
		 * @generated
		 */
		EClass AAS_ENDPOINT = eINSTANCE.getAASEndpoint();

		/**
		 * The meta object literal for the '<em><b>Address</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AAS_ENDPOINT__ADDRESS = eINSTANCE.getAASEndpoint_Address();

		/**
		 * The meta object literal for the '<em><b>Port</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AAS_ENDPOINT__PORT = eINSTANCE.getAASEndpoint_Port();

		/**
		 * The meta object literal for the '<em><b>Security</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AAS_ENDPOINT__SECURITY = eINSTANCE.getAASEndpoint_Security();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.EntityImpl <em>Entity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.EntityImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getEntity()
		 * @generated
		 */
		EClass ENTITY = eINSTANCE.getEntity();

		/**
		 * The meta object literal for the '<em><b>Entity Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY__ENTITY_TYPE = eINSTANCE.getEntity_EntityType();

		/**
		 * The meta object literal for the '<em><b>Asset</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY__ASSET = eINSTANCE.getEntity_Asset();

		/**
		 * The meta object literal for the '<em><b>Base Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY__BASE_PROPERTY = eINSTANCE.getEntity_Base_Property();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.RelationshipElementImpl <em>Relationship Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.RelationshipElementImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getRelationshipElement()
		 * @generated
		 */
		EClass RELATIONSHIP_ELEMENT = eINSTANCE.getRelationshipElement();

		/**
		 * The meta object literal for the '<em><b>Base Dependency</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATIONSHIP_ELEMENT__BASE_DEPENDENCY = eINSTANCE.getRelationshipElement_Base_Dependency();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.OperationImpl <em>Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.OperationImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getOperation()
		 * @generated
		 */
		EClass OPERATION = eINSTANCE.getOperation();

		/**
		 * The meta object literal for the '<em><b>Base Operation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION__BASE_OPERATION = eINSTANCE.getOperation_Base_Operation();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.SubmodelElementCollectionImpl <em>Submodel Element Collection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.SubmodelElementCollectionImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getSubmodelElementCollection()
		 * @generated
		 */
		EClass SUBMODEL_ELEMENT_COLLECTION = eINSTANCE.getSubmodelElementCollection();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENT_COLLECTION__VALUE = eINSTANCE.getSubmodelElementCollection_Value();

		/**
		 * The meta object literal for the '<em><b>Base Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENT_COLLECTION__BASE_PROPERTY = eINSTANCE.getSubmodelElementCollection_Base_Property();

		/**
		 * The meta object literal for the '<em><b>Base Data Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENT_COLLECTION__BASE_DATA_TYPE = eINSTANCE.getSubmodelElementCollection_Base_DataType();

		/**
		 * The meta object literal for the '<em><b>Ordered</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUBMODEL_ELEMENT_COLLECTION__ORDERED = eINSTANCE.getSubmodelElementCollection_Ordered();

		/**
		 * The meta object literal for the '<em><b>Allow Duplicates</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUBMODEL_ELEMENT_COLLECTION__ALLOW_DUPLICATES = eINSTANCE.getSubmodelElementCollection_AllowDuplicates();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.PropertyImpl <em>Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.PropertyImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getProperty()
		 * @generated
		 */
		EClass PROPERTY = eINSTANCE.getProperty();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.ReferenceElementImpl <em>Reference Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.ReferenceElementImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getReferenceElement()
		 * @generated
		 */
		EClass REFERENCE_ELEMENT = eINSTANCE.getReferenceElement();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE_ELEMENT__VALUE = eINSTANCE.getReferenceElement_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.EventImpl <em>Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.EventImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getEvent()
		 * @generated
		 */
		EClass EVENT = eINSTANCE.getEvent();

		/**
		 * The meta object literal for the '<em><b>Base Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT__BASE_PROPERTY = eINSTANCE.getEvent_Base_Property();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.BasicEventImpl <em>Basic Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.BasicEventImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getBasicEvent()
		 * @generated
		 */
		EClass BASIC_EVENT = eINSTANCE.getBasicEvent();

		/**
		 * The meta object literal for the '<em><b>Observed</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASIC_EVENT__OBSERVED = eINSTANCE.getBasicEvent_Observed();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.RangeImpl <em>Range</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.RangeImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getRange()
		 * @generated
		 */
		EClass RANGE = eINSTANCE.getRange();

		/**
		 * The meta object literal for the '<em><b>Min</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RANGE__MIN = eINSTANCE.getRange_Min();

		/**
		 * The meta object literal for the '<em><b>Max</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RANGE__MAX = eINSTANCE.getRange_Max();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.CapabilityImpl <em>Capability</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.CapabilityImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getCapability()
		 * @generated
		 */
		EClass CAPABILITY = eINSTANCE.getCapability();

		/**
		 * The meta object literal for the '<em><b>Base Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAPABILITY__BASE_PROPERTY = eINSTANCE.getCapability_Base_Property();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.ConceptDescriptionImpl <em>Concept Description</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.ConceptDescriptionImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getConceptDescription()
		 * @generated
		 */
		EClass CONCEPT_DESCRIPTION = eINSTANCE.getConceptDescription();

		/**
		 * The meta object literal for the '<em><b>Is Case Of</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONCEPT_DESCRIPTION__IS_CASE_OF = eINSTANCE.getConceptDescription_IsCaseOf();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.LangStringSetImpl <em>Lang String Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.LangStringSetImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getLangStringSet()
		 * @generated
		 */
		EClass LANG_STRING_SET = eINSTANCE.getLangStringSet();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.DataSpecificationContentImpl <em>Data Specification Content</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.DataSpecificationContentImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getDataSpecificationContent()
		 * @generated
		 */
		EClass DATA_SPECIFICATION_CONTENT = eINSTANCE.getDataSpecificationContent();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.DataSpecificationIEC61360Impl <em>Data Specification IEC61360</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.DataSpecificationIEC61360Impl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getDataSpecificationIEC61360()
		 * @generated
		 */
		EClass DATA_SPECIFICATION_IEC61360 = eINSTANCE.getDataSpecificationIEC61360();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SPECIFICATION_IEC61360__BASE_CLASS = eINSTANCE.getDataSpecificationIEC61360_Base_Class();

		/**
		 * The meta object literal for the '<em><b>Preferred Name</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SPECIFICATION_IEC61360__PREFERRED_NAME = eINSTANCE.getDataSpecificationIEC61360_PreferredName();

		/**
		 * The meta object literal for the '<em><b>Short Name</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SPECIFICATION_IEC61360__SHORT_NAME = eINSTANCE.getDataSpecificationIEC61360_ShortName();

		/**
		 * The meta object literal for the '<em><b>Unit</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_SPECIFICATION_IEC61360__UNIT = eINSTANCE.getDataSpecificationIEC61360_Unit();

		/**
		 * The meta object literal for the '<em><b>Unit Id</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SPECIFICATION_IEC61360__UNIT_ID = eINSTANCE.getDataSpecificationIEC61360_UnitId();

		/**
		 * The meta object literal for the '<em><b>Source Of Definition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_SPECIFICATION_IEC61360__SOURCE_OF_DEFINITION = eINSTANCE.getDataSpecificationIEC61360_SourceOfDefinition();

		/**
		 * The meta object literal for the '<em><b>Symbol</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_SPECIFICATION_IEC61360__SYMBOL = eINSTANCE.getDataSpecificationIEC61360_Symbol();

		/**
		 * The meta object literal for the '<em><b>Data Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SPECIFICATION_IEC61360__DATA_TYPE = eINSTANCE.getDataSpecificationIEC61360_DataType();

		/**
		 * The meta object literal for the '<em><b>Definition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SPECIFICATION_IEC61360__DEFINITION = eINSTANCE.getDataSpecificationIEC61360_Definition();

		/**
		 * The meta object literal for the '<em><b>Value Format</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_SPECIFICATION_IEC61360__VALUE_FORMAT = eINSTANCE.getDataSpecificationIEC61360_ValueFormat();

		/**
		 * The meta object literal for the '<em><b>Value List</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SPECIFICATION_IEC61360__VALUE_LIST = eINSTANCE.getDataSpecificationIEC61360_ValueList();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SPECIFICATION_IEC61360__VALUE = eINSTANCE.getDataSpecificationIEC61360_Value();

		/**
		 * The meta object literal for the '<em><b>Value Id</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SPECIFICATION_IEC61360__VALUE_ID = eINSTANCE.getDataSpecificationIEC61360_ValueId();

		/**
		 * The meta object literal for the '<em><b>Level Type</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_SPECIFICATION_IEC61360__LEVEL_TYPE = eINSTANCE.getDataSpecificationIEC61360_LevelType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.ValueReferencePairTypeImpl <em>Value Reference Pair Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.ValueReferencePairTypeImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getValueReferencePairType()
		 * @generated
		 */
		EClass VALUE_REFERENCE_PAIR_TYPE = eINSTANCE.getValueReferencePairType();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VALUE_REFERENCE_PAIR_TYPE__VALUE = eINSTANCE.getValueReferencePairType_Value();

		/**
		 * The meta object literal for the '<em><b>Value Id</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VALUE_REFERENCE_PAIR_TYPE__VALUE_ID = eINSTANCE.getValueReferencePairType_ValueId();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.ViewImpl <em>View</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.ViewImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getView()
		 * @generated
		 */
		EClass VIEW = eINSTANCE.getView();

		/**
		 * The meta object literal for the '<em><b>Contained Element</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VIEW__CONTAINED_ELEMENT = eINSTANCE.getView_ContainedElement();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.impl.MultiLanguagePropertyImpl <em>Multi Language Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.impl.MultiLanguagePropertyImpl
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getMultiLanguageProperty()
		 * @generated
		 */
		EClass MULTI_LANGUAGE_PROPERTY = eINSTANCE.getMultiLanguageProperty();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MULTI_LANGUAGE_PROPERTY__VALUE = eINSTANCE.getMultiLanguageProperty_Value();

		/**
		 * The meta object literal for the '<em><b>Value Id</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MULTI_LANGUAGE_PROPERTY__VALUE_ID = eINSTANCE.getMultiLanguageProperty_ValueId();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.IdentifierType <em>Identifier Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.IdentifierType
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getIdentifierType()
		 * @generated
		 */
		EEnum IDENTIFIER_TYPE = eINSTANCE.getIdentifierType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.LangEnum <em>Lang Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.LangEnum
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getLangEnum()
		 * @generated
		 */
		EEnum LANG_ENUM = eINSTANCE.getLangEnum();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.KeyElements <em>Key Elements</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.KeyElements
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getKeyElements()
		 * @generated
		 */
		EEnum KEY_ELEMENTS = eINSTANCE.getKeyElements();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.ReferableElements <em>Referable Elements</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.ReferableElements
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getReferableElements()
		 * @generated
		 */
		EEnum REFERABLE_ELEMENTS = eINSTANCE.getReferableElements();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.IdentifiableElement <em>Identifiable Element</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.IdentifiableElement
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getIdentifiableElement()
		 * @generated
		 */
		EEnum IDENTIFIABLE_ELEMENT = eINSTANCE.getIdentifiableElement();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.KeyType <em>Key Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.KeyType
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getKeyType()
		 * @generated
		 */
		EEnum KEY_TYPE = eINSTANCE.getKeyType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.LocalKeyType <em>Local Key Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.LocalKeyType
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getLocalKeyType()
		 * @generated
		 */
		EEnum LOCAL_KEY_TYPE = eINSTANCE.getLocalKeyType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.ModelingKind <em>Modeling Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.ModelingKind
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getModelingKind()
		 * @generated
		 */
		EEnum MODELING_KIND = eINSTANCE.getModelingKind();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.ProtocolKind <em>Protocol Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.ProtocolKind
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getProtocolKind()
		 * @generated
		 */
		EEnum PROTOCOL_KIND = eINSTANCE.getProtocolKind();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.IdType <em>Id Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.IdType
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getIdType()
		 * @generated
		 */
		EEnum ID_TYPE = eINSTANCE.getIdType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.AssetKind <em>Asset Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.AssetKind
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getAssetKind()
		 * @generated
		 */
		EEnum ASSET_KIND = eINSTANCE.getAssetKind();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.MimeType <em>Mime Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.MimeType
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getMimeType()
		 * @generated
		 */
		EEnum MIME_TYPE = eINSTANCE.getMimeType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.SecurityKind <em>Security Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.SecurityKind
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getSecurityKind()
		 * @generated
		 */
		EEnum SECURITY_KIND = eINSTANCE.getSecurityKind();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.EntityType <em>Entity Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.EntityType
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getEntityType()
		 * @generated
		 */
		EEnum ENTITY_TYPE = eINSTANCE.getEntityType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.LevelType <em>Level Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.LevelType
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getLevelType()
		 * @generated
		 */
		EEnum LEVEL_TYPE = eINSTANCE.getLevelType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.aas.DataTypeIEC61360 <em>Data Type IEC61360</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.aas.DataTypeIEC61360
		 * @see org.eclipse.papyrus.aas.impl.AASPackageImpl#getDataTypeIEC61360()
		 * @generated
		 */
		EEnum DATA_TYPE_IEC61360 = eINSTANCE.getDataTypeIEC61360();

	}

} //AASPackage
