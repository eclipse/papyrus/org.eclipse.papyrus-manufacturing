/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>View</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.aas.View#getContainedElement <em>Contained Element</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.aas.AASPackage#getView()
 * @model
 * @generated
 */
public interface View extends Referable, HasSemantics, HasDataSpecification {
	/**
	 * Returns the value of the '<em><b>Contained Element</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.aas.Referable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contained Element</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contained Element</em>' reference list.
	 * @see org.eclipse.papyrus.aas.AASPackage#getView_ContainedElement()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Referable> getContainedElement();

} // View
