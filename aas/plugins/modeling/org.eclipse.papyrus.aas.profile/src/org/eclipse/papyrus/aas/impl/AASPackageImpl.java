/**
 * Copyright (c) 2023 CEA LIST and others.
 *  
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 * 
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *  	CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.aas.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.aas.AASEndpoint;
import org.eclipse.papyrus.aas.AASFactory;
import org.eclipse.papyrus.aas.AASPackage;
import org.eclipse.papyrus.aas.AccessControl;
import org.eclipse.papyrus.aas.AccessControlPolicyPoints;
import org.eclipse.papyrus.aas.AdministrativeInformation;
import org.eclipse.papyrus.aas.Asset;
import org.eclipse.papyrus.aas.AssetAdministrationShell;
import org.eclipse.papyrus.aas.AssetInformation;
import org.eclipse.papyrus.aas.AssetKind;
import org.eclipse.papyrus.aas.BasicEvent;
import org.eclipse.papyrus.aas.Capability;
import org.eclipse.papyrus.aas.Certificate;
import org.eclipse.papyrus.aas.ConceptDescription;
import org.eclipse.papyrus.aas.DataElement;
import org.eclipse.papyrus.aas.DataSpecificationContent;
import org.eclipse.papyrus.aas.DataSpecificationIEC61360;
import org.eclipse.papyrus.aas.DataTypeIEC61360;
import org.eclipse.papyrus.aas.Endpoint;
import org.eclipse.papyrus.aas.Entity;
import org.eclipse.papyrus.aas.EntityType;
import org.eclipse.papyrus.aas.Event;
import org.eclipse.papyrus.aas.File;
import org.eclipse.papyrus.aas.HasDataSpecification;
import org.eclipse.papyrus.aas.HasKind;
import org.eclipse.papyrus.aas.HasSemantics;
import org.eclipse.papyrus.aas.IdType;
import org.eclipse.papyrus.aas.Identifiable;
import org.eclipse.papyrus.aas.IdentifiableElement;
import org.eclipse.papyrus.aas.Identifier;
import org.eclipse.papyrus.aas.IdentifierKeyValuePair;
import org.eclipse.papyrus.aas.IdentifierType;
import org.eclipse.papyrus.aas.Key;
import org.eclipse.papyrus.aas.KeyElements;
import org.eclipse.papyrus.aas.KeyType;
import org.eclipse.papyrus.aas.LangEnum;
import org.eclipse.papyrus.aas.LangString;
import org.eclipse.papyrus.aas.LangStringSet;
import org.eclipse.papyrus.aas.LevelType;
import org.eclipse.papyrus.aas.LocalKeyType;
import org.eclipse.papyrus.aas.MimeType;
import org.eclipse.papyrus.aas.ModelingKind;
import org.eclipse.papyrus.aas.MultiLanguageProperty;
import org.eclipse.papyrus.aas.NodeId;
import org.eclipse.papyrus.aas.Operation;
import org.eclipse.papyrus.aas.Property;
import org.eclipse.papyrus.aas.ProtocolKind;
import org.eclipse.papyrus.aas.Range;
import org.eclipse.papyrus.aas.Referable;
import org.eclipse.papyrus.aas.ReferableElements;
import org.eclipse.papyrus.aas.Reference;
import org.eclipse.papyrus.aas.ReferenceElement;
import org.eclipse.papyrus.aas.RelationshipElement;
import org.eclipse.papyrus.aas.Security;
import org.eclipse.papyrus.aas.SecurityKind;
import org.eclipse.papyrus.aas.Submodel;
import org.eclipse.papyrus.aas.SubmodelElement;
import org.eclipse.papyrus.aas.SubmodelElementCollection;
import org.eclipse.papyrus.aas.ValueReferencePairType;
import org.eclipse.papyrus.aas.View;
import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AASPackageImpl extends EPackageImpl implements AASPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetAdministrationShellEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass identifiableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass referableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass langStringEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass administrativeInformationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass identifierEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hasDataSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass referenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass keyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass securityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass accessControlPolicyPointsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass accessControlEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass submodelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hasKindEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hasSemanticsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass submodelElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass endpointEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nodeIdEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass certificateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetInformationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass identifierKeyValuePairEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fileEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aasEndpointEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass entityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass relationshipElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass submodelElementCollectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass referenceElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass basicEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rangeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass capabilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conceptDescriptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass langStringSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataSpecificationContentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataSpecificationIEC61360EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valueReferencePairTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass viewEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass multiLanguagePropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum identifierTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum langEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum keyElementsEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum referableElementsEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum identifiableElementEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum keyTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum localKeyTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum modelingKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum protocolKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum idTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum assetKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mimeTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum securityKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum entityTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum levelTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dataTypeIEC61360EEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.papyrus.aas.AASPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private AASPackageImpl() {
		super(eNS_URI, AASFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link AASPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static AASPackage init() {
		if (isInited) return (AASPackage)EPackage.Registry.INSTANCE.getEPackage(AASPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredAASPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		AASPackageImpl theAASPackage = registeredAASPackage instanceof AASPackageImpl ? (AASPackageImpl)registeredAASPackage : new AASPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theAASPackage.createPackageContents();

		// Initialize created meta-data
		theAASPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theAASPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(AASPackage.eNS_URI, theAASPackage);
		return theAASPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAssetAdministrationShell() {
		return assetAdministrationShellEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetAdministrationShell_DerivedFrom() {
		return (EReference)assetAdministrationShellEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetAdministrationShell_Security() {
		return (EReference)assetAdministrationShellEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetAdministrationShell_AssetInformation() {
		return (EReference)assetAdministrationShellEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetAdministrationShell_Asset() {
		return (EReference)assetAdministrationShellEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetAdministrationShell_Submodel() {
		return (EReference)assetAdministrationShellEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetAdministrationShell_Endpoint() {
		return (EReference)assetAdministrationShellEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getIdentifiable() {
		return identifiableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getIdentifiable_Administration() {
		return (EReference)identifiableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getIdentifiable_Identification() {
		return (EReference)identifiableEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getIdentifiable_Base_Package() {
		return (EReference)identifiableEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getReferable() {
		return referableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getReferable_IdShort() {
		return (EAttribute)referableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getReferable_Category() {
		return (EAttribute)referableEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getReferable_Description() {
		return (EReference)referableEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getReferable_Base_Class() {
		return (EReference)referableEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLangString() {
		return langStringEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLangString_Lang() {
		return (EAttribute)langStringEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLangString_Value() {
		return (EAttribute)langStringEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAdministrativeInformation() {
		return administrativeInformationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAdministrativeInformation_Version() {
		return (EAttribute)administrativeInformationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAdministrativeInformation_Revision() {
		return (EAttribute)administrativeInformationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getIdentifier() {
		return identifierEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getIdentifier_IdType() {
		return (EAttribute)identifierEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getIdentifier_Id() {
		return (EAttribute)identifierEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getHasDataSpecification() {
		return hasDataSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getHasDataSpecification_DataSpecification() {
		return (EReference)hasDataSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getReference() {
		return referenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getReference_Key() {
		return (EReference)referenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getReference_Base_Class() {
		return (EReference)referenceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getKey() {
		return keyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getKey_Type() {
		return (EAttribute)keyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getKey_Value() {
		return (EAttribute)keyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getKey_IdType() {
		return (EAttribute)keyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getKey_Base_Class() {
		return (EReference)keyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSecurity() {
		return securityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSecurity_Base_Class() {
		return (EReference)securityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSecurity_AccessControlPolicyPoints() {
		return (EReference)securityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSecurity_Certificate() {
		return (EReference)securityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSecurity_RequiredCertificateExtension() {
		return (EReference)securityEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAccessControlPolicyPoints() {
		return accessControlPolicyPointsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAccessControlPolicyPoints_LocalAccessControl() {
		return (EReference)accessControlPolicyPointsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAccessControlPolicyPoints_ExternalAccessControl() {
		return (EAttribute)accessControlPolicyPointsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAccessControlPolicyPoints_ExternalInformationPoints() {
		return (EAttribute)accessControlPolicyPointsEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAccessControlPolicyPoints_InternalInformationPoint() {
		return (EReference)accessControlPolicyPointsEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAccessControlPolicyPoints_ExternalPolicyDecisionPoints() {
		return (EAttribute)accessControlPolicyPointsEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAccessControlPolicyPoints_ExternalPolicyEnforcementPoint() {
		return (EAttribute)accessControlPolicyPointsEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAccessControl() {
		return accessControlEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSubmodel() {
		return submodelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSubmodel_Submodelelement() {
		return (EReference)submodelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getHasKind() {
		return hasKindEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getHasKind_Kind() {
		return (EAttribute)hasKindEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getHasKind_Base_HasKind_Class() {
		return (EReference)hasKindEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getHasSemantics() {
		return hasSemanticsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getHasSemantics_SemanticId() {
		return (EReference)hasSemanticsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getHasSemantics_Base_HasSemantics_Class() {
		return (EReference)hasSemanticsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSubmodelElement() {
		return submodelElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSubmodelElement_IsDynamic() {
		return (EAttribute)submodelElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSubmodelElement_EndPoint() {
		return (EReference)submodelElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSubmodelElement_NodeId() {
		return (EReference)submodelElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEndpoint() {
		return endpointEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEndpoint_Address() {
		return (EAttribute)endpointEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEndpoint_Protocol() {
		return (EAttribute)endpointEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEndpoint_Name() {
		return (EAttribute)endpointEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getNodeId() {
		return nodeIdEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getNodeId_NameSpaceIndex() {
		return (EAttribute)nodeIdEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getNodeId_Identifier() {
		return (EAttribute)nodeIdEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getNodeId_IdType() {
		return (EAttribute)nodeIdEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCertificate() {
		return certificateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAssetInformation() {
		return assetInformationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAssetInformation_AssetKind() {
		return (EAttribute)assetInformationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetInformation_GlobalAssetId() {
		return (EReference)assetInformationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetInformation_SpecificAssetId() {
		return (EReference)assetInformationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetInformation_BillOfMaterial() {
		return (EReference)assetInformationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAssetInformation_DefaultThumbnail() {
		return (EReference)assetInformationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getIdentifierKeyValuePair() {
		return identifierKeyValuePairEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getIdentifierKeyValuePair_Key() {
		return (EAttribute)identifierKeyValuePairEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getIdentifierKeyValuePair_Value() {
		return (EAttribute)identifierKeyValuePairEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getIdentifierKeyValuePair_ExternalSubjectId() {
		return (EReference)identifierKeyValuePairEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFile() {
		return fileEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFile_Path() {
		return (EAttribute)fileEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFile_MimeType() {
		return (EAttribute)fileEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDataElement() {
		return dataElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDataElement_Base_Property() {
		return (EReference)dataElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAsset() {
		return assetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAsset_Kind() {
		return (EAttribute)assetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAsset_Endpoint() {
		return (EReference)assetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAASEndpoint() {
		return aasEndpointEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAASEndpoint_Address() {
		return (EAttribute)aasEndpointEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAASEndpoint_Port() {
		return (EAttribute)aasEndpointEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAASEndpoint_Security() {
		return (EAttribute)aasEndpointEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEntity() {
		return entityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEntity_EntityType() {
		return (EAttribute)entityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getEntity_Asset() {
		return (EReference)entityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getEntity_Base_Property() {
		return (EReference)entityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRelationshipElement() {
		return relationshipElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRelationshipElement_Base_Dependency() {
		return (EReference)relationshipElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getOperation() {
		return operationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getOperation_Base_Operation() {
		return (EReference)operationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSubmodelElementCollection() {
		return submodelElementCollectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSubmodelElementCollection_Value() {
		return (EReference)submodelElementCollectionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSubmodelElementCollection_Base_Property() {
		return (EReference)submodelElementCollectionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSubmodelElementCollection_Base_DataType() {
		return (EReference)submodelElementCollectionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSubmodelElementCollection_Ordered() {
		return (EAttribute)submodelElementCollectionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSubmodelElementCollection_AllowDuplicates() {
		return (EAttribute)submodelElementCollectionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getProperty() {
		return propertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getReferenceElement() {
		return referenceElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getReferenceElement_Value() {
		return (EReference)referenceElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEvent() {
		return eventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getEvent_Base_Property() {
		return (EReference)eventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBasicEvent() {
		return basicEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBasicEvent_Observed() {
		return (EReference)basicEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRange() {
		return rangeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRange_Min() {
		return (EAttribute)rangeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRange_Max() {
		return (EAttribute)rangeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCapability() {
		return capabilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCapability_Base_Property() {
		return (EReference)capabilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getConceptDescription() {
		return conceptDescriptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getConceptDescription_IsCaseOf() {
		return (EReference)conceptDescriptionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLangStringSet() {
		return langStringSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDataSpecificationContent() {
		return dataSpecificationContentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDataSpecificationIEC61360() {
		return dataSpecificationIEC61360EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDataSpecificationIEC61360_Base_Class() {
		return (EReference)dataSpecificationIEC61360EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDataSpecificationIEC61360_PreferredName() {
		return (EReference)dataSpecificationIEC61360EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDataSpecificationIEC61360_ShortName() {
		return (EReference)dataSpecificationIEC61360EClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDataSpecificationIEC61360_Unit() {
		return (EAttribute)dataSpecificationIEC61360EClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDataSpecificationIEC61360_UnitId() {
		return (EReference)dataSpecificationIEC61360EClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDataSpecificationIEC61360_SourceOfDefinition() {
		return (EAttribute)dataSpecificationIEC61360EClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDataSpecificationIEC61360_Symbol() {
		return (EAttribute)dataSpecificationIEC61360EClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDataSpecificationIEC61360_DataType() {
		return (EReference)dataSpecificationIEC61360EClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDataSpecificationIEC61360_Definition() {
		return (EReference)dataSpecificationIEC61360EClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDataSpecificationIEC61360_ValueFormat() {
		return (EAttribute)dataSpecificationIEC61360EClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDataSpecificationIEC61360_ValueList() {
		return (EReference)dataSpecificationIEC61360EClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDataSpecificationIEC61360_Value() {
		return (EReference)dataSpecificationIEC61360EClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDataSpecificationIEC61360_ValueId() {
		return (EReference)dataSpecificationIEC61360EClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDataSpecificationIEC61360_LevelType() {
		return (EAttribute)dataSpecificationIEC61360EClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getValueReferencePairType() {
		return valueReferencePairTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getValueReferencePairType_Value() {
		return (EReference)valueReferencePairTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getValueReferencePairType_ValueId() {
		return (EReference)valueReferencePairTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getView() {
		return viewEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getView_ContainedElement() {
		return (EReference)viewEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMultiLanguageProperty() {
		return multiLanguagePropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMultiLanguageProperty_Value() {
		return (EReference)multiLanguagePropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMultiLanguageProperty_ValueId() {
		return (EReference)multiLanguagePropertyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getIdentifierType() {
		return identifierTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getLangEnum() {
		return langEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getKeyElements() {
		return keyElementsEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getReferableElements() {
		return referableElementsEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getIdentifiableElement() {
		return identifiableElementEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getKeyType() {
		return keyTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getLocalKeyType() {
		return localKeyTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getModelingKind() {
		return modelingKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getProtocolKind() {
		return protocolKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getIdType() {
		return idTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getAssetKind() {
		return assetKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getMimeType() {
		return mimeTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getSecurityKind() {
		return securityKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getEntityType() {
		return entityTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getLevelType() {
		return levelTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getDataTypeIEC61360() {
		return dataTypeIEC61360EEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AASFactory getAASFactory() {
		return (AASFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		assetAdministrationShellEClass = createEClass(ASSET_ADMINISTRATION_SHELL);
		createEReference(assetAdministrationShellEClass, ASSET_ADMINISTRATION_SHELL__DERIVED_FROM);
		createEReference(assetAdministrationShellEClass, ASSET_ADMINISTRATION_SHELL__SECURITY);
		createEReference(assetAdministrationShellEClass, ASSET_ADMINISTRATION_SHELL__ASSET_INFORMATION);
		createEReference(assetAdministrationShellEClass, ASSET_ADMINISTRATION_SHELL__ASSET);
		createEReference(assetAdministrationShellEClass, ASSET_ADMINISTRATION_SHELL__SUBMODEL);
		createEReference(assetAdministrationShellEClass, ASSET_ADMINISTRATION_SHELL__ENDPOINT);

		identifiableEClass = createEClass(IDENTIFIABLE);
		createEReference(identifiableEClass, IDENTIFIABLE__ADMINISTRATION);
		createEReference(identifiableEClass, IDENTIFIABLE__IDENTIFICATION);
		createEReference(identifiableEClass, IDENTIFIABLE__BASE_PACKAGE);

		referableEClass = createEClass(REFERABLE);
		createEAttribute(referableEClass, REFERABLE__ID_SHORT);
		createEAttribute(referableEClass, REFERABLE__CATEGORY);
		createEReference(referableEClass, REFERABLE__DESCRIPTION);
		createEReference(referableEClass, REFERABLE__BASE_CLASS);

		langStringEClass = createEClass(LANG_STRING);
		createEAttribute(langStringEClass, LANG_STRING__LANG);
		createEAttribute(langStringEClass, LANG_STRING__VALUE);

		administrativeInformationEClass = createEClass(ADMINISTRATIVE_INFORMATION);
		createEAttribute(administrativeInformationEClass, ADMINISTRATIVE_INFORMATION__VERSION);
		createEAttribute(administrativeInformationEClass, ADMINISTRATIVE_INFORMATION__REVISION);

		identifierEClass = createEClass(IDENTIFIER);
		createEAttribute(identifierEClass, IDENTIFIER__ID_TYPE);
		createEAttribute(identifierEClass, IDENTIFIER__ID);

		hasDataSpecificationEClass = createEClass(HAS_DATA_SPECIFICATION);
		createEReference(hasDataSpecificationEClass, HAS_DATA_SPECIFICATION__DATA_SPECIFICATION);

		referenceEClass = createEClass(REFERENCE);
		createEReference(referenceEClass, REFERENCE__KEY);
		createEReference(referenceEClass, REFERENCE__BASE_CLASS);

		keyEClass = createEClass(KEY);
		createEAttribute(keyEClass, KEY__TYPE);
		createEAttribute(keyEClass, KEY__VALUE);
		createEAttribute(keyEClass, KEY__ID_TYPE);
		createEReference(keyEClass, KEY__BASE_CLASS);

		securityEClass = createEClass(SECURITY);
		createEReference(securityEClass, SECURITY__BASE_CLASS);
		createEReference(securityEClass, SECURITY__ACCESS_CONTROL_POLICY_POINTS);
		createEReference(securityEClass, SECURITY__CERTIFICATE);
		createEReference(securityEClass, SECURITY__REQUIRED_CERTIFICATE_EXTENSION);

		accessControlPolicyPointsEClass = createEClass(ACCESS_CONTROL_POLICY_POINTS);
		createEReference(accessControlPolicyPointsEClass, ACCESS_CONTROL_POLICY_POINTS__LOCAL_ACCESS_CONTROL);
		createEAttribute(accessControlPolicyPointsEClass, ACCESS_CONTROL_POLICY_POINTS__EXTERNAL_ACCESS_CONTROL);
		createEAttribute(accessControlPolicyPointsEClass, ACCESS_CONTROL_POLICY_POINTS__EXTERNAL_INFORMATION_POINTS);
		createEReference(accessControlPolicyPointsEClass, ACCESS_CONTROL_POLICY_POINTS__INTERNAL_INFORMATION_POINT);
		createEAttribute(accessControlPolicyPointsEClass, ACCESS_CONTROL_POLICY_POINTS__EXTERNAL_POLICY_DECISION_POINTS);
		createEAttribute(accessControlPolicyPointsEClass, ACCESS_CONTROL_POLICY_POINTS__EXTERNAL_POLICY_ENFORCEMENT_POINT);

		accessControlEClass = createEClass(ACCESS_CONTROL);

		submodelEClass = createEClass(SUBMODEL);
		createEReference(submodelEClass, SUBMODEL__SUBMODELELEMENT);

		hasKindEClass = createEClass(HAS_KIND);
		createEAttribute(hasKindEClass, HAS_KIND__KIND);
		createEReference(hasKindEClass, HAS_KIND__BASE_HAS_KIND_CLASS);

		hasSemanticsEClass = createEClass(HAS_SEMANTICS);
		createEReference(hasSemanticsEClass, HAS_SEMANTICS__SEMANTIC_ID);
		createEReference(hasSemanticsEClass, HAS_SEMANTICS__BASE_HAS_SEMANTICS_CLASS);

		submodelElementEClass = createEClass(SUBMODEL_ELEMENT);
		createEAttribute(submodelElementEClass, SUBMODEL_ELEMENT__IS_DYNAMIC);
		createEReference(submodelElementEClass, SUBMODEL_ELEMENT__END_POINT);
		createEReference(submodelElementEClass, SUBMODEL_ELEMENT__NODE_ID);

		endpointEClass = createEClass(ENDPOINT);
		createEAttribute(endpointEClass, ENDPOINT__ADDRESS);
		createEAttribute(endpointEClass, ENDPOINT__PROTOCOL);
		createEAttribute(endpointEClass, ENDPOINT__NAME);

		nodeIdEClass = createEClass(NODE_ID);
		createEAttribute(nodeIdEClass, NODE_ID__NAME_SPACE_INDEX);
		createEAttribute(nodeIdEClass, NODE_ID__IDENTIFIER);
		createEAttribute(nodeIdEClass, NODE_ID__ID_TYPE);

		certificateEClass = createEClass(CERTIFICATE);

		assetInformationEClass = createEClass(ASSET_INFORMATION);
		createEAttribute(assetInformationEClass, ASSET_INFORMATION__ASSET_KIND);
		createEReference(assetInformationEClass, ASSET_INFORMATION__GLOBAL_ASSET_ID);
		createEReference(assetInformationEClass, ASSET_INFORMATION__SPECIFIC_ASSET_ID);
		createEReference(assetInformationEClass, ASSET_INFORMATION__BILL_OF_MATERIAL);
		createEReference(assetInformationEClass, ASSET_INFORMATION__DEFAULT_THUMBNAIL);

		identifierKeyValuePairEClass = createEClass(IDENTIFIER_KEY_VALUE_PAIR);
		createEAttribute(identifierKeyValuePairEClass, IDENTIFIER_KEY_VALUE_PAIR__KEY);
		createEAttribute(identifierKeyValuePairEClass, IDENTIFIER_KEY_VALUE_PAIR__VALUE);
		createEReference(identifierKeyValuePairEClass, IDENTIFIER_KEY_VALUE_PAIR__EXTERNAL_SUBJECT_ID);

		fileEClass = createEClass(FILE);
		createEAttribute(fileEClass, FILE__PATH);
		createEAttribute(fileEClass, FILE__MIME_TYPE);

		dataElementEClass = createEClass(DATA_ELEMENT);
		createEReference(dataElementEClass, DATA_ELEMENT__BASE_PROPERTY);

		assetEClass = createEClass(ASSET);
		createEAttribute(assetEClass, ASSET__KIND);
		createEReference(assetEClass, ASSET__ENDPOINT);

		aasEndpointEClass = createEClass(AAS_ENDPOINT);
		createEAttribute(aasEndpointEClass, AAS_ENDPOINT__ADDRESS);
		createEAttribute(aasEndpointEClass, AAS_ENDPOINT__PORT);
		createEAttribute(aasEndpointEClass, AAS_ENDPOINT__SECURITY);

		entityEClass = createEClass(ENTITY);
		createEAttribute(entityEClass, ENTITY__ENTITY_TYPE);
		createEReference(entityEClass, ENTITY__ASSET);
		createEReference(entityEClass, ENTITY__BASE_PROPERTY);

		relationshipElementEClass = createEClass(RELATIONSHIP_ELEMENT);
		createEReference(relationshipElementEClass, RELATIONSHIP_ELEMENT__BASE_DEPENDENCY);

		operationEClass = createEClass(OPERATION);
		createEReference(operationEClass, OPERATION__BASE_OPERATION);

		submodelElementCollectionEClass = createEClass(SUBMODEL_ELEMENT_COLLECTION);
		createEReference(submodelElementCollectionEClass, SUBMODEL_ELEMENT_COLLECTION__VALUE);
		createEReference(submodelElementCollectionEClass, SUBMODEL_ELEMENT_COLLECTION__BASE_PROPERTY);
		createEReference(submodelElementCollectionEClass, SUBMODEL_ELEMENT_COLLECTION__BASE_DATA_TYPE);
		createEAttribute(submodelElementCollectionEClass, SUBMODEL_ELEMENT_COLLECTION__ORDERED);
		createEAttribute(submodelElementCollectionEClass, SUBMODEL_ELEMENT_COLLECTION__ALLOW_DUPLICATES);

		propertyEClass = createEClass(PROPERTY);

		referenceElementEClass = createEClass(REFERENCE_ELEMENT);
		createEReference(referenceElementEClass, REFERENCE_ELEMENT__VALUE);

		eventEClass = createEClass(EVENT);
		createEReference(eventEClass, EVENT__BASE_PROPERTY);

		basicEventEClass = createEClass(BASIC_EVENT);
		createEReference(basicEventEClass, BASIC_EVENT__OBSERVED);

		rangeEClass = createEClass(RANGE);
		createEAttribute(rangeEClass, RANGE__MIN);
		createEAttribute(rangeEClass, RANGE__MAX);

		capabilityEClass = createEClass(CAPABILITY);
		createEReference(capabilityEClass, CAPABILITY__BASE_PROPERTY);

		conceptDescriptionEClass = createEClass(CONCEPT_DESCRIPTION);
		createEReference(conceptDescriptionEClass, CONCEPT_DESCRIPTION__IS_CASE_OF);

		langStringSetEClass = createEClass(LANG_STRING_SET);

		dataSpecificationContentEClass = createEClass(DATA_SPECIFICATION_CONTENT);

		dataSpecificationIEC61360EClass = createEClass(DATA_SPECIFICATION_IEC61360);
		createEReference(dataSpecificationIEC61360EClass, DATA_SPECIFICATION_IEC61360__BASE_CLASS);
		createEReference(dataSpecificationIEC61360EClass, DATA_SPECIFICATION_IEC61360__PREFERRED_NAME);
		createEReference(dataSpecificationIEC61360EClass, DATA_SPECIFICATION_IEC61360__SHORT_NAME);
		createEAttribute(dataSpecificationIEC61360EClass, DATA_SPECIFICATION_IEC61360__UNIT);
		createEReference(dataSpecificationIEC61360EClass, DATA_SPECIFICATION_IEC61360__UNIT_ID);
		createEAttribute(dataSpecificationIEC61360EClass, DATA_SPECIFICATION_IEC61360__SOURCE_OF_DEFINITION);
		createEAttribute(dataSpecificationIEC61360EClass, DATA_SPECIFICATION_IEC61360__SYMBOL);
		createEReference(dataSpecificationIEC61360EClass, DATA_SPECIFICATION_IEC61360__DATA_TYPE);
		createEReference(dataSpecificationIEC61360EClass, DATA_SPECIFICATION_IEC61360__DEFINITION);
		createEAttribute(dataSpecificationIEC61360EClass, DATA_SPECIFICATION_IEC61360__VALUE_FORMAT);
		createEReference(dataSpecificationIEC61360EClass, DATA_SPECIFICATION_IEC61360__VALUE_LIST);
		createEReference(dataSpecificationIEC61360EClass, DATA_SPECIFICATION_IEC61360__VALUE);
		createEReference(dataSpecificationIEC61360EClass, DATA_SPECIFICATION_IEC61360__VALUE_ID);
		createEAttribute(dataSpecificationIEC61360EClass, DATA_SPECIFICATION_IEC61360__LEVEL_TYPE);

		valueReferencePairTypeEClass = createEClass(VALUE_REFERENCE_PAIR_TYPE);
		createEReference(valueReferencePairTypeEClass, VALUE_REFERENCE_PAIR_TYPE__VALUE);
		createEReference(valueReferencePairTypeEClass, VALUE_REFERENCE_PAIR_TYPE__VALUE_ID);

		viewEClass = createEClass(VIEW);
		createEReference(viewEClass, VIEW__CONTAINED_ELEMENT);

		multiLanguagePropertyEClass = createEClass(MULTI_LANGUAGE_PROPERTY);
		createEReference(multiLanguagePropertyEClass, MULTI_LANGUAGE_PROPERTY__VALUE);
		createEReference(multiLanguagePropertyEClass, MULTI_LANGUAGE_PROPERTY__VALUE_ID);

		// Create enums
		identifierTypeEEnum = createEEnum(IDENTIFIER_TYPE);
		langEnumEEnum = createEEnum(LANG_ENUM);
		keyElementsEEnum = createEEnum(KEY_ELEMENTS);
		referableElementsEEnum = createEEnum(REFERABLE_ELEMENTS);
		identifiableElementEEnum = createEEnum(IDENTIFIABLE_ELEMENT);
		keyTypeEEnum = createEEnum(KEY_TYPE);
		localKeyTypeEEnum = createEEnum(LOCAL_KEY_TYPE);
		modelingKindEEnum = createEEnum(MODELING_KIND);
		protocolKindEEnum = createEEnum(PROTOCOL_KIND);
		idTypeEEnum = createEEnum(ID_TYPE);
		assetKindEEnum = createEEnum(ASSET_KIND);
		mimeTypeEEnum = createEEnum(MIME_TYPE);
		securityKindEEnum = createEEnum(SECURITY_KIND);
		entityTypeEEnum = createEEnum(ENTITY_TYPE);
		levelTypeEEnum = createEEnum(LEVEL_TYPE);
		dataTypeIEC61360EEnum = createEEnum(DATA_TYPE_IEC61360);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		assetAdministrationShellEClass.getESuperTypes().add(this.getIdentifiable());
		assetAdministrationShellEClass.getESuperTypes().add(this.getHasDataSpecification());
		identifiableEClass.getESuperTypes().add(this.getReferable());
		submodelEClass.getESuperTypes().add(this.getIdentifiable());
		submodelEClass.getESuperTypes().add(this.getHasKind());
		submodelEClass.getESuperTypes().add(this.getHasSemantics());
		submodelEClass.getESuperTypes().add(this.getHasDataSpecification());
		submodelElementEClass.getESuperTypes().add(this.getReferable());
		submodelElementEClass.getESuperTypes().add(this.getHasKind());
		submodelElementEClass.getESuperTypes().add(this.getHasSemantics());
		submodelElementEClass.getESuperTypes().add(this.getHasDataSpecification());
		identifierKeyValuePairEClass.getESuperTypes().add(this.getHasSemantics());
		fileEClass.getESuperTypes().add(this.getDataElement());
		dataElementEClass.getESuperTypes().add(this.getSubmodelElement());
		assetEClass.getESuperTypes().add(this.getIdentifiable());
		assetEClass.getESuperTypes().add(this.getHasDataSpecification());
		entityEClass.getESuperTypes().add(this.getSubmodelElement());
		relationshipElementEClass.getESuperTypes().add(this.getSubmodelElement());
		operationEClass.getESuperTypes().add(this.getSubmodelElement());
		submodelElementCollectionEClass.getESuperTypes().add(this.getSubmodelElement());
		propertyEClass.getESuperTypes().add(this.getDataElement());
		referenceElementEClass.getESuperTypes().add(this.getDataElement());
		eventEClass.getESuperTypes().add(this.getSubmodelElement());
		basicEventEClass.getESuperTypes().add(this.getEvent());
		rangeEClass.getESuperTypes().add(this.getDataElement());
		capabilityEClass.getESuperTypes().add(this.getSubmodelElement());
		conceptDescriptionEClass.getESuperTypes().add(this.getIdentifiable());
		conceptDescriptionEClass.getESuperTypes().add(this.getHasDataSpecification());
		viewEClass.getESuperTypes().add(this.getReferable());
		viewEClass.getESuperTypes().add(this.getHasSemantics());
		viewEClass.getESuperTypes().add(this.getHasDataSpecification());
		multiLanguagePropertyEClass.getESuperTypes().add(this.getDataElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(assetAdministrationShellEClass, AssetAdministrationShell.class, "AssetAdministrationShell", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssetAdministrationShell_DerivedFrom(), this.getAssetAdministrationShell(), null, "derivedFrom", null, 0, 1, AssetAdministrationShell.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAssetAdministrationShell_Security(), this.getSecurity(), null, "security", null, 0, 1, AssetAdministrationShell.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAssetAdministrationShell_AssetInformation(), this.getAssetInformation(), null, "assetInformation", null, 1, 1, AssetAdministrationShell.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAssetAdministrationShell_Asset(), this.getAsset(), null, "asset", null, 1, 1, AssetAdministrationShell.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAssetAdministrationShell_Submodel(), this.getSubmodel(), null, "submodel", null, 0, -1, AssetAdministrationShell.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEReference(getAssetAdministrationShell_Endpoint(), this.getAASEndpoint(), null, "endpoint", null, 0, 1, AssetAdministrationShell.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(identifiableEClass, Identifiable.class, "Identifiable", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIdentifiable_Administration(), this.getAdministrativeInformation(), null, "administration", null, 0, 1, Identifiable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getIdentifiable_Identification(), this.getIdentifier(), null, "identification", null, 1, 1, Identifiable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getIdentifiable_Base_Package(), theUMLPackage.getPackage(), null, "base_Package", null, 0, 1, Identifiable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(referableEClass, Referable.class, "Referable", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getReferable_IdShort(), theTypesPackage.getString(), "idShort", null, 1, 1, Referable.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEAttribute(getReferable_Category(), theTypesPackage.getString(), "category", null, 0, 1, Referable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getReferable_Description(), this.getLangString(), null, "description", null, 0, -1, Referable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getReferable_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 0, 1, Referable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(langStringEClass, LangString.class, "LangString", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLangString_Lang(), this.getLangEnum(), "lang", null, 1, 1, LangString.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLangString_Value(), theTypesPackage.getString(), "value", null, 1, 1, LangString.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(administrativeInformationEClass, AdministrativeInformation.class, "AdministrativeInformation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAdministrativeInformation_Version(), theTypesPackage.getString(), "version", null, 1, 1, AdministrativeInformation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getAdministrativeInformation_Revision(), theTypesPackage.getString(), "revision", null, 1, 1, AdministrativeInformation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(identifierEClass, Identifier.class, "Identifier", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIdentifier_IdType(), this.getIdentifierType(), "idType", null, 1, 1, Identifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getIdentifier_Id(), theTypesPackage.getString(), "id", null, 1, 1, Identifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(hasDataSpecificationEClass, HasDataSpecification.class, "HasDataSpecification", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHasDataSpecification_DataSpecification(), this.getReference(), null, "dataSpecification", null, 0, -1, HasDataSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(referenceEClass, Reference.class, "Reference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReference_Key(), this.getKey(), null, "key", null, 1, -1, Reference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getReference_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 0, 1, Reference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(keyEClass, Key.class, "Key", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getKey_Type(), this.getKeyElements(), "type", null, 1, 1, Key.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getKey_Value(), theTypesPackage.getString(), "value", null, 1, 1, Key.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getKey_IdType(), this.getKeyType(), "idType", null, 1, 1, Key.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getKey_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 0, 1, Key.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(securityEClass, Security.class, "Security", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSecurity_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 0, 1, Security.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSecurity_AccessControlPolicyPoints(), this.getAccessControlPolicyPoints(), null, "accessControlPolicyPoints", null, 1, 1, Security.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSecurity_Certificate(), this.getCertificate(), null, "certificate", null, 0, -1, Security.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSecurity_RequiredCertificateExtension(), this.getReference(), null, "requiredCertificateExtension", null, 0, -1, Security.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(accessControlPolicyPointsEClass, AccessControlPolicyPoints.class, "AccessControlPolicyPoints", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAccessControlPolicyPoints_LocalAccessControl(), this.getAccessControl(), null, "localAccessControl", null, 1, 1, AccessControlPolicyPoints.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getAccessControlPolicyPoints_ExternalAccessControl(), ecorePackage.getEBoolean(), "externalAccessControl", null, 1, 1, AccessControlPolicyPoints.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getAccessControlPolicyPoints_ExternalInformationPoints(), ecorePackage.getEBoolean(), "externalInformationPoints", null, 1, 1, AccessControlPolicyPoints.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAccessControlPolicyPoints_InternalInformationPoint(), this.getSubmodel(), null, "internalInformationPoint", null, 1, 1, AccessControlPolicyPoints.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getAccessControlPolicyPoints_ExternalPolicyDecisionPoints(), ecorePackage.getEBoolean(), "externalPolicyDecisionPoints", null, 1, 1, AccessControlPolicyPoints.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getAccessControlPolicyPoints_ExternalPolicyEnforcementPoint(), ecorePackage.getEBoolean(), "externalPolicyEnforcementPoint", null, 1, 1, AccessControlPolicyPoints.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(accessControlEClass, AccessControl.class, "AccessControl", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(submodelEClass, Submodel.class, "Submodel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSubmodel_Submodelelement(), this.getSubmodelElement(), null, "submodelelement", null, 0, -1, Submodel.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);

		initEClass(hasKindEClass, HasKind.class, "HasKind", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getHasKind_Kind(), this.getModelingKind(), "kind", null, 0, 1, HasKind.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getHasKind_Base_HasKind_Class(), theUMLPackage.getClass_(), null, "base_HasKind_Class", null, 0, 1, HasKind.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(hasSemanticsEClass, HasSemantics.class, "HasSemantics", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHasSemantics_SemanticId(), this.getReference(), null, "semanticId", null, 1, 1, HasSemantics.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getHasSemantics_Base_HasSemantics_Class(), theUMLPackage.getClass_(), null, "base_HasSemantics_Class", null, 0, 1, HasSemantics.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(submodelElementEClass, SubmodelElement.class, "SubmodelElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSubmodelElement_IsDynamic(), theTypesPackage.getBoolean(), "isDynamic", null, 1, 1, SubmodelElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSubmodelElement_EndPoint(), this.getEndpoint(), null, "endPoint", null, 0, 1, SubmodelElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSubmodelElement_NodeId(), this.getNodeId(), null, "nodeId", null, 0, 1, SubmodelElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(endpointEClass, Endpoint.class, "Endpoint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEndpoint_Address(), theTypesPackage.getString(), "address", null, 0, 1, Endpoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getEndpoint_Protocol(), this.getProtocolKind(), "protocol", null, 0, 1, Endpoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getEndpoint_Name(), theTypesPackage.getString(), "name", null, 0, 1, Endpoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(nodeIdEClass, NodeId.class, "NodeId", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNodeId_NameSpaceIndex(), theTypesPackage.getInteger(), "nameSpaceIndex", null, 0, 1, NodeId.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getNodeId_Identifier(), theTypesPackage.getString(), "identifier", null, 0, 1, NodeId.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getNodeId_IdType(), this.getIdType(), "idType", null, 0, 1, NodeId.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(certificateEClass, Certificate.class, "Certificate", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(assetInformationEClass, AssetInformation.class, "AssetInformation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAssetInformation_AssetKind(), this.getAssetKind(), "assetKind", null, 1, 1, AssetInformation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAssetInformation_GlobalAssetId(), this.getReference(), null, "globalAssetId", null, 0, 1, AssetInformation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAssetInformation_SpecificAssetId(), this.getIdentifierKeyValuePair(), null, "specificAssetId", null, 0, -1, AssetInformation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAssetInformation_BillOfMaterial(), this.getSubmodel(), null, "billOfMaterial", null, 0, -1, AssetInformation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAssetInformation_DefaultThumbnail(), this.getFile(), null, "defaultThumbnail", null, 0, 1, AssetInformation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(identifierKeyValuePairEClass, IdentifierKeyValuePair.class, "IdentifierKeyValuePair", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIdentifierKeyValuePair_Key(), theTypesPackage.getString(), "key", null, 1, 1, IdentifierKeyValuePair.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getIdentifierKeyValuePair_Value(), theTypesPackage.getString(), "value", null, 1, 1, IdentifierKeyValuePair.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getIdentifierKeyValuePair_ExternalSubjectId(), this.getReference(), null, "externalSubjectId", null, 0, 1, IdentifierKeyValuePair.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(fileEClass, File.class, "File", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFile_Path(), theTypesPackage.getString(), "path", null, 0, 1, File.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getFile_MimeType(), this.getMimeType(), "mimeType", null, 1, 1, File.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(dataElementEClass, DataElement.class, "DataElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataElement_Base_Property(), theUMLPackage.getProperty(), null, "base_Property", null, 1, 1, DataElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(assetEClass, Asset.class, "Asset", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAsset_Kind(), this.getAssetKind(), "kind", null, 1, 1, Asset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAsset_Endpoint(), this.getEndpoint(), null, "endpoint", null, 0, -1, Asset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(aasEndpointEClass, AASEndpoint.class, "AASEndpoint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAASEndpoint_Address(), theTypesPackage.getString(), "address", null, 1, 1, AASEndpoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getAASEndpoint_Port(), theTypesPackage.getInteger(), "port", null, 1, 1, AASEndpoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getAASEndpoint_Security(), this.getSecurityKind(), "security", null, 1, 1, AASEndpoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(entityEClass, Entity.class, "Entity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEntity_EntityType(), this.getEntityType(), "entityType", null, 1, 1, Entity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getEntity_Asset(), this.getAsset(), null, "asset", null, 1, 1, Entity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getEntity_Base_Property(), theUMLPackage.getProperty(), null, "base_Property", null, 0, 1, Entity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(relationshipElementEClass, RelationshipElement.class, "RelationshipElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRelationshipElement_Base_Dependency(), theUMLPackage.getDependency(), null, "base_Dependency", null, 0, 1, RelationshipElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(operationEClass, Operation.class, "Operation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperation_Base_Operation(), theUMLPackage.getOperation(), null, "base_Operation", null, 1, 1, Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(submodelElementCollectionEClass, SubmodelElementCollection.class, "SubmodelElementCollection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSubmodelElementCollection_Value(), this.getSubmodelElement(), null, "value", null, 0, -1, SubmodelElementCollection.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelElementCollection_Base_Property(), theUMLPackage.getProperty(), null, "base_Property", null, 0, 1, SubmodelElementCollection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSubmodelElementCollection_Base_DataType(), theUMLPackage.getDataType(), null, "base_DataType", null, 0, 1, SubmodelElementCollection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getSubmodelElementCollection_Ordered(), theTypesPackage.getBoolean(), "ordered", null, 0, 1, SubmodelElementCollection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSubmodelElementCollection_AllowDuplicates(), theTypesPackage.getBoolean(), "allowDuplicates", null, 0, 1, SubmodelElementCollection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(propertyEClass, Property.class, "Property", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(referenceElementEClass, ReferenceElement.class, "ReferenceElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReferenceElement_Value(), this.getReference(), null, "value", null, 0, 1, ReferenceElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(eventEClass, Event.class, "Event", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEvent_Base_Property(), theUMLPackage.getProperty(), null, "base_Property", null, 0, 1, Event.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(basicEventEClass, BasicEvent.class, "BasicEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBasicEvent_Observed(), theUMLPackage.getElement(), null, "observed", null, 1, 1, BasicEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(rangeEClass, Range.class, "Range", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRange_Min(), theTypesPackage.getInteger(), "min", null, 0, 1, Range.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRange_Max(), theTypesPackage.getInteger(), "max", null, 0, 1, Range.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(capabilityEClass, Capability.class, "Capability", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCapability_Base_Property(), theUMLPackage.getProperty(), null, "base_Property", null, 0, 1, Capability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(conceptDescriptionEClass, ConceptDescription.class, "ConceptDescription", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConceptDescription_IsCaseOf(), this.getReference(), null, "isCaseOf", null, 0, -1, ConceptDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(langStringSetEClass, LangStringSet.class, "LangStringSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dataSpecificationContentEClass, DataSpecificationContent.class, "DataSpecificationContent", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dataSpecificationIEC61360EClass, DataSpecificationIEC61360.class, "DataSpecificationIEC61360", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataSpecificationIEC61360_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 0, 1, DataSpecificationIEC61360.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getDataSpecificationIEC61360_PreferredName(), this.getLangString(), null, "preferredName", null, 1, -1, DataSpecificationIEC61360.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getDataSpecificationIEC61360_ShortName(), this.getLangString(), null, "shortName", null, 0, -1, DataSpecificationIEC61360.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getDataSpecificationIEC61360_Unit(), theTypesPackage.getString(), "unit", null, 0, 1, DataSpecificationIEC61360.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getDataSpecificationIEC61360_UnitId(), this.getReference(), null, "unitId", null, 0, 1, DataSpecificationIEC61360.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getDataSpecificationIEC61360_SourceOfDefinition(), theTypesPackage.getString(), "sourceOfDefinition", null, 0, 1, DataSpecificationIEC61360.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getDataSpecificationIEC61360_Symbol(), theTypesPackage.getString(), "symbol", null, 0, 1, DataSpecificationIEC61360.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getDataSpecificationIEC61360_DataType(), this.getDataSpecificationIEC61360(), null, "dataType", null, 0, 1, DataSpecificationIEC61360.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getDataSpecificationIEC61360_Definition(), this.getLangString(), null, "definition", null, 0, -1, DataSpecificationIEC61360.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getDataSpecificationIEC61360_ValueFormat(), theTypesPackage.getString(), "valueFormat", null, 0, 1, DataSpecificationIEC61360.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getDataSpecificationIEC61360_ValueList(), this.getValueReferencePairType(), null, "valueList", null, 0, -1, DataSpecificationIEC61360.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getDataSpecificationIEC61360_Value(), theUMLPackage.getValueSpecification(), null, "value", null, 0, 1, DataSpecificationIEC61360.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getDataSpecificationIEC61360_ValueId(), this.getReference(), null, "valueId", null, 0, 1, DataSpecificationIEC61360.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getDataSpecificationIEC61360_LevelType(), this.getLevelType(), "levelType", null, 0, -1, DataSpecificationIEC61360.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(valueReferencePairTypeEClass, ValueReferencePairType.class, "ValueReferencePairType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getValueReferencePairType_Value(), theUMLPackage.getValueSpecification(), null, "value", null, 1, 1, ValueReferencePairType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getValueReferencePairType_ValueId(), this.getReference(), null, "valueId", null, 1, 1, ValueReferencePairType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(viewEClass, View.class, "View", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getView_ContainedElement(), this.getReferable(), null, "containedElement", null, 0, -1, View.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(multiLanguagePropertyEClass, MultiLanguageProperty.class, "MultiLanguageProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMultiLanguageProperty_Value(), this.getLangString(), null, "value", null, 0, -1, MultiLanguageProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMultiLanguageProperty_ValueId(), this.getReference(), null, "valueId", null, 0, 1, MultiLanguageProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(identifierTypeEEnum, IdentifierType.class, "IdentifierType");
		addEEnumLiteral(identifierTypeEEnum, IdentifierType.CUSTOM);
		addEEnumLiteral(identifierTypeEEnum, IdentifierType.IRDI);
		addEEnumLiteral(identifierTypeEEnum, IdentifierType.IRI);

		initEEnum(langEnumEEnum, LangEnum.class, "LangEnum");
		addEEnumLiteral(langEnumEEnum, LangEnum.EN);
		addEEnumLiteral(langEnumEEnum, LangEnum.AR);
		addEEnumLiteral(langEnumEEnum, LangEnum.CS);
		addEEnumLiteral(langEnumEEnum, LangEnum.DA);
		addEEnumLiteral(langEnumEEnum, LangEnum.DE);
		addEEnumLiteral(langEnumEEnum, LangEnum.ES);
		addEEnumLiteral(langEnumEEnum, LangEnum.FR);
		addEEnumLiteral(langEnumEEnum, LangEnum.HI);
		addEEnumLiteral(langEnumEEnum, LangEnum.ID);
		addEEnumLiteral(langEnumEEnum, LangEnum.IT);
		addEEnumLiteral(langEnumEEnum, LangEnum.JA);
		addEEnumLiteral(langEnumEEnum, LangEnum.KO);
		addEEnumLiteral(langEnumEEnum, LangEnum.ML);
		addEEnumLiteral(langEnumEEnum, LangEnum.NL);
		addEEnumLiteral(langEnumEEnum, LangEnum.NO);
		addEEnumLiteral(langEnumEEnum, LangEnum.PL);
		addEEnumLiteral(langEnumEEnum, LangEnum.PT);
		addEEnumLiteral(langEnumEEnum, LangEnum.RO);
		addEEnumLiteral(langEnumEEnum, LangEnum.RU);
		addEEnumLiteral(langEnumEEnum, LangEnum.SR);
		addEEnumLiteral(langEnumEEnum, LangEnum.SV);
		addEEnumLiteral(langEnumEEnum, LangEnum.TH);
		addEEnumLiteral(langEnumEEnum, LangEnum.TR);
		addEEnumLiteral(langEnumEEnum, LangEnum.ZH);

		initEEnum(keyElementsEEnum, KeyElements.class, "KeyElements");
		addEEnumLiteral(keyElementsEEnum, KeyElements.GLOBAL_REFERENCE);
		addEEnumLiteral(keyElementsEEnum, KeyElements.FRAGMENT_REFERENCE);
		addEEnumLiteral(keyElementsEEnum, KeyElements.ACCESS_PERMISSION_RULE);
		addEEnumLiteral(keyElementsEEnum, KeyElements.ANNOTATED_RELATIONSHIP_ELEMENT);
		addEEnumLiteral(keyElementsEEnum, KeyElements.BASIC_EVENT);
		addEEnumLiteral(keyElementsEEnum, KeyElements.BLOB);
		addEEnumLiteral(keyElementsEEnum, KeyElements.CAPABILITY);
		addEEnumLiteral(keyElementsEEnum, KeyElements.CONCEPT_DICTIONARY);
		addEEnumLiteral(keyElementsEEnum, KeyElements.DATA_ELEMENT);
		addEEnumLiteral(keyElementsEEnum, KeyElements.FILE);
		addEEnumLiteral(keyElementsEEnum, KeyElements.ENTITY);
		addEEnumLiteral(keyElementsEEnum, KeyElements.EVENT);
		addEEnumLiteral(keyElementsEEnum, KeyElements.MULTI_LANGUAGE_PROPERTY);
		addEEnumLiteral(keyElementsEEnum, KeyElements.OPERATION);
		addEEnumLiteral(keyElementsEEnum, KeyElements.PROPERTY);
		addEEnumLiteral(keyElementsEEnum, KeyElements.RANGE);
		addEEnumLiteral(keyElementsEEnum, KeyElements.REFERENCE_ELEMENT);
		addEEnumLiteral(keyElementsEEnum, KeyElements.RELATIONSHIP_ELEMENT);
		addEEnumLiteral(keyElementsEEnum, KeyElements.SUBMODEL_ELEMENT);
		addEEnumLiteral(keyElementsEEnum, KeyElements.SUBMODEL_ELEMENT_COLLECTION);
		addEEnumLiteral(keyElementsEEnum, KeyElements.VIEW);
		addEEnumLiteral(keyElementsEEnum, KeyElements.CONCEPT_DESCRIPTION);
		addEEnumLiteral(keyElementsEEnum, KeyElements.ASSET);
		addEEnumLiteral(keyElementsEEnum, KeyElements.ASSET_ADMINISTRATION_SHELL);
		addEEnumLiteral(keyElementsEEnum, KeyElements.SUBMODEL);

		initEEnum(referableElementsEEnum, ReferableElements.class, "ReferableElements");
		addEEnumLiteral(referableElementsEEnum, ReferableElements.ACCESS_PERMISSION_RULE);
		addEEnumLiteral(referableElementsEEnum, ReferableElements.ANNOTATED_RELATIONSHIP_ELEMENT);
		addEEnumLiteral(referableElementsEEnum, ReferableElements.BASIC_EVENT);
		addEEnumLiteral(referableElementsEEnum, ReferableElements.BLOB);
		addEEnumLiteral(referableElementsEEnum, ReferableElements.CAPABILITY);
		addEEnumLiteral(referableElementsEEnum, ReferableElements.CONCEPT_DICTIONARY);
		addEEnumLiteral(referableElementsEEnum, ReferableElements.DATA_ELEMENT);
		addEEnumLiteral(referableElementsEEnum, ReferableElements.FILE);
		addEEnumLiteral(referableElementsEEnum, ReferableElements.ENTITY);
		addEEnumLiteral(referableElementsEEnum, ReferableElements.EVENT);
		addEEnumLiteral(referableElementsEEnum, ReferableElements.MULTI_LANGUAGE_PROPERTY);
		addEEnumLiteral(referableElementsEEnum, ReferableElements.OPERATION);
		addEEnumLiteral(referableElementsEEnum, ReferableElements.PROPERTY);
		addEEnumLiteral(referableElementsEEnum, ReferableElements.RANGE);
		addEEnumLiteral(referableElementsEEnum, ReferableElements.REFERENCE_ELEMENT);
		addEEnumLiteral(referableElementsEEnum, ReferableElements.RELATIONSHIP_ELEMENT);
		addEEnumLiteral(referableElementsEEnum, ReferableElements.SUBMODEL_ELEMENT);
		addEEnumLiteral(referableElementsEEnum, ReferableElements.SUBMODEL_ELEMENT_COLLECTION);
		addEEnumLiteral(referableElementsEEnum, ReferableElements.VIEW);
		addEEnumLiteral(referableElementsEEnum, ReferableElements.CONCEPT_DESCRIPTION);

		initEEnum(identifiableElementEEnum, IdentifiableElement.class, "IdentifiableElement");
		addEEnumLiteral(identifiableElementEEnum, IdentifiableElement.ASSET);
		addEEnumLiteral(identifiableElementEEnum, IdentifiableElement.ASSET_ADMINISTRATION_SHELL);
		addEEnumLiteral(identifiableElementEEnum, IdentifiableElement.CONCEPT_DESCRIPTION);
		addEEnumLiteral(identifiableElementEEnum, IdentifiableElement.SUBMODEL);

		initEEnum(keyTypeEEnum, KeyType.class, "KeyType");
		addEEnumLiteral(keyTypeEEnum, KeyType.CUSTOM);
		addEEnumLiteral(keyTypeEEnum, KeyType.IRDI);
		addEEnumLiteral(keyTypeEEnum, KeyType.IRI);
		addEEnumLiteral(keyTypeEEnum, KeyType.ID_SHORT);
		addEEnumLiteral(keyTypeEEnum, KeyType.FRAGMENT_ID);

		initEEnum(localKeyTypeEEnum, LocalKeyType.class, "LocalKeyType");
		addEEnumLiteral(localKeyTypeEEnum, LocalKeyType.ID_SHORT);
		addEEnumLiteral(localKeyTypeEEnum, LocalKeyType.FRAGMENT_ID);

		initEEnum(modelingKindEEnum, ModelingKind.class, "ModelingKind");
		addEEnumLiteral(modelingKindEEnum, ModelingKind.TEMPLATE);
		addEEnumLiteral(modelingKindEEnum, ModelingKind.INSTANCE);

		initEEnum(protocolKindEEnum, ProtocolKind.class, "ProtocolKind");
		addEEnumLiteral(protocolKindEEnum, ProtocolKind.HTTP);
		addEEnumLiteral(protocolKindEEnum, ProtocolKind.MQTT);
		addEEnumLiteral(protocolKindEEnum, ProtocolKind.OPCUA);
		addEEnumLiteral(protocolKindEEnum, ProtocolKind.CO_AP);
		addEEnumLiteral(protocolKindEEnum, ProtocolKind.ROS);
		addEEnumLiteral(protocolKindEEnum, ProtocolKind.WEB_SOCKET);
		addEEnumLiteral(protocolKindEEnum, ProtocolKind.OTHER);

		initEEnum(idTypeEEnum, IdType.class, "IdType");
		addEEnumLiteral(idTypeEEnum, IdType.STRING);
		addEEnumLiteral(idTypeEEnum, IdType.INTEGER);
		addEEnumLiteral(idTypeEEnum, IdType.LONG);
		addEEnumLiteral(idTypeEEnum, IdType.BYTE_ARRAY);
		addEEnumLiteral(idTypeEEnum, IdType.UUID);

		initEEnum(assetKindEEnum, AssetKind.class, "AssetKind");
		addEEnumLiteral(assetKindEEnum, AssetKind.TYPE);
		addEEnumLiteral(assetKindEEnum, AssetKind.INSTANCE);

		initEEnum(mimeTypeEEnum, MimeType.class, "MimeType");
		addEEnumLiteral(mimeTypeEEnum, MimeType.APPLICATIONJSON);
		addEEnumLiteral(mimeTypeEEnum, MimeType.APPLICATIONXLS);
		addEEnumLiteral(mimeTypeEEnum, MimeType.APPLICATIONPDF);
		addEEnumLiteral(mimeTypeEEnum, MimeType.APPLICATIONZIP);
		addEEnumLiteral(mimeTypeEEnum, MimeType.APPLICATIONXML);
		addEEnumLiteral(mimeTypeEEnum, MimeType.APPLICATIONIGES);
		addEEnumLiteral(mimeTypeEEnum, MimeType.APPLICATIONSTEP);
		addEEnumLiteral(mimeTypeEEnum, MimeType.IMAGEPNG);
		addEEnumLiteral(mimeTypeEEnum, MimeType.IMAGEBMP);
		addEEnumLiteral(mimeTypeEEnum, MimeType.IMAGEJPEG);
		addEEnumLiteral(mimeTypeEEnum, MimeType.IMAGEGIF);
		addEEnumLiteral(mimeTypeEEnum, MimeType.TEXTXML);
		addEEnumLiteral(mimeTypeEEnum, MimeType.TEXTPLAIN);
		addEEnumLiteral(mimeTypeEEnum, MimeType.TEXTHTML);
		addEEnumLiteral(mimeTypeEEnum, MimeType.OTHER);

		initEEnum(securityKindEEnum, SecurityKind.class, "SecurityKind");
		addEEnumLiteral(securityKindEEnum, SecurityKind.NONE);
		addEEnumLiteral(securityKindEEnum, SecurityKind.HTTPS);

		initEEnum(entityTypeEEnum, EntityType.class, "EntityType");
		addEEnumLiteral(entityTypeEEnum, EntityType.CO_MANAGED_ENTITY);
		addEEnumLiteral(entityTypeEEnum, EntityType.SELF_MANAGED_ENTITY);

		initEEnum(levelTypeEEnum, LevelType.class, "LevelType");
		addEEnumLiteral(levelTypeEEnum, LevelType.MIN);
		addEEnumLiteral(levelTypeEEnum, LevelType.MAX);
		addEEnumLiteral(levelTypeEEnum, LevelType.NOM);
		addEEnumLiteral(levelTypeEEnum, LevelType.TYP);

		initEEnum(dataTypeIEC61360EEnum, DataTypeIEC61360.class, "DataTypeIEC61360");
		addEEnumLiteral(dataTypeIEC61360EEnum, DataTypeIEC61360.__);
		addEEnumLiteral(dataTypeIEC61360EEnum, DataTypeIEC61360.DATE);
		addEEnumLiteral(dataTypeIEC61360EEnum, DataTypeIEC61360.STRING);
		addEEnumLiteral(dataTypeIEC61360EEnum, DataTypeIEC61360.STRING_TRANSLATABLE);
		addEEnumLiteral(dataTypeIEC61360EEnum, DataTypeIEC61360.INTEGER_MEASURE);
		addEEnumLiteral(dataTypeIEC61360EEnum, DataTypeIEC61360.INTEGER_COUNT);
		addEEnumLiteral(dataTypeIEC61360EEnum, DataTypeIEC61360.INTEGER_CURRENCY);
		addEEnumLiteral(dataTypeIEC61360EEnum, DataTypeIEC61360.REAL_MEASURE);
		addEEnumLiteral(dataTypeIEC61360EEnum, DataTypeIEC61360.REAL_COUNT);
		addEEnumLiteral(dataTypeIEC61360EEnum, DataTypeIEC61360.REAL_CURRENCY);
		addEEnumLiteral(dataTypeIEC61360EEnum, DataTypeIEC61360.BOOLEAN);
		addEEnumLiteral(dataTypeIEC61360EEnum, DataTypeIEC61360.URL);
		addEEnumLiteral(dataTypeIEC61360EEnum, DataTypeIEC61360.RATIONAL);
		addEEnumLiteral(dataTypeIEC61360EEnum, DataTypeIEC61360.RATIONAL_MEASURE);
		addEEnumLiteral(dataTypeIEC61360EEnum, DataTypeIEC61360.TIME);
		addEEnumLiteral(dataTypeIEC61360EEnum, DataTypeIEC61360.TIMESTAMP);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/uml2/2.0.0/UML
		createUMLAnnotations();
		// http:///org/eclipse/emf/ecore/util/ExtendedMetaData
		createExtendedMetaDataAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/uml2/2.0.0/UML</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createUMLAnnotations() {
		String source = "http://www.eclipse.org/uml2/2.0.0/UML";
		addAnnotation
		  (this,
		   source,
		   new String[] {
			   "originalName", "AAS"
		   });
		addAnnotation
		  (getHasKind_Base_HasKind_Class(),
		   source,
		   new String[] {
			   "originalName", "base_Class"
		   });
		addAnnotation
		  (getHasSemantics_Base_HasSemantics_Class(),
		   source,
		   new String[] {
			   "originalName", "base_Class"
		   });
		addAnnotation
		  (mimeTypeEEnum.getELiterals().get(0),
		   source,
		   new String[] {
			   "originalName", "application-json"
		   });
		addAnnotation
		  (mimeTypeEEnum.getELiterals().get(1),
		   source,
		   new String[] {
			   "originalName", "application-xls"
		   });
		addAnnotation
		  (mimeTypeEEnum.getELiterals().get(2),
		   source,
		   new String[] {
			   "originalName", "application-pdf"
		   });
		addAnnotation
		  (mimeTypeEEnum.getELiterals().get(3),
		   source,
		   new String[] {
			   "originalName", "application-zip"
		   });
		addAnnotation
		  (mimeTypeEEnum.getELiterals().get(4),
		   source,
		   new String[] {
			   "originalName", "application-xml"
		   });
		addAnnotation
		  (mimeTypeEEnum.getELiterals().get(5),
		   source,
		   new String[] {
			   "originalName", "application-iges"
		   });
		addAnnotation
		  (mimeTypeEEnum.getELiterals().get(6),
		   source,
		   new String[] {
			   "originalName", "application-step"
		   });
		addAnnotation
		  (mimeTypeEEnum.getELiterals().get(7),
		   source,
		   new String[] {
			   "originalName", "image-png"
		   });
		addAnnotation
		  (mimeTypeEEnum.getELiterals().get(8),
		   source,
		   new String[] {
			   "originalName", "image-bmp"
		   });
		addAnnotation
		  (mimeTypeEEnum.getELiterals().get(9),
		   source,
		   new String[] {
			   "originalName", "image-jpeg"
		   });
		addAnnotation
		  (mimeTypeEEnum.getELiterals().get(10),
		   source,
		   new String[] {
			   "originalName", "image-gif"
		   });
		addAnnotation
		  (mimeTypeEEnum.getELiterals().get(11),
		   source,
		   new String[] {
			   "originalName", "text-xml"
		   });
		addAnnotation
		  (mimeTypeEEnum.getELiterals().get(12),
		   source,
		   new String[] {
			   "originalName", "text-plain"
		   });
		addAnnotation
		  (mimeTypeEEnum.getELiterals().get(13),
		   source,
		   new String[] {
			   "originalName", "text-html"
		   });
		addAnnotation
		  (dataTypeIEC61360EEnum.getELiterals().get(0),
		   source,
		   new String[] {
			   "originalName", ""
		   });
	}

	/**
	 * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createExtendedMetaDataAnnotations() {
		String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";
		addAnnotation
		  (keyElementsEEnum,
		   source,
		   new String[] {
			   "baseType", "ReferableElements"
		   });
		addAnnotation
		  (referableElementsEEnum,
		   source,
		   new String[] {
			   "baseType", "IdentifiableElement"
		   });
		addAnnotation
		  (keyTypeEEnum,
		   source,
		   new String[] {
			   "baseType", "IdentifierType"
		   });
	}

} //AASPackageImpl
