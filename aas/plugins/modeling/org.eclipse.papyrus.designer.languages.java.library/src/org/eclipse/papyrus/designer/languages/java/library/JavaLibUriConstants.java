/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher
 *
 *****************************************************************************/
package org.eclipse.papyrus.designer.languages.java.library;

import org.eclipse.emf.common.util.URI;

/**
 * Utility class to get informations on JAVA library resources
 */
public final class JavaLibUriConstants {

	public static final String LIBRARY_PATHMAP = "pathmap://PapyrusJava_LIBRARIES/"; //$NON-NLS-1$

	public static final String LIBRARY_PATH = LIBRARY_PATHMAP + "JavaLibrary.uml"; //$NON-NLS-1$

	public static final URI LIBRARY_PATH_URI = URI.createURI(LIBRARY_PATH);
}
