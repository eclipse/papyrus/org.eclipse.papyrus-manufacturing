/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.aas.validation.constraints;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.aas.AssetAdministrationShell;
import org.eclipse.papyrus.aas.Submodel;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.util.UMLUtil;

public class AasContainsOnlySubmodels extends AbstractModelConstraint {

	@Override
	public IStatus validate(IValidationContext ctx) {
		
		IStatus status = ctx.createSuccessStatus();
		if(ctx.getTarget() instanceof NamedElement) {
			NamedElement elt = (NamedElement)ctx.getTarget();
			AssetAdministrationShell aas = UMLUtil.getStereotypeApplication(elt, AssetAdministrationShell.class);
			if (aas != null && elt instanceof org.eclipse.uml2.uml.Class) {
				if(! ((org.eclipse.uml2.uml.Class)elt).getAttributes().isEmpty()) {
					status = ctx.createFailureStatus("an AAS must contain only submodels");
				}
				if(! ((org.eclipse.uml2.uml.Class)elt).getOperations().isEmpty() ) {
					status = ctx.createFailureStatus("an AAS must contain only submodels");
				}
				
				for(Classifier subClass: ((org.eclipse.uml2.uml.Class)elt).getNestedClassifiers()){
					Submodel submodel=UMLUtil.getStereotypeApplication(subClass, Submodel.class);
					if(submodel==null) {
						status = ctx.createFailureStatus("an AAS must contain only submodels");
					}
				}
				
			}
		}
		
		
		// TODO Auto-generated method stub
		return status;
	}

}
