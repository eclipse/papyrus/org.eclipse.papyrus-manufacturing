/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.aas.validation.constraints;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.aas.Submodel;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;

public class SubmodelContainsOnlySubmodelElements extends AbstractModelConstraint {

	@Override
	public IStatus validate(IValidationContext ctx) {
		
		IStatus status = ctx.createSuccessStatus();
		if(ctx.getTarget() instanceof NamedElement) {
			NamedElement elt = (NamedElement)ctx.getTarget();
			Submodel submodel = UMLUtil.getStereotypeApplication(elt, Submodel.class);
			if (submodel != null && elt instanceof org.eclipse.uml2.uml.Property) {
				//verifying that the property has a type
				
				
					
					if (org.eclipse.papyrus.uml.tools.utils.UMLUtil.getAppliedStereotype(elt, "AAS::Property", false) != null) {
						
						
						status = ctx.createFailureStatus("a Submodel must contain only submodelElements");
						

					}
				
				

				
				
				
			
				
				
				
			}
		}
		
		
		// TODO Auto-generated method stub
		return status;
	}

}
