/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.aas.validation.constraints;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.aas.Identifiable;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.util.UMLUtil;

public class IdentifiableMustHaveIdentifier extends AbstractModelConstraint {

	@Override
	public IStatus validate(IValidationContext ctx) {
		
		IStatus status = ctx.createSuccessStatus();
		if(ctx.getTarget() instanceof NamedElement) {
			NamedElement elt = (NamedElement)ctx.getTarget();
			if(org.eclipse.papyrus.uml.tools.utils.UMLUtil.getAppliedStereotype(elt, "AAS::Identifiable", false) != null && !(elt instanceof Package)) {
				Identifiable identifiable = UMLUtil.getStereotypeApplication(elt, org.eclipse.papyrus.aas.Identifiable.class);
				if(identifiable.getIdentification()==null) {
					status = ctx.createFailureStatus("an Identifiable must have a the identification set");
				}else
					if(identifiable.getIdentification().getId().isEmpty()) {
						status = ctx.createFailureStatus("an Identifiable must have a the identification (id attribute) set");
					}
					else
						if(identifiable.getIdentification().getIdType()==null) {
							status = ctx.createFailureStatus("an Identifiable must have a the identification (idType attribute) set");
						}
				
			}
			
		}
			
		
		
		
		return status;
	}

}
