/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.aas.validation.constraints;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.aas.IdentifierType;
import org.eclipse.papyrus.aas.ModelingKind;
import org.eclipse.papyrus.aas.Submodel;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.util.UMLUtil;

public class SubmodelIdentificationConstraint extends AbstractModelConstraint {

	@Override
	public IStatus validate(IValidationContext ctx) {
		
		IStatus status = ctx.createSuccessStatus();
		if(ctx.getTarget() instanceof NamedElement) {
			NamedElement elt = (NamedElement)ctx.getTarget();
			Submodel submodel = UMLUtil.getStereotypeApplication(elt, Submodel.class);
			if (submodel != null && elt instanceof org.eclipse.uml2.uml.Class) {
				//Submodel of ModelingKind - TEMPLATE does not support CUSTOM as IdentifierType according to the Details of the Asset Administration Shell v3RC (Page 36).
					
				//verify that the submodel if of kinf Template
				
					if(submodel.getKind().getLiteral().equals(ModelingKind.TEMPLATE.getLiteral())) {
						if(submodel.getIdentification()!=null && submodel.getIdentification().getIdType()!=null) {
							//verify that the identification is not of type CUSTOM
							if (submodel.getIdentification().getIdType().getLiteral().equals(IdentifierType.CUSTOM.getLiteral()) ) {
								status = ctx.createFailureStatus("Submodel of ModelingKind - TEMPLATE does not support CUSTOM as IdentifierType");
								
							}
							
					}
					
				}
				
			}
		}
		
		
		// TODO Auto-generated method stub
		return status;
	}

}
