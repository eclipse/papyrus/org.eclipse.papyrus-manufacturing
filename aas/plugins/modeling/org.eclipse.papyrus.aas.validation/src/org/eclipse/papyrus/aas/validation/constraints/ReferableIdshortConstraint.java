/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.aas.validation.constraints;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.aas.Referable;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.util.UMLUtil;

public class ReferableIdshortConstraint extends AbstractModelConstraint {

	@Override
	public IStatus validate(IValidationContext ctx) {
		
		IStatus status = ctx.createSuccessStatus();
		if(ctx.getTarget() instanceof NamedElement) {
			NamedElement elt = (NamedElement)ctx.getTarget();
			if(org.eclipse.papyrus.uml.tools.utils.UMLUtil.getAppliedStereotype(elt, "AAS::Referable", false) != null && !(elt instanceof Package)) {
				Referable referable = UMLUtil.getStereotypeApplication(elt, org.eclipse.papyrus.aas.Referable.class);
				if(referable.getIdShort()==null) {
					status = ctx.createFailureStatus("a Referable must have an idshort");
				}else
					if(referable.getIdShort().isEmpty()) {
						status = ctx.createFailureStatus("a Referable must have an idshort");
					}
					else
						if(!referable.getIdShort().matches("[a-zA-Z]+\\w*")) {
							status = ctx.createFailureStatus("idShort of Referables shall only feature letters, digits, underscore (\"_\"); starting mandatory with a letter. I.e. [a-zA-Z][a-zA-Z0-9_]+");
						}
				
			}
			
		}
			
		
		
		
		return status;
	}

}
