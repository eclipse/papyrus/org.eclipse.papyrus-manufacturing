/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.aas.validation.constraints;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.aas.NodeId;
import org.eclipse.papyrus.aas.Property;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.util.UMLUtil;

public class PropertyEndpointConstraint extends AbstractModelConstraint {

	@Override
	public IStatus validate(IValidationContext ctx) {
		
		IStatus status = ctx.createSuccessStatus();
		if(ctx.getTarget() instanceof NamedElement) {
			NamedElement elt = (NamedElement)ctx.getTarget();
			Property pr = UMLUtil.getStereotypeApplication(elt, Property.class);
			if (pr != null && elt instanceof org.eclipse.uml2.uml.Property) {
				
				NodeId umlNodeid =pr.getNodeId();
				
				if(umlNodeid != null) {
					
					if (pr.getEndPoint() == null ) {
						status = ctx.createFailureStatus("Endpoint must be defined");
					}
					
					
					}
				
				
				}

				
			}
		
		
		
		return status;
	}

	
	
	


}
