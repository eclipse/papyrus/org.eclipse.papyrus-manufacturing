/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.aas.validation.constraints;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.aas.AASEndpoint;
import org.eclipse.papyrus.aas.Asset;
import org.eclipse.papyrus.aas.AssetAdministrationShell;
import org.eclipse.papyrus.aas.Endpoint;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.util.UMLUtil;

public class AssetEndpointAddressConstraint extends AbstractModelConstraint {

	private final org.apache.commons.validator.routines.UrlValidator urlValidator = new org.apache.commons.validator.routines.UrlValidator(
			org.apache.commons.validator.routines.UrlValidator.ALLOW_LOCAL_URLS
					+ org.apache.commons.validator.routines.UrlValidator.ALLOW_ALL_SCHEMES
					+ org.apache.commons.validator.routines.UrlValidator.ALLOW_2_SLASHES);

	private final org.apache.commons.validator.routines.DomainValidator domainValidator = org.apache.commons.validator.routines.DomainValidator
			.getInstance(true);
	private final org.apache.commons.validator.routines.InetAddressValidator inetAddressValidator = org.apache.commons.validator.routines.InetAddressValidator
			.getInstance();

	@Override
	public IStatus validate(IValidationContext ctx) {
		// TODO Auto-generated method stub
		IStatus status = ctx.createSuccessStatus();
		if (ctx.getTarget() instanceof NamedElement) {
			NamedElement elt = (NamedElement) ctx.getTarget();
			Asset asset = UMLUtil.getStereotypeApplication(elt, Asset.class);
			AssetAdministrationShell aas = UMLUtil.getStereotypeApplication(elt, AssetAdministrationShell.class);
			if ((asset != null && elt instanceof org.eclipse.uml2.uml.Class)) {
				if (asset.getEndpoint() != null && !asset.getEndpoint().isEmpty()) {
					for (Endpoint ep : asset.getEndpoint()) {
						if (ep.getAddress() != null && !ep.getAddress().isEmpty()) {

							if (!urlValidator.isValid((String) ep.getAddress())) {
								return ctx.createFailureStatus("The URL is not valid");
							}
						}
					}
				}

			}
			if ((aas != null && elt instanceof org.eclipse.uml2.uml.Class)) {
				if (aas.getEndpoint() != null) {
					AASEndpoint ep = aas.getEndpoint();
					String address = ep.getAddress();
					
					if (address != null && !address.isEmpty()) {
						if (!inetAddressValidator.isValid(address) && !domainValidator.isValid(address)) {
							return ctx.createFailureStatus("The URL is not valid");
						}
					}
				}

			}
		}
		return status;
	}

}
