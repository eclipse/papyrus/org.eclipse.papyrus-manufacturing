/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.aas.validation.constraints;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.aas.AssetAdministrationShell;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.util.UMLUtil;

public class AasMustReferenceAnAsset extends AbstractModelConstraint {

	@Override
	public IStatus validate(IValidationContext ctx) {
		
		IStatus status = ctx.createSuccessStatus();
		if(ctx.getTarget() instanceof NamedElement) {
			NamedElement elt = (NamedElement)ctx.getTarget();
			AssetAdministrationShell aas = UMLUtil.getStereotypeApplication(elt, AssetAdministrationShell.class);
			if (aas != null && elt instanceof org.eclipse.uml2.uml.Class) {
				
				if(aas.getAsset()==null ) {
					status = ctx.createFailureStatus("An AAS must reference one asset");
					
				}
				
				
			}
		}
		
		
		
		return status;
	}

}
