/*****************************************************************************
 * Copyright (c) 2021 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.aas.ui.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.papyrus.infra.nattable.handler.AbstractTableHandler;
import org.eclipse.papyrus.infra.nattable.model.nattable.Table;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;

/**
 * @author IK264250
 *
 */
public class CreateElementHandler extends AbstractTableHandler {

	/**
	 * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 *
	 * @param event
	 * @return
	 * @throws ExecutionException
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		String elementType = event.getParameter("org.eclipse.papyrus.aas.ui.commandParameter");
		CompositeCommand cc = new CompositeCommand("Creat Entity from Table menu");
		EObject context = getTableContext();
		TransactionalEditingDomain domain = getContextEditingDomain();
		IElementType elemtypEntity = ElementTypeRegistry.getInstance().getType(elementType);


		CreateElementRequest request = new CreateElementRequest(domain, context, elemtypEntity);
		IElementEditService provider = ElementEditServiceUtils.getCommandProvider(context);
		ICommand command = provider.getEditCommand(request);

		cc.add(command);


		domain.getCommandStack().execute(GMFtoEMFCommandWrapper.wrap(cc));
		return null;
	}



	protected EObject getTableContext() {
		final Table table = getTableSelectionWrapper().getNatTableModelManager().getTable();
		return table.getOwner();
	}

}
