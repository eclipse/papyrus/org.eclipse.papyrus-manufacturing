/*****************************************************************************
 * Copyright (c) 2016 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Fanch BONNABESSE (ALL4TEC) fanch.bonnabesse@all4tec.net - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.aas.ui.widgets;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.UnexecutableCommand;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.databinding.EObjectObservableValue;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.papyrus.infra.emf.utils.EMFHelper;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.papyrus.infra.ui.emf.databinding.EMFObservableValue;

/**
 * Structured ObservableValue.
 *
 * @since 2.0
 */
public class AASEObjectStructuredObservableValue extends EMFObservableValue {

	/**
	 * List of child of the ObservableValue.
	 */
	protected List<EObjectObservableValue> observables = new ArrayList<>();

	/**
	 * The parent of this EObjectStructuredObservableValue.
	 */
	protected AASEObjectStructuredObservableValue parent;

	private Function<? super ICommand, ? extends Command> commandWrapper;

	/**
	 * Constructor.
	 *
	 * @param eObject
	 *            The EObject to edit.
	 * @param eStructuralFeature
	 *            The structural feature.
	 * @param domain
	 *            The current editing domain.
	 * @param browseFeatures
	 *            The features for the browse.
	 */
	public AASEObjectStructuredObservableValue(final EObject eObject, final EStructuralFeature eStructuralFeature, final EditingDomain domain, final boolean browseFeatures, final AASEObjectStructuredObservableValue parent) {

		super(eObject, eStructuralFeature, domain);
		this.parent = parent;
		if (null != eObject) {
			if (browseFeatures) {
				browseFeatures(eObject);
			}
		}
		this.commandWrapper = GMFtoEMFCommandWrapper::wrap;
	}


	/**
	 * Get the child.
	 *
	 * @return The observables.
	 */
	public List<EObjectObservableValue> getObservables() {
		return observables;
	}

	/**
	 * Get the parent.
	 *
	 * @return The parent.
	 */
	public AASEObjectStructuredObservableValue getParent() {
		return parent;
	}



	/**
	 * Crete the child.
	 *
	 * @param eObject
	 *            The parent.
	 */
	private void browseFeatures(final EObject eObject) {
		EList<EStructuralFeature> eStructuralFeatures = eObject.eClass().getEStructuralFeatures();
		TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(eObject);
		for (EStructuralFeature eStructuralFeature : eStructuralFeatures) {
			EObjectObservableValue eObjectObservableValue;
			Object eGet = eObject.eGet(eStructuralFeature);
			if (eStructuralFeature instanceof EReference && eGet instanceof EObject) {
				eObjectObservableValue = new AASEObjectStructuredObservableValue((EObject) eGet, eStructuralFeature, editingDomain, true, this);
			} else {
				eObjectObservableValue = new AASEObjectStructuredObservableValue(eObject, eStructuralFeature, editingDomain, false, this);
			}
			observables.add(eObjectObservableValue);
		}
	}

	@Override
	public Command getSetCommand(Object value) {
		EObject eObjectValue = EMFHelper.getEObject(value);
		if (eObjectValue != null) {
			value = eObjectValue;
		}


		try {
			IElementEditService provider = ElementEditServiceUtils.getCommandProvider((EObject) getObserved());

			if (provider != null) {
				CompositeCommand cc = new CompositeCommand("Edit value"); //$NON-NLS-1$

				if (value instanceof EEnumLiteral) {
					value = ((EEnumLiteral) value).getInstance();
				}
				IEditCommandRequest createSetRequest = createSetRequest((TransactionalEditingDomain) domain, eObject, eStructuralFeature, value);


				if (createSetRequest == null) {
					return UnexecutableCommand.INSTANCE;
				}
				ICommand command = provider.getEditCommand(createSetRequest);

				if (command.canExecute()) {
					cc.add(provider.getEditCommand(createSetRequest));
				}
				return commandWrapper.apply(cc);
			}
		} catch (Exception ex) {

		}

		return UnexecutableCommand.INSTANCE;
	}


	protected IEditCommandRequest createSetRequest(TransactionalEditingDomain domain, EObject owner, EStructuralFeature feature, Object value) {
		return new SetRequest(domain, owner, feature, value);
	}


	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.databinding.EObjectObservableValue#getValueType()
	 */
	@Override
	public Object getValueType() {
		Object valueType = super.getValueType();
		if (null == valueType && null != eObject) {
			valueType = eObject.eClass();
		}
		return valueType;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.eclipse.emf.databinding.EObjectObservableValue#dispose()
	 */
	@Override
	public synchronized void dispose() {
		for (EObjectObservableValue eObjectObservableValue : observables) {
			eObjectObservableValue.dispose();
		}
		super.dispose();
	}
}
