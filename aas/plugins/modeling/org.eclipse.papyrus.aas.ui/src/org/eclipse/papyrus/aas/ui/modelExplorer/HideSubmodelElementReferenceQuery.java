package org.eclipse.papyrus.aas.ui.modelExplorer;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.papyrus.emf.facet.efacet.core.IFacetManager;
import org.eclipse.papyrus.emf.facet.efacet.core.exception.DerivedTypedElementException;
import org.eclipse.papyrus.emf.facet.query.java.core.IJavaQuery2;
import org.eclipse.papyrus.emf.facet.query.java.core.IParameterValueList2;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.UMLPackage;

public class HideSubmodelElementReferenceQuery implements IJavaQuery2<Class, List<EReference>> {
	@Override
	public List<EReference> evaluate(final Class context,
			final IParameterValueList2 parameterValues,
			final IFacetManager facetManager)
			throws DerivedTypedElementException {
		List<EReference> eRefToShow = new ArrayList<>();
		// eRefToShow.add(UMLPackage.Literals.CLASS__OWNED_OPERATION);
		eRefToShow.add(UMLPackage.Literals.CLASS__NESTED_CLASSIFIER);
		return eRefToShow;
	}
}
