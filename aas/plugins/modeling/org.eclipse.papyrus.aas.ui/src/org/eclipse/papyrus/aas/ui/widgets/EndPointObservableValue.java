/*****************************************************************************
 * Copyright (c) 2023 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.aas.ui.widgets;

import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.core.databinding.observable.Diffs;
import org.eclipse.core.databinding.observable.IObserving;
import org.eclipse.core.databinding.observable.value.ValueDiff;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.UnexecutableCommand;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.papyrus.aas.AASFactory;
import org.eclipse.papyrus.aas.AASPackage;
import org.eclipse.papyrus.aas.Asset;
import org.eclipse.papyrus.aas.AssetAdministrationShell;
import org.eclipse.papyrus.aas.DataElement;
import org.eclipse.papyrus.aas.Endpoint;
import org.eclipse.papyrus.aas.Property;
import org.eclipse.papyrus.aas.ProtocolKind;
import org.eclipse.papyrus.aas.Submodel;
import org.eclipse.papyrus.aas.SubmodelElement;
import org.eclipse.papyrus.aas.ui.utils.AASUtils;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.papyrus.uml.tools.databinding.PapyrusObservableValue;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * @author AS247872
 *
 */
public class EndPointObservableValue extends PapyrusObservableValue implements IObserving {



	private DataElement propertyElement;

	private PartAdapter partAdapter;

	private Object value;


	public EndPointObservableValue(final EObject property, final TransactionalEditingDomain domain) {


		super(property, AASPackage.eINSTANCE.getSubmodelElement_EndPoint(), domain);

		if (property instanceof DataElement) {
			propertyElement = (DataElement) property;
		}
		((PartAdapter) getPartAdapter()).adapt(propertyElement);

		if (property instanceof Element && AASUtils.hasSemantics(property)) {

			propertyElement = UMLUtil.getStereotypeApplication((Element) property, Property.class);

			// add listeners
			((PartAdapter) getPartAdapter()).adapt(propertyElement);
		}

	}

	@Override
	public synchronized void dispose() {
		try {
			if (partAdapter != null) {
				if (partAdapter.getTarget() != null) {
					partAdapter.unadapt(partAdapter.getTarget());
				}
				partAdapter = null;
			}

			value = null;
		} finally {
			super.dispose();
		}
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	protected Object doGetValue() {

		Endpoint end = propertyElement.getEndPoint();
		Endpoint result = null;
		// try to get it from the Asset else set value to null
		if (value != null && end instanceof Endpoint) {
			org.eclipse.papyrus.aas.Submodel aasSubmodel = UMLUtil.getStereotypeApplication(getSubmodel((Element) propertyElement.getBase_Property().eContainer()), Submodel.class);
			if (getAssetfromSubmodel(aasSubmodel) != null) {
				Asset asset = getAssetfromSubmodel(aasSubmodel);
				if (asset.getEndpoint() != null && !asset.getEndpoint().isEmpty()) {
					for (Endpoint ep : asset.getEndpoint()) {
						if (ep.getAddress() != null && ep.getName() != null && ep.getProtocol() != null) {
							if (ep.getAddress().equals(end.getAddress()) && ep.getName().equals(end.getName()) && ep.getProtocol().equals(end.getProtocol())) {
								result = ep;
							}

						}

					}
				}
			}
		}
		if (result != null) {
			String valueString = end.toString();

			// for (String splitString : valueString.split("[()]")) {
			// System.out.print(splitString + " ");
			// }
			String endpointParameter = valueString.split("[()]")[1];
			String endpointwithequals = endpointParameter.replace(": ", "=");
			value = "(" + endpointwithequals + ")";
		} else {
			// no endpoint is defined for the SubmodelElement;
			value = "Please Select in the list bellow an endpoint from the Asset's endpoints";
		}
		return value;
	}

	public Asset getAssetfromSubmodel(Submodel submodel) {
		List<Element> possibleAasContainers = ((Element) propertyElement.getBase_Property()).getModel().allOwnedElements().stream()
				.filter(e -> (UMLUtil.getStereotypeApplication(e, AssetAdministrationShell.class) != null))
				.collect(Collectors.toList());
		if (possibleAasContainers != null && !possibleAasContainers.isEmpty()) {
			for (Element element : possibleAasContainers) {
				AssetAdministrationShell ass = UMLUtil.getStereotypeApplication(element, AssetAdministrationShell.class);
				if (ass != null && ass.getSubmodel().contains(submodel)) {
					return ass.getAsset();
				}
			}
		}
		return null;
	}

	@Override
	public Command getCommand(final Object value) {

		Command result = UnexecutableCommand.INSTANCE;


		if (value != null && value instanceof String) {
			// construct the EndPoint from the value
			// Endpoint endpoint = convertToEndPoint((String) value);
			if (!(value.equals("") || value.equals("Please Select in the list bellow an endpoint from the Asset's endpoints"))) {
				result = getChangEndPointCommand(propertyElement, (String) value, domain);
			} else {
				result = getUnsetEndPointCommand(propertyElement);
			}
		}
		if (value instanceof Endpoint) {
			result = getChangEndPointCommand2(propertyElement, (Endpoint) value, domain);
		}
		return result;

	}


	private Endpoint getEndpoint(String endpoint) {

		if (endpoint != "") {


			Endpoint endPoint = AASFactory.eINSTANCE.createEndpoint();
			String[] propertiesValues = endpoint.substring(1, endpoint.length() - 1).split(",");
			for (int i = 0; i < propertiesValues.length; i++) {
				propertiesValues[i] = propertiesValues[i].split("=")[1];
			}

			endPoint.setAddress(propertiesValues[0]);
			endPoint.setName(propertiesValues[2]);
			endPoint.setProtocol(ProtocolKind.get(propertiesValues[1]));
			// endPoint.setPort(Integer.parseInt(propertiesValues[3]));
			return endPoint;
		}
		return null;
	}

	private String getStringfromEndpoint(Endpoint endpoint) {
		if (endpoint != null) {
			return endpoint.toString();
		}
		return "";
	}

	protected Command getUnsetEndPointCommand(SubmodelElement element) {
		ICommand result = null;


		IElementEditService provider = ElementEditServiceUtils.getCommandProvider(element);
		if (provider != null) {
			TransactionalEditingDomain ted = (TransactionalEditingDomain) domain;



			Endpoint upperValue = element.getEndPoint();
			if (upperValue != null) {
				result = provider.getEditCommand(new DestroyElementRequest(ted, upperValue, false));
			}


		}


		return (result == null) ? UnexecutableCommand.INSTANCE : GMFtoEMFCommandWrapper.wrap(result);
	}


	/**
	 * @param propertyElement2
	 * @param value2
	 * @param domain
	 * @return
	 */
	private Command getChangEndPointCommand(DataElement property, String value, EditingDomain domain) {
		ICommand result = null;

		Endpoint endPoint = AASFactory.eINSTANCE.createEndpoint();
		String[] propertiesValues = value.substring(1, value.length() - 1).split(",");
		for (int i = 0; i < propertiesValues.length; i++) {
			propertiesValues[i] = propertiesValues[i].split("=")[1];
		}
		endPoint.setAddress(propertiesValues[0]);
		endPoint.setName(propertiesValues[2]);
		endPoint.setProtocol(ProtocolKind.get(propertiesValues[1]));
		// endPoint.setPort(Integer.parseInt(propertiesValues[3]));
		IElementEditService provider = ElementEditServiceUtils.getCommandProvider(property);
		if (provider != null) {
			IEditCommandRequest request = createSetRequest((TransactionalEditingDomain) domain, property, AASPackage.eINSTANCE.getSubmodelElement_EndPoint(), endPoint);
			result = provider.getEditCommand(request);
		}

		return result == null ? UnexecutableCommand.INSTANCE : GMFtoEMFCommandWrapper.wrap(result);
	}

	private Command getChangEndPointCommand2(DataElement property, Endpoint endPoint, EditingDomain domain) {
		ICommand result = null;


		IElementEditService provider = ElementEditServiceUtils.getCommandProvider(property);
		if (provider != null) {
			IEditCommandRequest request = createSetRequest((TransactionalEditingDomain) domain, property, AASPackage.eINSTANCE.getSubmodelElement_EndPoint(), endPoint);
			result = provider.getEditCommand(request);
		}

		return result == null ? UnexecutableCommand.INSTANCE : GMFtoEMFCommandWrapper.wrap(result);
	}

	@Override
	protected IEditCommandRequest createSetRequest(TransactionalEditingDomain domain, EObject owner, EStructuralFeature feature, Object value) {
		return new SetRequest(domain, owner, feature, value);
	}

	@Override
	public Object getObserved() {
		return propertyElement;
	}

	@Override
	public Object getValueType() {
		return String.class;
	}



	/**
	 * Retrieve the listener for Aggregation
	 */
	private Adapter getPartAdapter() {
		if (partAdapter == null) {
			partAdapter = new PartAdapter();
		}
		return partAdapter;
	}

	private class PartAdapter extends MultiplicityElementAdapter {
		public PartAdapter() {
			super();
		}



		@Override
		public void handleMultiplicityChanged(Notification notification) {
			Object oldValue = value;
			Object newValue = doGetValue();

			if (!newValue.equals(oldValue)) {
				final ValueDiff<Object> diff = Diffs.createValueDiff(oldValue, newValue);
				getRealm().exec(new Runnable() {
					@Override
					public void run() {
						fireValueChange(diff);
					}
				});
			}
		}
	}

	/**
	 * @param element
	 * @return
	 */
	private static Element getSubmodel(Element element) {

		Element owner = element;
		if (isSubmodel(owner)) {
			return element;
		} else {
			while (!isSubmodel(owner)) {

				owner = owner.getOwner();
			}
		}
		return owner;
	}

	public static Boolean isSubmodel(Element c) {
		return UMLUtil.getStereotypeApplication(c, org.eclipse.papyrus.aas.Submodel.class) != null;
	}
}

