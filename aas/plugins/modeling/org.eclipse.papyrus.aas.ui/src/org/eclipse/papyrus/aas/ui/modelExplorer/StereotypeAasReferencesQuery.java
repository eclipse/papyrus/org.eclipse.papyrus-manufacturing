/*****************************************************************************
 * Copyright (c) 2021 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.aas.ui.modelExplorer;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.papyrus.aas.Asset;
import org.eclipse.papyrus.aas.AssetAdministrationShell;
import org.eclipse.papyrus.emf.facet.efacet.core.IFacetManager;
import org.eclipse.papyrus.emf.facet.efacet.core.exception.DerivedTypedElementException;
import org.eclipse.papyrus.emf.facet.query.java.core.IJavaQuery2;
import org.eclipse.papyrus.emf.facet.query.java.core.IParameterValueList2;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.util.UMLUtil;

public class StereotypeAasReferencesQuery implements IJavaQuery2<Class, List<Class>> {
	@Override
	public List<Class> evaluate(final Class context,
			final IParameterValueList2 parameterValues,
			final IFacetManager facetManager)
			throws DerivedTypedElementException {

		List<Class> result = new ArrayList<>();
		Asset appliedAsset = UMLUtil.getStereotypeApplication(context, Asset.class);
		if (appliedAsset != null) {
			// get the AAS related to this Asset
			List<Element> possibleRelatedAas = context.getModel().allOwnedElements().stream()
					.filter(Class.class::isInstance).map(Class.class::cast) // to filter the Package
					.filter(e -> (UMLUtil.getStereotypeApplication(e, AssetAdministrationShell.class) != null))
					.collect(Collectors.toList());
			if (possibleRelatedAas != null && !possibleRelatedAas.isEmpty()) {
				for (Element element : possibleRelatedAas) {
					AssetAdministrationShell aas = UMLUtil.getStereotypeApplication(element, AssetAdministrationShell.class);
					if (aas != null && aas.getAsset() != null && aas.getAsset().equals(appliedAsset)) {
						result.add(aas.getBase_Class());
					}
				}
			}

		}
		return result;
	}
}
