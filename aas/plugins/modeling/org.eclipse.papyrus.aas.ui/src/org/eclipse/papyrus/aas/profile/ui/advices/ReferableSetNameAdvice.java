/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.aas.profile.ui.advices;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.papyrus.aas.ui.utils.AASUtils;
import org.eclipse.swt.widgets.Display;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * @author AS247872
 *
 */
public class ReferableSetNameAdvice extends AbstractEditHelperAdvice {

	public ReferableSetNameAdvice() {
		super();
	}

	@Override
	public boolean approveRequest(IEditCommandRequest request) {

		if (request instanceof SetRequest) {
			SetRequest setRequest = (SetRequest) request;
			EStructuralFeature feature = setRequest.getFeature();

			if (AASUtils.isReferable(setRequest.getElementToEdit()) && UMLPackage.eINSTANCE.getNamedElement_Name().equals(feature)) {
				// new value should be a protocol or null
				Object newValue = ((SetRequest) request).getValue();
				if (((String) newValue).matches("[a-zA-Z]+\\w*")) {
					return true;
				} else {
					MessageDialog.openWarning(Display.getCurrent().getActiveShell(), "Name Edition Warning", "The name shall only feature letters, digits, underscore ('_'); starting with a small letter. I.e. [a-z][a-zA-Z0-9_]+ ");
					return false;
				}
			}

			return super.approveRequest(setRequest);

		}
		return super.approveRequest(request);
	}


}
