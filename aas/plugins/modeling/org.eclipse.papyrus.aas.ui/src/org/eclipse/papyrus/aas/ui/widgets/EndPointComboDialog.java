/*****************************************************************************
 * Copyright (c) 2023 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.aas.ui.widgets;

import java.util.Timer;
import java.util.TimerTask;

import org.eclipse.core.databinding.observable.value.AbstractObservableValue;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.ValueDiff;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ContentViewer;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.IElementComparer;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.papyrus.aas.AASFactory;
import org.eclipse.papyrus.aas.Endpoint;
import org.eclipse.papyrus.aas.ProtocolKind;
import org.eclipse.papyrus.infra.tools.databinding.AggregatedObservable;
import org.eclipse.papyrus.infra.widgets.editors.StringCombo;
import org.eclipse.papyrus.infra.widgets.providers.UnchangedObject;
import org.eclipse.papyrus.infra.widgets.providers.UnsetObject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Composite;

/**
 * @author AS247872
 *
 */
public class EndPointComboDialog extends StringCombo {



	private Timer timer;

	private TimerTask changeColorTask;
	private IBaseLabelProvider labelprovider;


	public EndPointComboDialog(Composite parent, int style) {
		super(parent, style, null, true);
		labelprovider = ((ContentViewer) this.viewer).getLabelProvider();


	}

	@Override
	protected IObservableValue getObservableValue() {
		return new CComboObservableValue();
		// return new EndPointComboObservableValue();
	}


	// public class EndPointComboObseravbleValue extends ComboObservableValue {
	//
	// /**
	// * Constructor.
	// *
	// * @param viewer
	// * @param modelProperty
	// */
	// public EndPointComboObseravbleValue(ComboViewer viewer, IObservableValue modelProperty) {
	// super(viewer, modelProperty);
	// viewer.setComparator(new EndPointComparator());
	// // TODO Auto-generated constructor stub
	// }
	//
	//
	//
	// }

	/**
	 * Comparator for the viewer. Sorts the templates by name and then
	 * description and context types by names.
	 */
	private static final class EndPointComparator implements IElementComparer {


		/**
		 * @see org.eclipse.jface.viewers.IElementComparer#equals(java.lang.Object, java.lang.Object)
		 *
		 * @param a
		 * @param b
		 * @return
		 */
		@Override
		public boolean equals(Object a, Object b) {
			if ((a instanceof Endpoint)
					&& (b instanceof Endpoint)) {

				String adress = ((Endpoint) a).getAddress();
				String adress2 = ((Endpoint) b).getAddress();
				if (adress.equals(adress2)) {
					return true;
				}


			}
			return false;
		}

		/**
		 * @see org.eclipse.jface.viewers.IElementComparer#hashCode(java.lang.Object)
		 *
		 * @param element
		 * @return
		 */
		@Override
		public int hashCode(Object element) {
			// TODO Auto-generated method stub
			return 0;
		}
	}

	class CComboObservableValue extends AbstractObservableValue implements SelectionListener, KeyListener, FocusListener {

		private String previousValue;

		public CComboObservableValue() {
			previousValue = combo.getText();
			combo.addSelectionListener(this); // Selection change
			combo.addKeyListener(this); // Enter pressed
			combo.addFocusListener(this); // Focus lost
		}

		@Override
		public Object getValueType() {
			return String.class;
		}

		@Override
		protected String doGetValue() {
			return combo.getText();
		}

		@Override
		protected void doSetValue(Object value) {
			if (modelProperty instanceof AggregatedObservable && ((AggregatedObservable) modelProperty).hasDifferentValues()) {
				combo.setText(UnchangedObject.instance.toString());
			} else if (value instanceof String) {

				previousValue = (String) value;
				combo.setText(previousValue);

			}
		}

		// Enter pressed
		@Override
		public void keyReleased(KeyEvent e) {
			if ((e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) && e.stateMask == SWT.NONE) {
				maybeFireChange();
				e.doit = false; // Stops the propagation of the event
			}
		}

		// Selection change
		@Override
		public void widgetSelected(SelectionEvent e) {
			maybeFireChange();
		}

		// Focus lost
		@Override
		public void focusLost(FocusEvent e) {
			maybeFireChange();
		}

		void maybeFireChange() {
			// Only report a change if there is actually a change, otherwise we get a no-op command that dirties the editor
			final String currentValue = doGetValue();
			if ((currentValue == null) ? previousValue != null : !currentValue.equals(previousValue)) {
				doFireChange();
			}
		}

		private void doFireChange() {
			final String oldValue = previousValue;
			final String currentValue = previousValue = doGetValue();
			fireValueChange(new ValueDiff() {

				@Override
				public Object getOldValue() {
					return oldValue;
				}

				@Override
				public Object getNewValue() {
					return currentValue;
				}
			});
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			// Nothing
		}

		@Override
		public void focusGained(FocusEvent e) {
			// Nothing
		}

		@Override
		public void keyPressed(KeyEvent e) {
			// Nothing
		}
	}

	class EndPointComboObservableValue extends AbstractObservableValue implements SelectionListener, KeyListener, FocusListener {

		private Object previousValue;

		public EndPointComboObservableValue(Viewer viewer2, IObservableValue modelProperty2) {
			// String endpoint = combo.getText();
			// previousValue = getEndpoint(endpoint);
			// super(viewer, widgetObservable);
			viewer = (ComboViewer) viewer2;
			modelProperty = modelProperty2;
			previousValue = viewer.getSelection(); // can not rely on getSelection
			combo.addSelectionListener(this); // Selection change
			combo.addKeyListener(this); // Enter pressed
			combo.addFocusListener(this); // Focus lost

			viewer.setComparer(new EndPointComparator());
			setViewer(viewer);
		}

		@Override
		public Object getValueType() {
			return Endpoint.class;
		}


		@Override
		protected Object doGetValue() {
			// String endpoint = combo.getText();
			// return getEndpoint(endpoint);
			ISelection selection = viewer.getSelection();
			if (!selection.isEmpty() && selection instanceof IStructuredSelection) {
				IStructuredSelection structuredSelection = (IStructuredSelection) selection;
				Object firstElement = structuredSelection.getFirstElement();
				if (firstElement == UnsetObject.instance) {
					return null;
				}
				if (firstElement == UnchangedObject.instance) {
					return null;
				}
				return firstElement;
			}

			return null;
		}

		/**
		 * @param endpoint
		 * @return
		 */
		private Endpoint getEndpoint(String endpoint) {

			if (endpoint != "") {


				Endpoint endPoint = AASFactory.eINSTANCE.createEndpoint();
				String[] propertiesValues = endpoint.substring(1, endpoint.length() - 1).split(",");
				for (int i = 0; i < propertiesValues.length; i++) {
					propertiesValues[i] = propertiesValues[i].split("=")[1];
				}

				endPoint.setAddress(propertiesValues[0]);
				endPoint.setName(propertiesValues[2]);
				endPoint.setProtocol(ProtocolKind.get(propertiesValues[1]));
				// endPoint.setPort(Integer.parseInt(propertiesValues[3]));
				return endPoint;
			}
			return null;
		}

		@Override
		protected void doSetValue(Object value) {
			if (modelProperty instanceof AggregatedObservable && (((AggregatedObservable) modelProperty).hasDifferentValues())) {
				combo.setText(UnchangedObject.instance.toString());

			} else if (value instanceof Endpoint) {
				// This is the new baseline value (coming from the model) against which to compare a future edit by the user
				previousValue = value;
				combo.setText(((ILabelProvider) viewer.getLabelProvider()).getText(previousValue));
				// List list = new ArrayList();
				// list.add(value);

				// viewer.setSelection(new StructuredSelection(list, new EndPointComparator()), true);
			}
		}

		// Enter pressed
		@Override
		public void keyReleased(KeyEvent e) {
			if ((e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) && e.stateMask == SWT.NONE) {
				maybeFireChange();

				e.doit = false; // Stops the propagation of the event

			}
		}

		// private void changesColorField() {
		//
		// if (binding != null) {
		//
		// if (timer == null) {
		// timer = new Timer(true);
		// }
		//
		// cancelChangeColorTask();
		// changeColorTask = new TimerTask() {
		//
		// @Override
		// public void run() {
		// if (EndPointComboDialog.this.isDisposed()) {
		// return;
		// }
		// EndPointComboDialog.this.getDisplay().syncExec(new Runnable() {
		//
		// @Override
		// public void run() {
		// combo.setBackground(DEFAULT);
		// combo.update();
		// }
		// });
		// }
		// };
		//
		// if (errorBinding) {
		// combo.setBackground(ERROR);
		// combo.update();
		// } else {
		// IStatus status = binding.getValidationStatus().getValue();
		// switch (status.getSeverity()) {
		// case IStatus.OK:
		// timer.schedule(changeColorTask, 600);
		// combo.setBackground(VALID);
		// combo.update();
		// break;
		// case IStatus.ERROR:
		// combo.setBackground(ERROR);
		// combo.update();
		// break;
		// default:
		// combo.setBackground(DEFAULT);
		// combo.update();
		// break;
		// }
		// }
		// }
		// }

		// Selection change
		@Override
		public void widgetSelected(SelectionEvent e) {
			maybeFireChange();
		}

		// Focus lost
		@Override
		public void focusLost(FocusEvent e) {
			maybeFireChange();

		}

		void maybeFireChange() {
			// Only report a change if there is actually a change, otherwise we get a no-op command that dirties the editor
			final Object currentValue = doGetValue();
			if (currentValue != null && !new EndPointComparator().equals(currentValue, previousValue)) {
				doFireChange();
			}
		}

		/**
		 * @param currentValue
		 * @param previousValue2
		 * @return
		 */
		private boolean aredifferent(Endpoint currentValue, Endpoint previousValue2) {
			if (previousValue2 == null) {
				return true;
			}
			if (!(currentValue.getName().equals(previousValue2.getName()) &&
					currentValue.getProtocol().equals(previousValue2.getProtocol()) &&
					currentValue.getAddress().equals(previousValue2.getAddress()))) {
				return true;
			}
			return false;
		}

		private void doFireChange() {
			final Object oldValue = previousValue;
			final Object currentValue = doGetValue();
			previousValue = currentValue;
			fireValueChange(new ValueDiff() {

				@Override
				public Object getOldValue() {
					return oldValue;
				}

				@Override
				public Object getNewValue() {
					return currentValue;
				}
			});
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			// Nothing
		}

		@Override
		public void focusGained(FocusEvent e) {
			// Nothing
		}

		@Override
		public void keyPressed(KeyEvent e) {
			// Nothing
		}
	}


}


