/*****************************************************************************
 * Copyright (c) 2021 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ibtihel khemir (CEA LIST) ibtihel.khemir@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.aas.ui.modelExplorer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.papyrus.aas.AssetAdministrationShell;
import org.eclipse.papyrus.aas.Submodel;
import org.eclipse.papyrus.emf.facet.efacet.core.IFacetManager;
import org.eclipse.papyrus.emf.facet.efacet.core.exception.DerivedTypedElementException;
import org.eclipse.papyrus.emf.facet.query.java.core.IJavaQuery2;
import org.eclipse.papyrus.emf.facet.query.java.core.IParameterValueList2;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.util.UMLUtil;

public class StereotypeSubModelReferencesQuery implements IJavaQuery2<Class, List<Class>> {

	@Override
	public List<Class> evaluate(final Class context,
			final IParameterValueList2 parameterValues,
			final IFacetManager facetManager)
			throws DerivedTypedElementException {
		ArrayList<Class> result = new ArrayList<>();
		AssetAdministrationShell appliedAssetAdministrationShell = UMLUtil.getStereotypeApplication(context, AssetAdministrationShell.class);
		if (appliedAssetAdministrationShell != null) {
			for (Iterator<Submodel> iterator = appliedAssetAdministrationShell.getSubmodel().iterator(); iterator.hasNext();) {
				Submodel parapeterValue = iterator.next();
				result.add((Class) UMLUtil.getBaseElement(parapeterValue));
			}
		}

		return result;
	}
}
