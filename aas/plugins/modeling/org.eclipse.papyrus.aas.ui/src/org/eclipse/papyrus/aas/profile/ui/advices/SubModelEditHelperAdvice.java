/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.aas.profile.ui.advices;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.commands.ConfigureElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.papyrus.aas.AssetAdministrationShell;
import org.eclipse.papyrus.aas.Reference;
import org.eclipse.papyrus.aas.Submodel;
import org.eclipse.papyrus.aas.ui.utils.IAASElementTypes;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * @author AS247872
 *
 */
public class SubModelEditHelperAdvice extends AbstractEditHelperAdvice {


	public SubModelEditHelperAdvice() {
		super();
	}

	@Override
	public ICommand getAfterConfigureCommand(ConfigureRequest request) {
		// TODO Auto-generated method stub
		EObject newElement = request.getElementToConfigure();
		EObject container = newElement.eContainer();
		TransactionalEditingDomain domain = request.getEditingDomain();
		// get the previous container to set the Submodel of the AAS

		if (container != null) {
			if (container instanceof Class) {

				return new ConfigureElementCommand(request) {
					@Override
					protected CommandResult doExecuteWithResult(IProgressMonitor progressMonitor, IAdaptable info) throws ExecutionException {

						Model model = ((Element) container).getModel();
						AssetAdministrationShell aas = UMLUtil.getStereotypeApplication((Element) container, AssetAdministrationShell.class);
						Submodel submodel = UMLUtil.getStereotypeApplication((Element) newElement, Submodel.class);
						// no need to populate the submodels list it is done automatically
						// if (aas != null) {
						// aas.getSubmodel().add(submodel);
						// }
						// for the semantic id display in the Properties view, we should have a Reference attached to the SubModel
						// create reference
						IElementType referenceElementType = ElementTypeRegistry.getInstance().getType(IAASElementTypes.REFERENCE_ID);
						CreateElementRequest request2 = new CreateElementRequest(domain, model, referenceElementType);
						IElementEditService provider2 = ElementEditServiceUtils.getCommandProvider(model);
						ICommand command = provider2.getEditCommand(request2);

						if ((command != null) && command.canExecute()) {
							try {
								command.execute(new NullProgressMonitor(), null);
							} catch (ExecutionException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						Object result = command.getCommandResult().getReturnValue();
						if (result != null) {
							Reference ref = getReference((Element) result);
							submodel.setSemanticId(ref);
						}
						return CommandResult.newOKCommandResult(newElement);

					}
				};
			} else if (container instanceof Package || container instanceof Model) {
				return new ElementConfigurationCommand(request);
			}
		}
		return super.getAfterConfigureCommand(request);


	}

	public static Reference getReference(Element element) {
		Reference conceptDescription = null;
		if (element != null) {
			conceptDescription = UMLUtil.getStereotypeApplication(element, Reference.class);
		}
		return conceptDescription;

	}

	// @Override
	// protected ICommand getBeforeConfigureCommand(ConfigureRequest request) {
	// return new ElementConfigurationCommand(request);
	// }

}
