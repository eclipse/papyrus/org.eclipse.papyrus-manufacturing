/*****************************************************************************
 * Copyright (c) 2021 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.aas.ui.utils;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.aas.Property;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.util.UMLUtil;

public final class AASUtils {

	public static boolean isProperty(EObject eObject) {
		if (eObject instanceof Element) {
			Element element = (Element) eObject;
			return (null != getStereotypeApplication(element));

		}
		return false;
	}

	public static boolean hasSemantics(EObject eObject) {
		if (eObject instanceof Element) {
			Element element = (Element) eObject;
			Stereotype sterotype = org.eclipse.papyrus.uml.tools.utils.UMLUtil.getAppliedStereotype(element, "AAS::HasSemantics", false);
			return (sterotype != null);

		}
		return false;
	}

	public static boolean isReferable(EObject eObject) {
		if (eObject instanceof Element) {
			Element element = (Element) eObject;
			Stereotype sterotype = org.eclipse.papyrus.uml.tools.utils.UMLUtil.getAppliedStereotype(element, "AAS::Referable", false);
			return (sterotype != null);

		}
		return false;
	}

	public static Property getStereotypeApplication(Element element) {
		Property key = null;
		if (element != null) {
			key = UMLUtil.getStereotypeApplication(element, Property.class);
		}
		return key;

	}

}
