/*****************************************************************************
 * Copyright (c) 2021 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Asma Smaoui (CEA LIST) ibtihel.khemir@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.aas.ui.modelExplorer;

import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.aas.AssetAdministrationShell;
import org.eclipse.papyrus.aas.Submodel;
import org.eclipse.papyrus.emf.facet.efacet.core.IFacetManager;
import org.eclipse.papyrus.emf.facet.efacet.core.exception.DerivedTypedElementException;
import org.eclipse.papyrus.emf.facet.query.java.core.IJavaQuery2;
import org.eclipse.papyrus.emf.facet.query.java.core.IParameterValueList2;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.util.UMLUtil;

public class GetSubmodelContainer implements IJavaQuery2<Class, EObject> {

	/**
	 * @see org.eclipse.papyrus.emf.facet.query.java.core.IJavaQuery2#evaluate(org.eclipse.emf.ecore.EObject, org.eclipse.papyrus.emf.facet.query.java.core.IParameterValueList2, org.eclipse.papyrus.emf.facet.efacet.core.IFacetManager)
	 *
	 * @param source
	 * @param parameterValues
	 * @param facetManager
	 * @return
	 * @throws DerivedTypedElementException
	 */
	@Override
	public EObject evaluate(Class source, IParameterValueList2 parameterValues, IFacetManager facetManager) throws DerivedTypedElementException {


		List<Element> possibleAasContainers = source.getModel().allOwnedElements().stream()
				.filter(e -> (UMLUtil.getStereotypeApplication(e, AssetAdministrationShell.class) != null))
				.collect(Collectors.toList());
		if (possibleAasContainers != null && !possibleAasContainers.isEmpty()) {
			for (Element element : possibleAasContainers) {
				AssetAdministrationShell ass = UMLUtil.getStereotypeApplication(element, AssetAdministrationShell.class);
				if (ass != null && ass.getSubmodel().contains(UMLUtil.getStereotypeApplication(source, Submodel.class))) {
					return ass.getBase_Class();
				}
			}
		}
		return null;

	}
}
