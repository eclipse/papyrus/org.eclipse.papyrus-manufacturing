package org.eclipse.papyrus.aas.ui.handlers;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager;
import org.eclipse.papyrus.infra.nattable.provider.TableStructuredSelection;
import org.eclipse.papyrus.infra.nattable.utils.TableSelectionWrapper;

public class CreateElementHandlerTester extends PropertyTester {

	public CreateElementHandlerTester() {
		// TODO Auto-generated constructor stub
	}

	private static final String IS_Bom_Table = "IsBOMTable"; //$NON-NLS-1$
	private static final String IS_Operation_Table = "IsOperationTable"; //$NON-NLS-1$
	private static final String IS_Property_Table = "IsPropertyTable"; //$NON-NLS-1$


	private static final String BOM_TABLE_TYPE = "BOMTableType";
	private static final String PROPERTY_TABLE_TYPE = "PropertiesTableType";

	private static final String OPERATION_TABLE_TYPE = "OperationsTableType";


	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {
		if (receiver instanceof TableStructuredSelection
				&& expectedValue instanceof Boolean) {
			final String type = getTableType((TableStructuredSelection) receiver);
			if (type != null) {
				if (IS_Bom_Table.equals(property)) {
					boolean value = expectedValue.equals(BOM_TABLE_TYPE.equals(type));
					if (value) {
						int i = 0;
						i++;
					}
					return expectedValue.equals(BOM_TABLE_TYPE.equals(type));
				}
				if (IS_Operation_Table.equals(property)) {
					return expectedValue.equals(OPERATION_TABLE_TYPE.equals(type));
				}
				if (IS_Property_Table.equals(property)) {
					return expectedValue.equals(PROPERTY_TABLE_TYPE.equals(type));
				}
			}
		}
		return false;
	}


	private String getTableType(final TableStructuredSelection selection) {
		TableSelectionWrapper wrapper = (TableSelectionWrapper) selection.getAdapter(TableSelectionWrapper.class);
		INattableModelManager manager = wrapper.getNatTableModelManager();
		if (manager != null && manager.getTable() != null) {
			return manager.getTable().getTableConfiguration().getType();
		}
		return null;
	}


}
