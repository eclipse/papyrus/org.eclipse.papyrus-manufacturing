/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.aas.profile.ui.modelelement;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.infra.emf.utils.EMFHelper;
import org.eclipse.papyrus.infra.properties.contexts.DataContextElement;
import org.eclipse.papyrus.infra.properties.ui.modelelement.EMFModelElement;
import org.eclipse.papyrus.uml.properties.modelelement.StereotypeModelElementFactory;
import org.eclipse.papyrus.uml.tools.utils.UMLUtil;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Stereotype;

public class AssStereotypeModelElementFactory extends StereotypeModelElementFactory {



	private static final String AAS = "AAS"; // $NON-NLS-0$

	@Override
	protected EMFModelElement doCreateFromSource(Object sourceElement, DataContextElement context) {

		Element umlElement = UMLUtil.resolveUMLElement(sourceElement);
		EMFModelElement modelElement = null;
		if (umlElement == null && sourceElement instanceof EObject) {
			umlElement = org.eclipse.uml2.uml.util.UMLUtil.getBaseElement((EObject) sourceElement);
		}

		if (umlElement != null) {
			Stereotype stereotype = UMLUtil.getAppliedSuperstereotype(umlElement, getQualifiedName(context));
			Stereotype actual = (stereotype == null) ? null : UMLUtil.getAppliedSubstereotype(umlElement, stereotype);
			EObject stereotypeApplication = (actual == null) ? null : umlElement.getStereotypeApplication(actual);

			if (null != stereotypeApplication) {

				// check if we are in the AAS case
				if (umlElement instanceof Element && null != stereotype && AAS.equals(stereotype.getProfile().getName())) {
					modelElement = new AssStereotypeModelElement(stereotypeApplication, stereotype, EMFHelper.resolveEditingDomain(sourceElement));
				} else {
					modelElement = super.doCreateFromSource(umlElement, context);
				}

			}
		} else {
			modelElement = super.doCreateFromSource(sourceElement, context);
		}
		return modelElement;

	}

}
