/*****************************************************************************
 * Copyright (c) 2021 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.aas.ui.utils;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.aas.HasSemantics;
import org.eclipse.papyrus.aas.Key;
import org.eclipse.papyrus.aas.KeyElements;
import org.eclipse.papyrus.aas.KeyType;
import org.eclipse.papyrus.aas.Reference;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Utility class on Key Stereotype properties element
 */
public final class KeyUtils {


	/**
	 * Checks if is a RT port.
	 *
	 * @param eObject
	 *            the e object
	 * @return true, if is a RT port
	 */
	public static boolean isKey(EObject eObject) {
		if (eObject instanceof Element) {
			Element element = (Element) eObject;
			return (null != getStereotypeApplication(element));

		}
		return false;
	}


	public static Key getStereotypeApplication(Element element) {
		Key key = null;
		if (element != null) {
			key = UMLUtil.getStereotypeApplication(element, Key.class);
		}
		return key;

	}

	public static HasSemantics getHasSemanticsApplication(Element element) {
		HasSemantics hassemantics = null;
		if (element != null) {
			hassemantics = UMLUtil.getStereotypeApplication(element, HasSemantics.class);
		}
		return hassemantics;

	}

	public static KeyElements getType(Key property) {
		return property.getType();
	}

	public static KeyType getidType(Element element) {
		return getStereotypeApplication(element) == null ? null : getStereotypeApplication(element).getIdType();
	}

	public static String getRegistration(Element element) {
		return getStereotypeApplication(element) == null ? null : getStereotypeApplication(element).getValue();
	}

	private static Key getKeyFromUMLElement(Element umlElement) {
		if (umlElement != null) {
			// get the semantic id of the HasSemantic
			Stereotype sterotype = org.eclipse.papyrus.uml.tools.utils.UMLUtil.getAppliedStereotype(umlElement, "AAS::HasSemantics", false);
			Reference semanticid = (Reference) umlElement.getValue(sterotype, "semanticId");
			if (semanticid != null && semanticid.getKey() != null) {
				Key key = semanticid.getKey().get(0);
				return key;
			}
		}
		return null;
	}


}

