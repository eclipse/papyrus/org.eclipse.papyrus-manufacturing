/*****************************************************************************
 * Copyright (c) 2021 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.aas.ui.utils;

/**
 * @author AS247872
 *
 */
public interface IAASElementTypes {



	public static final String REFERENCE_ID = "org.eclipse.papyrus.aAS.Reference"; //$NON-NLS-1$

	public static final String KEY_ID = "org.eclipse.papyrus.aAS.Key"; //$NON-NLS-1$




}
