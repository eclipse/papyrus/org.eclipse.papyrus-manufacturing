/*****************************************************************************
 * Copyright (c) 2023 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.aas.ui.widgets;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.papyrus.infra.properties.ui.widgets.AbstractPropertyEditor;
import org.eclipse.papyrus.infra.widgets.providers.IStaticContentProvider;
import org.eclipse.swt.widgets.Composite;

/**
 * @author AS247872
 *
 */
public class EndPointComboEditor extends AbstractPropertyEditor {

	protected EndPointComboDialog editor;
	protected EndPointComboDialog refCombo;

	public EndPointComboEditor(Composite parent, int style) {
		super(new EndPointComboDialog(parent, style));
		refCombo = (EndPointComboDialog) valueEditor;

	}



	@Override
	public void doBinding() {
		IStaticContentProvider contentProvider = input.getContentProvider(propertyPath);
		ILabelProvider labelProvider = input.getLabelProvider(propertyPath);
		// editor.setProviders(contentProvider, labelProvider);
		refCombo.setContentProvider(contentProvider);
		refCombo.setLabelProvider(labelProvider);
		super.doBinding();
	}


}
