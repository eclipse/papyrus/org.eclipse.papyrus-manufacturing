package org.eclipse.papyrus.aas.ui.modelExplorer;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.papyrus.emf.facet.efacet.core.IFacetManager;
import org.eclipse.papyrus.emf.facet.efacet.core.exception.DerivedTypedElementException;
import org.eclipse.papyrus.emf.facet.query.java.core.IJavaQuery2;
import org.eclipse.papyrus.emf.facet.query.java.core.IParameterValueList2;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.UMLPackage;

public class HidePackageableElementQuery implements IJavaQuery2<Model, List<EReference>> {
	@Override
	public List<EReference> evaluate(final Model context,
			final IParameterValueList2 parameterValues,
			final IFacetManager facetManager)
			throws DerivedTypedElementException {
		List<EReference> eRefToShow = new ArrayList<>();
		eRefToShow.add(UMLPackage.Literals.PACKAGE_IMPORT__IMPORTED_PACKAGE);
		return eRefToShow;
	}
}
