/*****************************************************************************
 * Copyright (c) 2021 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ibtihel khemir (CEA LIST) ibtihel.khemir@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.aas.ui.tableProvider;

import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.papyrus.aas.AASFactory;
import org.eclipse.papyrus.aas.Entity;
import org.eclipse.papyrus.aas.LangEnum;
import org.eclipse.papyrus.aas.LangString;
import org.eclipse.papyrus.aas.Operation;
import org.eclipse.papyrus.aas.Property;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager;
import org.eclipse.papyrus.infra.ui.converter.AbstractStringValueConverter;
import org.eclipse.papyrus.uml.nattable.manager.cell.StereotypePropertyCellManager;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * @author IK264250
 *
 */
public class DescriptionLangStringCollectionCellManager extends StereotypePropertyCellManager {

	private static final String BOM_TABLE_TYPE = "BOMTableType";
	private static final String PROPERTY_TABLE_TYPE = "PropertiesTableType";

	private static final String OPERATION_TABLE_TYPE = "OperationsTableType";
	private static final String DESCRIPTION = "description";

	/**
	 * @see org.eclipse.papyrus.uml.nattable.manager.cell.StereotypePropertyCellManager#handles(java.lang.Object, java.lang.Object, org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager)
	 *
	 * @param columnElement
	 * @param rowElement
	 * @param tableManager
	 * @return
	 */
	@Override
	public boolean handles(Object columnElement, Object rowElement, INattableModelManager tableManager) {
		// TODO Auto-generated method stub
		boolean res = super.handles(columnElement, rowElement, tableManager);
		if (res) {
			final List<Object> umlObjects = organizeAndResolvedObjects(columnElement, rowElement, null);
			res = umlObjects.get(1) instanceof String && ((String) umlObjects.get(1)).endsWith(DESCRIPTION);
		}
		return res;
	}

	/**
	 * @see org.eclipse.papyrus.uml.nattable.manager.cell.StereotypePropertyCellManager#doGetValue(java.lang.Object, java.lang.Object, org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager)
	 *
	 * @param columnElement
	 * @param rowElement
	 * @param tableManager
	 * @return
	 */
	@Override
	protected Object doGetValue(Object columnElement, Object rowElement, INattableModelManager tableManager) {
		// TODO Auto-generated method stub
		Object res = null;
		try {
			res = super.doGetValue(columnElement, rowElement, tableManager);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
		// return "toto";
	}

	/**
	 * @see org.eclipse.papyrus.uml.nattable.manager.cell.UMLFeatureCellManager#getOrCreateStringValueConverterClass(java.util.Map, java.lang.String, org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager)
	 *
	 * @param existingConverters
	 * @param multiValueSeparator
	 * @param tableManager
	 * @return
	 */
	@Override
	public AbstractStringValueConverter getOrCreateStringValueConverterClass(Map<Class<? extends AbstractStringValueConverter>, AbstractStringValueConverter> existingConverters, String multiValueSeparator, INattableModelManager tableManager) {
		// probably nothing to do
		return super.getOrCreateStringValueConverterClass(existingConverters, multiValueSeparator, tableManager);
	}

	/**
	 * @see org.eclipse.papyrus.uml.nattable.manager.cell.StereotypePropertyCellManager#getSetStringValueCommand(org.eclipse.emf.transaction.TransactionalEditingDomain, java.lang.Object, java.lang.Object, java.lang.String,
	 *      org.eclipse.papyrus.infra.ui.converter.AbstractStringValueConverter, org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager)
	 *
	 * @param domain
	 * @param columnElement
	 * @param rowElement
	 * @param newValue
	 * @param valueSolver
	 * @param tableManager
	 * @return
	 */
	@Override
	public Command getSetStringValueCommand(TransactionalEditingDomain domain, Object columnElement, Object rowElement, String newValue, AbstractStringValueConverter valueSolver, INattableModelManager tableManager) {
		CompositeCommand cc = new CompositeCommand("description LangString command"); //$NON-NLS-1$
		String tableType = tableManager.getTable().getTableConfiguration().getType();
		// Create a LangString
		LangString langstring = AASFactory.eINSTANCE.createLangString();
		langstring.setLang(LangEnum.EN);
		langstring.setValue(newValue);
		if (tableType.equals(BOM_TABLE_TYPE) == true) {
			Entity ModelProp = UMLUtil.getStereotypeApplication((Element) rowElement, Entity.class);
			ModelProp.getDescription().add(0, langstring);

		} else {
			if (tableType.equals(OPERATION_TABLE_TYPE) == true) {

				Operation ModelProp = UMLUtil.getStereotypeApplication((Element) rowElement, Operation.class);
				ModelProp.getDescription().add(0, langstring);
			} else {
				if (tableType.equals(PROPERTY_TABLE_TYPE) == true) {

					Property ModelProp = UMLUtil.getStereotypeApplication((Element) rowElement, Property.class);
					ModelProp.getDescription().add(0, langstring);
				}
			}
		}

		return GMFtoEMFCommandWrapper.wrap(cc);
	}

}
