package org.eclipse.papyrus.aas.ui.modelExplorer;

import org.eclipse.papyrus.emf.facet.efacet.core.IFacetManager;
import org.eclipse.papyrus.emf.facet.efacet.core.exception.DerivedTypedElementException;
import org.eclipse.papyrus.emf.facet.query.java.core.IJavaQuery2;
import org.eclipse.papyrus.emf.facet.query.java.core.IParameterValueList2;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.Operation;

public class StereotypeActivityReferenceQuery implements IJavaQuery2<Operation, Activity> {
	@Override
	public Activity evaluate(final Operation context,
			final IParameterValueList2 parameterValues,
			final IFacetManager facetManager)
			throws DerivedTypedElementException {
		if (context != null && !context.getMethods().isEmpty()) {
			Behavior parapeterValue = context.getMethods().get(0);
			// should test the type because we have now Opaque behaviors for operations.
			if (parapeterValue instanceof Activity) {
				Activity result = (Activity) parapeterValue;
				return result;
			}

		}
		return null;
	}
}
