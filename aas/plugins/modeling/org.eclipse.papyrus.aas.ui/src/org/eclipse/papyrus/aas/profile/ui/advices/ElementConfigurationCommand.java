/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.aas.profile.ui.advices;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.commands.ConfigureElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.CreateElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;

public class ElementConfigurationCommand extends ConfigureElementCommand {


	private final org.eclipse.uml2.uml.Class element;

	private IProgressMonitor progressMonitor;

	private IAdaptable info;

	private IElementType packageelementType;

	private IElementType elementType;

	public ElementConfigurationCommand(ConfigureRequest request) {
		super(request);
		this.element = (Class) request.getElementToConfigure();
		this.elementType = request.getTypeToConfigure();
		setPackageTypes(elementType);

	}

	/**
	 * @param elementType2
	 */
	private void setPackageTypes(IElementType elementType) {

		if (elementType.equals(AASElementTypesEnumerator.AAS) || elementType.equals(AASElementTypesEnumerator.AAS_UI)) {
			packageelementType = AASElementTypesEnumerator.AAS_Package;
		} else if (elementType.equals(AASElementTypesEnumerator.ASSET) || elementType.equals(AASElementTypesEnumerator.ASSET_UI)) {
			packageelementType = AASElementTypesEnumerator.ASSET_Package;
		} else if (elementType.equals(AASElementTypesEnumerator.CONCEPTDESCRIPTION)) {
			packageelementType = AASElementTypesEnumerator.CONCEPTDESCRIPTION_Package;
		} else if (elementType.equals(AASElementTypesEnumerator.SUBMODEL) || elementType.equals(AASElementTypesEnumerator.SUBMODEL_UI)) {
			packageelementType = AASElementTypesEnumerator.SUBMODEL_Package;
		}

	}

	protected EObject createElement(Class element, IElementType elementType) throws ExecutionException {
		if ((element == null)) {
			throw new ExecutionException("element is null");
		}

		Package container = getSpecificElementPackage(element.getModel(), getStereotypeName());

		EObject newElement = null;
		if (container == null) {
			// create a Package to hold the new element
			CreateElementRequest createElementRequest = new CreateElementRequest(element.getModel(), packageelementType);
			CreateElementCommand command = new CreateElementCommand(createElementRequest);
			command.execute(progressMonitor, info);
			newElement = command.getNewElement();

			if (newElement == null) {
				throw new ExecutionException("Element creation problem for " + elementType.getDisplayName() + ".");
			}


			container = (Package) newElement;
			if (this.packageelementType != null) {
				String packageName = getPackageName(packageelementType);
				container.setName(packageName);
			}
		}

		EList<PackageableElement> packagedElements = container.getPackagedElements();
		packagedElements.add(element);

		return newElement;
	}



	/**
	 * @param packageelementType2
	 * @return
	 */
	private String getPackageName(IElementType packageelementType) {
		if (packageelementType.equals(AASElementTypesEnumerator.AAS_Package)) {
			return "AASs";
		} else if (packageelementType.equals(AASElementTypesEnumerator.ASSET_Package)) {
			return "Assets";
		} else if (packageelementType.equals(AASElementTypesEnumerator.CONCEPTDESCRIPTION_Package)) {
			return "ConceptDescriptions";
		} else if (packageelementType.equals(AASElementTypesEnumerator.SUBMODEL_Package)) {
			return "Submodels";
		}
		return null;
	}

	private String getStereotypeName() {

		if (elementType.equals(AASElementTypesEnumerator.AAS) || elementType.equals(AASElementTypesEnumerator.AAS_UI)) {
			return "AAS::AssetAdministrationShell";
		} else if (elementType.equals(AASElementTypesEnumerator.ASSET) || elementType.equals(AASElementTypesEnumerator.ASSET_UI)) {
			return "AAS::Asset";
		} else if (elementType.equals(AASElementTypesEnumerator.CONCEPTDESCRIPTION)) {
			return "AAS::ConceptDescription";
		} else if (elementType.equals(AASElementTypesEnumerator.SUBMODEL) || elementType.equals(AASElementTypesEnumerator.SUBMODEL_UI)) {
			return "AAS::Submodel";
		}
		return null;
	}

	/**
	 * @see org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand#doExecuteWithResult(org.eclipse.core.runtime.IProgressMonitor, org.eclipse.core.runtime.IAdaptable)
	 */
	@Override
	protected CommandResult doExecuteWithResult(IProgressMonitor progressMonitor, IAdaptable info) throws ExecutionException {
		this.progressMonitor = progressMonitor;
		this.info = info;

		createElement(element, packageelementType);


		return CommandResult.newOKCommandResult(element);
	}

	private static Package getSpecificElementPackage(Model model, String stereotypeName) {
		EList<Package> packages = model.getNestedPackages();
		if (packages != null && !packages.isEmpty()) {
			for (Package p : packages) {
				if (p.getAppliedStereotypes() != null && p.getAppliedStereotype(stereotypeName) != null) {
					return p;

				}
			}
		}
		return null;
	}


}

