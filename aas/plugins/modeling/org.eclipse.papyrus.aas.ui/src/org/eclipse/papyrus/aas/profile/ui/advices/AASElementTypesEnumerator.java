/*****************************************************************************
 * Copyright (c) 2021 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.aas.profile.ui.advices;

import org.eclipse.gmf.runtime.emf.type.core.AbstractElementTypeEnumerator;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;

/**
 * Enumeration of UI-specific element types, such as the "creation element types"
 * that are not intended to match existing elements by are used to invoke edit-helper
 * behaviour for specific creation scenarios.
 */
public class AASElementTypesEnumerator extends AbstractElementTypeEnumerator {

	public static final IHintedType AAS_Package = (IHintedType) getElementType("org.eclipse.papyrus.aAS.AssetAdministrationShell_Package");
	public static final IHintedType AAS = (IHintedType) getElementType("org.eclipse.papyrus.aAS.AssetAdministrationShell");
	public static final IHintedType AAS_UI = (IHintedType) getElementType("org.eclipse.papyrus.aAS.di.AssetAdministrationShell_Class_Shape");
	public static final IHintedType SUBMODEL_Package = (IHintedType) getElementType("org.eclipse.papyrus.aAS.Submodel_Package");
	public static final IHintedType SUBMODEL = (IHintedType) getElementType("org.eclipse.papyrus.aAS.Submodel");
	public static final IHintedType SUBMODEL_UI = (IHintedType) getElementType("org.eclipse.papyrus.aAS.di.Submodel_Class_Shape");
	public static final IHintedType ASSET_Package = (IHintedType) getElementType("org.eclipse.papyrus.aAS.Asset_Package");
	public static final IHintedType ASSET = (IHintedType) getElementType("org.eclipse.papyrus.aAS.Asset");
	public static final IHintedType ASSET_UI = (IHintedType) getElementType("org.eclipse.papyrus.aAS.di.Asset_Class_Shape");
	public static final IHintedType CONCEPTDESCRIPTION_Package = (IHintedType) getElementType("org.eclipse.papyrus.aAS.ConceptDescription_Package");
	public static final IHintedType CONCEPTDESCRIPTION = (IHintedType) getElementType("org.eclipse.papyrus.aAS.ConceptDescription");
	public static final IHintedType REFERENCE = (IHintedType) getElementType("org.eclipse.papyrus.aAS.Reference");
	public static final IHintedType OPERATION = (IHintedType) getElementType("org.eclipse.papyrus.aAS.Operation");

	public static final String BPMN_ACTIVITY_STRING = "org.eclipse.papyrus.bpmn.BPMNProcess_Activity";
	public static final IHintedType BPMN_ACTIVITY = (IHintedType) getElementType(BPMN_ACTIVITY_STRING);


}
