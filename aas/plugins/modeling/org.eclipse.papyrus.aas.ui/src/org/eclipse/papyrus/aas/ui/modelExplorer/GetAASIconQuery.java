/*****************************************************************************
 * Copyright (c) 2021 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ibtihel khemir (CEA LIST) ibtihel.khemir@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.aas.ui.modelExplorer;

import java.net.URL;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.papyrus.aas.profile.ui.Activator;
import org.eclipse.papyrus.emf.facet.custom.metamodel.custompt.IImage;
import org.eclipse.papyrus.emf.facet.custom.ui.ImageUtils;
import org.eclipse.papyrus.emf.facet.efacet.core.IFacetManager;
import org.eclipse.papyrus.emf.facet.efacet.core.exception.DerivedTypedElementException;
import org.eclipse.papyrus.emf.facet.query.java.core.IJavaQuery2;
import org.eclipse.papyrus.emf.facet.query.java.core.IParameterValueList2;
import org.eclipse.uml2.uml.Element;
import org.osgi.framework.Bundle;

public class GetAASIconQuery implements IJavaQuery2<Element, IImage> {

	@Override
	public IImage evaluate(Element source, IParameterValueList2 parameterValues, IFacetManager facetManager) throws DerivedTypedElementException {

		/// first method
		/*
		 * ParameterValue parameterValue = parameterValues.getParameterValueByName("eObject");
		 * if (parameterValue.getValue() instanceof EStructuralFeature) {
		 * return (Image) ImageQuery.getEObjectImage((EStructuralFeature) parameterValue.getValue());
		 *
		 * }
		 *
		 */

		//// second method
		/*
		 * File file = new File("/org.eclipse.papyrus.aas.ui/resources/icons/AAS.png");
		 * URI uri = file.toURI();
		 * return (Image) ImageUtils.wrap(uri.toString());
		 *
		 */
		Bundle b = Activator.getContext().getBundle();
		URL url = b.getResource("/resources/icons/AAS.png");
		return ImageUtils.wrap(ImageDescriptor.createFromURL(url).createImage());

	}
}
