/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.aas.profile.ui.advices;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.GetEditContextRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest;
import org.eclipse.papyrus.aas.Submodel;
import org.eclipse.papyrus.aas.SubmodelElementCollection;
import org.eclipse.papyrus.infra.emf.gmf.command.EMFtoGMFCommandWrapper;
import org.eclipse.papyrus.uml.tools.commands.ApplyStereotypeCommand;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * @author Saadia Dhouib
 *
 */
public class SubModelElementCollectionAdvice extends AbstractEditHelperAdvice {

	protected EObject source;
	protected EObject target;

	@Override
	public boolean approveRequest(IEditCommandRequest request) {
		if (request instanceof GetEditContextRequest) {
			GetEditContextRequest context = (GetEditContextRequest) request;
			if (context.getEditCommandRequest() instanceof CreateRelationshipRequest) {
				return approveCreateRelationshipRequest((CreateRelationshipRequest) context.getEditCommandRequest());
			}
		}
		return super.approveRequest(request);
	}

	/**
	 * Check that the source of the assignment is a technical policy
	 *
	 * @param request
	 * @return
	 */
	protected boolean approveCreateRelationshipRequest(CreateRelationshipRequest request) {

		source = request.getSource();
		target = request.getTarget();
		if ((source instanceof Element) && (target instanceof Element)) {
			// to be refined. Source and target must be blocks
			if (UMLUtil.getStereotypeApplication((Element) source, Submodel.class) != null &&
					UMLUtil.getStereotypeApplication((Element) target, SubmodelElementCollection.class) != null) {
				return true;
			}
			if (UMLUtil.getStereotypeApplication((Element) source, SubmodelElementCollection.class) != null &&
					UMLUtil.getStereotypeApplication((Element) target, SubmodelElementCollection.class) != null) {
				return true;
			}
		}
		return false;
	}



	/**
	 * @see org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice#getAfterEditCommand(org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest)
	 *
	 * @param request
	 * @return
	 */
	@Override
	public ICommand getAfterConfigureCommand(ConfigureRequest request) {
		// TODO Auto-generated method stub
		EObject newElement = request.getElementToConfigure();
		if (!(newElement instanceof Association)) {
			return super.getAfterConfigureCommand(request);
		}
		final Association association = (Association) request.getElementToConfigure();

		// use wrapper which adds a new empty composite. It will then add new commands for updating
		// association ends. This technique is used, since the newly created association has no ends yet.
		CompositeCommand wrapper = new CompositeCommand("wrapper") { //$NON-NLS-1$
			boolean first = true;

			@Override
			public boolean canExecute() {
				if (first) {
					return true;
				}
				return super.canExecute();
			}

			/**
			 * @see org.eclipse.gmf.runtime.common.core.command.AbstractCommand#execute(org.eclipse.core.runtime.IProgressMonitor, org.eclipse.core.runtime.IAdaptable)
			 *
			 * @param progressMonitor
			 * @param info
			 * @return
			 * @throws ExecutionException
			 */
			@Override
			public IStatus execute(IProgressMonitor progressMonitor, IAdaptable info) throws ExecutionException {

				if (first) {
					RecordingCommand command = applyStereotypeCommand(association, request.getEditingDomain());
					if (command != null) {
						add(EMFtoGMFCommandWrapper.wrap(command));
						association.setName("parentOf");
					}
					first = false;
				}


				return super.execute(progressMonitor, info);
			}

		};


		return wrapper;



	}

	public static RecordingCommand applyStereotypeCommand(Association association, TransactionalEditingDomain editingDomain) {
		if (association.getMemberEnds() != null) {
			for (int i = 0; i < association.getMemberEnds().size(); i++) {
				Property memberEnd = association.getMemberEnds().get(i);
				if (memberEnd.getType() != null) {
					if (memberEnd.getType() instanceof Class) {
						Class typeofmemberend = (Class) memberEnd.getType();

						for (Stereotype st : typeofmemberend.getAppliedStereotypes()) {
							if (st != null && st.getName().equals("SubmodelElementCollection")) {
								ApplyStereotypeCommand applyCommand = new ApplyStereotypeCommand(memberEnd, st, editingDomain);
								return applyCommand;
							}
						}

					}
				}

			}
		}


		return null;
	}

}
