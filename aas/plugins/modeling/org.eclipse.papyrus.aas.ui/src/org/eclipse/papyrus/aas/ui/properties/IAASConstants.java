/*****************************************************************************
 * Copyright (c) 2021 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.aas.ui.properties;

/**
 * This Class groups all the Constants related to the AAS context.
 */
public interface IAASConstants {

	/** The custom property semantic id **/
	public final static String PROPERTY_SEMANTICID = "semanticid";
	/** The custom property type related to semantic id **/
	public final static String KEY_TYPE = "type";
	/** The custom property type related to semantic id **/
	public final static String KEY_IDTYPE = "idType";

}
