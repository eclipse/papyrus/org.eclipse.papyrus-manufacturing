/*****************************************************************************
 * Copyright (c) 2021 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Ibtihel khemir (CEA LIST) ibtihel.khemir@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.aas.ui.tableProvider;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.papyrus.aas.Entity;
import org.eclipse.papyrus.aas.Key;
import org.eclipse.papyrus.aas.Operation;
import org.eclipse.papyrus.aas.Property;
import org.eclipse.papyrus.aas.Reference;
import org.eclipse.papyrus.aas.ui.utils.IAASElementTypes;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.papyrus.infra.ui.converter.AbstractStringValueConverter;
import org.eclipse.papyrus.uml.nattable.manager.cell.StereotypePropertyCellManager;
import org.eclipse.papyrus.uml.types.core.requests.SetStereotypeValueRequest;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * @author IK264250
 *
 */
public class SemanticIdReferenceCellManager extends StereotypePropertyCellManager {

	private static final String BOM_TABLE_TYPE = "BOMTableType";//$NON-NLS-1$
	private static final String PROPERTY_TABLE_TYPE = "PropertiesTableType";//$NON-NLS-1$

	private static final String OPERATION_TABLE_TYPE = "OperationsTableType";//$NON-NLS-1$
	private static final String SEMENTICID = "semanticId";//$NON-NLS-1$

	/**
	 * @see org.eclipse.papyrus.uml.nattable.manager.cell.StereotypePropertyCellManager#handles(java.lang.Object, java.lang.Object, org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager)
	 *
	 * @param columnElement
	 * @param rowElement
	 * @param tableManager
	 * @return
	 */
	@Override
	public boolean handles(Object columnElement, Object rowElement, INattableModelManager tableManager) {
		// TODO Auto-generated method stub
		boolean res = super.handles(columnElement, rowElement, tableManager);
		if (res) {
			final List<Object> umlObjects = organizeAndResolvedObjects(columnElement, rowElement, null);
			res = umlObjects.get(1) instanceof String && ((String) umlObjects.get(1)).endsWith(SEMENTICID);

		}
		return res;
	}

	/**
	 * @see org.eclipse.papyrus.uml.nattable.manager.cell.StereotypePropertyCellManager#doGetValue(java.lang.Object, java.lang.Object, org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager)
	 *
	 * @param columnElement
	 * @param rowElement
	 * @param tableManager
	 * @return
	 */
	@Override
	protected Object doGetValue(Object columnElement, Object rowElement, INattableModelManager tableManager) {
		// TODO Auto-generated method stub
		Object res = super.doGetValue(columnElement, rowElement, tableManager);
		return res;
	}

	/**
	 * @see org.eclipse.papyrus.uml.nattable.manager.cell.StereotypePropertyCellManager#getSetStringValueCommand(org.eclipse.emf.transaction.TransactionalEditingDomain, java.lang.Object, java.lang.Object, java.lang.String,
	 *      org.eclipse.papyrus.infra.ui.converter.AbstractStringValueConverter, org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager)
	 *
	 * @param domain
	 * @param columnElement
	 * @param rowElement
	 * @param newValue
	 * @param valueSolver
	 * @param tableManager
	 * @return
	 */
	@Override
	public Command getSetStringValueCommand(TransactionalEditingDomain domain, Object columnElement, Object rowElement, String newValue, AbstractStringValueConverter valueSolver, INattableModelManager tableManager) {
		// TODO Auto-generated method stub
		CompositeCommand cc = new CompositeCommand("SemanticId ref command"); //$NON-NLS-1$

		/* Reference creation */
		Model model = ((Element) rowElement).getModel();
		IElementType semanticidRef = ElementTypeRegistry.getInstance().getType(IAASElementTypes.REFERENCE_ID);
		final CreateElementRequest request1 = new CreateElementRequest(domain, model, semanticidRef);
		IElementEditService provider1 = ElementEditServiceUtils.getCommandProvider(model);
		ICommand command1 = provider1.getEditCommand(request1);
		cc.add(command1);

		// set key to reference and reference to property
		ICommand cmd = new AbstractTransactionalCommand(domain, "Set Key and reference in property", null) { //$NON-NLS-1$

			@Override
			protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
				CommandResult result = CommandResult.newOKCommandResult();

				org.eclipse.uml2.uml.Class reference = (org.eclipse.uml2.uml.Class) request1.getNewElement();
				IElementType key1 = ElementTypeRegistry.getInstance().getType(IAASElementTypes.KEY_ID);
				final CreateElementRequest request2 = new CreateElementRequest(domain, reference, key1);
				IElementEditService provider2 = ElementEditServiceUtils.getCommandProvider(model);
				ICommand command2 = provider2.getEditCommand(request2);
				command2.execute(new NullProgressMonitor(), null);


				org.eclipse.uml2.uml.Class key = (org.eclipse.uml2.uml.Class) request2.getNewElement();
				if (reference == null || key == null) {
					result = CommandResult.newErrorCommandResult("We can't set the value"); //$NON-NLS-1$
				} else {
					// set key to reference
					Reference steAppli = UMLUtil.getStereotypeApplication(reference, Reference.class);
					Stereotype referenceStereotype = UMLUtil.getStereotype(steAppli);
					Key keysteAppli = UMLUtil.getStereotypeApplication(key, Key.class);

					SetStereotypeValueRequest request = new SetStereotypeValueRequest(domain, referenceStereotype, reference, "key", Collections.singletonList(keysteAppli)); //$NON-NLS-1$
					IElementEditService provider3 = ElementEditServiceUtils.getCommandProvider(reference);
					ICommand command3 = provider3.getEditCommand(request);
					command3.execute(new NullProgressMonitor(), null);

					keysteAppli.setValue(newValue);


					String tableType = tableManager.getTable().getTableConfiguration().getType();
					// set reference to Model Property
					if (tableType.equals(BOM_TABLE_TYPE) == true) {
						Entity ModelProp = UMLUtil.getStereotypeApplication((Element) rowElement, Entity.class);
						Stereotype propertyStereotype = UMLUtil.getStereotype(ModelProp);
						Reference ste2Appli = UMLUtil.getStereotypeApplication(reference, Reference.class);
						SetStereotypeValueRequest request1 = new SetStereotypeValueRequest(domain, propertyStereotype, ((Element) rowElement), "semanticId", ste2Appli); //$NON-NLS-1$
						IElementEditService provider4 = ElementEditServiceUtils.getCommandProvider(reference);
						ICommand command4 = provider4.getEditCommand(request1);
						command4.execute(new NullProgressMonitor(), null);
					} else {
						if (tableType.equals(OPERATION_TABLE_TYPE) == true) {
							Operation ModelProp = UMLUtil.getStereotypeApplication((Element) rowElement, Operation.class);
							Stereotype propertyStereotype = UMLUtil.getStereotype(ModelProp);
							Reference ste2Appli = UMLUtil.getStereotypeApplication(reference, Reference.class);
							SetStereotypeValueRequest request1 = new SetStereotypeValueRequest(domain, propertyStereotype, ((Element) rowElement), "semanticId", ste2Appli); //$NON-NLS-1$
							IElementEditService provider4 = ElementEditServiceUtils.getCommandProvider(reference);
							ICommand command4 = provider4.getEditCommand(request1);
							command4.execute(new NullProgressMonitor(), null);
						} else {
							if (tableType.equals(PROPERTY_TABLE_TYPE) == true) {
								Property ModelProp = UMLUtil.getStereotypeApplication((Element) rowElement, Property.class);
								Stereotype propertyStereotype = UMLUtil.getStereotype(ModelProp);
								Reference ste2Appli = UMLUtil.getStereotypeApplication(reference, Reference.class);
								SetStereotypeValueRequest request1 = new SetStereotypeValueRequest(domain, propertyStereotype, ((Element) rowElement), "semanticId", ste2Appli); //$NON-NLS-1$
								IElementEditService provider4 = ElementEditServiceUtils.getCommandProvider(reference);
								ICommand command4 = provider4.getEditCommand(request1);
								command4.execute(new NullProgressMonitor(), null);
							}
						}
					}

					// set reference to property 2 method
					// prop.setSemanticId(ste2Appli);

				}
				return result;
			}

		};

		cc.add(cmd);
		return GMFtoEMFCommandWrapper.wrap(cc);
	}

	/**
	 * @see org.eclipse.papyrus.uml.nattable.manager.cell.UMLFeatureCellManager#getOrCreateStringValueConverterClass(java.util.Map, java.lang.String, org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager)
	 *
	 * @param existingConverters
	 * @param multiValueSeparator
	 * @param tableManager
	 * @return
	 */
	@Override
	public AbstractStringValueConverter getOrCreateStringValueConverterClass(Map<Class<? extends AbstractStringValueConverter>, AbstractStringValueConverter> existingConverters, String multiValueSeparator, INattableModelManager tableManager) {
		// probably nothing to do
		return super.getOrCreateStringValueConverterClass(existingConverters, multiValueSeparator, tableManager);
	}


}
