package org.eclipse.papyrus.aas.ui.modelExplorer;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.papyrus.emf.facet.efacet.core.IFacetManager;
import org.eclipse.papyrus.emf.facet.efacet.core.exception.DerivedTypedElementException;
import org.eclipse.papyrus.emf.facet.query.java.core.IJavaQuery2;
import org.eclipse.papyrus.emf.facet.query.java.core.IParameterValueList2;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.util.UMLUtil;

public class HideActivityQuery implements IJavaQuery2<Class, List<Element>> {
	@Override
	public List<Element> evaluate(final Class context,
			final IParameterValueList2 parameterValues,
			final IFacetManager facetManager)
			throws DerivedTypedElementException {

		List<Element> result = new ArrayList<>();
		for (Element element : context.getOwnedElements()) {
			if (!(element instanceof Activity) && !(element instanceof OpaqueBehavior)) {
				// it is not an Activity could be added to result
				if (element instanceof Operation) {
					if (isAASOperation(element)) {
						result.add(element);
					}

				} else {

					result.add(element);
				}
			}
		}

		return result;
	}

	// hide operation that are not stereotyped "AAS::Operation
	public static Boolean isAASOperation(Element c) {
		return UMLUtil.getStereotypeApplication(c, org.eclipse.papyrus.aas.Operation.class) != null;
	}
}
