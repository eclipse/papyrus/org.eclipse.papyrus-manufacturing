/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.aas.profile.ui.modelelement;

import java.util.List;

import org.eclipse.core.databinding.observable.IObservable;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.databinding.EObjectObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.papyrus.aas.profile.ui.Activator;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.papyrus.infra.services.edit.ui.databinding.PapyrusObservableValue;
import org.eclipse.papyrus.infra.tools.databinding.DelegatingObservable;
import org.eclipse.papyrus.uml.properties.databinding.StereotypePropertyObservableValue;
import org.eclipse.papyrus.uml.properties.modelelement.StereotypeModelElement;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Stereotype;

/**
 * @author AS247872
 *
 */
public class AssStereotypeModelElement extends StereotypeModelElement {

	/**
	 * Constructor.
	 *
	 * @param stereotypeApplication
	 * @param stereotype
	 * @param domain
	 */
	public AssStereotypeModelElement(EObject stereotypeApplication, Stereotype stereotype, EditingDomain domain) {
		super(stereotypeApplication, stereotype, domain);


		// TODO Auto-generated constructor stub
	}

	/**
	 * @see org.eclipse.papyrus.infra.properties.ui.modelelement.EMFModelElement#isFeatureEditable(java.lang.String)
	 *
	 * @param propertyPath
	 * @return
	 */
	@Override
	protected boolean isFeatureEditable(String propertyPath) {
		if (propertyPath.equals("idShort") || propertyPath.equals("submodel")) {
			return false;
		} else {
			return true;
		}
	}


	@Override
	public IObservable doGetObservable(String propertyPath) {

		if (propertyPath.equals("endpoint") || propertyPath.equals("endPoint") || propertyPath.equals("identification")
				|| (propertyPath.equals("value") && (stereotype.getName().equals("LangString") || stereotype.getName().equals("MultiLanguageProperty")))
				|| propertyPath.equals("description")) {

			FeaturePath featurePath = getFeaturePath(propertyPath);
			EStructuralFeature feature = getFeature(featurePath);
			Element baseElement = org.eclipse.uml2.uml.util.UMLUtil.getBaseElement(source);

			if (feature.getUpperBound() != 1) {

				List<?> wrappedList = (List<?>) source.eGet(feature);

				// this observable uses the SetStereotypeValueRequest
				// IObservable papy = new StereotypePropertyObservableList(wrappedList, domain, baseElement, feature, stereotype);

				EObjectObservableValue papy = new StereotypePropertyObservableValue(source, feature, domain, stereotype);
				IObservableValue wrap = (IObservableValue) DelegatingObservable.wrap(papy);
				return wrap;
			}


			// this is used for the DataType Editor (need an observable that can be casted to IObservabkeValue)
			TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(source);
			PapyrusObservableValue papyrusObservableValue = new PapyrusObservableValue(source, feature, editingDomain, GMFtoEMFCommandWrapper::wrap);

			return papyrusObservableValue;

		} else {
			return super.doGetObservable(propertyPath);
		}
	}


	@Override
	public IValidator getValidator(String propertyPath) {

		IValidator result = super.getValidator(propertyPath);

		if (propertyPath.equals("name")) {
			// the type is String, but we like to prohibit :
			// 1) strings with ".." to not specify a multiplicity in the replication text
			// 2) negative number : -2 is not allowed but -2a is OK
			result = CompositeValidator.of(result,
					new IValidator() {

						@Override
						public IStatus validate(Object value) {


							if (!((String) value).matches("[a-zA-Z]+\\\\w*")) {
								return new Status(IStatus.ERROR, Activator.PLUGIN_ID, "The name shall only feature letters, digits, underscore ('_'); starting with a small letter");
							}
							return Status.OK_STATUS;

						}

					});
		}

		return result;
	}
}




