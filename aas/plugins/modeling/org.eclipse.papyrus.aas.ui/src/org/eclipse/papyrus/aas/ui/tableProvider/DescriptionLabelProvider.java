/*****************************************************************************
 * Copyright (c) 2021 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ibtihel khemir (CEA LIST) ibtihel.khemir@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.aas.ui.tableProvider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.papyrus.aas.LangString;
import org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager;
import org.eclipse.papyrus.infra.nattable.provider.GenericCellLabelProvider;
import org.eclipse.papyrus.infra.nattable.utils.AxisUtils;
import org.eclipse.papyrus.infra.nattable.utils.LabelProviderCellContextElementWrapper;
import org.eclipse.papyrus.infra.nattable.utils.NattableConfigAttributes;
import org.eclipse.papyrus.infra.services.labelprovider.service.LabelProviderService;

/**
 * @author IK264250
 *
 */
public class DescriptionLabelProvider extends GenericCellLabelProvider {

	private static final String DESCRIPTION = "description"; //$NON-NLS-1$

	/**
	 * Constructor.
	 *
	 */
	public DescriptionLabelProvider() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see org.eclipse.papyrus.infra.nattable.provider.GenericCellLabelProvider#accept(java.lang.Object)
	 *
	 * @param element
	 * @return
	 */
	@Override
	public boolean accept(Object element) {
		if (element instanceof LabelProviderCellContextElementWrapper) {
			LabelProviderCellContextElementWrapper wrapper = (LabelProviderCellContextElementWrapper) element;
			int i1 = wrapper.getColumnIndex();

			INattableModelManager manager = wrapper.getConfigRegistry().getConfigAttribute(NattableConfigAttributes.NATTABLE_MODEL_MANAGER_CONFIG_ATTRIBUTE, org.eclipse.nebula.widgets.nattable.style.DisplayMode.NORMAL,
					NattableConfigAttributes.NATTABLE_MODEL_MANAGER_ID);

			Object axis = manager.getColumnElement(i1);
			String propID = AxisUtils.getPropertyId(axis);
			String tableType = manager.getTable().getTableConfiguration().getType();

			List<String> confTable = new ArrayList<>();
			confTable.add("PropertiesTableType"); //$NON-NLS-1$
			confTable.add("BOMTableType"); //$NON-NLS-1$
			confTable.add("OperationsTableType"); //$NON-NLS-1$
			for (String currentType : confTable) {
				if (propID != null && propID.endsWith(DESCRIPTION) && tableType.equals(currentType)) {
					return true;
				}
			}
		}
		return false;

	}

	@Override
	protected String getText(LabelProviderService service, Object value) {
		StringBuilder builder = new StringBuilder();
		if (value instanceof Collection<?>) {
			Collection<?> coll = (Collection<?>) value;
			Iterator<?> iter = coll.iterator();
			if (iter.hasNext()) {
				builder.append(((LangString) iter.next()).getValue());
			}
			// Object[] list = ((Collection<?>) value).toArray();
			// builder.append(((LangString) list[0]).getValue());
		}
		return builder.toString();



	}


}
