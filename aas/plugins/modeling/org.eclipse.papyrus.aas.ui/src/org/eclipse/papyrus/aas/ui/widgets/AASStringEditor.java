/*****************************************************************************
 * Copyright (c) 2023 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.aas.ui.widgets;

import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.papyrus.aas.profile.ui.Activator;
import org.eclipse.papyrus.aas.profile.ui.modelelement.CompositeValidator;
import org.eclipse.papyrus.infra.properties.ui.widgets.StringEditor;
import org.eclipse.swt.widgets.Composite;

/**
 * @author AS247872
 *
 */
public class AASStringEditor extends StringEditor {



	public AASStringEditor(Composite parent, int style) {
		super(parent, style);
		// TODO Auto-generated constructor stub
	}



	/**
	 * @see org.eclipse.papyrus.infra.properties.ui.widgets.AbstractPropertyEditor#getValidator()
	 *
	 * @return
	 */
	@Override
	public IValidator getValidator() {

		IValidator result = super.getValidator();


		result = CompositeValidator.of(result,
				new IValidator() {

					@Override
					public IStatus validate(Object value) {


						if (!(((String) value).matches("[a-zA-Z]+\\w*"))) {
							return new Status(IStatus.ERROR, Activator.PLUGIN_ID, "The name shall only feature letters, digits, underscore ('_'); starting with a small letter");
						}
						return Status.OK_STATUS;

					}

				});

		return result;

	}


}
