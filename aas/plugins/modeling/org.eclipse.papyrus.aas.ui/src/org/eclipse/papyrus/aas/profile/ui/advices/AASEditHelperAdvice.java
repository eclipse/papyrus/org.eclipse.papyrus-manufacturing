/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.aas.profile.ui.advices;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.EditHelperContext;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.commands.ConfigureElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.CreateElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.papyrus.aas.Asset;
import org.eclipse.papyrus.aas.AssetAdministrationShell;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * @author AS247872
 *
 */
public class AASEditHelperAdvice extends AbstractEditHelperAdvice {



	private IProgressMonitor progressMonitor;

	private IAdaptable info;

	public AASEditHelperAdvice() {
		super();
	}

	/**
	 * @see org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice#getAfterSetCommand(org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest)
	 *
	 * @param request
	 * @return
	 */


	/**
	 * @see org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice#getAfterEditCommand(org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest)
	 *
	 * @param request
	 * @return
	 */
	@Override
	public ICommand getAfterEditCommand(IEditCommandRequest request) {



		Object context = request.getEditHelperContext();
		// the edit is via Palette or ModelExplorer --> return the super command
		if (context instanceof EditHelperContext && ((EditHelperContext) context).getElementType() != null) {
			return super.getAfterEditCommand(request);
		}
		// it is a modification from a property view for example, put on the AAS Package
		else if (request.getElementsToEdit() != null && !request.getElementsToEdit().isEmpty()) {
			Object element = request.getElementsToEdit().get(0);
			if (element instanceof Element) {
				try {
					return getChangeContainerCommand((Element) element);
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return super.getAfterEditCommand(request);

	}


	protected ICommand getChangeContainerCommand(Element element) throws ExecutionException {

		Package container = getSpecificElementPackage(element.getModel(), "AAS::AssetAdministrationShell");

		CompositeCommand changeContainerCommand = new CompositeCommand("Change Container");
		IElementEditService serviceEdit = ElementEditServiceUtils.getCommandProvider(element);
		EObject newElement = null;
		if (container == null) {
			// create a Package to hold the new element
			IElementType packageelementType = AASElementTypesEnumerator.AAS_Package;
			CreateElementRequest createElementRequest = new CreateElementRequest(element.getModel(), packageelementType);
			CreateElementCommand command = new CreateElementCommand(createElementRequest);
			try {
				command.execute(progressMonitor, info);
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			newElement = command.getNewElement();

			if (newElement == null) {
				throw new ExecutionException("Element creation problem for " + packageelementType.getDisplayName() + ".");
			}

			container = (Package) newElement;
			if (packageelementType != null) {
				String packageName = "AASs";
				SetRequest setNameRequest = new SetRequest(container, UMLPackage.eINSTANCE.getNamedElement_Name(), packageName);
				ICommand setNameCommand = serviceEdit.getEditCommand(setNameRequest);
				// container.setName(packageName);
				changeContainerCommand.compose(setNameCommand);
			}
		}

		EList<PackageableElement> packagedElements = container.getPackagedElements();
		List<PackageableElement> list = new ArrayList<>();
		list.addAll(packagedElements);
		list.add((PackageableElement) element);


		SetRequest setContainerRequest = new SetRequest(container, UMLPackage.eINSTANCE.getPackage_PackagedElement(), list);
		ICommand setContainerCommand = serviceEdit.getEditCommand(setContainerRequest);
		changeContainerCommand.compose(setContainerCommand);

		// setContainerCommand.execute(progressMonitor, serviceEdit);

		return changeContainerCommand;
	}

	private static Package getSpecificElementPackage(Model model, String stereotypeName) {
		EList<Package> packages = model.getNestedPackages();
		if (packages != null && !packages.isEmpty()) {
			for (Package p : packages) {
				if (p.getAppliedStereotypes() != null && p.getAppliedStereotype(stereotypeName) != null) {
					return p;

				}
			}
		}
		return null;
	}
	// @Override
	// protected ICommand getBeforeConfigureCommand(ConfigureRequest request) {
	// return new ElementConfigurationCommand(request);
	// }

	@Override
	public ICommand getAfterConfigureCommand(ConfigureRequest request) {
		// TODO Auto-generated method stub
		EObject newElement = request.getElementToConfigure();
		EObject container = newElement.eContainer();

		// get the previous container to set the Asset of the AAS

		if (container != null && container instanceof Element) {

			return new ConfigureElementCommand(request) {
				@Override
				protected CommandResult doExecuteWithResult(IProgressMonitor progressMonitor, IAdaptable info) throws ExecutionException {

					Asset asset = UMLUtil.getStereotypeApplication((Element) container, Asset.class);
					AssetAdministrationShell aas = UMLUtil.getStereotypeApplication((Element) newElement, AssetAdministrationShell.class);
					if (asset != null && aas != null) {
						aas.setAsset(asset);
					}

					return CommandResult.newOKCommandResult(newElement);

				}
			};
		}
		return super.getAfterConfigureCommand(request);
	}

	// @Override
	// protected ICommand getBeforeConfigureCommand(ConfigureRequest request) {
	// return new ElementConfigurationCommand(request);
	// }
}
