/*****************************************************************************
 * Copyright (c) 2021 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.aas.ui.modelExplorer;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.papyrus.aas.AssetAdministrationShell;
import org.eclipse.papyrus.aas.Reference;
import org.eclipse.papyrus.emf.facet.efacet.core.IFacetManager;
import org.eclipse.papyrus.emf.facet.efacet.core.exception.DerivedTypedElementException;
import org.eclipse.papyrus.emf.facet.query.java.core.IJavaQuery2;
import org.eclipse.papyrus.emf.facet.query.java.core.IParameterValueList2;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.util.UMLUtil;

public class GetVisibleElementsQuery implements IJavaQuery2<Model, List<PackageableElement>> {


	@Override
	public List<PackageableElement> evaluate(final Model context,
			final IParameterValueList2 parameterValues,
			final IFacetManager facetManager)
			throws DerivedTypedElementException {

		List<PackageableElement> result = new ArrayList<>();
		for (Element element : context.getPackagedElements()) {

			if (element instanceof DataType) {
				result.add((PackageableElement) element);
			}
			if (element instanceof Class && UMLUtil.getStereotypeApplication(element, Reference.class) == null) {
				result.add((PackageableElement) element);
			} else if (element instanceof Package && UMLUtil.getStereotypeApplication(element, AssetAdministrationShell.class) == null) {
				result.add((PackageableElement) element);
			}


		}
		return result;
	}
}
