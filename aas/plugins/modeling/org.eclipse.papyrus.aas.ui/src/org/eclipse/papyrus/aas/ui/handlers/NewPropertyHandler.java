package org.eclipse.papyrus.aas.ui.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.papyrus.infra.emf.utils.EMFHelper;
import org.eclipse.papyrus.infra.nattable.handler.AbstractTableHandler;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.PlatformUI;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.NamedElement;

public class NewPropertyHandler extends AbstractTableHandler {

	/**
	 * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 *
	 * @param event
	 * @return
	 * @throws ExecutionException
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		NamedElement context = getSelectedElement();
		CompositeCommand cc = new CompositeCommand("Creat property from Table menu");
		if (false == context instanceof org.eclipse.uml2.uml.Property
				|| false == context.getOwner() instanceof Class) {
			return null;
		}
		org.eclipse.uml2.uml.Class clazz = (Class) context.getOwner();

		// Create the property
		TransactionalEditingDomain domain = getContextEditingDomain();
		IElementType elemtypProperty = ElementTypeRegistry.getInstance().getType("org.eclipse.papyrus.aAS.Property_Property");
		CreateElementRequest request = new CreateElementRequest(domain, clazz, elemtypProperty);
		IElementEditService provider = ElementEditServiceUtils.getCommandProvider(clazz);
		ICommand command = provider.getEditCommand(request);

		cc.add(command);


		domain.getCommandStack().execute(GMFtoEMFCommandWrapper.wrap(cc));
		return null;
	}

	/**
	 * To get the selected Element
	 *
	 * @return
	 *         the selected named element or <code>null</code>
	 */
	private NamedElement getSelectedElement() {
		final ISelectionService service = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService();
		ISelection tmp = service.getSelection();
		if (tmp instanceof IStructuredSelection) {
			Object current = ((IStructuredSelection) tmp).getFirstElement();
			EObject eobject = EMFHelper.getEObject(current);
			if (eobject instanceof NamedElement) {
				return (NamedElement) eobject;
			}
		}
		return null;
	}



	/**
	 * @see org.eclipse.papyrus.infra.nattable.handler.AbstractTableHandler#computeEnable(java.lang.Object)
	 *
	 * @param evaluationContext
	 * @return
	 */
	@Override
	protected boolean computeEnable(Object evaluationContext) {
		// TODO check selection is an UML::Operation
		return super.computeEnable(evaluationContext);
	}
}
