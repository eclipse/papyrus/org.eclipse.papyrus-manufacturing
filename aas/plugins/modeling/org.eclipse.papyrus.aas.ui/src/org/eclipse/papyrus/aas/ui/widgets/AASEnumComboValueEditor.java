/*****************************************************************************
 * Copyright (c) 2021 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.aas.ui.widgets;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.papyrus.infra.widgets.editors.EnumCombo;
import org.eclipse.papyrus.infra.widgets.providers.IStaticContentProvider;
import org.eclipse.swt.widgets.Composite;

/**
 * Value Editor specific to the Key.
 */
public class AASEnumComboValueEditor extends EnumCombo {

	public AASEnumComboValueEditor(Composite parent, int style, String label) {
		super(parent, style, label);
	}

	public AASEnumComboValueEditor(Composite parent, int style) {
		super(parent, style);
	}

	/**
	 * @see org.eclipse.papyrus.infra.widgets.editors.EnumRadio#setProviders(org.eclipse.papyrus.infra.widgets.providers.IStaticContentProvider, org.eclipse.jface.viewers.ILabelProvider)
	 *
	 * @param contentProvider
	 * @param labelProvider
	 */
	@Override
	public void setProviders(IStaticContentProvider contentProvider, ILabelProvider labelProvider) {
		super.setProviders(contentProvider, labelProvider);


	}




}
