/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Saadia Dhouib (CEA LIST) saadia.dhouib@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.aas.profile.ui.modelelement;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.core.databinding.observable.ChangeEvent;
import org.eclipse.core.databinding.observable.IChangeListener;
import org.eclipse.core.databinding.observable.IObservable;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.papyrus.aas.Asset;
import org.eclipse.papyrus.aas.AssetAdministrationShell;
import org.eclipse.papyrus.aas.DataElement;
import org.eclipse.papyrus.aas.Endpoint;
import org.eclipse.papyrus.aas.KeyElements;
import org.eclipse.papyrus.aas.KeyType;
import org.eclipse.papyrus.aas.Submodel;
import org.eclipse.papyrus.aas.ui.properties.IAASConstants;
import org.eclipse.papyrus.aas.ui.widgets.EndPointObservableValue;
import org.eclipse.papyrus.infra.widgets.providers.IStaticContentProvider;
import org.eclipse.papyrus.infra.widgets.providers.StaticContentProvider;
import org.eclipse.papyrus.uml.properties.modelelement.StereotypeModelElement;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * @author AS247872
 *
 */
public class AASExtModelElement extends StereotypeModelElement {

	public AASExtModelElement(EObject stereotypeApplication, Stereotype stereotype, EditingDomain domain) {
		super(stereotypeApplication, stereotype, domain);


		// TODO Auto-generated constructor stub
	}

	public class AASExtModelElementChangeListener implements IChangeListener {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void handleChange(ChangeEvent event) {
			try {
				if (dataSource instanceof DataElement) {
					ChangeListenerUtils.fireDataSourceChanged(dataSource);
				}
			} catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | RuntimeException e) {
				System.out.println(e.getMessage());
			}
		}
	}



	@Override
	public IStaticContentProvider getContentProvider(String propertyPath) {

		if (propertyPath.contains(IAASConstants.KEY_TYPE)) {

			List<KeyElements> values = KeyElements.VALUES;
			return new StaticContentProvider(values.toArray());

		} else if (propertyPath.contains(IAASConstants.KEY_IDTYPE)) {

			List<KeyType> values = KeyType.VALUES;
			return new StaticContentProvider(values.toArray());

		} else if (propertyPath.equals("newEndpoint") && (source instanceof org.eclipse.papyrus.aas.Property || source instanceof org.eclipse.papyrus.aas.File)) {
			// here we should add the endpoints of the Asset otherwise nothing.
			Element element = ((org.eclipse.papyrus.aas.DataElement) source).getBase_Property().getOwner();

			org.eclipse.papyrus.aas.Submodel aasSubmodel = UMLUtil.getStereotypeApplication(getSubmodel(element), Submodel.class);

			if (getAssetfromSubmodel(aasSubmodel) != null) {
				Asset asset = getAssetfromSubmodel(aasSubmodel);
				EList<Endpoint> list = asset.getEndpoint();
				List<String> stringlist = new ArrayList<>();
				stringlist.add("Please Select in the list bellow an endpoint from the Asset's endpoints");
				if (list != null && !list.isEmpty()) {
					for (Endpoint endpoint : list) {
						String endpointvalue = getLabelProvider("endpoint").getText(endpoint);
						stringlist.add(endpointvalue);
					}

					return new StaticContentProvider(stringlist.toArray());
				}
			}

		}
		return super.getContentProvider(propertyPath);
	}


	public Asset getAssetfromSubmodel(Submodel submodel) {
		if (submodel != null) {
			Element element = ((org.eclipse.papyrus.aas.DataElement) source).getBase_Property();
			List<Element> possibleAasContainers = element.getModel().allOwnedElements().stream()
					.filter(e -> (UMLUtil.getStereotypeApplication(e, AssetAdministrationShell.class) != null))
					.collect(Collectors.toList());
			if (possibleAasContainers != null && !possibleAasContainers.isEmpty()) {
				for (Element ele : possibleAasContainers) {
					AssetAdministrationShell ass = UMLUtil.getStereotypeApplication(ele, AssetAdministrationShell.class);
					if (ass != null && ass.getSubmodel().contains(submodel)) {
						return ass.getAsset();
					}
				}
			}
		}
		return null;
	}

	@Override
	public IObservable doGetObservable(String propertyPath) {
		IObservable observable = null;



		if (propertyPath.equals("newEndpoint")) {
			observable = new EndPointObservableValue(source, (TransactionalEditingDomain) domain);
			observable.addChangeListener(new AASExtModelElementChangeListener());
			return observable;
		}
		return super.doGetObservable(propertyPath);
	}


	@Override
	public boolean forceRefresh(String propertyPath) {
		return true;
	}



	/**
	 * @see org.eclipse.papyrus.infra.properties.ui.modelelement.EMFModelElement#isEditable(java.lang.String)
	 *
	 * @param propertyPath
	 * @return
	 */
	@Override
	public boolean isEditable(String propertyPath) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected boolean isFeatureEditable(String propertyPath) {
		if (propertyPath.equals("idShort") || propertyPath.equals("submodel")) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * @param element
	 * @return
	 */
	private static Element getSubmodel(Element element) {

		Element owner = element;
		if (isSubmodel(owner)) {
			return element;
		} else {
			while (!isSubmodel(owner)) {

				owner = owner.getOwner();
			}
		}
		return owner;
	}

	public static Boolean isSubmodel(Element c) {
		return UMLUtil.getStereotypeApplication(c, org.eclipse.papyrus.aas.Submodel.class) != null;
	}

}
