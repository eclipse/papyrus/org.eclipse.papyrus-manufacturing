/**
 * ****************************************************************************
 *   Copyright (c) 2023 CEA LIST.
 *  
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License 2.0
 *   which accompanies this distribution, and is available at
 *   https://www.eclipse.org/legal/epl-2.0/
 *  
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *    Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *  
 *  ****************************************************************************
 */
package io.shell.admin.aas._1._0;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Concept Descriptions Ref T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._1._0.ConceptDescriptionsRefT#getConceptDescriptionRef <em>Concept Description Ref</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._1._0._0Package#getConceptDescriptionsRefT()
 * @model extendedMetaData="name='conceptDescriptionsRef_t' kind='elementOnly'"
 * @generated
 */
public interface ConceptDescriptionsRefT extends EObject {
	/**
	 * Returns the value of the '<em><b>Concept Description Ref</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas._1._0.ReferenceT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Concept Description Ref</em>' containment reference list.
	 * @see io.shell.admin.aas._1._0._0Package#getConceptDescriptionsRefT_ConceptDescriptionRef()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='conceptDescriptionRef' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ReferenceT> getConceptDescriptionRef();

} // ConceptDescriptionsRefT
