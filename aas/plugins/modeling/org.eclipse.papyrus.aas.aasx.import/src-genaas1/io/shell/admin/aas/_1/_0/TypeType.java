/**
 * ****************************************************************************
 *   Copyright (c) 2023 CEA LIST.
 *  
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License 2.0
 *   which accompanies this distribution, and is available at
 *   https://www.eclipse.org/legal/epl-2.0/
 *  
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *    Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *  
 *  ****************************************************************************
 */
package io.shell.admin.aas._1._0;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Type Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see io.shell.admin.aas._1._0._0Package#getTypeType()
 * @model extendedMetaData="name='type_._type'"
 * @generated
 */
public enum TypeType implements Enumerator {
	/**
	 * The '<em><b>Global Reference</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GLOBAL_REFERENCE_VALUE
	 * @generated
	 * @ordered
	 */
	GLOBAL_REFERENCE(0, "GlobalReference", "GlobalReference"),

	/**
	 * The '<em><b>Concept Dictionary</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONCEPT_DICTIONARY_VALUE
	 * @generated
	 * @ordered
	 */
	CONCEPT_DICTIONARY(1, "ConceptDictionary", "ConceptDictionary"),

	/**
	 * The '<em><b>Access Permission Rule</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACCESS_PERMISSION_RULE_VALUE
	 * @generated
	 * @ordered
	 */
	ACCESS_PERMISSION_RULE(2, "AccessPermissionRule", "AccessPermissionRule"),

	/**
	 * The '<em><b>Data Element</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DATA_ELEMENT_VALUE
	 * @generated
	 * @ordered
	 */
	DATA_ELEMENT(3, "DataElement", "DataElement"),

	/**
	 * The '<em><b>View</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VIEW_VALUE
	 * @generated
	 * @ordered
	 */
	VIEW(4, "View", "View"),

	/**
	 * The '<em><b>Property</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PROPERTY_VALUE
	 * @generated
	 * @ordered
	 */
	PROPERTY(5, "Property", "Property"),

	/**
	 * The '<em><b>Submodel Element</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SUBMODEL_ELEMENT_VALUE
	 * @generated
	 * @ordered
	 */
	SUBMODEL_ELEMENT(6, "SubmodelElement", "SubmodelElement"),

	/**
	 * The '<em><b>File</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FILE_VALUE
	 * @generated
	 * @ordered
	 */
	FILE(7, "File", "File"),

	/**
	 * The '<em><b>Blob</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BLOB_VALUE
	 * @generated
	 * @ordered
	 */
	BLOB(8, "Blob", "Blob"),

	/**
	 * The '<em><b>Reference Element</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REFERENCE_ELEMENT_VALUE
	 * @generated
	 * @ordered
	 */
	REFERENCE_ELEMENT(9, "ReferenceElement", "ReferenceElement"),

	/**
	 * The '<em><b>Submodel Element Collection</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SUBMODEL_ELEMENT_COLLECTION_VALUE
	 * @generated
	 * @ordered
	 */
	SUBMODEL_ELEMENT_COLLECTION(10, "SubmodelElementCollection", "SubmodelElementCollection"),

	/**
	 * The '<em><b>Relation Ship Element</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RELATION_SHIP_ELEMENT_VALUE
	 * @generated
	 * @ordered
	 */
	RELATION_SHIP_ELEMENT(11, "RelationShipElement", "RelationShipElement"),

	/**
	 * The '<em><b>Event</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EVENT_VALUE
	 * @generated
	 * @ordered
	 */
	EVENT(12, "Event", "Event"),

	/**
	 * The '<em><b>Operation</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OPERATION_VALUE
	 * @generated
	 * @ordered
	 */
	OPERATION(13, "Operation", "Operation"),

	/**
	 * The '<em><b>Operation Variable</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OPERATION_VARIABLE_VALUE
	 * @generated
	 * @ordered
	 */
	OPERATION_VARIABLE(14, "OperationVariable", "OperationVariable"),

	/**
	 * The '<em><b>Asset Administration Shell</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSET_ADMINISTRATION_SHELL_VALUE
	 * @generated
	 * @ordered
	 */
	ASSET_ADMINISTRATION_SHELL(15, "AssetAdministrationShell", "AssetAdministrationShell"),

	/**
	 * The '<em><b>Submodel</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SUBMODEL_VALUE
	 * @generated
	 * @ordered
	 */
	SUBMODEL(16, "Submodel", "Submodel"),

	/**
	 * The '<em><b>Concept Description</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONCEPT_DESCRIPTION_VALUE
	 * @generated
	 * @ordered
	 */
	CONCEPT_DESCRIPTION(17, "ConceptDescription", "ConceptDescription"),

	/**
	 * The '<em><b>Asset</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSET_VALUE
	 * @generated
	 * @ordered
	 */
	ASSET(18, "Asset", "Asset");

	/**
	 * The '<em><b>Global Reference</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GLOBAL_REFERENCE
	 * @model name="GlobalReference"
	 * @generated
	 * @ordered
	 */
	public static final int GLOBAL_REFERENCE_VALUE = 0;

	/**
	 * The '<em><b>Concept Dictionary</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONCEPT_DICTIONARY
	 * @model name="ConceptDictionary"
	 * @generated
	 * @ordered
	 */
	public static final int CONCEPT_DICTIONARY_VALUE = 1;

	/**
	 * The '<em><b>Access Permission Rule</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACCESS_PERMISSION_RULE
	 * @model name="AccessPermissionRule"
	 * @generated
	 * @ordered
	 */
	public static final int ACCESS_PERMISSION_RULE_VALUE = 2;

	/**
	 * The '<em><b>Data Element</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DATA_ELEMENT
	 * @model name="DataElement"
	 * @generated
	 * @ordered
	 */
	public static final int DATA_ELEMENT_VALUE = 3;

	/**
	 * The '<em><b>View</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VIEW
	 * @model name="View"
	 * @generated
	 * @ordered
	 */
	public static final int VIEW_VALUE = 4;

	/**
	 * The '<em><b>Property</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PROPERTY
	 * @model name="Property"
	 * @generated
	 * @ordered
	 */
	public static final int PROPERTY_VALUE = 5;

	/**
	 * The '<em><b>Submodel Element</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SUBMODEL_ELEMENT
	 * @model name="SubmodelElement"
	 * @generated
	 * @ordered
	 */
	public static final int SUBMODEL_ELEMENT_VALUE = 6;

	/**
	 * The '<em><b>File</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FILE
	 * @model name="File"
	 * @generated
	 * @ordered
	 */
	public static final int FILE_VALUE = 7;

	/**
	 * The '<em><b>Blob</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BLOB
	 * @model name="Blob"
	 * @generated
	 * @ordered
	 */
	public static final int BLOB_VALUE = 8;

	/**
	 * The '<em><b>Reference Element</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REFERENCE_ELEMENT
	 * @model name="ReferenceElement"
	 * @generated
	 * @ordered
	 */
	public static final int REFERENCE_ELEMENT_VALUE = 9;

	/**
	 * The '<em><b>Submodel Element Collection</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SUBMODEL_ELEMENT_COLLECTION
	 * @model name="SubmodelElementCollection"
	 * @generated
	 * @ordered
	 */
	public static final int SUBMODEL_ELEMENT_COLLECTION_VALUE = 10;

	/**
	 * The '<em><b>Relation Ship Element</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RELATION_SHIP_ELEMENT
	 * @model name="RelationShipElement"
	 * @generated
	 * @ordered
	 */
	public static final int RELATION_SHIP_ELEMENT_VALUE = 11;

	/**
	 * The '<em><b>Event</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EVENT
	 * @model name="Event"
	 * @generated
	 * @ordered
	 */
	public static final int EVENT_VALUE = 12;

	/**
	 * The '<em><b>Operation</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OPERATION
	 * @model name="Operation"
	 * @generated
	 * @ordered
	 */
	public static final int OPERATION_VALUE = 13;

	/**
	 * The '<em><b>Operation Variable</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OPERATION_VARIABLE
	 * @model name="OperationVariable"
	 * @generated
	 * @ordered
	 */
	public static final int OPERATION_VARIABLE_VALUE = 14;

	/**
	 * The '<em><b>Asset Administration Shell</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSET_ADMINISTRATION_SHELL
	 * @model name="AssetAdministrationShell"
	 * @generated
	 * @ordered
	 */
	public static final int ASSET_ADMINISTRATION_SHELL_VALUE = 15;

	/**
	 * The '<em><b>Submodel</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SUBMODEL
	 * @model name="Submodel"
	 * @generated
	 * @ordered
	 */
	public static final int SUBMODEL_VALUE = 16;

	/**
	 * The '<em><b>Concept Description</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONCEPT_DESCRIPTION
	 * @model name="ConceptDescription"
	 * @generated
	 * @ordered
	 */
	public static final int CONCEPT_DESCRIPTION_VALUE = 17;

	/**
	 * The '<em><b>Asset</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSET
	 * @model name="Asset"
	 * @generated
	 * @ordered
	 */
	public static final int ASSET_VALUE = 18;

	/**
	 * An array of all the '<em><b>Type Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final TypeType[] VALUES_ARRAY =
		new TypeType[] {
			GLOBAL_REFERENCE,
			CONCEPT_DICTIONARY,
			ACCESS_PERMISSION_RULE,
			DATA_ELEMENT,
			VIEW,
			PROPERTY,
			SUBMODEL_ELEMENT,
			FILE,
			BLOB,
			REFERENCE_ELEMENT,
			SUBMODEL_ELEMENT_COLLECTION,
			RELATION_SHIP_ELEMENT,
			EVENT,
			OPERATION,
			OPERATION_VARIABLE,
			ASSET_ADMINISTRATION_SHELL,
			SUBMODEL,
			CONCEPT_DESCRIPTION,
			ASSET,
		};

	/**
	 * A public read-only list of all the '<em><b>Type Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<TypeType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Type Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static TypeType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			TypeType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Type Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static TypeType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			TypeType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Type Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static TypeType get(int value) {
		switch (value) {
			case GLOBAL_REFERENCE_VALUE: return GLOBAL_REFERENCE;
			case CONCEPT_DICTIONARY_VALUE: return CONCEPT_DICTIONARY;
			case ACCESS_PERMISSION_RULE_VALUE: return ACCESS_PERMISSION_RULE;
			case DATA_ELEMENT_VALUE: return DATA_ELEMENT;
			case VIEW_VALUE: return VIEW;
			case PROPERTY_VALUE: return PROPERTY;
			case SUBMODEL_ELEMENT_VALUE: return SUBMODEL_ELEMENT;
			case FILE_VALUE: return FILE;
			case BLOB_VALUE: return BLOB;
			case REFERENCE_ELEMENT_VALUE: return REFERENCE_ELEMENT;
			case SUBMODEL_ELEMENT_COLLECTION_VALUE: return SUBMODEL_ELEMENT_COLLECTION;
			case RELATION_SHIP_ELEMENT_VALUE: return RELATION_SHIP_ELEMENT;
			case EVENT_VALUE: return EVENT;
			case OPERATION_VALUE: return OPERATION;
			case OPERATION_VARIABLE_VALUE: return OPERATION_VARIABLE;
			case ASSET_ADMINISTRATION_SHELL_VALUE: return ASSET_ADMINISTRATION_SHELL;
			case SUBMODEL_VALUE: return SUBMODEL;
			case CONCEPT_DESCRIPTION_VALUE: return CONCEPT_DESCRIPTION;
			case ASSET_VALUE: return ASSET;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private TypeType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //TypeType
