/**
 * ****************************************************************************
 *   Copyright (c) 2023 CEA LIST.
 *  
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License 2.0
 *   which accompanies this distribution, and is available at
 *   https://www.eclipse.org/legal/epl-2.0/
 *  
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *    Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *  
 *  ****************************************************************************
 */
package io.shell.admin.iec61360._1._0;

import io.shell.admin.aas._1._0.LangStringsT;
import io.shell.admin.aas._1._0.ReferenceT;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Document Root</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.iec61360._1._0.DocumentRoot#getMixed <em>Mixed</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.DocumentRoot#getCode <em>Code</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.DocumentRoot#getDataType <em>Data Type</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.DocumentRoot#getDefinition <em>Definition</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.DocumentRoot#getPreferredName <em>Preferred Name</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.DocumentRoot#getShortName <em>Short Name</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.DocumentRoot#getSourceOfDefinition <em>Source Of Definition</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.DocumentRoot#getSymbol <em>Symbol</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.DocumentRoot#getUnit <em>Unit</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.DocumentRoot#getUnitId <em>Unit Id</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.DocumentRoot#getValueFormat <em>Value Format</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.DocumentRoot#getValueList <em>Value List</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.DocumentRoot#getValueType <em>Value Type</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.iec61360._1._0._0Package#getDocumentRoot()
 * @model extendedMetaData="name='' kind='mixed'"
 * @generated
 */
public interface DocumentRoot extends EObject {
	/**
	 * Returns the value of the '<em><b>Mixed</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mixed</em>' attribute list.
	 * @see io.shell.admin.iec61360._1._0._0Package#getDocumentRoot_Mixed()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='elementWildcard' name=':mixed'"
	 * @generated
	 */
	FeatureMap getMixed();

	/**
	 * Returns the value of the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>XMLNS Prefix Map</em>' map.
	 * @see io.shell.admin.iec61360._1._0._0Package#getDocumentRoot_XMLNSPrefixMap()
	 * @model mapType="org.eclipse.emf.ecore.EStringToStringMapEntry&lt;org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EString&gt;" transient="true"
	 *        extendedMetaData="kind='attribute' name='xmlns:prefix'"
	 * @generated
	 */
	EMap<String, String> getXMLNSPrefixMap();

	/**
	 * Returns the value of the '<em><b>XSI Schema Location</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>XSI Schema Location</em>' map.
	 * @see io.shell.admin.iec61360._1._0._0Package#getDocumentRoot_XSISchemaLocation()
	 * @model mapType="org.eclipse.emf.ecore.EStringToStringMapEntry&lt;org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EString&gt;" transient="true"
	 *        extendedMetaData="kind='attribute' name='xsi:schemaLocation'"
	 * @generated
	 */
	EMap<String, String> getXSISchemaLocation();

	/**
	 * Returns the value of the '<em><b>Code</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' containment reference.
	 * @see #setCode(CodeT)
	 * @see io.shell.admin.iec61360._1._0._0Package#getDocumentRoot_Code()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='code' namespace='##targetNamespace'"
	 * @generated
	 */
	CodeT getCode();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getCode <em>Code</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' containment reference.
	 * @see #getCode()
	 * @generated
	 */
	void setCode(CodeT value);

	/**
	 * Returns the value of the '<em><b>Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Type</em>' attribute.
	 * @see #setDataType(String)
	 * @see io.shell.admin.iec61360._1._0._0Package#getDocumentRoot_DataType()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='dataType' namespace='##targetNamespace'"
	 * @generated
	 */
	String getDataType();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getDataType <em>Data Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Type</em>' attribute.
	 * @see #getDataType()
	 * @generated
	 */
	void setDataType(String value);

	/**
	 * Returns the value of the '<em><b>Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definition</em>' containment reference.
	 * @see #setDefinition(LangStringsT)
	 * @see io.shell.admin.iec61360._1._0._0Package#getDocumentRoot_Definition()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='definition' namespace='##targetNamespace'"
	 * @generated
	 */
	LangStringsT getDefinition();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getDefinition <em>Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Definition</em>' containment reference.
	 * @see #getDefinition()
	 * @generated
	 */
	void setDefinition(LangStringsT value);

	/**
	 * Returns the value of the '<em><b>Preferred Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preferred Name</em>' containment reference.
	 * @see #setPreferredName(LangStringsT)
	 * @see io.shell.admin.iec61360._1._0._0Package#getDocumentRoot_PreferredName()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='preferredName' namespace='##targetNamespace'"
	 * @generated
	 */
	LangStringsT getPreferredName();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getPreferredName <em>Preferred Name</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preferred Name</em>' containment reference.
	 * @see #getPreferredName()
	 * @generated
	 */
	void setPreferredName(LangStringsT value);

	/**
	 * Returns the value of the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Short Name</em>' attribute.
	 * @see #setShortName(String)
	 * @see io.shell.admin.iec61360._1._0._0Package#getDocumentRoot_ShortName()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='shortName' namespace='##targetNamespace'"
	 * @generated
	 */
	String getShortName();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getShortName <em>Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Short Name</em>' attribute.
	 * @see #getShortName()
	 * @generated
	 */
	void setShortName(String value);

	/**
	 * Returns the value of the '<em><b>Source Of Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Of Definition</em>' containment reference.
	 * @see #setSourceOfDefinition(LangStringsT)
	 * @see io.shell.admin.iec61360._1._0._0Package#getDocumentRoot_SourceOfDefinition()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='sourceOfDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	LangStringsT getSourceOfDefinition();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getSourceOfDefinition <em>Source Of Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Of Definition</em>' containment reference.
	 * @see #getSourceOfDefinition()
	 * @generated
	 */
	void setSourceOfDefinition(LangStringsT value);

	/**
	 * Returns the value of the '<em><b>Symbol</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symbol</em>' attribute.
	 * @see #setSymbol(String)
	 * @see io.shell.admin.iec61360._1._0._0Package#getDocumentRoot_Symbol()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='symbol' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSymbol();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getSymbol <em>Symbol</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Symbol</em>' attribute.
	 * @see #getSymbol()
	 * @generated
	 */
	void setSymbol(String value);

	/**
	 * Returns the value of the '<em><b>Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit</em>' attribute.
	 * @see #setUnit(String)
	 * @see io.shell.admin.iec61360._1._0._0Package#getDocumentRoot_Unit()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unit' namespace='##targetNamespace'"
	 * @generated
	 */
	String getUnit();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getUnit <em>Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit</em>' attribute.
	 * @see #getUnit()
	 * @generated
	 */
	void setUnit(String value);

	/**
	 * Returns the value of the '<em><b>Unit Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit Id</em>' containment reference.
	 * @see #setUnitId(ReferenceT)
	 * @see io.shell.admin.iec61360._1._0._0Package#getDocumentRoot_UnitId()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unitId' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getUnitId();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getUnitId <em>Unit Id</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit Id</em>' containment reference.
	 * @see #getUnitId()
	 * @generated
	 */
	void setUnitId(ReferenceT value);

	/**
	 * Returns the value of the '<em><b>Value Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Format</em>' attribute.
	 * @see #setValueFormat(String)
	 * @see io.shell.admin.iec61360._1._0._0Package#getDocumentRoot_ValueFormat()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='valueFormat' namespace='##targetNamespace'"
	 * @generated
	 */
	String getValueFormat();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getValueFormat <em>Value Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Format</em>' attribute.
	 * @see #getValueFormat()
	 * @generated
	 */
	void setValueFormat(String value);

	/**
	 * Returns the value of the '<em><b>Value List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value List</em>' containment reference.
	 * @see #setValueList(ValueListT)
	 * @see io.shell.admin.iec61360._1._0._0Package#getDocumentRoot_ValueList()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='valueList' namespace='##targetNamespace'"
	 * @generated
	 */
	ValueListT getValueList();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getValueList <em>Value List</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value List</em>' containment reference.
	 * @see #getValueList()
	 * @generated
	 */
	void setValueList(ValueListT value);

	/**
	 * Returns the value of the '<em><b>Value Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Type</em>' containment reference.
	 * @see #setValueType(ValueListT)
	 * @see io.shell.admin.iec61360._1._0._0Package#getDocumentRoot_ValueType()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='valueType' namespace='##targetNamespace'"
	 * @generated
	 */
	ValueListT getValueType();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getValueType <em>Value Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Type</em>' containment reference.
	 * @see #getValueType()
	 * @generated
	 */
	void setValueType(ValueListT value);

} // DocumentRoot
