/**
 * ****************************************************************************
 *   Copyright (c) 2023 CEA LIST.
 *  
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License 2.0
 *   which accompanies this distribution, and is available at
 *   https://www.eclipse.org/legal/epl-2.0/
 *  
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *    Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *  
 *  ****************************************************************************
 */
package io.shell.admin.aas._1._0;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operation Variable T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._1._0.OperationVariableT#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._1._0._0Package#getOperationVariableT()
 * @model extendedMetaData="name='operationVariable_t' kind='elementOnly'"
 * @generated
 */
public interface OperationVariableT extends SubmodelElementAbstractT {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(SubmodelElementT)
	 * @see io.shell.admin.aas._1._0._0Package#getOperationVariableT_Value()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='value' namespace='##targetNamespace'"
	 * @generated
	 */
	SubmodelElementT getValue();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.OperationVariableT#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(SubmodelElementT value);

} // OperationVariableT
