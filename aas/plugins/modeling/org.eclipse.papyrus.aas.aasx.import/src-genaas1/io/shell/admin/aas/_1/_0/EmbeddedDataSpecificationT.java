/**
 * ****************************************************************************
 *   Copyright (c) 2023 CEA LIST.
 *  
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License 2.0
 *   which accompanies this distribution, and is available at
 *   https://www.eclipse.org/legal/epl-2.0/
 *  
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *    Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *  
 *  ****************************************************************************
 */
package io.shell.admin.aas._1._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Embedded Data Specification T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._1._0.EmbeddedDataSpecificationT#getHasDataSpecification <em>Has Data Specification</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.EmbeddedDataSpecificationT#getDataSpecificationContent <em>Data Specification Content</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._1._0._0Package#getEmbeddedDataSpecificationT()
 * @model extendedMetaData="name='embeddedDataSpecification_t' kind='elementOnly'"
 * @generated
 */
public interface EmbeddedDataSpecificationT extends EObject {
	/**
	 * Returns the value of the '<em><b>Has Data Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Data Specification</em>' containment reference.
	 * @see #setHasDataSpecification(ReferenceT)
	 * @see io.shell.admin.aas._1._0._0Package#getEmbeddedDataSpecificationT_HasDataSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='hasDataSpecification' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getHasDataSpecification();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.EmbeddedDataSpecificationT#getHasDataSpecification <em>Has Data Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Data Specification</em>' containment reference.
	 * @see #getHasDataSpecification()
	 * @generated
	 */
	void setHasDataSpecification(ReferenceT value);

	/**
	 * Returns the value of the '<em><b>Data Specification Content</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Specification Content</em>' containment reference.
	 * @see #setDataSpecificationContent(DataSpecificationContentT)
	 * @see io.shell.admin.aas._1._0._0Package#getEmbeddedDataSpecificationT_DataSpecificationContent()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='dataSpecificationContent' namespace='##targetNamespace'"
	 * @generated
	 */
	DataSpecificationContentT getDataSpecificationContent();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.EmbeddedDataSpecificationT#getDataSpecificationContent <em>Data Specification Content</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Specification Content</em>' containment reference.
	 * @see #getDataSpecificationContent()
	 * @generated
	 */
	void setDataSpecificationContent(DataSpecificationContentT value);

} // EmbeddedDataSpecificationT
