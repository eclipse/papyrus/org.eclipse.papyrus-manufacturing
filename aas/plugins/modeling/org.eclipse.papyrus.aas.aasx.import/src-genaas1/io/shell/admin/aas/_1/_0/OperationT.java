/**
 * ****************************************************************************
 *   Copyright (c) 2023 CEA LIST.
 *  
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License 2.0
 *   which accompanies this distribution, and is available at
 *   https://www.eclipse.org/legal/epl-2.0/
 *  
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *    Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *  
 *  ****************************************************************************
 */
package io.shell.admin.aas._1._0;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operation T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._1._0.OperationT#getIn <em>In</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.OperationT#getOut <em>Out</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._1._0._0Package#getOperationT()
 * @model extendedMetaData="name='operation_t' kind='elementOnly'"
 * @generated
 */
public interface OperationT extends SubmodelElementAbstractT {
	/**
	 * Returns the value of the '<em><b>In</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas._1._0.OperationVariableT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In</em>' containment reference list.
	 * @see io.shell.admin.aas._1._0._0Package#getOperationT_In()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='in' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OperationVariableT> getIn();

	/**
	 * Returns the value of the '<em><b>Out</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas._1._0.OperationVariableT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Out</em>' containment reference list.
	 * @see io.shell.admin.aas._1._0._0Package#getOperationT_Out()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='out' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OperationVariableT> getOut();

} // OperationT
