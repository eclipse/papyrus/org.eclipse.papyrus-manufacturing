/**
 * ****************************************************************************
 *   Copyright (c) 2023 CEA LIST.
 *  
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License 2.0
 *   which accompanies this distribution, and is available at
 *   https://www.eclipse.org/legal/epl-2.0/
 *  
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *    Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *  
 *  ****************************************************************************
 */
package io.shell.admin.aas._1._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Qualifier T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._1._0.QualifierT#getSemanticId <em>Semantic Id</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.QualifierT#getQualifierType <em>Qualifier Type</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.QualifierT#getQualifierValue <em>Qualifier Value</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.QualifierT#getQualifierValueId <em>Qualifier Value Id</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._1._0._0Package#getQualifierT()
 * @model extendedMetaData="name='qualifier_t' kind='elementOnly'"
 * @generated
 */
public interface QualifierT extends EObject {
	/**
	 * Returns the value of the '<em><b>Semantic Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semantic Id</em>' containment reference.
	 * @see #setSemanticId(SemanticIdT)
	 * @see io.shell.admin.aas._1._0._0Package#getQualifierT_SemanticId()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='semanticId' namespace='##targetNamespace'"
	 * @generated
	 */
	SemanticIdT getSemanticId();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.QualifierT#getSemanticId <em>Semantic Id</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Semantic Id</em>' containment reference.
	 * @see #getSemanticId()
	 * @generated
	 */
	void setSemanticId(SemanticIdT value);

	/**
	 * Returns the value of the '<em><b>Qualifier Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Qualifier Type</em>' attribute.
	 * @see #setQualifierType(String)
	 * @see io.shell.admin.aas._1._0._0Package#getQualifierT_QualifierType()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='qualifierType' namespace='##targetNamespace'"
	 * @generated
	 */
	String getQualifierType();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.QualifierT#getQualifierType <em>Qualifier Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Qualifier Type</em>' attribute.
	 * @see #getQualifierType()
	 * @generated
	 */
	void setQualifierType(String value);

	/**
	 * Returns the value of the '<em><b>Qualifier Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Qualifier Value</em>' attribute.
	 * @see #setQualifierValue(String)
	 * @see io.shell.admin.aas._1._0._0Package#getQualifierT_QualifierValue()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='qualifierValue' namespace='##targetNamespace'"
	 * @generated
	 */
	String getQualifierValue();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.QualifierT#getQualifierValue <em>Qualifier Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Qualifier Value</em>' attribute.
	 * @see #getQualifierValue()
	 * @generated
	 */
	void setQualifierValue(String value);

	/**
	 * Returns the value of the '<em><b>Qualifier Value Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Qualifier Value Id</em>' containment reference.
	 * @see #setQualifierValueId(ReferenceT)
	 * @see io.shell.admin.aas._1._0._0Package#getQualifierT_QualifierValueId()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='qualifierValueId' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getQualifierValueId();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.QualifierT#getQualifierValueId <em>Qualifier Value Id</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Qualifier Value Id</em>' containment reference.
	 * @see #getQualifierValueId()
	 * @generated
	 */
	void setQualifierValueId(ReferenceT value);

} // QualifierT
