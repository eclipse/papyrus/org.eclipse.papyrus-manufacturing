/**
 * ****************************************************************************
 *   Copyright (c) 2023 CEA LIST.
 *  
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License 2.0
 *   which accompanies this distribution, and is available at
 *   https://www.eclipse.org/legal/epl-2.0/
 *  
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *    Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *  
 *  ****************************************************************************
 */
package io.shell.admin.iec61360._1._0.impl;

import io.shell.admin.iec61360._1._0.CodeT;
import io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T;
import io.shell.admin.iec61360._1._0.DocumentRoot;
import io.shell.admin.iec61360._1._0.ValueListT;
import io.shell.admin.iec61360._1._0._0Factory;
import io.shell.admin.iec61360._1._0._0Package;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class _0PackageImpl extends EPackageImpl implements _0Package {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass codeTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataSpecificationIEC61630TEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valueListTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass documentRootEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see io.shell.admin.iec61360._1._0._0Package#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private _0PackageImpl() {
		super(eNS_URI, _0Factory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link _0Package#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static _0Package init() {
		if (isInited) return (_0Package)EPackage.Registry.INSTANCE.getEPackage(_0Package.eNS_URI);

		// Obtain or create and register package
		Object registered_0Package = EPackage.Registry.INSTANCE.get(eNS_URI);
		_0PackageImpl the_0Package = registered_0Package instanceof _0PackageImpl ? (_0PackageImpl)registered_0Package : new _0PackageImpl();

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(io.shell.admin.aas._1._0._0Package.eNS_URI);
		io.shell.admin.aas._1._0.impl._0PackageImpl the_0Package_1 = (io.shell.admin.aas._1._0.impl._0PackageImpl)(registeredPackage instanceof io.shell.admin.aas._1._0.impl._0PackageImpl ? registeredPackage : io.shell.admin.aas._1._0._0Package.eINSTANCE);

		// Create package meta-data objects
		the_0Package.createPackageContents();
		the_0Package_1.createPackageContents();

		// Initialize created meta-data
		the_0Package.initializePackageContents();
		the_0Package_1.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		the_0Package.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(_0Package.eNS_URI, the_0Package);
		return the_0Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCodeT() {
		return codeTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataSpecificationIEC61630T() {
		return dataSpecificationIEC61630TEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataSpecificationIEC61630T_PreferredName() {
		return (EReference)dataSpecificationIEC61630TEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataSpecificationIEC61630T_ShortName() {
		return (EAttribute)dataSpecificationIEC61630TEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataSpecificationIEC61630T_Unit() {
		return (EAttribute)dataSpecificationIEC61630TEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataSpecificationIEC61630T_UnitId() {
		return (EReference)dataSpecificationIEC61630TEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataSpecificationIEC61630T_ValueFormat() {
		return (EAttribute)dataSpecificationIEC61630TEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataSpecificationIEC61630T_SourceOfDefinition() {
		return (EReference)dataSpecificationIEC61630TEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataSpecificationIEC61630T_Symbol() {
		return (EAttribute)dataSpecificationIEC61630TEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataSpecificationIEC61630T_DataType() {
		return (EAttribute)dataSpecificationIEC61630TEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataSpecificationIEC61630T_Definition() {
		return (EReference)dataSpecificationIEC61630TEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataSpecificationIEC61630T_ValueList() {
		return (EReference)dataSpecificationIEC61630TEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataSpecificationIEC61630T_Code() {
		return (EReference)dataSpecificationIEC61630TEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getValueListT() {
		return valueListTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDocumentRoot() {
		return documentRootEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDocumentRoot_Mixed() {
		return (EAttribute)documentRootEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_XMLNSPrefixMap() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_XSISchemaLocation() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_Code() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDocumentRoot_DataType() {
		return (EAttribute)documentRootEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_Definition() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_PreferredName() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDocumentRoot_ShortName() {
		return (EAttribute)documentRootEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_SourceOfDefinition() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDocumentRoot_Symbol() {
		return (EAttribute)documentRootEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDocumentRoot_Unit() {
		return (EAttribute)documentRootEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_UnitId() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDocumentRoot_ValueFormat() {
		return (EAttribute)documentRootEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ValueList() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ValueType() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public _0Factory get_0Factory() {
		return (_0Factory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		codeTEClass = createEClass(CODE_T);

		dataSpecificationIEC61630TEClass = createEClass(DATA_SPECIFICATION_IEC61630T);
		createEReference(dataSpecificationIEC61630TEClass, DATA_SPECIFICATION_IEC61630T__PREFERRED_NAME);
		createEAttribute(dataSpecificationIEC61630TEClass, DATA_SPECIFICATION_IEC61630T__SHORT_NAME);
		createEAttribute(dataSpecificationIEC61630TEClass, DATA_SPECIFICATION_IEC61630T__UNIT);
		createEReference(dataSpecificationIEC61630TEClass, DATA_SPECIFICATION_IEC61630T__UNIT_ID);
		createEAttribute(dataSpecificationIEC61630TEClass, DATA_SPECIFICATION_IEC61630T__VALUE_FORMAT);
		createEReference(dataSpecificationIEC61630TEClass, DATA_SPECIFICATION_IEC61630T__SOURCE_OF_DEFINITION);
		createEAttribute(dataSpecificationIEC61630TEClass, DATA_SPECIFICATION_IEC61630T__SYMBOL);
		createEAttribute(dataSpecificationIEC61630TEClass, DATA_SPECIFICATION_IEC61630T__DATA_TYPE);
		createEReference(dataSpecificationIEC61630TEClass, DATA_SPECIFICATION_IEC61630T__DEFINITION);
		createEReference(dataSpecificationIEC61630TEClass, DATA_SPECIFICATION_IEC61630T__VALUE_LIST);
		createEReference(dataSpecificationIEC61630TEClass, DATA_SPECIFICATION_IEC61630T__CODE);

		valueListTEClass = createEClass(VALUE_LIST_T);

		documentRootEClass = createEClass(DOCUMENT_ROOT);
		createEAttribute(documentRootEClass, DOCUMENT_ROOT__MIXED);
		createEReference(documentRootEClass, DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
		createEReference(documentRootEClass, DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
		createEReference(documentRootEClass, DOCUMENT_ROOT__CODE);
		createEAttribute(documentRootEClass, DOCUMENT_ROOT__DATA_TYPE);
		createEReference(documentRootEClass, DOCUMENT_ROOT__DEFINITION);
		createEReference(documentRootEClass, DOCUMENT_ROOT__PREFERRED_NAME);
		createEAttribute(documentRootEClass, DOCUMENT_ROOT__SHORT_NAME);
		createEReference(documentRootEClass, DOCUMENT_ROOT__SOURCE_OF_DEFINITION);
		createEAttribute(documentRootEClass, DOCUMENT_ROOT__SYMBOL);
		createEAttribute(documentRootEClass, DOCUMENT_ROOT__UNIT);
		createEReference(documentRootEClass, DOCUMENT_ROOT__UNIT_ID);
		createEAttribute(documentRootEClass, DOCUMENT_ROOT__VALUE_FORMAT);
		createEReference(documentRootEClass, DOCUMENT_ROOT__VALUE_LIST);
		createEReference(documentRootEClass, DOCUMENT_ROOT__VALUE_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		io.shell.admin.aas._1._0._0Package the_0Package_1 = (io.shell.admin.aas._1._0._0Package)EPackage.Registry.INSTANCE.getEPackage(io.shell.admin.aas._1._0._0Package.eNS_URI);
		XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(codeTEClass, CodeT.class, "CodeT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dataSpecificationIEC61630TEClass, DataSpecificationIEC61630T.class, "DataSpecificationIEC61630T", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataSpecificationIEC61630T_PreferredName(), the_0Package_1.getLangStringsT(), null, "preferredName", null, 1, 1, DataSpecificationIEC61630T.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDataSpecificationIEC61630T_ShortName(), theXMLTypePackage.getString(), "shortName", null, 0, 1, DataSpecificationIEC61630T.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDataSpecificationIEC61630T_Unit(), theXMLTypePackage.getString(), "unit", null, 0, 1, DataSpecificationIEC61630T.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataSpecificationIEC61630T_UnitId(), the_0Package_1.getReferenceT(), null, "unitId", null, 0, 1, DataSpecificationIEC61630T.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDataSpecificationIEC61630T_ValueFormat(), theXMLTypePackage.getString(), "valueFormat", null, 0, 1, DataSpecificationIEC61630T.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataSpecificationIEC61630T_SourceOfDefinition(), the_0Package_1.getLangStringsT(), null, "sourceOfDefinition", null, 0, 1, DataSpecificationIEC61630T.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDataSpecificationIEC61630T_Symbol(), theXMLTypePackage.getString(), "symbol", null, 0, 1, DataSpecificationIEC61630T.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDataSpecificationIEC61630T_DataType(), theXMLTypePackage.getString(), "dataType", null, 0, 1, DataSpecificationIEC61630T.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataSpecificationIEC61630T_Definition(), the_0Package_1.getLangStringsT(), null, "definition", null, 0, 1, DataSpecificationIEC61630T.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataSpecificationIEC61630T_ValueList(), this.getValueListT(), null, "valueList", null, 0, 1, DataSpecificationIEC61630T.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataSpecificationIEC61630T_Code(), this.getCodeT(), null, "code", null, 0, 1, DataSpecificationIEC61630T.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(valueListTEClass, ValueListT.class, "ValueListT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(documentRootEClass, DocumentRoot.class, "DocumentRoot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDocumentRoot_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, null, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_XMLNSPrefixMap(), ecorePackage.getEStringToStringMapEntry(), null, "xMLNSPrefixMap", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_XSISchemaLocation(), ecorePackage.getEStringToStringMapEntry(), null, "xSISchemaLocation", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_Code(), this.getCodeT(), null, "code", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getDocumentRoot_DataType(), theXMLTypePackage.getString(), "dataType", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_Definition(), the_0Package_1.getLangStringsT(), null, "definition", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_PreferredName(), the_0Package_1.getLangStringsT(), null, "preferredName", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getDocumentRoot_ShortName(), theXMLTypePackage.getString(), "shortName", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_SourceOfDefinition(), the_0Package_1.getLangStringsT(), null, "sourceOfDefinition", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getDocumentRoot_Symbol(), theXMLTypePackage.getString(), "symbol", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getDocumentRoot_Unit(), theXMLTypePackage.getString(), "unit", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_UnitId(), the_0Package_1.getReferenceT(), null, "unitId", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getDocumentRoot_ValueFormat(), theXMLTypePackage.getString(), "valueFormat", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_ValueList(), this.getValueListT(), null, "valueList", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_ValueType(), this.getValueListT(), null, "valueType", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http:///org/eclipse/emf/ecore/util/ExtendedMetaData
		createExtendedMetaDataAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createExtendedMetaDataAnnotations() {
		String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";
		addAnnotation
		  (codeTEClass,
		   source,
		   new String[] {
			   "name", "code_t",
			   "kind", "empty"
		   });
		addAnnotation
		  (dataSpecificationIEC61630TEClass,
		   source,
		   new String[] {
			   "name", "dataSpecificationIEC61630_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getDataSpecificationIEC61630T_PreferredName(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "preferredName",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDataSpecificationIEC61630T_ShortName(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "shortName",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDataSpecificationIEC61630T_Unit(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "unit",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDataSpecificationIEC61630T_UnitId(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "unitId",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDataSpecificationIEC61630T_ValueFormat(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "valueFormat",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDataSpecificationIEC61630T_SourceOfDefinition(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "sourceOfDefinition",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDataSpecificationIEC61630T_Symbol(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "symbol",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDataSpecificationIEC61630T_DataType(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "dataType",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDataSpecificationIEC61630T_Definition(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "definition",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDataSpecificationIEC61630T_ValueList(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "valueList",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDataSpecificationIEC61630T_Code(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "code",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (valueListTEClass,
		   source,
		   new String[] {
			   "name", "valueList_t",
			   "kind", "empty"
		   });
		addAnnotation
		  (documentRootEClass,
		   source,
		   new String[] {
			   "name", "",
			   "kind", "mixed"
		   });
		addAnnotation
		  (getDocumentRoot_Mixed(),
		   source,
		   new String[] {
			   "kind", "elementWildcard",
			   "name", ":mixed"
		   });
		addAnnotation
		  (getDocumentRoot_XMLNSPrefixMap(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "xmlns:prefix"
		   });
		addAnnotation
		  (getDocumentRoot_XSISchemaLocation(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "xsi:schemaLocation"
		   });
		addAnnotation
		  (getDocumentRoot_Code(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "code",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDocumentRoot_DataType(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "dataType",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDocumentRoot_Definition(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "definition",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDocumentRoot_PreferredName(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "preferredName",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDocumentRoot_ShortName(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "shortName",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDocumentRoot_SourceOfDefinition(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "sourceOfDefinition",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDocumentRoot_Symbol(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "symbol",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDocumentRoot_Unit(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "unit",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDocumentRoot_UnitId(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "unitId",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDocumentRoot_ValueFormat(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "valueFormat",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDocumentRoot_ValueList(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "valueList",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDocumentRoot_ValueType(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "valueType",
			   "namespace", "##targetNamespace"
		   });
	}

} //_0PackageImpl
