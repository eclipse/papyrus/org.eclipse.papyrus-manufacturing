/**
 * ****************************************************************************
 *   Copyright (c) 2023 CEA LIST.
 *  
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License 2.0
 *   which accompanies this distribution, and is available at
 *   https://www.eclipse.org/legal/epl-2.0/
 *  
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *    Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *  
 *  ****************************************************************************
 */
package io.shell.admin.aas._1._0;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event T</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see io.shell.admin.aas._1._0._0Package#getEventT()
 * @model extendedMetaData="name='event_t' kind='elementOnly'"
 * @generated
 */
public interface EventT extends SubmodelElementAbstractT {
} // EventT
