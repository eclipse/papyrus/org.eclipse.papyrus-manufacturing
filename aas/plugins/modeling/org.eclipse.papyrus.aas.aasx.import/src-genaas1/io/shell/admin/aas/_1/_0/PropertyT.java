/**
 * ****************************************************************************
 *   Copyright (c) 2023 CEA LIST.
 *  
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License 2.0
 *   which accompanies this distribution, and is available at
 *   https://www.eclipse.org/legal/epl-2.0/
 *  
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *    Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *  
 *  ****************************************************************************
 */
package io.shell.admin.aas._1._0;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Property T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._1._0.PropertyT#getValueType <em>Value Type</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.PropertyT#getValue <em>Value</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.PropertyT#getValueId <em>Value Id</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._1._0._0Package#getPropertyT()
 * @model extendedMetaData="name='property_t' kind='elementOnly'"
 * @generated
 */
public interface PropertyT extends SubmodelElementAbstractT {
	/**
	 * Returns the value of the '<em><b>Value Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Type</em>' attribute.
	 * @see #setValueType(String)
	 * @see io.shell.admin.aas._1._0._0Package#getPropertyT_ValueType()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='valueType' namespace='##targetNamespace'"
	 * @generated
	 */
	String getValueType();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.PropertyT#getValueType <em>Value Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Type</em>' attribute.
	 * @see #getValueType()
	 * @generated
	 */
	void setValueType(String value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(PropertyValueTypeT)
	 * @see io.shell.admin.aas._1._0._0Package#getPropertyT_Value()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='value' namespace='##targetNamespace'"
	 * @generated
	 */
	PropertyValueTypeT getValue();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.PropertyT#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(PropertyValueTypeT value);

	/**
	 * Returns the value of the '<em><b>Value Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Id</em>' containment reference.
	 * @see #setValueId(ReferenceT)
	 * @see io.shell.admin.aas._1._0._0Package#getPropertyT_ValueId()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='valueId' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getValueId();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.PropertyT#getValueId <em>Value Id</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Id</em>' containment reference.
	 * @see #getValueId()
	 * @generated
	 */
	void setValueId(ReferenceT value);

} // PropertyT
