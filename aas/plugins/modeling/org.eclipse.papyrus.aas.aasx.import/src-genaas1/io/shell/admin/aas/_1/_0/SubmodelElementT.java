/**
 * ****************************************************************************
 *   Copyright (c) 2023 CEA LIST.
 *  
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License 2.0
 *   which accompanies this distribution, and is available at
 *   https://www.eclipse.org/legal/epl-2.0/
 *  
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *    Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *  
 *  ****************************************************************************
 */
package io.shell.admin.aas._1._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Submodel Element T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._1._0.SubmodelElementT#getProperty <em>Property</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.SubmodelElementT#getFile <em>File</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.SubmodelElementT#getBlob <em>Blob</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.SubmodelElementT#getReferenceElement <em>Reference Element</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.SubmodelElementT#getSubmodelElementCollection <em>Submodel Element Collection</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.SubmodelElementT#getRelationshipElement <em>Relationship Element</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.SubmodelElementT#getOperation <em>Operation</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.SubmodelElementT#getOperationVariable <em>Operation Variable</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.SubmodelElementT#getEvent <em>Event</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._1._0._0Package#getSubmodelElementT()
 * @model extendedMetaData="name='submodelElement_t' kind='elementOnly'"
 * @generated
 */
public interface SubmodelElementT extends EObject {
	/**
	 * Returns the value of the '<em><b>Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property</em>' containment reference.
	 * @see #setProperty(PropertyT)
	 * @see io.shell.admin.aas._1._0._0Package#getSubmodelElementT_Property()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='property' namespace='##targetNamespace'"
	 * @generated
	 */
	PropertyT getProperty();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.SubmodelElementT#getProperty <em>Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property</em>' containment reference.
	 * @see #getProperty()
	 * @generated
	 */
	void setProperty(PropertyT value);

	/**
	 * Returns the value of the '<em><b>File</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>File</em>' containment reference.
	 * @see #setFile(FileT)
	 * @see io.shell.admin.aas._1._0._0Package#getSubmodelElementT_File()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='file' namespace='##targetNamespace'"
	 * @generated
	 */
	FileT getFile();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.SubmodelElementT#getFile <em>File</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>File</em>' containment reference.
	 * @see #getFile()
	 * @generated
	 */
	void setFile(FileT value);

	/**
	 * Returns the value of the '<em><b>Blob</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Blob</em>' containment reference.
	 * @see #setBlob(BlobT)
	 * @see io.shell.admin.aas._1._0._0Package#getSubmodelElementT_Blob()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='blob' namespace='##targetNamespace'"
	 * @generated
	 */
	BlobT getBlob();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.SubmodelElementT#getBlob <em>Blob</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Blob</em>' containment reference.
	 * @see #getBlob()
	 * @generated
	 */
	void setBlob(BlobT value);

	/**
	 * Returns the value of the '<em><b>Reference Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference Element</em>' containment reference.
	 * @see #setReferenceElement(ReferenceElementT)
	 * @see io.shell.admin.aas._1._0._0Package#getSubmodelElementT_ReferenceElement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='referenceElement' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceElementT getReferenceElement();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.SubmodelElementT#getReferenceElement <em>Reference Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference Element</em>' containment reference.
	 * @see #getReferenceElement()
	 * @generated
	 */
	void setReferenceElement(ReferenceElementT value);

	/**
	 * Returns the value of the '<em><b>Submodel Element Collection</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Submodel Element Collection</em>' containment reference.
	 * @see #setSubmodelElementCollection(SubmodelElementCollectionT)
	 * @see io.shell.admin.aas._1._0._0Package#getSubmodelElementT_SubmodelElementCollection()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='submodelElementCollection' namespace='##targetNamespace'"
	 * @generated
	 */
	SubmodelElementCollectionT getSubmodelElementCollection();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.SubmodelElementT#getSubmodelElementCollection <em>Submodel Element Collection</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Submodel Element Collection</em>' containment reference.
	 * @see #getSubmodelElementCollection()
	 * @generated
	 */
	void setSubmodelElementCollection(SubmodelElementCollectionT value);

	/**
	 * Returns the value of the '<em><b>Relationship Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relationship Element</em>' containment reference.
	 * @see #setRelationshipElement(RelationshipElementT)
	 * @see io.shell.admin.aas._1._0._0Package#getSubmodelElementT_RelationshipElement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='relationshipElement' namespace='##targetNamespace'"
	 * @generated
	 */
	RelationshipElementT getRelationshipElement();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.SubmodelElementT#getRelationshipElement <em>Relationship Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relationship Element</em>' containment reference.
	 * @see #getRelationshipElement()
	 * @generated
	 */
	void setRelationshipElement(RelationshipElementT value);

	/**
	 * Returns the value of the '<em><b>Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' containment reference.
	 * @see #setOperation(OperationT)
	 * @see io.shell.admin.aas._1._0._0Package#getSubmodelElementT_Operation()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='operation' namespace='##targetNamespace'"
	 * @generated
	 */
	OperationT getOperation();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.SubmodelElementT#getOperation <em>Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operation</em>' containment reference.
	 * @see #getOperation()
	 * @generated
	 */
	void setOperation(OperationT value);

	/**
	 * Returns the value of the '<em><b>Operation Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation Variable</em>' containment reference.
	 * @see #setOperationVariable(OperationVariableT)
	 * @see io.shell.admin.aas._1._0._0Package#getSubmodelElementT_OperationVariable()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='operationVariable' namespace='##targetNamespace'"
	 * @generated
	 */
	OperationVariableT getOperationVariable();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.SubmodelElementT#getOperationVariable <em>Operation Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operation Variable</em>' containment reference.
	 * @see #getOperationVariable()
	 * @generated
	 */
	void setOperationVariable(OperationVariableT value);

	/**
	 * Returns the value of the '<em><b>Event</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event</em>' containment reference.
	 * @see #setEvent(EventT)
	 * @see io.shell.admin.aas._1._0._0Package#getSubmodelElementT_Event()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='event' namespace='##targetNamespace'"
	 * @generated
	 */
	EventT getEvent();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.SubmodelElementT#getEvent <em>Event</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event</em>' containment reference.
	 * @see #getEvent()
	 * @generated
	 */
	void setEvent(EventT value);

} // SubmodelElementT
