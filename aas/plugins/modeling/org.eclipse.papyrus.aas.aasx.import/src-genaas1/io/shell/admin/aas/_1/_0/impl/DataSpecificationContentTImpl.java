/**
 * ****************************************************************************
 *   Copyright (c) 2023 CEA LIST.
 *  
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License 2.0
 *   which accompanies this distribution, and is available at
 *   https://www.eclipse.org/legal/epl-2.0/
 *  
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *    Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *  
 *  ****************************************************************************
 */
package io.shell.admin.aas._1._0.impl;

import io.shell.admin.aas._1._0.DataSpecificationContentT;
import io.shell.admin.aas._1._0._0Package;

import io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Specification Content T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._1._0.impl.DataSpecificationContentTImpl#getDataSpecificationIEC61360 <em>Data Specification IEC61360</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataSpecificationContentTImpl extends MinimalEObjectImpl.Container implements DataSpecificationContentT {
	/**
	 * The cached value of the '{@link #getDataSpecificationIEC61360() <em>Data Specification IEC61360</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataSpecificationIEC61360()
	 * @generated
	 * @ordered
	 */
	protected DataSpecificationIEC61630T dataSpecificationIEC61360;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataSpecificationContentTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.DATA_SPECIFICATION_CONTENT_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataSpecificationIEC61630T getDataSpecificationIEC61360() {
		return dataSpecificationIEC61360;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataSpecificationIEC61360(DataSpecificationIEC61630T newDataSpecificationIEC61360, NotificationChain msgs) {
		DataSpecificationIEC61630T oldDataSpecificationIEC61360 = dataSpecificationIEC61360;
		dataSpecificationIEC61360 = newDataSpecificationIEC61360;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.DATA_SPECIFICATION_CONTENT_T__DATA_SPECIFICATION_IEC61360, oldDataSpecificationIEC61360, newDataSpecificationIEC61360);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataSpecificationIEC61360(DataSpecificationIEC61630T newDataSpecificationIEC61360) {
		if (newDataSpecificationIEC61360 != dataSpecificationIEC61360) {
			NotificationChain msgs = null;
			if (dataSpecificationIEC61360 != null)
				msgs = ((InternalEObject)dataSpecificationIEC61360).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_SPECIFICATION_CONTENT_T__DATA_SPECIFICATION_IEC61360, null, msgs);
			if (newDataSpecificationIEC61360 != null)
				msgs = ((InternalEObject)newDataSpecificationIEC61360).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_SPECIFICATION_CONTENT_T__DATA_SPECIFICATION_IEC61360, null, msgs);
			msgs = basicSetDataSpecificationIEC61360(newDataSpecificationIEC61360, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.DATA_SPECIFICATION_CONTENT_T__DATA_SPECIFICATION_IEC61360, newDataSpecificationIEC61360, newDataSpecificationIEC61360));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.DATA_SPECIFICATION_CONTENT_T__DATA_SPECIFICATION_IEC61360:
				return basicSetDataSpecificationIEC61360(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.DATA_SPECIFICATION_CONTENT_T__DATA_SPECIFICATION_IEC61360:
				return getDataSpecificationIEC61360();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.DATA_SPECIFICATION_CONTENT_T__DATA_SPECIFICATION_IEC61360:
				setDataSpecificationIEC61360((DataSpecificationIEC61630T)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.DATA_SPECIFICATION_CONTENT_T__DATA_SPECIFICATION_IEC61360:
				setDataSpecificationIEC61360((DataSpecificationIEC61630T)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.DATA_SPECIFICATION_CONTENT_T__DATA_SPECIFICATION_IEC61360:
				return dataSpecificationIEC61360 != null;
		}
		return super.eIsSet(featureID);
	}

} //DataSpecificationContentTImpl
