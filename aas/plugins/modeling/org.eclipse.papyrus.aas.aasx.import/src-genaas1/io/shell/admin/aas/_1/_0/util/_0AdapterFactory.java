/**
 * ****************************************************************************
 *   Copyright (c) 2023 CEA LIST.
 *  
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License 2.0
 *   which accompanies this distribution, and is available at
 *   https://www.eclipse.org/legal/epl-2.0/
 *  
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *    Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *  
 *  ****************************************************************************
 */
package io.shell.admin.aas._1._0.util;

import io.shell.admin.aas._1._0.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see io.shell.admin.aas._1._0._0Package
 * @generated
 */
public class _0AdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static _0Package modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public _0AdapterFactory() {
		if (modelPackage == null) {
			modelPackage = _0Package.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected _0Switch<Adapter> modelSwitch =
		new _0Switch<Adapter>() {
			@Override
			public Adapter caseAasenvT(AasenvT object) {
				return createAasenvTAdapter();
			}
			@Override
			public Adapter caseAdministrationT(AdministrationT object) {
				return createAdministrationTAdapter();
			}
			@Override
			public Adapter caseAssetAdministrationShellsT(AssetAdministrationShellsT object) {
				return createAssetAdministrationShellsTAdapter();
			}
			@Override
			public Adapter caseAssetAdministrationShellT(AssetAdministrationShellT object) {
				return createAssetAdministrationShellTAdapter();
			}
			@Override
			public Adapter caseAssetsT(AssetsT object) {
				return createAssetsTAdapter();
			}
			@Override
			public Adapter caseAssetT(AssetT object) {
				return createAssetTAdapter();
			}
			@Override
			public Adapter caseBlobT(BlobT object) {
				return createBlobTAdapter();
			}
			@Override
			public Adapter caseBlobTypeT(BlobTypeT object) {
				return createBlobTypeTAdapter();
			}
			@Override
			public Adapter caseConceptDescriptionsRefT(ConceptDescriptionsRefT object) {
				return createConceptDescriptionsRefTAdapter();
			}
			@Override
			public Adapter caseConceptDescriptionsT(ConceptDescriptionsT object) {
				return createConceptDescriptionsTAdapter();
			}
			@Override
			public Adapter caseConceptDescriptionT(ConceptDescriptionT object) {
				return createConceptDescriptionTAdapter();
			}
			@Override
			public Adapter caseConceptDictionariesT(ConceptDictionariesT object) {
				return createConceptDictionariesTAdapter();
			}
			@Override
			public Adapter caseConceptDictionaryT(ConceptDictionaryT object) {
				return createConceptDictionaryTAdapter();
			}
			@Override
			public Adapter caseConstraintT(ConstraintT object) {
				return createConstraintTAdapter();
			}
			@Override
			public Adapter caseContainedElementsT(ContainedElementsT object) {
				return createContainedElementsTAdapter();
			}
			@Override
			public Adapter caseDataSpecificationContentT(DataSpecificationContentT object) {
				return createDataSpecificationContentTAdapter();
			}
			@Override
			public Adapter caseDocumentRoot(DocumentRoot object) {
				return createDocumentRootAdapter();
			}
			@Override
			public Adapter caseEmbeddedDataSpecificationT(EmbeddedDataSpecificationT object) {
				return createEmbeddedDataSpecificationTAdapter();
			}
			@Override
			public Adapter caseEventT(EventT object) {
				return createEventTAdapter();
			}
			@Override
			public Adapter caseFileT(FileT object) {
				return createFileTAdapter();
			}
			@Override
			public Adapter caseFormulaT(FormulaT object) {
				return createFormulaTAdapter();
			}
			@Override
			public Adapter caseIdentificationT(IdentificationT object) {
				return createIdentificationTAdapter();
			}
			@Override
			public Adapter caseIdPropertyDefinitionT(IdPropertyDefinitionT object) {
				return createIdPropertyDefinitionTAdapter();
			}
			@Override
			public Adapter caseIdShortT(IdShortT object) {
				return createIdShortTAdapter();
			}
			@Override
			public Adapter caseKeysT(KeysT object) {
				return createKeysTAdapter();
			}
			@Override
			public Adapter caseKeyT(KeyT object) {
				return createKeyTAdapter();
			}
			@Override
			public Adapter caseLangStringsT(LangStringsT object) {
				return createLangStringsTAdapter();
			}
			@Override
			public Adapter caseLangStringT(LangStringT object) {
				return createLangStringTAdapter();
			}
			@Override
			public Adapter caseOperationT(OperationT object) {
				return createOperationTAdapter();
			}
			@Override
			public Adapter caseOperationVariableT(OperationVariableT object) {
				return createOperationVariableTAdapter();
			}
			@Override
			public Adapter casePathTypeT(PathTypeT object) {
				return createPathTypeTAdapter();
			}
			@Override
			public Adapter casePropertyT(PropertyT object) {
				return createPropertyTAdapter();
			}
			@Override
			public Adapter casePropertyValueTypeT(PropertyValueTypeT object) {
				return createPropertyValueTypeTAdapter();
			}
			@Override
			public Adapter casePrvalueTypeT(PrvalueTypeT object) {
				return createPrvalueTypeTAdapter();
			}
			@Override
			public Adapter caseQualifiersT(QualifiersT object) {
				return createQualifiersTAdapter();
			}
			@Override
			public Adapter caseQualifierT(QualifierT object) {
				return createQualifierTAdapter();
			}
			@Override
			public Adapter caseReferenceElementT(ReferenceElementT object) {
				return createReferenceElementTAdapter();
			}
			@Override
			public Adapter caseReferencesT(ReferencesT object) {
				return createReferencesTAdapter();
			}
			@Override
			public Adapter caseReferenceT(ReferenceT object) {
				return createReferenceTAdapter();
			}
			@Override
			public Adapter caseRelationshipElementT(RelationshipElementT object) {
				return createRelationshipElementTAdapter();
			}
			@Override
			public Adapter caseSemanticIdT(SemanticIdT object) {
				return createSemanticIdTAdapter();
			}
			@Override
			public Adapter caseSubmodelElementAbstractT(SubmodelElementAbstractT object) {
				return createSubmodelElementAbstractTAdapter();
			}
			@Override
			public Adapter caseSubmodelElementCollectionT(SubmodelElementCollectionT object) {
				return createSubmodelElementCollectionTAdapter();
			}
			@Override
			public Adapter caseSubmodelElementsT(SubmodelElementsT object) {
				return createSubmodelElementsTAdapter();
			}
			@Override
			public Adapter caseSubmodelElementT(SubmodelElementT object) {
				return createSubmodelElementTAdapter();
			}
			@Override
			public Adapter caseSubmodelRefsT(SubmodelRefsT object) {
				return createSubmodelRefsTAdapter();
			}
			@Override
			public Adapter caseSubmodelsT(SubmodelsT object) {
				return createSubmodelsTAdapter();
			}
			@Override
			public Adapter caseSubmodelT(SubmodelT object) {
				return createSubmodelTAdapter();
			}
			@Override
			public Adapter caseViewsT(ViewsT object) {
				return createViewsTAdapter();
			}
			@Override
			public Adapter caseViewT(ViewT object) {
				return createViewTAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.AasenvT <em>Aasenv T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.AasenvT
	 * @generated
	 */
	public Adapter createAasenvTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.AdministrationT <em>Administration T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.AdministrationT
	 * @generated
	 */
	public Adapter createAdministrationTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.AssetAdministrationShellsT <em>Asset Administration Shells T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.AssetAdministrationShellsT
	 * @generated
	 */
	public Adapter createAssetAdministrationShellsTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.AssetAdministrationShellT <em>Asset Administration Shell T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.AssetAdministrationShellT
	 * @generated
	 */
	public Adapter createAssetAdministrationShellTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.AssetsT <em>Assets T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.AssetsT
	 * @generated
	 */
	public Adapter createAssetsTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.AssetT <em>Asset T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.AssetT
	 * @generated
	 */
	public Adapter createAssetTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.BlobT <em>Blob T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.BlobT
	 * @generated
	 */
	public Adapter createBlobTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.BlobTypeT <em>Blob Type T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.BlobTypeT
	 * @generated
	 */
	public Adapter createBlobTypeTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.ConceptDescriptionsRefT <em>Concept Descriptions Ref T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.ConceptDescriptionsRefT
	 * @generated
	 */
	public Adapter createConceptDescriptionsRefTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.ConceptDescriptionsT <em>Concept Descriptions T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.ConceptDescriptionsT
	 * @generated
	 */
	public Adapter createConceptDescriptionsTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.ConceptDescriptionT <em>Concept Description T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.ConceptDescriptionT
	 * @generated
	 */
	public Adapter createConceptDescriptionTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.ConceptDictionariesT <em>Concept Dictionaries T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.ConceptDictionariesT
	 * @generated
	 */
	public Adapter createConceptDictionariesTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.ConceptDictionaryT <em>Concept Dictionary T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.ConceptDictionaryT
	 * @generated
	 */
	public Adapter createConceptDictionaryTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.ConstraintT <em>Constraint T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.ConstraintT
	 * @generated
	 */
	public Adapter createConstraintTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.ContainedElementsT <em>Contained Elements T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.ContainedElementsT
	 * @generated
	 */
	public Adapter createContainedElementsTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.DataSpecificationContentT <em>Data Specification Content T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.DataSpecificationContentT
	 * @generated
	 */
	public Adapter createDataSpecificationContentTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.DocumentRoot
	 * @generated
	 */
	public Adapter createDocumentRootAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.EmbeddedDataSpecificationT <em>Embedded Data Specification T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.EmbeddedDataSpecificationT
	 * @generated
	 */
	public Adapter createEmbeddedDataSpecificationTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.EventT <em>Event T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.EventT
	 * @generated
	 */
	public Adapter createEventTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.FileT <em>File T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.FileT
	 * @generated
	 */
	public Adapter createFileTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.FormulaT <em>Formula T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.FormulaT
	 * @generated
	 */
	public Adapter createFormulaTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.IdentificationT <em>Identification T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.IdentificationT
	 * @generated
	 */
	public Adapter createIdentificationTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.IdPropertyDefinitionT <em>Id Property Definition T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.IdPropertyDefinitionT
	 * @generated
	 */
	public Adapter createIdPropertyDefinitionTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.IdShortT <em>Id Short T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.IdShortT
	 * @generated
	 */
	public Adapter createIdShortTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.KeysT <em>Keys T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.KeysT
	 * @generated
	 */
	public Adapter createKeysTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.KeyT <em>Key T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.KeyT
	 * @generated
	 */
	public Adapter createKeyTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.LangStringsT <em>Lang Strings T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.LangStringsT
	 * @generated
	 */
	public Adapter createLangStringsTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.LangStringT <em>Lang String T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.LangStringT
	 * @generated
	 */
	public Adapter createLangStringTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.OperationT <em>Operation T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.OperationT
	 * @generated
	 */
	public Adapter createOperationTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.OperationVariableT <em>Operation Variable T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.OperationVariableT
	 * @generated
	 */
	public Adapter createOperationVariableTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.PathTypeT <em>Path Type T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.PathTypeT
	 * @generated
	 */
	public Adapter createPathTypeTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.PropertyT <em>Property T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.PropertyT
	 * @generated
	 */
	public Adapter createPropertyTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.PropertyValueTypeT <em>Property Value Type T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.PropertyValueTypeT
	 * @generated
	 */
	public Adapter createPropertyValueTypeTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.PrvalueTypeT <em>Prvalue Type T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.PrvalueTypeT
	 * @generated
	 */
	public Adapter createPrvalueTypeTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.QualifiersT <em>Qualifiers T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.QualifiersT
	 * @generated
	 */
	public Adapter createQualifiersTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.QualifierT <em>Qualifier T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.QualifierT
	 * @generated
	 */
	public Adapter createQualifierTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.ReferenceElementT <em>Reference Element T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.ReferenceElementT
	 * @generated
	 */
	public Adapter createReferenceElementTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.ReferencesT <em>References T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.ReferencesT
	 * @generated
	 */
	public Adapter createReferencesTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.ReferenceT <em>Reference T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.ReferenceT
	 * @generated
	 */
	public Adapter createReferenceTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.RelationshipElementT <em>Relationship Element T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.RelationshipElementT
	 * @generated
	 */
	public Adapter createRelationshipElementTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.SemanticIdT <em>Semantic Id T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.SemanticIdT
	 * @generated
	 */
	public Adapter createSemanticIdTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.SubmodelElementAbstractT <em>Submodel Element Abstract T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.SubmodelElementAbstractT
	 * @generated
	 */
	public Adapter createSubmodelElementAbstractTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.SubmodelElementCollectionT <em>Submodel Element Collection T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.SubmodelElementCollectionT
	 * @generated
	 */
	public Adapter createSubmodelElementCollectionTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.SubmodelElementsT <em>Submodel Elements T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.SubmodelElementsT
	 * @generated
	 */
	public Adapter createSubmodelElementsTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.SubmodelElementT <em>Submodel Element T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.SubmodelElementT
	 * @generated
	 */
	public Adapter createSubmodelElementTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.SubmodelRefsT <em>Submodel Refs T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.SubmodelRefsT
	 * @generated
	 */
	public Adapter createSubmodelRefsTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.SubmodelsT <em>Submodels T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.SubmodelsT
	 * @generated
	 */
	public Adapter createSubmodelsTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.SubmodelT <em>Submodel T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.SubmodelT
	 * @generated
	 */
	public Adapter createSubmodelTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.ViewsT <em>Views T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.ViewsT
	 * @generated
	 */
	public Adapter createViewsTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas._1._0.ViewT <em>View T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas._1._0.ViewT
	 * @generated
	 */
	public Adapter createViewTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //_0AdapterFactory
