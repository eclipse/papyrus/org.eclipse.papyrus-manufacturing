/**
 * ****************************************************************************
 *   Copyright (c) 2023 CEA LIST.
 *  
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License 2.0
 *   which accompanies this distribution, and is available at
 *   https://www.eclipse.org/legal/epl-2.0/
 *  
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *    Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *  
 *  ****************************************************************************
 */
package io.shell.admin.aas._1._0.impl;

import io.shell.admin.aas._1._0.ContainedElementsT;
import io.shell.admin.aas._1._0.EmbeddedDataSpecificationT;
import io.shell.admin.aas._1._0.IdShortT;
import io.shell.admin.aas._1._0.LangStringsT;
import io.shell.admin.aas._1._0.SemanticIdT;
import io.shell.admin.aas._1._0.ViewT;
import io.shell.admin.aas._1._0._0Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>View T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._1._0.impl.ViewTImpl#getIdShort <em>Id Short</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.impl.ViewTImpl#getCategory <em>Category</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.impl.ViewTImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.impl.ViewTImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.impl.ViewTImpl#getSemanticId <em>Semantic Id</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.impl.ViewTImpl#getEmbeddedDataSpecification <em>Embedded Data Specification</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.impl.ViewTImpl#getContainedElements <em>Contained Elements</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ViewTImpl extends MinimalEObjectImpl.Container implements ViewT {
	/**
	 * The cached value of the '{@link #getIdShort() <em>Id Short</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdShort()
	 * @generated
	 * @ordered
	 */
	protected IdShortT idShort;

	/**
	 * The default value of the '{@link #getCategory() <em>Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCategory()
	 * @generated
	 * @ordered
	 */
	protected static final String CATEGORY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCategory() <em>Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCategory()
	 * @generated
	 * @ordered
	 */
	protected String category = CATEGORY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected LangStringsT description;

	/**
	 * The default value of the '{@link #getParent() <em>Parent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParent()
	 * @generated
	 * @ordered
	 */
	protected static final String PARENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getParent() <em>Parent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParent()
	 * @generated
	 * @ordered
	 */
	protected String parent = PARENT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSemanticId() <em>Semantic Id</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemanticId()
	 * @generated
	 * @ordered
	 */
	protected SemanticIdT semanticId;

	/**
	 * The cached value of the '{@link #getEmbeddedDataSpecification() <em>Embedded Data Specification</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEmbeddedDataSpecification()
	 * @generated
	 * @ordered
	 */
	protected EList<EmbeddedDataSpecificationT> embeddedDataSpecification;

	/**
	 * The cached value of the '{@link #getContainedElements() <em>Contained Elements</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainedElements()
	 * @generated
	 * @ordered
	 */
	protected ContainedElementsT containedElements;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ViewTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.VIEW_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdShortT getIdShort() {
		return idShort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIdShort(IdShortT newIdShort, NotificationChain msgs) {
		IdShortT oldIdShort = idShort;
		idShort = newIdShort;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.VIEW_T__ID_SHORT, oldIdShort, newIdShort);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdShort(IdShortT newIdShort) {
		if (newIdShort != idShort) {
			NotificationChain msgs = null;
			if (idShort != null)
				msgs = ((InternalEObject)idShort).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.VIEW_T__ID_SHORT, null, msgs);
			if (newIdShort != null)
				msgs = ((InternalEObject)newIdShort).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.VIEW_T__ID_SHORT, null, msgs);
			msgs = basicSetIdShort(newIdShort, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.VIEW_T__ID_SHORT, newIdShort, newIdShort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCategory(String newCategory) {
		String oldCategory = category;
		category = newCategory;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.VIEW_T__CATEGORY, oldCategory, category));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LangStringsT getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDescription(LangStringsT newDescription, NotificationChain msgs) {
		LangStringsT oldDescription = description;
		description = newDescription;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.VIEW_T__DESCRIPTION, oldDescription, newDescription);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(LangStringsT newDescription) {
		if (newDescription != description) {
			NotificationChain msgs = null;
			if (description != null)
				msgs = ((InternalEObject)description).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.VIEW_T__DESCRIPTION, null, msgs);
			if (newDescription != null)
				msgs = ((InternalEObject)newDescription).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.VIEW_T__DESCRIPTION, null, msgs);
			msgs = basicSetDescription(newDescription, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.VIEW_T__DESCRIPTION, newDescription, newDescription));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getParent() {
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParent(String newParent) {
		String oldParent = parent;
		parent = newParent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.VIEW_T__PARENT, oldParent, parent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SemanticIdT getSemanticId() {
		return semanticId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSemanticId(SemanticIdT newSemanticId, NotificationChain msgs) {
		SemanticIdT oldSemanticId = semanticId;
		semanticId = newSemanticId;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.VIEW_T__SEMANTIC_ID, oldSemanticId, newSemanticId);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSemanticId(SemanticIdT newSemanticId) {
		if (newSemanticId != semanticId) {
			NotificationChain msgs = null;
			if (semanticId != null)
				msgs = ((InternalEObject)semanticId).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.VIEW_T__SEMANTIC_ID, null, msgs);
			if (newSemanticId != null)
				msgs = ((InternalEObject)newSemanticId).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.VIEW_T__SEMANTIC_ID, null, msgs);
			msgs = basicSetSemanticId(newSemanticId, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.VIEW_T__SEMANTIC_ID, newSemanticId, newSemanticId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EmbeddedDataSpecificationT> getEmbeddedDataSpecification() {
		if (embeddedDataSpecification == null) {
			embeddedDataSpecification = new EObjectContainmentEList<EmbeddedDataSpecificationT>(EmbeddedDataSpecificationT.class, this, _0Package.VIEW_T__EMBEDDED_DATA_SPECIFICATION);
		}
		return embeddedDataSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ContainedElementsT getContainedElements() {
		return containedElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContainedElements(ContainedElementsT newContainedElements, NotificationChain msgs) {
		ContainedElementsT oldContainedElements = containedElements;
		containedElements = newContainedElements;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.VIEW_T__CONTAINED_ELEMENTS, oldContainedElements, newContainedElements);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainedElements(ContainedElementsT newContainedElements) {
		if (newContainedElements != containedElements) {
			NotificationChain msgs = null;
			if (containedElements != null)
				msgs = ((InternalEObject)containedElements).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.VIEW_T__CONTAINED_ELEMENTS, null, msgs);
			if (newContainedElements != null)
				msgs = ((InternalEObject)newContainedElements).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.VIEW_T__CONTAINED_ELEMENTS, null, msgs);
			msgs = basicSetContainedElements(newContainedElements, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.VIEW_T__CONTAINED_ELEMENTS, newContainedElements, newContainedElements));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.VIEW_T__ID_SHORT:
				return basicSetIdShort(null, msgs);
			case _0Package.VIEW_T__DESCRIPTION:
				return basicSetDescription(null, msgs);
			case _0Package.VIEW_T__SEMANTIC_ID:
				return basicSetSemanticId(null, msgs);
			case _0Package.VIEW_T__EMBEDDED_DATA_SPECIFICATION:
				return ((InternalEList<?>)getEmbeddedDataSpecification()).basicRemove(otherEnd, msgs);
			case _0Package.VIEW_T__CONTAINED_ELEMENTS:
				return basicSetContainedElements(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.VIEW_T__ID_SHORT:
				return getIdShort();
			case _0Package.VIEW_T__CATEGORY:
				return getCategory();
			case _0Package.VIEW_T__DESCRIPTION:
				return getDescription();
			case _0Package.VIEW_T__PARENT:
				return getParent();
			case _0Package.VIEW_T__SEMANTIC_ID:
				return getSemanticId();
			case _0Package.VIEW_T__EMBEDDED_DATA_SPECIFICATION:
				return getEmbeddedDataSpecification();
			case _0Package.VIEW_T__CONTAINED_ELEMENTS:
				return getContainedElements();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.VIEW_T__ID_SHORT:
				setIdShort((IdShortT)newValue);
				return;
			case _0Package.VIEW_T__CATEGORY:
				setCategory((String)newValue);
				return;
			case _0Package.VIEW_T__DESCRIPTION:
				setDescription((LangStringsT)newValue);
				return;
			case _0Package.VIEW_T__PARENT:
				setParent((String)newValue);
				return;
			case _0Package.VIEW_T__SEMANTIC_ID:
				setSemanticId((SemanticIdT)newValue);
				return;
			case _0Package.VIEW_T__EMBEDDED_DATA_SPECIFICATION:
				getEmbeddedDataSpecification().clear();
				getEmbeddedDataSpecification().addAll((Collection<? extends EmbeddedDataSpecificationT>)newValue);
				return;
			case _0Package.VIEW_T__CONTAINED_ELEMENTS:
				setContainedElements((ContainedElementsT)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.VIEW_T__ID_SHORT:
				setIdShort((IdShortT)null);
				return;
			case _0Package.VIEW_T__CATEGORY:
				setCategory(CATEGORY_EDEFAULT);
				return;
			case _0Package.VIEW_T__DESCRIPTION:
				setDescription((LangStringsT)null);
				return;
			case _0Package.VIEW_T__PARENT:
				setParent(PARENT_EDEFAULT);
				return;
			case _0Package.VIEW_T__SEMANTIC_ID:
				setSemanticId((SemanticIdT)null);
				return;
			case _0Package.VIEW_T__EMBEDDED_DATA_SPECIFICATION:
				getEmbeddedDataSpecification().clear();
				return;
			case _0Package.VIEW_T__CONTAINED_ELEMENTS:
				setContainedElements((ContainedElementsT)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.VIEW_T__ID_SHORT:
				return idShort != null;
			case _0Package.VIEW_T__CATEGORY:
				return CATEGORY_EDEFAULT == null ? category != null : !CATEGORY_EDEFAULT.equals(category);
			case _0Package.VIEW_T__DESCRIPTION:
				return description != null;
			case _0Package.VIEW_T__PARENT:
				return PARENT_EDEFAULT == null ? parent != null : !PARENT_EDEFAULT.equals(parent);
			case _0Package.VIEW_T__SEMANTIC_ID:
				return semanticId != null;
			case _0Package.VIEW_T__EMBEDDED_DATA_SPECIFICATION:
				return embeddedDataSpecification != null && !embeddedDataSpecification.isEmpty();
			case _0Package.VIEW_T__CONTAINED_ELEMENTS:
				return containedElements != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (category: ");
		result.append(category);
		result.append(", parent: ");
		result.append(parent);
		result.append(')');
		return result.toString();
	}

} //ViewTImpl
