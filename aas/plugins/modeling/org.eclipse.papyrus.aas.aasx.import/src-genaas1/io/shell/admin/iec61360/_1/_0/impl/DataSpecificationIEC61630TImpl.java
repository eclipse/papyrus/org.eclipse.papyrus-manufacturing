/**
 * ****************************************************************************
 *   Copyright (c) 2023 CEA LIST.
 *  
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License 2.0
 *   which accompanies this distribution, and is available at
 *   https://www.eclipse.org/legal/epl-2.0/
 *  
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *    Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *  
 *  ****************************************************************************
 */
package io.shell.admin.iec61360._1._0.impl;

import io.shell.admin.aas._1._0.LangStringsT;
import io.shell.admin.aas._1._0.ReferenceT;

import io.shell.admin.iec61360._1._0.CodeT;
import io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T;
import io.shell.admin.iec61360._1._0.ValueListT;
import io.shell.admin.iec61360._1._0._0Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Specification IEC61630T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DataSpecificationIEC61630TImpl#getPreferredName <em>Preferred Name</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DataSpecificationIEC61630TImpl#getShortName <em>Short Name</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DataSpecificationIEC61630TImpl#getUnit <em>Unit</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DataSpecificationIEC61630TImpl#getUnitId <em>Unit Id</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DataSpecificationIEC61630TImpl#getValueFormat <em>Value Format</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DataSpecificationIEC61630TImpl#getSourceOfDefinition <em>Source Of Definition</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DataSpecificationIEC61630TImpl#getSymbol <em>Symbol</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DataSpecificationIEC61630TImpl#getDataType <em>Data Type</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DataSpecificationIEC61630TImpl#getDefinition <em>Definition</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DataSpecificationIEC61630TImpl#getValueList <em>Value List</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DataSpecificationIEC61630TImpl#getCode <em>Code</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataSpecificationIEC61630TImpl extends MinimalEObjectImpl.Container implements DataSpecificationIEC61630T {
	/**
	 * The cached value of the '{@link #getPreferredName() <em>Preferred Name</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreferredName()
	 * @generated
	 * @ordered
	 */
	protected LangStringsT preferredName;

	/**
	 * The default value of the '{@link #getShortName() <em>Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShortName()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getShortName() <em>Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShortName()
	 * @generated
	 * @ordered
	 */
	protected String shortName = SHORT_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getUnit() <em>Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnit()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnit() <em>Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnit()
	 * @generated
	 * @ordered
	 */
	protected String unit = UNIT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getUnitId() <em>Unit Id</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitId()
	 * @generated
	 * @ordered
	 */
	protected ReferenceT unitId;

	/**
	 * The default value of the '{@link #getValueFormat() <em>Value Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueFormat()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_FORMAT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValueFormat() <em>Value Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueFormat()
	 * @generated
	 * @ordered
	 */
	protected String valueFormat = VALUE_FORMAT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSourceOfDefinition() <em>Source Of Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceOfDefinition()
	 * @generated
	 * @ordered
	 */
	protected LangStringsT sourceOfDefinition;

	/**
	 * The default value of the '{@link #getSymbol() <em>Symbol</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSymbol()
	 * @generated
	 * @ordered
	 */
	protected static final String SYMBOL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSymbol() <em>Symbol</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSymbol()
	 * @generated
	 * @ordered
	 */
	protected String symbol = SYMBOL_EDEFAULT;

	/**
	 * The default value of the '{@link #getDataType() <em>Data Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataType()
	 * @generated
	 * @ordered
	 */
	protected static final String DATA_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDataType() <em>Data Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataType()
	 * @generated
	 * @ordered
	 */
	protected String dataType = DATA_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDefinition() <em>Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefinition()
	 * @generated
	 * @ordered
	 */
	protected LangStringsT definition;

	/**
	 * The cached value of the '{@link #getValueList() <em>Value List</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueList()
	 * @generated
	 * @ordered
	 */
	protected ValueListT valueList;

	/**
	 * The cached value of the '{@link #getCode() <em>Code</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected CodeT code;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataSpecificationIEC61630TImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.DATA_SPECIFICATION_IEC61630T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LangStringsT getPreferredName() {
		return preferredName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPreferredName(LangStringsT newPreferredName, NotificationChain msgs) {
		LangStringsT oldPreferredName = preferredName;
		preferredName = newPreferredName;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.DATA_SPECIFICATION_IEC61630T__PREFERRED_NAME, oldPreferredName, newPreferredName);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreferredName(LangStringsT newPreferredName) {
		if (newPreferredName != preferredName) {
			NotificationChain msgs = null;
			if (preferredName != null)
				msgs = ((InternalEObject)preferredName).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_SPECIFICATION_IEC61630T__PREFERRED_NAME, null, msgs);
			if (newPreferredName != null)
				msgs = ((InternalEObject)newPreferredName).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_SPECIFICATION_IEC61630T__PREFERRED_NAME, null, msgs);
			msgs = basicSetPreferredName(newPreferredName, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.DATA_SPECIFICATION_IEC61630T__PREFERRED_NAME, newPreferredName, newPreferredName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShortName(String newShortName) {
		String oldShortName = shortName;
		shortName = newShortName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.DATA_SPECIFICATION_IEC61630T__SHORT_NAME, oldShortName, shortName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnit(String newUnit) {
		String oldUnit = unit;
		unit = newUnit;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.DATA_SPECIFICATION_IEC61630T__UNIT, oldUnit, unit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceT getUnitId() {
		return unitId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnitId(ReferenceT newUnitId, NotificationChain msgs) {
		ReferenceT oldUnitId = unitId;
		unitId = newUnitId;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.DATA_SPECIFICATION_IEC61630T__UNIT_ID, oldUnitId, newUnitId);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnitId(ReferenceT newUnitId) {
		if (newUnitId != unitId) {
			NotificationChain msgs = null;
			if (unitId != null)
				msgs = ((InternalEObject)unitId).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_SPECIFICATION_IEC61630T__UNIT_ID, null, msgs);
			if (newUnitId != null)
				msgs = ((InternalEObject)newUnitId).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_SPECIFICATION_IEC61630T__UNIT_ID, null, msgs);
			msgs = basicSetUnitId(newUnitId, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.DATA_SPECIFICATION_IEC61630T__UNIT_ID, newUnitId, newUnitId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValueFormat() {
		return valueFormat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueFormat(String newValueFormat) {
		String oldValueFormat = valueFormat;
		valueFormat = newValueFormat;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_FORMAT, oldValueFormat, valueFormat));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LangStringsT getSourceOfDefinition() {
		return sourceOfDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSourceOfDefinition(LangStringsT newSourceOfDefinition, NotificationChain msgs) {
		LangStringsT oldSourceOfDefinition = sourceOfDefinition;
		sourceOfDefinition = newSourceOfDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.DATA_SPECIFICATION_IEC61630T__SOURCE_OF_DEFINITION, oldSourceOfDefinition, newSourceOfDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceOfDefinition(LangStringsT newSourceOfDefinition) {
		if (newSourceOfDefinition != sourceOfDefinition) {
			NotificationChain msgs = null;
			if (sourceOfDefinition != null)
				msgs = ((InternalEObject)sourceOfDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_SPECIFICATION_IEC61630T__SOURCE_OF_DEFINITION, null, msgs);
			if (newSourceOfDefinition != null)
				msgs = ((InternalEObject)newSourceOfDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_SPECIFICATION_IEC61630T__SOURCE_OF_DEFINITION, null, msgs);
			msgs = basicSetSourceOfDefinition(newSourceOfDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.DATA_SPECIFICATION_IEC61630T__SOURCE_OF_DEFINITION, newSourceOfDefinition, newSourceOfDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSymbol() {
		return symbol;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSymbol(String newSymbol) {
		String oldSymbol = symbol;
		symbol = newSymbol;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.DATA_SPECIFICATION_IEC61630T__SYMBOL, oldSymbol, symbol));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDataType() {
		return dataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataType(String newDataType) {
		String oldDataType = dataType;
		dataType = newDataType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.DATA_SPECIFICATION_IEC61630T__DATA_TYPE, oldDataType, dataType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LangStringsT getDefinition() {
		return definition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefinition(LangStringsT newDefinition, NotificationChain msgs) {
		LangStringsT oldDefinition = definition;
		definition = newDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.DATA_SPECIFICATION_IEC61630T__DEFINITION, oldDefinition, newDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefinition(LangStringsT newDefinition) {
		if (newDefinition != definition) {
			NotificationChain msgs = null;
			if (definition != null)
				msgs = ((InternalEObject)definition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_SPECIFICATION_IEC61630T__DEFINITION, null, msgs);
			if (newDefinition != null)
				msgs = ((InternalEObject)newDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_SPECIFICATION_IEC61630T__DEFINITION, null, msgs);
			msgs = basicSetDefinition(newDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.DATA_SPECIFICATION_IEC61630T__DEFINITION, newDefinition, newDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueListT getValueList() {
		return valueList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetValueList(ValueListT newValueList, NotificationChain msgs) {
		ValueListT oldValueList = valueList;
		valueList = newValueList;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_LIST, oldValueList, newValueList);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueList(ValueListT newValueList) {
		if (newValueList != valueList) {
			NotificationChain msgs = null;
			if (valueList != null)
				msgs = ((InternalEObject)valueList).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_LIST, null, msgs);
			if (newValueList != null)
				msgs = ((InternalEObject)newValueList).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_LIST, null, msgs);
			msgs = basicSetValueList(newValueList, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_LIST, newValueList, newValueList));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeT getCode() {
		return code;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCode(CodeT newCode, NotificationChain msgs) {
		CodeT oldCode = code;
		code = newCode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.DATA_SPECIFICATION_IEC61630T__CODE, oldCode, newCode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCode(CodeT newCode) {
		if (newCode != code) {
			NotificationChain msgs = null;
			if (code != null)
				msgs = ((InternalEObject)code).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_SPECIFICATION_IEC61630T__CODE, null, msgs);
			if (newCode != null)
				msgs = ((InternalEObject)newCode).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_SPECIFICATION_IEC61630T__CODE, null, msgs);
			msgs = basicSetCode(newCode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.DATA_SPECIFICATION_IEC61630T__CODE, newCode, newCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.DATA_SPECIFICATION_IEC61630T__PREFERRED_NAME:
				return basicSetPreferredName(null, msgs);
			case _0Package.DATA_SPECIFICATION_IEC61630T__UNIT_ID:
				return basicSetUnitId(null, msgs);
			case _0Package.DATA_SPECIFICATION_IEC61630T__SOURCE_OF_DEFINITION:
				return basicSetSourceOfDefinition(null, msgs);
			case _0Package.DATA_SPECIFICATION_IEC61630T__DEFINITION:
				return basicSetDefinition(null, msgs);
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_LIST:
				return basicSetValueList(null, msgs);
			case _0Package.DATA_SPECIFICATION_IEC61630T__CODE:
				return basicSetCode(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.DATA_SPECIFICATION_IEC61630T__PREFERRED_NAME:
				return getPreferredName();
			case _0Package.DATA_SPECIFICATION_IEC61630T__SHORT_NAME:
				return getShortName();
			case _0Package.DATA_SPECIFICATION_IEC61630T__UNIT:
				return getUnit();
			case _0Package.DATA_SPECIFICATION_IEC61630T__UNIT_ID:
				return getUnitId();
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_FORMAT:
				return getValueFormat();
			case _0Package.DATA_SPECIFICATION_IEC61630T__SOURCE_OF_DEFINITION:
				return getSourceOfDefinition();
			case _0Package.DATA_SPECIFICATION_IEC61630T__SYMBOL:
				return getSymbol();
			case _0Package.DATA_SPECIFICATION_IEC61630T__DATA_TYPE:
				return getDataType();
			case _0Package.DATA_SPECIFICATION_IEC61630T__DEFINITION:
				return getDefinition();
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_LIST:
				return getValueList();
			case _0Package.DATA_SPECIFICATION_IEC61630T__CODE:
				return getCode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.DATA_SPECIFICATION_IEC61630T__PREFERRED_NAME:
				setPreferredName((LangStringsT)newValue);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__SHORT_NAME:
				setShortName((String)newValue);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__UNIT:
				setUnit((String)newValue);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__UNIT_ID:
				setUnitId((ReferenceT)newValue);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_FORMAT:
				setValueFormat((String)newValue);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__SOURCE_OF_DEFINITION:
				setSourceOfDefinition((LangStringsT)newValue);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__SYMBOL:
				setSymbol((String)newValue);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__DATA_TYPE:
				setDataType((String)newValue);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__DEFINITION:
				setDefinition((LangStringsT)newValue);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_LIST:
				setValueList((ValueListT)newValue);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__CODE:
				setCode((CodeT)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.DATA_SPECIFICATION_IEC61630T__PREFERRED_NAME:
				setPreferredName((LangStringsT)null);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__SHORT_NAME:
				setShortName(SHORT_NAME_EDEFAULT);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__UNIT:
				setUnit(UNIT_EDEFAULT);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__UNIT_ID:
				setUnitId((ReferenceT)null);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_FORMAT:
				setValueFormat(VALUE_FORMAT_EDEFAULT);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__SOURCE_OF_DEFINITION:
				setSourceOfDefinition((LangStringsT)null);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__SYMBOL:
				setSymbol(SYMBOL_EDEFAULT);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__DATA_TYPE:
				setDataType(DATA_TYPE_EDEFAULT);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__DEFINITION:
				setDefinition((LangStringsT)null);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_LIST:
				setValueList((ValueListT)null);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__CODE:
				setCode((CodeT)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.DATA_SPECIFICATION_IEC61630T__PREFERRED_NAME:
				return preferredName != null;
			case _0Package.DATA_SPECIFICATION_IEC61630T__SHORT_NAME:
				return SHORT_NAME_EDEFAULT == null ? shortName != null : !SHORT_NAME_EDEFAULT.equals(shortName);
			case _0Package.DATA_SPECIFICATION_IEC61630T__UNIT:
				return UNIT_EDEFAULT == null ? unit != null : !UNIT_EDEFAULT.equals(unit);
			case _0Package.DATA_SPECIFICATION_IEC61630T__UNIT_ID:
				return unitId != null;
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_FORMAT:
				return VALUE_FORMAT_EDEFAULT == null ? valueFormat != null : !VALUE_FORMAT_EDEFAULT.equals(valueFormat);
			case _0Package.DATA_SPECIFICATION_IEC61630T__SOURCE_OF_DEFINITION:
				return sourceOfDefinition != null;
			case _0Package.DATA_SPECIFICATION_IEC61630T__SYMBOL:
				return SYMBOL_EDEFAULT == null ? symbol != null : !SYMBOL_EDEFAULT.equals(symbol);
			case _0Package.DATA_SPECIFICATION_IEC61630T__DATA_TYPE:
				return DATA_TYPE_EDEFAULT == null ? dataType != null : !DATA_TYPE_EDEFAULT.equals(dataType);
			case _0Package.DATA_SPECIFICATION_IEC61630T__DEFINITION:
				return definition != null;
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_LIST:
				return valueList != null;
			case _0Package.DATA_SPECIFICATION_IEC61630T__CODE:
				return code != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (shortName: ");
		result.append(shortName);
		result.append(", unit: ");
		result.append(unit);
		result.append(", valueFormat: ");
		result.append(valueFormat);
		result.append(", symbol: ");
		result.append(symbol);
		result.append(", dataType: ");
		result.append(dataType);
		result.append(')');
		return result.toString();
	}

} //DataSpecificationIEC61630TImpl
