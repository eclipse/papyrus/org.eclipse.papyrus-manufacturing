/**
 * ****************************************************************************
 *   Copyright (c) 2023 CEA LIST.
 *  
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License 2.0
 *   which accompanies this distribution, and is available at
 *   https://www.eclipse.org/legal/epl-2.0/
 *  
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *    Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *  
 *  ****************************************************************************
 */
package io.shell.admin.aas._1._0;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Concept Dictionaries T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._1._0.ConceptDictionariesT#getConceptDictionary <em>Concept Dictionary</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._1._0._0Package#getConceptDictionariesT()
 * @model extendedMetaData="name='conceptDictionaries_t' kind='elementOnly'"
 * @generated
 */
public interface ConceptDictionariesT extends EObject {
	/**
	 * Returns the value of the '<em><b>Concept Dictionary</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas._1._0.ConceptDictionaryT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Concept Dictionary</em>' containment reference list.
	 * @see io.shell.admin.aas._1._0._0Package#getConceptDictionariesT_ConceptDictionary()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='conceptDictionary' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ConceptDictionaryT> getConceptDictionary();

} // ConceptDictionariesT
