/**
 * ****************************************************************************
 *   Copyright (c) 2023 CEA LIST.
 *  
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License 2.0
 *   which accompanies this distribution, and is available at
 *   https://www.eclipse.org/legal/epl-2.0/
 *  
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *    Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *  
 *  ****************************************************************************
 */
package io.shell.admin.aas._1._0.impl;

import io.shell.admin.aas._1._0.ConceptDescriptionsRefT;
import io.shell.admin.aas._1._0.ConceptDictionaryT;
import io.shell.admin.aas._1._0.IdShortT;
import io.shell.admin.aas._1._0.LangStringsT;
import io.shell.admin.aas._1._0._0Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Concept Dictionary T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._1._0.impl.ConceptDictionaryTImpl#getIdShort <em>Id Short</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.impl.ConceptDictionaryTImpl#getCategory <em>Category</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.impl.ConceptDictionaryTImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.impl.ConceptDictionaryTImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.impl.ConceptDictionaryTImpl#getConceptDescriptionRefs <em>Concept Description Refs</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConceptDictionaryTImpl extends MinimalEObjectImpl.Container implements ConceptDictionaryT {
	/**
	 * The cached value of the '{@link #getIdShort() <em>Id Short</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdShort()
	 * @generated
	 * @ordered
	 */
	protected IdShortT idShort;

	/**
	 * The default value of the '{@link #getCategory() <em>Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCategory()
	 * @generated
	 * @ordered
	 */
	protected static final String CATEGORY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCategory() <em>Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCategory()
	 * @generated
	 * @ordered
	 */
	protected String category = CATEGORY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected LangStringsT description;

	/**
	 * The default value of the '{@link #getParent() <em>Parent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParent()
	 * @generated
	 * @ordered
	 */
	protected static final String PARENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getParent() <em>Parent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParent()
	 * @generated
	 * @ordered
	 */
	protected String parent = PARENT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getConceptDescriptionRefs() <em>Concept Description Refs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConceptDescriptionRefs()
	 * @generated
	 * @ordered
	 */
	protected ConceptDescriptionsRefT conceptDescriptionRefs;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConceptDictionaryTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.CONCEPT_DICTIONARY_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdShortT getIdShort() {
		return idShort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIdShort(IdShortT newIdShort, NotificationChain msgs) {
		IdShortT oldIdShort = idShort;
		idShort = newIdShort;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.CONCEPT_DICTIONARY_T__ID_SHORT, oldIdShort, newIdShort);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdShort(IdShortT newIdShort) {
		if (newIdShort != idShort) {
			NotificationChain msgs = null;
			if (idShort != null)
				msgs = ((InternalEObject)idShort).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.CONCEPT_DICTIONARY_T__ID_SHORT, null, msgs);
			if (newIdShort != null)
				msgs = ((InternalEObject)newIdShort).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.CONCEPT_DICTIONARY_T__ID_SHORT, null, msgs);
			msgs = basicSetIdShort(newIdShort, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.CONCEPT_DICTIONARY_T__ID_SHORT, newIdShort, newIdShort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCategory(String newCategory) {
		String oldCategory = category;
		category = newCategory;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.CONCEPT_DICTIONARY_T__CATEGORY, oldCategory, category));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LangStringsT getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDescription(LangStringsT newDescription, NotificationChain msgs) {
		LangStringsT oldDescription = description;
		description = newDescription;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.CONCEPT_DICTIONARY_T__DESCRIPTION, oldDescription, newDescription);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(LangStringsT newDescription) {
		if (newDescription != description) {
			NotificationChain msgs = null;
			if (description != null)
				msgs = ((InternalEObject)description).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.CONCEPT_DICTIONARY_T__DESCRIPTION, null, msgs);
			if (newDescription != null)
				msgs = ((InternalEObject)newDescription).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.CONCEPT_DICTIONARY_T__DESCRIPTION, null, msgs);
			msgs = basicSetDescription(newDescription, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.CONCEPT_DICTIONARY_T__DESCRIPTION, newDescription, newDescription));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getParent() {
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParent(String newParent) {
		String oldParent = parent;
		parent = newParent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.CONCEPT_DICTIONARY_T__PARENT, oldParent, parent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConceptDescriptionsRefT getConceptDescriptionRefs() {
		return conceptDescriptionRefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConceptDescriptionRefs(ConceptDescriptionsRefT newConceptDescriptionRefs, NotificationChain msgs) {
		ConceptDescriptionsRefT oldConceptDescriptionRefs = conceptDescriptionRefs;
		conceptDescriptionRefs = newConceptDescriptionRefs;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.CONCEPT_DICTIONARY_T__CONCEPT_DESCRIPTION_REFS, oldConceptDescriptionRefs, newConceptDescriptionRefs);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConceptDescriptionRefs(ConceptDescriptionsRefT newConceptDescriptionRefs) {
		if (newConceptDescriptionRefs != conceptDescriptionRefs) {
			NotificationChain msgs = null;
			if (conceptDescriptionRefs != null)
				msgs = ((InternalEObject)conceptDescriptionRefs).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.CONCEPT_DICTIONARY_T__CONCEPT_DESCRIPTION_REFS, null, msgs);
			if (newConceptDescriptionRefs != null)
				msgs = ((InternalEObject)newConceptDescriptionRefs).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.CONCEPT_DICTIONARY_T__CONCEPT_DESCRIPTION_REFS, null, msgs);
			msgs = basicSetConceptDescriptionRefs(newConceptDescriptionRefs, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.CONCEPT_DICTIONARY_T__CONCEPT_DESCRIPTION_REFS, newConceptDescriptionRefs, newConceptDescriptionRefs));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.CONCEPT_DICTIONARY_T__ID_SHORT:
				return basicSetIdShort(null, msgs);
			case _0Package.CONCEPT_DICTIONARY_T__DESCRIPTION:
				return basicSetDescription(null, msgs);
			case _0Package.CONCEPT_DICTIONARY_T__CONCEPT_DESCRIPTION_REFS:
				return basicSetConceptDescriptionRefs(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.CONCEPT_DICTIONARY_T__ID_SHORT:
				return getIdShort();
			case _0Package.CONCEPT_DICTIONARY_T__CATEGORY:
				return getCategory();
			case _0Package.CONCEPT_DICTIONARY_T__DESCRIPTION:
				return getDescription();
			case _0Package.CONCEPT_DICTIONARY_T__PARENT:
				return getParent();
			case _0Package.CONCEPT_DICTIONARY_T__CONCEPT_DESCRIPTION_REFS:
				return getConceptDescriptionRefs();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.CONCEPT_DICTIONARY_T__ID_SHORT:
				setIdShort((IdShortT)newValue);
				return;
			case _0Package.CONCEPT_DICTIONARY_T__CATEGORY:
				setCategory((String)newValue);
				return;
			case _0Package.CONCEPT_DICTIONARY_T__DESCRIPTION:
				setDescription((LangStringsT)newValue);
				return;
			case _0Package.CONCEPT_DICTIONARY_T__PARENT:
				setParent((String)newValue);
				return;
			case _0Package.CONCEPT_DICTIONARY_T__CONCEPT_DESCRIPTION_REFS:
				setConceptDescriptionRefs((ConceptDescriptionsRefT)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.CONCEPT_DICTIONARY_T__ID_SHORT:
				setIdShort((IdShortT)null);
				return;
			case _0Package.CONCEPT_DICTIONARY_T__CATEGORY:
				setCategory(CATEGORY_EDEFAULT);
				return;
			case _0Package.CONCEPT_DICTIONARY_T__DESCRIPTION:
				setDescription((LangStringsT)null);
				return;
			case _0Package.CONCEPT_DICTIONARY_T__PARENT:
				setParent(PARENT_EDEFAULT);
				return;
			case _0Package.CONCEPT_DICTIONARY_T__CONCEPT_DESCRIPTION_REFS:
				setConceptDescriptionRefs((ConceptDescriptionsRefT)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.CONCEPT_DICTIONARY_T__ID_SHORT:
				return idShort != null;
			case _0Package.CONCEPT_DICTIONARY_T__CATEGORY:
				return CATEGORY_EDEFAULT == null ? category != null : !CATEGORY_EDEFAULT.equals(category);
			case _0Package.CONCEPT_DICTIONARY_T__DESCRIPTION:
				return description != null;
			case _0Package.CONCEPT_DICTIONARY_T__PARENT:
				return PARENT_EDEFAULT == null ? parent != null : !PARENT_EDEFAULT.equals(parent);
			case _0Package.CONCEPT_DICTIONARY_T__CONCEPT_DESCRIPTION_REFS:
				return conceptDescriptionRefs != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (category: ");
		result.append(category);
		result.append(", parent: ");
		result.append(parent);
		result.append(')');
		return result.toString();
	}

} //ConceptDictionaryTImpl
