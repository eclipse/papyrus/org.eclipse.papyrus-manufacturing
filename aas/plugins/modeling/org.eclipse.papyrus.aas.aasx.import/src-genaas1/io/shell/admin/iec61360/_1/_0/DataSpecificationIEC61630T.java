/**
 * ****************************************************************************
 *   Copyright (c) 2023 CEA LIST.
 *  
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License 2.0
 *   which accompanies this distribution, and is available at
 *   https://www.eclipse.org/legal/epl-2.0/
 *  
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *    Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *  
 *  ****************************************************************************
 */
package io.shell.admin.iec61360._1._0;

import io.shell.admin.aas._1._0.LangStringsT;
import io.shell.admin.aas._1._0.ReferenceT;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Specification IEC61630T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getPreferredName <em>Preferred Name</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getShortName <em>Short Name</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getUnit <em>Unit</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getUnitId <em>Unit Id</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getValueFormat <em>Value Format</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getSourceOfDefinition <em>Source Of Definition</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getSymbol <em>Symbol</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getDataType <em>Data Type</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getDefinition <em>Definition</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getValueList <em>Value List</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getCode <em>Code</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.iec61360._1._0._0Package#getDataSpecificationIEC61630T()
 * @model extendedMetaData="name='dataSpecificationIEC61630_t' kind='elementOnly'"
 * @generated
 */
public interface DataSpecificationIEC61630T extends EObject {
	/**
	 * Returns the value of the '<em><b>Preferred Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preferred Name</em>' containment reference.
	 * @see #setPreferredName(LangStringsT)
	 * @see io.shell.admin.iec61360._1._0._0Package#getDataSpecificationIEC61630T_PreferredName()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='preferredName' namespace='##targetNamespace'"
	 * @generated
	 */
	LangStringsT getPreferredName();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getPreferredName <em>Preferred Name</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preferred Name</em>' containment reference.
	 * @see #getPreferredName()
	 * @generated
	 */
	void setPreferredName(LangStringsT value);

	/**
	 * Returns the value of the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Short Name</em>' attribute.
	 * @see #setShortName(String)
	 * @see io.shell.admin.iec61360._1._0._0Package#getDataSpecificationIEC61630T_ShortName()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='shortName' namespace='##targetNamespace'"
	 * @generated
	 */
	String getShortName();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getShortName <em>Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Short Name</em>' attribute.
	 * @see #getShortName()
	 * @generated
	 */
	void setShortName(String value);

	/**
	 * Returns the value of the '<em><b>Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit</em>' attribute.
	 * @see #setUnit(String)
	 * @see io.shell.admin.iec61360._1._0._0Package#getDataSpecificationIEC61630T_Unit()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='unit' namespace='##targetNamespace'"
	 * @generated
	 */
	String getUnit();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getUnit <em>Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit</em>' attribute.
	 * @see #getUnit()
	 * @generated
	 */
	void setUnit(String value);

	/**
	 * Returns the value of the '<em><b>Unit Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit Id</em>' containment reference.
	 * @see #setUnitId(ReferenceT)
	 * @see io.shell.admin.iec61360._1._0._0Package#getDataSpecificationIEC61630T_UnitId()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unitId' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getUnitId();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getUnitId <em>Unit Id</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit Id</em>' containment reference.
	 * @see #getUnitId()
	 * @generated
	 */
	void setUnitId(ReferenceT value);

	/**
	 * Returns the value of the '<em><b>Value Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Format</em>' attribute.
	 * @see #setValueFormat(String)
	 * @see io.shell.admin.iec61360._1._0._0Package#getDataSpecificationIEC61630T_ValueFormat()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='valueFormat' namespace='##targetNamespace'"
	 * @generated
	 */
	String getValueFormat();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getValueFormat <em>Value Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Format</em>' attribute.
	 * @see #getValueFormat()
	 * @generated
	 */
	void setValueFormat(String value);

	/**
	 * Returns the value of the '<em><b>Source Of Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Of Definition</em>' containment reference.
	 * @see #setSourceOfDefinition(LangStringsT)
	 * @see io.shell.admin.iec61360._1._0._0Package#getDataSpecificationIEC61630T_SourceOfDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='sourceOfDefinition' namespace='##targetNamespace'"
	 * @generated
	 */
	LangStringsT getSourceOfDefinition();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getSourceOfDefinition <em>Source Of Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Of Definition</em>' containment reference.
	 * @see #getSourceOfDefinition()
	 * @generated
	 */
	void setSourceOfDefinition(LangStringsT value);

	/**
	 * Returns the value of the '<em><b>Symbol</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symbol</em>' attribute.
	 * @see #setSymbol(String)
	 * @see io.shell.admin.iec61360._1._0._0Package#getDataSpecificationIEC61630T_Symbol()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='symbol' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSymbol();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getSymbol <em>Symbol</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Symbol</em>' attribute.
	 * @see #getSymbol()
	 * @generated
	 */
	void setSymbol(String value);

	/**
	 * Returns the value of the '<em><b>Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Type</em>' attribute.
	 * @see #setDataType(String)
	 * @see io.shell.admin.iec61360._1._0._0Package#getDataSpecificationIEC61630T_DataType()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='dataType' namespace='##targetNamespace'"
	 * @generated
	 */
	String getDataType();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getDataType <em>Data Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Type</em>' attribute.
	 * @see #getDataType()
	 * @generated
	 */
	void setDataType(String value);

	/**
	 * Returns the value of the '<em><b>Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definition</em>' containment reference.
	 * @see #setDefinition(LangStringsT)
	 * @see io.shell.admin.iec61360._1._0._0Package#getDataSpecificationIEC61630T_Definition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='definition' namespace='##targetNamespace'"
	 * @generated
	 */
	LangStringsT getDefinition();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getDefinition <em>Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Definition</em>' containment reference.
	 * @see #getDefinition()
	 * @generated
	 */
	void setDefinition(LangStringsT value);

	/**
	 * Returns the value of the '<em><b>Value List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value List</em>' containment reference.
	 * @see #setValueList(ValueListT)
	 * @see io.shell.admin.iec61360._1._0._0Package#getDataSpecificationIEC61630T_ValueList()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='valueList' namespace='##targetNamespace'"
	 * @generated
	 */
	ValueListT getValueList();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getValueList <em>Value List</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value List</em>' containment reference.
	 * @see #getValueList()
	 * @generated
	 */
	void setValueList(ValueListT value);

	/**
	 * Returns the value of the '<em><b>Code</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' containment reference.
	 * @see #setCode(CodeT)
	 * @see io.shell.admin.iec61360._1._0._0Package#getDataSpecificationIEC61630T_Code()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='code' namespace='##targetNamespace'"
	 * @generated
	 */
	CodeT getCode();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getCode <em>Code</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' containment reference.
	 * @see #getCode()
	 * @generated
	 */
	void setCode(CodeT value);

} // DataSpecificationIEC61630T
