/**
 * ****************************************************************************
 *   Copyright (c) 2023 CEA LIST.
 *  
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License 2.0
 *   which accompanies this distribution, and is available at
 *   https://www.eclipse.org/legal/epl-2.0/
 *  
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *    Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *  
 *  ****************************************************************************
 */
package io.shell.admin.iec61360._1._0.impl;

import io.shell.admin.aas._1._0.LangStringsT;
import io.shell.admin.aas._1._0.ReferenceT;

import io.shell.admin.iec61360._1._0.CodeT;
import io.shell.admin.iec61360._1._0.DocumentRoot;
import io.shell.admin.iec61360._1._0.ValueListT;
import io.shell.admin.iec61360._1._0._0Package;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EStringToStringMapEntryImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Document Root</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DocumentRootImpl#getMixed <em>Mixed</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DocumentRootImpl#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DocumentRootImpl#getXSISchemaLocation <em>XSI Schema Location</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DocumentRootImpl#getCode <em>Code</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DocumentRootImpl#getDataType <em>Data Type</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DocumentRootImpl#getDefinition <em>Definition</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DocumentRootImpl#getPreferredName <em>Preferred Name</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DocumentRootImpl#getShortName <em>Short Name</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DocumentRootImpl#getSourceOfDefinition <em>Source Of Definition</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DocumentRootImpl#getSymbol <em>Symbol</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DocumentRootImpl#getUnit <em>Unit</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DocumentRootImpl#getUnitId <em>Unit Id</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DocumentRootImpl#getValueFormat <em>Value Format</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DocumentRootImpl#getValueList <em>Value List</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._1._0.impl.DocumentRootImpl#getValueType <em>Value Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DocumentRootImpl extends MinimalEObjectImpl.Container implements DocumentRoot {
	/**
	 * The cached value of the '{@link #getMixed() <em>Mixed</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMixed()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap mixed;

	/**
	 * The cached value of the '{@link #getXMLNSPrefixMap() <em>XMLNS Prefix Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXMLNSPrefixMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> xMLNSPrefixMap;

	/**
	 * The cached value of the '{@link #getXSISchemaLocation() <em>XSI Schema Location</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXSISchemaLocation()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> xSISchemaLocation;

	/**
	 * The default value of the '{@link #getDataType() <em>Data Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataType()
	 * @generated
	 * @ordered
	 */
	protected static final String DATA_TYPE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getShortName() <em>Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShortName()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORT_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getSymbol() <em>Symbol</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSymbol()
	 * @generated
	 * @ordered
	 */
	protected static final String SYMBOL_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getUnit() <em>Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnit()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIT_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getValueFormat() <em>Value Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueFormat()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_FORMAT_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DocumentRootImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.DOCUMENT_ROOT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getMixed() {
		if (mixed == null) {
			mixed = new BasicFeatureMap(this, _0Package.DOCUMENT_ROOT__MIXED);
		}
		return mixed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, String> getXMLNSPrefixMap() {
		if (xMLNSPrefixMap == null) {
			xMLNSPrefixMap = new EcoreEMap<String,String>(EcorePackage.Literals.ESTRING_TO_STRING_MAP_ENTRY, EStringToStringMapEntryImpl.class, this, _0Package.DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
		}
		return xMLNSPrefixMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, String> getXSISchemaLocation() {
		if (xSISchemaLocation == null) {
			xSISchemaLocation = new EcoreEMap<String,String>(EcorePackage.Literals.ESTRING_TO_STRING_MAP_ENTRY, EStringToStringMapEntryImpl.class, this, _0Package.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
		}
		return xSISchemaLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeT getCode() {
		return (CodeT)getMixed().get(_0Package.Literals.DOCUMENT_ROOT__CODE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCode(CodeT newCode, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(_0Package.Literals.DOCUMENT_ROOT__CODE, newCode, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCode(CodeT newCode) {
		((FeatureMap.Internal)getMixed()).set(_0Package.Literals.DOCUMENT_ROOT__CODE, newCode);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDataType() {
		return (String)getMixed().get(_0Package.Literals.DOCUMENT_ROOT__DATA_TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataType(String newDataType) {
		((FeatureMap.Internal)getMixed()).set(_0Package.Literals.DOCUMENT_ROOT__DATA_TYPE, newDataType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LangStringsT getDefinition() {
		return (LangStringsT)getMixed().get(_0Package.Literals.DOCUMENT_ROOT__DEFINITION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefinition(LangStringsT newDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(_0Package.Literals.DOCUMENT_ROOT__DEFINITION, newDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefinition(LangStringsT newDefinition) {
		((FeatureMap.Internal)getMixed()).set(_0Package.Literals.DOCUMENT_ROOT__DEFINITION, newDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LangStringsT getPreferredName() {
		return (LangStringsT)getMixed().get(_0Package.Literals.DOCUMENT_ROOT__PREFERRED_NAME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPreferredName(LangStringsT newPreferredName, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(_0Package.Literals.DOCUMENT_ROOT__PREFERRED_NAME, newPreferredName, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreferredName(LangStringsT newPreferredName) {
		((FeatureMap.Internal)getMixed()).set(_0Package.Literals.DOCUMENT_ROOT__PREFERRED_NAME, newPreferredName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getShortName() {
		return (String)getMixed().get(_0Package.Literals.DOCUMENT_ROOT__SHORT_NAME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShortName(String newShortName) {
		((FeatureMap.Internal)getMixed()).set(_0Package.Literals.DOCUMENT_ROOT__SHORT_NAME, newShortName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LangStringsT getSourceOfDefinition() {
		return (LangStringsT)getMixed().get(_0Package.Literals.DOCUMENT_ROOT__SOURCE_OF_DEFINITION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSourceOfDefinition(LangStringsT newSourceOfDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(_0Package.Literals.DOCUMENT_ROOT__SOURCE_OF_DEFINITION, newSourceOfDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceOfDefinition(LangStringsT newSourceOfDefinition) {
		((FeatureMap.Internal)getMixed()).set(_0Package.Literals.DOCUMENT_ROOT__SOURCE_OF_DEFINITION, newSourceOfDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSymbol() {
		return (String)getMixed().get(_0Package.Literals.DOCUMENT_ROOT__SYMBOL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSymbol(String newSymbol) {
		((FeatureMap.Internal)getMixed()).set(_0Package.Literals.DOCUMENT_ROOT__SYMBOL, newSymbol);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUnit() {
		return (String)getMixed().get(_0Package.Literals.DOCUMENT_ROOT__UNIT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnit(String newUnit) {
		((FeatureMap.Internal)getMixed()).set(_0Package.Literals.DOCUMENT_ROOT__UNIT, newUnit);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceT getUnitId() {
		return (ReferenceT)getMixed().get(_0Package.Literals.DOCUMENT_ROOT__UNIT_ID, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnitId(ReferenceT newUnitId, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(_0Package.Literals.DOCUMENT_ROOT__UNIT_ID, newUnitId, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnitId(ReferenceT newUnitId) {
		((FeatureMap.Internal)getMixed()).set(_0Package.Literals.DOCUMENT_ROOT__UNIT_ID, newUnitId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValueFormat() {
		return (String)getMixed().get(_0Package.Literals.DOCUMENT_ROOT__VALUE_FORMAT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueFormat(String newValueFormat) {
		((FeatureMap.Internal)getMixed()).set(_0Package.Literals.DOCUMENT_ROOT__VALUE_FORMAT, newValueFormat);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueListT getValueList() {
		return (ValueListT)getMixed().get(_0Package.Literals.DOCUMENT_ROOT__VALUE_LIST, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetValueList(ValueListT newValueList, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(_0Package.Literals.DOCUMENT_ROOT__VALUE_LIST, newValueList, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueList(ValueListT newValueList) {
		((FeatureMap.Internal)getMixed()).set(_0Package.Literals.DOCUMENT_ROOT__VALUE_LIST, newValueList);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueListT getValueType() {
		return (ValueListT)getMixed().get(_0Package.Literals.DOCUMENT_ROOT__VALUE_TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetValueType(ValueListT newValueType, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(_0Package.Literals.DOCUMENT_ROOT__VALUE_TYPE, newValueType, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueType(ValueListT newValueType) {
		((FeatureMap.Internal)getMixed()).set(_0Package.Literals.DOCUMENT_ROOT__VALUE_TYPE, newValueType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.DOCUMENT_ROOT__MIXED:
				return ((InternalEList<?>)getMixed()).basicRemove(otherEnd, msgs);
			case _0Package.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				return ((InternalEList<?>)getXMLNSPrefixMap()).basicRemove(otherEnd, msgs);
			case _0Package.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				return ((InternalEList<?>)getXSISchemaLocation()).basicRemove(otherEnd, msgs);
			case _0Package.DOCUMENT_ROOT__CODE:
				return basicSetCode(null, msgs);
			case _0Package.DOCUMENT_ROOT__DEFINITION:
				return basicSetDefinition(null, msgs);
			case _0Package.DOCUMENT_ROOT__PREFERRED_NAME:
				return basicSetPreferredName(null, msgs);
			case _0Package.DOCUMENT_ROOT__SOURCE_OF_DEFINITION:
				return basicSetSourceOfDefinition(null, msgs);
			case _0Package.DOCUMENT_ROOT__UNIT_ID:
				return basicSetUnitId(null, msgs);
			case _0Package.DOCUMENT_ROOT__VALUE_LIST:
				return basicSetValueList(null, msgs);
			case _0Package.DOCUMENT_ROOT__VALUE_TYPE:
				return basicSetValueType(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.DOCUMENT_ROOT__MIXED:
				if (coreType) return getMixed();
				return ((FeatureMap.Internal)getMixed()).getWrapper();
			case _0Package.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				if (coreType) return getXMLNSPrefixMap();
				else return getXMLNSPrefixMap().map();
			case _0Package.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				if (coreType) return getXSISchemaLocation();
				else return getXSISchemaLocation().map();
			case _0Package.DOCUMENT_ROOT__CODE:
				return getCode();
			case _0Package.DOCUMENT_ROOT__DATA_TYPE:
				return getDataType();
			case _0Package.DOCUMENT_ROOT__DEFINITION:
				return getDefinition();
			case _0Package.DOCUMENT_ROOT__PREFERRED_NAME:
				return getPreferredName();
			case _0Package.DOCUMENT_ROOT__SHORT_NAME:
				return getShortName();
			case _0Package.DOCUMENT_ROOT__SOURCE_OF_DEFINITION:
				return getSourceOfDefinition();
			case _0Package.DOCUMENT_ROOT__SYMBOL:
				return getSymbol();
			case _0Package.DOCUMENT_ROOT__UNIT:
				return getUnit();
			case _0Package.DOCUMENT_ROOT__UNIT_ID:
				return getUnitId();
			case _0Package.DOCUMENT_ROOT__VALUE_FORMAT:
				return getValueFormat();
			case _0Package.DOCUMENT_ROOT__VALUE_LIST:
				return getValueList();
			case _0Package.DOCUMENT_ROOT__VALUE_TYPE:
				return getValueType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.DOCUMENT_ROOT__MIXED:
				((FeatureMap.Internal)getMixed()).set(newValue);
				return;
			case _0Package.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				((EStructuralFeature.Setting)getXMLNSPrefixMap()).set(newValue);
				return;
			case _0Package.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				((EStructuralFeature.Setting)getXSISchemaLocation()).set(newValue);
				return;
			case _0Package.DOCUMENT_ROOT__CODE:
				setCode((CodeT)newValue);
				return;
			case _0Package.DOCUMENT_ROOT__DATA_TYPE:
				setDataType((String)newValue);
				return;
			case _0Package.DOCUMENT_ROOT__DEFINITION:
				setDefinition((LangStringsT)newValue);
				return;
			case _0Package.DOCUMENT_ROOT__PREFERRED_NAME:
				setPreferredName((LangStringsT)newValue);
				return;
			case _0Package.DOCUMENT_ROOT__SHORT_NAME:
				setShortName((String)newValue);
				return;
			case _0Package.DOCUMENT_ROOT__SOURCE_OF_DEFINITION:
				setSourceOfDefinition((LangStringsT)newValue);
				return;
			case _0Package.DOCUMENT_ROOT__SYMBOL:
				setSymbol((String)newValue);
				return;
			case _0Package.DOCUMENT_ROOT__UNIT:
				setUnit((String)newValue);
				return;
			case _0Package.DOCUMENT_ROOT__UNIT_ID:
				setUnitId((ReferenceT)newValue);
				return;
			case _0Package.DOCUMENT_ROOT__VALUE_FORMAT:
				setValueFormat((String)newValue);
				return;
			case _0Package.DOCUMENT_ROOT__VALUE_LIST:
				setValueList((ValueListT)newValue);
				return;
			case _0Package.DOCUMENT_ROOT__VALUE_TYPE:
				setValueType((ValueListT)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.DOCUMENT_ROOT__MIXED:
				getMixed().clear();
				return;
			case _0Package.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				getXMLNSPrefixMap().clear();
				return;
			case _0Package.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				getXSISchemaLocation().clear();
				return;
			case _0Package.DOCUMENT_ROOT__CODE:
				setCode((CodeT)null);
				return;
			case _0Package.DOCUMENT_ROOT__DATA_TYPE:
				setDataType(DATA_TYPE_EDEFAULT);
				return;
			case _0Package.DOCUMENT_ROOT__DEFINITION:
				setDefinition((LangStringsT)null);
				return;
			case _0Package.DOCUMENT_ROOT__PREFERRED_NAME:
				setPreferredName((LangStringsT)null);
				return;
			case _0Package.DOCUMENT_ROOT__SHORT_NAME:
				setShortName(SHORT_NAME_EDEFAULT);
				return;
			case _0Package.DOCUMENT_ROOT__SOURCE_OF_DEFINITION:
				setSourceOfDefinition((LangStringsT)null);
				return;
			case _0Package.DOCUMENT_ROOT__SYMBOL:
				setSymbol(SYMBOL_EDEFAULT);
				return;
			case _0Package.DOCUMENT_ROOT__UNIT:
				setUnit(UNIT_EDEFAULT);
				return;
			case _0Package.DOCUMENT_ROOT__UNIT_ID:
				setUnitId((ReferenceT)null);
				return;
			case _0Package.DOCUMENT_ROOT__VALUE_FORMAT:
				setValueFormat(VALUE_FORMAT_EDEFAULT);
				return;
			case _0Package.DOCUMENT_ROOT__VALUE_LIST:
				setValueList((ValueListT)null);
				return;
			case _0Package.DOCUMENT_ROOT__VALUE_TYPE:
				setValueType((ValueListT)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.DOCUMENT_ROOT__MIXED:
				return mixed != null && !mixed.isEmpty();
			case _0Package.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				return xMLNSPrefixMap != null && !xMLNSPrefixMap.isEmpty();
			case _0Package.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				return xSISchemaLocation != null && !xSISchemaLocation.isEmpty();
			case _0Package.DOCUMENT_ROOT__CODE:
				return getCode() != null;
			case _0Package.DOCUMENT_ROOT__DATA_TYPE:
				return DATA_TYPE_EDEFAULT == null ? getDataType() != null : !DATA_TYPE_EDEFAULT.equals(getDataType());
			case _0Package.DOCUMENT_ROOT__DEFINITION:
				return getDefinition() != null;
			case _0Package.DOCUMENT_ROOT__PREFERRED_NAME:
				return getPreferredName() != null;
			case _0Package.DOCUMENT_ROOT__SHORT_NAME:
				return SHORT_NAME_EDEFAULT == null ? getShortName() != null : !SHORT_NAME_EDEFAULT.equals(getShortName());
			case _0Package.DOCUMENT_ROOT__SOURCE_OF_DEFINITION:
				return getSourceOfDefinition() != null;
			case _0Package.DOCUMENT_ROOT__SYMBOL:
				return SYMBOL_EDEFAULT == null ? getSymbol() != null : !SYMBOL_EDEFAULT.equals(getSymbol());
			case _0Package.DOCUMENT_ROOT__UNIT:
				return UNIT_EDEFAULT == null ? getUnit() != null : !UNIT_EDEFAULT.equals(getUnit());
			case _0Package.DOCUMENT_ROOT__UNIT_ID:
				return getUnitId() != null;
			case _0Package.DOCUMENT_ROOT__VALUE_FORMAT:
				return VALUE_FORMAT_EDEFAULT == null ? getValueFormat() != null : !VALUE_FORMAT_EDEFAULT.equals(getValueFormat());
			case _0Package.DOCUMENT_ROOT__VALUE_LIST:
				return getValueList() != null;
			case _0Package.DOCUMENT_ROOT__VALUE_TYPE:
				return getValueType() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (mixed: ");
		result.append(mixed);
		result.append(')');
		return result.toString();
	}

} //DocumentRootImpl
