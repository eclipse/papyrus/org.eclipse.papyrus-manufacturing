/**
 * ****************************************************************************
 *   Copyright (c) 2023 CEA LIST.
 *  
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License 2.0
 *   which accompanies this distribution, and is available at
 *   https://www.eclipse.org/legal/epl-2.0/
 *  
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *    Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *  
 *  ****************************************************************************
 */
package io.shell.admin.iec61360._1._0;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see io.shell.admin.iec61360._1._0._0Factory
 * @model kind="package"
 * @generated
 */
public interface _0Package extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "_0";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.admin-shell.io/IEC61360/1/0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "_0";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	_0Package eINSTANCE = io.shell.admin.iec61360._1._0.impl._0PackageImpl.init();

	/**
	 * The meta object id for the '{@link io.shell.admin.iec61360._1._0.impl.CodeTImpl <em>Code T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.iec61360._1._0.impl.CodeTImpl
	 * @see io.shell.admin.iec61360._1._0.impl._0PackageImpl#getCodeT()
	 * @generated
	 */
	int CODE_T = 0;

	/**
	 * The number of structural features of the '<em>Code T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_T_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Code T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.iec61360._1._0.impl.DataSpecificationIEC61630TImpl <em>Data Specification IEC61630T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.iec61360._1._0.impl.DataSpecificationIEC61630TImpl
	 * @see io.shell.admin.iec61360._1._0.impl._0PackageImpl#getDataSpecificationIEC61630T()
	 * @generated
	 */
	int DATA_SPECIFICATION_IEC61630T = 1;

	/**
	 * The feature id for the '<em><b>Preferred Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T__PREFERRED_NAME = 0;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T__SHORT_NAME = 1;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T__UNIT = 2;

	/**
	 * The feature id for the '<em><b>Unit Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T__UNIT_ID = 3;

	/**
	 * The feature id for the '<em><b>Value Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T__VALUE_FORMAT = 4;

	/**
	 * The feature id for the '<em><b>Source Of Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T__SOURCE_OF_DEFINITION = 5;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T__SYMBOL = 6;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T__DATA_TYPE = 7;

	/**
	 * The feature id for the '<em><b>Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T__DEFINITION = 8;

	/**
	 * The feature id for the '<em><b>Value List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T__VALUE_LIST = 9;

	/**
	 * The feature id for the '<em><b>Code</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T__CODE = 10;

	/**
	 * The number of structural features of the '<em>Data Specification IEC61630T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T_FEATURE_COUNT = 11;

	/**
	 * The number of operations of the '<em>Data Specification IEC61630T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.iec61360._1._0.impl.ValueListTImpl <em>Value List T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.iec61360._1._0.impl.ValueListTImpl
	 * @see io.shell.admin.iec61360._1._0.impl._0PackageImpl#getValueListT()
	 * @generated
	 */
	int VALUE_LIST_T = 2;

	/**
	 * The number of structural features of the '<em>Value List T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_LIST_T_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Value List T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_LIST_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.iec61360._1._0.impl.DocumentRootImpl <em>Document Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.iec61360._1._0.impl.DocumentRootImpl
	 * @see io.shell.admin.iec61360._1._0.impl._0PackageImpl#getDocumentRoot()
	 * @generated
	 */
	int DOCUMENT_ROOT = 3;

	/**
	 * The feature id for the '<em><b>Mixed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__MIXED = 0;

	/**
	 * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

	/**
	 * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

	/**
	 * The feature id for the '<em><b>Code</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__CODE = 3;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DATA_TYPE = 4;

	/**
	 * The feature id for the '<em><b>Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DEFINITION = 5;

	/**
	 * The feature id for the '<em><b>Preferred Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__PREFERRED_NAME = 6;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__SHORT_NAME = 7;

	/**
	 * The feature id for the '<em><b>Source Of Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__SOURCE_OF_DEFINITION = 8;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__SYMBOL = 9;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__UNIT = 10;

	/**
	 * The feature id for the '<em><b>Unit Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__UNIT_ID = 11;

	/**
	 * The feature id for the '<em><b>Value Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__VALUE_FORMAT = 12;

	/**
	 * The feature id for the '<em><b>Value List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__VALUE_LIST = 13;

	/**
	 * The feature id for the '<em><b>Value Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__VALUE_TYPE = 14;

	/**
	 * The number of structural features of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_FEATURE_COUNT = 15;

	/**
	 * The number of operations of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link io.shell.admin.iec61360._1._0.CodeT <em>Code T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Code T</em>'.
	 * @see io.shell.admin.iec61360._1._0.CodeT
	 * @generated
	 */
	EClass getCodeT();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T <em>Data Specification IEC61630T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Specification IEC61630T</em>'.
	 * @see io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T
	 * @generated
	 */
	EClass getDataSpecificationIEC61630T();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getPreferredName <em>Preferred Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Preferred Name</em>'.
	 * @see io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getPreferredName()
	 * @see #getDataSpecificationIEC61630T()
	 * @generated
	 */
	EReference getDataSpecificationIEC61630T_PreferredName();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getShortName <em>Short Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Short Name</em>'.
	 * @see io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getShortName()
	 * @see #getDataSpecificationIEC61630T()
	 * @generated
	 */
	EAttribute getDataSpecificationIEC61630T_ShortName();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getUnit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unit</em>'.
	 * @see io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getUnit()
	 * @see #getDataSpecificationIEC61630T()
	 * @generated
	 */
	EAttribute getDataSpecificationIEC61630T_Unit();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getUnitId <em>Unit Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Unit Id</em>'.
	 * @see io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getUnitId()
	 * @see #getDataSpecificationIEC61630T()
	 * @generated
	 */
	EReference getDataSpecificationIEC61630T_UnitId();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getValueFormat <em>Value Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Format</em>'.
	 * @see io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getValueFormat()
	 * @see #getDataSpecificationIEC61630T()
	 * @generated
	 */
	EAttribute getDataSpecificationIEC61630T_ValueFormat();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getSourceOfDefinition <em>Source Of Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Source Of Definition</em>'.
	 * @see io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getSourceOfDefinition()
	 * @see #getDataSpecificationIEC61630T()
	 * @generated
	 */
	EReference getDataSpecificationIEC61630T_SourceOfDefinition();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getSymbol <em>Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Symbol</em>'.
	 * @see io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getSymbol()
	 * @see #getDataSpecificationIEC61630T()
	 * @generated
	 */
	EAttribute getDataSpecificationIEC61630T_Symbol();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getDataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Data Type</em>'.
	 * @see io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getDataType()
	 * @see #getDataSpecificationIEC61630T()
	 * @generated
	 */
	EAttribute getDataSpecificationIEC61630T_DataType();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getDefinition <em>Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Definition</em>'.
	 * @see io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getDefinition()
	 * @see #getDataSpecificationIEC61630T()
	 * @generated
	 */
	EReference getDataSpecificationIEC61630T_Definition();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getValueList <em>Value List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value List</em>'.
	 * @see io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getValueList()
	 * @see #getDataSpecificationIEC61630T()
	 * @generated
	 */
	EReference getDataSpecificationIEC61630T_ValueList();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Code</em>'.
	 * @see io.shell.admin.iec61360._1._0.DataSpecificationIEC61630T#getCode()
	 * @see #getDataSpecificationIEC61630T()
	 * @generated
	 */
	EReference getDataSpecificationIEC61630T_Code();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.iec61360._1._0.ValueListT <em>Value List T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value List T</em>'.
	 * @see io.shell.admin.iec61360._1._0.ValueListT
	 * @generated
	 */
	EClass getValueListT();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.iec61360._1._0.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Document Root</em>'.
	 * @see io.shell.admin.iec61360._1._0.DocumentRoot
	 * @generated
	 */
	EClass getDocumentRoot();

	/**
	 * Returns the meta object for the attribute list '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getMixed <em>Mixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Mixed</em>'.
	 * @see io.shell.admin.iec61360._1._0.DocumentRoot#getMixed()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EAttribute getDocumentRoot_Mixed();

	/**
	 * Returns the meta object for the map '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
	 * @see io.shell.admin.iec61360._1._0.DocumentRoot#getXMLNSPrefixMap()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XMLNSPrefixMap();

	/**
	 * Returns the meta object for the map '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XSI Schema Location</em>'.
	 * @see io.shell.admin.iec61360._1._0.DocumentRoot#getXSISchemaLocation()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XSISchemaLocation();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Code</em>'.
	 * @see io.shell.admin.iec61360._1._0.DocumentRoot#getCode()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Code();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getDataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Data Type</em>'.
	 * @see io.shell.admin.iec61360._1._0.DocumentRoot#getDataType()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EAttribute getDocumentRoot_DataType();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getDefinition <em>Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Definition</em>'.
	 * @see io.shell.admin.iec61360._1._0.DocumentRoot#getDefinition()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Definition();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getPreferredName <em>Preferred Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Preferred Name</em>'.
	 * @see io.shell.admin.iec61360._1._0.DocumentRoot#getPreferredName()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_PreferredName();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getShortName <em>Short Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Short Name</em>'.
	 * @see io.shell.admin.iec61360._1._0.DocumentRoot#getShortName()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EAttribute getDocumentRoot_ShortName();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getSourceOfDefinition <em>Source Of Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Source Of Definition</em>'.
	 * @see io.shell.admin.iec61360._1._0.DocumentRoot#getSourceOfDefinition()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_SourceOfDefinition();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getSymbol <em>Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Symbol</em>'.
	 * @see io.shell.admin.iec61360._1._0.DocumentRoot#getSymbol()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EAttribute getDocumentRoot_Symbol();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getUnit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unit</em>'.
	 * @see io.shell.admin.iec61360._1._0.DocumentRoot#getUnit()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EAttribute getDocumentRoot_Unit();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getUnitId <em>Unit Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Unit Id</em>'.
	 * @see io.shell.admin.iec61360._1._0.DocumentRoot#getUnitId()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_UnitId();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getValueFormat <em>Value Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Format</em>'.
	 * @see io.shell.admin.iec61360._1._0.DocumentRoot#getValueFormat()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EAttribute getDocumentRoot_ValueFormat();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getValueList <em>Value List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value List</em>'.
	 * @see io.shell.admin.iec61360._1._0.DocumentRoot#getValueList()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ValueList();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.iec61360._1._0.DocumentRoot#getValueType <em>Value Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value Type</em>'.
	 * @see io.shell.admin.iec61360._1._0.DocumentRoot#getValueType()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ValueType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	_0Factory get_0Factory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link io.shell.admin.iec61360._1._0.impl.CodeTImpl <em>Code T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.iec61360._1._0.impl.CodeTImpl
		 * @see io.shell.admin.iec61360._1._0.impl._0PackageImpl#getCodeT()
		 * @generated
		 */
		EClass CODE_T = eINSTANCE.getCodeT();

		/**
		 * The meta object literal for the '{@link io.shell.admin.iec61360._1._0.impl.DataSpecificationIEC61630TImpl <em>Data Specification IEC61630T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.iec61360._1._0.impl.DataSpecificationIEC61630TImpl
		 * @see io.shell.admin.iec61360._1._0.impl._0PackageImpl#getDataSpecificationIEC61630T()
		 * @generated
		 */
		EClass DATA_SPECIFICATION_IEC61630T = eINSTANCE.getDataSpecificationIEC61630T();

		/**
		 * The meta object literal for the '<em><b>Preferred Name</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SPECIFICATION_IEC61630T__PREFERRED_NAME = eINSTANCE.getDataSpecificationIEC61630T_PreferredName();

		/**
		 * The meta object literal for the '<em><b>Short Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_SPECIFICATION_IEC61630T__SHORT_NAME = eINSTANCE.getDataSpecificationIEC61630T_ShortName();

		/**
		 * The meta object literal for the '<em><b>Unit</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_SPECIFICATION_IEC61630T__UNIT = eINSTANCE.getDataSpecificationIEC61630T_Unit();

		/**
		 * The meta object literal for the '<em><b>Unit Id</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SPECIFICATION_IEC61630T__UNIT_ID = eINSTANCE.getDataSpecificationIEC61630T_UnitId();

		/**
		 * The meta object literal for the '<em><b>Value Format</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_SPECIFICATION_IEC61630T__VALUE_FORMAT = eINSTANCE.getDataSpecificationIEC61630T_ValueFormat();

		/**
		 * The meta object literal for the '<em><b>Source Of Definition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SPECIFICATION_IEC61630T__SOURCE_OF_DEFINITION = eINSTANCE.getDataSpecificationIEC61630T_SourceOfDefinition();

		/**
		 * The meta object literal for the '<em><b>Symbol</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_SPECIFICATION_IEC61630T__SYMBOL = eINSTANCE.getDataSpecificationIEC61630T_Symbol();

		/**
		 * The meta object literal for the '<em><b>Data Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_SPECIFICATION_IEC61630T__DATA_TYPE = eINSTANCE.getDataSpecificationIEC61630T_DataType();

		/**
		 * The meta object literal for the '<em><b>Definition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SPECIFICATION_IEC61630T__DEFINITION = eINSTANCE.getDataSpecificationIEC61630T_Definition();

		/**
		 * The meta object literal for the '<em><b>Value List</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SPECIFICATION_IEC61630T__VALUE_LIST = eINSTANCE.getDataSpecificationIEC61630T_ValueList();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SPECIFICATION_IEC61630T__CODE = eINSTANCE.getDataSpecificationIEC61630T_Code();

		/**
		 * The meta object literal for the '{@link io.shell.admin.iec61360._1._0.impl.ValueListTImpl <em>Value List T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.iec61360._1._0.impl.ValueListTImpl
		 * @see io.shell.admin.iec61360._1._0.impl._0PackageImpl#getValueListT()
		 * @generated
		 */
		EClass VALUE_LIST_T = eINSTANCE.getValueListT();

		/**
		 * The meta object literal for the '{@link io.shell.admin.iec61360._1._0.impl.DocumentRootImpl <em>Document Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.iec61360._1._0.impl.DocumentRootImpl
		 * @see io.shell.admin.iec61360._1._0.impl._0PackageImpl#getDocumentRoot()
		 * @generated
		 */
		EClass DOCUMENT_ROOT = eINSTANCE.getDocumentRoot();

		/**
		 * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENT_ROOT__MIXED = eINSTANCE.getDocumentRoot_Mixed();

		/**
		 * The meta object literal for the '<em><b>XMLNS Prefix Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XMLNS_PREFIX_MAP = eINSTANCE.getDocumentRoot_XMLNSPrefixMap();

		/**
		 * The meta object literal for the '<em><b>XSI Schema Location</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = eINSTANCE.getDocumentRoot_XSISchemaLocation();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__CODE = eINSTANCE.getDocumentRoot_Code();

		/**
		 * The meta object literal for the '<em><b>Data Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENT_ROOT__DATA_TYPE = eINSTANCE.getDocumentRoot_DataType();

		/**
		 * The meta object literal for the '<em><b>Definition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__DEFINITION = eINSTANCE.getDocumentRoot_Definition();

		/**
		 * The meta object literal for the '<em><b>Preferred Name</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__PREFERRED_NAME = eINSTANCE.getDocumentRoot_PreferredName();

		/**
		 * The meta object literal for the '<em><b>Short Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENT_ROOT__SHORT_NAME = eINSTANCE.getDocumentRoot_ShortName();

		/**
		 * The meta object literal for the '<em><b>Source Of Definition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__SOURCE_OF_DEFINITION = eINSTANCE.getDocumentRoot_SourceOfDefinition();

		/**
		 * The meta object literal for the '<em><b>Symbol</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENT_ROOT__SYMBOL = eINSTANCE.getDocumentRoot_Symbol();

		/**
		 * The meta object literal for the '<em><b>Unit</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENT_ROOT__UNIT = eINSTANCE.getDocumentRoot_Unit();

		/**
		 * The meta object literal for the '<em><b>Unit Id</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__UNIT_ID = eINSTANCE.getDocumentRoot_UnitId();

		/**
		 * The meta object literal for the '<em><b>Value Format</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENT_ROOT__VALUE_FORMAT = eINSTANCE.getDocumentRoot_ValueFormat();

		/**
		 * The meta object literal for the '<em><b>Value List</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__VALUE_LIST = eINSTANCE.getDocumentRoot_ValueList();

		/**
		 * The meta object literal for the '<em><b>Value Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__VALUE_TYPE = eINSTANCE.getDocumentRoot_ValueType();

	}

} //_0Package
