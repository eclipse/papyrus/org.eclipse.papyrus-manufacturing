/**
 * ****************************************************************************
 *   Copyright (c) 2023 CEA LIST.
 *  
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License 2.0
 *   which accompanies this distribution, and is available at
 *   https://www.eclipse.org/legal/epl-2.0/
 *  
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *    Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *  
 *  ****************************************************************************
 */
package io.shell.admin.aas._1._0.impl;

import io.shell.admin.aas._1._0.ConceptDictionariesT;
import io.shell.admin.aas._1._0.ConceptDictionaryT;
import io.shell.admin.aas._1._0._0Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Concept Dictionaries T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._1._0.impl.ConceptDictionariesTImpl#getConceptDictionary <em>Concept Dictionary</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConceptDictionariesTImpl extends MinimalEObjectImpl.Container implements ConceptDictionariesT {
	/**
	 * The cached value of the '{@link #getConceptDictionary() <em>Concept Dictionary</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConceptDictionary()
	 * @generated
	 * @ordered
	 */
	protected EList<ConceptDictionaryT> conceptDictionary;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConceptDictionariesTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.CONCEPT_DICTIONARIES_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConceptDictionaryT> getConceptDictionary() {
		if (conceptDictionary == null) {
			conceptDictionary = new EObjectContainmentEList<ConceptDictionaryT>(ConceptDictionaryT.class, this, _0Package.CONCEPT_DICTIONARIES_T__CONCEPT_DICTIONARY);
		}
		return conceptDictionary;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.CONCEPT_DICTIONARIES_T__CONCEPT_DICTIONARY:
				return ((InternalEList<?>)getConceptDictionary()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.CONCEPT_DICTIONARIES_T__CONCEPT_DICTIONARY:
				return getConceptDictionary();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.CONCEPT_DICTIONARIES_T__CONCEPT_DICTIONARY:
				getConceptDictionary().clear();
				getConceptDictionary().addAll((Collection<? extends ConceptDictionaryT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.CONCEPT_DICTIONARIES_T__CONCEPT_DICTIONARY:
				getConceptDictionary().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.CONCEPT_DICTIONARIES_T__CONCEPT_DICTIONARY:
				return conceptDictionary != null && !conceptDictionary.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ConceptDictionariesTImpl
