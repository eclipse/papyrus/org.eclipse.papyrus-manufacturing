/**
 * ****************************************************************************
 *   Copyright (c) 2023 CEA LIST.
 *  
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License 2.0
 *   which accompanies this distribution, and is available at
 *   https://www.eclipse.org/legal/epl-2.0/
 *  
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *    Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *  
 *  ****************************************************************************
 */
package io.shell.admin.aas._1._0.impl;

import io.shell.admin.aas._1._0.AasenvT;
import io.shell.admin.aas._1._0.AssetAdministrationShellsT;
import io.shell.admin.aas._1._0.AssetsT;
import io.shell.admin.aas._1._0.ConceptDescriptionsT;
import io.shell.admin.aas._1._0.SubmodelsT;
import io.shell.admin.aas._1._0._0Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Aasenv T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._1._0.impl.AasenvTImpl#getAssetAdministrationShells <em>Asset Administration Shells</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.impl.AasenvTImpl#getAssets <em>Assets</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.impl.AasenvTImpl#getSubmodels <em>Submodels</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.impl.AasenvTImpl#getConceptDescriptions <em>Concept Descriptions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AasenvTImpl extends MinimalEObjectImpl.Container implements AasenvT {
	/**
	 * The cached value of the '{@link #getAssetAdministrationShells() <em>Asset Administration Shells</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssetAdministrationShells()
	 * @generated
	 * @ordered
	 */
	protected AssetAdministrationShellsT assetAdministrationShells;

	/**
	 * The cached value of the '{@link #getAssets() <em>Assets</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssets()
	 * @generated
	 * @ordered
	 */
	protected AssetsT assets;

	/**
	 * The cached value of the '{@link #getSubmodels() <em>Submodels</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubmodels()
	 * @generated
	 * @ordered
	 */
	protected SubmodelsT submodels;

	/**
	 * The cached value of the '{@link #getConceptDescriptions() <em>Concept Descriptions</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConceptDescriptions()
	 * @generated
	 * @ordered
	 */
	protected ConceptDescriptionsT conceptDescriptions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AasenvTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.AASENV_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssetAdministrationShellsT getAssetAdministrationShells() {
		return assetAdministrationShells;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssetAdministrationShells(AssetAdministrationShellsT newAssetAdministrationShells, NotificationChain msgs) {
		AssetAdministrationShellsT oldAssetAdministrationShells = assetAdministrationShells;
		assetAdministrationShells = newAssetAdministrationShells;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.AASENV_T__ASSET_ADMINISTRATION_SHELLS, oldAssetAdministrationShells, newAssetAdministrationShells);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssetAdministrationShells(AssetAdministrationShellsT newAssetAdministrationShells) {
		if (newAssetAdministrationShells != assetAdministrationShells) {
			NotificationChain msgs = null;
			if (assetAdministrationShells != null)
				msgs = ((InternalEObject)assetAdministrationShells).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.AASENV_T__ASSET_ADMINISTRATION_SHELLS, null, msgs);
			if (newAssetAdministrationShells != null)
				msgs = ((InternalEObject)newAssetAdministrationShells).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.AASENV_T__ASSET_ADMINISTRATION_SHELLS, null, msgs);
			msgs = basicSetAssetAdministrationShells(newAssetAdministrationShells, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.AASENV_T__ASSET_ADMINISTRATION_SHELLS, newAssetAdministrationShells, newAssetAdministrationShells));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssetsT getAssets() {
		return assets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssets(AssetsT newAssets, NotificationChain msgs) {
		AssetsT oldAssets = assets;
		assets = newAssets;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.AASENV_T__ASSETS, oldAssets, newAssets);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssets(AssetsT newAssets) {
		if (newAssets != assets) {
			NotificationChain msgs = null;
			if (assets != null)
				msgs = ((InternalEObject)assets).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.AASENV_T__ASSETS, null, msgs);
			if (newAssets != null)
				msgs = ((InternalEObject)newAssets).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.AASENV_T__ASSETS, null, msgs);
			msgs = basicSetAssets(newAssets, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.AASENV_T__ASSETS, newAssets, newAssets));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubmodelsT getSubmodels() {
		return submodels;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSubmodels(SubmodelsT newSubmodels, NotificationChain msgs) {
		SubmodelsT oldSubmodels = submodels;
		submodels = newSubmodels;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.AASENV_T__SUBMODELS, oldSubmodels, newSubmodels);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubmodels(SubmodelsT newSubmodels) {
		if (newSubmodels != submodels) {
			NotificationChain msgs = null;
			if (submodels != null)
				msgs = ((InternalEObject)submodels).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.AASENV_T__SUBMODELS, null, msgs);
			if (newSubmodels != null)
				msgs = ((InternalEObject)newSubmodels).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.AASENV_T__SUBMODELS, null, msgs);
			msgs = basicSetSubmodels(newSubmodels, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.AASENV_T__SUBMODELS, newSubmodels, newSubmodels));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConceptDescriptionsT getConceptDescriptions() {
		return conceptDescriptions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConceptDescriptions(ConceptDescriptionsT newConceptDescriptions, NotificationChain msgs) {
		ConceptDescriptionsT oldConceptDescriptions = conceptDescriptions;
		conceptDescriptions = newConceptDescriptions;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.AASENV_T__CONCEPT_DESCRIPTIONS, oldConceptDescriptions, newConceptDescriptions);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConceptDescriptions(ConceptDescriptionsT newConceptDescriptions) {
		if (newConceptDescriptions != conceptDescriptions) {
			NotificationChain msgs = null;
			if (conceptDescriptions != null)
				msgs = ((InternalEObject)conceptDescriptions).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.AASENV_T__CONCEPT_DESCRIPTIONS, null, msgs);
			if (newConceptDescriptions != null)
				msgs = ((InternalEObject)newConceptDescriptions).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.AASENV_T__CONCEPT_DESCRIPTIONS, null, msgs);
			msgs = basicSetConceptDescriptions(newConceptDescriptions, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.AASENV_T__CONCEPT_DESCRIPTIONS, newConceptDescriptions, newConceptDescriptions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.AASENV_T__ASSET_ADMINISTRATION_SHELLS:
				return basicSetAssetAdministrationShells(null, msgs);
			case _0Package.AASENV_T__ASSETS:
				return basicSetAssets(null, msgs);
			case _0Package.AASENV_T__SUBMODELS:
				return basicSetSubmodels(null, msgs);
			case _0Package.AASENV_T__CONCEPT_DESCRIPTIONS:
				return basicSetConceptDescriptions(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.AASENV_T__ASSET_ADMINISTRATION_SHELLS:
				return getAssetAdministrationShells();
			case _0Package.AASENV_T__ASSETS:
				return getAssets();
			case _0Package.AASENV_T__SUBMODELS:
				return getSubmodels();
			case _0Package.AASENV_T__CONCEPT_DESCRIPTIONS:
				return getConceptDescriptions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.AASENV_T__ASSET_ADMINISTRATION_SHELLS:
				setAssetAdministrationShells((AssetAdministrationShellsT)newValue);
				return;
			case _0Package.AASENV_T__ASSETS:
				setAssets((AssetsT)newValue);
				return;
			case _0Package.AASENV_T__SUBMODELS:
				setSubmodels((SubmodelsT)newValue);
				return;
			case _0Package.AASENV_T__CONCEPT_DESCRIPTIONS:
				setConceptDescriptions((ConceptDescriptionsT)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.AASENV_T__ASSET_ADMINISTRATION_SHELLS:
				setAssetAdministrationShells((AssetAdministrationShellsT)null);
				return;
			case _0Package.AASENV_T__ASSETS:
				setAssets((AssetsT)null);
				return;
			case _0Package.AASENV_T__SUBMODELS:
				setSubmodels((SubmodelsT)null);
				return;
			case _0Package.AASENV_T__CONCEPT_DESCRIPTIONS:
				setConceptDescriptions((ConceptDescriptionsT)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.AASENV_T__ASSET_ADMINISTRATION_SHELLS:
				return assetAdministrationShells != null;
			case _0Package.AASENV_T__ASSETS:
				return assets != null;
			case _0Package.AASENV_T__SUBMODELS:
				return submodels != null;
			case _0Package.AASENV_T__CONCEPT_DESCRIPTIONS:
				return conceptDescriptions != null;
		}
		return super.eIsSet(featureID);
	}

} //AasenvTImpl
