/**
 * ****************************************************************************
 *   Copyright (c) 2023 CEA LIST.
 *  
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License 2.0
 *   which accompanies this distribution, and is available at
 *   https://www.eclipse.org/legal/epl-2.0/
 *  
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *    Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *  
 *  ****************************************************************************
 */
package io.shell.admin.aas._1._0;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Asset Administration Shell T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._1._0.AssetAdministrationShellT#getIdShort <em>Id Short</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.AssetAdministrationShellT#getCategory <em>Category</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.AssetAdministrationShellT#getDescription <em>Description</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.AssetAdministrationShellT#getParent <em>Parent</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.AssetAdministrationShellT#getIdentification <em>Identification</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.AssetAdministrationShellT#getAdministration <em>Administration</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.AssetAdministrationShellT#getEmbeddedDataSpecification <em>Embedded Data Specification</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.AssetAdministrationShellT#getDerivedFrom <em>Derived From</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.AssetAdministrationShellT#getAssetRef <em>Asset Ref</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.AssetAdministrationShellT#getSubmodelRefs <em>Submodel Refs</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.AssetAdministrationShellT#getViews <em>Views</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.AssetAdministrationShellT#getConceptDictionaries <em>Concept Dictionaries</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._1._0._0Package#getAssetAdministrationShellT()
 * @model extendedMetaData="name='assetAdministrationShell_t' kind='elementOnly'"
 * @generated
 */
public interface AssetAdministrationShellT extends EObject {
	/**
	 * Returns the value of the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id Short</em>' containment reference.
	 * @see #setIdShort(IdShortT)
	 * @see io.shell.admin.aas._1._0._0Package#getAssetAdministrationShellT_IdShort()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='idShort' namespace='##targetNamespace'"
	 * @generated
	 */
	IdShortT getIdShort();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.AssetAdministrationShellT#getIdShort <em>Id Short</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id Short</em>' containment reference.
	 * @see #getIdShort()
	 * @generated
	 */
	void setIdShort(IdShortT value);

	/**
	 * Returns the value of the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Category</em>' attribute.
	 * @see #setCategory(String)
	 * @see io.shell.admin.aas._1._0._0Package#getAssetAdministrationShellT_Category()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='category' namespace='##targetNamespace'"
	 * @generated
	 */
	String getCategory();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.AssetAdministrationShellT#getCategory <em>Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Category</em>' attribute.
	 * @see #getCategory()
	 * @generated
	 */
	void setCategory(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference.
	 * @see #setDescription(LangStringsT)
	 * @see io.shell.admin.aas._1._0._0Package#getAssetAdministrationShellT_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='description' namespace='##targetNamespace'"
	 * @generated
	 */
	LangStringsT getDescription();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.AssetAdministrationShellT#getDescription <em>Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' containment reference.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(LangStringsT value);

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' attribute.
	 * @see #setParent(String)
	 * @see io.shell.admin.aas._1._0._0Package#getAssetAdministrationShellT_Parent()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='parent' namespace='##targetNamespace'"
	 * @generated
	 */
	String getParent();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.AssetAdministrationShellT#getParent <em>Parent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' attribute.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(String value);

	/**
	 * Returns the value of the '<em><b>Identification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identification</em>' containment reference.
	 * @see #setIdentification(IdentificationT)
	 * @see io.shell.admin.aas._1._0._0Package#getAssetAdministrationShellT_Identification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='identification' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentificationT getIdentification();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.AssetAdministrationShellT#getIdentification <em>Identification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Identification</em>' containment reference.
	 * @see #getIdentification()
	 * @generated
	 */
	void setIdentification(IdentificationT value);

	/**
	 * Returns the value of the '<em><b>Administration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Administration</em>' containment reference.
	 * @see #setAdministration(AdministrationT)
	 * @see io.shell.admin.aas._1._0._0Package#getAssetAdministrationShellT_Administration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='administration' namespace='##targetNamespace'"
	 * @generated
	 */
	AdministrationT getAdministration();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.AssetAdministrationShellT#getAdministration <em>Administration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Administration</em>' containment reference.
	 * @see #getAdministration()
	 * @generated
	 */
	void setAdministration(AdministrationT value);

	/**
	 * Returns the value of the '<em><b>Embedded Data Specification</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas._1._0.EmbeddedDataSpecificationT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Embedded Data Specification</em>' containment reference list.
	 * @see io.shell.admin.aas._1._0._0Package#getAssetAdministrationShellT_EmbeddedDataSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='embeddedDataSpecification' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<EmbeddedDataSpecificationT> getEmbeddedDataSpecification();

	/**
	 * Returns the value of the '<em><b>Derived From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived From</em>' containment reference.
	 * @see #setDerivedFrom(ReferenceT)
	 * @see io.shell.admin.aas._1._0._0Package#getAssetAdministrationShellT_DerivedFrom()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='derivedFrom' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getDerivedFrom();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.AssetAdministrationShellT#getDerivedFrom <em>Derived From</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Derived From</em>' containment reference.
	 * @see #getDerivedFrom()
	 * @generated
	 */
	void setDerivedFrom(ReferenceT value);

	/**
	 * Returns the value of the '<em><b>Asset Ref</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asset Ref</em>' containment reference.
	 * @see #setAssetRef(ReferenceT)
	 * @see io.shell.admin.aas._1._0._0Package#getAssetAdministrationShellT_AssetRef()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='assetRef' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getAssetRef();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.AssetAdministrationShellT#getAssetRef <em>Asset Ref</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Asset Ref</em>' containment reference.
	 * @see #getAssetRef()
	 * @generated
	 */
	void setAssetRef(ReferenceT value);

	/**
	 * Returns the value of the '<em><b>Submodel Refs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Submodel Refs</em>' containment reference.
	 * @see #setSubmodelRefs(SubmodelRefsT)
	 * @see io.shell.admin.aas._1._0._0Package#getAssetAdministrationShellT_SubmodelRefs()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='submodelRefs' namespace='##targetNamespace'"
	 * @generated
	 */
	SubmodelRefsT getSubmodelRefs();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.AssetAdministrationShellT#getSubmodelRefs <em>Submodel Refs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Submodel Refs</em>' containment reference.
	 * @see #getSubmodelRefs()
	 * @generated
	 */
	void setSubmodelRefs(SubmodelRefsT value);

	/**
	 * Returns the value of the '<em><b>Views</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Views</em>' containment reference.
	 * @see #setViews(ViewsT)
	 * @see io.shell.admin.aas._1._0._0Package#getAssetAdministrationShellT_Views()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='views' namespace='##targetNamespace'"
	 * @generated
	 */
	ViewsT getViews();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.AssetAdministrationShellT#getViews <em>Views</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Views</em>' containment reference.
	 * @see #getViews()
	 * @generated
	 */
	void setViews(ViewsT value);

	/**
	 * Returns the value of the '<em><b>Concept Dictionaries</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Concept Dictionaries</em>' containment reference.
	 * @see #setConceptDictionaries(ConceptDictionariesT)
	 * @see io.shell.admin.aas._1._0._0Package#getAssetAdministrationShellT_ConceptDictionaries()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='conceptDictionaries' namespace='##targetNamespace'"
	 * @generated
	 */
	ConceptDictionariesT getConceptDictionaries();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._1._0.AssetAdministrationShellT#getConceptDictionaries <em>Concept Dictionaries</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Concept Dictionaries</em>' containment reference.
	 * @see #getConceptDictionaries()
	 * @generated
	 */
	void setConceptDictionaries(ConceptDictionariesT value);

} // AssetAdministrationShellT
