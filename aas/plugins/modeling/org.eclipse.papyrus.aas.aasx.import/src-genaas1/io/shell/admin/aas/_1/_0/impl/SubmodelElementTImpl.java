/**
 * ****************************************************************************
 *   Copyright (c) 2023 CEA LIST.
 *  
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License 2.0
 *   which accompanies this distribution, and is available at
 *   https://www.eclipse.org/legal/epl-2.0/
 *  
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *    Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *  
 *  ****************************************************************************
 */
package io.shell.admin.aas._1._0.impl;

import io.shell.admin.aas._1._0.BlobT;
import io.shell.admin.aas._1._0.EventT;
import io.shell.admin.aas._1._0.FileT;
import io.shell.admin.aas._1._0.OperationT;
import io.shell.admin.aas._1._0.OperationVariableT;
import io.shell.admin.aas._1._0.PropertyT;
import io.shell.admin.aas._1._0.ReferenceElementT;
import io.shell.admin.aas._1._0.RelationshipElementT;
import io.shell.admin.aas._1._0.SubmodelElementCollectionT;
import io.shell.admin.aas._1._0.SubmodelElementT;
import io.shell.admin.aas._1._0._0Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Submodel Element T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._1._0.impl.SubmodelElementTImpl#getProperty <em>Property</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.impl.SubmodelElementTImpl#getFile <em>File</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.impl.SubmodelElementTImpl#getBlob <em>Blob</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.impl.SubmodelElementTImpl#getReferenceElement <em>Reference Element</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.impl.SubmodelElementTImpl#getSubmodelElementCollection <em>Submodel Element Collection</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.impl.SubmodelElementTImpl#getRelationshipElement <em>Relationship Element</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.impl.SubmodelElementTImpl#getOperation <em>Operation</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.impl.SubmodelElementTImpl#getOperationVariable <em>Operation Variable</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.impl.SubmodelElementTImpl#getEvent <em>Event</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubmodelElementTImpl extends MinimalEObjectImpl.Container implements SubmodelElementT {
	/**
	 * The cached value of the '{@link #getProperty() <em>Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperty()
	 * @generated
	 * @ordered
	 */
	protected PropertyT property;

	/**
	 * The cached value of the '{@link #getFile() <em>File</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFile()
	 * @generated
	 * @ordered
	 */
	protected FileT file;

	/**
	 * The cached value of the '{@link #getBlob() <em>Blob</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBlob()
	 * @generated
	 * @ordered
	 */
	protected BlobT blob;

	/**
	 * The cached value of the '{@link #getReferenceElement() <em>Reference Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenceElement()
	 * @generated
	 * @ordered
	 */
	protected ReferenceElementT referenceElement;

	/**
	 * The cached value of the '{@link #getSubmodelElementCollection() <em>Submodel Element Collection</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubmodelElementCollection()
	 * @generated
	 * @ordered
	 */
	protected SubmodelElementCollectionT submodelElementCollection;

	/**
	 * The cached value of the '{@link #getRelationshipElement() <em>Relationship Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelationshipElement()
	 * @generated
	 * @ordered
	 */
	protected RelationshipElementT relationshipElement;

	/**
	 * The cached value of the '{@link #getOperation() <em>Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperation()
	 * @generated
	 * @ordered
	 */
	protected OperationT operation;

	/**
	 * The cached value of the '{@link #getOperationVariable() <em>Operation Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationVariable()
	 * @generated
	 * @ordered
	 */
	protected OperationVariableT operationVariable;

	/**
	 * The cached value of the '{@link #getEvent() <em>Event</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvent()
	 * @generated
	 * @ordered
	 */
	protected EventT event;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubmodelElementTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.SUBMODEL_ELEMENT_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyT getProperty() {
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProperty(PropertyT newProperty, NotificationChain msgs) {
		PropertyT oldProperty = property;
		property = newProperty;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__PROPERTY, oldProperty, newProperty);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProperty(PropertyT newProperty) {
		if (newProperty != property) {
			NotificationChain msgs = null;
			if (property != null)
				msgs = ((InternalEObject)property).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__PROPERTY, null, msgs);
			if (newProperty != null)
				msgs = ((InternalEObject)newProperty).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__PROPERTY, null, msgs);
			msgs = basicSetProperty(newProperty, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__PROPERTY, newProperty, newProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FileT getFile() {
		return file;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFile(FileT newFile, NotificationChain msgs) {
		FileT oldFile = file;
		file = newFile;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__FILE, oldFile, newFile);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFile(FileT newFile) {
		if (newFile != file) {
			NotificationChain msgs = null;
			if (file != null)
				msgs = ((InternalEObject)file).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__FILE, null, msgs);
			if (newFile != null)
				msgs = ((InternalEObject)newFile).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__FILE, null, msgs);
			msgs = basicSetFile(newFile, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__FILE, newFile, newFile));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlobT getBlob() {
		return blob;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBlob(BlobT newBlob, NotificationChain msgs) {
		BlobT oldBlob = blob;
		blob = newBlob;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__BLOB, oldBlob, newBlob);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBlob(BlobT newBlob) {
		if (newBlob != blob) {
			NotificationChain msgs = null;
			if (blob != null)
				msgs = ((InternalEObject)blob).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__BLOB, null, msgs);
			if (newBlob != null)
				msgs = ((InternalEObject)newBlob).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__BLOB, null, msgs);
			msgs = basicSetBlob(newBlob, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__BLOB, newBlob, newBlob));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceElementT getReferenceElement() {
		return referenceElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReferenceElement(ReferenceElementT newReferenceElement, NotificationChain msgs) {
		ReferenceElementT oldReferenceElement = referenceElement;
		referenceElement = newReferenceElement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__REFERENCE_ELEMENT, oldReferenceElement, newReferenceElement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferenceElement(ReferenceElementT newReferenceElement) {
		if (newReferenceElement != referenceElement) {
			NotificationChain msgs = null;
			if (referenceElement != null)
				msgs = ((InternalEObject)referenceElement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__REFERENCE_ELEMENT, null, msgs);
			if (newReferenceElement != null)
				msgs = ((InternalEObject)newReferenceElement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__REFERENCE_ELEMENT, null, msgs);
			msgs = basicSetReferenceElement(newReferenceElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__REFERENCE_ELEMENT, newReferenceElement, newReferenceElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubmodelElementCollectionT getSubmodelElementCollection() {
		return submodelElementCollection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSubmodelElementCollection(SubmodelElementCollectionT newSubmodelElementCollection, NotificationChain msgs) {
		SubmodelElementCollectionT oldSubmodelElementCollection = submodelElementCollection;
		submodelElementCollection = newSubmodelElementCollection;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__SUBMODEL_ELEMENT_COLLECTION, oldSubmodelElementCollection, newSubmodelElementCollection);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubmodelElementCollection(SubmodelElementCollectionT newSubmodelElementCollection) {
		if (newSubmodelElementCollection != submodelElementCollection) {
			NotificationChain msgs = null;
			if (submodelElementCollection != null)
				msgs = ((InternalEObject)submodelElementCollection).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__SUBMODEL_ELEMENT_COLLECTION, null, msgs);
			if (newSubmodelElementCollection != null)
				msgs = ((InternalEObject)newSubmodelElementCollection).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__SUBMODEL_ELEMENT_COLLECTION, null, msgs);
			msgs = basicSetSubmodelElementCollection(newSubmodelElementCollection, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__SUBMODEL_ELEMENT_COLLECTION, newSubmodelElementCollection, newSubmodelElementCollection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelationshipElementT getRelationshipElement() {
		return relationshipElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRelationshipElement(RelationshipElementT newRelationshipElement, NotificationChain msgs) {
		RelationshipElementT oldRelationshipElement = relationshipElement;
		relationshipElement = newRelationshipElement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__RELATIONSHIP_ELEMENT, oldRelationshipElement, newRelationshipElement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRelationshipElement(RelationshipElementT newRelationshipElement) {
		if (newRelationshipElement != relationshipElement) {
			NotificationChain msgs = null;
			if (relationshipElement != null)
				msgs = ((InternalEObject)relationshipElement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__RELATIONSHIP_ELEMENT, null, msgs);
			if (newRelationshipElement != null)
				msgs = ((InternalEObject)newRelationshipElement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__RELATIONSHIP_ELEMENT, null, msgs);
			msgs = basicSetRelationshipElement(newRelationshipElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__RELATIONSHIP_ELEMENT, newRelationshipElement, newRelationshipElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationT getOperation() {
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperation(OperationT newOperation, NotificationChain msgs) {
		OperationT oldOperation = operation;
		operation = newOperation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__OPERATION, oldOperation, newOperation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperation(OperationT newOperation) {
		if (newOperation != operation) {
			NotificationChain msgs = null;
			if (operation != null)
				msgs = ((InternalEObject)operation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__OPERATION, null, msgs);
			if (newOperation != null)
				msgs = ((InternalEObject)newOperation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__OPERATION, null, msgs);
			msgs = basicSetOperation(newOperation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__OPERATION, newOperation, newOperation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationVariableT getOperationVariable() {
		return operationVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperationVariable(OperationVariableT newOperationVariable, NotificationChain msgs) {
		OperationVariableT oldOperationVariable = operationVariable;
		operationVariable = newOperationVariable;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__OPERATION_VARIABLE, oldOperationVariable, newOperationVariable);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationVariable(OperationVariableT newOperationVariable) {
		if (newOperationVariable != operationVariable) {
			NotificationChain msgs = null;
			if (operationVariable != null)
				msgs = ((InternalEObject)operationVariable).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__OPERATION_VARIABLE, null, msgs);
			if (newOperationVariable != null)
				msgs = ((InternalEObject)newOperationVariable).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__OPERATION_VARIABLE, null, msgs);
			msgs = basicSetOperationVariable(newOperationVariable, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__OPERATION_VARIABLE, newOperationVariable, newOperationVariable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventT getEvent() {
		return event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEvent(EventT newEvent, NotificationChain msgs) {
		EventT oldEvent = event;
		event = newEvent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__EVENT, oldEvent, newEvent);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEvent(EventT newEvent) {
		if (newEvent != event) {
			NotificationChain msgs = null;
			if (event != null)
				msgs = ((InternalEObject)event).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__EVENT, null, msgs);
			if (newEvent != null)
				msgs = ((InternalEObject)newEvent).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__EVENT, null, msgs);
			msgs = basicSetEvent(newEvent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__EVENT, newEvent, newEvent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.SUBMODEL_ELEMENT_T__PROPERTY:
				return basicSetProperty(null, msgs);
			case _0Package.SUBMODEL_ELEMENT_T__FILE:
				return basicSetFile(null, msgs);
			case _0Package.SUBMODEL_ELEMENT_T__BLOB:
				return basicSetBlob(null, msgs);
			case _0Package.SUBMODEL_ELEMENT_T__REFERENCE_ELEMENT:
				return basicSetReferenceElement(null, msgs);
			case _0Package.SUBMODEL_ELEMENT_T__SUBMODEL_ELEMENT_COLLECTION:
				return basicSetSubmodelElementCollection(null, msgs);
			case _0Package.SUBMODEL_ELEMENT_T__RELATIONSHIP_ELEMENT:
				return basicSetRelationshipElement(null, msgs);
			case _0Package.SUBMODEL_ELEMENT_T__OPERATION:
				return basicSetOperation(null, msgs);
			case _0Package.SUBMODEL_ELEMENT_T__OPERATION_VARIABLE:
				return basicSetOperationVariable(null, msgs);
			case _0Package.SUBMODEL_ELEMENT_T__EVENT:
				return basicSetEvent(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.SUBMODEL_ELEMENT_T__PROPERTY:
				return getProperty();
			case _0Package.SUBMODEL_ELEMENT_T__FILE:
				return getFile();
			case _0Package.SUBMODEL_ELEMENT_T__BLOB:
				return getBlob();
			case _0Package.SUBMODEL_ELEMENT_T__REFERENCE_ELEMENT:
				return getReferenceElement();
			case _0Package.SUBMODEL_ELEMENT_T__SUBMODEL_ELEMENT_COLLECTION:
				return getSubmodelElementCollection();
			case _0Package.SUBMODEL_ELEMENT_T__RELATIONSHIP_ELEMENT:
				return getRelationshipElement();
			case _0Package.SUBMODEL_ELEMENT_T__OPERATION:
				return getOperation();
			case _0Package.SUBMODEL_ELEMENT_T__OPERATION_VARIABLE:
				return getOperationVariable();
			case _0Package.SUBMODEL_ELEMENT_T__EVENT:
				return getEvent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.SUBMODEL_ELEMENT_T__PROPERTY:
				setProperty((PropertyT)newValue);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__FILE:
				setFile((FileT)newValue);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__BLOB:
				setBlob((BlobT)newValue);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__REFERENCE_ELEMENT:
				setReferenceElement((ReferenceElementT)newValue);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__SUBMODEL_ELEMENT_COLLECTION:
				setSubmodelElementCollection((SubmodelElementCollectionT)newValue);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__RELATIONSHIP_ELEMENT:
				setRelationshipElement((RelationshipElementT)newValue);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__OPERATION:
				setOperation((OperationT)newValue);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__OPERATION_VARIABLE:
				setOperationVariable((OperationVariableT)newValue);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__EVENT:
				setEvent((EventT)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.SUBMODEL_ELEMENT_T__PROPERTY:
				setProperty((PropertyT)null);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__FILE:
				setFile((FileT)null);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__BLOB:
				setBlob((BlobT)null);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__REFERENCE_ELEMENT:
				setReferenceElement((ReferenceElementT)null);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__SUBMODEL_ELEMENT_COLLECTION:
				setSubmodelElementCollection((SubmodelElementCollectionT)null);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__RELATIONSHIP_ELEMENT:
				setRelationshipElement((RelationshipElementT)null);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__OPERATION:
				setOperation((OperationT)null);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__OPERATION_VARIABLE:
				setOperationVariable((OperationVariableT)null);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__EVENT:
				setEvent((EventT)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.SUBMODEL_ELEMENT_T__PROPERTY:
				return property != null;
			case _0Package.SUBMODEL_ELEMENT_T__FILE:
				return file != null;
			case _0Package.SUBMODEL_ELEMENT_T__BLOB:
				return blob != null;
			case _0Package.SUBMODEL_ELEMENT_T__REFERENCE_ELEMENT:
				return referenceElement != null;
			case _0Package.SUBMODEL_ELEMENT_T__SUBMODEL_ELEMENT_COLLECTION:
				return submodelElementCollection != null;
			case _0Package.SUBMODEL_ELEMENT_T__RELATIONSHIP_ELEMENT:
				return relationshipElement != null;
			case _0Package.SUBMODEL_ELEMENT_T__OPERATION:
				return operation != null;
			case _0Package.SUBMODEL_ELEMENT_T__OPERATION_VARIABLE:
				return operationVariable != null;
			case _0Package.SUBMODEL_ELEMENT_T__EVENT:
				return event != null;
		}
		return super.eIsSet(featureID);
	}

} //SubmodelElementTImpl
