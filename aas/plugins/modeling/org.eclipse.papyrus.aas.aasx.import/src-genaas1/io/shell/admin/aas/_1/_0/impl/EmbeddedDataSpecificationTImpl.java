/**
 * ****************************************************************************
 *   Copyright (c) 2023 CEA LIST.
 *  
 *  
 *   All rights reserved. This program and the accompanying materials
 *   are made available under the terms of the Eclipse Public License 2.0
 *   which accompanies this distribution, and is available at
 *   https://www.eclipse.org/legal/epl-2.0/
 *  
 *   SPDX-License-Identifier: EPL-2.0
 *  
 *   Contributors:
 *    Asma Smaoui (CEA LIST) asma.smaoui@cea.fr - Initial API and implementation
 *  
 *  ****************************************************************************
 */
package io.shell.admin.aas._1._0.impl;

import io.shell.admin.aas._1._0.DataSpecificationContentT;
import io.shell.admin.aas._1._0.EmbeddedDataSpecificationT;
import io.shell.admin.aas._1._0.ReferenceT;
import io.shell.admin.aas._1._0._0Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Embedded Data Specification T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._1._0.impl.EmbeddedDataSpecificationTImpl#getHasDataSpecification <em>Has Data Specification</em>}</li>
 *   <li>{@link io.shell.admin.aas._1._0.impl.EmbeddedDataSpecificationTImpl#getDataSpecificationContent <em>Data Specification Content</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EmbeddedDataSpecificationTImpl extends MinimalEObjectImpl.Container implements EmbeddedDataSpecificationT {
	/**
	 * The cached value of the '{@link #getHasDataSpecification() <em>Has Data Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasDataSpecification()
	 * @generated
	 * @ordered
	 */
	protected ReferenceT hasDataSpecification;

	/**
	 * The cached value of the '{@link #getDataSpecificationContent() <em>Data Specification Content</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataSpecificationContent()
	 * @generated
	 * @ordered
	 */
	protected DataSpecificationContentT dataSpecificationContent;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EmbeddedDataSpecificationTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.EMBEDDED_DATA_SPECIFICATION_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceT getHasDataSpecification() {
		return hasDataSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasDataSpecification(ReferenceT newHasDataSpecification, NotificationChain msgs) {
		ReferenceT oldHasDataSpecification = hasDataSpecification;
		hasDataSpecification = newHasDataSpecification;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.EMBEDDED_DATA_SPECIFICATION_T__HAS_DATA_SPECIFICATION, oldHasDataSpecification, newHasDataSpecification);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasDataSpecification(ReferenceT newHasDataSpecification) {
		if (newHasDataSpecification != hasDataSpecification) {
			NotificationChain msgs = null;
			if (hasDataSpecification != null)
				msgs = ((InternalEObject)hasDataSpecification).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.EMBEDDED_DATA_SPECIFICATION_T__HAS_DATA_SPECIFICATION, null, msgs);
			if (newHasDataSpecification != null)
				msgs = ((InternalEObject)newHasDataSpecification).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.EMBEDDED_DATA_SPECIFICATION_T__HAS_DATA_SPECIFICATION, null, msgs);
			msgs = basicSetHasDataSpecification(newHasDataSpecification, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.EMBEDDED_DATA_SPECIFICATION_T__HAS_DATA_SPECIFICATION, newHasDataSpecification, newHasDataSpecification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataSpecificationContentT getDataSpecificationContent() {
		return dataSpecificationContent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataSpecificationContent(DataSpecificationContentT newDataSpecificationContent, NotificationChain msgs) {
		DataSpecificationContentT oldDataSpecificationContent = dataSpecificationContent;
		dataSpecificationContent = newDataSpecificationContent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.EMBEDDED_DATA_SPECIFICATION_T__DATA_SPECIFICATION_CONTENT, oldDataSpecificationContent, newDataSpecificationContent);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataSpecificationContent(DataSpecificationContentT newDataSpecificationContent) {
		if (newDataSpecificationContent != dataSpecificationContent) {
			NotificationChain msgs = null;
			if (dataSpecificationContent != null)
				msgs = ((InternalEObject)dataSpecificationContent).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.EMBEDDED_DATA_SPECIFICATION_T__DATA_SPECIFICATION_CONTENT, null, msgs);
			if (newDataSpecificationContent != null)
				msgs = ((InternalEObject)newDataSpecificationContent).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.EMBEDDED_DATA_SPECIFICATION_T__DATA_SPECIFICATION_CONTENT, null, msgs);
			msgs = basicSetDataSpecificationContent(newDataSpecificationContent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.EMBEDDED_DATA_SPECIFICATION_T__DATA_SPECIFICATION_CONTENT, newDataSpecificationContent, newDataSpecificationContent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.EMBEDDED_DATA_SPECIFICATION_T__HAS_DATA_SPECIFICATION:
				return basicSetHasDataSpecification(null, msgs);
			case _0Package.EMBEDDED_DATA_SPECIFICATION_T__DATA_SPECIFICATION_CONTENT:
				return basicSetDataSpecificationContent(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.EMBEDDED_DATA_SPECIFICATION_T__HAS_DATA_SPECIFICATION:
				return getHasDataSpecification();
			case _0Package.EMBEDDED_DATA_SPECIFICATION_T__DATA_SPECIFICATION_CONTENT:
				return getDataSpecificationContent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.EMBEDDED_DATA_SPECIFICATION_T__HAS_DATA_SPECIFICATION:
				setHasDataSpecification((ReferenceT)newValue);
				return;
			case _0Package.EMBEDDED_DATA_SPECIFICATION_T__DATA_SPECIFICATION_CONTENT:
				setDataSpecificationContent((DataSpecificationContentT)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.EMBEDDED_DATA_SPECIFICATION_T__HAS_DATA_SPECIFICATION:
				setHasDataSpecification((ReferenceT)null);
				return;
			case _0Package.EMBEDDED_DATA_SPECIFICATION_T__DATA_SPECIFICATION_CONTENT:
				setDataSpecificationContent((DataSpecificationContentT)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.EMBEDDED_DATA_SPECIFICATION_T__HAS_DATA_SPECIFICATION:
				return hasDataSpecification != null;
			case _0Package.EMBEDDED_DATA_SPECIFICATION_T__DATA_SPECIFICATION_CONTENT:
				return dataSpecificationContent != null;
		}
		return super.eIsSet(featureID);
	}

} //EmbeddedDataSpecificationTImpl
