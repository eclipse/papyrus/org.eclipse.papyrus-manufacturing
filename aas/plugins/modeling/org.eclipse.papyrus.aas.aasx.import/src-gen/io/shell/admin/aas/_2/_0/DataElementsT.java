/**
 */
package io.shell.admin.aas._2._0;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Elements T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.DataElementsT#getDataElement <em>Data Element</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getDataElementsT()
 * @model extendedMetaData="name='dataElements_t' kind='elementOnly'"
 * @generated
 */
public interface DataElementsT extends EObject {
	/**
	 * Returns the value of the '<em><b>Data Element</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas._2._0.DataElementAbstractT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Element</em>' containment reference list.
	 * @see io.shell.admin.aas._2._0._0Package#getDataElementsT_DataElement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='dataElement' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DataElementAbstractT> getDataElement();

} // DataElementsT
