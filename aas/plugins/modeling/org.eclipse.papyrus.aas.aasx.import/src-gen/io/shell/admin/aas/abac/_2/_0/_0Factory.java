/**
 */
package io.shell.admin.aas.abac._2._0;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see io.shell.admin.aas.abac._2._0._0Package
 * @generated
 */
public interface _0Factory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	_0Factory eINSTANCE = io.shell.admin.aas.abac._2._0.impl._0FactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Access Control Policy Points T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Access Control Policy Points T</em>'.
	 * @generated
	 */
	AccessControlPolicyPointsT createAccessControlPolicyPointsT();

	/**
	 * Returns a new object of class '<em>Access Control T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Access Control T</em>'.
	 * @generated
	 */
	AccessControlT createAccessControlT();

	/**
	 * Returns a new object of class '<em>Access Permission Rules T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Access Permission Rules T</em>'.
	 * @generated
	 */
	AccessPermissionRulesT createAccessPermissionRulesT();

	/**
	 * Returns a new object of class '<em>Access Permission Rule T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Access Permission Rule T</em>'.
	 * @generated
	 */
	AccessPermissionRuleT createAccessPermissionRuleT();

	/**
	 * Returns a new object of class '<em>Blob Certificate T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blob Certificate T</em>'.
	 * @generated
	 */
	BlobCertificateT createBlobCertificateT();

	/**
	 * Returns a new object of class '<em>Certificate Abstract T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Certificate Abstract T</em>'.
	 * @generated
	 */
	CertificateAbstractT createCertificateAbstractT();

	/**
	 * Returns a new object of class '<em>Certificates T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Certificates T</em>'.
	 * @generated
	 */
	CertificatesT createCertificatesT();

	/**
	 * Returns a new object of class '<em>Certificate T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Certificate T</em>'.
	 * @generated
	 */
	CertificateT createCertificateT();

	/**
	 * Returns a new object of class '<em>Contained Extensions T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Contained Extensions T</em>'.
	 * @generated
	 */
	ContainedExtensionsT createContainedExtensionsT();

	/**
	 * Returns a new object of class '<em>Internal Information Points</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Internal Information Points</em>'.
	 * @generated
	 */
	InternalInformationPoints createInternalInformationPoints();

	/**
	 * Returns a new object of class '<em>Object Attributes T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Object Attributes T</em>'.
	 * @generated
	 */
	ObjectAttributesT createObjectAttributesT();

	/**
	 * Returns a new object of class '<em>Permission Per Object T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Permission Per Object T</em>'.
	 * @generated
	 */
	PermissionPerObjectT createPermissionPerObjectT();

	/**
	 * Returns a new object of class '<em>Permissions T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Permissions T</em>'.
	 * @generated
	 */
	PermissionsT createPermissionsT();

	/**
	 * Returns a new object of class '<em>Policy Administration Point T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Policy Administration Point T</em>'.
	 * @generated
	 */
	PolicyAdministrationPointT createPolicyAdministrationPointT();

	/**
	 * Returns a new object of class '<em>Policy Decision Point T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Policy Decision Point T</em>'.
	 * @generated
	 */
	PolicyDecisionPointT createPolicyDecisionPointT();

	/**
	 * Returns a new object of class '<em>Policy Enforcement Point T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Policy Enforcement Point T</em>'.
	 * @generated
	 */
	PolicyEnforcementPointT createPolicyEnforcementPointT();

	/**
	 * Returns a new object of class '<em>Policy Information Points T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Policy Information Points T</em>'.
	 * @generated
	 */
	PolicyInformationPointsT createPolicyInformationPointsT();

	/**
	 * Returns a new object of class '<em>Security T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Security T</em>'.
	 * @generated
	 */
	SecurityT createSecurityT();

	/**
	 * Returns a new object of class '<em>Subject Attributes T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Subject Attributes T</em>'.
	 * @generated
	 */
	SubjectAttributesT createSubjectAttributesT();

	/**
	 * Returns a new object of class '<em>Document Root</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Document Root</em>'.
	 * @generated
	 */
	DocumentRoot createDocumentRoot();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	_0Package get_0Package();

} //_0Factory
