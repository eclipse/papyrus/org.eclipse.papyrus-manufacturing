/**
 */
package io.shell.admin.aas.abac._2._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Policy Decision Point T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.PolicyDecisionPointT#isExternalPolicyDecisionPoint <em>External Policy Decision Point</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas.abac._2._0._0Package#getPolicyDecisionPointT()
 * @model extendedMetaData="name='policyDecisionPoint_t' kind='elementOnly'"
 * @generated
 */
public interface PolicyDecisionPointT extends EObject {
	/**
	 * Returns the value of the '<em><b>External Policy Decision Point</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Policy Decision Point</em>' attribute.
	 * @see #isSetExternalPolicyDecisionPoint()
	 * @see #unsetExternalPolicyDecisionPoint()
	 * @see #setExternalPolicyDecisionPoint(boolean)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getPolicyDecisionPointT_ExternalPolicyDecisionPoint()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean" required="true"
	 *        extendedMetaData="kind='element' name='externalPolicyDecisionPoint' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isExternalPolicyDecisionPoint();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.PolicyDecisionPointT#isExternalPolicyDecisionPoint <em>External Policy Decision Point</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>External Policy Decision Point</em>' attribute.
	 * @see #isSetExternalPolicyDecisionPoint()
	 * @see #unsetExternalPolicyDecisionPoint()
	 * @see #isExternalPolicyDecisionPoint()
	 * @generated
	 */
	void setExternalPolicyDecisionPoint(boolean value);

	/**
	 * Unsets the value of the '{@link io.shell.admin.aas.abac._2._0.PolicyDecisionPointT#isExternalPolicyDecisionPoint <em>External Policy Decision Point</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExternalPolicyDecisionPoint()
	 * @see #isExternalPolicyDecisionPoint()
	 * @see #setExternalPolicyDecisionPoint(boolean)
	 * @generated
	 */
	void unsetExternalPolicyDecisionPoint();

	/**
	 * Returns whether the value of the '{@link io.shell.admin.aas.abac._2._0.PolicyDecisionPointT#isExternalPolicyDecisionPoint <em>External Policy Decision Point</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>External Policy Decision Point</em>' attribute is set.
	 * @see #unsetExternalPolicyDecisionPoint()
	 * @see #isExternalPolicyDecisionPoint()
	 * @see #setExternalPolicyDecisionPoint(boolean)
	 * @generated
	 */
	boolean isSetExternalPolicyDecisionPoint();

} // PolicyDecisionPointT
