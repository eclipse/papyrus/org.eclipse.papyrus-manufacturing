/**
 */
package io.shell.admin.aas.abac._2._0.impl;

import io.shell.admin.aas._2._0.ReferenceT;

import io.shell.admin.aas.abac._2._0.AccessControlT;
import io.shell.admin.aas.abac._2._0.AccessPermissionRulesT;
import io.shell.admin.aas.abac._2._0._0Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Access Control T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.AccessControlTImpl#getSelectableSubjectAttributes <em>Selectable Subject Attributes</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.AccessControlTImpl#getDefaultSubjectAttributes <em>Default Subject Attributes</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.AccessControlTImpl#getSelectablePermissions <em>Selectable Permissions</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.AccessControlTImpl#getDefaultPermissions <em>Default Permissions</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.AccessControlTImpl#getSelectableEnvironmentAttributes <em>Selectable Environment Attributes</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.AccessControlTImpl#getDefaultEnvironmentAttributes <em>Default Environment Attributes</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.AccessControlTImpl#getAccessPermissionRules <em>Access Permission Rules</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AccessControlTImpl extends MinimalEObjectImpl.Container implements AccessControlT {
	/**
	 * The cached value of the '{@link #getSelectableSubjectAttributes() <em>Selectable Subject Attributes</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelectableSubjectAttributes()
	 * @generated
	 * @ordered
	 */
	protected ReferenceT selectableSubjectAttributes;

	/**
	 * The cached value of the '{@link #getDefaultSubjectAttributes() <em>Default Subject Attributes</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultSubjectAttributes()
	 * @generated
	 * @ordered
	 */
	protected ReferenceT defaultSubjectAttributes;

	/**
	 * The cached value of the '{@link #getSelectablePermissions() <em>Selectable Permissions</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelectablePermissions()
	 * @generated
	 * @ordered
	 */
	protected ReferenceT selectablePermissions;

	/**
	 * The cached value of the '{@link #getDefaultPermissions() <em>Default Permissions</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultPermissions()
	 * @generated
	 * @ordered
	 */
	protected ReferenceT defaultPermissions;

	/**
	 * The cached value of the '{@link #getSelectableEnvironmentAttributes() <em>Selectable Environment Attributes</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelectableEnvironmentAttributes()
	 * @generated
	 * @ordered
	 */
	protected ReferenceT selectableEnvironmentAttributes;

	/**
	 * The cached value of the '{@link #getDefaultEnvironmentAttributes() <em>Default Environment Attributes</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultEnvironmentAttributes()
	 * @generated
	 * @ordered
	 */
	protected ReferenceT defaultEnvironmentAttributes;

	/**
	 * The cached value of the '{@link #getAccessPermissionRules() <em>Access Permission Rules</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessPermissionRules()
	 * @generated
	 * @ordered
	 */
	protected AccessPermissionRulesT accessPermissionRules;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AccessControlTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.ACCESS_CONTROL_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceT getSelectableSubjectAttributes() {
		return selectableSubjectAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSelectableSubjectAttributes(ReferenceT newSelectableSubjectAttributes, NotificationChain msgs) {
		ReferenceT oldSelectableSubjectAttributes = selectableSubjectAttributes;
		selectableSubjectAttributes = newSelectableSubjectAttributes;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_CONTROL_T__SELECTABLE_SUBJECT_ATTRIBUTES, oldSelectableSubjectAttributes, newSelectableSubjectAttributes);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelectableSubjectAttributes(ReferenceT newSelectableSubjectAttributes) {
		if (newSelectableSubjectAttributes != selectableSubjectAttributes) {
			NotificationChain msgs = null;
			if (selectableSubjectAttributes != null)
				msgs = ((InternalEObject)selectableSubjectAttributes).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_CONTROL_T__SELECTABLE_SUBJECT_ATTRIBUTES, null, msgs);
			if (newSelectableSubjectAttributes != null)
				msgs = ((InternalEObject)newSelectableSubjectAttributes).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_CONTROL_T__SELECTABLE_SUBJECT_ATTRIBUTES, null, msgs);
			msgs = basicSetSelectableSubjectAttributes(newSelectableSubjectAttributes, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_CONTROL_T__SELECTABLE_SUBJECT_ATTRIBUTES, newSelectableSubjectAttributes, newSelectableSubjectAttributes));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceT getDefaultSubjectAttributes() {
		return defaultSubjectAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefaultSubjectAttributes(ReferenceT newDefaultSubjectAttributes, NotificationChain msgs) {
		ReferenceT oldDefaultSubjectAttributes = defaultSubjectAttributes;
		defaultSubjectAttributes = newDefaultSubjectAttributes;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_CONTROL_T__DEFAULT_SUBJECT_ATTRIBUTES, oldDefaultSubjectAttributes, newDefaultSubjectAttributes);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultSubjectAttributes(ReferenceT newDefaultSubjectAttributes) {
		if (newDefaultSubjectAttributes != defaultSubjectAttributes) {
			NotificationChain msgs = null;
			if (defaultSubjectAttributes != null)
				msgs = ((InternalEObject)defaultSubjectAttributes).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_CONTROL_T__DEFAULT_SUBJECT_ATTRIBUTES, null, msgs);
			if (newDefaultSubjectAttributes != null)
				msgs = ((InternalEObject)newDefaultSubjectAttributes).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_CONTROL_T__DEFAULT_SUBJECT_ATTRIBUTES, null, msgs);
			msgs = basicSetDefaultSubjectAttributes(newDefaultSubjectAttributes, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_CONTROL_T__DEFAULT_SUBJECT_ATTRIBUTES, newDefaultSubjectAttributes, newDefaultSubjectAttributes));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceT getSelectablePermissions() {
		return selectablePermissions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSelectablePermissions(ReferenceT newSelectablePermissions, NotificationChain msgs) {
		ReferenceT oldSelectablePermissions = selectablePermissions;
		selectablePermissions = newSelectablePermissions;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_CONTROL_T__SELECTABLE_PERMISSIONS, oldSelectablePermissions, newSelectablePermissions);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelectablePermissions(ReferenceT newSelectablePermissions) {
		if (newSelectablePermissions != selectablePermissions) {
			NotificationChain msgs = null;
			if (selectablePermissions != null)
				msgs = ((InternalEObject)selectablePermissions).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_CONTROL_T__SELECTABLE_PERMISSIONS, null, msgs);
			if (newSelectablePermissions != null)
				msgs = ((InternalEObject)newSelectablePermissions).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_CONTROL_T__SELECTABLE_PERMISSIONS, null, msgs);
			msgs = basicSetSelectablePermissions(newSelectablePermissions, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_CONTROL_T__SELECTABLE_PERMISSIONS, newSelectablePermissions, newSelectablePermissions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceT getDefaultPermissions() {
		return defaultPermissions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefaultPermissions(ReferenceT newDefaultPermissions, NotificationChain msgs) {
		ReferenceT oldDefaultPermissions = defaultPermissions;
		defaultPermissions = newDefaultPermissions;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_CONTROL_T__DEFAULT_PERMISSIONS, oldDefaultPermissions, newDefaultPermissions);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultPermissions(ReferenceT newDefaultPermissions) {
		if (newDefaultPermissions != defaultPermissions) {
			NotificationChain msgs = null;
			if (defaultPermissions != null)
				msgs = ((InternalEObject)defaultPermissions).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_CONTROL_T__DEFAULT_PERMISSIONS, null, msgs);
			if (newDefaultPermissions != null)
				msgs = ((InternalEObject)newDefaultPermissions).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_CONTROL_T__DEFAULT_PERMISSIONS, null, msgs);
			msgs = basicSetDefaultPermissions(newDefaultPermissions, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_CONTROL_T__DEFAULT_PERMISSIONS, newDefaultPermissions, newDefaultPermissions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceT getSelectableEnvironmentAttributes() {
		return selectableEnvironmentAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSelectableEnvironmentAttributes(ReferenceT newSelectableEnvironmentAttributes, NotificationChain msgs) {
		ReferenceT oldSelectableEnvironmentAttributes = selectableEnvironmentAttributes;
		selectableEnvironmentAttributes = newSelectableEnvironmentAttributes;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_CONTROL_T__SELECTABLE_ENVIRONMENT_ATTRIBUTES, oldSelectableEnvironmentAttributes, newSelectableEnvironmentAttributes);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelectableEnvironmentAttributes(ReferenceT newSelectableEnvironmentAttributes) {
		if (newSelectableEnvironmentAttributes != selectableEnvironmentAttributes) {
			NotificationChain msgs = null;
			if (selectableEnvironmentAttributes != null)
				msgs = ((InternalEObject)selectableEnvironmentAttributes).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_CONTROL_T__SELECTABLE_ENVIRONMENT_ATTRIBUTES, null, msgs);
			if (newSelectableEnvironmentAttributes != null)
				msgs = ((InternalEObject)newSelectableEnvironmentAttributes).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_CONTROL_T__SELECTABLE_ENVIRONMENT_ATTRIBUTES, null, msgs);
			msgs = basicSetSelectableEnvironmentAttributes(newSelectableEnvironmentAttributes, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_CONTROL_T__SELECTABLE_ENVIRONMENT_ATTRIBUTES, newSelectableEnvironmentAttributes, newSelectableEnvironmentAttributes));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceT getDefaultEnvironmentAttributes() {
		return defaultEnvironmentAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefaultEnvironmentAttributes(ReferenceT newDefaultEnvironmentAttributes, NotificationChain msgs) {
		ReferenceT oldDefaultEnvironmentAttributes = defaultEnvironmentAttributes;
		defaultEnvironmentAttributes = newDefaultEnvironmentAttributes;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_CONTROL_T__DEFAULT_ENVIRONMENT_ATTRIBUTES, oldDefaultEnvironmentAttributes, newDefaultEnvironmentAttributes);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultEnvironmentAttributes(ReferenceT newDefaultEnvironmentAttributes) {
		if (newDefaultEnvironmentAttributes != defaultEnvironmentAttributes) {
			NotificationChain msgs = null;
			if (defaultEnvironmentAttributes != null)
				msgs = ((InternalEObject)defaultEnvironmentAttributes).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_CONTROL_T__DEFAULT_ENVIRONMENT_ATTRIBUTES, null, msgs);
			if (newDefaultEnvironmentAttributes != null)
				msgs = ((InternalEObject)newDefaultEnvironmentAttributes).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_CONTROL_T__DEFAULT_ENVIRONMENT_ATTRIBUTES, null, msgs);
			msgs = basicSetDefaultEnvironmentAttributes(newDefaultEnvironmentAttributes, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_CONTROL_T__DEFAULT_ENVIRONMENT_ATTRIBUTES, newDefaultEnvironmentAttributes, newDefaultEnvironmentAttributes));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessPermissionRulesT getAccessPermissionRules() {
		return accessPermissionRules;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccessPermissionRules(AccessPermissionRulesT newAccessPermissionRules, NotificationChain msgs) {
		AccessPermissionRulesT oldAccessPermissionRules = accessPermissionRules;
		accessPermissionRules = newAccessPermissionRules;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_CONTROL_T__ACCESS_PERMISSION_RULES, oldAccessPermissionRules, newAccessPermissionRules);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessPermissionRules(AccessPermissionRulesT newAccessPermissionRules) {
		if (newAccessPermissionRules != accessPermissionRules) {
			NotificationChain msgs = null;
			if (accessPermissionRules != null)
				msgs = ((InternalEObject)accessPermissionRules).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_CONTROL_T__ACCESS_PERMISSION_RULES, null, msgs);
			if (newAccessPermissionRules != null)
				msgs = ((InternalEObject)newAccessPermissionRules).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_CONTROL_T__ACCESS_PERMISSION_RULES, null, msgs);
			msgs = basicSetAccessPermissionRules(newAccessPermissionRules, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_CONTROL_T__ACCESS_PERMISSION_RULES, newAccessPermissionRules, newAccessPermissionRules));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.ACCESS_CONTROL_T__SELECTABLE_SUBJECT_ATTRIBUTES:
				return basicSetSelectableSubjectAttributes(null, msgs);
			case _0Package.ACCESS_CONTROL_T__DEFAULT_SUBJECT_ATTRIBUTES:
				return basicSetDefaultSubjectAttributes(null, msgs);
			case _0Package.ACCESS_CONTROL_T__SELECTABLE_PERMISSIONS:
				return basicSetSelectablePermissions(null, msgs);
			case _0Package.ACCESS_CONTROL_T__DEFAULT_PERMISSIONS:
				return basicSetDefaultPermissions(null, msgs);
			case _0Package.ACCESS_CONTROL_T__SELECTABLE_ENVIRONMENT_ATTRIBUTES:
				return basicSetSelectableEnvironmentAttributes(null, msgs);
			case _0Package.ACCESS_CONTROL_T__DEFAULT_ENVIRONMENT_ATTRIBUTES:
				return basicSetDefaultEnvironmentAttributes(null, msgs);
			case _0Package.ACCESS_CONTROL_T__ACCESS_PERMISSION_RULES:
				return basicSetAccessPermissionRules(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.ACCESS_CONTROL_T__SELECTABLE_SUBJECT_ATTRIBUTES:
				return getSelectableSubjectAttributes();
			case _0Package.ACCESS_CONTROL_T__DEFAULT_SUBJECT_ATTRIBUTES:
				return getDefaultSubjectAttributes();
			case _0Package.ACCESS_CONTROL_T__SELECTABLE_PERMISSIONS:
				return getSelectablePermissions();
			case _0Package.ACCESS_CONTROL_T__DEFAULT_PERMISSIONS:
				return getDefaultPermissions();
			case _0Package.ACCESS_CONTROL_T__SELECTABLE_ENVIRONMENT_ATTRIBUTES:
				return getSelectableEnvironmentAttributes();
			case _0Package.ACCESS_CONTROL_T__DEFAULT_ENVIRONMENT_ATTRIBUTES:
				return getDefaultEnvironmentAttributes();
			case _0Package.ACCESS_CONTROL_T__ACCESS_PERMISSION_RULES:
				return getAccessPermissionRules();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.ACCESS_CONTROL_T__SELECTABLE_SUBJECT_ATTRIBUTES:
				setSelectableSubjectAttributes((ReferenceT)newValue);
				return;
			case _0Package.ACCESS_CONTROL_T__DEFAULT_SUBJECT_ATTRIBUTES:
				setDefaultSubjectAttributes((ReferenceT)newValue);
				return;
			case _0Package.ACCESS_CONTROL_T__SELECTABLE_PERMISSIONS:
				setSelectablePermissions((ReferenceT)newValue);
				return;
			case _0Package.ACCESS_CONTROL_T__DEFAULT_PERMISSIONS:
				setDefaultPermissions((ReferenceT)newValue);
				return;
			case _0Package.ACCESS_CONTROL_T__SELECTABLE_ENVIRONMENT_ATTRIBUTES:
				setSelectableEnvironmentAttributes((ReferenceT)newValue);
				return;
			case _0Package.ACCESS_CONTROL_T__DEFAULT_ENVIRONMENT_ATTRIBUTES:
				setDefaultEnvironmentAttributes((ReferenceT)newValue);
				return;
			case _0Package.ACCESS_CONTROL_T__ACCESS_PERMISSION_RULES:
				setAccessPermissionRules((AccessPermissionRulesT)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.ACCESS_CONTROL_T__SELECTABLE_SUBJECT_ATTRIBUTES:
				setSelectableSubjectAttributes((ReferenceT)null);
				return;
			case _0Package.ACCESS_CONTROL_T__DEFAULT_SUBJECT_ATTRIBUTES:
				setDefaultSubjectAttributes((ReferenceT)null);
				return;
			case _0Package.ACCESS_CONTROL_T__SELECTABLE_PERMISSIONS:
				setSelectablePermissions((ReferenceT)null);
				return;
			case _0Package.ACCESS_CONTROL_T__DEFAULT_PERMISSIONS:
				setDefaultPermissions((ReferenceT)null);
				return;
			case _0Package.ACCESS_CONTROL_T__SELECTABLE_ENVIRONMENT_ATTRIBUTES:
				setSelectableEnvironmentAttributes((ReferenceT)null);
				return;
			case _0Package.ACCESS_CONTROL_T__DEFAULT_ENVIRONMENT_ATTRIBUTES:
				setDefaultEnvironmentAttributes((ReferenceT)null);
				return;
			case _0Package.ACCESS_CONTROL_T__ACCESS_PERMISSION_RULES:
				setAccessPermissionRules((AccessPermissionRulesT)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.ACCESS_CONTROL_T__SELECTABLE_SUBJECT_ATTRIBUTES:
				return selectableSubjectAttributes != null;
			case _0Package.ACCESS_CONTROL_T__DEFAULT_SUBJECT_ATTRIBUTES:
				return defaultSubjectAttributes != null;
			case _0Package.ACCESS_CONTROL_T__SELECTABLE_PERMISSIONS:
				return selectablePermissions != null;
			case _0Package.ACCESS_CONTROL_T__DEFAULT_PERMISSIONS:
				return defaultPermissions != null;
			case _0Package.ACCESS_CONTROL_T__SELECTABLE_ENVIRONMENT_ATTRIBUTES:
				return selectableEnvironmentAttributes != null;
			case _0Package.ACCESS_CONTROL_T__DEFAULT_ENVIRONMENT_ATTRIBUTES:
				return defaultEnvironmentAttributes != null;
			case _0Package.ACCESS_CONTROL_T__ACCESS_PERMISSION_RULES:
				return accessPermissionRules != null;
		}
		return super.eIsSet(featureID);
	}

} //AccessControlTImpl
