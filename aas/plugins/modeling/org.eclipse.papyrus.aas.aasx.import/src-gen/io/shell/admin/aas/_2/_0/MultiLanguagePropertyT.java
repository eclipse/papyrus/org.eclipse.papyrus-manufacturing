/**
 */
package io.shell.admin.aas._2._0;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Multi Language Property T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.MultiLanguagePropertyT#getValueId <em>Value Id</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.MultiLanguagePropertyT#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getMultiLanguagePropertyT()
 * @model extendedMetaData="name='multiLanguageProperty_t' kind='elementOnly'"
 * @generated
 */
public interface MultiLanguagePropertyT extends SubmodelElementAbstractT {
	/**
	 * Returns the value of the '<em><b>Value Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Id</em>' containment reference.
	 * @see #setValueId(ReferenceT)
	 * @see io.shell.admin.aas._2._0._0Package#getMultiLanguagePropertyT_ValueId()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='valueId' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getValueId();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.MultiLanguagePropertyT#getValueId <em>Value Id</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Id</em>' containment reference.
	 * @see #getValueId()
	 * @generated
	 */
	void setValueId(ReferenceT value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(LangStringSetT)
	 * @see io.shell.admin.aas._2._0._0Package#getMultiLanguagePropertyT_Value()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='value' namespace='##targetNamespace'"
	 * @generated
	 */
	LangStringSetT getValue();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.MultiLanguagePropertyT#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(LangStringSetT value);

} // MultiLanguagePropertyT
