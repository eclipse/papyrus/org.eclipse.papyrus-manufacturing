/**
 */
package io.shell.admin.aas.abac._2._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Certificate Abstract T</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see io.shell.admin.aas.abac._2._0._0Package#getCertificateAbstractT()
 * @model extendedMetaData="name='certificateAbstract_t' kind='empty'"
 * @generated
 */
public interface CertificateAbstractT extends EObject {
} // CertificateAbstractT
