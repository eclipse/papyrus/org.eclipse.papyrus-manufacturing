/**
 */
package io.shell.admin.aas._2._0;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Range T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.RangeT#getValueType <em>Value Type</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.RangeT#getMin <em>Min</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.RangeT#getMax <em>Max</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getRangeT()
 * @model extendedMetaData="name='range_t' kind='elementOnly'"
 * @generated
 */
public interface RangeT extends SubmodelElementAbstractT {
	/**
	 * Returns the value of the '<em><b>Value Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Type</em>' containment reference.
	 * @see #setValueType(DataTypeDefT)
	 * @see io.shell.admin.aas._2._0._0Package#getRangeT_ValueType()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='valueType' namespace='##targetNamespace'"
	 * @generated
	 */
	DataTypeDefT getValueType();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.RangeT#getValueType <em>Value Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Type</em>' containment reference.
	 * @see #getValueType()
	 * @generated
	 */
	void setValueType(DataTypeDefT value);

	/**
	 * Returns the value of the '<em><b>Min</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min</em>' containment reference.
	 * @see #setMin(ValueDataTypeT)
	 * @see io.shell.admin.aas._2._0._0Package#getRangeT_Min()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='min' namespace='##targetNamespace'"
	 * @generated
	 */
	ValueDataTypeT getMin();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.RangeT#getMin <em>Min</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min</em>' containment reference.
	 * @see #getMin()
	 * @generated
	 */
	void setMin(ValueDataTypeT value);

	/**
	 * Returns the value of the '<em><b>Max</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max</em>' containment reference.
	 * @see #setMax(ValueDataTypeT)
	 * @see io.shell.admin.aas._2._0._0Package#getRangeT_Max()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='max' namespace='##targetNamespace'"
	 * @generated
	 */
	ValueDataTypeT getMax();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.RangeT#getMax <em>Max</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max</em>' containment reference.
	 * @see #getMax()
	 * @generated
	 */
	void setMax(ValueDataTypeT value);

} // RangeT
