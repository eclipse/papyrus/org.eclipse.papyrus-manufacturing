/**
 */
package io.shell.admin.aas.abac._2._0;

import io.shell.admin.aas._2._0.ReferenceT;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Contained Extensions T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.ContainedExtensionsT#getContainedExtension <em>Contained Extension</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas.abac._2._0._0Package#getContainedExtensionsT()
 * @model extendedMetaData="name='containedExtensions_t' kind='elementOnly'"
 * @generated
 */
public interface ContainedExtensionsT extends EObject {
	/**
	 * Returns the value of the '<em><b>Contained Extension</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas._2._0.ReferenceT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contained Extension</em>' containment reference list.
	 * @see io.shell.admin.aas.abac._2._0._0Package#getContainedExtensionsT_ContainedExtension()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='containedExtension' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ReferenceT> getContainedExtension();

} // ContainedExtensionsT
