/**
 */
package io.shell.admin.aas._2._0.impl;

import io.shell.admin.aas._2._0.AasenvT;
import io.shell.admin.aas._2._0.AdministrationT;
import io.shell.admin.aas._2._0.AnnotatedRelationshipElementT;
import io.shell.admin.aas._2._0.AssetAdministrationShellT;
import io.shell.admin.aas._2._0.AssetAdministrationShellsT;
import io.shell.admin.aas._2._0.AssetKindT;
import io.shell.admin.aas._2._0.AssetT;
import io.shell.admin.aas._2._0.AssetsT;
import io.shell.admin.aas._2._0.BasicEventT;
import io.shell.admin.aas._2._0.BlobT;
import io.shell.admin.aas._2._0.BlobTypeT;
import io.shell.admin.aas._2._0.ConceptDescriptionRefsT;
import io.shell.admin.aas._2._0.ConceptDescriptionT;
import io.shell.admin.aas._2._0.ConceptDescriptionsT;
import io.shell.admin.aas._2._0.ConceptDictionariesT;
import io.shell.admin.aas._2._0.ConceptDictionaryT;
import io.shell.admin.aas._2._0.ConstraintT;
import io.shell.admin.aas._2._0.ContainedElementsT;
import io.shell.admin.aas._2._0.DataElementAbstractT;
import io.shell.admin.aas._2._0.DataElementsT;
import io.shell.admin.aas._2._0.DataSpecificationContentT;
import io.shell.admin.aas._2._0.DataTypeDefT;
import io.shell.admin.aas._2._0.DocumentRoot;
import io.shell.admin.aas._2._0.EmbeddedDataSpecificationT;
import io.shell.admin.aas._2._0.EntityT;
import io.shell.admin.aas._2._0.EntityTypeType;
import io.shell.admin.aas._2._0.EventAbstractT;
import io.shell.admin.aas._2._0.FileT;
import io.shell.admin.aas._2._0.FormulaT;
import io.shell.admin.aas._2._0.IdPropertyDefinitionT;
import io.shell.admin.aas._2._0.IdShortT;
import io.shell.admin.aas._2._0.IdTypeType;
import io.shell.admin.aas._2._0.IdTypeType1;
import io.shell.admin.aas._2._0.IdentificationT;
import io.shell.admin.aas._2._0.IdentifierT;
import io.shell.admin.aas._2._0.IdentifierTypeT;
import io.shell.admin.aas._2._0.KeyT;
import io.shell.admin.aas._2._0.KeysT;
import io.shell.admin.aas._2._0.LangStringSetT;
import io.shell.admin.aas._2._0.LangStringT;
import io.shell.admin.aas._2._0.ModelingKindT;
import io.shell.admin.aas._2._0.MultiLanguagePropertyT;
import io.shell.admin.aas._2._0.OperationT;
import io.shell.admin.aas._2._0.OperationVariableT;
import io.shell.admin.aas._2._0.PathTypeT;
import io.shell.admin.aas._2._0.PropertyT;
import io.shell.admin.aas._2._0.QualifierT;
import io.shell.admin.aas._2._0.QualifierTypeT;
import io.shell.admin.aas._2._0.RangeT;
import io.shell.admin.aas._2._0.ReferenceElementT;
import io.shell.admin.aas._2._0.ReferenceT;
import io.shell.admin.aas._2._0.ReferencesT;
import io.shell.admin.aas._2._0.RelationshipElementT;
import io.shell.admin.aas._2._0.SemanticIdT;
import io.shell.admin.aas._2._0.SubmodelElementAbstractT;
import io.shell.admin.aas._2._0.SubmodelElementCollectionT;
import io.shell.admin.aas._2._0.SubmodelElementT;
import io.shell.admin.aas._2._0.SubmodelElementsT;
import io.shell.admin.aas._2._0.SubmodelRefsT;
import io.shell.admin.aas._2._0.SubmodelT;
import io.shell.admin.aas._2._0.SubmodelsT;
import io.shell.admin.aas._2._0.TypeType;
import io.shell.admin.aas._2._0.ValueDataTypeT;
import io.shell.admin.aas._2._0.ViewT;
import io.shell.admin.aas._2._0.ViewsT;
import io.shell.admin.aas._2._0._0Factory;
import io.shell.admin.aas._2._0._0Package;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class _0PackageImpl extends EPackageImpl implements _0Package {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aasenvTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass administrationTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass annotatedRelationshipElementTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetAdministrationShellsTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetAdministrationShellTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetsTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass basicEventTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blobTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blobTypeTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conceptDescriptionRefsTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conceptDescriptionsTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conceptDescriptionTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conceptDictionariesTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conceptDictionaryTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constraintTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass containedElementsTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataElementAbstractTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataElementsTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataSpecificationContentTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataTypeDefTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass documentRootEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass embeddedDataSpecificationTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass entityTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventAbstractTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fileTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass formulaTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass identificationTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass identifierTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass idPropertyDefinitionTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass idShortTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass keysTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass keyTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass langStringSetTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass langStringTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass multiLanguagePropertyTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationVariableTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pathTypeTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propertyTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass qualifierTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass qualifierTypeTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rangeTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass referenceElementTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass referencesTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass referenceTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass relationshipElementTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass semanticIdTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass submodelElementAbstractTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass submodelElementCollectionTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass submodelElementsTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass submodelElementTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass submodelRefsTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass submodelsTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass submodelTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valueDataTypeTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass viewsTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass viewTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum assetKindTEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum entityTypeTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum identifierTypeTEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum idTypeTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum idTypeType1EEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum modelingKindTEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum typeTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType assetKindTObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType entityTypeTEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType entityTypeTypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType identifierTypeTObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType idTypeTypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType idTypeTypeObject1EDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType modelingKindTObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType typeTypeObjectEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see io.shell.admin.aas._2._0._0Package#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private _0PackageImpl() {
		super(eNS_URI, _0Factory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link _0Package#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static _0Package init() {
		if (isInited) return (_0Package)EPackage.Registry.INSTANCE.getEPackage(_0Package.eNS_URI);

		// Obtain or create and register package
		Object registered_0Package = EPackage.Registry.INSTANCE.get(eNS_URI);
		_0PackageImpl the_0Package = registered_0Package instanceof _0PackageImpl ? (_0PackageImpl)registered_0Package : new _0PackageImpl();

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(io.shell.admin.iec61360._2._0._0Package.eNS_URI);
		io.shell.admin.iec61360._2._0.impl._0PackageImpl the_0Package_1 = (io.shell.admin.iec61360._2._0.impl._0PackageImpl)(registeredPackage instanceof io.shell.admin.iec61360._2._0.impl._0PackageImpl ? registeredPackage : io.shell.admin.iec61360._2._0._0Package.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(io.shell.admin.aas.abac._2._0._0Package.eNS_URI);
		io.shell.admin.aas.abac._2._0.impl._0PackageImpl the_0Package_2 = (io.shell.admin.aas.abac._2._0.impl._0PackageImpl)(registeredPackage instanceof io.shell.admin.aas.abac._2._0.impl._0PackageImpl ? registeredPackage : io.shell.admin.aas.abac._2._0._0Package.eINSTANCE);

		// Create package meta-data objects
		the_0Package.createPackageContents();
		the_0Package_1.createPackageContents();
		the_0Package_2.createPackageContents();

		// Initialize created meta-data
		the_0Package.initializePackageContents();
		the_0Package_1.initializePackageContents();
		the_0Package_2.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		the_0Package.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(_0Package.eNS_URI, the_0Package);
		return the_0Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAasenvT() {
		return aasenvTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAasenvT_AssetAdministrationShells() {
		return (EReference)aasenvTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAasenvT_Assets() {
		return (EReference)aasenvTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAasenvT_Submodels() {
		return (EReference)aasenvTEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAasenvT_ConceptDescriptions() {
		return (EReference)aasenvTEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAdministrationT() {
		return administrationTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAdministrationT_Version() {
		return (EAttribute)administrationTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAdministrationT_Revision() {
		return (EAttribute)administrationTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnnotatedRelationshipElementT() {
		return annotatedRelationshipElementTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnnotatedRelationshipElementT_Annotations() {
		return (EReference)annotatedRelationshipElementTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssetAdministrationShellsT() {
		return assetAdministrationShellsTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssetAdministrationShellsT_AssetAdministrationShell() {
		return (EReference)assetAdministrationShellsTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssetAdministrationShellT() {
		return assetAdministrationShellTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssetAdministrationShellT_IdShort() {
		return (EReference)assetAdministrationShellTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssetAdministrationShellT_Category() {
		return (EAttribute)assetAdministrationShellTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssetAdministrationShellT_Description() {
		return (EReference)assetAdministrationShellTEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssetAdministrationShellT_Parent() {
		return (EReference)assetAdministrationShellTEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssetAdministrationShellT_Identification() {
		return (EReference)assetAdministrationShellTEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssetAdministrationShellT_Administration() {
		return (EReference)assetAdministrationShellTEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssetAdministrationShellT_EmbeddedDataSpecification() {
		return (EReference)assetAdministrationShellTEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssetAdministrationShellT_DerivedFrom() {
		return (EReference)assetAdministrationShellTEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssetAdministrationShellT_AssetRef() {
		return (EReference)assetAdministrationShellTEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssetAdministrationShellT_SubmodelRefs() {
		return (EReference)assetAdministrationShellTEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssetAdministrationShellT_Views() {
		return (EReference)assetAdministrationShellTEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssetAdministrationShellT_ConceptDictionaries() {
		return (EReference)assetAdministrationShellTEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssetAdministrationShellT_Security() {
		return (EReference)assetAdministrationShellTEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssetsT() {
		return assetsTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssetsT_Asset() {
		return (EReference)assetsTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssetT() {
		return assetTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssetT_IdShort() {
		return (EReference)assetTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssetT_Category() {
		return (EAttribute)assetTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssetT_Description() {
		return (EReference)assetTEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssetT_Parent() {
		return (EReference)assetTEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssetT_Identification() {
		return (EReference)assetTEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssetT_Administration() {
		return (EReference)assetTEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssetT_EmbeddedDataSpecification() {
		return (EReference)assetTEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssetT_AssetIdentificationModelRef() {
		return (EReference)assetTEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssetT_BillOfMaterialRef() {
		return (EReference)assetTEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssetT_Kind() {
		return (EAttribute)assetTEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBasicEventT() {
		return basicEventTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBasicEventT_Observed() {
		return (EReference)basicEventTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlobT() {
		return blobTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlobT_Value() {
		return (EReference)blobTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlobT_MimeType() {
		return (EAttribute)blobTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlobTypeT() {
		return blobTypeTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlobTypeT_Value() {
		return (EAttribute)blobTypeTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConceptDescriptionRefsT() {
		return conceptDescriptionRefsTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConceptDescriptionRefsT_ConceptDescriptionRef() {
		return (EReference)conceptDescriptionRefsTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConceptDescriptionsT() {
		return conceptDescriptionsTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConceptDescriptionsT_ConceptDescription() {
		return (EReference)conceptDescriptionsTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConceptDescriptionT() {
		return conceptDescriptionTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConceptDescriptionT_IdShort() {
		return (EReference)conceptDescriptionTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConceptDescriptionT_Category() {
		return (EAttribute)conceptDescriptionTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConceptDescriptionT_Description() {
		return (EReference)conceptDescriptionTEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConceptDescriptionT_Parent() {
		return (EReference)conceptDescriptionTEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConceptDescriptionT_Identification() {
		return (EReference)conceptDescriptionTEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConceptDescriptionT_Administration() {
		return (EReference)conceptDescriptionTEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConceptDescriptionT_EmbeddedDataSpecification() {
		return (EReference)conceptDescriptionTEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConceptDescriptionT_IsCaseOf() {
		return (EReference)conceptDescriptionTEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConceptDictionariesT() {
		return conceptDictionariesTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConceptDictionariesT_ConceptDictionary() {
		return (EReference)conceptDictionariesTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConceptDictionaryT() {
		return conceptDictionaryTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConceptDictionaryT_IdShort() {
		return (EReference)conceptDictionaryTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConceptDictionaryT_Category() {
		return (EAttribute)conceptDictionaryTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConceptDictionaryT_Description() {
		return (EReference)conceptDictionaryTEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConceptDictionaryT_Parent() {
		return (EReference)conceptDictionaryTEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConceptDictionaryT_ConceptDescriptionRefs() {
		return (EReference)conceptDictionaryTEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConstraintT() {
		return constraintTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConstraintT_Formula() {
		return (EReference)constraintTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConstraintT_Qualifier() {
		return (EReference)constraintTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getContainedElementsT() {
		return containedElementsTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getContainedElementsT_ContainedElementRef() {
		return (EReference)containedElementsTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataElementAbstractT() {
		return dataElementAbstractTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataElementAbstractT_MultiLanguageProperty() {
		return (EReference)dataElementAbstractTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataElementAbstractT_Property() {
		return (EReference)dataElementAbstractTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataElementAbstractT_Range() {
		return (EReference)dataElementAbstractTEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataElementAbstractT_Blob() {
		return (EReference)dataElementAbstractTEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataElementAbstractT_File() {
		return (EReference)dataElementAbstractTEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataElementAbstractT_ReferenceElement() {
		return (EReference)dataElementAbstractTEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataElementsT() {
		return dataElementsTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataElementsT_DataElement() {
		return (EReference)dataElementsTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataSpecificationContentT() {
		return dataSpecificationContentTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataSpecificationContentT_DataSpecificationIEC61360() {
		return (EReference)dataSpecificationContentTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataTypeDefT() {
		return dataTypeDefTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataTypeDefT_Value() {
		return (EAttribute)dataTypeDefTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDocumentRoot() {
		return documentRootEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDocumentRoot_Mixed() {
		return (EAttribute)documentRootEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_XMLNSPrefixMap() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_XSISchemaLocation() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_Aasenv() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_Key() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEmbeddedDataSpecificationT() {
		return embeddedDataSpecificationTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEmbeddedDataSpecificationT_DataSpecificationContent() {
		return (EReference)embeddedDataSpecificationTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEmbeddedDataSpecificationT_DataSpecification() {
		return (EReference)embeddedDataSpecificationTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEntityT() {
		return entityTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEntityT_Statements() {
		return (EReference)entityTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEntityT_EntityType() {
		return (EAttribute)entityTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEntityT_AssetRef() {
		return (EReference)entityTEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEventAbstractT() {
		return eventAbstractTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFileT() {
		return fileTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFileT_MimeType() {
		return (EAttribute)fileTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFileT_Value() {
		return (EReference)fileTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFormulaT() {
		return formulaTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFormulaT_DependsOnRefs() {
		return (EReference)formulaTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIdentificationT() {
		return identificationTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdentificationT_Value() {
		return (EAttribute)identificationTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdentificationT_IdType() {
		return (EAttribute)identificationTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIdentifierT() {
		return identifierTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdentifierT_Id() {
		return (EAttribute)identifierTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdentifierT_IdType() {
		return (EAttribute)identifierTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIdPropertyDefinitionT() {
		return idPropertyDefinitionTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdPropertyDefinitionT_Value() {
		return (EAttribute)idPropertyDefinitionTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdPropertyDefinitionT_IdType() {
		return (EAttribute)idPropertyDefinitionTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIdShortT() {
		return idShortTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdShortT_Value() {
		return (EAttribute)idShortTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getKeysT() {
		return keysTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getKeysT_Key() {
		return (EReference)keysTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getKeyT() {
		return keyTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getKeyT_Value() {
		return (EAttribute)keyTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getKeyT_IdType() {
		return (EAttribute)keyTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getKeyT_Local() {
		return (EAttribute)keyTEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getKeyT_Type() {
		return (EAttribute)keyTEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLangStringSetT() {
		return langStringSetTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLangStringSetT_LangString() {
		return (EReference)langStringSetTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLangStringT() {
		return langStringTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLangStringT_Value() {
		return (EAttribute)langStringTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLangStringT_Lang() {
		return (EAttribute)langStringTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMultiLanguagePropertyT() {
		return multiLanguagePropertyTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMultiLanguagePropertyT_ValueId() {
		return (EReference)multiLanguagePropertyTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMultiLanguagePropertyT_Value() {
		return (EReference)multiLanguagePropertyTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationT() {
		return operationTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationT_InputVariable() {
		return (EReference)operationTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationT_OutputVariable() {
		return (EReference)operationTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationT_InoutputVariable() {
		return (EReference)operationTEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationVariableT() {
		return operationVariableTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationVariableT_Value() {
		return (EReference)operationVariableTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPathTypeT() {
		return pathTypeTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPathTypeT_Value() {
		return (EAttribute)pathTypeTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPropertyT() {
		return propertyTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertyT_ValueType() {
		return (EReference)propertyTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertyT_Value() {
		return (EReference)propertyTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertyT_ValueId() {
		return (EReference)propertyTEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getQualifierT() {
		return qualifierTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getQualifierT_SemanticId() {
		return (EReference)qualifierTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getQualifierT_Type() {
		return (EReference)qualifierTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getQualifierT_ValueType() {
		return (EReference)qualifierTEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getQualifierT_ValueId() {
		return (EReference)qualifierTEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getQualifierT_Value() {
		return (EReference)qualifierTEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getQualifierTypeT() {
		return qualifierTypeTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getQualifierTypeT_Value() {
		return (EAttribute)qualifierTypeTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRangeT() {
		return rangeTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRangeT_ValueType() {
		return (EReference)rangeTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRangeT_Min() {
		return (EReference)rangeTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRangeT_Max() {
		return (EReference)rangeTEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReferenceElementT() {
		return referenceElementTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferenceElementT_Value() {
		return (EReference)referenceElementTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReferencesT() {
		return referencesTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferencesT_Reference() {
		return (EReference)referencesTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReferenceT() {
		return referenceTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferenceT_Keys() {
		return (EReference)referenceTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRelationshipElementT() {
		return relationshipElementTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRelationshipElementT_First() {
		return (EReference)relationshipElementTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRelationshipElementT_Second() {
		return (EReference)relationshipElementTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSemanticIdT() {
		return semanticIdTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSubmodelElementAbstractT() {
		return submodelElementAbstractTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelElementAbstractT_IdShort() {
		return (EReference)submodelElementAbstractTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSubmodelElementAbstractT_Category() {
		return (EAttribute)submodelElementAbstractTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelElementAbstractT_Description() {
		return (EReference)submodelElementAbstractTEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelElementAbstractT_Parent() {
		return (EReference)submodelElementAbstractTEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSubmodelElementAbstractT_Kind() {
		return (EAttribute)submodelElementAbstractTEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelElementAbstractT_SemanticId() {
		return (EReference)submodelElementAbstractTEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelElementAbstractT_Qualifier() {
		return (EReference)submodelElementAbstractTEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelElementAbstractT_EmbeddedDataSpecification() {
		return (EReference)submodelElementAbstractTEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSubmodelElementCollectionT() {
		return submodelElementCollectionTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelElementCollectionT_Value() {
		return (EReference)submodelElementCollectionTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSubmodelElementCollectionT_Ordered() {
		return (EAttribute)submodelElementCollectionTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSubmodelElementCollectionT_AllowDuplicates() {
		return (EAttribute)submodelElementCollectionTEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSubmodelElementsT() {
		return submodelElementsTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelElementsT_SubmodelElement() {
		return (EReference)submodelElementsTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSubmodelElementT() {
		return submodelElementTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelElementT_MultiLanguageProperty() {
		return (EReference)submodelElementTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelElementT_Property() {
		return (EReference)submodelElementTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelElementT_Range() {
		return (EReference)submodelElementTEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelElementT_Blob() {
		return (EReference)submodelElementTEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelElementT_File() {
		return (EReference)submodelElementTEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelElementT_ReferenceElement() {
		return (EReference)submodelElementTEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelElementT_AnnotatedRelationshipElement() {
		return (EReference)submodelElementTEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelElementT_BasicEvent() {
		return (EReference)submodelElementTEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelElementT_Capability() {
		return (EReference)submodelElementTEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelElementT_Entity() {
		return (EReference)submodelElementTEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelElementT_Operation() {
		return (EReference)submodelElementTEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelElementT_RelationshipElement() {
		return (EReference)submodelElementTEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelElementT_SubmodelElementCollection() {
		return (EReference)submodelElementTEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSubmodelRefsT() {
		return submodelRefsTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelRefsT_SubmodelRef() {
		return (EReference)submodelRefsTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSubmodelsT() {
		return submodelsTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelsT_Submodel() {
		return (EReference)submodelsTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSubmodelT() {
		return submodelTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelT_IdShort() {
		return (EReference)submodelTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSubmodelT_Category() {
		return (EAttribute)submodelTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelT_Description() {
		return (EReference)submodelTEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelT_Parent() {
		return (EReference)submodelTEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelT_Identification() {
		return (EReference)submodelTEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelT_Administration() {
		return (EReference)submodelTEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSubmodelT_Kind() {
		return (EAttribute)submodelTEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelT_SemanticId() {
		return (EReference)submodelTEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelT_Qualifier() {
		return (EReference)submodelTEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelT_EmbeddedDataSpecification() {
		return (EReference)submodelTEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubmodelT_SubmodelElements() {
		return (EReference)submodelTEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getValueDataTypeT() {
		return valueDataTypeTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getValueDataTypeT_Value() {
		return (EAttribute)valueDataTypeTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getViewsT() {
		return viewsTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getViewsT_View() {
		return (EReference)viewsTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getViewT() {
		return viewTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getViewT_IdShort() {
		return (EReference)viewTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getViewT_Category() {
		return (EAttribute)viewTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getViewT_Description() {
		return (EReference)viewTEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getViewT_Parent() {
		return (EReference)viewTEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getViewT_SemanticId() {
		return (EReference)viewTEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getViewT_EmbeddedDataSpecification() {
		return (EReference)viewTEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getViewT_ContainedElements() {
		return (EReference)viewTEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAssetKindT() {
		return assetKindTEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getEntityTypeType() {
		return entityTypeTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getIdentifierTypeT() {
		return identifierTypeTEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getIdTypeType() {
		return idTypeTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getIdTypeType1() {
		return idTypeType1EEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getModelingKindT() {
		return modelingKindTEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getTypeType() {
		return typeTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getAssetKindTObject() {
		return assetKindTObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEntityTypeT() {
		return entityTypeTEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEntityTypeTypeObject() {
		return entityTypeTypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getIdentifierTypeTObject() {
		return identifierTypeTObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getIdTypeTypeObject() {
		return idTypeTypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getIdTypeTypeObject1() {
		return idTypeTypeObject1EDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getModelingKindTObject() {
		return modelingKindTObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getTypeTypeObject() {
		return typeTypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public _0Factory get_0Factory() {
		return (_0Factory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		aasenvTEClass = createEClass(AASENV_T);
		createEReference(aasenvTEClass, AASENV_T__ASSET_ADMINISTRATION_SHELLS);
		createEReference(aasenvTEClass, AASENV_T__ASSETS);
		createEReference(aasenvTEClass, AASENV_T__SUBMODELS);
		createEReference(aasenvTEClass, AASENV_T__CONCEPT_DESCRIPTIONS);

		administrationTEClass = createEClass(ADMINISTRATION_T);
		createEAttribute(administrationTEClass, ADMINISTRATION_T__VERSION);
		createEAttribute(administrationTEClass, ADMINISTRATION_T__REVISION);

		annotatedRelationshipElementTEClass = createEClass(ANNOTATED_RELATIONSHIP_ELEMENT_T);
		createEReference(annotatedRelationshipElementTEClass, ANNOTATED_RELATIONSHIP_ELEMENT_T__ANNOTATIONS);

		assetAdministrationShellsTEClass = createEClass(ASSET_ADMINISTRATION_SHELLS_T);
		createEReference(assetAdministrationShellsTEClass, ASSET_ADMINISTRATION_SHELLS_T__ASSET_ADMINISTRATION_SHELL);

		assetAdministrationShellTEClass = createEClass(ASSET_ADMINISTRATION_SHELL_T);
		createEReference(assetAdministrationShellTEClass, ASSET_ADMINISTRATION_SHELL_T__ID_SHORT);
		createEAttribute(assetAdministrationShellTEClass, ASSET_ADMINISTRATION_SHELL_T__CATEGORY);
		createEReference(assetAdministrationShellTEClass, ASSET_ADMINISTRATION_SHELL_T__DESCRIPTION);
		createEReference(assetAdministrationShellTEClass, ASSET_ADMINISTRATION_SHELL_T__PARENT);
		createEReference(assetAdministrationShellTEClass, ASSET_ADMINISTRATION_SHELL_T__IDENTIFICATION);
		createEReference(assetAdministrationShellTEClass, ASSET_ADMINISTRATION_SHELL_T__ADMINISTRATION);
		createEReference(assetAdministrationShellTEClass, ASSET_ADMINISTRATION_SHELL_T__EMBEDDED_DATA_SPECIFICATION);
		createEReference(assetAdministrationShellTEClass, ASSET_ADMINISTRATION_SHELL_T__DERIVED_FROM);
		createEReference(assetAdministrationShellTEClass, ASSET_ADMINISTRATION_SHELL_T__ASSET_REF);
		createEReference(assetAdministrationShellTEClass, ASSET_ADMINISTRATION_SHELL_T__SUBMODEL_REFS);
		createEReference(assetAdministrationShellTEClass, ASSET_ADMINISTRATION_SHELL_T__VIEWS);
		createEReference(assetAdministrationShellTEClass, ASSET_ADMINISTRATION_SHELL_T__CONCEPT_DICTIONARIES);
		createEReference(assetAdministrationShellTEClass, ASSET_ADMINISTRATION_SHELL_T__SECURITY);

		assetsTEClass = createEClass(ASSETS_T);
		createEReference(assetsTEClass, ASSETS_T__ASSET);

		assetTEClass = createEClass(ASSET_T);
		createEReference(assetTEClass, ASSET_T__ID_SHORT);
		createEAttribute(assetTEClass, ASSET_T__CATEGORY);
		createEReference(assetTEClass, ASSET_T__DESCRIPTION);
		createEReference(assetTEClass, ASSET_T__PARENT);
		createEReference(assetTEClass, ASSET_T__IDENTIFICATION);
		createEReference(assetTEClass, ASSET_T__ADMINISTRATION);
		createEReference(assetTEClass, ASSET_T__EMBEDDED_DATA_SPECIFICATION);
		createEReference(assetTEClass, ASSET_T__ASSET_IDENTIFICATION_MODEL_REF);
		createEReference(assetTEClass, ASSET_T__BILL_OF_MATERIAL_REF);
		createEAttribute(assetTEClass, ASSET_T__KIND);

		basicEventTEClass = createEClass(BASIC_EVENT_T);
		createEReference(basicEventTEClass, BASIC_EVENT_T__OBSERVED);

		blobTEClass = createEClass(BLOB_T);
		createEReference(blobTEClass, BLOB_T__VALUE);
		createEAttribute(blobTEClass, BLOB_T__MIME_TYPE);

		blobTypeTEClass = createEClass(BLOB_TYPE_T);
		createEAttribute(blobTypeTEClass, BLOB_TYPE_T__VALUE);

		conceptDescriptionRefsTEClass = createEClass(CONCEPT_DESCRIPTION_REFS_T);
		createEReference(conceptDescriptionRefsTEClass, CONCEPT_DESCRIPTION_REFS_T__CONCEPT_DESCRIPTION_REF);

		conceptDescriptionsTEClass = createEClass(CONCEPT_DESCRIPTIONS_T);
		createEReference(conceptDescriptionsTEClass, CONCEPT_DESCRIPTIONS_T__CONCEPT_DESCRIPTION);

		conceptDescriptionTEClass = createEClass(CONCEPT_DESCRIPTION_T);
		createEReference(conceptDescriptionTEClass, CONCEPT_DESCRIPTION_T__ID_SHORT);
		createEAttribute(conceptDescriptionTEClass, CONCEPT_DESCRIPTION_T__CATEGORY);
		createEReference(conceptDescriptionTEClass, CONCEPT_DESCRIPTION_T__DESCRIPTION);
		createEReference(conceptDescriptionTEClass, CONCEPT_DESCRIPTION_T__PARENT);
		createEReference(conceptDescriptionTEClass, CONCEPT_DESCRIPTION_T__IDENTIFICATION);
		createEReference(conceptDescriptionTEClass, CONCEPT_DESCRIPTION_T__ADMINISTRATION);
		createEReference(conceptDescriptionTEClass, CONCEPT_DESCRIPTION_T__EMBEDDED_DATA_SPECIFICATION);
		createEReference(conceptDescriptionTEClass, CONCEPT_DESCRIPTION_T__IS_CASE_OF);

		conceptDictionariesTEClass = createEClass(CONCEPT_DICTIONARIES_T);
		createEReference(conceptDictionariesTEClass, CONCEPT_DICTIONARIES_T__CONCEPT_DICTIONARY);

		conceptDictionaryTEClass = createEClass(CONCEPT_DICTIONARY_T);
		createEReference(conceptDictionaryTEClass, CONCEPT_DICTIONARY_T__ID_SHORT);
		createEAttribute(conceptDictionaryTEClass, CONCEPT_DICTIONARY_T__CATEGORY);
		createEReference(conceptDictionaryTEClass, CONCEPT_DICTIONARY_T__DESCRIPTION);
		createEReference(conceptDictionaryTEClass, CONCEPT_DICTIONARY_T__PARENT);
		createEReference(conceptDictionaryTEClass, CONCEPT_DICTIONARY_T__CONCEPT_DESCRIPTION_REFS);

		constraintTEClass = createEClass(CONSTRAINT_T);
		createEReference(constraintTEClass, CONSTRAINT_T__FORMULA);
		createEReference(constraintTEClass, CONSTRAINT_T__QUALIFIER);

		containedElementsTEClass = createEClass(CONTAINED_ELEMENTS_T);
		createEReference(containedElementsTEClass, CONTAINED_ELEMENTS_T__CONTAINED_ELEMENT_REF);

		dataElementAbstractTEClass = createEClass(DATA_ELEMENT_ABSTRACT_T);
		createEReference(dataElementAbstractTEClass, DATA_ELEMENT_ABSTRACT_T__MULTI_LANGUAGE_PROPERTY);
		createEReference(dataElementAbstractTEClass, DATA_ELEMENT_ABSTRACT_T__PROPERTY);
		createEReference(dataElementAbstractTEClass, DATA_ELEMENT_ABSTRACT_T__RANGE);
		createEReference(dataElementAbstractTEClass, DATA_ELEMENT_ABSTRACT_T__BLOB);
		createEReference(dataElementAbstractTEClass, DATA_ELEMENT_ABSTRACT_T__FILE);
		createEReference(dataElementAbstractTEClass, DATA_ELEMENT_ABSTRACT_T__REFERENCE_ELEMENT);

		dataElementsTEClass = createEClass(DATA_ELEMENTS_T);
		createEReference(dataElementsTEClass, DATA_ELEMENTS_T__DATA_ELEMENT);

		dataSpecificationContentTEClass = createEClass(DATA_SPECIFICATION_CONTENT_T);
		createEReference(dataSpecificationContentTEClass, DATA_SPECIFICATION_CONTENT_T__DATA_SPECIFICATION_IEC61360);

		dataTypeDefTEClass = createEClass(DATA_TYPE_DEF_T);
		createEAttribute(dataTypeDefTEClass, DATA_TYPE_DEF_T__VALUE);

		documentRootEClass = createEClass(DOCUMENT_ROOT);
		createEAttribute(documentRootEClass, DOCUMENT_ROOT__MIXED);
		createEReference(documentRootEClass, DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
		createEReference(documentRootEClass, DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
		createEReference(documentRootEClass, DOCUMENT_ROOT__AASENV);
		createEReference(documentRootEClass, DOCUMENT_ROOT__KEY);

		embeddedDataSpecificationTEClass = createEClass(EMBEDDED_DATA_SPECIFICATION_T);
		createEReference(embeddedDataSpecificationTEClass, EMBEDDED_DATA_SPECIFICATION_T__DATA_SPECIFICATION_CONTENT);
		createEReference(embeddedDataSpecificationTEClass, EMBEDDED_DATA_SPECIFICATION_T__DATA_SPECIFICATION);

		entityTEClass = createEClass(ENTITY_T);
		createEReference(entityTEClass, ENTITY_T__STATEMENTS);
		createEAttribute(entityTEClass, ENTITY_T__ENTITY_TYPE);
		createEReference(entityTEClass, ENTITY_T__ASSET_REF);

		eventAbstractTEClass = createEClass(EVENT_ABSTRACT_T);

		fileTEClass = createEClass(FILE_T);
		createEAttribute(fileTEClass, FILE_T__MIME_TYPE);
		createEReference(fileTEClass, FILE_T__VALUE);

		formulaTEClass = createEClass(FORMULA_T);
		createEReference(formulaTEClass, FORMULA_T__DEPENDS_ON_REFS);

		identificationTEClass = createEClass(IDENTIFICATION_T);
		createEAttribute(identificationTEClass, IDENTIFICATION_T__VALUE);
		createEAttribute(identificationTEClass, IDENTIFICATION_T__ID_TYPE);

		identifierTEClass = createEClass(IDENTIFIER_T);
		createEAttribute(identifierTEClass, IDENTIFIER_T__ID);
		createEAttribute(identifierTEClass, IDENTIFIER_T__ID_TYPE);

		idPropertyDefinitionTEClass = createEClass(ID_PROPERTY_DEFINITION_T);
		createEAttribute(idPropertyDefinitionTEClass, ID_PROPERTY_DEFINITION_T__VALUE);
		createEAttribute(idPropertyDefinitionTEClass, ID_PROPERTY_DEFINITION_T__ID_TYPE);

		idShortTEClass = createEClass(ID_SHORT_T);
		createEAttribute(idShortTEClass, ID_SHORT_T__VALUE);

		keysTEClass = createEClass(KEYS_T);
		createEReference(keysTEClass, KEYS_T__KEY);

		keyTEClass = createEClass(KEY_T);
		createEAttribute(keyTEClass, KEY_T__VALUE);
		createEAttribute(keyTEClass, KEY_T__ID_TYPE);
		createEAttribute(keyTEClass, KEY_T__LOCAL);
		createEAttribute(keyTEClass, KEY_T__TYPE);

		langStringSetTEClass = createEClass(LANG_STRING_SET_T);
		createEReference(langStringSetTEClass, LANG_STRING_SET_T__LANG_STRING);

		langStringTEClass = createEClass(LANG_STRING_T);
		createEAttribute(langStringTEClass, LANG_STRING_T__VALUE);
		createEAttribute(langStringTEClass, LANG_STRING_T__LANG);

		multiLanguagePropertyTEClass = createEClass(MULTI_LANGUAGE_PROPERTY_T);
		createEReference(multiLanguagePropertyTEClass, MULTI_LANGUAGE_PROPERTY_T__VALUE_ID);
		createEReference(multiLanguagePropertyTEClass, MULTI_LANGUAGE_PROPERTY_T__VALUE);

		operationTEClass = createEClass(OPERATION_T);
		createEReference(operationTEClass, OPERATION_T__INPUT_VARIABLE);
		createEReference(operationTEClass, OPERATION_T__OUTPUT_VARIABLE);
		createEReference(operationTEClass, OPERATION_T__INOUTPUT_VARIABLE);

		operationVariableTEClass = createEClass(OPERATION_VARIABLE_T);
		createEReference(operationVariableTEClass, OPERATION_VARIABLE_T__VALUE);

		pathTypeTEClass = createEClass(PATH_TYPE_T);
		createEAttribute(pathTypeTEClass, PATH_TYPE_T__VALUE);

		propertyTEClass = createEClass(PROPERTY_T);
		createEReference(propertyTEClass, PROPERTY_T__VALUE_TYPE);
		createEReference(propertyTEClass, PROPERTY_T__VALUE);
		createEReference(propertyTEClass, PROPERTY_T__VALUE_ID);

		qualifierTEClass = createEClass(QUALIFIER_T);
		createEReference(qualifierTEClass, QUALIFIER_T__SEMANTIC_ID);
		createEReference(qualifierTEClass, QUALIFIER_T__TYPE);
		createEReference(qualifierTEClass, QUALIFIER_T__VALUE_TYPE);
		createEReference(qualifierTEClass, QUALIFIER_T__VALUE_ID);
		createEReference(qualifierTEClass, QUALIFIER_T__VALUE);

		qualifierTypeTEClass = createEClass(QUALIFIER_TYPE_T);
		createEAttribute(qualifierTypeTEClass, QUALIFIER_TYPE_T__VALUE);

		rangeTEClass = createEClass(RANGE_T);
		createEReference(rangeTEClass, RANGE_T__VALUE_TYPE);
		createEReference(rangeTEClass, RANGE_T__MIN);
		createEReference(rangeTEClass, RANGE_T__MAX);

		referenceElementTEClass = createEClass(REFERENCE_ELEMENT_T);
		createEReference(referenceElementTEClass, REFERENCE_ELEMENT_T__VALUE);

		referencesTEClass = createEClass(REFERENCES_T);
		createEReference(referencesTEClass, REFERENCES_T__REFERENCE);

		referenceTEClass = createEClass(REFERENCE_T);
		createEReference(referenceTEClass, REFERENCE_T__KEYS);

		relationshipElementTEClass = createEClass(RELATIONSHIP_ELEMENT_T);
		createEReference(relationshipElementTEClass, RELATIONSHIP_ELEMENT_T__FIRST);
		createEReference(relationshipElementTEClass, RELATIONSHIP_ELEMENT_T__SECOND);

		semanticIdTEClass = createEClass(SEMANTIC_ID_T);

		submodelElementAbstractTEClass = createEClass(SUBMODEL_ELEMENT_ABSTRACT_T);
		createEReference(submodelElementAbstractTEClass, SUBMODEL_ELEMENT_ABSTRACT_T__ID_SHORT);
		createEAttribute(submodelElementAbstractTEClass, SUBMODEL_ELEMENT_ABSTRACT_T__CATEGORY);
		createEReference(submodelElementAbstractTEClass, SUBMODEL_ELEMENT_ABSTRACT_T__DESCRIPTION);
		createEReference(submodelElementAbstractTEClass, SUBMODEL_ELEMENT_ABSTRACT_T__PARENT);
		createEAttribute(submodelElementAbstractTEClass, SUBMODEL_ELEMENT_ABSTRACT_T__KIND);
		createEReference(submodelElementAbstractTEClass, SUBMODEL_ELEMENT_ABSTRACT_T__SEMANTIC_ID);
		createEReference(submodelElementAbstractTEClass, SUBMODEL_ELEMENT_ABSTRACT_T__QUALIFIER);
		createEReference(submodelElementAbstractTEClass, SUBMODEL_ELEMENT_ABSTRACT_T__EMBEDDED_DATA_SPECIFICATION);

		submodelElementCollectionTEClass = createEClass(SUBMODEL_ELEMENT_COLLECTION_T);
		createEReference(submodelElementCollectionTEClass, SUBMODEL_ELEMENT_COLLECTION_T__VALUE);
		createEAttribute(submodelElementCollectionTEClass, SUBMODEL_ELEMENT_COLLECTION_T__ORDERED);
		createEAttribute(submodelElementCollectionTEClass, SUBMODEL_ELEMENT_COLLECTION_T__ALLOW_DUPLICATES);

		submodelElementsTEClass = createEClass(SUBMODEL_ELEMENTS_T);
		createEReference(submodelElementsTEClass, SUBMODEL_ELEMENTS_T__SUBMODEL_ELEMENT);

		submodelElementTEClass = createEClass(SUBMODEL_ELEMENT_T);
		createEReference(submodelElementTEClass, SUBMODEL_ELEMENT_T__MULTI_LANGUAGE_PROPERTY);
		createEReference(submodelElementTEClass, SUBMODEL_ELEMENT_T__PROPERTY);
		createEReference(submodelElementTEClass, SUBMODEL_ELEMENT_T__RANGE);
		createEReference(submodelElementTEClass, SUBMODEL_ELEMENT_T__BLOB);
		createEReference(submodelElementTEClass, SUBMODEL_ELEMENT_T__FILE);
		createEReference(submodelElementTEClass, SUBMODEL_ELEMENT_T__REFERENCE_ELEMENT);
		createEReference(submodelElementTEClass, SUBMODEL_ELEMENT_T__ANNOTATED_RELATIONSHIP_ELEMENT);
		createEReference(submodelElementTEClass, SUBMODEL_ELEMENT_T__BASIC_EVENT);
		createEReference(submodelElementTEClass, SUBMODEL_ELEMENT_T__CAPABILITY);
		createEReference(submodelElementTEClass, SUBMODEL_ELEMENT_T__ENTITY);
		createEReference(submodelElementTEClass, SUBMODEL_ELEMENT_T__OPERATION);
		createEReference(submodelElementTEClass, SUBMODEL_ELEMENT_T__RELATIONSHIP_ELEMENT);
		createEReference(submodelElementTEClass, SUBMODEL_ELEMENT_T__SUBMODEL_ELEMENT_COLLECTION);

		submodelRefsTEClass = createEClass(SUBMODEL_REFS_T);
		createEReference(submodelRefsTEClass, SUBMODEL_REFS_T__SUBMODEL_REF);

		submodelsTEClass = createEClass(SUBMODELS_T);
		createEReference(submodelsTEClass, SUBMODELS_T__SUBMODEL);

		submodelTEClass = createEClass(SUBMODEL_T);
		createEReference(submodelTEClass, SUBMODEL_T__ID_SHORT);
		createEAttribute(submodelTEClass, SUBMODEL_T__CATEGORY);
		createEReference(submodelTEClass, SUBMODEL_T__DESCRIPTION);
		createEReference(submodelTEClass, SUBMODEL_T__PARENT);
		createEReference(submodelTEClass, SUBMODEL_T__IDENTIFICATION);
		createEReference(submodelTEClass, SUBMODEL_T__ADMINISTRATION);
		createEAttribute(submodelTEClass, SUBMODEL_T__KIND);
		createEReference(submodelTEClass, SUBMODEL_T__SEMANTIC_ID);
		createEReference(submodelTEClass, SUBMODEL_T__QUALIFIER);
		createEReference(submodelTEClass, SUBMODEL_T__EMBEDDED_DATA_SPECIFICATION);
		createEReference(submodelTEClass, SUBMODEL_T__SUBMODEL_ELEMENTS);

		valueDataTypeTEClass = createEClass(VALUE_DATA_TYPE_T);
		createEAttribute(valueDataTypeTEClass, VALUE_DATA_TYPE_T__VALUE);

		viewsTEClass = createEClass(VIEWS_T);
		createEReference(viewsTEClass, VIEWS_T__VIEW);

		viewTEClass = createEClass(VIEW_T);
		createEReference(viewTEClass, VIEW_T__ID_SHORT);
		createEAttribute(viewTEClass, VIEW_T__CATEGORY);
		createEReference(viewTEClass, VIEW_T__DESCRIPTION);
		createEReference(viewTEClass, VIEW_T__PARENT);
		createEReference(viewTEClass, VIEW_T__SEMANTIC_ID);
		createEReference(viewTEClass, VIEW_T__EMBEDDED_DATA_SPECIFICATION);
		createEReference(viewTEClass, VIEW_T__CONTAINED_ELEMENTS);

		// Create enums
		assetKindTEEnum = createEEnum(ASSET_KIND_T);
		entityTypeTypeEEnum = createEEnum(ENTITY_TYPE_TYPE);
		identifierTypeTEEnum = createEEnum(IDENTIFIER_TYPE_T);
		idTypeTypeEEnum = createEEnum(ID_TYPE_TYPE);
		idTypeType1EEnum = createEEnum(ID_TYPE_TYPE1);
		modelingKindTEEnum = createEEnum(MODELING_KIND_T);
		typeTypeEEnum = createEEnum(TYPE_TYPE);

		// Create data types
		assetKindTObjectEDataType = createEDataType(ASSET_KIND_TOBJECT);
		entityTypeTEDataType = createEDataType(ENTITY_TYPE_T);
		entityTypeTypeObjectEDataType = createEDataType(ENTITY_TYPE_TYPE_OBJECT);
		identifierTypeTObjectEDataType = createEDataType(IDENTIFIER_TYPE_TOBJECT);
		idTypeTypeObjectEDataType = createEDataType(ID_TYPE_TYPE_OBJECT);
		idTypeTypeObject1EDataType = createEDataType(ID_TYPE_TYPE_OBJECT1);
		modelingKindTObjectEDataType = createEDataType(MODELING_KIND_TOBJECT);
		typeTypeObjectEDataType = createEDataType(TYPE_TYPE_OBJECT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);
		io.shell.admin.aas.abac._2._0._0Package the_0Package_2 = (io.shell.admin.aas.abac._2._0._0Package)EPackage.Registry.INSTANCE.getEPackage(io.shell.admin.aas.abac._2._0._0Package.eNS_URI);
		io.shell.admin.iec61360._2._0._0Package the_0Package_1 = (io.shell.admin.iec61360._2._0._0Package)EPackage.Registry.INSTANCE.getEPackage(io.shell.admin.iec61360._2._0._0Package.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		annotatedRelationshipElementTEClass.getESuperTypes().add(this.getRelationshipElementT());
		basicEventTEClass.getESuperTypes().add(this.getEventAbstractT());
		blobTEClass.getESuperTypes().add(this.getSubmodelElementAbstractT());
		entityTEClass.getESuperTypes().add(this.getSubmodelElementAbstractT());
		eventAbstractTEClass.getESuperTypes().add(this.getSubmodelElementAbstractT());
		fileTEClass.getESuperTypes().add(this.getSubmodelElementAbstractT());
		multiLanguagePropertyTEClass.getESuperTypes().add(this.getSubmodelElementAbstractT());
		operationTEClass.getESuperTypes().add(this.getSubmodelElementAbstractT());
		propertyTEClass.getESuperTypes().add(this.getSubmodelElementAbstractT());
		rangeTEClass.getESuperTypes().add(this.getSubmodelElementAbstractT());
		referenceElementTEClass.getESuperTypes().add(this.getSubmodelElementAbstractT());
		relationshipElementTEClass.getESuperTypes().add(this.getSubmodelElementAbstractT());
		semanticIdTEClass.getESuperTypes().add(this.getReferenceT());
		submodelElementCollectionTEClass.getESuperTypes().add(this.getSubmodelElementAbstractT());

		// Initialize classes, features, and operations; add parameters
		initEClass(aasenvTEClass, AasenvT.class, "AasenvT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAasenvT_AssetAdministrationShells(), this.getAssetAdministrationShellsT(), null, "assetAdministrationShells", null, 0, 1, AasenvT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAasenvT_Assets(), this.getAssetsT(), null, "assets", null, 0, 1, AasenvT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAasenvT_Submodels(), this.getSubmodelsT(), null, "submodels", null, 0, 1, AasenvT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAasenvT_ConceptDescriptions(), this.getConceptDescriptionsT(), null, "conceptDescriptions", null, 0, 1, AasenvT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(administrationTEClass, AdministrationT.class, "AdministrationT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAdministrationT_Version(), theXMLTypePackage.getString(), "version", null, 0, 1, AdministrationT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAdministrationT_Revision(), theXMLTypePackage.getString(), "revision", null, 0, 1, AdministrationT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(annotatedRelationshipElementTEClass, AnnotatedRelationshipElementT.class, "AnnotatedRelationshipElementT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAnnotatedRelationshipElementT_Annotations(), this.getDataElementsT(), null, "annotations", null, 1, 1, AnnotatedRelationshipElementT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assetAdministrationShellsTEClass, AssetAdministrationShellsT.class, "AssetAdministrationShellsT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssetAdministrationShellsT_AssetAdministrationShell(), this.getAssetAdministrationShellT(), null, "assetAdministrationShell", null, 0, -1, AssetAdministrationShellsT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assetAdministrationShellTEClass, AssetAdministrationShellT.class, "AssetAdministrationShellT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssetAdministrationShellT_IdShort(), this.getIdShortT(), null, "idShort", null, 1, 1, AssetAdministrationShellT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssetAdministrationShellT_Category(), theXMLTypePackage.getString(), "category", null, 0, 1, AssetAdministrationShellT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetAdministrationShellT_Description(), this.getLangStringSetT(), null, "description", null, 0, 1, AssetAdministrationShellT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetAdministrationShellT_Parent(), this.getReferenceT(), null, "parent", null, 0, 1, AssetAdministrationShellT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetAdministrationShellT_Identification(), this.getIdentificationT(), null, "identification", null, 1, 1, AssetAdministrationShellT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetAdministrationShellT_Administration(), this.getAdministrationT(), null, "administration", null, 0, 1, AssetAdministrationShellT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetAdministrationShellT_EmbeddedDataSpecification(), this.getEmbeddedDataSpecificationT(), null, "embeddedDataSpecification", null, 0, -1, AssetAdministrationShellT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetAdministrationShellT_DerivedFrom(), this.getReferenceT(), null, "derivedFrom", null, 0, 1, AssetAdministrationShellT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetAdministrationShellT_AssetRef(), this.getReferenceT(), null, "assetRef", null, 1, 1, AssetAdministrationShellT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetAdministrationShellT_SubmodelRefs(), this.getSubmodelRefsT(), null, "submodelRefs", null, 0, 1, AssetAdministrationShellT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetAdministrationShellT_Views(), this.getViewsT(), null, "views", null, 0, 1, AssetAdministrationShellT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetAdministrationShellT_ConceptDictionaries(), this.getConceptDictionariesT(), null, "conceptDictionaries", null, 0, 1, AssetAdministrationShellT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetAdministrationShellT_Security(), the_0Package_2.getSecurityT(), null, "security", null, 0, 1, AssetAdministrationShellT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assetsTEClass, AssetsT.class, "AssetsT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssetsT_Asset(), this.getAssetT(), null, "asset", null, 0, -1, AssetsT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assetTEClass, AssetT.class, "AssetT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssetT_IdShort(), this.getIdShortT(), null, "idShort", null, 1, 1, AssetT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssetT_Category(), theXMLTypePackage.getString(), "category", null, 0, 1, AssetT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetT_Description(), this.getLangStringSetT(), null, "description", null, 0, 1, AssetT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetT_Parent(), this.getReferenceT(), null, "parent", null, 0, 1, AssetT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetT_Identification(), this.getIdentificationT(), null, "identification", null, 1, 1, AssetT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetT_Administration(), this.getAdministrationT(), null, "administration", null, 0, 1, AssetT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetT_EmbeddedDataSpecification(), this.getEmbeddedDataSpecificationT(), null, "embeddedDataSpecification", null, 0, -1, AssetT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetT_AssetIdentificationModelRef(), this.getReferenceT(), null, "assetIdentificationModelRef", null, 0, 1, AssetT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssetT_BillOfMaterialRef(), this.getReferenceT(), null, "billOfMaterialRef", null, 0, 1, AssetT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssetT_Kind(), this.getAssetKindT(), "kind", null, 0, 1, AssetT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(basicEventTEClass, BasicEventT.class, "BasicEventT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBasicEventT_Observed(), this.getReferenceT(), null, "observed", null, 1, 1, BasicEventT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(blobTEClass, BlobT.class, "BlobT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBlobT_Value(), this.getBlobTypeT(), null, "value", null, 0, 1, BlobT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBlobT_MimeType(), theXMLTypePackage.getString(), "mimeType", null, 1, 1, BlobT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(blobTypeTEClass, BlobTypeT.class, "BlobTypeT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBlobTypeT_Value(), theXMLTypePackage.getBase64Binary(), "value", null, 0, 1, BlobTypeT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(conceptDescriptionRefsTEClass, ConceptDescriptionRefsT.class, "ConceptDescriptionRefsT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConceptDescriptionRefsT_ConceptDescriptionRef(), this.getReferenceT(), null, "conceptDescriptionRef", null, 0, -1, ConceptDescriptionRefsT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(conceptDescriptionsTEClass, ConceptDescriptionsT.class, "ConceptDescriptionsT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConceptDescriptionsT_ConceptDescription(), this.getConceptDescriptionT(), null, "conceptDescription", null, 0, -1, ConceptDescriptionsT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(conceptDescriptionTEClass, ConceptDescriptionT.class, "ConceptDescriptionT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConceptDescriptionT_IdShort(), this.getIdShortT(), null, "idShort", null, 1, 1, ConceptDescriptionT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getConceptDescriptionT_Category(), theXMLTypePackage.getString(), "category", null, 0, 1, ConceptDescriptionT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConceptDescriptionT_Description(), this.getLangStringSetT(), null, "description", null, 0, 1, ConceptDescriptionT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConceptDescriptionT_Parent(), this.getReferenceT(), null, "parent", null, 0, 1, ConceptDescriptionT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConceptDescriptionT_Identification(), this.getIdentificationT(), null, "identification", null, 1, 1, ConceptDescriptionT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConceptDescriptionT_Administration(), this.getAdministrationT(), null, "administration", null, 0, 1, ConceptDescriptionT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConceptDescriptionT_EmbeddedDataSpecification(), this.getEmbeddedDataSpecificationT(), null, "embeddedDataSpecification", null, 0, -1, ConceptDescriptionT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConceptDescriptionT_IsCaseOf(), this.getReferenceT(), null, "isCaseOf", null, 0, -1, ConceptDescriptionT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(conceptDictionariesTEClass, ConceptDictionariesT.class, "ConceptDictionariesT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConceptDictionariesT_ConceptDictionary(), this.getConceptDictionaryT(), null, "conceptDictionary", null, 0, -1, ConceptDictionariesT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(conceptDictionaryTEClass, ConceptDictionaryT.class, "ConceptDictionaryT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConceptDictionaryT_IdShort(), this.getIdShortT(), null, "idShort", null, 1, 1, ConceptDictionaryT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getConceptDictionaryT_Category(), theXMLTypePackage.getString(), "category", null, 0, 1, ConceptDictionaryT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConceptDictionaryT_Description(), this.getLangStringSetT(), null, "description", null, 0, 1, ConceptDictionaryT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConceptDictionaryT_Parent(), this.getReferenceT(), null, "parent", null, 0, 1, ConceptDictionaryT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConceptDictionaryT_ConceptDescriptionRefs(), this.getConceptDescriptionRefsT(), null, "conceptDescriptionRefs", null, 1, 1, ConceptDictionaryT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(constraintTEClass, ConstraintT.class, "ConstraintT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConstraintT_Formula(), this.getFormulaT(), null, "formula", null, 0, 1, ConstraintT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConstraintT_Qualifier(), this.getQualifierT(), null, "qualifier", null, 0, 1, ConstraintT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(containedElementsTEClass, ContainedElementsT.class, "ContainedElementsT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getContainedElementsT_ContainedElementRef(), this.getReferenceT(), null, "containedElementRef", null, 0, -1, ContainedElementsT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataElementAbstractTEClass, DataElementAbstractT.class, "DataElementAbstractT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataElementAbstractT_MultiLanguageProperty(), this.getMultiLanguagePropertyT(), null, "multiLanguageProperty", null, 0, 1, DataElementAbstractT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataElementAbstractT_Property(), this.getPropertyT(), null, "property", null, 0, 1, DataElementAbstractT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataElementAbstractT_Range(), this.getRangeT(), null, "range", null, 0, 1, DataElementAbstractT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataElementAbstractT_Blob(), this.getBlobT(), null, "blob", null, 0, 1, DataElementAbstractT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataElementAbstractT_File(), this.getFileT(), null, "file", null, 0, 1, DataElementAbstractT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataElementAbstractT_ReferenceElement(), this.getReferenceElementT(), null, "referenceElement", null, 0, 1, DataElementAbstractT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataElementsTEClass, DataElementsT.class, "DataElementsT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataElementsT_DataElement(), this.getDataElementAbstractT(), null, "dataElement", null, 0, -1, DataElementsT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataSpecificationContentTEClass, DataSpecificationContentT.class, "DataSpecificationContentT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataSpecificationContentT_DataSpecificationIEC61360(), the_0Package_1.getDataSpecificationIEC61630T(), null, "dataSpecificationIEC61360", null, 0, 1, DataSpecificationContentT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataTypeDefTEClass, DataTypeDefT.class, "DataTypeDefT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDataTypeDefT_Value(), theXMLTypePackage.getString(), "value", null, 0, 1, DataTypeDefT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(documentRootEClass, DocumentRoot.class, "DocumentRoot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDocumentRoot_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, null, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_XMLNSPrefixMap(), ecorePackage.getEStringToStringMapEntry(), null, "xMLNSPrefixMap", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_XSISchemaLocation(), ecorePackage.getEStringToStringMapEntry(), null, "xSISchemaLocation", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_Aasenv(), this.getAasenvT(), null, "aasenv", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_Key(), this.getKeyT(), null, "key", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(embeddedDataSpecificationTEClass, EmbeddedDataSpecificationT.class, "EmbeddedDataSpecificationT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEmbeddedDataSpecificationT_DataSpecificationContent(), this.getDataSpecificationContentT(), null, "dataSpecificationContent", null, 0, 1, EmbeddedDataSpecificationT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEmbeddedDataSpecificationT_DataSpecification(), this.getReferenceT(), null, "dataSpecification", null, 0, 1, EmbeddedDataSpecificationT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(entityTEClass, EntityT.class, "EntityT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEntityT_Statements(), this.getSubmodelElementsT(), null, "statements", null, 1, 1, EntityT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEntityT_EntityType(), this.getEntityTypeType(), "entityType", null, 1, 1, EntityT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEntityT_AssetRef(), this.getReferenceT(), null, "assetRef", null, 0, 1, EntityT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eventAbstractTEClass, EventAbstractT.class, "EventAbstractT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fileTEClass, FileT.class, "FileT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFileT_MimeType(), theXMLTypePackage.getString(), "mimeType", null, 1, 1, FileT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFileT_Value(), this.getPathTypeT(), null, "value", null, 0, 1, FileT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(formulaTEClass, FormulaT.class, "FormulaT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFormulaT_DependsOnRefs(), this.getReferencesT(), null, "dependsOnRefs", null, 0, 1, FormulaT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(identificationTEClass, IdentificationT.class, "IdentificationT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIdentificationT_Value(), theXMLTypePackage.getString(), "value", null, 0, 1, IdentificationT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIdentificationT_IdType(), this.getIdTypeType(), "idType", null, 0, 1, IdentificationT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(identifierTEClass, IdentifierT.class, "IdentifierT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIdentifierT_Id(), theXMLTypePackage.getString(), "id", null, 1, 1, IdentifierT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIdentifierT_IdType(), this.getIdentifierTypeT(), "idType", null, 1, 1, IdentifierT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(idPropertyDefinitionTEClass, IdPropertyDefinitionT.class, "IdPropertyDefinitionT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIdPropertyDefinitionT_Value(), theXMLTypePackage.getString(), "value", null, 0, 1, IdPropertyDefinitionT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIdPropertyDefinitionT_IdType(), theXMLTypePackage.getString(), "idType", null, 0, 1, IdPropertyDefinitionT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(idShortTEClass, IdShortT.class, "IdShortT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIdShortT_Value(), theXMLTypePackage.getString(), "value", null, 0, 1, IdShortT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(keysTEClass, KeysT.class, "KeysT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getKeysT_Key(), this.getKeyT(), null, "key", null, 0, -1, KeysT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(keyTEClass, KeyT.class, "KeyT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getKeyT_Value(), theXMLTypePackage.getString(), "value", null, 0, 1, KeyT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getKeyT_IdType(), this.getIdTypeType1(), "idType", null, 0, 1, KeyT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getKeyT_Local(), theXMLTypePackage.getBoolean(), "local", null, 0, 1, KeyT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getKeyT_Type(), this.getTypeType(), "type", null, 0, 1, KeyT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(langStringSetTEClass, LangStringSetT.class, "LangStringSetT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLangStringSetT_LangString(), this.getLangStringT(), null, "langString", null, 1, -1, LangStringSetT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(langStringTEClass, LangStringT.class, "LangStringT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLangStringT_Value(), theXMLTypePackage.getString(), "value", null, 0, 1, LangStringT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLangStringT_Lang(), theXMLTypePackage.getString(), "lang", null, 0, 1, LangStringT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(multiLanguagePropertyTEClass, MultiLanguagePropertyT.class, "MultiLanguagePropertyT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMultiLanguagePropertyT_ValueId(), this.getReferenceT(), null, "valueId", null, 0, 1, MultiLanguagePropertyT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMultiLanguagePropertyT_Value(), this.getLangStringSetT(), null, "value", null, 0, 1, MultiLanguagePropertyT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operationTEClass, OperationT.class, "OperationT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperationT_InputVariable(), this.getOperationVariableT(), null, "inputVariable", null, 0, -1, OperationT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationT_OutputVariable(), this.getOperationVariableT(), null, "outputVariable", null, 0, -1, OperationT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationT_InoutputVariable(), this.getOperationVariableT(), null, "inoutputVariable", null, 0, -1, OperationT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operationVariableTEClass, OperationVariableT.class, "OperationVariableT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperationVariableT_Value(), this.getSubmodelElementT(), null, "value", null, 1, 1, OperationVariableT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pathTypeTEClass, PathTypeT.class, "PathTypeT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPathTypeT_Value(), theXMLTypePackage.getString(), "value", null, 0, 1, PathTypeT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(propertyTEClass, PropertyT.class, "PropertyT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPropertyT_ValueType(), this.getDataTypeDefT(), null, "valueType", null, 1, 1, PropertyT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPropertyT_Value(), this.getValueDataTypeT(), null, "value", null, 0, 1, PropertyT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPropertyT_ValueId(), this.getReferenceT(), null, "valueId", null, 0, 1, PropertyT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(qualifierTEClass, QualifierT.class, "QualifierT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getQualifierT_SemanticId(), this.getSemanticIdT(), null, "semanticId", null, 0, 1, QualifierT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getQualifierT_Type(), this.getQualifierTypeT(), null, "type", null, 1, 1, QualifierT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getQualifierT_ValueType(), this.getDataTypeDefT(), null, "valueType", null, 1, 1, QualifierT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getQualifierT_ValueId(), this.getReferenceT(), null, "valueId", null, 0, 1, QualifierT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getQualifierT_Value(), this.getValueDataTypeT(), null, "value", null, 0, 1, QualifierT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(qualifierTypeTEClass, QualifierTypeT.class, "QualifierTypeT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getQualifierTypeT_Value(), theXMLTypePackage.getString(), "value", null, 0, 1, QualifierTypeT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(rangeTEClass, RangeT.class, "RangeT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRangeT_ValueType(), this.getDataTypeDefT(), null, "valueType", null, 1, 1, RangeT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRangeT_Min(), this.getValueDataTypeT(), null, "min", null, 0, 1, RangeT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRangeT_Max(), this.getValueDataTypeT(), null, "max", null, 0, 1, RangeT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(referenceElementTEClass, ReferenceElementT.class, "ReferenceElementT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReferenceElementT_Value(), this.getReferenceT(), null, "value", null, 0, 1, ReferenceElementT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(referencesTEClass, ReferencesT.class, "ReferencesT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReferencesT_Reference(), this.getReferenceT(), null, "reference", null, 0, -1, ReferencesT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(referenceTEClass, ReferenceT.class, "ReferenceT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReferenceT_Keys(), this.getKeysT(), null, "keys", null, 1, 1, ReferenceT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(relationshipElementTEClass, RelationshipElementT.class, "RelationshipElementT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRelationshipElementT_First(), this.getReferenceT(), null, "first", null, 1, 1, RelationshipElementT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRelationshipElementT_Second(), this.getReferenceT(), null, "second", null, 1, 1, RelationshipElementT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(semanticIdTEClass, SemanticIdT.class, "SemanticIdT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(submodelElementAbstractTEClass, SubmodelElementAbstractT.class, "SubmodelElementAbstractT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSubmodelElementAbstractT_IdShort(), this.getIdShortT(), null, "idShort", null, 1, 1, SubmodelElementAbstractT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSubmodelElementAbstractT_Category(), theXMLTypePackage.getString(), "category", null, 0, 1, SubmodelElementAbstractT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelElementAbstractT_Description(), this.getLangStringSetT(), null, "description", null, 0, 1, SubmodelElementAbstractT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelElementAbstractT_Parent(), this.getReferenceT(), null, "parent", null, 0, 1, SubmodelElementAbstractT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSubmodelElementAbstractT_Kind(), this.getModelingKindT(), "kind", null, 0, 1, SubmodelElementAbstractT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelElementAbstractT_SemanticId(), this.getSemanticIdT(), null, "semanticId", null, 0, 1, SubmodelElementAbstractT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelElementAbstractT_Qualifier(), this.getConstraintT(), null, "qualifier", null, 0, -1, SubmodelElementAbstractT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelElementAbstractT_EmbeddedDataSpecification(), this.getEmbeddedDataSpecificationT(), null, "embeddedDataSpecification", null, 0, -1, SubmodelElementAbstractT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(submodelElementCollectionTEClass, SubmodelElementCollectionT.class, "SubmodelElementCollectionT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSubmodelElementCollectionT_Value(), this.getSubmodelElementsT(), null, "value", null, 1, 1, SubmodelElementCollectionT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSubmodelElementCollectionT_Ordered(), theXMLTypePackage.getBoolean(), "ordered", null, 1, 1, SubmodelElementCollectionT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSubmodelElementCollectionT_AllowDuplicates(), theXMLTypePackage.getBoolean(), "allowDuplicates", null, 1, 1, SubmodelElementCollectionT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(submodelElementsTEClass, SubmodelElementsT.class, "SubmodelElementsT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSubmodelElementsT_SubmodelElement(), this.getSubmodelElementT(), null, "submodelElement", null, 0, -1, SubmodelElementsT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(submodelElementTEClass, SubmodelElementT.class, "SubmodelElementT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSubmodelElementT_MultiLanguageProperty(), this.getMultiLanguagePropertyT(), null, "multiLanguageProperty", null, 0, 1, SubmodelElementT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelElementT_Property(), this.getPropertyT(), null, "property", null, 0, 1, SubmodelElementT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelElementT_Range(), this.getRangeT(), null, "range", null, 0, 1, SubmodelElementT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelElementT_Blob(), this.getBlobT(), null, "blob", null, 0, 1, SubmodelElementT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelElementT_File(), this.getFileT(), null, "file", null, 0, 1, SubmodelElementT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelElementT_ReferenceElement(), this.getReferenceElementT(), null, "referenceElement", null, 0, 1, SubmodelElementT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelElementT_AnnotatedRelationshipElement(), this.getAnnotatedRelationshipElementT(), null, "annotatedRelationshipElement", null, 0, 1, SubmodelElementT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelElementT_BasicEvent(), this.getBasicEventT(), null, "basicEvent", null, 0, 1, SubmodelElementT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelElementT_Capability(), this.getSubmodelElementAbstractT(), null, "capability", null, 0, 1, SubmodelElementT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelElementT_Entity(), this.getEntityT(), null, "entity", null, 0, 1, SubmodelElementT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelElementT_Operation(), this.getOperationT(), null, "operation", null, 0, 1, SubmodelElementT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelElementT_RelationshipElement(), this.getRelationshipElementT(), null, "relationshipElement", null, 0, 1, SubmodelElementT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelElementT_SubmodelElementCollection(), this.getSubmodelElementCollectionT(), null, "submodelElementCollection", null, 0, 1, SubmodelElementT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(submodelRefsTEClass, SubmodelRefsT.class, "SubmodelRefsT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSubmodelRefsT_SubmodelRef(), this.getReferenceT(), null, "submodelRef", null, 0, -1, SubmodelRefsT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(submodelsTEClass, SubmodelsT.class, "SubmodelsT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSubmodelsT_Submodel(), this.getSubmodelT(), null, "submodel", null, 0, -1, SubmodelsT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(submodelTEClass, SubmodelT.class, "SubmodelT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSubmodelT_IdShort(), this.getIdShortT(), null, "idShort", null, 1, 1, SubmodelT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSubmodelT_Category(), theXMLTypePackage.getString(), "category", null, 0, 1, SubmodelT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelT_Description(), this.getLangStringSetT(), null, "description", null, 0, 1, SubmodelT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelT_Parent(), this.getReferenceT(), null, "parent", null, 0, 1, SubmodelT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelT_Identification(), this.getIdentificationT(), null, "identification", null, 1, 1, SubmodelT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelT_Administration(), this.getAdministrationT(), null, "administration", null, 0, 1, SubmodelT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSubmodelT_Kind(), this.getModelingKindT(), "kind", null, 0, 1, SubmodelT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelT_SemanticId(), this.getSemanticIdT(), null, "semanticId", null, 0, 1, SubmodelT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelT_Qualifier(), this.getConstraintT(), null, "qualifier", null, 0, -1, SubmodelT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelT_EmbeddedDataSpecification(), this.getEmbeddedDataSpecificationT(), null, "embeddedDataSpecification", null, 0, -1, SubmodelT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubmodelT_SubmodelElements(), this.getSubmodelElementsT(), null, "submodelElements", null, 1, 1, SubmodelT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(valueDataTypeTEClass, ValueDataTypeT.class, "ValueDataTypeT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getValueDataTypeT_Value(), theXMLTypePackage.getAnySimpleType(), "value", null, 0, 1, ValueDataTypeT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(viewsTEClass, ViewsT.class, "ViewsT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getViewsT_View(), this.getViewT(), null, "view", null, 0, -1, ViewsT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(viewTEClass, ViewT.class, "ViewT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getViewT_IdShort(), this.getIdShortT(), null, "idShort", null, 1, 1, ViewT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getViewT_Category(), theXMLTypePackage.getString(), "category", null, 0, 1, ViewT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getViewT_Description(), this.getLangStringSetT(), null, "description", null, 0, 1, ViewT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getViewT_Parent(), this.getReferenceT(), null, "parent", null, 0, 1, ViewT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getViewT_SemanticId(), this.getSemanticIdT(), null, "semanticId", null, 0, 1, ViewT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getViewT_EmbeddedDataSpecification(), this.getEmbeddedDataSpecificationT(), null, "embeddedDataSpecification", null, 0, -1, ViewT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getViewT_ContainedElements(), this.getContainedElementsT(), null, "containedElements", null, 1, 1, ViewT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(assetKindTEEnum, AssetKindT.class, "AssetKindT");
		addEEnumLiteral(assetKindTEEnum, AssetKindT.INSTANCE);
		addEEnumLiteral(assetKindTEEnum, AssetKindT.TEMPLATE);

		initEEnum(entityTypeTypeEEnum, EntityTypeType.class, "EntityTypeType");
		addEEnumLiteral(entityTypeTypeEEnum, EntityTypeType.CO_MANAGED_ENTITY);
		addEEnumLiteral(entityTypeTypeEEnum, EntityTypeType.SELF_MANAGED_ENTITY);

		initEEnum(identifierTypeTEEnum, IdentifierTypeT.class, "IdentifierTypeT");
		addEEnumLiteral(identifierTypeTEEnum, IdentifierTypeT.CUSTOM);
		addEEnumLiteral(identifierTypeTEEnum, IdentifierTypeT.IRDI);
		addEEnumLiteral(identifierTypeTEEnum, IdentifierTypeT.IRI);

		initEEnum(idTypeTypeEEnum, IdTypeType.class, "IdTypeType");
		addEEnumLiteral(idTypeTypeEEnum, IdTypeType.CUSTOM);
		addEEnumLiteral(idTypeTypeEEnum, IdTypeType.IRDI);
		addEEnumLiteral(idTypeTypeEEnum, IdTypeType.IRI);

		initEEnum(idTypeType1EEnum, IdTypeType1.class, "IdTypeType1");
		addEEnumLiteral(idTypeType1EEnum, IdTypeType1.CUSTOM);
		addEEnumLiteral(idTypeType1EEnum, IdTypeType1.FRAGMENT_ID);
		addEEnumLiteral(idTypeType1EEnum, IdTypeType1.ID_SHORT);
		addEEnumLiteral(idTypeType1EEnum, IdTypeType1.IRDI);
		addEEnumLiteral(idTypeType1EEnum, IdTypeType1.IRI);

		initEEnum(modelingKindTEEnum, ModelingKindT.class, "ModelingKindT");
		addEEnumLiteral(modelingKindTEEnum, ModelingKindT.INSTANCE);
		addEEnumLiteral(modelingKindTEEnum, ModelingKindT.TEMPLATE);

		initEEnum(typeTypeEEnum, TypeType.class, "TypeType");
		addEEnumLiteral(typeTypeEEnum, TypeType.ACCESS_PERMISSION_RULE);
		addEEnumLiteral(typeTypeEEnum, TypeType.ANNOTATED_RELATIONSHIP_ELEMENT);
		addEEnumLiteral(typeTypeEEnum, TypeType.ASSET);
		addEEnumLiteral(typeTypeEEnum, TypeType.ASSET_ADMINISTRATION_SHELL);
		addEEnumLiteral(typeTypeEEnum, TypeType.BASIC_EVENT);
		addEEnumLiteral(typeTypeEEnum, TypeType.BLOB);
		addEEnumLiteral(typeTypeEEnum, TypeType.CAPABILITY);
		addEEnumLiteral(typeTypeEEnum, TypeType.CONCEPT_DESCRIPTION);
		addEEnumLiteral(typeTypeEEnum, TypeType.CONCEPT_DICTIONARY);
		addEEnumLiteral(typeTypeEEnum, TypeType.DATA_ELEMENT);
		addEEnumLiteral(typeTypeEEnum, TypeType.ENTITY);
		addEEnumLiteral(typeTypeEEnum, TypeType.EVENT);
		addEEnumLiteral(typeTypeEEnum, TypeType.FILE);
		addEEnumLiteral(typeTypeEEnum, TypeType.FRAGMENT_REFERENCE);
		addEEnumLiteral(typeTypeEEnum, TypeType.GLOBAL_REFERENCE);
		addEEnumLiteral(typeTypeEEnum, TypeType.MULTI_LANGUAGE_PROPERTY);
		addEEnumLiteral(typeTypeEEnum, TypeType.OPERATION);
		addEEnumLiteral(typeTypeEEnum, TypeType.PROPERTY);
		addEEnumLiteral(typeTypeEEnum, TypeType.RANGE);
		addEEnumLiteral(typeTypeEEnum, TypeType.REFERENCE_ELEMENT);
		addEEnumLiteral(typeTypeEEnum, TypeType.RELATIONSHIP_ELEMENT);
		addEEnumLiteral(typeTypeEEnum, TypeType.SUBMODEL);
		addEEnumLiteral(typeTypeEEnum, TypeType.SUBMODEL_ELEMENT);
		addEEnumLiteral(typeTypeEEnum, TypeType.SUBMODEL_ELEMENT_COLLECTION);
		addEEnumLiteral(typeTypeEEnum, TypeType.VIEW);

		// Initialize data types
		initEDataType(assetKindTObjectEDataType, AssetKindT.class, "AssetKindTObject", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(entityTypeTEDataType, String.class, "EntityTypeT", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(entityTypeTypeObjectEDataType, EntityTypeType.class, "EntityTypeTypeObject", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(identifierTypeTObjectEDataType, IdentifierTypeT.class, "IdentifierTypeTObject", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(idTypeTypeObjectEDataType, IdTypeType.class, "IdTypeTypeObject", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(idTypeTypeObject1EDataType, IdTypeType1.class, "IdTypeTypeObject1", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(modelingKindTObjectEDataType, ModelingKindT.class, "ModelingKindTObject", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(typeTypeObjectEDataType, TypeType.class, "TypeTypeObject", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http:///org/eclipse/emf/ecore/util/ExtendedMetaData
		createExtendedMetaDataAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createExtendedMetaDataAnnotations() {
		String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";
		addAnnotation
		  (aasenvTEClass,
		   source,
		   new String[] {
			   "name", "aasenv_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getAasenvT_AssetAdministrationShells(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "assetAdministrationShells",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAasenvT_Assets(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "assets",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAasenvT_Submodels(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "submodels",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAasenvT_ConceptDescriptions(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "conceptDescriptions",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (administrationTEClass,
		   source,
		   new String[] {
			   "name", "administration_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getAdministrationT_Version(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "version",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAdministrationT_Revision(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "revision",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (annotatedRelationshipElementTEClass,
		   source,
		   new String[] {
			   "name", "annotatedRelationshipElement_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getAnnotatedRelationshipElementT_Annotations(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "annotations",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (assetAdministrationShellsTEClass,
		   source,
		   new String[] {
			   "name", "assetAdministrationShells_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getAssetAdministrationShellsT_AssetAdministrationShell(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "assetAdministrationShell",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (assetAdministrationShellTEClass,
		   source,
		   new String[] {
			   "name", "assetAdministrationShell_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getAssetAdministrationShellT_IdShort(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "idShort",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAssetAdministrationShellT_Category(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "category",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAssetAdministrationShellT_Description(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "description",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAssetAdministrationShellT_Parent(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "parent",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAssetAdministrationShellT_Identification(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "identification",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAssetAdministrationShellT_Administration(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "administration",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAssetAdministrationShellT_EmbeddedDataSpecification(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "embeddedDataSpecification",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAssetAdministrationShellT_DerivedFrom(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "derivedFrom",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAssetAdministrationShellT_AssetRef(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "assetRef",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAssetAdministrationShellT_SubmodelRefs(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "submodelRefs",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAssetAdministrationShellT_Views(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "views",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAssetAdministrationShellT_ConceptDictionaries(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "conceptDictionaries",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAssetAdministrationShellT_Security(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "security",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (assetKindTEEnum,
		   source,
		   new String[] {
			   "name", "assetKind_t"
		   });
		addAnnotation
		  (assetKindTObjectEDataType,
		   source,
		   new String[] {
			   "name", "assetKind_t:Object",
			   "baseType", "assetKind_t"
		   });
		addAnnotation
		  (assetsTEClass,
		   source,
		   new String[] {
			   "name", "assets_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getAssetsT_Asset(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "asset",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (assetTEClass,
		   source,
		   new String[] {
			   "name", "asset_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getAssetT_IdShort(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "idShort",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAssetT_Category(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "category",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAssetT_Description(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "description",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAssetT_Parent(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "parent",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAssetT_Identification(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "identification",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAssetT_Administration(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "administration",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAssetT_EmbeddedDataSpecification(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "embeddedDataSpecification",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAssetT_AssetIdentificationModelRef(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "assetIdentificationModelRef",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAssetT_BillOfMaterialRef(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "billOfMaterialRef",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAssetT_Kind(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "kind",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (basicEventTEClass,
		   source,
		   new String[] {
			   "name", "basicEvent_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getBasicEventT_Observed(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "observed",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (blobTEClass,
		   source,
		   new String[] {
			   "name", "blob_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getBlobT_Value(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "value",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getBlobT_MimeType(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "mimeType",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (blobTypeTEClass,
		   source,
		   new String[] {
			   "name", "blobType_t",
			   "kind", "simple"
		   });
		addAnnotation
		  (getBlobTypeT_Value(),
		   source,
		   new String[] {
			   "name", ":0",
			   "kind", "simple"
		   });
		addAnnotation
		  (conceptDescriptionRefsTEClass,
		   source,
		   new String[] {
			   "name", "conceptDescriptionRefs_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getConceptDescriptionRefsT_ConceptDescriptionRef(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "conceptDescriptionRef",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (conceptDescriptionsTEClass,
		   source,
		   new String[] {
			   "name", "conceptDescriptions_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getConceptDescriptionsT_ConceptDescription(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "conceptDescription",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (conceptDescriptionTEClass,
		   source,
		   new String[] {
			   "name", "conceptDescription_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getConceptDescriptionT_IdShort(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "idShort",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getConceptDescriptionT_Category(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "category",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getConceptDescriptionT_Description(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "description",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getConceptDescriptionT_Parent(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "parent",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getConceptDescriptionT_Identification(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "identification",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getConceptDescriptionT_Administration(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "administration",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getConceptDescriptionT_EmbeddedDataSpecification(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "embeddedDataSpecification",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getConceptDescriptionT_IsCaseOf(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "isCaseOf",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (conceptDictionariesTEClass,
		   source,
		   new String[] {
			   "name", "conceptDictionaries_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getConceptDictionariesT_ConceptDictionary(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "conceptDictionary",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (conceptDictionaryTEClass,
		   source,
		   new String[] {
			   "name", "conceptDictionary_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getConceptDictionaryT_IdShort(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "idShort",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getConceptDictionaryT_Category(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "category",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getConceptDictionaryT_Description(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "description",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getConceptDictionaryT_Parent(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "parent",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getConceptDictionaryT_ConceptDescriptionRefs(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "conceptDescriptionRefs",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (constraintTEClass,
		   source,
		   new String[] {
			   "name", "constraint_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getConstraintT_Formula(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "formula",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getConstraintT_Qualifier(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "qualifier",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (containedElementsTEClass,
		   source,
		   new String[] {
			   "name", "containedElements_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getContainedElementsT_ContainedElementRef(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "containedElementRef",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (dataElementAbstractTEClass,
		   source,
		   new String[] {
			   "name", "dataElementAbstract_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getDataElementAbstractT_MultiLanguageProperty(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "multiLanguageProperty",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDataElementAbstractT_Property(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "property",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDataElementAbstractT_Range(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "range",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDataElementAbstractT_Blob(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "blob",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDataElementAbstractT_File(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "file",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDataElementAbstractT_ReferenceElement(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "referenceElement",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (dataElementsTEClass,
		   source,
		   new String[] {
			   "name", "dataElements_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getDataElementsT_DataElement(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "dataElement",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (dataSpecificationContentTEClass,
		   source,
		   new String[] {
			   "name", "dataSpecificationContent_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getDataSpecificationContentT_DataSpecificationIEC61360(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "dataSpecificationIEC61360",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (dataTypeDefTEClass,
		   source,
		   new String[] {
			   "name", "dataTypeDef_t",
			   "kind", "simple"
		   });
		addAnnotation
		  (getDataTypeDefT_Value(),
		   source,
		   new String[] {
			   "name", ":0",
			   "kind", "simple"
		   });
		addAnnotation
		  (documentRootEClass,
		   source,
		   new String[] {
			   "name", "",
			   "kind", "mixed"
		   });
		addAnnotation
		  (getDocumentRoot_Mixed(),
		   source,
		   new String[] {
			   "kind", "elementWildcard",
			   "name", ":mixed"
		   });
		addAnnotation
		  (getDocumentRoot_XMLNSPrefixMap(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "xmlns:prefix"
		   });
		addAnnotation
		  (getDocumentRoot_XSISchemaLocation(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "xsi:schemaLocation"
		   });
		addAnnotation
		  (getDocumentRoot_Aasenv(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "aasenv",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getDocumentRoot_Key(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "key",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (embeddedDataSpecificationTEClass,
		   source,
		   new String[] {
			   "name", "embeddedDataSpecification_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getEmbeddedDataSpecificationT_DataSpecificationContent(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "dataSpecificationContent",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getEmbeddedDataSpecificationT_DataSpecification(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "dataSpecification",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (entityTEClass,
		   source,
		   new String[] {
			   "name", "entity_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getEntityT_Statements(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "statements",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getEntityT_EntityType(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "entityType",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getEntityT_AssetRef(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "assetRef",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (entityTypeTEDataType,
		   source,
		   new String[] {
			   "name", "entityType_t",
			   "baseType", "http://www.eclipse.org/emf/2003/XMLType#string"
		   });
		addAnnotation
		  (entityTypeTypeEEnum,
		   source,
		   new String[] {
			   "name", "entityType_._type"
		   });
		addAnnotation
		  (entityTypeTypeObjectEDataType,
		   source,
		   new String[] {
			   "name", "entityType_._type:Object",
			   "baseType", "entityType_._type"
		   });
		addAnnotation
		  (eventAbstractTEClass,
		   source,
		   new String[] {
			   "name", "eventAbstract_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (fileTEClass,
		   source,
		   new String[] {
			   "name", "file_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getFileT_MimeType(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "mimeType",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getFileT_Value(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "value",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (formulaTEClass,
		   source,
		   new String[] {
			   "name", "formula_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getFormulaT_DependsOnRefs(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "dependsOnRefs",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (identificationTEClass,
		   source,
		   new String[] {
			   "name", "identification_t",
			   "kind", "simple"
		   });
		addAnnotation
		  (getIdentificationT_Value(),
		   source,
		   new String[] {
			   "name", ":0",
			   "kind", "simple"
		   });
		addAnnotation
		  (getIdentificationT_IdType(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "idType"
		   });
		addAnnotation
		  (identifierTEClass,
		   source,
		   new String[] {
			   "name", "identifier_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getIdentifierT_Id(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "id",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getIdentifierT_IdType(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "idType",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (identifierTypeTEEnum,
		   source,
		   new String[] {
			   "name", "identifierType_t"
		   });
		addAnnotation
		  (identifierTypeTObjectEDataType,
		   source,
		   new String[] {
			   "name", "identifierType_t:Object",
			   "baseType", "identifierType_t"
		   });
		addAnnotation
		  (idPropertyDefinitionTEClass,
		   source,
		   new String[] {
			   "name", "idPropertyDefinition_t",
			   "kind", "simple"
		   });
		addAnnotation
		  (getIdPropertyDefinitionT_Value(),
		   source,
		   new String[] {
			   "name", ":0",
			   "kind", "simple"
		   });
		addAnnotation
		  (getIdPropertyDefinitionT_IdType(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "idType"
		   });
		addAnnotation
		  (idShortTEClass,
		   source,
		   new String[] {
			   "name", "idShort_t",
			   "kind", "simple"
		   });
		addAnnotation
		  (getIdShortT_Value(),
		   source,
		   new String[] {
			   "name", ":0",
			   "kind", "simple"
		   });
		addAnnotation
		  (idTypeTypeEEnum,
		   source,
		   new String[] {
			   "name", "idType_._type"
		   });
		addAnnotation
		  (idTypeType1EEnum,
		   source,
		   new String[] {
			   "name", "idType_._1_._type"
		   });
		addAnnotation
		  (idTypeTypeObjectEDataType,
		   source,
		   new String[] {
			   "name", "idType_._type:Object",
			   "baseType", "idType_._type"
		   });
		addAnnotation
		  (idTypeTypeObject1EDataType,
		   source,
		   new String[] {
			   "name", "idType_._1_._type:Object",
			   "baseType", "idType_._1_._type"
		   });
		addAnnotation
		  (keysTEClass,
		   source,
		   new String[] {
			   "name", "keys_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getKeysT_Key(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "key",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (keyTEClass,
		   source,
		   new String[] {
			   "name", "key_t",
			   "kind", "simple"
		   });
		addAnnotation
		  (getKeyT_Value(),
		   source,
		   new String[] {
			   "name", ":0",
			   "kind", "simple"
		   });
		addAnnotation
		  (getKeyT_IdType(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "idType"
		   });
		addAnnotation
		  (getKeyT_Local(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "local"
		   });
		addAnnotation
		  (getKeyT_Type(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "type"
		   });
		addAnnotation
		  (langStringSetTEClass,
		   source,
		   new String[] {
			   "name", "langStringSet_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getLangStringSetT_LangString(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "langString",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (langStringTEClass,
		   source,
		   new String[] {
			   "name", "langString_t",
			   "kind", "simple"
		   });
		addAnnotation
		  (getLangStringT_Value(),
		   source,
		   new String[] {
			   "name", ":0",
			   "kind", "simple"
		   });
		addAnnotation
		  (getLangStringT_Lang(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "lang"
		   });
		addAnnotation
		  (modelingKindTEEnum,
		   source,
		   new String[] {
			   "name", "modelingKind_t"
		   });
		addAnnotation
		  (modelingKindTObjectEDataType,
		   source,
		   new String[] {
			   "name", "modelingKind_t:Object",
			   "baseType", "modelingKind_t"
		   });
		addAnnotation
		  (multiLanguagePropertyTEClass,
		   source,
		   new String[] {
			   "name", "multiLanguageProperty_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getMultiLanguagePropertyT_ValueId(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "valueId",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getMultiLanguagePropertyT_Value(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "value",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (operationTEClass,
		   source,
		   new String[] {
			   "name", "operation_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getOperationT_InputVariable(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "inputVariable",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getOperationT_OutputVariable(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "outputVariable",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getOperationT_InoutputVariable(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "inoutputVariable",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (operationVariableTEClass,
		   source,
		   new String[] {
			   "name", "operationVariable_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getOperationVariableT_Value(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "value",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (pathTypeTEClass,
		   source,
		   new String[] {
			   "name", "pathType_t",
			   "kind", "simple"
		   });
		addAnnotation
		  (getPathTypeT_Value(),
		   source,
		   new String[] {
			   "name", ":0",
			   "kind", "simple"
		   });
		addAnnotation
		  (propertyTEClass,
		   source,
		   new String[] {
			   "name", "property_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getPropertyT_ValueType(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "valueType",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getPropertyT_Value(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "value",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getPropertyT_ValueId(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "valueId",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (qualifierTEClass,
		   source,
		   new String[] {
			   "name", "qualifier_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getQualifierT_SemanticId(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "semanticId",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getQualifierT_Type(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "type",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getQualifierT_ValueType(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "valueType",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getQualifierT_ValueId(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "valueId",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getQualifierT_Value(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "value",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (qualifierTypeTEClass,
		   source,
		   new String[] {
			   "name", "qualifierType_t",
			   "kind", "simple"
		   });
		addAnnotation
		  (getQualifierTypeT_Value(),
		   source,
		   new String[] {
			   "name", ":0",
			   "kind", "simple"
		   });
		addAnnotation
		  (rangeTEClass,
		   source,
		   new String[] {
			   "name", "range_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getRangeT_ValueType(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "valueType",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getRangeT_Min(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "min",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getRangeT_Max(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "max",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (referenceElementTEClass,
		   source,
		   new String[] {
			   "name", "referenceElement_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getReferenceElementT_Value(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "value",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (referencesTEClass,
		   source,
		   new String[] {
			   "name", "references_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getReferencesT_Reference(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "reference",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (referenceTEClass,
		   source,
		   new String[] {
			   "name", "reference_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getReferenceT_Keys(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "keys",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (relationshipElementTEClass,
		   source,
		   new String[] {
			   "name", "relationshipElement_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getRelationshipElementT_First(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "first",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getRelationshipElementT_Second(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "second",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (semanticIdTEClass,
		   source,
		   new String[] {
			   "name", "semanticId_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (submodelElementAbstractTEClass,
		   source,
		   new String[] {
			   "name", "submodelElementAbstract_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getSubmodelElementAbstractT_IdShort(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "idShort",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelElementAbstractT_Category(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "category",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelElementAbstractT_Description(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "description",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelElementAbstractT_Parent(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "parent",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelElementAbstractT_Kind(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "kind",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelElementAbstractT_SemanticId(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "semanticId",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelElementAbstractT_Qualifier(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "qualifier",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelElementAbstractT_EmbeddedDataSpecification(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "embeddedDataSpecification",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (submodelElementCollectionTEClass,
		   source,
		   new String[] {
			   "name", "submodelElementCollection_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getSubmodelElementCollectionT_Value(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "value",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelElementCollectionT_Ordered(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "ordered",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelElementCollectionT_AllowDuplicates(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "allowDuplicates",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (submodelElementsTEClass,
		   source,
		   new String[] {
			   "name", "submodelElements_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getSubmodelElementsT_SubmodelElement(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "submodelElement",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (submodelElementTEClass,
		   source,
		   new String[] {
			   "name", "submodelElement_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getSubmodelElementT_MultiLanguageProperty(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "multiLanguageProperty",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelElementT_Property(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "property",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelElementT_Range(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "range",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelElementT_Blob(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "blob",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelElementT_File(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "file",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelElementT_ReferenceElement(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "referenceElement",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelElementT_AnnotatedRelationshipElement(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "annotatedRelationshipElement",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelElementT_BasicEvent(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "basicEvent",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelElementT_Capability(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "capability",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelElementT_Entity(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "entity",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelElementT_Operation(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "operation",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelElementT_RelationshipElement(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "relationshipElement",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelElementT_SubmodelElementCollection(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "submodelElementCollection",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (submodelRefsTEClass,
		   source,
		   new String[] {
			   "name", "submodelRefs_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getSubmodelRefsT_SubmodelRef(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "submodelRef",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (submodelsTEClass,
		   source,
		   new String[] {
			   "name", "submodels_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getSubmodelsT_Submodel(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "submodel",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (submodelTEClass,
		   source,
		   new String[] {
			   "name", "submodel_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getSubmodelT_IdShort(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "idShort",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelT_Category(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "category",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelT_Description(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "description",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelT_Parent(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "parent",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelT_Identification(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "identification",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelT_Administration(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "administration",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelT_Kind(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "kind",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelT_SemanticId(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "semanticId",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelT_Qualifier(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "qualifier",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelT_EmbeddedDataSpecification(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "embeddedDataSpecification",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSubmodelT_SubmodelElements(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "submodelElements",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (typeTypeEEnum,
		   source,
		   new String[] {
			   "name", "type_._type"
		   });
		addAnnotation
		  (typeTypeObjectEDataType,
		   source,
		   new String[] {
			   "name", "type_._type:Object",
			   "baseType", "type_._type"
		   });
		addAnnotation
		  (valueDataTypeTEClass,
		   source,
		   new String[] {
			   "name", "valueDataType_t",
			   "kind", "simple"
		   });
		addAnnotation
		  (getValueDataTypeT_Value(),
		   source,
		   new String[] {
			   "name", ":0",
			   "kind", "simple"
		   });
		addAnnotation
		  (viewsTEClass,
		   source,
		   new String[] {
			   "name", "views_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getViewsT_View(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "view",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (viewTEClass,
		   source,
		   new String[] {
			   "name", "view_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getViewT_IdShort(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "idShort",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getViewT_Category(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "category",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getViewT_Description(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "description",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getViewT_Parent(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "parent",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getViewT_SemanticId(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "semanticId",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getViewT_EmbeddedDataSpecification(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "embeddedDataSpecification",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getViewT_ContainedElements(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "containedElements",
			   "namespace", "##targetNamespace"
		   });
	}

} //_0PackageImpl
