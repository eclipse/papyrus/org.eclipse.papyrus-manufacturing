/**
 */
package io.shell.admin.aas.abac._2._0.impl;

import io.shell.admin.aas.abac._2._0.BlobCertificateT;
import io.shell.admin.aas.abac._2._0.CertificateT;
import io.shell.admin.aas.abac._2._0._0Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Certificate T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.CertificateTImpl#getBlobCertificate <em>Blob Certificate</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CertificateTImpl extends MinimalEObjectImpl.Container implements CertificateT {
	/**
	 * The cached value of the '{@link #getBlobCertificate() <em>Blob Certificate</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBlobCertificate()
	 * @generated
	 * @ordered
	 */
	protected BlobCertificateT blobCertificate;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CertificateTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.CERTIFICATE_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlobCertificateT getBlobCertificate() {
		return blobCertificate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBlobCertificate(BlobCertificateT newBlobCertificate, NotificationChain msgs) {
		BlobCertificateT oldBlobCertificate = blobCertificate;
		blobCertificate = newBlobCertificate;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.CERTIFICATE_T__BLOB_CERTIFICATE, oldBlobCertificate, newBlobCertificate);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBlobCertificate(BlobCertificateT newBlobCertificate) {
		if (newBlobCertificate != blobCertificate) {
			NotificationChain msgs = null;
			if (blobCertificate != null)
				msgs = ((InternalEObject)blobCertificate).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.CERTIFICATE_T__BLOB_CERTIFICATE, null, msgs);
			if (newBlobCertificate != null)
				msgs = ((InternalEObject)newBlobCertificate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.CERTIFICATE_T__BLOB_CERTIFICATE, null, msgs);
			msgs = basicSetBlobCertificate(newBlobCertificate, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.CERTIFICATE_T__BLOB_CERTIFICATE, newBlobCertificate, newBlobCertificate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.CERTIFICATE_T__BLOB_CERTIFICATE:
				return basicSetBlobCertificate(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.CERTIFICATE_T__BLOB_CERTIFICATE:
				return getBlobCertificate();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.CERTIFICATE_T__BLOB_CERTIFICATE:
				setBlobCertificate((BlobCertificateT)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.CERTIFICATE_T__BLOB_CERTIFICATE:
				setBlobCertificate((BlobCertificateT)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.CERTIFICATE_T__BLOB_CERTIFICATE:
				return blobCertificate != null;
		}
		return super.eIsSet(featureID);
	}

} //CertificateTImpl
