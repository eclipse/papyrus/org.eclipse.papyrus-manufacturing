/**
 */
package io.shell.admin.aas._2._0.impl;

import io.shell.admin.aas._2._0.ReferenceT;
import io.shell.admin.aas._2._0.SubmodelRefsT;
import io.shell.admin.aas._2._0._0Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Submodel Refs T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.impl.SubmodelRefsTImpl#getSubmodelRef <em>Submodel Ref</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubmodelRefsTImpl extends MinimalEObjectImpl.Container implements SubmodelRefsT {
	/**
	 * The cached value of the '{@link #getSubmodelRef() <em>Submodel Ref</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubmodelRef()
	 * @generated
	 * @ordered
	 */
	protected EList<ReferenceT> submodelRef;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubmodelRefsTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.SUBMODEL_REFS_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReferenceT> getSubmodelRef() {
		if (submodelRef == null) {
			submodelRef = new EObjectContainmentEList<ReferenceT>(ReferenceT.class, this, _0Package.SUBMODEL_REFS_T__SUBMODEL_REF);
		}
		return submodelRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.SUBMODEL_REFS_T__SUBMODEL_REF:
				return ((InternalEList<?>)getSubmodelRef()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.SUBMODEL_REFS_T__SUBMODEL_REF:
				return getSubmodelRef();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.SUBMODEL_REFS_T__SUBMODEL_REF:
				getSubmodelRef().clear();
				getSubmodelRef().addAll((Collection<? extends ReferenceT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.SUBMODEL_REFS_T__SUBMODEL_REF:
				getSubmodelRef().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.SUBMODEL_REFS_T__SUBMODEL_REF:
				return submodelRef != null && !submodelRef.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SubmodelRefsTImpl
