/**
 */
package io.shell.admin.aas.abac._2._0.impl;

import io.shell.admin.aas.abac._2._0.AccessControlPolicyPointsT;
import io.shell.admin.aas.abac._2._0.AccessControlT;
import io.shell.admin.aas.abac._2._0.AccessPermissionRuleT;
import io.shell.admin.aas.abac._2._0.AccessPermissionRulesT;
import io.shell.admin.aas.abac._2._0.BlobCertificateT;
import io.shell.admin.aas.abac._2._0.CertificateAbstractT;
import io.shell.admin.aas.abac._2._0.CertificateT;
import io.shell.admin.aas.abac._2._0.CertificatesT;
import io.shell.admin.aas.abac._2._0.ContainedExtensionsT;
import io.shell.admin.aas.abac._2._0.DocumentRoot;
import io.shell.admin.aas.abac._2._0.InternalInformationPoints;
import io.shell.admin.aas.abac._2._0.ObjectAttributesT;
import io.shell.admin.aas.abac._2._0.PermissionKind;
import io.shell.admin.aas.abac._2._0.PermissionPerObjectT;
import io.shell.admin.aas.abac._2._0.PermissionsT;
import io.shell.admin.aas.abac._2._0.PolicyAdministrationPointT;
import io.shell.admin.aas.abac._2._0.PolicyDecisionPointT;
import io.shell.admin.aas.abac._2._0.PolicyEnforcementPointT;
import io.shell.admin.aas.abac._2._0.PolicyInformationPointsT;
import io.shell.admin.aas.abac._2._0.SecurityT;
import io.shell.admin.aas.abac._2._0.SubjectAttributesT;
import io.shell.admin.aas.abac._2._0._0Factory;
import io.shell.admin.aas.abac._2._0._0Package;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class _0PackageImpl extends EPackageImpl implements _0Package {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass accessControlPolicyPointsTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass accessControlTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass accessPermissionRulesTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass accessPermissionRuleTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blobCertificateTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass certificateAbstractTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass certificatesTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass certificateTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass containedExtensionsTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass internalInformationPointsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass objectAttributesTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass permissionPerObjectTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass permissionsTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass policyAdministrationPointTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass policyDecisionPointTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass policyEnforcementPointTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass policyInformationPointsTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass securityTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass subjectAttributesTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass documentRootEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum permissionKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType permissionKindObjectEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see io.shell.admin.aas.abac._2._0._0Package#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private _0PackageImpl() {
		super(eNS_URI, _0Factory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link _0Package#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static _0Package init() {
		if (isInited) return (_0Package)EPackage.Registry.INSTANCE.getEPackage(_0Package.eNS_URI);

		// Obtain or create and register package
		Object registered_0Package = EPackage.Registry.INSTANCE.get(eNS_URI);
		_0PackageImpl the_0Package = registered_0Package instanceof _0PackageImpl ? (_0PackageImpl)registered_0Package : new _0PackageImpl();

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(io.shell.admin.aas._2._0._0Package.eNS_URI);
		io.shell.admin.aas._2._0.impl._0PackageImpl the_0Package_1 = (io.shell.admin.aas._2._0.impl._0PackageImpl)(registeredPackage instanceof io.shell.admin.aas._2._0.impl._0PackageImpl ? registeredPackage : io.shell.admin.aas._2._0._0Package.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(io.shell.admin.iec61360._2._0._0Package.eNS_URI);
		io.shell.admin.iec61360._2._0.impl._0PackageImpl the_0Package_2 = (io.shell.admin.iec61360._2._0.impl._0PackageImpl)(registeredPackage instanceof io.shell.admin.iec61360._2._0.impl._0PackageImpl ? registeredPackage : io.shell.admin.iec61360._2._0._0Package.eINSTANCE);

		// Create package meta-data objects
		the_0Package.createPackageContents();
		the_0Package_1.createPackageContents();
		the_0Package_2.createPackageContents();

		// Initialize created meta-data
		the_0Package.initializePackageContents();
		the_0Package_1.initializePackageContents();
		the_0Package_2.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		the_0Package.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(_0Package.eNS_URI, the_0Package);
		return the_0Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAccessControlPolicyPointsT() {
		return accessControlPolicyPointsTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccessControlPolicyPointsT_PolicyAdministrationPoint() {
		return (EReference)accessControlPolicyPointsTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccessControlPolicyPointsT_PolicyDecisionPoint() {
		return (EReference)accessControlPolicyPointsTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccessControlPolicyPointsT_PolicyEnforcementPoint() {
		return (EReference)accessControlPolicyPointsTEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccessControlPolicyPointsT_PolicyInformationPoints() {
		return (EReference)accessControlPolicyPointsTEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAccessControlT() {
		return accessControlTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccessControlT_SelectableSubjectAttributes() {
		return (EReference)accessControlTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccessControlT_DefaultSubjectAttributes() {
		return (EReference)accessControlTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccessControlT_SelectablePermissions() {
		return (EReference)accessControlTEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccessControlT_DefaultPermissions() {
		return (EReference)accessControlTEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccessControlT_SelectableEnvironmentAttributes() {
		return (EReference)accessControlTEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccessControlT_DefaultEnvironmentAttributes() {
		return (EReference)accessControlTEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccessControlT_AccessPermissionRules() {
		return (EReference)accessControlTEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAccessPermissionRulesT() {
		return accessPermissionRulesTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccessPermissionRulesT_AccessPermissionRule() {
		return (EReference)accessPermissionRulesTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAccessPermissionRuleT() {
		return accessPermissionRuleTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccessPermissionRuleT_Qualifier() {
		return (EReference)accessPermissionRuleTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccessPermissionRuleT_IdShort() {
		return (EReference)accessPermissionRuleTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAccessPermissionRuleT_Category() {
		return (EAttribute)accessPermissionRuleTEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccessPermissionRuleT_Description() {
		return (EReference)accessPermissionRuleTEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccessPermissionRuleT_Parent() {
		return (EReference)accessPermissionRuleTEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccessPermissionRuleT_TargetSubjectAttributes() {
		return (EReference)accessPermissionRuleTEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccessPermissionRuleT_PermissionsPerObject() {
		return (EReference)accessPermissionRuleTEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlobCertificateT() {
		return blobCertificateTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlobCertificateT_BlobCertificate() {
		return (EReference)blobCertificateTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlobCertificateT_ContainedExtensions() {
		return (EReference)blobCertificateTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBlobCertificateT_LastCertificate() {
		return (EAttribute)blobCertificateTEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCertificateAbstractT() {
		return certificateAbstractTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCertificatesT() {
		return certificatesTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCertificatesT_Certificate() {
		return (EReference)certificatesTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCertificateT() {
		return certificateTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCertificateT_BlobCertificate() {
		return (EReference)certificateTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getContainedExtensionsT() {
		return containedExtensionsTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getContainedExtensionsT_ContainedExtension() {
		return (EReference)containedExtensionsTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInternalInformationPoints() {
		return internalInformationPointsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInternalInformationPoints_InternalInformationPoint() {
		return (EReference)internalInformationPointsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getObjectAttributesT() {
		return objectAttributesTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObjectAttributesT_ObjectAttribute() {
		return (EReference)objectAttributesTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPermissionPerObjectT() {
		return permissionPerObjectTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPermissionPerObjectT_Object() {
		return (EReference)permissionPerObjectTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPermissionPerObjectT_TargetObjectAttributes() {
		return (EReference)permissionPerObjectTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPermissionPerObjectT_Permissions() {
		return (EReference)permissionPerObjectTEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPermissionsT() {
		return permissionsTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPermissionsT_Permission() {
		return (EReference)permissionsTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPermissionsT_KindOfPermission() {
		return (EAttribute)permissionsTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPolicyAdministrationPointT() {
		return policyAdministrationPointTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPolicyAdministrationPointT_LocalAccessControl() {
		return (EReference)policyAdministrationPointTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPolicyAdministrationPointT_ExternalAccessControl() {
		return (EAttribute)policyAdministrationPointTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPolicyDecisionPointT() {
		return policyDecisionPointTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPolicyDecisionPointT_ExternalPolicyDecisionPoint() {
		return (EAttribute)policyDecisionPointTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPolicyEnforcementPointT() {
		return policyEnforcementPointTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPolicyEnforcementPointT_ExternalPolicyEnforcementPoint() {
		return (EAttribute)policyEnforcementPointTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPolicyInformationPointsT() {
		return policyInformationPointsTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPolicyInformationPointsT_ExternalInformationPoints() {
		return (EAttribute)policyInformationPointsTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPolicyInformationPointsT_InternalInformationPoints() {
		return (EReference)policyInformationPointsTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSecurityT() {
		return securityTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSecurityT_AccessControlPolicyPoints() {
		return (EReference)securityTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSecurityT_Certificates() {
		return (EReference)securityTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSecurityT_RequiredCertificateExtensions() {
		return (EReference)securityTEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSubjectAttributesT() {
		return subjectAttributesTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubjectAttributesT_SubjectAttribute() {
		return (EReference)subjectAttributesTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDocumentRoot() {
		return documentRootEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDocumentRoot_Mixed() {
		return (EAttribute)documentRootEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_XMLNSPrefixMap() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_XSISchemaLocation() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_InternalInformationPoints() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getPermissionKind() {
		return permissionKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getPermissionKindObject() {
		return permissionKindObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public _0Factory get_0Factory() {
		return (_0Factory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		accessControlPolicyPointsTEClass = createEClass(ACCESS_CONTROL_POLICY_POINTS_T);
		createEReference(accessControlPolicyPointsTEClass, ACCESS_CONTROL_POLICY_POINTS_T__POLICY_ADMINISTRATION_POINT);
		createEReference(accessControlPolicyPointsTEClass, ACCESS_CONTROL_POLICY_POINTS_T__POLICY_DECISION_POINT);
		createEReference(accessControlPolicyPointsTEClass, ACCESS_CONTROL_POLICY_POINTS_T__POLICY_ENFORCEMENT_POINT);
		createEReference(accessControlPolicyPointsTEClass, ACCESS_CONTROL_POLICY_POINTS_T__POLICY_INFORMATION_POINTS);

		accessControlTEClass = createEClass(ACCESS_CONTROL_T);
		createEReference(accessControlTEClass, ACCESS_CONTROL_T__SELECTABLE_SUBJECT_ATTRIBUTES);
		createEReference(accessControlTEClass, ACCESS_CONTROL_T__DEFAULT_SUBJECT_ATTRIBUTES);
		createEReference(accessControlTEClass, ACCESS_CONTROL_T__SELECTABLE_PERMISSIONS);
		createEReference(accessControlTEClass, ACCESS_CONTROL_T__DEFAULT_PERMISSIONS);
		createEReference(accessControlTEClass, ACCESS_CONTROL_T__SELECTABLE_ENVIRONMENT_ATTRIBUTES);
		createEReference(accessControlTEClass, ACCESS_CONTROL_T__DEFAULT_ENVIRONMENT_ATTRIBUTES);
		createEReference(accessControlTEClass, ACCESS_CONTROL_T__ACCESS_PERMISSION_RULES);

		accessPermissionRulesTEClass = createEClass(ACCESS_PERMISSION_RULES_T);
		createEReference(accessPermissionRulesTEClass, ACCESS_PERMISSION_RULES_T__ACCESS_PERMISSION_RULE);

		accessPermissionRuleTEClass = createEClass(ACCESS_PERMISSION_RULE_T);
		createEReference(accessPermissionRuleTEClass, ACCESS_PERMISSION_RULE_T__QUALIFIER);
		createEReference(accessPermissionRuleTEClass, ACCESS_PERMISSION_RULE_T__ID_SHORT);
		createEAttribute(accessPermissionRuleTEClass, ACCESS_PERMISSION_RULE_T__CATEGORY);
		createEReference(accessPermissionRuleTEClass, ACCESS_PERMISSION_RULE_T__DESCRIPTION);
		createEReference(accessPermissionRuleTEClass, ACCESS_PERMISSION_RULE_T__PARENT);
		createEReference(accessPermissionRuleTEClass, ACCESS_PERMISSION_RULE_T__TARGET_SUBJECT_ATTRIBUTES);
		createEReference(accessPermissionRuleTEClass, ACCESS_PERMISSION_RULE_T__PERMISSIONS_PER_OBJECT);

		blobCertificateTEClass = createEClass(BLOB_CERTIFICATE_T);
		createEReference(blobCertificateTEClass, BLOB_CERTIFICATE_T__BLOB_CERTIFICATE);
		createEReference(blobCertificateTEClass, BLOB_CERTIFICATE_T__CONTAINED_EXTENSIONS);
		createEAttribute(blobCertificateTEClass, BLOB_CERTIFICATE_T__LAST_CERTIFICATE);

		certificateAbstractTEClass = createEClass(CERTIFICATE_ABSTRACT_T);

		certificatesTEClass = createEClass(CERTIFICATES_T);
		createEReference(certificatesTEClass, CERTIFICATES_T__CERTIFICATE);

		certificateTEClass = createEClass(CERTIFICATE_T);
		createEReference(certificateTEClass, CERTIFICATE_T__BLOB_CERTIFICATE);

		containedExtensionsTEClass = createEClass(CONTAINED_EXTENSIONS_T);
		createEReference(containedExtensionsTEClass, CONTAINED_EXTENSIONS_T__CONTAINED_EXTENSION);

		internalInformationPointsEClass = createEClass(INTERNAL_INFORMATION_POINTS);
		createEReference(internalInformationPointsEClass, INTERNAL_INFORMATION_POINTS__INTERNAL_INFORMATION_POINT);

		objectAttributesTEClass = createEClass(OBJECT_ATTRIBUTES_T);
		createEReference(objectAttributesTEClass, OBJECT_ATTRIBUTES_T__OBJECT_ATTRIBUTE);

		permissionPerObjectTEClass = createEClass(PERMISSION_PER_OBJECT_T);
		createEReference(permissionPerObjectTEClass, PERMISSION_PER_OBJECT_T__OBJECT);
		createEReference(permissionPerObjectTEClass, PERMISSION_PER_OBJECT_T__TARGET_OBJECT_ATTRIBUTES);
		createEReference(permissionPerObjectTEClass, PERMISSION_PER_OBJECT_T__PERMISSIONS);

		permissionsTEClass = createEClass(PERMISSIONS_T);
		createEReference(permissionsTEClass, PERMISSIONS_T__PERMISSION);
		createEAttribute(permissionsTEClass, PERMISSIONS_T__KIND_OF_PERMISSION);

		policyAdministrationPointTEClass = createEClass(POLICY_ADMINISTRATION_POINT_T);
		createEReference(policyAdministrationPointTEClass, POLICY_ADMINISTRATION_POINT_T__LOCAL_ACCESS_CONTROL);
		createEAttribute(policyAdministrationPointTEClass, POLICY_ADMINISTRATION_POINT_T__EXTERNAL_ACCESS_CONTROL);

		policyDecisionPointTEClass = createEClass(POLICY_DECISION_POINT_T);
		createEAttribute(policyDecisionPointTEClass, POLICY_DECISION_POINT_T__EXTERNAL_POLICY_DECISION_POINT);

		policyEnforcementPointTEClass = createEClass(POLICY_ENFORCEMENT_POINT_T);
		createEAttribute(policyEnforcementPointTEClass, POLICY_ENFORCEMENT_POINT_T__EXTERNAL_POLICY_ENFORCEMENT_POINT);

		policyInformationPointsTEClass = createEClass(POLICY_INFORMATION_POINTS_T);
		createEAttribute(policyInformationPointsTEClass, POLICY_INFORMATION_POINTS_T__EXTERNAL_INFORMATION_POINTS);
		createEReference(policyInformationPointsTEClass, POLICY_INFORMATION_POINTS_T__INTERNAL_INFORMATION_POINTS);

		securityTEClass = createEClass(SECURITY_T);
		createEReference(securityTEClass, SECURITY_T__ACCESS_CONTROL_POLICY_POINTS);
		createEReference(securityTEClass, SECURITY_T__CERTIFICATES);
		createEReference(securityTEClass, SECURITY_T__REQUIRED_CERTIFICATE_EXTENSIONS);

		subjectAttributesTEClass = createEClass(SUBJECT_ATTRIBUTES_T);
		createEReference(subjectAttributesTEClass, SUBJECT_ATTRIBUTES_T__SUBJECT_ATTRIBUTE);

		documentRootEClass = createEClass(DOCUMENT_ROOT);
		createEAttribute(documentRootEClass, DOCUMENT_ROOT__MIXED);
		createEReference(documentRootEClass, DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
		createEReference(documentRootEClass, DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
		createEReference(documentRootEClass, DOCUMENT_ROOT__INTERNAL_INFORMATION_POINTS);

		// Create enums
		permissionKindEEnum = createEEnum(PERMISSION_KIND);

		// Create data types
		permissionKindObjectEDataType = createEDataType(PERMISSION_KIND_OBJECT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		io.shell.admin.aas._2._0._0Package the_0Package_1 = (io.shell.admin.aas._2._0._0Package)EPackage.Registry.INSTANCE.getEPackage(io.shell.admin.aas._2._0._0Package.eNS_URI);
		XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		blobCertificateTEClass.getESuperTypes().add(this.getCertificateAbstractT());

		// Initialize classes, features, and operations; add parameters
		initEClass(accessControlPolicyPointsTEClass, AccessControlPolicyPointsT.class, "AccessControlPolicyPointsT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAccessControlPolicyPointsT_PolicyAdministrationPoint(), this.getPolicyAdministrationPointT(), null, "policyAdministrationPoint", null, 1, 1, AccessControlPolicyPointsT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAccessControlPolicyPointsT_PolicyDecisionPoint(), this.getPolicyDecisionPointT(), null, "policyDecisionPoint", null, 1, 1, AccessControlPolicyPointsT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAccessControlPolicyPointsT_PolicyEnforcementPoint(), this.getPolicyEnforcementPointT(), null, "policyEnforcementPoint", null, 1, 1, AccessControlPolicyPointsT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAccessControlPolicyPointsT_PolicyInformationPoints(), this.getPolicyInformationPointsT(), null, "policyInformationPoints", null, 0, 1, AccessControlPolicyPointsT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(accessControlTEClass, AccessControlT.class, "AccessControlT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAccessControlT_SelectableSubjectAttributes(), the_0Package_1.getReferenceT(), null, "selectableSubjectAttributes", null, 0, 1, AccessControlT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAccessControlT_DefaultSubjectAttributes(), the_0Package_1.getReferenceT(), null, "defaultSubjectAttributes", null, 1, 1, AccessControlT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAccessControlT_SelectablePermissions(), the_0Package_1.getReferenceT(), null, "selectablePermissions", null, 0, 1, AccessControlT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAccessControlT_DefaultPermissions(), the_0Package_1.getReferenceT(), null, "defaultPermissions", null, 1, 1, AccessControlT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAccessControlT_SelectableEnvironmentAttributes(), the_0Package_1.getReferenceT(), null, "selectableEnvironmentAttributes", null, 0, 1, AccessControlT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAccessControlT_DefaultEnvironmentAttributes(), the_0Package_1.getReferenceT(), null, "defaultEnvironmentAttributes", null, 0, 1, AccessControlT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAccessControlT_AccessPermissionRules(), this.getAccessPermissionRulesT(), null, "accessPermissionRules", null, 0, 1, AccessControlT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(accessPermissionRulesTEClass, AccessPermissionRulesT.class, "AccessPermissionRulesT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAccessPermissionRulesT_AccessPermissionRule(), this.getAccessPermissionRuleT(), null, "accessPermissionRule", null, 0, -1, AccessPermissionRulesT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(accessPermissionRuleTEClass, AccessPermissionRuleT.class, "AccessPermissionRuleT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAccessPermissionRuleT_Qualifier(), the_0Package_1.getConstraintT(), null, "qualifier", null, 0, -1, AccessPermissionRuleT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAccessPermissionRuleT_IdShort(), the_0Package_1.getIdShortT(), null, "idShort", null, 1, 1, AccessPermissionRuleT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAccessPermissionRuleT_Category(), theXMLTypePackage.getString(), "category", null, 0, 1, AccessPermissionRuleT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAccessPermissionRuleT_Description(), the_0Package_1.getLangStringSetT(), null, "description", null, 0, 1, AccessPermissionRuleT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAccessPermissionRuleT_Parent(), the_0Package_1.getReferenceT(), null, "parent", null, 0, 1, AccessPermissionRuleT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAccessPermissionRuleT_TargetSubjectAttributes(), this.getSubjectAttributesT(), null, "targetSubjectAttributes", null, 1, -1, AccessPermissionRuleT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAccessPermissionRuleT_PermissionsPerObject(), this.getPermissionPerObjectT(), null, "permissionsPerObject", null, 0, -1, AccessPermissionRuleT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(blobCertificateTEClass, BlobCertificateT.class, "BlobCertificateT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBlobCertificateT_BlobCertificate(), the_0Package_1.getBlobT(), null, "blobCertificate", null, 1, 1, BlobCertificateT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBlobCertificateT_ContainedExtensions(), this.getContainedExtensionsT(), null, "containedExtensions", null, 0, 1, BlobCertificateT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBlobCertificateT_LastCertificate(), theXMLTypePackage.getBoolean(), "lastCertificate", null, 1, 1, BlobCertificateT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(certificateAbstractTEClass, CertificateAbstractT.class, "CertificateAbstractT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(certificatesTEClass, CertificatesT.class, "CertificatesT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCertificatesT_Certificate(), this.getCertificateT(), null, "certificate", null, 1, -1, CertificatesT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(certificateTEClass, CertificateT.class, "CertificateT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCertificateT_BlobCertificate(), this.getBlobCertificateT(), null, "blobCertificate", null, 0, 1, CertificateT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(containedExtensionsTEClass, ContainedExtensionsT.class, "ContainedExtensionsT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getContainedExtensionsT_ContainedExtension(), the_0Package_1.getReferenceT(), null, "containedExtension", null, 0, -1, ContainedExtensionsT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(internalInformationPointsEClass, InternalInformationPoints.class, "InternalInformationPoints", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInternalInformationPoints_InternalInformationPoint(), the_0Package_1.getSubmodelRefsT(), null, "internalInformationPoint", null, 0, -1, InternalInformationPoints.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(objectAttributesTEClass, ObjectAttributesT.class, "ObjectAttributesT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getObjectAttributesT_ObjectAttribute(), the_0Package_1.getPropertyT(), null, "objectAttribute", null, 1, -1, ObjectAttributesT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(permissionPerObjectTEClass, PermissionPerObjectT.class, "PermissionPerObjectT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPermissionPerObjectT_Object(), the_0Package_1.getReferenceT(), null, "object", null, 1, 1, PermissionPerObjectT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPermissionPerObjectT_TargetObjectAttributes(), this.getObjectAttributesT(), null, "targetObjectAttributes", null, 0, 1, PermissionPerObjectT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPermissionPerObjectT_Permissions(), this.getPermissionsT(), null, "permissions", null, 0, 1, PermissionPerObjectT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(permissionsTEClass, PermissionsT.class, "PermissionsT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPermissionsT_Permission(), the_0Package_1.getPropertyT(), null, "permission", null, 1, 1, PermissionsT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPermissionsT_KindOfPermission(), this.getPermissionKind(), "kindOfPermission", null, 1, 1, PermissionsT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(policyAdministrationPointTEClass, PolicyAdministrationPointT.class, "PolicyAdministrationPointT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPolicyAdministrationPointT_LocalAccessControl(), this.getAccessControlT(), null, "localAccessControl", null, 0, 1, PolicyAdministrationPointT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPolicyAdministrationPointT_ExternalAccessControl(), theXMLTypePackage.getBoolean(), "externalAccessControl", null, 0, 1, PolicyAdministrationPointT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(policyDecisionPointTEClass, PolicyDecisionPointT.class, "PolicyDecisionPointT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPolicyDecisionPointT_ExternalPolicyDecisionPoint(), theXMLTypePackage.getBoolean(), "externalPolicyDecisionPoint", null, 1, 1, PolicyDecisionPointT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(policyEnforcementPointTEClass, PolicyEnforcementPointT.class, "PolicyEnforcementPointT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPolicyEnforcementPointT_ExternalPolicyEnforcementPoint(), theXMLTypePackage.getBoolean(), "externalPolicyEnforcementPoint", null, 1, 1, PolicyEnforcementPointT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(policyInformationPointsTEClass, PolicyInformationPointsT.class, "PolicyInformationPointsT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPolicyInformationPointsT_ExternalInformationPoints(), theXMLTypePackage.getBoolean(), "externalInformationPoints", null, 1, 1, PolicyInformationPointsT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPolicyInformationPointsT_InternalInformationPoints(), this.getInternalInformationPoints(), null, "internalInformationPoints", null, 0, 1, PolicyInformationPointsT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(securityTEClass, SecurityT.class, "SecurityT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSecurityT_AccessControlPolicyPoints(), this.getAccessControlPolicyPointsT(), null, "accessControlPolicyPoints", null, 1, 1, SecurityT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSecurityT_Certificates(), this.getCertificatesT(), null, "certificates", null, 0, 1, SecurityT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSecurityT_RequiredCertificateExtensions(), the_0Package_1.getReferencesT(), null, "requiredCertificateExtensions", null, 0, 1, SecurityT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(subjectAttributesTEClass, SubjectAttributesT.class, "SubjectAttributesT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSubjectAttributesT_SubjectAttribute(), the_0Package_1.getPropertyT(), null, "subjectAttribute", null, 1, -1, SubjectAttributesT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(documentRootEClass, DocumentRoot.class, "DocumentRoot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDocumentRoot_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, null, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_XMLNSPrefixMap(), ecorePackage.getEStringToStringMapEntry(), null, "xMLNSPrefixMap", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_XSISchemaLocation(), ecorePackage.getEStringToStringMapEntry(), null, "xSISchemaLocation", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_InternalInformationPoints(), the_0Package_1.getSubmodelRefsT(), null, "internalInformationPoints", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(permissionKindEEnum, PermissionKind.class, "PermissionKind");
		addEEnumLiteral(permissionKindEEnum, PermissionKind.ALLOW);
		addEEnumLiteral(permissionKindEEnum, PermissionKind.DENY);
		addEEnumLiteral(permissionKindEEnum, PermissionKind.NOT_APPLICABLE);
		addEEnumLiteral(permissionKindEEnum, PermissionKind.UNDEFINED);

		// Initialize data types
		initEDataType(permissionKindObjectEDataType, PermissionKind.class, "PermissionKindObject", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http:///org/eclipse/emf/ecore/util/ExtendedMetaData
		createExtendedMetaDataAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createExtendedMetaDataAnnotations() {
		String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";
		addAnnotation
		  (accessControlPolicyPointsTEClass,
		   source,
		   new String[] {
			   "name", "accessControlPolicyPoints_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getAccessControlPolicyPointsT_PolicyAdministrationPoint(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "policyAdministrationPoint",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAccessControlPolicyPointsT_PolicyDecisionPoint(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "policyDecisionPoint",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAccessControlPolicyPointsT_PolicyEnforcementPoint(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "policyEnforcementPoint",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAccessControlPolicyPointsT_PolicyInformationPoints(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "policyInformationPoints",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (accessControlTEClass,
		   source,
		   new String[] {
			   "name", "accessControl_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getAccessControlT_SelectableSubjectAttributes(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "selectableSubjectAttributes",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAccessControlT_DefaultSubjectAttributes(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "defaultSubjectAttributes",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAccessControlT_SelectablePermissions(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "selectablePermissions",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAccessControlT_DefaultPermissions(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "defaultPermissions",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAccessControlT_SelectableEnvironmentAttributes(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "selectableEnvironmentAttributes",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAccessControlT_DefaultEnvironmentAttributes(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "defaultEnvironmentAttributes",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAccessControlT_AccessPermissionRules(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "accessPermissionRules",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (accessPermissionRulesTEClass,
		   source,
		   new String[] {
			   "name", "accessPermissionRules_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getAccessPermissionRulesT_AccessPermissionRule(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "accessPermissionRule",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (accessPermissionRuleTEClass,
		   source,
		   new String[] {
			   "name", "accessPermissionRule_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getAccessPermissionRuleT_Qualifier(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "qualifier",
			   "namespace", "http://www.admin-shell.io/aas/2/0"
		   });
		addAnnotation
		  (getAccessPermissionRuleT_IdShort(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "idShort",
			   "namespace", "http://www.admin-shell.io/aas/2/0"
		   });
		addAnnotation
		  (getAccessPermissionRuleT_Category(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "category",
			   "namespace", "http://www.admin-shell.io/aas/2/0"
		   });
		addAnnotation
		  (getAccessPermissionRuleT_Description(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "description",
			   "namespace", "http://www.admin-shell.io/aas/2/0"
		   });
		addAnnotation
		  (getAccessPermissionRuleT_Parent(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "parent",
			   "namespace", "http://www.admin-shell.io/aas/2/0"
		   });
		addAnnotation
		  (getAccessPermissionRuleT_TargetSubjectAttributes(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "targetSubjectAttributes",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getAccessPermissionRuleT_PermissionsPerObject(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "permissionsPerObject",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (blobCertificateTEClass,
		   source,
		   new String[] {
			   "name", "blobCertificate_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getBlobCertificateT_BlobCertificate(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "blobCertificate",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getBlobCertificateT_ContainedExtensions(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "containedExtensions",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getBlobCertificateT_LastCertificate(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "lastCertificate",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (certificateAbstractTEClass,
		   source,
		   new String[] {
			   "name", "certificateAbstract_t",
			   "kind", "empty"
		   });
		addAnnotation
		  (certificatesTEClass,
		   source,
		   new String[] {
			   "name", "certificates_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getCertificatesT_Certificate(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "certificate",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (certificateTEClass,
		   source,
		   new String[] {
			   "name", "certificate_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getCertificateT_BlobCertificate(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "blobCertificate",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (containedExtensionsTEClass,
		   source,
		   new String[] {
			   "name", "containedExtensions_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getContainedExtensionsT_ContainedExtension(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "containedExtension",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (internalInformationPointsEClass,
		   source,
		   new String[] {
			   "name", "internalInformationPoints",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getInternalInformationPoints_InternalInformationPoint(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "internalInformationPoint",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (objectAttributesTEClass,
		   source,
		   new String[] {
			   "name", "objectAttributes_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getObjectAttributesT_ObjectAttribute(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "objectAttribute",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (permissionKindEEnum,
		   source,
		   new String[] {
			   "name", "permissionKind"
		   });
		addAnnotation
		  (permissionKindObjectEDataType,
		   source,
		   new String[] {
			   "name", "permissionKind:Object",
			   "baseType", "permissionKind"
		   });
		addAnnotation
		  (permissionPerObjectTEClass,
		   source,
		   new String[] {
			   "name", "permissionPerObject_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getPermissionPerObjectT_Object(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "object",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getPermissionPerObjectT_TargetObjectAttributes(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "targetObjectAttributes",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getPermissionPerObjectT_Permissions(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "permissions",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (permissionsTEClass,
		   source,
		   new String[] {
			   "name", "permissions_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getPermissionsT_Permission(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "permission",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getPermissionsT_KindOfPermission(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "kindOfPermission",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (policyAdministrationPointTEClass,
		   source,
		   new String[] {
			   "name", "policyAdministrationPoint_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getPolicyAdministrationPointT_LocalAccessControl(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "localAccessControl",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getPolicyAdministrationPointT_ExternalAccessControl(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "externalAccessControl",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (policyDecisionPointTEClass,
		   source,
		   new String[] {
			   "name", "policyDecisionPoint_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getPolicyDecisionPointT_ExternalPolicyDecisionPoint(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "externalPolicyDecisionPoint",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (policyEnforcementPointTEClass,
		   source,
		   new String[] {
			   "name", "policyEnforcementPoint_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getPolicyEnforcementPointT_ExternalPolicyEnforcementPoint(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "externalPolicyEnforcementPoint",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (policyInformationPointsTEClass,
		   source,
		   new String[] {
			   "name", "policyInformationPoints_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getPolicyInformationPointsT_ExternalInformationPoints(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "externalInformationPoints",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getPolicyInformationPointsT_InternalInformationPoints(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "internalInformationPoints",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (securityTEClass,
		   source,
		   new String[] {
			   "name", "security_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getSecurityT_AccessControlPolicyPoints(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "accessControlPolicyPoints",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSecurityT_Certificates(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "certificates",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getSecurityT_RequiredCertificateExtensions(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "requiredCertificateExtensions",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (subjectAttributesTEClass,
		   source,
		   new String[] {
			   "name", "subjectAttributes_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getSubjectAttributesT_SubjectAttribute(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "subjectAttribute",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (documentRootEClass,
		   source,
		   new String[] {
			   "name", "",
			   "kind", "mixed"
		   });
		addAnnotation
		  (getDocumentRoot_Mixed(),
		   source,
		   new String[] {
			   "kind", "elementWildcard",
			   "name", ":mixed"
		   });
		addAnnotation
		  (getDocumentRoot_XMLNSPrefixMap(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "xmlns:prefix"
		   });
		addAnnotation
		  (getDocumentRoot_XSISchemaLocation(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "xsi:schemaLocation"
		   });
		addAnnotation
		  (getDocumentRoot_InternalInformationPoints(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "internalInformationPoints",
			   "namespace", "##targetNamespace"
		   });
	}

} //_0PackageImpl
