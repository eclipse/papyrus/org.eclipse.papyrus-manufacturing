/**
 */
package io.shell.admin.aas._2._0.impl;

import io.shell.admin.aas._2._0.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.xml.type.XMLTypeFactory;
import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class _0FactoryImpl extends EFactoryImpl implements _0Factory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static _0Factory init() {
		try {
			_0Factory the_0Factory = (_0Factory)EPackage.Registry.INSTANCE.getEFactory(_0Package.eNS_URI);
			if (the_0Factory != null) {
				return the_0Factory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new _0FactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public _0FactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case _0Package.AASENV_T: return createAasenvT();
			case _0Package.ADMINISTRATION_T: return createAdministrationT();
			case _0Package.ANNOTATED_RELATIONSHIP_ELEMENT_T: return createAnnotatedRelationshipElementT();
			case _0Package.ASSET_ADMINISTRATION_SHELLS_T: return createAssetAdministrationShellsT();
			case _0Package.ASSET_ADMINISTRATION_SHELL_T: return createAssetAdministrationShellT();
			case _0Package.ASSETS_T: return createAssetsT();
			case _0Package.ASSET_T: return createAssetT();
			case _0Package.BASIC_EVENT_T: return createBasicEventT();
			case _0Package.BLOB_T: return createBlobT();
			case _0Package.BLOB_TYPE_T: return createBlobTypeT();
			case _0Package.CONCEPT_DESCRIPTION_REFS_T: return createConceptDescriptionRefsT();
			case _0Package.CONCEPT_DESCRIPTIONS_T: return createConceptDescriptionsT();
			case _0Package.CONCEPT_DESCRIPTION_T: return createConceptDescriptionT();
			case _0Package.CONCEPT_DICTIONARIES_T: return createConceptDictionariesT();
			case _0Package.CONCEPT_DICTIONARY_T: return createConceptDictionaryT();
			case _0Package.CONSTRAINT_T: return createConstraintT();
			case _0Package.CONTAINED_ELEMENTS_T: return createContainedElementsT();
			case _0Package.DATA_ELEMENT_ABSTRACT_T: return createDataElementAbstractT();
			case _0Package.DATA_ELEMENTS_T: return createDataElementsT();
			case _0Package.DATA_SPECIFICATION_CONTENT_T: return createDataSpecificationContentT();
			case _0Package.DATA_TYPE_DEF_T: return createDataTypeDefT();
			case _0Package.DOCUMENT_ROOT: return createDocumentRoot();
			case _0Package.EMBEDDED_DATA_SPECIFICATION_T: return createEmbeddedDataSpecificationT();
			case _0Package.ENTITY_T: return createEntityT();
			case _0Package.EVENT_ABSTRACT_T: return createEventAbstractT();
			case _0Package.FILE_T: return createFileT();
			case _0Package.FORMULA_T: return createFormulaT();
			case _0Package.IDENTIFICATION_T: return createIdentificationT();
			case _0Package.IDENTIFIER_T: return createIdentifierT();
			case _0Package.ID_PROPERTY_DEFINITION_T: return createIdPropertyDefinitionT();
			case _0Package.ID_SHORT_T: return createIdShortT();
			case _0Package.KEYS_T: return createKeysT();
			case _0Package.KEY_T: return createKeyT();
			case _0Package.LANG_STRING_SET_T: return createLangStringSetT();
			case _0Package.LANG_STRING_T: return createLangStringT();
			case _0Package.MULTI_LANGUAGE_PROPERTY_T: return createMultiLanguagePropertyT();
			case _0Package.OPERATION_T: return createOperationT();
			case _0Package.OPERATION_VARIABLE_T: return createOperationVariableT();
			case _0Package.PATH_TYPE_T: return createPathTypeT();
			case _0Package.PROPERTY_T: return createPropertyT();
			case _0Package.QUALIFIER_T: return createQualifierT();
			case _0Package.QUALIFIER_TYPE_T: return createQualifierTypeT();
			case _0Package.RANGE_T: return createRangeT();
			case _0Package.REFERENCE_ELEMENT_T: return createReferenceElementT();
			case _0Package.REFERENCES_T: return createReferencesT();
			case _0Package.REFERENCE_T: return createReferenceT();
			case _0Package.RELATIONSHIP_ELEMENT_T: return createRelationshipElementT();
			case _0Package.SEMANTIC_ID_T: return createSemanticIdT();
			case _0Package.SUBMODEL_ELEMENT_ABSTRACT_T: return createSubmodelElementAbstractT();
			case _0Package.SUBMODEL_ELEMENT_COLLECTION_T: return createSubmodelElementCollectionT();
			case _0Package.SUBMODEL_ELEMENTS_T: return createSubmodelElementsT();
			case _0Package.SUBMODEL_ELEMENT_T: return createSubmodelElementT();
			case _0Package.SUBMODEL_REFS_T: return createSubmodelRefsT();
			case _0Package.SUBMODELS_T: return createSubmodelsT();
			case _0Package.SUBMODEL_T: return createSubmodelT();
			case _0Package.VALUE_DATA_TYPE_T: return createValueDataTypeT();
			case _0Package.VIEWS_T: return createViewsT();
			case _0Package.VIEW_T: return createViewT();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case _0Package.ASSET_KIND_T:
				return createAssetKindTFromString(eDataType, initialValue);
			case _0Package.ENTITY_TYPE_TYPE:
				return createEntityTypeTypeFromString(eDataType, initialValue);
			case _0Package.IDENTIFIER_TYPE_T:
				return createIdentifierTypeTFromString(eDataType, initialValue);
			case _0Package.ID_TYPE_TYPE:
				return createIdTypeTypeFromString(eDataType, initialValue);
			case _0Package.ID_TYPE_TYPE1:
				return createIdTypeType1FromString(eDataType, initialValue);
			case _0Package.MODELING_KIND_T:
				return createModelingKindTFromString(eDataType, initialValue);
			case _0Package.TYPE_TYPE:
				return createTypeTypeFromString(eDataType, initialValue);
			case _0Package.ASSET_KIND_TOBJECT:
				return createAssetKindTObjectFromString(eDataType, initialValue);
			case _0Package.ENTITY_TYPE_T:
				return createEntityTypeTFromString(eDataType, initialValue);
			case _0Package.ENTITY_TYPE_TYPE_OBJECT:
				return createEntityTypeTypeObjectFromString(eDataType, initialValue);
			case _0Package.IDENTIFIER_TYPE_TOBJECT:
				return createIdentifierTypeTObjectFromString(eDataType, initialValue);
			case _0Package.ID_TYPE_TYPE_OBJECT:
				return createIdTypeTypeObjectFromString(eDataType, initialValue);
			case _0Package.ID_TYPE_TYPE_OBJECT1:
				return createIdTypeTypeObject1FromString(eDataType, initialValue);
			case _0Package.MODELING_KIND_TOBJECT:
				return createModelingKindTObjectFromString(eDataType, initialValue);
			case _0Package.TYPE_TYPE_OBJECT:
				return createTypeTypeObjectFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case _0Package.ASSET_KIND_T:
				return convertAssetKindTToString(eDataType, instanceValue);
			case _0Package.ENTITY_TYPE_TYPE:
				return convertEntityTypeTypeToString(eDataType, instanceValue);
			case _0Package.IDENTIFIER_TYPE_T:
				return convertIdentifierTypeTToString(eDataType, instanceValue);
			case _0Package.ID_TYPE_TYPE:
				return convertIdTypeTypeToString(eDataType, instanceValue);
			case _0Package.ID_TYPE_TYPE1:
				return convertIdTypeType1ToString(eDataType, instanceValue);
			case _0Package.MODELING_KIND_T:
				return convertModelingKindTToString(eDataType, instanceValue);
			case _0Package.TYPE_TYPE:
				return convertTypeTypeToString(eDataType, instanceValue);
			case _0Package.ASSET_KIND_TOBJECT:
				return convertAssetKindTObjectToString(eDataType, instanceValue);
			case _0Package.ENTITY_TYPE_T:
				return convertEntityTypeTToString(eDataType, instanceValue);
			case _0Package.ENTITY_TYPE_TYPE_OBJECT:
				return convertEntityTypeTypeObjectToString(eDataType, instanceValue);
			case _0Package.IDENTIFIER_TYPE_TOBJECT:
				return convertIdentifierTypeTObjectToString(eDataType, instanceValue);
			case _0Package.ID_TYPE_TYPE_OBJECT:
				return convertIdTypeTypeObjectToString(eDataType, instanceValue);
			case _0Package.ID_TYPE_TYPE_OBJECT1:
				return convertIdTypeTypeObject1ToString(eDataType, instanceValue);
			case _0Package.MODELING_KIND_TOBJECT:
				return convertModelingKindTObjectToString(eDataType, instanceValue);
			case _0Package.TYPE_TYPE_OBJECT:
				return convertTypeTypeObjectToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AasenvT createAasenvT() {
		AasenvTImpl aasenvT = new AasenvTImpl();
		return aasenvT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdministrationT createAdministrationT() {
		AdministrationTImpl administrationT = new AdministrationTImpl();
		return administrationT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotatedRelationshipElementT createAnnotatedRelationshipElementT() {
		AnnotatedRelationshipElementTImpl annotatedRelationshipElementT = new AnnotatedRelationshipElementTImpl();
		return annotatedRelationshipElementT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssetAdministrationShellsT createAssetAdministrationShellsT() {
		AssetAdministrationShellsTImpl assetAdministrationShellsT = new AssetAdministrationShellsTImpl();
		return assetAdministrationShellsT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssetAdministrationShellT createAssetAdministrationShellT() {
		AssetAdministrationShellTImpl assetAdministrationShellT = new AssetAdministrationShellTImpl();
		return assetAdministrationShellT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssetsT createAssetsT() {
		AssetsTImpl assetsT = new AssetsTImpl();
		return assetsT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssetT createAssetT() {
		AssetTImpl assetT = new AssetTImpl();
		return assetT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicEventT createBasicEventT() {
		BasicEventTImpl basicEventT = new BasicEventTImpl();
		return basicEventT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlobT createBlobT() {
		BlobTImpl blobT = new BlobTImpl();
		return blobT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlobTypeT createBlobTypeT() {
		BlobTypeTImpl blobTypeT = new BlobTypeTImpl();
		return blobTypeT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConceptDescriptionRefsT createConceptDescriptionRefsT() {
		ConceptDescriptionRefsTImpl conceptDescriptionRefsT = new ConceptDescriptionRefsTImpl();
		return conceptDescriptionRefsT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConceptDescriptionsT createConceptDescriptionsT() {
		ConceptDescriptionsTImpl conceptDescriptionsT = new ConceptDescriptionsTImpl();
		return conceptDescriptionsT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConceptDescriptionT createConceptDescriptionT() {
		ConceptDescriptionTImpl conceptDescriptionT = new ConceptDescriptionTImpl();
		return conceptDescriptionT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConceptDictionariesT createConceptDictionariesT() {
		ConceptDictionariesTImpl conceptDictionariesT = new ConceptDictionariesTImpl();
		return conceptDictionariesT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConceptDictionaryT createConceptDictionaryT() {
		ConceptDictionaryTImpl conceptDictionaryT = new ConceptDictionaryTImpl();
		return conceptDictionaryT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstraintT createConstraintT() {
		ConstraintTImpl constraintT = new ConstraintTImpl();
		return constraintT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ContainedElementsT createContainedElementsT() {
		ContainedElementsTImpl containedElementsT = new ContainedElementsTImpl();
		return containedElementsT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataElementAbstractT createDataElementAbstractT() {
		DataElementAbstractTImpl dataElementAbstractT = new DataElementAbstractTImpl();
		return dataElementAbstractT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataElementsT createDataElementsT() {
		DataElementsTImpl dataElementsT = new DataElementsTImpl();
		return dataElementsT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataSpecificationContentT createDataSpecificationContentT() {
		DataSpecificationContentTImpl dataSpecificationContentT = new DataSpecificationContentTImpl();
		return dataSpecificationContentT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataTypeDefT createDataTypeDefT() {
		DataTypeDefTImpl dataTypeDefT = new DataTypeDefTImpl();
		return dataTypeDefT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DocumentRoot createDocumentRoot() {
		DocumentRootImpl documentRoot = new DocumentRootImpl();
		return documentRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EmbeddedDataSpecificationT createEmbeddedDataSpecificationT() {
		EmbeddedDataSpecificationTImpl embeddedDataSpecificationT = new EmbeddedDataSpecificationTImpl();
		return embeddedDataSpecificationT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntityT createEntityT() {
		EntityTImpl entityT = new EntityTImpl();
		return entityT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventAbstractT createEventAbstractT() {
		EventAbstractTImpl eventAbstractT = new EventAbstractTImpl();
		return eventAbstractT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FileT createFileT() {
		FileTImpl fileT = new FileTImpl();
		return fileT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormulaT createFormulaT() {
		FormulaTImpl formulaT = new FormulaTImpl();
		return formulaT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentificationT createIdentificationT() {
		IdentificationTImpl identificationT = new IdentificationTImpl();
		return identificationT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierT createIdentifierT() {
		IdentifierTImpl identifierT = new IdentifierTImpl();
		return identifierT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdPropertyDefinitionT createIdPropertyDefinitionT() {
		IdPropertyDefinitionTImpl idPropertyDefinitionT = new IdPropertyDefinitionTImpl();
		return idPropertyDefinitionT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdShortT createIdShortT() {
		IdShortTImpl idShortT = new IdShortTImpl();
		return idShortT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public KeysT createKeysT() {
		KeysTImpl keysT = new KeysTImpl();
		return keysT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public KeyT createKeyT() {
		KeyTImpl keyT = new KeyTImpl();
		return keyT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LangStringSetT createLangStringSetT() {
		LangStringSetTImpl langStringSetT = new LangStringSetTImpl();
		return langStringSetT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LangStringT createLangStringT() {
		LangStringTImpl langStringT = new LangStringTImpl();
		return langStringT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiLanguagePropertyT createMultiLanguagePropertyT() {
		MultiLanguagePropertyTImpl multiLanguagePropertyT = new MultiLanguagePropertyTImpl();
		return multiLanguagePropertyT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationT createOperationT() {
		OperationTImpl operationT = new OperationTImpl();
		return operationT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationVariableT createOperationVariableT() {
		OperationVariableTImpl operationVariableT = new OperationVariableTImpl();
		return operationVariableT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PathTypeT createPathTypeT() {
		PathTypeTImpl pathTypeT = new PathTypeTImpl();
		return pathTypeT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyT createPropertyT() {
		PropertyTImpl propertyT = new PropertyTImpl();
		return propertyT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QualifierT createQualifierT() {
		QualifierTImpl qualifierT = new QualifierTImpl();
		return qualifierT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QualifierTypeT createQualifierTypeT() {
		QualifierTypeTImpl qualifierTypeT = new QualifierTypeTImpl();
		return qualifierTypeT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RangeT createRangeT() {
		RangeTImpl rangeT = new RangeTImpl();
		return rangeT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceElementT createReferenceElementT() {
		ReferenceElementTImpl referenceElementT = new ReferenceElementTImpl();
		return referenceElementT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferencesT createReferencesT() {
		ReferencesTImpl referencesT = new ReferencesTImpl();
		return referencesT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceT createReferenceT() {
		ReferenceTImpl referenceT = new ReferenceTImpl();
		return referenceT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelationshipElementT createRelationshipElementT() {
		RelationshipElementTImpl relationshipElementT = new RelationshipElementTImpl();
		return relationshipElementT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SemanticIdT createSemanticIdT() {
		SemanticIdTImpl semanticIdT = new SemanticIdTImpl();
		return semanticIdT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubmodelElementAbstractT createSubmodelElementAbstractT() {
		SubmodelElementAbstractTImpl submodelElementAbstractT = new SubmodelElementAbstractTImpl();
		return submodelElementAbstractT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubmodelElementCollectionT createSubmodelElementCollectionT() {
		SubmodelElementCollectionTImpl submodelElementCollectionT = new SubmodelElementCollectionTImpl();
		return submodelElementCollectionT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubmodelElementsT createSubmodelElementsT() {
		SubmodelElementsTImpl submodelElementsT = new SubmodelElementsTImpl();
		return submodelElementsT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubmodelElementT createSubmodelElementT() {
		SubmodelElementTImpl submodelElementT = new SubmodelElementTImpl();
		return submodelElementT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubmodelRefsT createSubmodelRefsT() {
		SubmodelRefsTImpl submodelRefsT = new SubmodelRefsTImpl();
		return submodelRefsT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubmodelsT createSubmodelsT() {
		SubmodelsTImpl submodelsT = new SubmodelsTImpl();
		return submodelsT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubmodelT createSubmodelT() {
		SubmodelTImpl submodelT = new SubmodelTImpl();
		return submodelT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueDataTypeT createValueDataTypeT() {
		ValueDataTypeTImpl valueDataTypeT = new ValueDataTypeTImpl();
		return valueDataTypeT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ViewsT createViewsT() {
		ViewsTImpl viewsT = new ViewsTImpl();
		return viewsT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ViewT createViewT() {
		ViewTImpl viewT = new ViewTImpl();
		return viewT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssetKindT createAssetKindTFromString(EDataType eDataType, String initialValue) {
		AssetKindT result = AssetKindT.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAssetKindTToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntityTypeType createEntityTypeTypeFromString(EDataType eDataType, String initialValue) {
		EntityTypeType result = EntityTypeType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEntityTypeTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierTypeT createIdentifierTypeTFromString(EDataType eDataType, String initialValue) {
		IdentifierTypeT result = IdentifierTypeT.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIdentifierTypeTToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdTypeType createIdTypeTypeFromString(EDataType eDataType, String initialValue) {
		IdTypeType result = IdTypeType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIdTypeTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdTypeType1 createIdTypeType1FromString(EDataType eDataType, String initialValue) {
		IdTypeType1 result = IdTypeType1.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIdTypeType1ToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelingKindT createModelingKindTFromString(EDataType eDataType, String initialValue) {
		ModelingKindT result = ModelingKindT.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertModelingKindTToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeType createTypeTypeFromString(EDataType eDataType, String initialValue) {
		TypeType result = TypeType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTypeTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssetKindT createAssetKindTObjectFromString(EDataType eDataType, String initialValue) {
		return createAssetKindTFromString(_0Package.Literals.ASSET_KIND_T, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAssetKindTObjectToString(EDataType eDataType, Object instanceValue) {
		return convertAssetKindTToString(_0Package.Literals.ASSET_KIND_T, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createEntityTypeTFromString(EDataType eDataType, String initialValue) {
		return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.STRING, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEntityTypeTToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.STRING, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntityTypeType createEntityTypeTypeObjectFromString(EDataType eDataType, String initialValue) {
		return createEntityTypeTypeFromString(_0Package.Literals.ENTITY_TYPE_TYPE, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEntityTypeTypeObjectToString(EDataType eDataType, Object instanceValue) {
		return convertEntityTypeTypeToString(_0Package.Literals.ENTITY_TYPE_TYPE, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentifierTypeT createIdentifierTypeTObjectFromString(EDataType eDataType, String initialValue) {
		return createIdentifierTypeTFromString(_0Package.Literals.IDENTIFIER_TYPE_T, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIdentifierTypeTObjectToString(EDataType eDataType, Object instanceValue) {
		return convertIdentifierTypeTToString(_0Package.Literals.IDENTIFIER_TYPE_T, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdTypeType createIdTypeTypeObjectFromString(EDataType eDataType, String initialValue) {
		return createIdTypeTypeFromString(_0Package.Literals.ID_TYPE_TYPE, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIdTypeTypeObjectToString(EDataType eDataType, Object instanceValue) {
		return convertIdTypeTypeToString(_0Package.Literals.ID_TYPE_TYPE, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdTypeType1 createIdTypeTypeObject1FromString(EDataType eDataType, String initialValue) {
		return createIdTypeType1FromString(_0Package.Literals.ID_TYPE_TYPE1, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIdTypeTypeObject1ToString(EDataType eDataType, Object instanceValue) {
		return convertIdTypeType1ToString(_0Package.Literals.ID_TYPE_TYPE1, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelingKindT createModelingKindTObjectFromString(EDataType eDataType, String initialValue) {
		return createModelingKindTFromString(_0Package.Literals.MODELING_KIND_T, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertModelingKindTObjectToString(EDataType eDataType, Object instanceValue) {
		return convertModelingKindTToString(_0Package.Literals.MODELING_KIND_T, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeType createTypeTypeObjectFromString(EDataType eDataType, String initialValue) {
		return createTypeTypeFromString(_0Package.Literals.TYPE_TYPE, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTypeTypeObjectToString(EDataType eDataType, Object instanceValue) {
		return convertTypeTypeToString(_0Package.Literals.TYPE_TYPE, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public _0Package get_0Package() {
		return (_0Package)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static _0Package getPackage() {
		return _0Package.eINSTANCE;
	}

} //_0FactoryImpl
