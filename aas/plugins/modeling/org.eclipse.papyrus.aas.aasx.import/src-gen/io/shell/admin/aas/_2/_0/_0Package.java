/**
 */
package io.shell.admin.aas._2._0;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see io.shell.admin.aas._2._0._0Factory
 * @model kind="package"
 * @generated
 */
public interface _0Package extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "_0";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.admin-shell.io/aas/2/0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "_0";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	_0Package eINSTANCE = io.shell.admin.aas._2._0.impl._0PackageImpl.init();

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.AasenvTImpl <em>Aasenv T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.AasenvTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getAasenvT()
	 * @generated
	 */
	int AASENV_T = 0;

	/**
	 * The feature id for the '<em><b>Asset Administration Shells</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AASENV_T__ASSET_ADMINISTRATION_SHELLS = 0;

	/**
	 * The feature id for the '<em><b>Assets</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AASENV_T__ASSETS = 1;

	/**
	 * The feature id for the '<em><b>Submodels</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AASENV_T__SUBMODELS = 2;

	/**
	 * The feature id for the '<em><b>Concept Descriptions</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AASENV_T__CONCEPT_DESCRIPTIONS = 3;

	/**
	 * The number of structural features of the '<em>Aasenv T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AASENV_T_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Aasenv T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AASENV_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.AdministrationTImpl <em>Administration T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.AdministrationTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getAdministrationT()
	 * @generated
	 */
	int ADMINISTRATION_T = 1;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMINISTRATION_T__VERSION = 0;

	/**
	 * The feature id for the '<em><b>Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMINISTRATION_T__REVISION = 1;

	/**
	 * The number of structural features of the '<em>Administration T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMINISTRATION_T_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Administration T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMINISTRATION_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.SubmodelElementAbstractTImpl <em>Submodel Element Abstract T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.SubmodelElementAbstractTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getSubmodelElementAbstractT()
	 * @generated
	 */
	int SUBMODEL_ELEMENT_ABSTRACT_T = 48;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_ABSTRACT_T__ID_SHORT = 0;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_ABSTRACT_T__CATEGORY = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_ABSTRACT_T__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_ABSTRACT_T__PARENT = 3;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_ABSTRACT_T__KIND = 4;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_ABSTRACT_T__SEMANTIC_ID = 5;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_ABSTRACT_T__QUALIFIER = 6;

	/**
	 * The feature id for the '<em><b>Embedded Data Specification</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_ABSTRACT_T__EMBEDDED_DATA_SPECIFICATION = 7;

	/**
	 * The number of structural features of the '<em>Submodel Element Abstract T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Submodel Element Abstract T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_ABSTRACT_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.RelationshipElementTImpl <em>Relationship Element T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.RelationshipElementTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getRelationshipElementT()
	 * @generated
	 */
	int RELATIONSHIP_ELEMENT_T = 46;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT_T__ID_SHORT = SUBMODEL_ELEMENT_ABSTRACT_T__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT_T__CATEGORY = SUBMODEL_ELEMENT_ABSTRACT_T__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT_T__DESCRIPTION = SUBMODEL_ELEMENT_ABSTRACT_T__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT_T__PARENT = SUBMODEL_ELEMENT_ABSTRACT_T__PARENT;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT_T__KIND = SUBMODEL_ELEMENT_ABSTRACT_T__KIND;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT_T__SEMANTIC_ID = SUBMODEL_ELEMENT_ABSTRACT_T__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT_T__QUALIFIER = SUBMODEL_ELEMENT_ABSTRACT_T__QUALIFIER;

	/**
	 * The feature id for the '<em><b>Embedded Data Specification</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT_T__EMBEDDED_DATA_SPECIFICATION = SUBMODEL_ELEMENT_ABSTRACT_T__EMBEDDED_DATA_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>First</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT_T__FIRST = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Second</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT_T__SECOND = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Relationship Element T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT_T_FEATURE_COUNT = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Relationship Element T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_ELEMENT_T_OPERATION_COUNT = SUBMODEL_ELEMENT_ABSTRACT_T_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.AnnotatedRelationshipElementTImpl <em>Annotated Relationship Element T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.AnnotatedRelationshipElementTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getAnnotatedRelationshipElementT()
	 * @generated
	 */
	int ANNOTATED_RELATIONSHIP_ELEMENT_T = 2;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_RELATIONSHIP_ELEMENT_T__ID_SHORT = RELATIONSHIP_ELEMENT_T__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_RELATIONSHIP_ELEMENT_T__CATEGORY = RELATIONSHIP_ELEMENT_T__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_RELATIONSHIP_ELEMENT_T__DESCRIPTION = RELATIONSHIP_ELEMENT_T__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_RELATIONSHIP_ELEMENT_T__PARENT = RELATIONSHIP_ELEMENT_T__PARENT;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_RELATIONSHIP_ELEMENT_T__KIND = RELATIONSHIP_ELEMENT_T__KIND;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_RELATIONSHIP_ELEMENT_T__SEMANTIC_ID = RELATIONSHIP_ELEMENT_T__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_RELATIONSHIP_ELEMENT_T__QUALIFIER = RELATIONSHIP_ELEMENT_T__QUALIFIER;

	/**
	 * The feature id for the '<em><b>Embedded Data Specification</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_RELATIONSHIP_ELEMENT_T__EMBEDDED_DATA_SPECIFICATION = RELATIONSHIP_ELEMENT_T__EMBEDDED_DATA_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>First</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_RELATIONSHIP_ELEMENT_T__FIRST = RELATIONSHIP_ELEMENT_T__FIRST;

	/**
	 * The feature id for the '<em><b>Second</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_RELATIONSHIP_ELEMENT_T__SECOND = RELATIONSHIP_ELEMENT_T__SECOND;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_RELATIONSHIP_ELEMENT_T__ANNOTATIONS = RELATIONSHIP_ELEMENT_T_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Annotated Relationship Element T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_RELATIONSHIP_ELEMENT_T_FEATURE_COUNT = RELATIONSHIP_ELEMENT_T_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Annotated Relationship Element T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_RELATIONSHIP_ELEMENT_T_OPERATION_COUNT = RELATIONSHIP_ELEMENT_T_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.AssetAdministrationShellsTImpl <em>Asset Administration Shells T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.AssetAdministrationShellsTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getAssetAdministrationShellsT()
	 * @generated
	 */
	int ASSET_ADMINISTRATION_SHELLS_T = 3;

	/**
	 * The feature id for the '<em><b>Asset Administration Shell</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELLS_T__ASSET_ADMINISTRATION_SHELL = 0;

	/**
	 * The number of structural features of the '<em>Asset Administration Shells T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELLS_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Asset Administration Shells T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELLS_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.AssetAdministrationShellTImpl <em>Asset Administration Shell T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.AssetAdministrationShellTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getAssetAdministrationShellT()
	 * @generated
	 */
	int ASSET_ADMINISTRATION_SHELL_T = 4;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL_T__ID_SHORT = 0;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL_T__CATEGORY = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL_T__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL_T__PARENT = 3;

	/**
	 * The feature id for the '<em><b>Identification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL_T__IDENTIFICATION = 4;

	/**
	 * The feature id for the '<em><b>Administration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL_T__ADMINISTRATION = 5;

	/**
	 * The feature id for the '<em><b>Embedded Data Specification</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL_T__EMBEDDED_DATA_SPECIFICATION = 6;

	/**
	 * The feature id for the '<em><b>Derived From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL_T__DERIVED_FROM = 7;

	/**
	 * The feature id for the '<em><b>Asset Ref</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL_T__ASSET_REF = 8;

	/**
	 * The feature id for the '<em><b>Submodel Refs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL_T__SUBMODEL_REFS = 9;

	/**
	 * The feature id for the '<em><b>Views</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL_T__VIEWS = 10;

	/**
	 * The feature id for the '<em><b>Concept Dictionaries</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL_T__CONCEPT_DICTIONARIES = 11;

	/**
	 * The feature id for the '<em><b>Security</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL_T__SECURITY = 12;

	/**
	 * The number of structural features of the '<em>Asset Administration Shell T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL_T_FEATURE_COUNT = 13;

	/**
	 * The number of operations of the '<em>Asset Administration Shell T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_ADMINISTRATION_SHELL_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.AssetsTImpl <em>Assets T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.AssetsTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getAssetsT()
	 * @generated
	 */
	int ASSETS_T = 5;

	/**
	 * The feature id for the '<em><b>Asset</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSETS_T__ASSET = 0;

	/**
	 * The number of structural features of the '<em>Assets T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSETS_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Assets T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSETS_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.AssetTImpl <em>Asset T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.AssetTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getAssetT()
	 * @generated
	 */
	int ASSET_T = 6;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_T__ID_SHORT = 0;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_T__CATEGORY = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_T__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_T__PARENT = 3;

	/**
	 * The feature id for the '<em><b>Identification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_T__IDENTIFICATION = 4;

	/**
	 * The feature id for the '<em><b>Administration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_T__ADMINISTRATION = 5;

	/**
	 * The feature id for the '<em><b>Embedded Data Specification</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_T__EMBEDDED_DATA_SPECIFICATION = 6;

	/**
	 * The feature id for the '<em><b>Asset Identification Model Ref</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_T__ASSET_IDENTIFICATION_MODEL_REF = 7;

	/**
	 * The feature id for the '<em><b>Bill Of Material Ref</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_T__BILL_OF_MATERIAL_REF = 8;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_T__KIND = 9;

	/**
	 * The number of structural features of the '<em>Asset T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_T_FEATURE_COUNT = 10;

	/**
	 * The number of operations of the '<em>Asset T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.EventAbstractTImpl <em>Event Abstract T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.EventAbstractTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getEventAbstractT()
	 * @generated
	 */
	int EVENT_ABSTRACT_T = 24;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_ABSTRACT_T__ID_SHORT = SUBMODEL_ELEMENT_ABSTRACT_T__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_ABSTRACT_T__CATEGORY = SUBMODEL_ELEMENT_ABSTRACT_T__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_ABSTRACT_T__DESCRIPTION = SUBMODEL_ELEMENT_ABSTRACT_T__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_ABSTRACT_T__PARENT = SUBMODEL_ELEMENT_ABSTRACT_T__PARENT;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_ABSTRACT_T__KIND = SUBMODEL_ELEMENT_ABSTRACT_T__KIND;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_ABSTRACT_T__SEMANTIC_ID = SUBMODEL_ELEMENT_ABSTRACT_T__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_ABSTRACT_T__QUALIFIER = SUBMODEL_ELEMENT_ABSTRACT_T__QUALIFIER;

	/**
	 * The feature id for the '<em><b>Embedded Data Specification</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_ABSTRACT_T__EMBEDDED_DATA_SPECIFICATION = SUBMODEL_ELEMENT_ABSTRACT_T__EMBEDDED_DATA_SPECIFICATION;

	/**
	 * The number of structural features of the '<em>Event Abstract T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_ABSTRACT_T_FEATURE_COUNT = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Event Abstract T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_ABSTRACT_T_OPERATION_COUNT = SUBMODEL_ELEMENT_ABSTRACT_T_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.BasicEventTImpl <em>Basic Event T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.BasicEventTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getBasicEventT()
	 * @generated
	 */
	int BASIC_EVENT_T = 7;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT_T__ID_SHORT = EVENT_ABSTRACT_T__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT_T__CATEGORY = EVENT_ABSTRACT_T__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT_T__DESCRIPTION = EVENT_ABSTRACT_T__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT_T__PARENT = EVENT_ABSTRACT_T__PARENT;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT_T__KIND = EVENT_ABSTRACT_T__KIND;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT_T__SEMANTIC_ID = EVENT_ABSTRACT_T__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT_T__QUALIFIER = EVENT_ABSTRACT_T__QUALIFIER;

	/**
	 * The feature id for the '<em><b>Embedded Data Specification</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT_T__EMBEDDED_DATA_SPECIFICATION = EVENT_ABSTRACT_T__EMBEDDED_DATA_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Observed</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT_T__OBSERVED = EVENT_ABSTRACT_T_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Basic Event T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT_T_FEATURE_COUNT = EVENT_ABSTRACT_T_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Basic Event T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT_T_OPERATION_COUNT = EVENT_ABSTRACT_T_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.BlobTImpl <em>Blob T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.BlobTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getBlobT()
	 * @generated
	 */
	int BLOB_T = 8;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOB_T__ID_SHORT = SUBMODEL_ELEMENT_ABSTRACT_T__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOB_T__CATEGORY = SUBMODEL_ELEMENT_ABSTRACT_T__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOB_T__DESCRIPTION = SUBMODEL_ELEMENT_ABSTRACT_T__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOB_T__PARENT = SUBMODEL_ELEMENT_ABSTRACT_T__PARENT;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOB_T__KIND = SUBMODEL_ELEMENT_ABSTRACT_T__KIND;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOB_T__SEMANTIC_ID = SUBMODEL_ELEMENT_ABSTRACT_T__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOB_T__QUALIFIER = SUBMODEL_ELEMENT_ABSTRACT_T__QUALIFIER;

	/**
	 * The feature id for the '<em><b>Embedded Data Specification</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOB_T__EMBEDDED_DATA_SPECIFICATION = SUBMODEL_ELEMENT_ABSTRACT_T__EMBEDDED_DATA_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOB_T__VALUE = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Mime Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOB_T__MIME_TYPE = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Blob T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOB_T_FEATURE_COUNT = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Blob T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOB_T_OPERATION_COUNT = SUBMODEL_ELEMENT_ABSTRACT_T_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.BlobTypeTImpl <em>Blob Type T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.BlobTypeTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getBlobTypeT()
	 * @generated
	 */
	int BLOB_TYPE_T = 9;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOB_TYPE_T__VALUE = 0;

	/**
	 * The number of structural features of the '<em>Blob Type T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOB_TYPE_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Blob Type T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOB_TYPE_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.ConceptDescriptionRefsTImpl <em>Concept Description Refs T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.ConceptDescriptionRefsTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getConceptDescriptionRefsT()
	 * @generated
	 */
	int CONCEPT_DESCRIPTION_REFS_T = 10;

	/**
	 * The feature id for the '<em><b>Concept Description Ref</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTION_REFS_T__CONCEPT_DESCRIPTION_REF = 0;

	/**
	 * The number of structural features of the '<em>Concept Description Refs T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTION_REFS_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Concept Description Refs T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTION_REFS_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.ConceptDescriptionsTImpl <em>Concept Descriptions T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.ConceptDescriptionsTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getConceptDescriptionsT()
	 * @generated
	 */
	int CONCEPT_DESCRIPTIONS_T = 11;

	/**
	 * The feature id for the '<em><b>Concept Description</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTIONS_T__CONCEPT_DESCRIPTION = 0;

	/**
	 * The number of structural features of the '<em>Concept Descriptions T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTIONS_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Concept Descriptions T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTIONS_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.ConceptDescriptionTImpl <em>Concept Description T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.ConceptDescriptionTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getConceptDescriptionT()
	 * @generated
	 */
	int CONCEPT_DESCRIPTION_T = 12;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTION_T__ID_SHORT = 0;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTION_T__CATEGORY = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTION_T__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTION_T__PARENT = 3;

	/**
	 * The feature id for the '<em><b>Identification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTION_T__IDENTIFICATION = 4;

	/**
	 * The feature id for the '<em><b>Administration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTION_T__ADMINISTRATION = 5;

	/**
	 * The feature id for the '<em><b>Embedded Data Specification</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTION_T__EMBEDDED_DATA_SPECIFICATION = 6;

	/**
	 * The feature id for the '<em><b>Is Case Of</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTION_T__IS_CASE_OF = 7;

	/**
	 * The number of structural features of the '<em>Concept Description T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTION_T_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Concept Description T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DESCRIPTION_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.ConceptDictionariesTImpl <em>Concept Dictionaries T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.ConceptDictionariesTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getConceptDictionariesT()
	 * @generated
	 */
	int CONCEPT_DICTIONARIES_T = 13;

	/**
	 * The feature id for the '<em><b>Concept Dictionary</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DICTIONARIES_T__CONCEPT_DICTIONARY = 0;

	/**
	 * The number of structural features of the '<em>Concept Dictionaries T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DICTIONARIES_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Concept Dictionaries T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DICTIONARIES_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.ConceptDictionaryTImpl <em>Concept Dictionary T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.ConceptDictionaryTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getConceptDictionaryT()
	 * @generated
	 */
	int CONCEPT_DICTIONARY_T = 14;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DICTIONARY_T__ID_SHORT = 0;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DICTIONARY_T__CATEGORY = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DICTIONARY_T__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DICTIONARY_T__PARENT = 3;

	/**
	 * The feature id for the '<em><b>Concept Description Refs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DICTIONARY_T__CONCEPT_DESCRIPTION_REFS = 4;

	/**
	 * The number of structural features of the '<em>Concept Dictionary T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DICTIONARY_T_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Concept Dictionary T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCEPT_DICTIONARY_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.ConstraintTImpl <em>Constraint T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.ConstraintTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getConstraintT()
	 * @generated
	 */
	int CONSTRAINT_T = 15;

	/**
	 * The feature id for the '<em><b>Formula</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_T__FORMULA = 0;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_T__QUALIFIER = 1;

	/**
	 * The number of structural features of the '<em>Constraint T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_T_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Constraint T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.ContainedElementsTImpl <em>Contained Elements T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.ContainedElementsTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getContainedElementsT()
	 * @generated
	 */
	int CONTAINED_ELEMENTS_T = 16;

	/**
	 * The feature id for the '<em><b>Contained Element Ref</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_ELEMENTS_T__CONTAINED_ELEMENT_REF = 0;

	/**
	 * The number of structural features of the '<em>Contained Elements T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_ELEMENTS_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Contained Elements T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_ELEMENTS_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.DataElementAbstractTImpl <em>Data Element Abstract T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.DataElementAbstractTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getDataElementAbstractT()
	 * @generated
	 */
	int DATA_ELEMENT_ABSTRACT_T = 17;

	/**
	 * The feature id for the '<em><b>Multi Language Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT_ABSTRACT_T__MULTI_LANGUAGE_PROPERTY = 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT_ABSTRACT_T__PROPERTY = 1;

	/**
	 * The feature id for the '<em><b>Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT_ABSTRACT_T__RANGE = 2;

	/**
	 * The feature id for the '<em><b>Blob</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT_ABSTRACT_T__BLOB = 3;

	/**
	 * The feature id for the '<em><b>File</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT_ABSTRACT_T__FILE = 4;

	/**
	 * The feature id for the '<em><b>Reference Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT_ABSTRACT_T__REFERENCE_ELEMENT = 5;

	/**
	 * The number of structural features of the '<em>Data Element Abstract T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT_ABSTRACT_T_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Data Element Abstract T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT_ABSTRACT_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.DataElementsTImpl <em>Data Elements T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.DataElementsTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getDataElementsT()
	 * @generated
	 */
	int DATA_ELEMENTS_T = 18;

	/**
	 * The feature id for the '<em><b>Data Element</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENTS_T__DATA_ELEMENT = 0;

	/**
	 * The number of structural features of the '<em>Data Elements T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENTS_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Data Elements T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENTS_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.DataSpecificationContentTImpl <em>Data Specification Content T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.DataSpecificationContentTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getDataSpecificationContentT()
	 * @generated
	 */
	int DATA_SPECIFICATION_CONTENT_T = 19;

	/**
	 * The feature id for the '<em><b>Data Specification IEC61360</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_CONTENT_T__DATA_SPECIFICATION_IEC61360 = 0;

	/**
	 * The number of structural features of the '<em>Data Specification Content T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_CONTENT_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Data Specification Content T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_CONTENT_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.DataTypeDefTImpl <em>Data Type Def T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.DataTypeDefTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getDataTypeDefT()
	 * @generated
	 */
	int DATA_TYPE_DEF_T = 20;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE_DEF_T__VALUE = 0;

	/**
	 * The number of structural features of the '<em>Data Type Def T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE_DEF_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Data Type Def T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE_DEF_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.DocumentRootImpl <em>Document Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.DocumentRootImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getDocumentRoot()
	 * @generated
	 */
	int DOCUMENT_ROOT = 21;

	/**
	 * The feature id for the '<em><b>Mixed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__MIXED = 0;

	/**
	 * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

	/**
	 * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

	/**
	 * The feature id for the '<em><b>Aasenv</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__AASENV = 3;

	/**
	 * The feature id for the '<em><b>Key</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__KEY = 4;

	/**
	 * The number of structural features of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.EmbeddedDataSpecificationTImpl <em>Embedded Data Specification T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.EmbeddedDataSpecificationTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getEmbeddedDataSpecificationT()
	 * @generated
	 */
	int EMBEDDED_DATA_SPECIFICATION_T = 22;

	/**
	 * The feature id for the '<em><b>Data Specification Content</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMBEDDED_DATA_SPECIFICATION_T__DATA_SPECIFICATION_CONTENT = 0;

	/**
	 * The feature id for the '<em><b>Data Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMBEDDED_DATA_SPECIFICATION_T__DATA_SPECIFICATION = 1;

	/**
	 * The number of structural features of the '<em>Embedded Data Specification T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMBEDDED_DATA_SPECIFICATION_T_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Embedded Data Specification T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMBEDDED_DATA_SPECIFICATION_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.EntityTImpl <em>Entity T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.EntityTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getEntityT()
	 * @generated
	 */
	int ENTITY_T = 23;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_T__ID_SHORT = SUBMODEL_ELEMENT_ABSTRACT_T__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_T__CATEGORY = SUBMODEL_ELEMENT_ABSTRACT_T__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_T__DESCRIPTION = SUBMODEL_ELEMENT_ABSTRACT_T__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_T__PARENT = SUBMODEL_ELEMENT_ABSTRACT_T__PARENT;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_T__KIND = SUBMODEL_ELEMENT_ABSTRACT_T__KIND;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_T__SEMANTIC_ID = SUBMODEL_ELEMENT_ABSTRACT_T__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_T__QUALIFIER = SUBMODEL_ELEMENT_ABSTRACT_T__QUALIFIER;

	/**
	 * The feature id for the '<em><b>Embedded Data Specification</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_T__EMBEDDED_DATA_SPECIFICATION = SUBMODEL_ELEMENT_ABSTRACT_T__EMBEDDED_DATA_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Statements</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_T__STATEMENTS = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Entity Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_T__ENTITY_TYPE = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Asset Ref</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_T__ASSET_REF = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Entity T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_T_FEATURE_COUNT = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Entity T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_T_OPERATION_COUNT = SUBMODEL_ELEMENT_ABSTRACT_T_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.FileTImpl <em>File T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.FileTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getFileT()
	 * @generated
	 */
	int FILE_T = 25;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_T__ID_SHORT = SUBMODEL_ELEMENT_ABSTRACT_T__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_T__CATEGORY = SUBMODEL_ELEMENT_ABSTRACT_T__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_T__DESCRIPTION = SUBMODEL_ELEMENT_ABSTRACT_T__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_T__PARENT = SUBMODEL_ELEMENT_ABSTRACT_T__PARENT;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_T__KIND = SUBMODEL_ELEMENT_ABSTRACT_T__KIND;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_T__SEMANTIC_ID = SUBMODEL_ELEMENT_ABSTRACT_T__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_T__QUALIFIER = SUBMODEL_ELEMENT_ABSTRACT_T__QUALIFIER;

	/**
	 * The feature id for the '<em><b>Embedded Data Specification</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_T__EMBEDDED_DATA_SPECIFICATION = SUBMODEL_ELEMENT_ABSTRACT_T__EMBEDDED_DATA_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Mime Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_T__MIME_TYPE = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_T__VALUE = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>File T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_T_FEATURE_COUNT = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>File T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_T_OPERATION_COUNT = SUBMODEL_ELEMENT_ABSTRACT_T_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.FormulaTImpl <em>Formula T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.FormulaTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getFormulaT()
	 * @generated
	 */
	int FORMULA_T = 26;

	/**
	 * The feature id for the '<em><b>Depends On Refs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA_T__DEPENDS_ON_REFS = 0;

	/**
	 * The number of structural features of the '<em>Formula T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Formula T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.IdentificationTImpl <em>Identification T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.IdentificationTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getIdentificationT()
	 * @generated
	 */
	int IDENTIFICATION_T = 27;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFICATION_T__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Id Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFICATION_T__ID_TYPE = 1;

	/**
	 * The number of structural features of the '<em>Identification T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFICATION_T_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Identification T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFICATION_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.IdentifierTImpl <em>Identifier T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.IdentifierTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getIdentifierT()
	 * @generated
	 */
	int IDENTIFIER_T = 28;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIER_T__ID = 0;

	/**
	 * The feature id for the '<em><b>Id Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIER_T__ID_TYPE = 1;

	/**
	 * The number of structural features of the '<em>Identifier T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIER_T_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Identifier T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIER_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.IdPropertyDefinitionTImpl <em>Id Property Definition T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.IdPropertyDefinitionTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getIdPropertyDefinitionT()
	 * @generated
	 */
	int ID_PROPERTY_DEFINITION_T = 29;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ID_PROPERTY_DEFINITION_T__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Id Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ID_PROPERTY_DEFINITION_T__ID_TYPE = 1;

	/**
	 * The number of structural features of the '<em>Id Property Definition T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ID_PROPERTY_DEFINITION_T_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Id Property Definition T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ID_PROPERTY_DEFINITION_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.IdShortTImpl <em>Id Short T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.IdShortTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getIdShortT()
	 * @generated
	 */
	int ID_SHORT_T = 30;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ID_SHORT_T__VALUE = 0;

	/**
	 * The number of structural features of the '<em>Id Short T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ID_SHORT_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Id Short T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ID_SHORT_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.KeysTImpl <em>Keys T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.KeysTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getKeysT()
	 * @generated
	 */
	int KEYS_T = 31;

	/**
	 * The feature id for the '<em><b>Key</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYS_T__KEY = 0;

	/**
	 * The number of structural features of the '<em>Keys T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYS_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Keys T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYS_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.KeyTImpl <em>Key T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.KeyTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getKeyT()
	 * @generated
	 */
	int KEY_T = 32;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_T__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Id Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_T__ID_TYPE = 1;

	/**
	 * The feature id for the '<em><b>Local</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_T__LOCAL = 2;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_T__TYPE = 3;

	/**
	 * The number of structural features of the '<em>Key T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_T_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Key T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.LangStringSetTImpl <em>Lang String Set T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.LangStringSetTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getLangStringSetT()
	 * @generated
	 */
	int LANG_STRING_SET_T = 33;

	/**
	 * The feature id for the '<em><b>Lang String</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANG_STRING_SET_T__LANG_STRING = 0;

	/**
	 * The number of structural features of the '<em>Lang String Set T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANG_STRING_SET_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Lang String Set T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANG_STRING_SET_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.LangStringTImpl <em>Lang String T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.LangStringTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getLangStringT()
	 * @generated
	 */
	int LANG_STRING_T = 34;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANG_STRING_T__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANG_STRING_T__LANG = 1;

	/**
	 * The number of structural features of the '<em>Lang String T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANG_STRING_T_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Lang String T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANG_STRING_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.MultiLanguagePropertyTImpl <em>Multi Language Property T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.MultiLanguagePropertyTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getMultiLanguagePropertyT()
	 * @generated
	 */
	int MULTI_LANGUAGE_PROPERTY_T = 35;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY_T__ID_SHORT = SUBMODEL_ELEMENT_ABSTRACT_T__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY_T__CATEGORY = SUBMODEL_ELEMENT_ABSTRACT_T__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY_T__DESCRIPTION = SUBMODEL_ELEMENT_ABSTRACT_T__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY_T__PARENT = SUBMODEL_ELEMENT_ABSTRACT_T__PARENT;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY_T__KIND = SUBMODEL_ELEMENT_ABSTRACT_T__KIND;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY_T__SEMANTIC_ID = SUBMODEL_ELEMENT_ABSTRACT_T__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY_T__QUALIFIER = SUBMODEL_ELEMENT_ABSTRACT_T__QUALIFIER;

	/**
	 * The feature id for the '<em><b>Embedded Data Specification</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY_T__EMBEDDED_DATA_SPECIFICATION = SUBMODEL_ELEMENT_ABSTRACT_T__EMBEDDED_DATA_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Value Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY_T__VALUE_ID = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY_T__VALUE = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Multi Language Property T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY_T_FEATURE_COUNT = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Multi Language Property T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_PROPERTY_T_OPERATION_COUNT = SUBMODEL_ELEMENT_ABSTRACT_T_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.OperationTImpl <em>Operation T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.OperationTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getOperationT()
	 * @generated
	 */
	int OPERATION_T = 36;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_T__ID_SHORT = SUBMODEL_ELEMENT_ABSTRACT_T__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_T__CATEGORY = SUBMODEL_ELEMENT_ABSTRACT_T__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_T__DESCRIPTION = SUBMODEL_ELEMENT_ABSTRACT_T__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_T__PARENT = SUBMODEL_ELEMENT_ABSTRACT_T__PARENT;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_T__KIND = SUBMODEL_ELEMENT_ABSTRACT_T__KIND;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_T__SEMANTIC_ID = SUBMODEL_ELEMENT_ABSTRACT_T__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_T__QUALIFIER = SUBMODEL_ELEMENT_ABSTRACT_T__QUALIFIER;

	/**
	 * The feature id for the '<em><b>Embedded Data Specification</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_T__EMBEDDED_DATA_SPECIFICATION = SUBMODEL_ELEMENT_ABSTRACT_T__EMBEDDED_DATA_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Input Variable</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_T__INPUT_VARIABLE = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Output Variable</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_T__OUTPUT_VARIABLE = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Inoutput Variable</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_T__INOUTPUT_VARIABLE = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Operation T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_T_FEATURE_COUNT = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Operation T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_T_OPERATION_COUNT = SUBMODEL_ELEMENT_ABSTRACT_T_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.OperationVariableTImpl <em>Operation Variable T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.OperationVariableTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getOperationVariableT()
	 * @generated
	 */
	int OPERATION_VARIABLE_T = 37;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_VARIABLE_T__VALUE = 0;

	/**
	 * The number of structural features of the '<em>Operation Variable T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_VARIABLE_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Operation Variable T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_VARIABLE_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.PathTypeTImpl <em>Path Type T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.PathTypeTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getPathTypeT()
	 * @generated
	 */
	int PATH_TYPE_T = 38;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_TYPE_T__VALUE = 0;

	/**
	 * The number of structural features of the '<em>Path Type T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_TYPE_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Path Type T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_TYPE_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.PropertyTImpl <em>Property T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.PropertyTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getPropertyT()
	 * @generated
	 */
	int PROPERTY_T = 39;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_T__ID_SHORT = SUBMODEL_ELEMENT_ABSTRACT_T__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_T__CATEGORY = SUBMODEL_ELEMENT_ABSTRACT_T__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_T__DESCRIPTION = SUBMODEL_ELEMENT_ABSTRACT_T__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_T__PARENT = SUBMODEL_ELEMENT_ABSTRACT_T__PARENT;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_T__KIND = SUBMODEL_ELEMENT_ABSTRACT_T__KIND;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_T__SEMANTIC_ID = SUBMODEL_ELEMENT_ABSTRACT_T__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_T__QUALIFIER = SUBMODEL_ELEMENT_ABSTRACT_T__QUALIFIER;

	/**
	 * The feature id for the '<em><b>Embedded Data Specification</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_T__EMBEDDED_DATA_SPECIFICATION = SUBMODEL_ELEMENT_ABSTRACT_T__EMBEDDED_DATA_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Value Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_T__VALUE_TYPE = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_T__VALUE = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Value Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_T__VALUE_ID = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Property T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_T_FEATURE_COUNT = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Property T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_T_OPERATION_COUNT = SUBMODEL_ELEMENT_ABSTRACT_T_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.QualifierTImpl <em>Qualifier T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.QualifierTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getQualifierT()
	 * @generated
	 */
	int QUALIFIER_T = 40;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFIER_T__SEMANTIC_ID = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFIER_T__TYPE = 1;

	/**
	 * The feature id for the '<em><b>Value Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFIER_T__VALUE_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Value Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFIER_T__VALUE_ID = 3;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFIER_T__VALUE = 4;

	/**
	 * The number of structural features of the '<em>Qualifier T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFIER_T_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Qualifier T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFIER_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.QualifierTypeTImpl <em>Qualifier Type T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.QualifierTypeTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getQualifierTypeT()
	 * @generated
	 */
	int QUALIFIER_TYPE_T = 41;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFIER_TYPE_T__VALUE = 0;

	/**
	 * The number of structural features of the '<em>Qualifier Type T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFIER_TYPE_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Qualifier Type T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFIER_TYPE_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.RangeTImpl <em>Range T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.RangeTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getRangeT()
	 * @generated
	 */
	int RANGE_T = 42;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_T__ID_SHORT = SUBMODEL_ELEMENT_ABSTRACT_T__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_T__CATEGORY = SUBMODEL_ELEMENT_ABSTRACT_T__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_T__DESCRIPTION = SUBMODEL_ELEMENT_ABSTRACT_T__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_T__PARENT = SUBMODEL_ELEMENT_ABSTRACT_T__PARENT;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_T__KIND = SUBMODEL_ELEMENT_ABSTRACT_T__KIND;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_T__SEMANTIC_ID = SUBMODEL_ELEMENT_ABSTRACT_T__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_T__QUALIFIER = SUBMODEL_ELEMENT_ABSTRACT_T__QUALIFIER;

	/**
	 * The feature id for the '<em><b>Embedded Data Specification</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_T__EMBEDDED_DATA_SPECIFICATION = SUBMODEL_ELEMENT_ABSTRACT_T__EMBEDDED_DATA_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Value Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_T__VALUE_TYPE = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Min</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_T__MIN = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Max</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_T__MAX = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Range T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_T_FEATURE_COUNT = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Range T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_T_OPERATION_COUNT = SUBMODEL_ELEMENT_ABSTRACT_T_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.ReferenceElementTImpl <em>Reference Element T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.ReferenceElementTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getReferenceElementT()
	 * @generated
	 */
	int REFERENCE_ELEMENT_T = 43;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT_T__ID_SHORT = SUBMODEL_ELEMENT_ABSTRACT_T__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT_T__CATEGORY = SUBMODEL_ELEMENT_ABSTRACT_T__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT_T__DESCRIPTION = SUBMODEL_ELEMENT_ABSTRACT_T__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT_T__PARENT = SUBMODEL_ELEMENT_ABSTRACT_T__PARENT;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT_T__KIND = SUBMODEL_ELEMENT_ABSTRACT_T__KIND;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT_T__SEMANTIC_ID = SUBMODEL_ELEMENT_ABSTRACT_T__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT_T__QUALIFIER = SUBMODEL_ELEMENT_ABSTRACT_T__QUALIFIER;

	/**
	 * The feature id for the '<em><b>Embedded Data Specification</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT_T__EMBEDDED_DATA_SPECIFICATION = SUBMODEL_ELEMENT_ABSTRACT_T__EMBEDDED_DATA_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT_T__VALUE = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Reference Element T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT_T_FEATURE_COUNT = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Reference Element T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_ELEMENT_T_OPERATION_COUNT = SUBMODEL_ELEMENT_ABSTRACT_T_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.ReferencesTImpl <em>References T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.ReferencesTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getReferencesT()
	 * @generated
	 */
	int REFERENCES_T = 44;

	/**
	 * The feature id for the '<em><b>Reference</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCES_T__REFERENCE = 0;

	/**
	 * The number of structural features of the '<em>References T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCES_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>References T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCES_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.ReferenceTImpl <em>Reference T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.ReferenceTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getReferenceT()
	 * @generated
	 */
	int REFERENCE_T = 45;

	/**
	 * The feature id for the '<em><b>Keys</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_T__KEYS = 0;

	/**
	 * The number of structural features of the '<em>Reference T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Reference T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.SemanticIdTImpl <em>Semantic Id T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.SemanticIdTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getSemanticIdT()
	 * @generated
	 */
	int SEMANTIC_ID_T = 47;

	/**
	 * The feature id for the '<em><b>Keys</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMANTIC_ID_T__KEYS = REFERENCE_T__KEYS;

	/**
	 * The number of structural features of the '<em>Semantic Id T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMANTIC_ID_T_FEATURE_COUNT = REFERENCE_T_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Semantic Id T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMANTIC_ID_T_OPERATION_COUNT = REFERENCE_T_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.SubmodelElementCollectionTImpl <em>Submodel Element Collection T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.SubmodelElementCollectionTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getSubmodelElementCollectionT()
	 * @generated
	 */
	int SUBMODEL_ELEMENT_COLLECTION_T = 49;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION_T__ID_SHORT = SUBMODEL_ELEMENT_ABSTRACT_T__ID_SHORT;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION_T__CATEGORY = SUBMODEL_ELEMENT_ABSTRACT_T__CATEGORY;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION_T__DESCRIPTION = SUBMODEL_ELEMENT_ABSTRACT_T__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION_T__PARENT = SUBMODEL_ELEMENT_ABSTRACT_T__PARENT;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION_T__KIND = SUBMODEL_ELEMENT_ABSTRACT_T__KIND;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION_T__SEMANTIC_ID = SUBMODEL_ELEMENT_ABSTRACT_T__SEMANTIC_ID;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION_T__QUALIFIER = SUBMODEL_ELEMENT_ABSTRACT_T__QUALIFIER;

	/**
	 * The feature id for the '<em><b>Embedded Data Specification</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION_T__EMBEDDED_DATA_SPECIFICATION = SUBMODEL_ELEMENT_ABSTRACT_T__EMBEDDED_DATA_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION_T__VALUE = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Ordered</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION_T__ORDERED = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Allow Duplicates</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION_T__ALLOW_DUPLICATES = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Submodel Element Collection T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION_T_FEATURE_COUNT = SUBMODEL_ELEMENT_ABSTRACT_T_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Submodel Element Collection T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_COLLECTION_T_OPERATION_COUNT = SUBMODEL_ELEMENT_ABSTRACT_T_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.SubmodelElementsTImpl <em>Submodel Elements T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.SubmodelElementsTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getSubmodelElementsT()
	 * @generated
	 */
	int SUBMODEL_ELEMENTS_T = 50;

	/**
	 * The feature id for the '<em><b>Submodel Element</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENTS_T__SUBMODEL_ELEMENT = 0;

	/**
	 * The number of structural features of the '<em>Submodel Elements T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENTS_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Submodel Elements T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENTS_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.SubmodelElementTImpl <em>Submodel Element T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.SubmodelElementTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getSubmodelElementT()
	 * @generated
	 */
	int SUBMODEL_ELEMENT_T = 51;

	/**
	 * The feature id for the '<em><b>Multi Language Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_T__MULTI_LANGUAGE_PROPERTY = 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_T__PROPERTY = 1;

	/**
	 * The feature id for the '<em><b>Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_T__RANGE = 2;

	/**
	 * The feature id for the '<em><b>Blob</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_T__BLOB = 3;

	/**
	 * The feature id for the '<em><b>File</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_T__FILE = 4;

	/**
	 * The feature id for the '<em><b>Reference Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_T__REFERENCE_ELEMENT = 5;

	/**
	 * The feature id for the '<em><b>Annotated Relationship Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_T__ANNOTATED_RELATIONSHIP_ELEMENT = 6;

	/**
	 * The feature id for the '<em><b>Basic Event</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_T__BASIC_EVENT = 7;

	/**
	 * The feature id for the '<em><b>Capability</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_T__CAPABILITY = 8;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_T__ENTITY = 9;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_T__OPERATION = 10;

	/**
	 * The feature id for the '<em><b>Relationship Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_T__RELATIONSHIP_ELEMENT = 11;

	/**
	 * The feature id for the '<em><b>Submodel Element Collection</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_T__SUBMODEL_ELEMENT_COLLECTION = 12;

	/**
	 * The number of structural features of the '<em>Submodel Element T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_T_FEATURE_COUNT = 13;

	/**
	 * The number of operations of the '<em>Submodel Element T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_ELEMENT_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.SubmodelRefsTImpl <em>Submodel Refs T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.SubmodelRefsTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getSubmodelRefsT()
	 * @generated
	 */
	int SUBMODEL_REFS_T = 52;

	/**
	 * The feature id for the '<em><b>Submodel Ref</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_REFS_T__SUBMODEL_REF = 0;

	/**
	 * The number of structural features of the '<em>Submodel Refs T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_REFS_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Submodel Refs T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_REFS_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.SubmodelsTImpl <em>Submodels T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.SubmodelsTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getSubmodelsT()
	 * @generated
	 */
	int SUBMODELS_T = 53;

	/**
	 * The feature id for the '<em><b>Submodel</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODELS_T__SUBMODEL = 0;

	/**
	 * The number of structural features of the '<em>Submodels T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODELS_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Submodels T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODELS_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.SubmodelTImpl <em>Submodel T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.SubmodelTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getSubmodelT()
	 * @generated
	 */
	int SUBMODEL_T = 54;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_T__ID_SHORT = 0;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_T__CATEGORY = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_T__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_T__PARENT = 3;

	/**
	 * The feature id for the '<em><b>Identification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_T__IDENTIFICATION = 4;

	/**
	 * The feature id for the '<em><b>Administration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_T__ADMINISTRATION = 5;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_T__KIND = 6;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_T__SEMANTIC_ID = 7;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_T__QUALIFIER = 8;

	/**
	 * The feature id for the '<em><b>Embedded Data Specification</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_T__EMBEDDED_DATA_SPECIFICATION = 9;

	/**
	 * The feature id for the '<em><b>Submodel Elements</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_T__SUBMODEL_ELEMENTS = 10;

	/**
	 * The number of structural features of the '<em>Submodel T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_T_FEATURE_COUNT = 11;

	/**
	 * The number of operations of the '<em>Submodel T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMODEL_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.ValueDataTypeTImpl <em>Value Data Type T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.ValueDataTypeTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getValueDataTypeT()
	 * @generated
	 */
	int VALUE_DATA_TYPE_T = 55;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_DATA_TYPE_T__VALUE = 0;

	/**
	 * The number of structural features of the '<em>Value Data Type T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_DATA_TYPE_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Value Data Type T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_DATA_TYPE_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.ViewsTImpl <em>Views T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.ViewsTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getViewsT()
	 * @generated
	 */
	int VIEWS_T = 56;

	/**
	 * The feature id for the '<em><b>View</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEWS_T__VIEW = 0;

	/**
	 * The number of structural features of the '<em>Views T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEWS_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Views T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEWS_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.impl.ViewTImpl <em>View T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.impl.ViewTImpl
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getViewT()
	 * @generated
	 */
	int VIEW_T = 57;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEW_T__ID_SHORT = 0;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEW_T__CATEGORY = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEW_T__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEW_T__PARENT = 3;

	/**
	 * The feature id for the '<em><b>Semantic Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEW_T__SEMANTIC_ID = 4;

	/**
	 * The feature id for the '<em><b>Embedded Data Specification</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEW_T__EMBEDDED_DATA_SPECIFICATION = 5;

	/**
	 * The feature id for the '<em><b>Contained Elements</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEW_T__CONTAINED_ELEMENTS = 6;

	/**
	 * The number of structural features of the '<em>View T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEW_T_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>View T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIEW_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.AssetKindT <em>Asset Kind T</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.AssetKindT
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getAssetKindT()
	 * @generated
	 */
	int ASSET_KIND_T = 58;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.EntityTypeType <em>Entity Type Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.EntityTypeType
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getEntityTypeType()
	 * @generated
	 */
	int ENTITY_TYPE_TYPE = 59;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.IdentifierTypeT <em>Identifier Type T</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.IdentifierTypeT
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getIdentifierTypeT()
	 * @generated
	 */
	int IDENTIFIER_TYPE_T = 60;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.IdTypeType <em>Id Type Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.IdTypeType
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getIdTypeType()
	 * @generated
	 */
	int ID_TYPE_TYPE = 61;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.IdTypeType1 <em>Id Type Type1</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.IdTypeType1
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getIdTypeType1()
	 * @generated
	 */
	int ID_TYPE_TYPE1 = 62;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.ModelingKindT <em>Modeling Kind T</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.ModelingKindT
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getModelingKindT()
	 * @generated
	 */
	int MODELING_KIND_T = 63;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas._2._0.TypeType <em>Type Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.TypeType
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getTypeType()
	 * @generated
	 */
	int TYPE_TYPE = 64;

	/**
	 * The meta object id for the '<em>Asset Kind TObject</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.AssetKindT
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getAssetKindTObject()
	 * @generated
	 */
	int ASSET_KIND_TOBJECT = 65;

	/**
	 * The meta object id for the '<em>Entity Type T</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getEntityTypeT()
	 * @generated
	 */
	int ENTITY_TYPE_T = 66;

	/**
	 * The meta object id for the '<em>Entity Type Type Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.EntityTypeType
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getEntityTypeTypeObject()
	 * @generated
	 */
	int ENTITY_TYPE_TYPE_OBJECT = 67;

	/**
	 * The meta object id for the '<em>Identifier Type TObject</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.IdentifierTypeT
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getIdentifierTypeTObject()
	 * @generated
	 */
	int IDENTIFIER_TYPE_TOBJECT = 68;

	/**
	 * The meta object id for the '<em>Id Type Type Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.IdTypeType
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getIdTypeTypeObject()
	 * @generated
	 */
	int ID_TYPE_TYPE_OBJECT = 69;

	/**
	 * The meta object id for the '<em>Id Type Type Object1</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.IdTypeType1
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getIdTypeTypeObject1()
	 * @generated
	 */
	int ID_TYPE_TYPE_OBJECT1 = 70;

	/**
	 * The meta object id for the '<em>Modeling Kind TObject</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.ModelingKindT
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getModelingKindTObject()
	 * @generated
	 */
	int MODELING_KIND_TOBJECT = 71;

	/**
	 * The meta object id for the '<em>Type Type Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas._2._0.TypeType
	 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getTypeTypeObject()
	 * @generated
	 */
	int TYPE_TYPE_OBJECT = 72;


	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.AasenvT <em>Aasenv T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aasenv T</em>'.
	 * @see io.shell.admin.aas._2._0.AasenvT
	 * @generated
	 */
	EClass getAasenvT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.AasenvT#getAssetAdministrationShells <em>Asset Administration Shells</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Asset Administration Shells</em>'.
	 * @see io.shell.admin.aas._2._0.AasenvT#getAssetAdministrationShells()
	 * @see #getAasenvT()
	 * @generated
	 */
	EReference getAasenvT_AssetAdministrationShells();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.AasenvT#getAssets <em>Assets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Assets</em>'.
	 * @see io.shell.admin.aas._2._0.AasenvT#getAssets()
	 * @see #getAasenvT()
	 * @generated
	 */
	EReference getAasenvT_Assets();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.AasenvT#getSubmodels <em>Submodels</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Submodels</em>'.
	 * @see io.shell.admin.aas._2._0.AasenvT#getSubmodels()
	 * @see #getAasenvT()
	 * @generated
	 */
	EReference getAasenvT_Submodels();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.AasenvT#getConceptDescriptions <em>Concept Descriptions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Concept Descriptions</em>'.
	 * @see io.shell.admin.aas._2._0.AasenvT#getConceptDescriptions()
	 * @see #getAasenvT()
	 * @generated
	 */
	EReference getAasenvT_ConceptDescriptions();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.AdministrationT <em>Administration T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Administration T</em>'.
	 * @see io.shell.admin.aas._2._0.AdministrationT
	 * @generated
	 */
	EClass getAdministrationT();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.AdministrationT#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see io.shell.admin.aas._2._0.AdministrationT#getVersion()
	 * @see #getAdministrationT()
	 * @generated
	 */
	EAttribute getAdministrationT_Version();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.AdministrationT#getRevision <em>Revision</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Revision</em>'.
	 * @see io.shell.admin.aas._2._0.AdministrationT#getRevision()
	 * @see #getAdministrationT()
	 * @generated
	 */
	EAttribute getAdministrationT_Revision();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.AnnotatedRelationshipElementT <em>Annotated Relationship Element T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotated Relationship Element T</em>'.
	 * @see io.shell.admin.aas._2._0.AnnotatedRelationshipElementT
	 * @generated
	 */
	EClass getAnnotatedRelationshipElementT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.AnnotatedRelationshipElementT#getAnnotations <em>Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Annotations</em>'.
	 * @see io.shell.admin.aas._2._0.AnnotatedRelationshipElementT#getAnnotations()
	 * @see #getAnnotatedRelationshipElementT()
	 * @generated
	 */
	EReference getAnnotatedRelationshipElementT_Annotations();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.AssetAdministrationShellsT <em>Asset Administration Shells T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asset Administration Shells T</em>'.
	 * @see io.shell.admin.aas._2._0.AssetAdministrationShellsT
	 * @generated
	 */
	EClass getAssetAdministrationShellsT();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.AssetAdministrationShellsT#getAssetAdministrationShell <em>Asset Administration Shell</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Asset Administration Shell</em>'.
	 * @see io.shell.admin.aas._2._0.AssetAdministrationShellsT#getAssetAdministrationShell()
	 * @see #getAssetAdministrationShellsT()
	 * @generated
	 */
	EReference getAssetAdministrationShellsT_AssetAdministrationShell();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.AssetAdministrationShellT <em>Asset Administration Shell T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asset Administration Shell T</em>'.
	 * @see io.shell.admin.aas._2._0.AssetAdministrationShellT
	 * @generated
	 */
	EClass getAssetAdministrationShellT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.AssetAdministrationShellT#getIdShort <em>Id Short</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Id Short</em>'.
	 * @see io.shell.admin.aas._2._0.AssetAdministrationShellT#getIdShort()
	 * @see #getAssetAdministrationShellT()
	 * @generated
	 */
	EReference getAssetAdministrationShellT_IdShort();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.AssetAdministrationShellT#getCategory <em>Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Category</em>'.
	 * @see io.shell.admin.aas._2._0.AssetAdministrationShellT#getCategory()
	 * @see #getAssetAdministrationShellT()
	 * @generated
	 */
	EAttribute getAssetAdministrationShellT_Category();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.AssetAdministrationShellT#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Description</em>'.
	 * @see io.shell.admin.aas._2._0.AssetAdministrationShellT#getDescription()
	 * @see #getAssetAdministrationShellT()
	 * @generated
	 */
	EReference getAssetAdministrationShellT_Description();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.AssetAdministrationShellT#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parent</em>'.
	 * @see io.shell.admin.aas._2._0.AssetAdministrationShellT#getParent()
	 * @see #getAssetAdministrationShellT()
	 * @generated
	 */
	EReference getAssetAdministrationShellT_Parent();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.AssetAdministrationShellT#getIdentification <em>Identification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Identification</em>'.
	 * @see io.shell.admin.aas._2._0.AssetAdministrationShellT#getIdentification()
	 * @see #getAssetAdministrationShellT()
	 * @generated
	 */
	EReference getAssetAdministrationShellT_Identification();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.AssetAdministrationShellT#getAdministration <em>Administration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Administration</em>'.
	 * @see io.shell.admin.aas._2._0.AssetAdministrationShellT#getAdministration()
	 * @see #getAssetAdministrationShellT()
	 * @generated
	 */
	EReference getAssetAdministrationShellT_Administration();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.AssetAdministrationShellT#getEmbeddedDataSpecification <em>Embedded Data Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Embedded Data Specification</em>'.
	 * @see io.shell.admin.aas._2._0.AssetAdministrationShellT#getEmbeddedDataSpecification()
	 * @see #getAssetAdministrationShellT()
	 * @generated
	 */
	EReference getAssetAdministrationShellT_EmbeddedDataSpecification();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.AssetAdministrationShellT#getDerivedFrom <em>Derived From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Derived From</em>'.
	 * @see io.shell.admin.aas._2._0.AssetAdministrationShellT#getDerivedFrom()
	 * @see #getAssetAdministrationShellT()
	 * @generated
	 */
	EReference getAssetAdministrationShellT_DerivedFrom();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.AssetAdministrationShellT#getAssetRef <em>Asset Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Asset Ref</em>'.
	 * @see io.shell.admin.aas._2._0.AssetAdministrationShellT#getAssetRef()
	 * @see #getAssetAdministrationShellT()
	 * @generated
	 */
	EReference getAssetAdministrationShellT_AssetRef();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.AssetAdministrationShellT#getSubmodelRefs <em>Submodel Refs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Submodel Refs</em>'.
	 * @see io.shell.admin.aas._2._0.AssetAdministrationShellT#getSubmodelRefs()
	 * @see #getAssetAdministrationShellT()
	 * @generated
	 */
	EReference getAssetAdministrationShellT_SubmodelRefs();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.AssetAdministrationShellT#getViews <em>Views</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Views</em>'.
	 * @see io.shell.admin.aas._2._0.AssetAdministrationShellT#getViews()
	 * @see #getAssetAdministrationShellT()
	 * @generated
	 */
	EReference getAssetAdministrationShellT_Views();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.AssetAdministrationShellT#getConceptDictionaries <em>Concept Dictionaries</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Concept Dictionaries</em>'.
	 * @see io.shell.admin.aas._2._0.AssetAdministrationShellT#getConceptDictionaries()
	 * @see #getAssetAdministrationShellT()
	 * @generated
	 */
	EReference getAssetAdministrationShellT_ConceptDictionaries();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.AssetAdministrationShellT#getSecurity <em>Security</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Security</em>'.
	 * @see io.shell.admin.aas._2._0.AssetAdministrationShellT#getSecurity()
	 * @see #getAssetAdministrationShellT()
	 * @generated
	 */
	EReference getAssetAdministrationShellT_Security();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.AssetsT <em>Assets T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assets T</em>'.
	 * @see io.shell.admin.aas._2._0.AssetsT
	 * @generated
	 */
	EClass getAssetsT();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.AssetsT#getAsset <em>Asset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Asset</em>'.
	 * @see io.shell.admin.aas._2._0.AssetsT#getAsset()
	 * @see #getAssetsT()
	 * @generated
	 */
	EReference getAssetsT_Asset();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.AssetT <em>Asset T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asset T</em>'.
	 * @see io.shell.admin.aas._2._0.AssetT
	 * @generated
	 */
	EClass getAssetT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.AssetT#getIdShort <em>Id Short</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Id Short</em>'.
	 * @see io.shell.admin.aas._2._0.AssetT#getIdShort()
	 * @see #getAssetT()
	 * @generated
	 */
	EReference getAssetT_IdShort();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.AssetT#getCategory <em>Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Category</em>'.
	 * @see io.shell.admin.aas._2._0.AssetT#getCategory()
	 * @see #getAssetT()
	 * @generated
	 */
	EAttribute getAssetT_Category();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.AssetT#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Description</em>'.
	 * @see io.shell.admin.aas._2._0.AssetT#getDescription()
	 * @see #getAssetT()
	 * @generated
	 */
	EReference getAssetT_Description();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.AssetT#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parent</em>'.
	 * @see io.shell.admin.aas._2._0.AssetT#getParent()
	 * @see #getAssetT()
	 * @generated
	 */
	EReference getAssetT_Parent();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.AssetT#getIdentification <em>Identification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Identification</em>'.
	 * @see io.shell.admin.aas._2._0.AssetT#getIdentification()
	 * @see #getAssetT()
	 * @generated
	 */
	EReference getAssetT_Identification();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.AssetT#getAdministration <em>Administration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Administration</em>'.
	 * @see io.shell.admin.aas._2._0.AssetT#getAdministration()
	 * @see #getAssetT()
	 * @generated
	 */
	EReference getAssetT_Administration();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.AssetT#getEmbeddedDataSpecification <em>Embedded Data Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Embedded Data Specification</em>'.
	 * @see io.shell.admin.aas._2._0.AssetT#getEmbeddedDataSpecification()
	 * @see #getAssetT()
	 * @generated
	 */
	EReference getAssetT_EmbeddedDataSpecification();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.AssetT#getAssetIdentificationModelRef <em>Asset Identification Model Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Asset Identification Model Ref</em>'.
	 * @see io.shell.admin.aas._2._0.AssetT#getAssetIdentificationModelRef()
	 * @see #getAssetT()
	 * @generated
	 */
	EReference getAssetT_AssetIdentificationModelRef();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.AssetT#getBillOfMaterialRef <em>Bill Of Material Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Bill Of Material Ref</em>'.
	 * @see io.shell.admin.aas._2._0.AssetT#getBillOfMaterialRef()
	 * @see #getAssetT()
	 * @generated
	 */
	EReference getAssetT_BillOfMaterialRef();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.AssetT#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see io.shell.admin.aas._2._0.AssetT#getKind()
	 * @see #getAssetT()
	 * @generated
	 */
	EAttribute getAssetT_Kind();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.BasicEventT <em>Basic Event T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Basic Event T</em>'.
	 * @see io.shell.admin.aas._2._0.BasicEventT
	 * @generated
	 */
	EClass getBasicEventT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.BasicEventT#getObserved <em>Observed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Observed</em>'.
	 * @see io.shell.admin.aas._2._0.BasicEventT#getObserved()
	 * @see #getBasicEventT()
	 * @generated
	 */
	EReference getBasicEventT_Observed();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.BlobT <em>Blob T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blob T</em>'.
	 * @see io.shell.admin.aas._2._0.BlobT
	 * @generated
	 */
	EClass getBlobT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.BlobT#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see io.shell.admin.aas._2._0.BlobT#getValue()
	 * @see #getBlobT()
	 * @generated
	 */
	EReference getBlobT_Value();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.BlobT#getMimeType <em>Mime Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mime Type</em>'.
	 * @see io.shell.admin.aas._2._0.BlobT#getMimeType()
	 * @see #getBlobT()
	 * @generated
	 */
	EAttribute getBlobT_MimeType();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.BlobTypeT <em>Blob Type T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blob Type T</em>'.
	 * @see io.shell.admin.aas._2._0.BlobTypeT
	 * @generated
	 */
	EClass getBlobTypeT();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.BlobTypeT#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see io.shell.admin.aas._2._0.BlobTypeT#getValue()
	 * @see #getBlobTypeT()
	 * @generated
	 */
	EAttribute getBlobTypeT_Value();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.ConceptDescriptionRefsT <em>Concept Description Refs T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Concept Description Refs T</em>'.
	 * @see io.shell.admin.aas._2._0.ConceptDescriptionRefsT
	 * @generated
	 */
	EClass getConceptDescriptionRefsT();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.ConceptDescriptionRefsT#getConceptDescriptionRef <em>Concept Description Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Concept Description Ref</em>'.
	 * @see io.shell.admin.aas._2._0.ConceptDescriptionRefsT#getConceptDescriptionRef()
	 * @see #getConceptDescriptionRefsT()
	 * @generated
	 */
	EReference getConceptDescriptionRefsT_ConceptDescriptionRef();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.ConceptDescriptionsT <em>Concept Descriptions T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Concept Descriptions T</em>'.
	 * @see io.shell.admin.aas._2._0.ConceptDescriptionsT
	 * @generated
	 */
	EClass getConceptDescriptionsT();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.ConceptDescriptionsT#getConceptDescription <em>Concept Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Concept Description</em>'.
	 * @see io.shell.admin.aas._2._0.ConceptDescriptionsT#getConceptDescription()
	 * @see #getConceptDescriptionsT()
	 * @generated
	 */
	EReference getConceptDescriptionsT_ConceptDescription();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.ConceptDescriptionT <em>Concept Description T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Concept Description T</em>'.
	 * @see io.shell.admin.aas._2._0.ConceptDescriptionT
	 * @generated
	 */
	EClass getConceptDescriptionT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.ConceptDescriptionT#getIdShort <em>Id Short</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Id Short</em>'.
	 * @see io.shell.admin.aas._2._0.ConceptDescriptionT#getIdShort()
	 * @see #getConceptDescriptionT()
	 * @generated
	 */
	EReference getConceptDescriptionT_IdShort();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.ConceptDescriptionT#getCategory <em>Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Category</em>'.
	 * @see io.shell.admin.aas._2._0.ConceptDescriptionT#getCategory()
	 * @see #getConceptDescriptionT()
	 * @generated
	 */
	EAttribute getConceptDescriptionT_Category();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.ConceptDescriptionT#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Description</em>'.
	 * @see io.shell.admin.aas._2._0.ConceptDescriptionT#getDescription()
	 * @see #getConceptDescriptionT()
	 * @generated
	 */
	EReference getConceptDescriptionT_Description();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.ConceptDescriptionT#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parent</em>'.
	 * @see io.shell.admin.aas._2._0.ConceptDescriptionT#getParent()
	 * @see #getConceptDescriptionT()
	 * @generated
	 */
	EReference getConceptDescriptionT_Parent();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.ConceptDescriptionT#getIdentification <em>Identification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Identification</em>'.
	 * @see io.shell.admin.aas._2._0.ConceptDescriptionT#getIdentification()
	 * @see #getConceptDescriptionT()
	 * @generated
	 */
	EReference getConceptDescriptionT_Identification();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.ConceptDescriptionT#getAdministration <em>Administration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Administration</em>'.
	 * @see io.shell.admin.aas._2._0.ConceptDescriptionT#getAdministration()
	 * @see #getConceptDescriptionT()
	 * @generated
	 */
	EReference getConceptDescriptionT_Administration();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.ConceptDescriptionT#getEmbeddedDataSpecification <em>Embedded Data Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Embedded Data Specification</em>'.
	 * @see io.shell.admin.aas._2._0.ConceptDescriptionT#getEmbeddedDataSpecification()
	 * @see #getConceptDescriptionT()
	 * @generated
	 */
	EReference getConceptDescriptionT_EmbeddedDataSpecification();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.ConceptDescriptionT#getIsCaseOf <em>Is Case Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Is Case Of</em>'.
	 * @see io.shell.admin.aas._2._0.ConceptDescriptionT#getIsCaseOf()
	 * @see #getConceptDescriptionT()
	 * @generated
	 */
	EReference getConceptDescriptionT_IsCaseOf();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.ConceptDictionariesT <em>Concept Dictionaries T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Concept Dictionaries T</em>'.
	 * @see io.shell.admin.aas._2._0.ConceptDictionariesT
	 * @generated
	 */
	EClass getConceptDictionariesT();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.ConceptDictionariesT#getConceptDictionary <em>Concept Dictionary</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Concept Dictionary</em>'.
	 * @see io.shell.admin.aas._2._0.ConceptDictionariesT#getConceptDictionary()
	 * @see #getConceptDictionariesT()
	 * @generated
	 */
	EReference getConceptDictionariesT_ConceptDictionary();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.ConceptDictionaryT <em>Concept Dictionary T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Concept Dictionary T</em>'.
	 * @see io.shell.admin.aas._2._0.ConceptDictionaryT
	 * @generated
	 */
	EClass getConceptDictionaryT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.ConceptDictionaryT#getIdShort <em>Id Short</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Id Short</em>'.
	 * @see io.shell.admin.aas._2._0.ConceptDictionaryT#getIdShort()
	 * @see #getConceptDictionaryT()
	 * @generated
	 */
	EReference getConceptDictionaryT_IdShort();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.ConceptDictionaryT#getCategory <em>Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Category</em>'.
	 * @see io.shell.admin.aas._2._0.ConceptDictionaryT#getCategory()
	 * @see #getConceptDictionaryT()
	 * @generated
	 */
	EAttribute getConceptDictionaryT_Category();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.ConceptDictionaryT#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Description</em>'.
	 * @see io.shell.admin.aas._2._0.ConceptDictionaryT#getDescription()
	 * @see #getConceptDictionaryT()
	 * @generated
	 */
	EReference getConceptDictionaryT_Description();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.ConceptDictionaryT#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parent</em>'.
	 * @see io.shell.admin.aas._2._0.ConceptDictionaryT#getParent()
	 * @see #getConceptDictionaryT()
	 * @generated
	 */
	EReference getConceptDictionaryT_Parent();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.ConceptDictionaryT#getConceptDescriptionRefs <em>Concept Description Refs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Concept Description Refs</em>'.
	 * @see io.shell.admin.aas._2._0.ConceptDictionaryT#getConceptDescriptionRefs()
	 * @see #getConceptDictionaryT()
	 * @generated
	 */
	EReference getConceptDictionaryT_ConceptDescriptionRefs();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.ConstraintT <em>Constraint T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constraint T</em>'.
	 * @see io.shell.admin.aas._2._0.ConstraintT
	 * @generated
	 */
	EClass getConstraintT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.ConstraintT#getFormula <em>Formula</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Formula</em>'.
	 * @see io.shell.admin.aas._2._0.ConstraintT#getFormula()
	 * @see #getConstraintT()
	 * @generated
	 */
	EReference getConstraintT_Formula();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.ConstraintT#getQualifier <em>Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Qualifier</em>'.
	 * @see io.shell.admin.aas._2._0.ConstraintT#getQualifier()
	 * @see #getConstraintT()
	 * @generated
	 */
	EReference getConstraintT_Qualifier();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.ContainedElementsT <em>Contained Elements T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Contained Elements T</em>'.
	 * @see io.shell.admin.aas._2._0.ContainedElementsT
	 * @generated
	 */
	EClass getContainedElementsT();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.ContainedElementsT#getContainedElementRef <em>Contained Element Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Contained Element Ref</em>'.
	 * @see io.shell.admin.aas._2._0.ContainedElementsT#getContainedElementRef()
	 * @see #getContainedElementsT()
	 * @generated
	 */
	EReference getContainedElementsT_ContainedElementRef();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.DataElementAbstractT <em>Data Element Abstract T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Element Abstract T</em>'.
	 * @see io.shell.admin.aas._2._0.DataElementAbstractT
	 * @generated
	 */
	EClass getDataElementAbstractT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.DataElementAbstractT#getMultiLanguageProperty <em>Multi Language Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Multi Language Property</em>'.
	 * @see io.shell.admin.aas._2._0.DataElementAbstractT#getMultiLanguageProperty()
	 * @see #getDataElementAbstractT()
	 * @generated
	 */
	EReference getDataElementAbstractT_MultiLanguageProperty();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.DataElementAbstractT#getProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Property</em>'.
	 * @see io.shell.admin.aas._2._0.DataElementAbstractT#getProperty()
	 * @see #getDataElementAbstractT()
	 * @generated
	 */
	EReference getDataElementAbstractT_Property();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.DataElementAbstractT#getRange <em>Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Range</em>'.
	 * @see io.shell.admin.aas._2._0.DataElementAbstractT#getRange()
	 * @see #getDataElementAbstractT()
	 * @generated
	 */
	EReference getDataElementAbstractT_Range();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.DataElementAbstractT#getBlob <em>Blob</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Blob</em>'.
	 * @see io.shell.admin.aas._2._0.DataElementAbstractT#getBlob()
	 * @see #getDataElementAbstractT()
	 * @generated
	 */
	EReference getDataElementAbstractT_Blob();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.DataElementAbstractT#getFile <em>File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>File</em>'.
	 * @see io.shell.admin.aas._2._0.DataElementAbstractT#getFile()
	 * @see #getDataElementAbstractT()
	 * @generated
	 */
	EReference getDataElementAbstractT_File();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.DataElementAbstractT#getReferenceElement <em>Reference Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reference Element</em>'.
	 * @see io.shell.admin.aas._2._0.DataElementAbstractT#getReferenceElement()
	 * @see #getDataElementAbstractT()
	 * @generated
	 */
	EReference getDataElementAbstractT_ReferenceElement();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.DataElementsT <em>Data Elements T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Elements T</em>'.
	 * @see io.shell.admin.aas._2._0.DataElementsT
	 * @generated
	 */
	EClass getDataElementsT();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.DataElementsT#getDataElement <em>Data Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Element</em>'.
	 * @see io.shell.admin.aas._2._0.DataElementsT#getDataElement()
	 * @see #getDataElementsT()
	 * @generated
	 */
	EReference getDataElementsT_DataElement();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.DataSpecificationContentT <em>Data Specification Content T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Specification Content T</em>'.
	 * @see io.shell.admin.aas._2._0.DataSpecificationContentT
	 * @generated
	 */
	EClass getDataSpecificationContentT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.DataSpecificationContentT#getDataSpecificationIEC61360 <em>Data Specification IEC61360</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Specification IEC61360</em>'.
	 * @see io.shell.admin.aas._2._0.DataSpecificationContentT#getDataSpecificationIEC61360()
	 * @see #getDataSpecificationContentT()
	 * @generated
	 */
	EReference getDataSpecificationContentT_DataSpecificationIEC61360();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.DataTypeDefT <em>Data Type Def T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Type Def T</em>'.
	 * @see io.shell.admin.aas._2._0.DataTypeDefT
	 * @generated
	 */
	EClass getDataTypeDefT();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.DataTypeDefT#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see io.shell.admin.aas._2._0.DataTypeDefT#getValue()
	 * @see #getDataTypeDefT()
	 * @generated
	 */
	EAttribute getDataTypeDefT_Value();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Document Root</em>'.
	 * @see io.shell.admin.aas._2._0.DocumentRoot
	 * @generated
	 */
	EClass getDocumentRoot();

	/**
	 * Returns the meta object for the attribute list '{@link io.shell.admin.aas._2._0.DocumentRoot#getMixed <em>Mixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Mixed</em>'.
	 * @see io.shell.admin.aas._2._0.DocumentRoot#getMixed()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EAttribute getDocumentRoot_Mixed();

	/**
	 * Returns the meta object for the map '{@link io.shell.admin.aas._2._0.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
	 * @see io.shell.admin.aas._2._0.DocumentRoot#getXMLNSPrefixMap()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XMLNSPrefixMap();

	/**
	 * Returns the meta object for the map '{@link io.shell.admin.aas._2._0.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XSI Schema Location</em>'.
	 * @see io.shell.admin.aas._2._0.DocumentRoot#getXSISchemaLocation()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XSISchemaLocation();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.DocumentRoot#getAasenv <em>Aasenv</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Aasenv</em>'.
	 * @see io.shell.admin.aas._2._0.DocumentRoot#getAasenv()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Aasenv();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.DocumentRoot#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Key</em>'.
	 * @see io.shell.admin.aas._2._0.DocumentRoot#getKey()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Key();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.EmbeddedDataSpecificationT <em>Embedded Data Specification T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Embedded Data Specification T</em>'.
	 * @see io.shell.admin.aas._2._0.EmbeddedDataSpecificationT
	 * @generated
	 */
	EClass getEmbeddedDataSpecificationT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.EmbeddedDataSpecificationT#getDataSpecificationContent <em>Data Specification Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Specification Content</em>'.
	 * @see io.shell.admin.aas._2._0.EmbeddedDataSpecificationT#getDataSpecificationContent()
	 * @see #getEmbeddedDataSpecificationT()
	 * @generated
	 */
	EReference getEmbeddedDataSpecificationT_DataSpecificationContent();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.EmbeddedDataSpecificationT#getDataSpecification <em>Data Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Specification</em>'.
	 * @see io.shell.admin.aas._2._0.EmbeddedDataSpecificationT#getDataSpecification()
	 * @see #getEmbeddedDataSpecificationT()
	 * @generated
	 */
	EReference getEmbeddedDataSpecificationT_DataSpecification();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.EntityT <em>Entity T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Entity T</em>'.
	 * @see io.shell.admin.aas._2._0.EntityT
	 * @generated
	 */
	EClass getEntityT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.EntityT#getStatements <em>Statements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Statements</em>'.
	 * @see io.shell.admin.aas._2._0.EntityT#getStatements()
	 * @see #getEntityT()
	 * @generated
	 */
	EReference getEntityT_Statements();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.EntityT#getEntityType <em>Entity Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Entity Type</em>'.
	 * @see io.shell.admin.aas._2._0.EntityT#getEntityType()
	 * @see #getEntityT()
	 * @generated
	 */
	EAttribute getEntityT_EntityType();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.EntityT#getAssetRef <em>Asset Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Asset Ref</em>'.
	 * @see io.shell.admin.aas._2._0.EntityT#getAssetRef()
	 * @see #getEntityT()
	 * @generated
	 */
	EReference getEntityT_AssetRef();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.EventAbstractT <em>Event Abstract T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Abstract T</em>'.
	 * @see io.shell.admin.aas._2._0.EventAbstractT
	 * @generated
	 */
	EClass getEventAbstractT();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.FileT <em>File T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>File T</em>'.
	 * @see io.shell.admin.aas._2._0.FileT
	 * @generated
	 */
	EClass getFileT();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.FileT#getMimeType <em>Mime Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mime Type</em>'.
	 * @see io.shell.admin.aas._2._0.FileT#getMimeType()
	 * @see #getFileT()
	 * @generated
	 */
	EAttribute getFileT_MimeType();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.FileT#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see io.shell.admin.aas._2._0.FileT#getValue()
	 * @see #getFileT()
	 * @generated
	 */
	EReference getFileT_Value();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.FormulaT <em>Formula T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Formula T</em>'.
	 * @see io.shell.admin.aas._2._0.FormulaT
	 * @generated
	 */
	EClass getFormulaT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.FormulaT#getDependsOnRefs <em>Depends On Refs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Depends On Refs</em>'.
	 * @see io.shell.admin.aas._2._0.FormulaT#getDependsOnRefs()
	 * @see #getFormulaT()
	 * @generated
	 */
	EReference getFormulaT_DependsOnRefs();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.IdentificationT <em>Identification T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Identification T</em>'.
	 * @see io.shell.admin.aas._2._0.IdentificationT
	 * @generated
	 */
	EClass getIdentificationT();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.IdentificationT#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see io.shell.admin.aas._2._0.IdentificationT#getValue()
	 * @see #getIdentificationT()
	 * @generated
	 */
	EAttribute getIdentificationT_Value();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.IdentificationT#getIdType <em>Id Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id Type</em>'.
	 * @see io.shell.admin.aas._2._0.IdentificationT#getIdType()
	 * @see #getIdentificationT()
	 * @generated
	 */
	EAttribute getIdentificationT_IdType();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.IdentifierT <em>Identifier T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Identifier T</em>'.
	 * @see io.shell.admin.aas._2._0.IdentifierT
	 * @generated
	 */
	EClass getIdentifierT();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.IdentifierT#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see io.shell.admin.aas._2._0.IdentifierT#getId()
	 * @see #getIdentifierT()
	 * @generated
	 */
	EAttribute getIdentifierT_Id();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.IdentifierT#getIdType <em>Id Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id Type</em>'.
	 * @see io.shell.admin.aas._2._0.IdentifierT#getIdType()
	 * @see #getIdentifierT()
	 * @generated
	 */
	EAttribute getIdentifierT_IdType();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.IdPropertyDefinitionT <em>Id Property Definition T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Id Property Definition T</em>'.
	 * @see io.shell.admin.aas._2._0.IdPropertyDefinitionT
	 * @generated
	 */
	EClass getIdPropertyDefinitionT();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.IdPropertyDefinitionT#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see io.shell.admin.aas._2._0.IdPropertyDefinitionT#getValue()
	 * @see #getIdPropertyDefinitionT()
	 * @generated
	 */
	EAttribute getIdPropertyDefinitionT_Value();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.IdPropertyDefinitionT#getIdType <em>Id Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id Type</em>'.
	 * @see io.shell.admin.aas._2._0.IdPropertyDefinitionT#getIdType()
	 * @see #getIdPropertyDefinitionT()
	 * @generated
	 */
	EAttribute getIdPropertyDefinitionT_IdType();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.IdShortT <em>Id Short T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Id Short T</em>'.
	 * @see io.shell.admin.aas._2._0.IdShortT
	 * @generated
	 */
	EClass getIdShortT();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.IdShortT#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see io.shell.admin.aas._2._0.IdShortT#getValue()
	 * @see #getIdShortT()
	 * @generated
	 */
	EAttribute getIdShortT_Value();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.KeysT <em>Keys T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Keys T</em>'.
	 * @see io.shell.admin.aas._2._0.KeysT
	 * @generated
	 */
	EClass getKeysT();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.KeysT#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Key</em>'.
	 * @see io.shell.admin.aas._2._0.KeysT#getKey()
	 * @see #getKeysT()
	 * @generated
	 */
	EReference getKeysT_Key();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.KeyT <em>Key T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Key T</em>'.
	 * @see io.shell.admin.aas._2._0.KeyT
	 * @generated
	 */
	EClass getKeyT();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.KeyT#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see io.shell.admin.aas._2._0.KeyT#getValue()
	 * @see #getKeyT()
	 * @generated
	 */
	EAttribute getKeyT_Value();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.KeyT#getIdType <em>Id Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id Type</em>'.
	 * @see io.shell.admin.aas._2._0.KeyT#getIdType()
	 * @see #getKeyT()
	 * @generated
	 */
	EAttribute getKeyT_IdType();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.KeyT#isLocal <em>Local</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Local</em>'.
	 * @see io.shell.admin.aas._2._0.KeyT#isLocal()
	 * @see #getKeyT()
	 * @generated
	 */
	EAttribute getKeyT_Local();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.KeyT#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see io.shell.admin.aas._2._0.KeyT#getType()
	 * @see #getKeyT()
	 * @generated
	 */
	EAttribute getKeyT_Type();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.LangStringSetT <em>Lang String Set T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lang String Set T</em>'.
	 * @see io.shell.admin.aas._2._0.LangStringSetT
	 * @generated
	 */
	EClass getLangStringSetT();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.LangStringSetT#getLangString <em>Lang String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Lang String</em>'.
	 * @see io.shell.admin.aas._2._0.LangStringSetT#getLangString()
	 * @see #getLangStringSetT()
	 * @generated
	 */
	EReference getLangStringSetT_LangString();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.LangStringT <em>Lang String T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lang String T</em>'.
	 * @see io.shell.admin.aas._2._0.LangStringT
	 * @generated
	 */
	EClass getLangStringT();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.LangStringT#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see io.shell.admin.aas._2._0.LangStringT#getValue()
	 * @see #getLangStringT()
	 * @generated
	 */
	EAttribute getLangStringT_Value();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.LangStringT#getLang <em>Lang</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lang</em>'.
	 * @see io.shell.admin.aas._2._0.LangStringT#getLang()
	 * @see #getLangStringT()
	 * @generated
	 */
	EAttribute getLangStringT_Lang();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.MultiLanguagePropertyT <em>Multi Language Property T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Multi Language Property T</em>'.
	 * @see io.shell.admin.aas._2._0.MultiLanguagePropertyT
	 * @generated
	 */
	EClass getMultiLanguagePropertyT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.MultiLanguagePropertyT#getValueId <em>Value Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value Id</em>'.
	 * @see io.shell.admin.aas._2._0.MultiLanguagePropertyT#getValueId()
	 * @see #getMultiLanguagePropertyT()
	 * @generated
	 */
	EReference getMultiLanguagePropertyT_ValueId();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.MultiLanguagePropertyT#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see io.shell.admin.aas._2._0.MultiLanguagePropertyT#getValue()
	 * @see #getMultiLanguagePropertyT()
	 * @generated
	 */
	EReference getMultiLanguagePropertyT_Value();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.OperationT <em>Operation T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation T</em>'.
	 * @see io.shell.admin.aas._2._0.OperationT
	 * @generated
	 */
	EClass getOperationT();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.OperationT#getInputVariable <em>Input Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Input Variable</em>'.
	 * @see io.shell.admin.aas._2._0.OperationT#getInputVariable()
	 * @see #getOperationT()
	 * @generated
	 */
	EReference getOperationT_InputVariable();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.OperationT#getOutputVariable <em>Output Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Output Variable</em>'.
	 * @see io.shell.admin.aas._2._0.OperationT#getOutputVariable()
	 * @see #getOperationT()
	 * @generated
	 */
	EReference getOperationT_OutputVariable();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.OperationT#getInoutputVariable <em>Inoutput Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Inoutput Variable</em>'.
	 * @see io.shell.admin.aas._2._0.OperationT#getInoutputVariable()
	 * @see #getOperationT()
	 * @generated
	 */
	EReference getOperationT_InoutputVariable();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.OperationVariableT <em>Operation Variable T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation Variable T</em>'.
	 * @see io.shell.admin.aas._2._0.OperationVariableT
	 * @generated
	 */
	EClass getOperationVariableT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.OperationVariableT#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see io.shell.admin.aas._2._0.OperationVariableT#getValue()
	 * @see #getOperationVariableT()
	 * @generated
	 */
	EReference getOperationVariableT_Value();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.PathTypeT <em>Path Type T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Path Type T</em>'.
	 * @see io.shell.admin.aas._2._0.PathTypeT
	 * @generated
	 */
	EClass getPathTypeT();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.PathTypeT#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see io.shell.admin.aas._2._0.PathTypeT#getValue()
	 * @see #getPathTypeT()
	 * @generated
	 */
	EAttribute getPathTypeT_Value();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.PropertyT <em>Property T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property T</em>'.
	 * @see io.shell.admin.aas._2._0.PropertyT
	 * @generated
	 */
	EClass getPropertyT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.PropertyT#getValueType <em>Value Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value Type</em>'.
	 * @see io.shell.admin.aas._2._0.PropertyT#getValueType()
	 * @see #getPropertyT()
	 * @generated
	 */
	EReference getPropertyT_ValueType();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.PropertyT#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see io.shell.admin.aas._2._0.PropertyT#getValue()
	 * @see #getPropertyT()
	 * @generated
	 */
	EReference getPropertyT_Value();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.PropertyT#getValueId <em>Value Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value Id</em>'.
	 * @see io.shell.admin.aas._2._0.PropertyT#getValueId()
	 * @see #getPropertyT()
	 * @generated
	 */
	EReference getPropertyT_ValueId();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.QualifierT <em>Qualifier T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Qualifier T</em>'.
	 * @see io.shell.admin.aas._2._0.QualifierT
	 * @generated
	 */
	EClass getQualifierT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.QualifierT#getSemanticId <em>Semantic Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Semantic Id</em>'.
	 * @see io.shell.admin.aas._2._0.QualifierT#getSemanticId()
	 * @see #getQualifierT()
	 * @generated
	 */
	EReference getQualifierT_SemanticId();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.QualifierT#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see io.shell.admin.aas._2._0.QualifierT#getType()
	 * @see #getQualifierT()
	 * @generated
	 */
	EReference getQualifierT_Type();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.QualifierT#getValueType <em>Value Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value Type</em>'.
	 * @see io.shell.admin.aas._2._0.QualifierT#getValueType()
	 * @see #getQualifierT()
	 * @generated
	 */
	EReference getQualifierT_ValueType();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.QualifierT#getValueId <em>Value Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value Id</em>'.
	 * @see io.shell.admin.aas._2._0.QualifierT#getValueId()
	 * @see #getQualifierT()
	 * @generated
	 */
	EReference getQualifierT_ValueId();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.QualifierT#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see io.shell.admin.aas._2._0.QualifierT#getValue()
	 * @see #getQualifierT()
	 * @generated
	 */
	EReference getQualifierT_Value();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.QualifierTypeT <em>Qualifier Type T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Qualifier Type T</em>'.
	 * @see io.shell.admin.aas._2._0.QualifierTypeT
	 * @generated
	 */
	EClass getQualifierTypeT();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.QualifierTypeT#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see io.shell.admin.aas._2._0.QualifierTypeT#getValue()
	 * @see #getQualifierTypeT()
	 * @generated
	 */
	EAttribute getQualifierTypeT_Value();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.RangeT <em>Range T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Range T</em>'.
	 * @see io.shell.admin.aas._2._0.RangeT
	 * @generated
	 */
	EClass getRangeT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.RangeT#getValueType <em>Value Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value Type</em>'.
	 * @see io.shell.admin.aas._2._0.RangeT#getValueType()
	 * @see #getRangeT()
	 * @generated
	 */
	EReference getRangeT_ValueType();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.RangeT#getMin <em>Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Min</em>'.
	 * @see io.shell.admin.aas._2._0.RangeT#getMin()
	 * @see #getRangeT()
	 * @generated
	 */
	EReference getRangeT_Min();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.RangeT#getMax <em>Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Max</em>'.
	 * @see io.shell.admin.aas._2._0.RangeT#getMax()
	 * @see #getRangeT()
	 * @generated
	 */
	EReference getRangeT_Max();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.ReferenceElementT <em>Reference Element T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference Element T</em>'.
	 * @see io.shell.admin.aas._2._0.ReferenceElementT
	 * @generated
	 */
	EClass getReferenceElementT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.ReferenceElementT#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see io.shell.admin.aas._2._0.ReferenceElementT#getValue()
	 * @see #getReferenceElementT()
	 * @generated
	 */
	EReference getReferenceElementT_Value();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.ReferencesT <em>References T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>References T</em>'.
	 * @see io.shell.admin.aas._2._0.ReferencesT
	 * @generated
	 */
	EClass getReferencesT();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.ReferencesT#getReference <em>Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reference</em>'.
	 * @see io.shell.admin.aas._2._0.ReferencesT#getReference()
	 * @see #getReferencesT()
	 * @generated
	 */
	EReference getReferencesT_Reference();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.ReferenceT <em>Reference T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference T</em>'.
	 * @see io.shell.admin.aas._2._0.ReferenceT
	 * @generated
	 */
	EClass getReferenceT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.ReferenceT#getKeys <em>Keys</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Keys</em>'.
	 * @see io.shell.admin.aas._2._0.ReferenceT#getKeys()
	 * @see #getReferenceT()
	 * @generated
	 */
	EReference getReferenceT_Keys();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.RelationshipElementT <em>Relationship Element T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Relationship Element T</em>'.
	 * @see io.shell.admin.aas._2._0.RelationshipElementT
	 * @generated
	 */
	EClass getRelationshipElementT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.RelationshipElementT#getFirst <em>First</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>First</em>'.
	 * @see io.shell.admin.aas._2._0.RelationshipElementT#getFirst()
	 * @see #getRelationshipElementT()
	 * @generated
	 */
	EReference getRelationshipElementT_First();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.RelationshipElementT#getSecond <em>Second</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Second</em>'.
	 * @see io.shell.admin.aas._2._0.RelationshipElementT#getSecond()
	 * @see #getRelationshipElementT()
	 * @generated
	 */
	EReference getRelationshipElementT_Second();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.SemanticIdT <em>Semantic Id T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Semantic Id T</em>'.
	 * @see io.shell.admin.aas._2._0.SemanticIdT
	 * @generated
	 */
	EClass getSemanticIdT();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.SubmodelElementAbstractT <em>Submodel Element Abstract T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Submodel Element Abstract T</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementAbstractT
	 * @generated
	 */
	EClass getSubmodelElementAbstractT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.SubmodelElementAbstractT#getIdShort <em>Id Short</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Id Short</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementAbstractT#getIdShort()
	 * @see #getSubmodelElementAbstractT()
	 * @generated
	 */
	EReference getSubmodelElementAbstractT_IdShort();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.SubmodelElementAbstractT#getCategory <em>Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Category</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementAbstractT#getCategory()
	 * @see #getSubmodelElementAbstractT()
	 * @generated
	 */
	EAttribute getSubmodelElementAbstractT_Category();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.SubmodelElementAbstractT#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Description</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementAbstractT#getDescription()
	 * @see #getSubmodelElementAbstractT()
	 * @generated
	 */
	EReference getSubmodelElementAbstractT_Description();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.SubmodelElementAbstractT#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parent</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementAbstractT#getParent()
	 * @see #getSubmodelElementAbstractT()
	 * @generated
	 */
	EReference getSubmodelElementAbstractT_Parent();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.SubmodelElementAbstractT#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementAbstractT#getKind()
	 * @see #getSubmodelElementAbstractT()
	 * @generated
	 */
	EAttribute getSubmodelElementAbstractT_Kind();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.SubmodelElementAbstractT#getSemanticId <em>Semantic Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Semantic Id</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementAbstractT#getSemanticId()
	 * @see #getSubmodelElementAbstractT()
	 * @generated
	 */
	EReference getSubmodelElementAbstractT_SemanticId();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.SubmodelElementAbstractT#getQualifier <em>Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Qualifier</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementAbstractT#getQualifier()
	 * @see #getSubmodelElementAbstractT()
	 * @generated
	 */
	EReference getSubmodelElementAbstractT_Qualifier();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.SubmodelElementAbstractT#getEmbeddedDataSpecification <em>Embedded Data Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Embedded Data Specification</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementAbstractT#getEmbeddedDataSpecification()
	 * @see #getSubmodelElementAbstractT()
	 * @generated
	 */
	EReference getSubmodelElementAbstractT_EmbeddedDataSpecification();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.SubmodelElementCollectionT <em>Submodel Element Collection T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Submodel Element Collection T</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementCollectionT
	 * @generated
	 */
	EClass getSubmodelElementCollectionT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.SubmodelElementCollectionT#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementCollectionT#getValue()
	 * @see #getSubmodelElementCollectionT()
	 * @generated
	 */
	EReference getSubmodelElementCollectionT_Value();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.SubmodelElementCollectionT#isOrdered <em>Ordered</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ordered</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementCollectionT#isOrdered()
	 * @see #getSubmodelElementCollectionT()
	 * @generated
	 */
	EAttribute getSubmodelElementCollectionT_Ordered();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.SubmodelElementCollectionT#isAllowDuplicates <em>Allow Duplicates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Allow Duplicates</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementCollectionT#isAllowDuplicates()
	 * @see #getSubmodelElementCollectionT()
	 * @generated
	 */
	EAttribute getSubmodelElementCollectionT_AllowDuplicates();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.SubmodelElementsT <em>Submodel Elements T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Submodel Elements T</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementsT
	 * @generated
	 */
	EClass getSubmodelElementsT();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.SubmodelElementsT#getSubmodelElement <em>Submodel Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Submodel Element</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementsT#getSubmodelElement()
	 * @see #getSubmodelElementsT()
	 * @generated
	 */
	EReference getSubmodelElementsT_SubmodelElement();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.SubmodelElementT <em>Submodel Element T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Submodel Element T</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementT
	 * @generated
	 */
	EClass getSubmodelElementT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.SubmodelElementT#getMultiLanguageProperty <em>Multi Language Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Multi Language Property</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementT#getMultiLanguageProperty()
	 * @see #getSubmodelElementT()
	 * @generated
	 */
	EReference getSubmodelElementT_MultiLanguageProperty();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.SubmodelElementT#getProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Property</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementT#getProperty()
	 * @see #getSubmodelElementT()
	 * @generated
	 */
	EReference getSubmodelElementT_Property();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.SubmodelElementT#getRange <em>Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Range</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementT#getRange()
	 * @see #getSubmodelElementT()
	 * @generated
	 */
	EReference getSubmodelElementT_Range();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.SubmodelElementT#getBlob <em>Blob</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Blob</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementT#getBlob()
	 * @see #getSubmodelElementT()
	 * @generated
	 */
	EReference getSubmodelElementT_Blob();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.SubmodelElementT#getFile <em>File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>File</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementT#getFile()
	 * @see #getSubmodelElementT()
	 * @generated
	 */
	EReference getSubmodelElementT_File();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.SubmodelElementT#getReferenceElement <em>Reference Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reference Element</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementT#getReferenceElement()
	 * @see #getSubmodelElementT()
	 * @generated
	 */
	EReference getSubmodelElementT_ReferenceElement();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.SubmodelElementT#getAnnotatedRelationshipElement <em>Annotated Relationship Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Annotated Relationship Element</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementT#getAnnotatedRelationshipElement()
	 * @see #getSubmodelElementT()
	 * @generated
	 */
	EReference getSubmodelElementT_AnnotatedRelationshipElement();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.SubmodelElementT#getBasicEvent <em>Basic Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Basic Event</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementT#getBasicEvent()
	 * @see #getSubmodelElementT()
	 * @generated
	 */
	EReference getSubmodelElementT_BasicEvent();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.SubmodelElementT#getCapability <em>Capability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Capability</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementT#getCapability()
	 * @see #getSubmodelElementT()
	 * @generated
	 */
	EReference getSubmodelElementT_Capability();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.SubmodelElementT#getEntity <em>Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Entity</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementT#getEntity()
	 * @see #getSubmodelElementT()
	 * @generated
	 */
	EReference getSubmodelElementT_Entity();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.SubmodelElementT#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operation</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementT#getOperation()
	 * @see #getSubmodelElementT()
	 * @generated
	 */
	EReference getSubmodelElementT_Operation();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.SubmodelElementT#getRelationshipElement <em>Relationship Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Relationship Element</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementT#getRelationshipElement()
	 * @see #getSubmodelElementT()
	 * @generated
	 */
	EReference getSubmodelElementT_RelationshipElement();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.SubmodelElementT#getSubmodelElementCollection <em>Submodel Element Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Submodel Element Collection</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelElementT#getSubmodelElementCollection()
	 * @see #getSubmodelElementT()
	 * @generated
	 */
	EReference getSubmodelElementT_SubmodelElementCollection();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.SubmodelRefsT <em>Submodel Refs T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Submodel Refs T</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelRefsT
	 * @generated
	 */
	EClass getSubmodelRefsT();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.SubmodelRefsT#getSubmodelRef <em>Submodel Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Submodel Ref</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelRefsT#getSubmodelRef()
	 * @see #getSubmodelRefsT()
	 * @generated
	 */
	EReference getSubmodelRefsT_SubmodelRef();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.SubmodelsT <em>Submodels T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Submodels T</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelsT
	 * @generated
	 */
	EClass getSubmodelsT();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.SubmodelsT#getSubmodel <em>Submodel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Submodel</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelsT#getSubmodel()
	 * @see #getSubmodelsT()
	 * @generated
	 */
	EReference getSubmodelsT_Submodel();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.SubmodelT <em>Submodel T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Submodel T</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelT
	 * @generated
	 */
	EClass getSubmodelT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.SubmodelT#getIdShort <em>Id Short</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Id Short</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelT#getIdShort()
	 * @see #getSubmodelT()
	 * @generated
	 */
	EReference getSubmodelT_IdShort();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.SubmodelT#getCategory <em>Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Category</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelT#getCategory()
	 * @see #getSubmodelT()
	 * @generated
	 */
	EAttribute getSubmodelT_Category();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.SubmodelT#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Description</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelT#getDescription()
	 * @see #getSubmodelT()
	 * @generated
	 */
	EReference getSubmodelT_Description();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.SubmodelT#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parent</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelT#getParent()
	 * @see #getSubmodelT()
	 * @generated
	 */
	EReference getSubmodelT_Parent();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.SubmodelT#getIdentification <em>Identification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Identification</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelT#getIdentification()
	 * @see #getSubmodelT()
	 * @generated
	 */
	EReference getSubmodelT_Identification();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.SubmodelT#getAdministration <em>Administration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Administration</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelT#getAdministration()
	 * @see #getSubmodelT()
	 * @generated
	 */
	EReference getSubmodelT_Administration();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.SubmodelT#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelT#getKind()
	 * @see #getSubmodelT()
	 * @generated
	 */
	EAttribute getSubmodelT_Kind();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.SubmodelT#getSemanticId <em>Semantic Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Semantic Id</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelT#getSemanticId()
	 * @see #getSubmodelT()
	 * @generated
	 */
	EReference getSubmodelT_SemanticId();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.SubmodelT#getQualifier <em>Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Qualifier</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelT#getQualifier()
	 * @see #getSubmodelT()
	 * @generated
	 */
	EReference getSubmodelT_Qualifier();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.SubmodelT#getEmbeddedDataSpecification <em>Embedded Data Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Embedded Data Specification</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelT#getEmbeddedDataSpecification()
	 * @see #getSubmodelT()
	 * @generated
	 */
	EReference getSubmodelT_EmbeddedDataSpecification();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.SubmodelT#getSubmodelElements <em>Submodel Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Submodel Elements</em>'.
	 * @see io.shell.admin.aas._2._0.SubmodelT#getSubmodelElements()
	 * @see #getSubmodelT()
	 * @generated
	 */
	EReference getSubmodelT_SubmodelElements();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.ValueDataTypeT <em>Value Data Type T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value Data Type T</em>'.
	 * @see io.shell.admin.aas._2._0.ValueDataTypeT
	 * @generated
	 */
	EClass getValueDataTypeT();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.ValueDataTypeT#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see io.shell.admin.aas._2._0.ValueDataTypeT#getValue()
	 * @see #getValueDataTypeT()
	 * @generated
	 */
	EAttribute getValueDataTypeT_Value();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.ViewsT <em>Views T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Views T</em>'.
	 * @see io.shell.admin.aas._2._0.ViewsT
	 * @generated
	 */
	EClass getViewsT();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.ViewsT#getView <em>View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>View</em>'.
	 * @see io.shell.admin.aas._2._0.ViewsT#getView()
	 * @see #getViewsT()
	 * @generated
	 */
	EReference getViewsT_View();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas._2._0.ViewT <em>View T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>View T</em>'.
	 * @see io.shell.admin.aas._2._0.ViewT
	 * @generated
	 */
	EClass getViewT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.ViewT#getIdShort <em>Id Short</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Id Short</em>'.
	 * @see io.shell.admin.aas._2._0.ViewT#getIdShort()
	 * @see #getViewT()
	 * @generated
	 */
	EReference getViewT_IdShort();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas._2._0.ViewT#getCategory <em>Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Category</em>'.
	 * @see io.shell.admin.aas._2._0.ViewT#getCategory()
	 * @see #getViewT()
	 * @generated
	 */
	EAttribute getViewT_Category();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.ViewT#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Description</em>'.
	 * @see io.shell.admin.aas._2._0.ViewT#getDescription()
	 * @see #getViewT()
	 * @generated
	 */
	EReference getViewT_Description();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.ViewT#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parent</em>'.
	 * @see io.shell.admin.aas._2._0.ViewT#getParent()
	 * @see #getViewT()
	 * @generated
	 */
	EReference getViewT_Parent();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.ViewT#getSemanticId <em>Semantic Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Semantic Id</em>'.
	 * @see io.shell.admin.aas._2._0.ViewT#getSemanticId()
	 * @see #getViewT()
	 * @generated
	 */
	EReference getViewT_SemanticId();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas._2._0.ViewT#getEmbeddedDataSpecification <em>Embedded Data Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Embedded Data Specification</em>'.
	 * @see io.shell.admin.aas._2._0.ViewT#getEmbeddedDataSpecification()
	 * @see #getViewT()
	 * @generated
	 */
	EReference getViewT_EmbeddedDataSpecification();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas._2._0.ViewT#getContainedElements <em>Contained Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Contained Elements</em>'.
	 * @see io.shell.admin.aas._2._0.ViewT#getContainedElements()
	 * @see #getViewT()
	 * @generated
	 */
	EReference getViewT_ContainedElements();

	/**
	 * Returns the meta object for enum '{@link io.shell.admin.aas._2._0.AssetKindT <em>Asset Kind T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Asset Kind T</em>'.
	 * @see io.shell.admin.aas._2._0.AssetKindT
	 * @generated
	 */
	EEnum getAssetKindT();

	/**
	 * Returns the meta object for enum '{@link io.shell.admin.aas._2._0.EntityTypeType <em>Entity Type Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Entity Type Type</em>'.
	 * @see io.shell.admin.aas._2._0.EntityTypeType
	 * @generated
	 */
	EEnum getEntityTypeType();

	/**
	 * Returns the meta object for enum '{@link io.shell.admin.aas._2._0.IdentifierTypeT <em>Identifier Type T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Identifier Type T</em>'.
	 * @see io.shell.admin.aas._2._0.IdentifierTypeT
	 * @generated
	 */
	EEnum getIdentifierTypeT();

	/**
	 * Returns the meta object for enum '{@link io.shell.admin.aas._2._0.IdTypeType <em>Id Type Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Id Type Type</em>'.
	 * @see io.shell.admin.aas._2._0.IdTypeType
	 * @generated
	 */
	EEnum getIdTypeType();

	/**
	 * Returns the meta object for enum '{@link io.shell.admin.aas._2._0.IdTypeType1 <em>Id Type Type1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Id Type Type1</em>'.
	 * @see io.shell.admin.aas._2._0.IdTypeType1
	 * @generated
	 */
	EEnum getIdTypeType1();

	/**
	 * Returns the meta object for enum '{@link io.shell.admin.aas._2._0.ModelingKindT <em>Modeling Kind T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Modeling Kind T</em>'.
	 * @see io.shell.admin.aas._2._0.ModelingKindT
	 * @generated
	 */
	EEnum getModelingKindT();

	/**
	 * Returns the meta object for enum '{@link io.shell.admin.aas._2._0.TypeType <em>Type Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Type Type</em>'.
	 * @see io.shell.admin.aas._2._0.TypeType
	 * @generated
	 */
	EEnum getTypeType();

	/**
	 * Returns the meta object for data type '{@link io.shell.admin.aas._2._0.AssetKindT <em>Asset Kind TObject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Asset Kind TObject</em>'.
	 * @see io.shell.admin.aas._2._0.AssetKindT
	 * @model instanceClass="io.shell.admin.aas._2._0.AssetKindT"
	 *        extendedMetaData="name='assetKind_t:Object' baseType='assetKind_t'"
	 * @generated
	 */
	EDataType getAssetKindTObject();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Entity Type T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Entity Type T</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 *        extendedMetaData="name='entityType_t' baseType='http://www.eclipse.org/emf/2003/XMLType#string'"
	 * @generated
	 */
	EDataType getEntityTypeT();

	/**
	 * Returns the meta object for data type '{@link io.shell.admin.aas._2._0.EntityTypeType <em>Entity Type Type Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Entity Type Type Object</em>'.
	 * @see io.shell.admin.aas._2._0.EntityTypeType
	 * @model instanceClass="io.shell.admin.aas._2._0.EntityTypeType"
	 *        extendedMetaData="name='entityType_._type:Object' baseType='entityType_._type'"
	 * @generated
	 */
	EDataType getEntityTypeTypeObject();

	/**
	 * Returns the meta object for data type '{@link io.shell.admin.aas._2._0.IdentifierTypeT <em>Identifier Type TObject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Identifier Type TObject</em>'.
	 * @see io.shell.admin.aas._2._0.IdentifierTypeT
	 * @model instanceClass="io.shell.admin.aas._2._0.IdentifierTypeT"
	 *        extendedMetaData="name='identifierType_t:Object' baseType='identifierType_t'"
	 * @generated
	 */
	EDataType getIdentifierTypeTObject();

	/**
	 * Returns the meta object for data type '{@link io.shell.admin.aas._2._0.IdTypeType <em>Id Type Type Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Id Type Type Object</em>'.
	 * @see io.shell.admin.aas._2._0.IdTypeType
	 * @model instanceClass="io.shell.admin.aas._2._0.IdTypeType"
	 *        extendedMetaData="name='idType_._type:Object' baseType='idType_._type'"
	 * @generated
	 */
	EDataType getIdTypeTypeObject();

	/**
	 * Returns the meta object for data type '{@link io.shell.admin.aas._2._0.IdTypeType1 <em>Id Type Type Object1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Id Type Type Object1</em>'.
	 * @see io.shell.admin.aas._2._0.IdTypeType1
	 * @model instanceClass="io.shell.admin.aas._2._0.IdTypeType1"
	 *        extendedMetaData="name='idType_._1_._type:Object' baseType='idType_._1_._type'"
	 * @generated
	 */
	EDataType getIdTypeTypeObject1();

	/**
	 * Returns the meta object for data type '{@link io.shell.admin.aas._2._0.ModelingKindT <em>Modeling Kind TObject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Modeling Kind TObject</em>'.
	 * @see io.shell.admin.aas._2._0.ModelingKindT
	 * @model instanceClass="io.shell.admin.aas._2._0.ModelingKindT"
	 *        extendedMetaData="name='modelingKind_t:Object' baseType='modelingKind_t'"
	 * @generated
	 */
	EDataType getModelingKindTObject();

	/**
	 * Returns the meta object for data type '{@link io.shell.admin.aas._2._0.TypeType <em>Type Type Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Type Type Object</em>'.
	 * @see io.shell.admin.aas._2._0.TypeType
	 * @model instanceClass="io.shell.admin.aas._2._0.TypeType"
	 *        extendedMetaData="name='type_._type:Object' baseType='type_._type'"
	 * @generated
	 */
	EDataType getTypeTypeObject();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	_0Factory get_0Factory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.AasenvTImpl <em>Aasenv T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.AasenvTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getAasenvT()
		 * @generated
		 */
		EClass AASENV_T = eINSTANCE.getAasenvT();

		/**
		 * The meta object literal for the '<em><b>Asset Administration Shells</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AASENV_T__ASSET_ADMINISTRATION_SHELLS = eINSTANCE.getAasenvT_AssetAdministrationShells();

		/**
		 * The meta object literal for the '<em><b>Assets</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AASENV_T__ASSETS = eINSTANCE.getAasenvT_Assets();

		/**
		 * The meta object literal for the '<em><b>Submodels</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AASENV_T__SUBMODELS = eINSTANCE.getAasenvT_Submodels();

		/**
		 * The meta object literal for the '<em><b>Concept Descriptions</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AASENV_T__CONCEPT_DESCRIPTIONS = eINSTANCE.getAasenvT_ConceptDescriptions();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.AdministrationTImpl <em>Administration T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.AdministrationTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getAdministrationT()
		 * @generated
		 */
		EClass ADMINISTRATION_T = eINSTANCE.getAdministrationT();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ADMINISTRATION_T__VERSION = eINSTANCE.getAdministrationT_Version();

		/**
		 * The meta object literal for the '<em><b>Revision</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ADMINISTRATION_T__REVISION = eINSTANCE.getAdministrationT_Revision();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.AnnotatedRelationshipElementTImpl <em>Annotated Relationship Element T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.AnnotatedRelationshipElementTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getAnnotatedRelationshipElementT()
		 * @generated
		 */
		EClass ANNOTATED_RELATIONSHIP_ELEMENT_T = eINSTANCE.getAnnotatedRelationshipElementT();

		/**
		 * The meta object literal for the '<em><b>Annotations</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANNOTATED_RELATIONSHIP_ELEMENT_T__ANNOTATIONS = eINSTANCE.getAnnotatedRelationshipElementT_Annotations();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.AssetAdministrationShellsTImpl <em>Asset Administration Shells T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.AssetAdministrationShellsTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getAssetAdministrationShellsT()
		 * @generated
		 */
		EClass ASSET_ADMINISTRATION_SHELLS_T = eINSTANCE.getAssetAdministrationShellsT();

		/**
		 * The meta object literal for the '<em><b>Asset Administration Shell</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_ADMINISTRATION_SHELLS_T__ASSET_ADMINISTRATION_SHELL = eINSTANCE.getAssetAdministrationShellsT_AssetAdministrationShell();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.AssetAdministrationShellTImpl <em>Asset Administration Shell T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.AssetAdministrationShellTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getAssetAdministrationShellT()
		 * @generated
		 */
		EClass ASSET_ADMINISTRATION_SHELL_T = eINSTANCE.getAssetAdministrationShellT();

		/**
		 * The meta object literal for the '<em><b>Id Short</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_ADMINISTRATION_SHELL_T__ID_SHORT = eINSTANCE.getAssetAdministrationShellT_IdShort();

		/**
		 * The meta object literal for the '<em><b>Category</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET_ADMINISTRATION_SHELL_T__CATEGORY = eINSTANCE.getAssetAdministrationShellT_Category();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_ADMINISTRATION_SHELL_T__DESCRIPTION = eINSTANCE.getAssetAdministrationShellT_Description();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_ADMINISTRATION_SHELL_T__PARENT = eINSTANCE.getAssetAdministrationShellT_Parent();

		/**
		 * The meta object literal for the '<em><b>Identification</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_ADMINISTRATION_SHELL_T__IDENTIFICATION = eINSTANCE.getAssetAdministrationShellT_Identification();

		/**
		 * The meta object literal for the '<em><b>Administration</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_ADMINISTRATION_SHELL_T__ADMINISTRATION = eINSTANCE.getAssetAdministrationShellT_Administration();

		/**
		 * The meta object literal for the '<em><b>Embedded Data Specification</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_ADMINISTRATION_SHELL_T__EMBEDDED_DATA_SPECIFICATION = eINSTANCE.getAssetAdministrationShellT_EmbeddedDataSpecification();

		/**
		 * The meta object literal for the '<em><b>Derived From</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_ADMINISTRATION_SHELL_T__DERIVED_FROM = eINSTANCE.getAssetAdministrationShellT_DerivedFrom();

		/**
		 * The meta object literal for the '<em><b>Asset Ref</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_ADMINISTRATION_SHELL_T__ASSET_REF = eINSTANCE.getAssetAdministrationShellT_AssetRef();

		/**
		 * The meta object literal for the '<em><b>Submodel Refs</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_ADMINISTRATION_SHELL_T__SUBMODEL_REFS = eINSTANCE.getAssetAdministrationShellT_SubmodelRefs();

		/**
		 * The meta object literal for the '<em><b>Views</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_ADMINISTRATION_SHELL_T__VIEWS = eINSTANCE.getAssetAdministrationShellT_Views();

		/**
		 * The meta object literal for the '<em><b>Concept Dictionaries</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_ADMINISTRATION_SHELL_T__CONCEPT_DICTIONARIES = eINSTANCE.getAssetAdministrationShellT_ConceptDictionaries();

		/**
		 * The meta object literal for the '<em><b>Security</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_ADMINISTRATION_SHELL_T__SECURITY = eINSTANCE.getAssetAdministrationShellT_Security();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.AssetsTImpl <em>Assets T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.AssetsTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getAssetsT()
		 * @generated
		 */
		EClass ASSETS_T = eINSTANCE.getAssetsT();

		/**
		 * The meta object literal for the '<em><b>Asset</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSETS_T__ASSET = eINSTANCE.getAssetsT_Asset();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.AssetTImpl <em>Asset T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.AssetTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getAssetT()
		 * @generated
		 */
		EClass ASSET_T = eINSTANCE.getAssetT();

		/**
		 * The meta object literal for the '<em><b>Id Short</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_T__ID_SHORT = eINSTANCE.getAssetT_IdShort();

		/**
		 * The meta object literal for the '<em><b>Category</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET_T__CATEGORY = eINSTANCE.getAssetT_Category();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_T__DESCRIPTION = eINSTANCE.getAssetT_Description();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_T__PARENT = eINSTANCE.getAssetT_Parent();

		/**
		 * The meta object literal for the '<em><b>Identification</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_T__IDENTIFICATION = eINSTANCE.getAssetT_Identification();

		/**
		 * The meta object literal for the '<em><b>Administration</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_T__ADMINISTRATION = eINSTANCE.getAssetT_Administration();

		/**
		 * The meta object literal for the '<em><b>Embedded Data Specification</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_T__EMBEDDED_DATA_SPECIFICATION = eINSTANCE.getAssetT_EmbeddedDataSpecification();

		/**
		 * The meta object literal for the '<em><b>Asset Identification Model Ref</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_T__ASSET_IDENTIFICATION_MODEL_REF = eINSTANCE.getAssetT_AssetIdentificationModelRef();

		/**
		 * The meta object literal for the '<em><b>Bill Of Material Ref</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET_T__BILL_OF_MATERIAL_REF = eINSTANCE.getAssetT_BillOfMaterialRef();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET_T__KIND = eINSTANCE.getAssetT_Kind();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.BasicEventTImpl <em>Basic Event T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.BasicEventTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getBasicEventT()
		 * @generated
		 */
		EClass BASIC_EVENT_T = eINSTANCE.getBasicEventT();

		/**
		 * The meta object literal for the '<em><b>Observed</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASIC_EVENT_T__OBSERVED = eINSTANCE.getBasicEventT_Observed();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.BlobTImpl <em>Blob T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.BlobTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getBlobT()
		 * @generated
		 */
		EClass BLOB_T = eINSTANCE.getBlobT();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOB_T__VALUE = eINSTANCE.getBlobT_Value();

		/**
		 * The meta object literal for the '<em><b>Mime Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLOB_T__MIME_TYPE = eINSTANCE.getBlobT_MimeType();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.BlobTypeTImpl <em>Blob Type T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.BlobTypeTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getBlobTypeT()
		 * @generated
		 */
		EClass BLOB_TYPE_T = eINSTANCE.getBlobTypeT();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLOB_TYPE_T__VALUE = eINSTANCE.getBlobTypeT_Value();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.ConceptDescriptionRefsTImpl <em>Concept Description Refs T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.ConceptDescriptionRefsTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getConceptDescriptionRefsT()
		 * @generated
		 */
		EClass CONCEPT_DESCRIPTION_REFS_T = eINSTANCE.getConceptDescriptionRefsT();

		/**
		 * The meta object literal for the '<em><b>Concept Description Ref</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONCEPT_DESCRIPTION_REFS_T__CONCEPT_DESCRIPTION_REF = eINSTANCE.getConceptDescriptionRefsT_ConceptDescriptionRef();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.ConceptDescriptionsTImpl <em>Concept Descriptions T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.ConceptDescriptionsTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getConceptDescriptionsT()
		 * @generated
		 */
		EClass CONCEPT_DESCRIPTIONS_T = eINSTANCE.getConceptDescriptionsT();

		/**
		 * The meta object literal for the '<em><b>Concept Description</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONCEPT_DESCRIPTIONS_T__CONCEPT_DESCRIPTION = eINSTANCE.getConceptDescriptionsT_ConceptDescription();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.ConceptDescriptionTImpl <em>Concept Description T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.ConceptDescriptionTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getConceptDescriptionT()
		 * @generated
		 */
		EClass CONCEPT_DESCRIPTION_T = eINSTANCE.getConceptDescriptionT();

		/**
		 * The meta object literal for the '<em><b>Id Short</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONCEPT_DESCRIPTION_T__ID_SHORT = eINSTANCE.getConceptDescriptionT_IdShort();

		/**
		 * The meta object literal for the '<em><b>Category</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONCEPT_DESCRIPTION_T__CATEGORY = eINSTANCE.getConceptDescriptionT_Category();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONCEPT_DESCRIPTION_T__DESCRIPTION = eINSTANCE.getConceptDescriptionT_Description();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONCEPT_DESCRIPTION_T__PARENT = eINSTANCE.getConceptDescriptionT_Parent();

		/**
		 * The meta object literal for the '<em><b>Identification</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONCEPT_DESCRIPTION_T__IDENTIFICATION = eINSTANCE.getConceptDescriptionT_Identification();

		/**
		 * The meta object literal for the '<em><b>Administration</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONCEPT_DESCRIPTION_T__ADMINISTRATION = eINSTANCE.getConceptDescriptionT_Administration();

		/**
		 * The meta object literal for the '<em><b>Embedded Data Specification</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONCEPT_DESCRIPTION_T__EMBEDDED_DATA_SPECIFICATION = eINSTANCE.getConceptDescriptionT_EmbeddedDataSpecification();

		/**
		 * The meta object literal for the '<em><b>Is Case Of</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONCEPT_DESCRIPTION_T__IS_CASE_OF = eINSTANCE.getConceptDescriptionT_IsCaseOf();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.ConceptDictionariesTImpl <em>Concept Dictionaries T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.ConceptDictionariesTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getConceptDictionariesT()
		 * @generated
		 */
		EClass CONCEPT_DICTIONARIES_T = eINSTANCE.getConceptDictionariesT();

		/**
		 * The meta object literal for the '<em><b>Concept Dictionary</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONCEPT_DICTIONARIES_T__CONCEPT_DICTIONARY = eINSTANCE.getConceptDictionariesT_ConceptDictionary();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.ConceptDictionaryTImpl <em>Concept Dictionary T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.ConceptDictionaryTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getConceptDictionaryT()
		 * @generated
		 */
		EClass CONCEPT_DICTIONARY_T = eINSTANCE.getConceptDictionaryT();

		/**
		 * The meta object literal for the '<em><b>Id Short</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONCEPT_DICTIONARY_T__ID_SHORT = eINSTANCE.getConceptDictionaryT_IdShort();

		/**
		 * The meta object literal for the '<em><b>Category</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONCEPT_DICTIONARY_T__CATEGORY = eINSTANCE.getConceptDictionaryT_Category();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONCEPT_DICTIONARY_T__DESCRIPTION = eINSTANCE.getConceptDictionaryT_Description();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONCEPT_DICTIONARY_T__PARENT = eINSTANCE.getConceptDictionaryT_Parent();

		/**
		 * The meta object literal for the '<em><b>Concept Description Refs</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONCEPT_DICTIONARY_T__CONCEPT_DESCRIPTION_REFS = eINSTANCE.getConceptDictionaryT_ConceptDescriptionRefs();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.ConstraintTImpl <em>Constraint T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.ConstraintTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getConstraintT()
		 * @generated
		 */
		EClass CONSTRAINT_T = eINSTANCE.getConstraintT();

		/**
		 * The meta object literal for the '<em><b>Formula</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTRAINT_T__FORMULA = eINSTANCE.getConstraintT_Formula();

		/**
		 * The meta object literal for the '<em><b>Qualifier</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTRAINT_T__QUALIFIER = eINSTANCE.getConstraintT_Qualifier();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.ContainedElementsTImpl <em>Contained Elements T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.ContainedElementsTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getContainedElementsT()
		 * @generated
		 */
		EClass CONTAINED_ELEMENTS_T = eINSTANCE.getContainedElementsT();

		/**
		 * The meta object literal for the '<em><b>Contained Element Ref</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTAINED_ELEMENTS_T__CONTAINED_ELEMENT_REF = eINSTANCE.getContainedElementsT_ContainedElementRef();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.DataElementAbstractTImpl <em>Data Element Abstract T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.DataElementAbstractTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getDataElementAbstractT()
		 * @generated
		 */
		EClass DATA_ELEMENT_ABSTRACT_T = eINSTANCE.getDataElementAbstractT();

		/**
		 * The meta object literal for the '<em><b>Multi Language Property</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_ELEMENT_ABSTRACT_T__MULTI_LANGUAGE_PROPERTY = eINSTANCE.getDataElementAbstractT_MultiLanguageProperty();

		/**
		 * The meta object literal for the '<em><b>Property</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_ELEMENT_ABSTRACT_T__PROPERTY = eINSTANCE.getDataElementAbstractT_Property();

		/**
		 * The meta object literal for the '<em><b>Range</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_ELEMENT_ABSTRACT_T__RANGE = eINSTANCE.getDataElementAbstractT_Range();

		/**
		 * The meta object literal for the '<em><b>Blob</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_ELEMENT_ABSTRACT_T__BLOB = eINSTANCE.getDataElementAbstractT_Blob();

		/**
		 * The meta object literal for the '<em><b>File</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_ELEMENT_ABSTRACT_T__FILE = eINSTANCE.getDataElementAbstractT_File();

		/**
		 * The meta object literal for the '<em><b>Reference Element</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_ELEMENT_ABSTRACT_T__REFERENCE_ELEMENT = eINSTANCE.getDataElementAbstractT_ReferenceElement();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.DataElementsTImpl <em>Data Elements T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.DataElementsTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getDataElementsT()
		 * @generated
		 */
		EClass DATA_ELEMENTS_T = eINSTANCE.getDataElementsT();

		/**
		 * The meta object literal for the '<em><b>Data Element</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_ELEMENTS_T__DATA_ELEMENT = eINSTANCE.getDataElementsT_DataElement();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.DataSpecificationContentTImpl <em>Data Specification Content T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.DataSpecificationContentTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getDataSpecificationContentT()
		 * @generated
		 */
		EClass DATA_SPECIFICATION_CONTENT_T = eINSTANCE.getDataSpecificationContentT();

		/**
		 * The meta object literal for the '<em><b>Data Specification IEC61360</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SPECIFICATION_CONTENT_T__DATA_SPECIFICATION_IEC61360 = eINSTANCE.getDataSpecificationContentT_DataSpecificationIEC61360();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.DataTypeDefTImpl <em>Data Type Def T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.DataTypeDefTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getDataTypeDefT()
		 * @generated
		 */
		EClass DATA_TYPE_DEF_T = eINSTANCE.getDataTypeDefT();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_TYPE_DEF_T__VALUE = eINSTANCE.getDataTypeDefT_Value();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.DocumentRootImpl <em>Document Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.DocumentRootImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getDocumentRoot()
		 * @generated
		 */
		EClass DOCUMENT_ROOT = eINSTANCE.getDocumentRoot();

		/**
		 * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENT_ROOT__MIXED = eINSTANCE.getDocumentRoot_Mixed();

		/**
		 * The meta object literal for the '<em><b>XMLNS Prefix Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XMLNS_PREFIX_MAP = eINSTANCE.getDocumentRoot_XMLNSPrefixMap();

		/**
		 * The meta object literal for the '<em><b>XSI Schema Location</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = eINSTANCE.getDocumentRoot_XSISchemaLocation();

		/**
		 * The meta object literal for the '<em><b>Aasenv</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__AASENV = eINSTANCE.getDocumentRoot_Aasenv();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__KEY = eINSTANCE.getDocumentRoot_Key();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.EmbeddedDataSpecificationTImpl <em>Embedded Data Specification T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.EmbeddedDataSpecificationTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getEmbeddedDataSpecificationT()
		 * @generated
		 */
		EClass EMBEDDED_DATA_SPECIFICATION_T = eINSTANCE.getEmbeddedDataSpecificationT();

		/**
		 * The meta object literal for the '<em><b>Data Specification Content</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EMBEDDED_DATA_SPECIFICATION_T__DATA_SPECIFICATION_CONTENT = eINSTANCE.getEmbeddedDataSpecificationT_DataSpecificationContent();

		/**
		 * The meta object literal for the '<em><b>Data Specification</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EMBEDDED_DATA_SPECIFICATION_T__DATA_SPECIFICATION = eINSTANCE.getEmbeddedDataSpecificationT_DataSpecification();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.EntityTImpl <em>Entity T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.EntityTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getEntityT()
		 * @generated
		 */
		EClass ENTITY_T = eINSTANCE.getEntityT();

		/**
		 * The meta object literal for the '<em><b>Statements</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY_T__STATEMENTS = eINSTANCE.getEntityT_Statements();

		/**
		 * The meta object literal for the '<em><b>Entity Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_T__ENTITY_TYPE = eINSTANCE.getEntityT_EntityType();

		/**
		 * The meta object literal for the '<em><b>Asset Ref</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY_T__ASSET_REF = eINSTANCE.getEntityT_AssetRef();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.EventAbstractTImpl <em>Event Abstract T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.EventAbstractTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getEventAbstractT()
		 * @generated
		 */
		EClass EVENT_ABSTRACT_T = eINSTANCE.getEventAbstractT();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.FileTImpl <em>File T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.FileTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getFileT()
		 * @generated
		 */
		EClass FILE_T = eINSTANCE.getFileT();

		/**
		 * The meta object literal for the '<em><b>Mime Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FILE_T__MIME_TYPE = eINSTANCE.getFileT_MimeType();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FILE_T__VALUE = eINSTANCE.getFileT_Value();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.FormulaTImpl <em>Formula T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.FormulaTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getFormulaT()
		 * @generated
		 */
		EClass FORMULA_T = eINSTANCE.getFormulaT();

		/**
		 * The meta object literal for the '<em><b>Depends On Refs</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORMULA_T__DEPENDS_ON_REFS = eINSTANCE.getFormulaT_DependsOnRefs();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.IdentificationTImpl <em>Identification T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.IdentificationTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getIdentificationT()
		 * @generated
		 */
		EClass IDENTIFICATION_T = eINSTANCE.getIdentificationT();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IDENTIFICATION_T__VALUE = eINSTANCE.getIdentificationT_Value();

		/**
		 * The meta object literal for the '<em><b>Id Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IDENTIFICATION_T__ID_TYPE = eINSTANCE.getIdentificationT_IdType();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.IdentifierTImpl <em>Identifier T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.IdentifierTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getIdentifierT()
		 * @generated
		 */
		EClass IDENTIFIER_T = eINSTANCE.getIdentifierT();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IDENTIFIER_T__ID = eINSTANCE.getIdentifierT_Id();

		/**
		 * The meta object literal for the '<em><b>Id Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IDENTIFIER_T__ID_TYPE = eINSTANCE.getIdentifierT_IdType();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.IdPropertyDefinitionTImpl <em>Id Property Definition T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.IdPropertyDefinitionTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getIdPropertyDefinitionT()
		 * @generated
		 */
		EClass ID_PROPERTY_DEFINITION_T = eINSTANCE.getIdPropertyDefinitionT();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ID_PROPERTY_DEFINITION_T__VALUE = eINSTANCE.getIdPropertyDefinitionT_Value();

		/**
		 * The meta object literal for the '<em><b>Id Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ID_PROPERTY_DEFINITION_T__ID_TYPE = eINSTANCE.getIdPropertyDefinitionT_IdType();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.IdShortTImpl <em>Id Short T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.IdShortTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getIdShortT()
		 * @generated
		 */
		EClass ID_SHORT_T = eINSTANCE.getIdShortT();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ID_SHORT_T__VALUE = eINSTANCE.getIdShortT_Value();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.KeysTImpl <em>Keys T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.KeysTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getKeysT()
		 * @generated
		 */
		EClass KEYS_T = eINSTANCE.getKeysT();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference KEYS_T__KEY = eINSTANCE.getKeysT_Key();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.KeyTImpl <em>Key T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.KeyTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getKeyT()
		 * @generated
		 */
		EClass KEY_T = eINSTANCE.getKeyT();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute KEY_T__VALUE = eINSTANCE.getKeyT_Value();

		/**
		 * The meta object literal for the '<em><b>Id Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute KEY_T__ID_TYPE = eINSTANCE.getKeyT_IdType();

		/**
		 * The meta object literal for the '<em><b>Local</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute KEY_T__LOCAL = eINSTANCE.getKeyT_Local();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute KEY_T__TYPE = eINSTANCE.getKeyT_Type();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.LangStringSetTImpl <em>Lang String Set T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.LangStringSetTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getLangStringSetT()
		 * @generated
		 */
		EClass LANG_STRING_SET_T = eINSTANCE.getLangStringSetT();

		/**
		 * The meta object literal for the '<em><b>Lang String</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LANG_STRING_SET_T__LANG_STRING = eINSTANCE.getLangStringSetT_LangString();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.LangStringTImpl <em>Lang String T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.LangStringTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getLangStringT()
		 * @generated
		 */
		EClass LANG_STRING_T = eINSTANCE.getLangStringT();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LANG_STRING_T__VALUE = eINSTANCE.getLangStringT_Value();

		/**
		 * The meta object literal for the '<em><b>Lang</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LANG_STRING_T__LANG = eINSTANCE.getLangStringT_Lang();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.MultiLanguagePropertyTImpl <em>Multi Language Property T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.MultiLanguagePropertyTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getMultiLanguagePropertyT()
		 * @generated
		 */
		EClass MULTI_LANGUAGE_PROPERTY_T = eINSTANCE.getMultiLanguagePropertyT();

		/**
		 * The meta object literal for the '<em><b>Value Id</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MULTI_LANGUAGE_PROPERTY_T__VALUE_ID = eINSTANCE.getMultiLanguagePropertyT_ValueId();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MULTI_LANGUAGE_PROPERTY_T__VALUE = eINSTANCE.getMultiLanguagePropertyT_Value();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.OperationTImpl <em>Operation T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.OperationTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getOperationT()
		 * @generated
		 */
		EClass OPERATION_T = eINSTANCE.getOperationT();

		/**
		 * The meta object literal for the '<em><b>Input Variable</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_T__INPUT_VARIABLE = eINSTANCE.getOperationT_InputVariable();

		/**
		 * The meta object literal for the '<em><b>Output Variable</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_T__OUTPUT_VARIABLE = eINSTANCE.getOperationT_OutputVariable();

		/**
		 * The meta object literal for the '<em><b>Inoutput Variable</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_T__INOUTPUT_VARIABLE = eINSTANCE.getOperationT_InoutputVariable();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.OperationVariableTImpl <em>Operation Variable T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.OperationVariableTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getOperationVariableT()
		 * @generated
		 */
		EClass OPERATION_VARIABLE_T = eINSTANCE.getOperationVariableT();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_VARIABLE_T__VALUE = eINSTANCE.getOperationVariableT_Value();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.PathTypeTImpl <em>Path Type T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.PathTypeTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getPathTypeT()
		 * @generated
		 */
		EClass PATH_TYPE_T = eINSTANCE.getPathTypeT();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PATH_TYPE_T__VALUE = eINSTANCE.getPathTypeT_Value();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.PropertyTImpl <em>Property T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.PropertyTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getPropertyT()
		 * @generated
		 */
		EClass PROPERTY_T = eINSTANCE.getPropertyT();

		/**
		 * The meta object literal for the '<em><b>Value Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_T__VALUE_TYPE = eINSTANCE.getPropertyT_ValueType();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_T__VALUE = eINSTANCE.getPropertyT_Value();

		/**
		 * The meta object literal for the '<em><b>Value Id</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_T__VALUE_ID = eINSTANCE.getPropertyT_ValueId();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.QualifierTImpl <em>Qualifier T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.QualifierTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getQualifierT()
		 * @generated
		 */
		EClass QUALIFIER_T = eINSTANCE.getQualifierT();

		/**
		 * The meta object literal for the '<em><b>Semantic Id</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference QUALIFIER_T__SEMANTIC_ID = eINSTANCE.getQualifierT_SemanticId();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference QUALIFIER_T__TYPE = eINSTANCE.getQualifierT_Type();

		/**
		 * The meta object literal for the '<em><b>Value Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference QUALIFIER_T__VALUE_TYPE = eINSTANCE.getQualifierT_ValueType();

		/**
		 * The meta object literal for the '<em><b>Value Id</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference QUALIFIER_T__VALUE_ID = eINSTANCE.getQualifierT_ValueId();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference QUALIFIER_T__VALUE = eINSTANCE.getQualifierT_Value();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.QualifierTypeTImpl <em>Qualifier Type T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.QualifierTypeTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getQualifierTypeT()
		 * @generated
		 */
		EClass QUALIFIER_TYPE_T = eINSTANCE.getQualifierTypeT();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUALIFIER_TYPE_T__VALUE = eINSTANCE.getQualifierTypeT_Value();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.RangeTImpl <em>Range T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.RangeTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getRangeT()
		 * @generated
		 */
		EClass RANGE_T = eINSTANCE.getRangeT();

		/**
		 * The meta object literal for the '<em><b>Value Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RANGE_T__VALUE_TYPE = eINSTANCE.getRangeT_ValueType();

		/**
		 * The meta object literal for the '<em><b>Min</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RANGE_T__MIN = eINSTANCE.getRangeT_Min();

		/**
		 * The meta object literal for the '<em><b>Max</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RANGE_T__MAX = eINSTANCE.getRangeT_Max();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.ReferenceElementTImpl <em>Reference Element T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.ReferenceElementTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getReferenceElementT()
		 * @generated
		 */
		EClass REFERENCE_ELEMENT_T = eINSTANCE.getReferenceElementT();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE_ELEMENT_T__VALUE = eINSTANCE.getReferenceElementT_Value();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.ReferencesTImpl <em>References T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.ReferencesTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getReferencesT()
		 * @generated
		 */
		EClass REFERENCES_T = eINSTANCE.getReferencesT();

		/**
		 * The meta object literal for the '<em><b>Reference</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCES_T__REFERENCE = eINSTANCE.getReferencesT_Reference();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.ReferenceTImpl <em>Reference T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.ReferenceTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getReferenceT()
		 * @generated
		 */
		EClass REFERENCE_T = eINSTANCE.getReferenceT();

		/**
		 * The meta object literal for the '<em><b>Keys</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE_T__KEYS = eINSTANCE.getReferenceT_Keys();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.RelationshipElementTImpl <em>Relationship Element T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.RelationshipElementTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getRelationshipElementT()
		 * @generated
		 */
		EClass RELATIONSHIP_ELEMENT_T = eINSTANCE.getRelationshipElementT();

		/**
		 * The meta object literal for the '<em><b>First</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATIONSHIP_ELEMENT_T__FIRST = eINSTANCE.getRelationshipElementT_First();

		/**
		 * The meta object literal for the '<em><b>Second</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATIONSHIP_ELEMENT_T__SECOND = eINSTANCE.getRelationshipElementT_Second();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.SemanticIdTImpl <em>Semantic Id T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.SemanticIdTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getSemanticIdT()
		 * @generated
		 */
		EClass SEMANTIC_ID_T = eINSTANCE.getSemanticIdT();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.SubmodelElementAbstractTImpl <em>Submodel Element Abstract T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.SubmodelElementAbstractTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getSubmodelElementAbstractT()
		 * @generated
		 */
		EClass SUBMODEL_ELEMENT_ABSTRACT_T = eINSTANCE.getSubmodelElementAbstractT();

		/**
		 * The meta object literal for the '<em><b>Id Short</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENT_ABSTRACT_T__ID_SHORT = eINSTANCE.getSubmodelElementAbstractT_IdShort();

		/**
		 * The meta object literal for the '<em><b>Category</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUBMODEL_ELEMENT_ABSTRACT_T__CATEGORY = eINSTANCE.getSubmodelElementAbstractT_Category();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENT_ABSTRACT_T__DESCRIPTION = eINSTANCE.getSubmodelElementAbstractT_Description();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENT_ABSTRACT_T__PARENT = eINSTANCE.getSubmodelElementAbstractT_Parent();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUBMODEL_ELEMENT_ABSTRACT_T__KIND = eINSTANCE.getSubmodelElementAbstractT_Kind();

		/**
		 * The meta object literal for the '<em><b>Semantic Id</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENT_ABSTRACT_T__SEMANTIC_ID = eINSTANCE.getSubmodelElementAbstractT_SemanticId();

		/**
		 * The meta object literal for the '<em><b>Qualifier</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENT_ABSTRACT_T__QUALIFIER = eINSTANCE.getSubmodelElementAbstractT_Qualifier();

		/**
		 * The meta object literal for the '<em><b>Embedded Data Specification</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENT_ABSTRACT_T__EMBEDDED_DATA_SPECIFICATION = eINSTANCE.getSubmodelElementAbstractT_EmbeddedDataSpecification();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.SubmodelElementCollectionTImpl <em>Submodel Element Collection T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.SubmodelElementCollectionTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getSubmodelElementCollectionT()
		 * @generated
		 */
		EClass SUBMODEL_ELEMENT_COLLECTION_T = eINSTANCE.getSubmodelElementCollectionT();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENT_COLLECTION_T__VALUE = eINSTANCE.getSubmodelElementCollectionT_Value();

		/**
		 * The meta object literal for the '<em><b>Ordered</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUBMODEL_ELEMENT_COLLECTION_T__ORDERED = eINSTANCE.getSubmodelElementCollectionT_Ordered();

		/**
		 * The meta object literal for the '<em><b>Allow Duplicates</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUBMODEL_ELEMENT_COLLECTION_T__ALLOW_DUPLICATES = eINSTANCE.getSubmodelElementCollectionT_AllowDuplicates();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.SubmodelElementsTImpl <em>Submodel Elements T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.SubmodelElementsTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getSubmodelElementsT()
		 * @generated
		 */
		EClass SUBMODEL_ELEMENTS_T = eINSTANCE.getSubmodelElementsT();

		/**
		 * The meta object literal for the '<em><b>Submodel Element</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENTS_T__SUBMODEL_ELEMENT = eINSTANCE.getSubmodelElementsT_SubmodelElement();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.SubmodelElementTImpl <em>Submodel Element T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.SubmodelElementTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getSubmodelElementT()
		 * @generated
		 */
		EClass SUBMODEL_ELEMENT_T = eINSTANCE.getSubmodelElementT();

		/**
		 * The meta object literal for the '<em><b>Multi Language Property</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENT_T__MULTI_LANGUAGE_PROPERTY = eINSTANCE.getSubmodelElementT_MultiLanguageProperty();

		/**
		 * The meta object literal for the '<em><b>Property</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENT_T__PROPERTY = eINSTANCE.getSubmodelElementT_Property();

		/**
		 * The meta object literal for the '<em><b>Range</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENT_T__RANGE = eINSTANCE.getSubmodelElementT_Range();

		/**
		 * The meta object literal for the '<em><b>Blob</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENT_T__BLOB = eINSTANCE.getSubmodelElementT_Blob();

		/**
		 * The meta object literal for the '<em><b>File</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENT_T__FILE = eINSTANCE.getSubmodelElementT_File();

		/**
		 * The meta object literal for the '<em><b>Reference Element</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENT_T__REFERENCE_ELEMENT = eINSTANCE.getSubmodelElementT_ReferenceElement();

		/**
		 * The meta object literal for the '<em><b>Annotated Relationship Element</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENT_T__ANNOTATED_RELATIONSHIP_ELEMENT = eINSTANCE.getSubmodelElementT_AnnotatedRelationshipElement();

		/**
		 * The meta object literal for the '<em><b>Basic Event</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENT_T__BASIC_EVENT = eINSTANCE.getSubmodelElementT_BasicEvent();

		/**
		 * The meta object literal for the '<em><b>Capability</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENT_T__CAPABILITY = eINSTANCE.getSubmodelElementT_Capability();

		/**
		 * The meta object literal for the '<em><b>Entity</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENT_T__ENTITY = eINSTANCE.getSubmodelElementT_Entity();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENT_T__OPERATION = eINSTANCE.getSubmodelElementT_Operation();

		/**
		 * The meta object literal for the '<em><b>Relationship Element</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENT_T__RELATIONSHIP_ELEMENT = eINSTANCE.getSubmodelElementT_RelationshipElement();

		/**
		 * The meta object literal for the '<em><b>Submodel Element Collection</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_ELEMENT_T__SUBMODEL_ELEMENT_COLLECTION = eINSTANCE.getSubmodelElementT_SubmodelElementCollection();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.SubmodelRefsTImpl <em>Submodel Refs T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.SubmodelRefsTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getSubmodelRefsT()
		 * @generated
		 */
		EClass SUBMODEL_REFS_T = eINSTANCE.getSubmodelRefsT();

		/**
		 * The meta object literal for the '<em><b>Submodel Ref</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_REFS_T__SUBMODEL_REF = eINSTANCE.getSubmodelRefsT_SubmodelRef();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.SubmodelsTImpl <em>Submodels T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.SubmodelsTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getSubmodelsT()
		 * @generated
		 */
		EClass SUBMODELS_T = eINSTANCE.getSubmodelsT();

		/**
		 * The meta object literal for the '<em><b>Submodel</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODELS_T__SUBMODEL = eINSTANCE.getSubmodelsT_Submodel();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.SubmodelTImpl <em>Submodel T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.SubmodelTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getSubmodelT()
		 * @generated
		 */
		EClass SUBMODEL_T = eINSTANCE.getSubmodelT();

		/**
		 * The meta object literal for the '<em><b>Id Short</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_T__ID_SHORT = eINSTANCE.getSubmodelT_IdShort();

		/**
		 * The meta object literal for the '<em><b>Category</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUBMODEL_T__CATEGORY = eINSTANCE.getSubmodelT_Category();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_T__DESCRIPTION = eINSTANCE.getSubmodelT_Description();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_T__PARENT = eINSTANCE.getSubmodelT_Parent();

		/**
		 * The meta object literal for the '<em><b>Identification</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_T__IDENTIFICATION = eINSTANCE.getSubmodelT_Identification();

		/**
		 * The meta object literal for the '<em><b>Administration</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_T__ADMINISTRATION = eINSTANCE.getSubmodelT_Administration();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUBMODEL_T__KIND = eINSTANCE.getSubmodelT_Kind();

		/**
		 * The meta object literal for the '<em><b>Semantic Id</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_T__SEMANTIC_ID = eINSTANCE.getSubmodelT_SemanticId();

		/**
		 * The meta object literal for the '<em><b>Qualifier</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_T__QUALIFIER = eINSTANCE.getSubmodelT_Qualifier();

		/**
		 * The meta object literal for the '<em><b>Embedded Data Specification</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_T__EMBEDDED_DATA_SPECIFICATION = eINSTANCE.getSubmodelT_EmbeddedDataSpecification();

		/**
		 * The meta object literal for the '<em><b>Submodel Elements</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMODEL_T__SUBMODEL_ELEMENTS = eINSTANCE.getSubmodelT_SubmodelElements();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.ValueDataTypeTImpl <em>Value Data Type T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.ValueDataTypeTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getValueDataTypeT()
		 * @generated
		 */
		EClass VALUE_DATA_TYPE_T = eINSTANCE.getValueDataTypeT();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VALUE_DATA_TYPE_T__VALUE = eINSTANCE.getValueDataTypeT_Value();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.ViewsTImpl <em>Views T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.ViewsTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getViewsT()
		 * @generated
		 */
		EClass VIEWS_T = eINSTANCE.getViewsT();

		/**
		 * The meta object literal for the '<em><b>View</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VIEWS_T__VIEW = eINSTANCE.getViewsT_View();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.impl.ViewTImpl <em>View T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.impl.ViewTImpl
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getViewT()
		 * @generated
		 */
		EClass VIEW_T = eINSTANCE.getViewT();

		/**
		 * The meta object literal for the '<em><b>Id Short</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VIEW_T__ID_SHORT = eINSTANCE.getViewT_IdShort();

		/**
		 * The meta object literal for the '<em><b>Category</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VIEW_T__CATEGORY = eINSTANCE.getViewT_Category();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VIEW_T__DESCRIPTION = eINSTANCE.getViewT_Description();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VIEW_T__PARENT = eINSTANCE.getViewT_Parent();

		/**
		 * The meta object literal for the '<em><b>Semantic Id</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VIEW_T__SEMANTIC_ID = eINSTANCE.getViewT_SemanticId();

		/**
		 * The meta object literal for the '<em><b>Embedded Data Specification</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VIEW_T__EMBEDDED_DATA_SPECIFICATION = eINSTANCE.getViewT_EmbeddedDataSpecification();

		/**
		 * The meta object literal for the '<em><b>Contained Elements</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VIEW_T__CONTAINED_ELEMENTS = eINSTANCE.getViewT_ContainedElements();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.AssetKindT <em>Asset Kind T</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.AssetKindT
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getAssetKindT()
		 * @generated
		 */
		EEnum ASSET_KIND_T = eINSTANCE.getAssetKindT();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.EntityTypeType <em>Entity Type Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.EntityTypeType
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getEntityTypeType()
		 * @generated
		 */
		EEnum ENTITY_TYPE_TYPE = eINSTANCE.getEntityTypeType();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.IdentifierTypeT <em>Identifier Type T</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.IdentifierTypeT
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getIdentifierTypeT()
		 * @generated
		 */
		EEnum IDENTIFIER_TYPE_T = eINSTANCE.getIdentifierTypeT();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.IdTypeType <em>Id Type Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.IdTypeType
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getIdTypeType()
		 * @generated
		 */
		EEnum ID_TYPE_TYPE = eINSTANCE.getIdTypeType();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.IdTypeType1 <em>Id Type Type1</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.IdTypeType1
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getIdTypeType1()
		 * @generated
		 */
		EEnum ID_TYPE_TYPE1 = eINSTANCE.getIdTypeType1();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.ModelingKindT <em>Modeling Kind T</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.ModelingKindT
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getModelingKindT()
		 * @generated
		 */
		EEnum MODELING_KIND_T = eINSTANCE.getModelingKindT();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas._2._0.TypeType <em>Type Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.TypeType
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getTypeType()
		 * @generated
		 */
		EEnum TYPE_TYPE = eINSTANCE.getTypeType();

		/**
		 * The meta object literal for the '<em>Asset Kind TObject</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.AssetKindT
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getAssetKindTObject()
		 * @generated
		 */
		EDataType ASSET_KIND_TOBJECT = eINSTANCE.getAssetKindTObject();

		/**
		 * The meta object literal for the '<em>Entity Type T</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getEntityTypeT()
		 * @generated
		 */
		EDataType ENTITY_TYPE_T = eINSTANCE.getEntityTypeT();

		/**
		 * The meta object literal for the '<em>Entity Type Type Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.EntityTypeType
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getEntityTypeTypeObject()
		 * @generated
		 */
		EDataType ENTITY_TYPE_TYPE_OBJECT = eINSTANCE.getEntityTypeTypeObject();

		/**
		 * The meta object literal for the '<em>Identifier Type TObject</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.IdentifierTypeT
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getIdentifierTypeTObject()
		 * @generated
		 */
		EDataType IDENTIFIER_TYPE_TOBJECT = eINSTANCE.getIdentifierTypeTObject();

		/**
		 * The meta object literal for the '<em>Id Type Type Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.IdTypeType
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getIdTypeTypeObject()
		 * @generated
		 */
		EDataType ID_TYPE_TYPE_OBJECT = eINSTANCE.getIdTypeTypeObject();

		/**
		 * The meta object literal for the '<em>Id Type Type Object1</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.IdTypeType1
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getIdTypeTypeObject1()
		 * @generated
		 */
		EDataType ID_TYPE_TYPE_OBJECT1 = eINSTANCE.getIdTypeTypeObject1();

		/**
		 * The meta object literal for the '<em>Modeling Kind TObject</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.ModelingKindT
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getModelingKindTObject()
		 * @generated
		 */
		EDataType MODELING_KIND_TOBJECT = eINSTANCE.getModelingKindTObject();

		/**
		 * The meta object literal for the '<em>Type Type Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas._2._0.TypeType
		 * @see io.shell.admin.aas._2._0.impl._0PackageImpl#getTypeTypeObject()
		 * @generated
		 */
		EDataType TYPE_TYPE_OBJECT = eINSTANCE.getTypeTypeObject();

	}

} //_0Package
