/**
 */
package io.shell.admin.iec61360._2._0;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc --> A representation of the literals of the enumeration
 * '<em><b>Data Type IEC61360T</b></em>', and utility methods for working with
 * them. <!-- end-user-doc -->
 * 
 * @see io.shell.admin.iec61360._2._0._0Package#getDataTypeIEC61360T()
 * @model extendedMetaData="name='dataTypeIEC61360_t'"
 * @generated
 */
public enum DataTypeIEC61360T implements Enumerator {
	/**
	 * The '<em><b></b></em>' literal object. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #__VALUE
	 * @generated
	 * @ordered
	 */
	__(0, "__", ""),

	/**
	 * The '<em><b>BOOLEAN</b></em>' literal object. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #BOOLEAN_VALUE
	 * @generated
	 * @ordered
	 */
	BOOLEAN(1, "BOOLEAN", "BOOLEAN"),

	/**
	 * The '<em><b>DATE</b></em>' literal object. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #DATE_VALUE
	 * @generated
	 * @ordered
	 */
	DATE(2, "DATE", "DATE"),

	/**
	 * The '<em><b>RATIONAL</b></em>' literal object. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #RATIONAL_VALUE
	 * @generated
	 * @ordered
	 */
	RATIONAL(3, "RATIONAL", "RATIONAL"),

	/**
	 * The '<em><b>RATIONALMEASURE</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #RATIONALMEASURE_VALUE
	 * @generated
	 * @ordered
	 */
	RATIONALMEASURE(4, "RATIONALMEASURE", "RATIONAL_MEASURE"),

	/**
	 * The '<em><b>REALCOUNT</b></em>' literal object. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #REALCOUNT_VALUE
	 * @generated
	 * @ordered
	 */
	REALCOUNT(5, "REALCOUNT", "REAL_COUNT"),

	/**
	 * The '<em><b>REALCURRENCY</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #REALCURRENCY_VALUE
	 * @generated
	 * @ordered
	 */
	REALCURRENCY(6, "REALCURRENCY", "REAL_CURRENCY"),

	/**
	 * The '<em><b>REALMEASURE</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #REALMEASURE_VALUE
	 * @generated
	 * @ordered
	 */
	REALMEASURE(7, "REALMEASURE", "REAL_MEASURE"),

	/**
	 * The '<em><b>STRING</b></em>' literal object. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #STRING_VALUE
	 * @generated
	 * @ordered
	 */
	STRING(8, "STRING", "STRING"),

	/**
	 * The '<em><b>STRINGTRANSLATABLE</b></em>' literal object. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #STRINGTRANSLATABLE_VALUE
	 * @generated
	 * @ordered
	 */
	STRINGTRANSLATABLE(9, "STRINGTRANSLATABLE", "STRING_TRANSLATABLE"),

	/**
	 * The '<em><b>TIME</b></em>' literal object. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #TIME_VALUE
	 * @generated
	 * @ordered
	 */
	TIME(10, "TIME", "TIME"),

	/**
	 * The '<em><b>TIMESTAMP</b></em>' literal object. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #TIMESTAMP_VALUE
	 * @generated
	 * @ordered
	 */
	TIMESTAMP(11, "TIMESTAMP", "TIMESTAMP"),

	/**
	 * The '<em><b>URL</b></em>' literal object. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #URL_VALUE
	 * @generated
	 * @ordered
	 */
	URL(12, "URL", "URL"),

	/**
	 * The '<em><b>INTEGERCOUNT</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #INTEGERCOUNT_VALUE
	 * @generated
	 * @ordered
	 */
	INTEGERCOUNT(13, "INTEGERCOUNT", "INTEGER_COUNT"),

	/**
	 * The '<em><b>INTEGERCURRENCY</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #INTEGERCURRENCY_VALUE
	 * @generated
	 * @ordered
	 */
	INTEGERCURRENCY(14, "INTEGERCURRENCY", "INTEGER_CURRENCY"),

	/**
	 * The '<em><b>INTEGERMEASURE</b></em>' literal object. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #INTEGERMEASURE_VALUE
	 * @generated
	 * @ordered
	 */
	INTEGERMEASURE(15, "INTEGERMEASURE", "INTEGER_MEASURE");

	/**
	 * The '<em><b></b></em>' literal value. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #_
	 * @model literal=""
	 * @generated
	 * @ordered
	 */
	public static final int __VALUE = 0;

	/**
	 * The '<em><b>BOOLEAN</b></em>' literal value. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #BOOLEAN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BOOLEAN_VALUE = 1;

	/**
	 * The '<em><b>DATE</b></em>' literal value. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #DATE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DATE_VALUE = 2;

	/**
	 * The '<em><b>RATIONAL</b></em>' literal value. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #RATIONAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int RATIONAL_VALUE = 3;

	/**
	 * The '<em><b>RATIONALMEASURE</b></em>' literal value. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #RATIONALMEASURE
	 * @model literal="RATIONAL_MEASURE"
	 * @generated
	 * @ordered
	 */
	public static final int RATIONALMEASURE_VALUE = 4;

	/**
	 * The '<em><b>REALCOUNT</b></em>' literal value. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #REALCOUNT
	 * @model literal="REAL_COUNT"
	 * @generated
	 * @ordered
	 */
	public static final int REALCOUNT_VALUE = 5;

	/**
	 * The '<em><b>REALCURRENCY</b></em>' literal value. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #REALCURRENCY
	 * @model literal="REAL_CURRENCY"
	 * @generated
	 * @ordered
	 */
	public static final int REALCURRENCY_VALUE = 6;

	/**
	 * The '<em><b>REALMEASURE</b></em>' literal value. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #REALMEASURE
	 * @model literal="REAL_MEASURE"
	 * @generated
	 * @ordered
	 */
	public static final int REALMEASURE_VALUE = 7;

	/**
	 * The '<em><b>STRING</b></em>' literal value. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #STRING
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int STRING_VALUE = 8;

	/**
	 * The '<em><b>STRINGTRANSLATABLE</b></em>' literal value. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #STRINGTRANSLATABLE
	 * @model literal="STRING_TRANSLATABLE"
	 * @generated
	 * @ordered
	 */
	public static final int STRINGTRANSLATABLE_VALUE = 9;

	/**
	 * The '<em><b>TIME</b></em>' literal value. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #TIME
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TIME_VALUE = 10;

	/**
	 * The '<em><b>TIMESTAMP</b></em>' literal value. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #TIMESTAMP
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TIMESTAMP_VALUE = 11;

	/**
	 * The '<em><b>URL</b></em>' literal value. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #URL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int URL_VALUE = 12;

	/**
	 * The '<em><b>INTEGERCOUNT</b></em>' literal value. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #INTEGERCOUNT
	 * @model literal="INTEGER_COUNT"
	 * @generated
	 * @ordered
	 */
	public static final int INTEGERCOUNT_VALUE = 13;

	/**
	 * The '<em><b>INTEGERCURRENCY</b></em>' literal value. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #INTEGERCURRENCY
	 * @model literal="INTEGER_CURRENCY"
	 * @generated
	 * @ordered
	 */
	public static final int INTEGERCURRENCY_VALUE = 14;

	/**
	 * The '<em><b>INTEGERMEASURE</b></em>' literal value. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #INTEGERMEASURE
	 * @model literal="INTEGER_MEASURE"
	 * @generated
	 * @ordered
	 */
	public static final int INTEGERMEASURE_VALUE = 15;

	/**
	 * An array of all the '<em><b>Data Type IEC61360T</b></em>' enumerators. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private static final DataTypeIEC61360T[] VALUES_ARRAY = new DataTypeIEC61360T[] { __, BOOLEAN, DATE, RATIONAL,
			RATIONALMEASURE, REALCOUNT, REALCURRENCY, REALMEASURE, STRING, STRINGTRANSLATABLE, TIME, TIMESTAMP, URL,
			INTEGERCOUNT, INTEGERCURRENCY, INTEGERMEASURE, };

	/**
	 * A public read-only list of all the '<em><b>Data Type IEC61360T</b></em>'
	 * enumerators. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final List<DataTypeIEC61360T> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Data Type IEC61360T</b></em>' literal with the specified
	 * literal value. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DataTypeIEC61360T get(String literal) {
		if (literal.equals("")) {
			return VALUES_ARRAY[0];
		} else {
			for (int i = 0; i < VALUES_ARRAY.length; ++i) {
				DataTypeIEC61360T result = VALUES_ARRAY[i];
				if (result.toString().equals(literal)) {
					return result;
				}
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Data Type IEC61360T</b></em>' literal with the specified
	 * name. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DataTypeIEC61360T getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DataTypeIEC61360T result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Data Type IEC61360T</b></em>' literal with the specified
	 * integer value. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DataTypeIEC61360T get(int value) {
		switch (value) {
		case __VALUE:
			return __;
		case BOOLEAN_VALUE:
			return BOOLEAN;
		case DATE_VALUE:
			return DATE;
		case RATIONAL_VALUE:
			return RATIONAL;
		case RATIONALMEASURE_VALUE:
			return RATIONALMEASURE;
		case REALCOUNT_VALUE:
			return REALCOUNT;
		case REALCURRENCY_VALUE:
			return REALCURRENCY;
		case REALMEASURE_VALUE:
			return REALMEASURE;
		case STRING_VALUE:
			return STRING;
		case STRINGTRANSLATABLE_VALUE:
			return STRINGTRANSLATABLE;
		case TIME_VALUE:
			return TIME;
		case TIMESTAMP_VALUE:
			return TIMESTAMP;
		case URL_VALUE:
			return URL;
		case INTEGERCOUNT_VALUE:
			return INTEGERCOUNT;
		case INTEGERCURRENCY_VALUE:
			return INTEGERCURRENCY;
		case INTEGERMEASURE_VALUE:
			return INTEGERMEASURE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	private DataTypeIEC61360T(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string
	 * representation. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} // DataTypeIEC61360T
