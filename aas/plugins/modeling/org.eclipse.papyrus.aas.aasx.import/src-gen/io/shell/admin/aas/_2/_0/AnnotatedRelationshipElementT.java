/**
 */
package io.shell.admin.aas._2._0;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Annotated Relationship Element T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.AnnotatedRelationshipElementT#getAnnotations <em>Annotations</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getAnnotatedRelationshipElementT()
 * @model extendedMetaData="name='annotatedRelationshipElement_t' kind='elementOnly'"
 * @generated
 */
public interface AnnotatedRelationshipElementT extends RelationshipElementT {
	/**
	 * Returns the value of the '<em><b>Annotations</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotations</em>' containment reference.
	 * @see #setAnnotations(DataElementsT)
	 * @see io.shell.admin.aas._2._0._0Package#getAnnotatedRelationshipElementT_Annotations()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='annotations' namespace='##targetNamespace'"
	 * @generated
	 */
	DataElementsT getAnnotations();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.AnnotatedRelationshipElementT#getAnnotations <em>Annotations</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Annotations</em>' containment reference.
	 * @see #getAnnotations()
	 * @generated
	 */
	void setAnnotations(DataElementsT value);

} // AnnotatedRelationshipElementT
