/**
 */
package io.shell.admin.aas.abac._2._0.impl;

import io.shell.admin.aas.abac._2._0.AccessControlPolicyPointsT;
import io.shell.admin.aas.abac._2._0.PolicyAdministrationPointT;
import io.shell.admin.aas.abac._2._0.PolicyDecisionPointT;
import io.shell.admin.aas.abac._2._0.PolicyEnforcementPointT;
import io.shell.admin.aas.abac._2._0.PolicyInformationPointsT;
import io.shell.admin.aas.abac._2._0._0Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Access Control Policy Points T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.AccessControlPolicyPointsTImpl#getPolicyAdministrationPoint <em>Policy Administration Point</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.AccessControlPolicyPointsTImpl#getPolicyDecisionPoint <em>Policy Decision Point</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.AccessControlPolicyPointsTImpl#getPolicyEnforcementPoint <em>Policy Enforcement Point</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.AccessControlPolicyPointsTImpl#getPolicyInformationPoints <em>Policy Information Points</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AccessControlPolicyPointsTImpl extends MinimalEObjectImpl.Container implements AccessControlPolicyPointsT {
	/**
	 * The cached value of the '{@link #getPolicyAdministrationPoint() <em>Policy Administration Point</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPolicyAdministrationPoint()
	 * @generated
	 * @ordered
	 */
	protected PolicyAdministrationPointT policyAdministrationPoint;

	/**
	 * The cached value of the '{@link #getPolicyDecisionPoint() <em>Policy Decision Point</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPolicyDecisionPoint()
	 * @generated
	 * @ordered
	 */
	protected PolicyDecisionPointT policyDecisionPoint;

	/**
	 * The cached value of the '{@link #getPolicyEnforcementPoint() <em>Policy Enforcement Point</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPolicyEnforcementPoint()
	 * @generated
	 * @ordered
	 */
	protected PolicyEnforcementPointT policyEnforcementPoint;

	/**
	 * The cached value of the '{@link #getPolicyInformationPoints() <em>Policy Information Points</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPolicyInformationPoints()
	 * @generated
	 * @ordered
	 */
	protected PolicyInformationPointsT policyInformationPoints;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AccessControlPolicyPointsTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.ACCESS_CONTROL_POLICY_POINTS_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PolicyAdministrationPointT getPolicyAdministrationPoint() {
		return policyAdministrationPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPolicyAdministrationPoint(PolicyAdministrationPointT newPolicyAdministrationPoint, NotificationChain msgs) {
		PolicyAdministrationPointT oldPolicyAdministrationPoint = policyAdministrationPoint;
		policyAdministrationPoint = newPolicyAdministrationPoint;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_ADMINISTRATION_POINT, oldPolicyAdministrationPoint, newPolicyAdministrationPoint);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPolicyAdministrationPoint(PolicyAdministrationPointT newPolicyAdministrationPoint) {
		if (newPolicyAdministrationPoint != policyAdministrationPoint) {
			NotificationChain msgs = null;
			if (policyAdministrationPoint != null)
				msgs = ((InternalEObject)policyAdministrationPoint).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_ADMINISTRATION_POINT, null, msgs);
			if (newPolicyAdministrationPoint != null)
				msgs = ((InternalEObject)newPolicyAdministrationPoint).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_ADMINISTRATION_POINT, null, msgs);
			msgs = basicSetPolicyAdministrationPoint(newPolicyAdministrationPoint, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_ADMINISTRATION_POINT, newPolicyAdministrationPoint, newPolicyAdministrationPoint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PolicyDecisionPointT getPolicyDecisionPoint() {
		return policyDecisionPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPolicyDecisionPoint(PolicyDecisionPointT newPolicyDecisionPoint, NotificationChain msgs) {
		PolicyDecisionPointT oldPolicyDecisionPoint = policyDecisionPoint;
		policyDecisionPoint = newPolicyDecisionPoint;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_DECISION_POINT, oldPolicyDecisionPoint, newPolicyDecisionPoint);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPolicyDecisionPoint(PolicyDecisionPointT newPolicyDecisionPoint) {
		if (newPolicyDecisionPoint != policyDecisionPoint) {
			NotificationChain msgs = null;
			if (policyDecisionPoint != null)
				msgs = ((InternalEObject)policyDecisionPoint).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_DECISION_POINT, null, msgs);
			if (newPolicyDecisionPoint != null)
				msgs = ((InternalEObject)newPolicyDecisionPoint).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_DECISION_POINT, null, msgs);
			msgs = basicSetPolicyDecisionPoint(newPolicyDecisionPoint, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_DECISION_POINT, newPolicyDecisionPoint, newPolicyDecisionPoint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PolicyEnforcementPointT getPolicyEnforcementPoint() {
		return policyEnforcementPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPolicyEnforcementPoint(PolicyEnforcementPointT newPolicyEnforcementPoint, NotificationChain msgs) {
		PolicyEnforcementPointT oldPolicyEnforcementPoint = policyEnforcementPoint;
		policyEnforcementPoint = newPolicyEnforcementPoint;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_ENFORCEMENT_POINT, oldPolicyEnforcementPoint, newPolicyEnforcementPoint);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPolicyEnforcementPoint(PolicyEnforcementPointT newPolicyEnforcementPoint) {
		if (newPolicyEnforcementPoint != policyEnforcementPoint) {
			NotificationChain msgs = null;
			if (policyEnforcementPoint != null)
				msgs = ((InternalEObject)policyEnforcementPoint).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_ENFORCEMENT_POINT, null, msgs);
			if (newPolicyEnforcementPoint != null)
				msgs = ((InternalEObject)newPolicyEnforcementPoint).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_ENFORCEMENT_POINT, null, msgs);
			msgs = basicSetPolicyEnforcementPoint(newPolicyEnforcementPoint, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_ENFORCEMENT_POINT, newPolicyEnforcementPoint, newPolicyEnforcementPoint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PolicyInformationPointsT getPolicyInformationPoints() {
		return policyInformationPoints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPolicyInformationPoints(PolicyInformationPointsT newPolicyInformationPoints, NotificationChain msgs) {
		PolicyInformationPointsT oldPolicyInformationPoints = policyInformationPoints;
		policyInformationPoints = newPolicyInformationPoints;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_INFORMATION_POINTS, oldPolicyInformationPoints, newPolicyInformationPoints);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPolicyInformationPoints(PolicyInformationPointsT newPolicyInformationPoints) {
		if (newPolicyInformationPoints != policyInformationPoints) {
			NotificationChain msgs = null;
			if (policyInformationPoints != null)
				msgs = ((InternalEObject)policyInformationPoints).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_INFORMATION_POINTS, null, msgs);
			if (newPolicyInformationPoints != null)
				msgs = ((InternalEObject)newPolicyInformationPoints).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_INFORMATION_POINTS, null, msgs);
			msgs = basicSetPolicyInformationPoints(newPolicyInformationPoints, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_INFORMATION_POINTS, newPolicyInformationPoints, newPolicyInformationPoints));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_ADMINISTRATION_POINT:
				return basicSetPolicyAdministrationPoint(null, msgs);
			case _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_DECISION_POINT:
				return basicSetPolicyDecisionPoint(null, msgs);
			case _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_ENFORCEMENT_POINT:
				return basicSetPolicyEnforcementPoint(null, msgs);
			case _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_INFORMATION_POINTS:
				return basicSetPolicyInformationPoints(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_ADMINISTRATION_POINT:
				return getPolicyAdministrationPoint();
			case _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_DECISION_POINT:
				return getPolicyDecisionPoint();
			case _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_ENFORCEMENT_POINT:
				return getPolicyEnforcementPoint();
			case _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_INFORMATION_POINTS:
				return getPolicyInformationPoints();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_ADMINISTRATION_POINT:
				setPolicyAdministrationPoint((PolicyAdministrationPointT)newValue);
				return;
			case _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_DECISION_POINT:
				setPolicyDecisionPoint((PolicyDecisionPointT)newValue);
				return;
			case _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_ENFORCEMENT_POINT:
				setPolicyEnforcementPoint((PolicyEnforcementPointT)newValue);
				return;
			case _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_INFORMATION_POINTS:
				setPolicyInformationPoints((PolicyInformationPointsT)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_ADMINISTRATION_POINT:
				setPolicyAdministrationPoint((PolicyAdministrationPointT)null);
				return;
			case _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_DECISION_POINT:
				setPolicyDecisionPoint((PolicyDecisionPointT)null);
				return;
			case _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_ENFORCEMENT_POINT:
				setPolicyEnforcementPoint((PolicyEnforcementPointT)null);
				return;
			case _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_INFORMATION_POINTS:
				setPolicyInformationPoints((PolicyInformationPointsT)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_ADMINISTRATION_POINT:
				return policyAdministrationPoint != null;
			case _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_DECISION_POINT:
				return policyDecisionPoint != null;
			case _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_ENFORCEMENT_POINT:
				return policyEnforcementPoint != null;
			case _0Package.ACCESS_CONTROL_POLICY_POINTS_T__POLICY_INFORMATION_POINTS:
				return policyInformationPoints != null;
		}
		return super.eIsSet(featureID);
	}

} //AccessControlPolicyPointsTImpl
