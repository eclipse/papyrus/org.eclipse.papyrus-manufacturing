/**
 */
package io.shell.admin.aas.abac._2._0.impl;

import io.shell.admin.aas._2._0.PropertyT;

import io.shell.admin.aas.abac._2._0.ObjectAttributesT;
import io.shell.admin.aas.abac._2._0._0Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Object Attributes T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.ObjectAttributesTImpl#getObjectAttribute <em>Object Attribute</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ObjectAttributesTImpl extends MinimalEObjectImpl.Container implements ObjectAttributesT {
	/**
	 * The cached value of the '{@link #getObjectAttribute() <em>Object Attribute</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectAttribute()
	 * @generated
	 * @ordered
	 */
	protected EList<PropertyT> objectAttribute;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObjectAttributesTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.OBJECT_ATTRIBUTES_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PropertyT> getObjectAttribute() {
		if (objectAttribute == null) {
			objectAttribute = new EObjectContainmentEList<PropertyT>(PropertyT.class, this, _0Package.OBJECT_ATTRIBUTES_T__OBJECT_ATTRIBUTE);
		}
		return objectAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.OBJECT_ATTRIBUTES_T__OBJECT_ATTRIBUTE:
				return ((InternalEList<?>)getObjectAttribute()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.OBJECT_ATTRIBUTES_T__OBJECT_ATTRIBUTE:
				return getObjectAttribute();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.OBJECT_ATTRIBUTES_T__OBJECT_ATTRIBUTE:
				getObjectAttribute().clear();
				getObjectAttribute().addAll((Collection<? extends PropertyT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.OBJECT_ATTRIBUTES_T__OBJECT_ATTRIBUTE:
				getObjectAttribute().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.OBJECT_ATTRIBUTES_T__OBJECT_ATTRIBUTE:
				return objectAttribute != null && !objectAttribute.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ObjectAttributesTImpl
