/**
 */
package io.shell.admin.aas._2._0.impl;

import io.shell.admin.aas._2._0.SemanticIdT;
import io.shell.admin.aas._2._0._0Package;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Semantic Id T</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SemanticIdTImpl extends ReferenceTImpl implements SemanticIdT {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SemanticIdTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.SEMANTIC_ID_T;
	}

} //SemanticIdTImpl
