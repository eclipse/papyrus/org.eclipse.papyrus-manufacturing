/**
 */
package io.shell.admin.aas.abac._2._0.impl;

import io.shell.admin.aas.abac._2._0.InternalInformationPoints;
import io.shell.admin.aas.abac._2._0.PolicyInformationPointsT;
import io.shell.admin.aas.abac._2._0._0Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Policy Information Points T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.PolicyInformationPointsTImpl#isExternalInformationPoints <em>External Information Points</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.PolicyInformationPointsTImpl#getInternalInformationPoints <em>Internal Information Points</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PolicyInformationPointsTImpl extends MinimalEObjectImpl.Container implements PolicyInformationPointsT {
	/**
	 * The default value of the '{@link #isExternalInformationPoints() <em>External Information Points</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExternalInformationPoints()
	 * @generated
	 * @ordered
	 */
	protected static final boolean EXTERNAL_INFORMATION_POINTS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isExternalInformationPoints() <em>External Information Points</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExternalInformationPoints()
	 * @generated
	 * @ordered
	 */
	protected boolean externalInformationPoints = EXTERNAL_INFORMATION_POINTS_EDEFAULT;

	/**
	 * This is true if the External Information Points attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean externalInformationPointsESet;

	/**
	 * The cached value of the '{@link #getInternalInformationPoints() <em>Internal Information Points</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInternalInformationPoints()
	 * @generated
	 * @ordered
	 */
	protected InternalInformationPoints internalInformationPoints;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PolicyInformationPointsTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.POLICY_INFORMATION_POINTS_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isExternalInformationPoints() {
		return externalInformationPoints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExternalInformationPoints(boolean newExternalInformationPoints) {
		boolean oldExternalInformationPoints = externalInformationPoints;
		externalInformationPoints = newExternalInformationPoints;
		boolean oldExternalInformationPointsESet = externalInformationPointsESet;
		externalInformationPointsESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.POLICY_INFORMATION_POINTS_T__EXTERNAL_INFORMATION_POINTS, oldExternalInformationPoints, externalInformationPoints, !oldExternalInformationPointsESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetExternalInformationPoints() {
		boolean oldExternalInformationPoints = externalInformationPoints;
		boolean oldExternalInformationPointsESet = externalInformationPointsESet;
		externalInformationPoints = EXTERNAL_INFORMATION_POINTS_EDEFAULT;
		externalInformationPointsESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, _0Package.POLICY_INFORMATION_POINTS_T__EXTERNAL_INFORMATION_POINTS, oldExternalInformationPoints, EXTERNAL_INFORMATION_POINTS_EDEFAULT, oldExternalInformationPointsESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetExternalInformationPoints() {
		return externalInformationPointsESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InternalInformationPoints getInternalInformationPoints() {
		return internalInformationPoints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInternalInformationPoints(InternalInformationPoints newInternalInformationPoints, NotificationChain msgs) {
		InternalInformationPoints oldInternalInformationPoints = internalInformationPoints;
		internalInformationPoints = newInternalInformationPoints;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.POLICY_INFORMATION_POINTS_T__INTERNAL_INFORMATION_POINTS, oldInternalInformationPoints, newInternalInformationPoints);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInternalInformationPoints(InternalInformationPoints newInternalInformationPoints) {
		if (newInternalInformationPoints != internalInformationPoints) {
			NotificationChain msgs = null;
			if (internalInformationPoints != null)
				msgs = ((InternalEObject)internalInformationPoints).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.POLICY_INFORMATION_POINTS_T__INTERNAL_INFORMATION_POINTS, null, msgs);
			if (newInternalInformationPoints != null)
				msgs = ((InternalEObject)newInternalInformationPoints).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.POLICY_INFORMATION_POINTS_T__INTERNAL_INFORMATION_POINTS, null, msgs);
			msgs = basicSetInternalInformationPoints(newInternalInformationPoints, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.POLICY_INFORMATION_POINTS_T__INTERNAL_INFORMATION_POINTS, newInternalInformationPoints, newInternalInformationPoints));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.POLICY_INFORMATION_POINTS_T__INTERNAL_INFORMATION_POINTS:
				return basicSetInternalInformationPoints(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.POLICY_INFORMATION_POINTS_T__EXTERNAL_INFORMATION_POINTS:
				return isExternalInformationPoints();
			case _0Package.POLICY_INFORMATION_POINTS_T__INTERNAL_INFORMATION_POINTS:
				return getInternalInformationPoints();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.POLICY_INFORMATION_POINTS_T__EXTERNAL_INFORMATION_POINTS:
				setExternalInformationPoints((Boolean)newValue);
				return;
			case _0Package.POLICY_INFORMATION_POINTS_T__INTERNAL_INFORMATION_POINTS:
				setInternalInformationPoints((InternalInformationPoints)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.POLICY_INFORMATION_POINTS_T__EXTERNAL_INFORMATION_POINTS:
				unsetExternalInformationPoints();
				return;
			case _0Package.POLICY_INFORMATION_POINTS_T__INTERNAL_INFORMATION_POINTS:
				setInternalInformationPoints((InternalInformationPoints)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.POLICY_INFORMATION_POINTS_T__EXTERNAL_INFORMATION_POINTS:
				return isSetExternalInformationPoints();
			case _0Package.POLICY_INFORMATION_POINTS_T__INTERNAL_INFORMATION_POINTS:
				return internalInformationPoints != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (externalInformationPoints: ");
		if (externalInformationPointsESet) result.append(externalInformationPoints); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //PolicyInformationPointsTImpl
