/**
 */
package io.shell.admin.iec61360._2._0;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value List T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.iec61360._2._0.ValueListT#getValueReferencePair <em>Value Reference Pair</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.iec61360._2._0._0Package#getValueListT()
 * @model extendedMetaData="name='valueList_t' kind='elementOnly'"
 * @generated
 */
public interface ValueListT extends EObject {
	/**
	 * Returns the value of the '<em><b>Value Reference Pair</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.iec61360._2._0.ValueReferencePairT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Reference Pair</em>' containment reference list.
	 * @see io.shell.admin.iec61360._2._0._0Package#getValueListT_ValueReferencePair()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='valueReferencePair' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ValueReferencePairT> getValueReferencePair();

} // ValueListT
