/**
 */
package io.shell.admin.aas._2._0.impl;

import io.shell.admin.aas._2._0.EntityT;
import io.shell.admin.aas._2._0.EntityTypeType;
import io.shell.admin.aas._2._0.ReferenceT;
import io.shell.admin.aas._2._0.SubmodelElementsT;
import io.shell.admin.aas._2._0._0Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Entity T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.impl.EntityTImpl#getStatements <em>Statements</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.EntityTImpl#getEntityType <em>Entity Type</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.EntityTImpl#getAssetRef <em>Asset Ref</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EntityTImpl extends SubmodelElementAbstractTImpl implements EntityT {
	/**
	 * The cached value of the '{@link #getStatements() <em>Statements</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatements()
	 * @generated
	 * @ordered
	 */
	protected SubmodelElementsT statements;

	/**
	 * The default value of the '{@link #getEntityType() <em>Entity Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntityType()
	 * @generated
	 * @ordered
	 */
	protected static final EntityTypeType ENTITY_TYPE_EDEFAULT = EntityTypeType.CO_MANAGED_ENTITY;

	/**
	 * The cached value of the '{@link #getEntityType() <em>Entity Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntityType()
	 * @generated
	 * @ordered
	 */
	protected EntityTypeType entityType = ENTITY_TYPE_EDEFAULT;

	/**
	 * This is true if the Entity Type attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean entityTypeESet;

	/**
	 * The cached value of the '{@link #getAssetRef() <em>Asset Ref</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssetRef()
	 * @generated
	 * @ordered
	 */
	protected ReferenceT assetRef;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EntityTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.ENTITY_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubmodelElementsT getStatements() {
		return statements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStatements(SubmodelElementsT newStatements, NotificationChain msgs) {
		SubmodelElementsT oldStatements = statements;
		statements = newStatements;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ENTITY_T__STATEMENTS, oldStatements, newStatements);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStatements(SubmodelElementsT newStatements) {
		if (newStatements != statements) {
			NotificationChain msgs = null;
			if (statements != null)
				msgs = ((InternalEObject)statements).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ENTITY_T__STATEMENTS, null, msgs);
			if (newStatements != null)
				msgs = ((InternalEObject)newStatements).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ENTITY_T__STATEMENTS, null, msgs);
			msgs = basicSetStatements(newStatements, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ENTITY_T__STATEMENTS, newStatements, newStatements));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntityTypeType getEntityType() {
		return entityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntityType(EntityTypeType newEntityType) {
		EntityTypeType oldEntityType = entityType;
		entityType = newEntityType == null ? ENTITY_TYPE_EDEFAULT : newEntityType;
		boolean oldEntityTypeESet = entityTypeESet;
		entityTypeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ENTITY_T__ENTITY_TYPE, oldEntityType, entityType, !oldEntityTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetEntityType() {
		EntityTypeType oldEntityType = entityType;
		boolean oldEntityTypeESet = entityTypeESet;
		entityType = ENTITY_TYPE_EDEFAULT;
		entityTypeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, _0Package.ENTITY_T__ENTITY_TYPE, oldEntityType, ENTITY_TYPE_EDEFAULT, oldEntityTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetEntityType() {
		return entityTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceT getAssetRef() {
		return assetRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssetRef(ReferenceT newAssetRef, NotificationChain msgs) {
		ReferenceT oldAssetRef = assetRef;
		assetRef = newAssetRef;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ENTITY_T__ASSET_REF, oldAssetRef, newAssetRef);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssetRef(ReferenceT newAssetRef) {
		if (newAssetRef != assetRef) {
			NotificationChain msgs = null;
			if (assetRef != null)
				msgs = ((InternalEObject)assetRef).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ENTITY_T__ASSET_REF, null, msgs);
			if (newAssetRef != null)
				msgs = ((InternalEObject)newAssetRef).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ENTITY_T__ASSET_REF, null, msgs);
			msgs = basicSetAssetRef(newAssetRef, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ENTITY_T__ASSET_REF, newAssetRef, newAssetRef));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.ENTITY_T__STATEMENTS:
				return basicSetStatements(null, msgs);
			case _0Package.ENTITY_T__ASSET_REF:
				return basicSetAssetRef(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.ENTITY_T__STATEMENTS:
				return getStatements();
			case _0Package.ENTITY_T__ENTITY_TYPE:
				return getEntityType();
			case _0Package.ENTITY_T__ASSET_REF:
				return getAssetRef();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.ENTITY_T__STATEMENTS:
				setStatements((SubmodelElementsT)newValue);
				return;
			case _0Package.ENTITY_T__ENTITY_TYPE:
				setEntityType((EntityTypeType)newValue);
				return;
			case _0Package.ENTITY_T__ASSET_REF:
				setAssetRef((ReferenceT)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.ENTITY_T__STATEMENTS:
				setStatements((SubmodelElementsT)null);
				return;
			case _0Package.ENTITY_T__ENTITY_TYPE:
				unsetEntityType();
				return;
			case _0Package.ENTITY_T__ASSET_REF:
				setAssetRef((ReferenceT)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.ENTITY_T__STATEMENTS:
				return statements != null;
			case _0Package.ENTITY_T__ENTITY_TYPE:
				return isSetEntityType();
			case _0Package.ENTITY_T__ASSET_REF:
				return assetRef != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (entityType: ");
		if (entityTypeESet) result.append(entityType); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //EntityTImpl
