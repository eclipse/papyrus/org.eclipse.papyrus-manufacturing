/**
 */
package io.shell.admin.aas._2._0.impl;

import io.shell.admin.aas._2._0.ConceptDescriptionRefsT;
import io.shell.admin.aas._2._0.ReferenceT;
import io.shell.admin.aas._2._0._0Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Concept Description Refs T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.impl.ConceptDescriptionRefsTImpl#getConceptDescriptionRef <em>Concept Description Ref</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConceptDescriptionRefsTImpl extends MinimalEObjectImpl.Container implements ConceptDescriptionRefsT {
	/**
	 * The cached value of the '{@link #getConceptDescriptionRef() <em>Concept Description Ref</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConceptDescriptionRef()
	 * @generated
	 * @ordered
	 */
	protected EList<ReferenceT> conceptDescriptionRef;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConceptDescriptionRefsTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.CONCEPT_DESCRIPTION_REFS_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReferenceT> getConceptDescriptionRef() {
		if (conceptDescriptionRef == null) {
			conceptDescriptionRef = new EObjectContainmentEList<ReferenceT>(ReferenceT.class, this, _0Package.CONCEPT_DESCRIPTION_REFS_T__CONCEPT_DESCRIPTION_REF);
		}
		return conceptDescriptionRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.CONCEPT_DESCRIPTION_REFS_T__CONCEPT_DESCRIPTION_REF:
				return ((InternalEList<?>)getConceptDescriptionRef()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.CONCEPT_DESCRIPTION_REFS_T__CONCEPT_DESCRIPTION_REF:
				return getConceptDescriptionRef();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.CONCEPT_DESCRIPTION_REFS_T__CONCEPT_DESCRIPTION_REF:
				getConceptDescriptionRef().clear();
				getConceptDescriptionRef().addAll((Collection<? extends ReferenceT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.CONCEPT_DESCRIPTION_REFS_T__CONCEPT_DESCRIPTION_REF:
				getConceptDescriptionRef().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.CONCEPT_DESCRIPTION_REFS_T__CONCEPT_DESCRIPTION_REF:
				return conceptDescriptionRef != null && !conceptDescriptionRef.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ConceptDescriptionRefsTImpl
