/**
 */
package io.shell.admin.aas._2._0.util;

import io.shell.admin.aas._2._0.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see io.shell.admin.aas._2._0._0Package
 * @generated
 */
public class _0Switch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static _0Package modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public _0Switch() {
		if (modelPackage == null) {
			modelPackage = _0Package.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case _0Package.AASENV_T: {
				AasenvT aasenvT = (AasenvT)theEObject;
				T result = caseAasenvT(aasenvT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.ADMINISTRATION_T: {
				AdministrationT administrationT = (AdministrationT)theEObject;
				T result = caseAdministrationT(administrationT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.ANNOTATED_RELATIONSHIP_ELEMENT_T: {
				AnnotatedRelationshipElementT annotatedRelationshipElementT = (AnnotatedRelationshipElementT)theEObject;
				T result = caseAnnotatedRelationshipElementT(annotatedRelationshipElementT);
				if (result == null) result = caseRelationshipElementT(annotatedRelationshipElementT);
				if (result == null) result = caseSubmodelElementAbstractT(annotatedRelationshipElementT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.ASSET_ADMINISTRATION_SHELLS_T: {
				AssetAdministrationShellsT assetAdministrationShellsT = (AssetAdministrationShellsT)theEObject;
				T result = caseAssetAdministrationShellsT(assetAdministrationShellsT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.ASSET_ADMINISTRATION_SHELL_T: {
				AssetAdministrationShellT assetAdministrationShellT = (AssetAdministrationShellT)theEObject;
				T result = caseAssetAdministrationShellT(assetAdministrationShellT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.ASSETS_T: {
				AssetsT assetsT = (AssetsT)theEObject;
				T result = caseAssetsT(assetsT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.ASSET_T: {
				AssetT assetT = (AssetT)theEObject;
				T result = caseAssetT(assetT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.BASIC_EVENT_T: {
				BasicEventT basicEventT = (BasicEventT)theEObject;
				T result = caseBasicEventT(basicEventT);
				if (result == null) result = caseEventAbstractT(basicEventT);
				if (result == null) result = caseSubmodelElementAbstractT(basicEventT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.BLOB_T: {
				BlobT blobT = (BlobT)theEObject;
				T result = caseBlobT(blobT);
				if (result == null) result = caseSubmodelElementAbstractT(blobT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.BLOB_TYPE_T: {
				BlobTypeT blobTypeT = (BlobTypeT)theEObject;
				T result = caseBlobTypeT(blobTypeT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.CONCEPT_DESCRIPTION_REFS_T: {
				ConceptDescriptionRefsT conceptDescriptionRefsT = (ConceptDescriptionRefsT)theEObject;
				T result = caseConceptDescriptionRefsT(conceptDescriptionRefsT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.CONCEPT_DESCRIPTIONS_T: {
				ConceptDescriptionsT conceptDescriptionsT = (ConceptDescriptionsT)theEObject;
				T result = caseConceptDescriptionsT(conceptDescriptionsT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.CONCEPT_DESCRIPTION_T: {
				ConceptDescriptionT conceptDescriptionT = (ConceptDescriptionT)theEObject;
				T result = caseConceptDescriptionT(conceptDescriptionT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.CONCEPT_DICTIONARIES_T: {
				ConceptDictionariesT conceptDictionariesT = (ConceptDictionariesT)theEObject;
				T result = caseConceptDictionariesT(conceptDictionariesT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.CONCEPT_DICTIONARY_T: {
				ConceptDictionaryT conceptDictionaryT = (ConceptDictionaryT)theEObject;
				T result = caseConceptDictionaryT(conceptDictionaryT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.CONSTRAINT_T: {
				ConstraintT constraintT = (ConstraintT)theEObject;
				T result = caseConstraintT(constraintT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.CONTAINED_ELEMENTS_T: {
				ContainedElementsT containedElementsT = (ContainedElementsT)theEObject;
				T result = caseContainedElementsT(containedElementsT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.DATA_ELEMENT_ABSTRACT_T: {
				DataElementAbstractT dataElementAbstractT = (DataElementAbstractT)theEObject;
				T result = caseDataElementAbstractT(dataElementAbstractT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.DATA_ELEMENTS_T: {
				DataElementsT dataElementsT = (DataElementsT)theEObject;
				T result = caseDataElementsT(dataElementsT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.DATA_SPECIFICATION_CONTENT_T: {
				DataSpecificationContentT dataSpecificationContentT = (DataSpecificationContentT)theEObject;
				T result = caseDataSpecificationContentT(dataSpecificationContentT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.DATA_TYPE_DEF_T: {
				DataTypeDefT dataTypeDefT = (DataTypeDefT)theEObject;
				T result = caseDataTypeDefT(dataTypeDefT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.DOCUMENT_ROOT: {
				DocumentRoot documentRoot = (DocumentRoot)theEObject;
				T result = caseDocumentRoot(documentRoot);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.EMBEDDED_DATA_SPECIFICATION_T: {
				EmbeddedDataSpecificationT embeddedDataSpecificationT = (EmbeddedDataSpecificationT)theEObject;
				T result = caseEmbeddedDataSpecificationT(embeddedDataSpecificationT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.ENTITY_T: {
				EntityT entityT = (EntityT)theEObject;
				T result = caseEntityT(entityT);
				if (result == null) result = caseSubmodelElementAbstractT(entityT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.EVENT_ABSTRACT_T: {
				EventAbstractT eventAbstractT = (EventAbstractT)theEObject;
				T result = caseEventAbstractT(eventAbstractT);
				if (result == null) result = caseSubmodelElementAbstractT(eventAbstractT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.FILE_T: {
				FileT fileT = (FileT)theEObject;
				T result = caseFileT(fileT);
				if (result == null) result = caseSubmodelElementAbstractT(fileT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.FORMULA_T: {
				FormulaT formulaT = (FormulaT)theEObject;
				T result = caseFormulaT(formulaT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.IDENTIFICATION_T: {
				IdentificationT identificationT = (IdentificationT)theEObject;
				T result = caseIdentificationT(identificationT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.IDENTIFIER_T: {
				IdentifierT identifierT = (IdentifierT)theEObject;
				T result = caseIdentifierT(identifierT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.ID_PROPERTY_DEFINITION_T: {
				IdPropertyDefinitionT idPropertyDefinitionT = (IdPropertyDefinitionT)theEObject;
				T result = caseIdPropertyDefinitionT(idPropertyDefinitionT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.ID_SHORT_T: {
				IdShortT idShortT = (IdShortT)theEObject;
				T result = caseIdShortT(idShortT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.KEYS_T: {
				KeysT keysT = (KeysT)theEObject;
				T result = caseKeysT(keysT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.KEY_T: {
				KeyT keyT = (KeyT)theEObject;
				T result = caseKeyT(keyT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.LANG_STRING_SET_T: {
				LangStringSetT langStringSetT = (LangStringSetT)theEObject;
				T result = caseLangStringSetT(langStringSetT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.LANG_STRING_T: {
				LangStringT langStringT = (LangStringT)theEObject;
				T result = caseLangStringT(langStringT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.MULTI_LANGUAGE_PROPERTY_T: {
				MultiLanguagePropertyT multiLanguagePropertyT = (MultiLanguagePropertyT)theEObject;
				T result = caseMultiLanguagePropertyT(multiLanguagePropertyT);
				if (result == null) result = caseSubmodelElementAbstractT(multiLanguagePropertyT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.OPERATION_T: {
				OperationT operationT = (OperationT)theEObject;
				T result = caseOperationT(operationT);
				if (result == null) result = caseSubmodelElementAbstractT(operationT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.OPERATION_VARIABLE_T: {
				OperationVariableT operationVariableT = (OperationVariableT)theEObject;
				T result = caseOperationVariableT(operationVariableT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.PATH_TYPE_T: {
				PathTypeT pathTypeT = (PathTypeT)theEObject;
				T result = casePathTypeT(pathTypeT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.PROPERTY_T: {
				PropertyT propertyT = (PropertyT)theEObject;
				T result = casePropertyT(propertyT);
				if (result == null) result = caseSubmodelElementAbstractT(propertyT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.QUALIFIER_T: {
				QualifierT qualifierT = (QualifierT)theEObject;
				T result = caseQualifierT(qualifierT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.QUALIFIER_TYPE_T: {
				QualifierTypeT qualifierTypeT = (QualifierTypeT)theEObject;
				T result = caseQualifierTypeT(qualifierTypeT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.RANGE_T: {
				RangeT rangeT = (RangeT)theEObject;
				T result = caseRangeT(rangeT);
				if (result == null) result = caseSubmodelElementAbstractT(rangeT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.REFERENCE_ELEMENT_T: {
				ReferenceElementT referenceElementT = (ReferenceElementT)theEObject;
				T result = caseReferenceElementT(referenceElementT);
				if (result == null) result = caseSubmodelElementAbstractT(referenceElementT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.REFERENCES_T: {
				ReferencesT referencesT = (ReferencesT)theEObject;
				T result = caseReferencesT(referencesT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.REFERENCE_T: {
				ReferenceT referenceT = (ReferenceT)theEObject;
				T result = caseReferenceT(referenceT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.RELATIONSHIP_ELEMENT_T: {
				RelationshipElementT relationshipElementT = (RelationshipElementT)theEObject;
				T result = caseRelationshipElementT(relationshipElementT);
				if (result == null) result = caseSubmodelElementAbstractT(relationshipElementT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.SEMANTIC_ID_T: {
				SemanticIdT semanticIdT = (SemanticIdT)theEObject;
				T result = caseSemanticIdT(semanticIdT);
				if (result == null) result = caseReferenceT(semanticIdT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.SUBMODEL_ELEMENT_ABSTRACT_T: {
				SubmodelElementAbstractT submodelElementAbstractT = (SubmodelElementAbstractT)theEObject;
				T result = caseSubmodelElementAbstractT(submodelElementAbstractT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.SUBMODEL_ELEMENT_COLLECTION_T: {
				SubmodelElementCollectionT submodelElementCollectionT = (SubmodelElementCollectionT)theEObject;
				T result = caseSubmodelElementCollectionT(submodelElementCollectionT);
				if (result == null) result = caseSubmodelElementAbstractT(submodelElementCollectionT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.SUBMODEL_ELEMENTS_T: {
				SubmodelElementsT submodelElementsT = (SubmodelElementsT)theEObject;
				T result = caseSubmodelElementsT(submodelElementsT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.SUBMODEL_ELEMENT_T: {
				SubmodelElementT submodelElementT = (SubmodelElementT)theEObject;
				T result = caseSubmodelElementT(submodelElementT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.SUBMODEL_REFS_T: {
				SubmodelRefsT submodelRefsT = (SubmodelRefsT)theEObject;
				T result = caseSubmodelRefsT(submodelRefsT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.SUBMODELS_T: {
				SubmodelsT submodelsT = (SubmodelsT)theEObject;
				T result = caseSubmodelsT(submodelsT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.SUBMODEL_T: {
				SubmodelT submodelT = (SubmodelT)theEObject;
				T result = caseSubmodelT(submodelT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.VALUE_DATA_TYPE_T: {
				ValueDataTypeT valueDataTypeT = (ValueDataTypeT)theEObject;
				T result = caseValueDataTypeT(valueDataTypeT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.VIEWS_T: {
				ViewsT viewsT = (ViewsT)theEObject;
				T result = caseViewsT(viewsT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.VIEW_T: {
				ViewT viewT = (ViewT)theEObject;
				T result = caseViewT(viewT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aasenv T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aasenv T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAasenvT(AasenvT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Administration T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Administration T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAdministrationT(AdministrationT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotated Relationship Element T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotated Relationship Element T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotatedRelationshipElementT(AnnotatedRelationshipElementT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Asset Administration Shells T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Asset Administration Shells T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssetAdministrationShellsT(AssetAdministrationShellsT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Asset Administration Shell T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Asset Administration Shell T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssetAdministrationShellT(AssetAdministrationShellT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Assets T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Assets T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssetsT(AssetsT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Asset T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Asset T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssetT(AssetT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Basic Event T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Basic Event T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBasicEventT(BasicEventT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blob T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blob T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlobT(BlobT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blob Type T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blob Type T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlobTypeT(BlobTypeT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Concept Description Refs T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Concept Description Refs T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConceptDescriptionRefsT(ConceptDescriptionRefsT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Concept Descriptions T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Concept Descriptions T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConceptDescriptionsT(ConceptDescriptionsT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Concept Description T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Concept Description T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConceptDescriptionT(ConceptDescriptionT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Concept Dictionaries T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Concept Dictionaries T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConceptDictionariesT(ConceptDictionariesT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Concept Dictionary T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Concept Dictionary T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConceptDictionaryT(ConceptDictionaryT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constraint T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constraint T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstraintT(ConstraintT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Contained Elements T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Contained Elements T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContainedElementsT(ContainedElementsT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Element Abstract T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Element Abstract T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataElementAbstractT(DataElementAbstractT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Elements T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Elements T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataElementsT(DataElementsT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Specification Content T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Specification Content T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataSpecificationContentT(DataSpecificationContentT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Type Def T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Type Def T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataTypeDefT(DataTypeDefT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Document Root</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Document Root</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDocumentRoot(DocumentRoot object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Embedded Data Specification T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Embedded Data Specification T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEmbeddedDataSpecificationT(EmbeddedDataSpecificationT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Entity T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Entity T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEntityT(EntityT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event Abstract T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event Abstract T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEventAbstractT(EventAbstractT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>File T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>File T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFileT(FileT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formula T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formula T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormulaT(FormulaT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Identification T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Identification T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIdentificationT(IdentificationT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Identifier T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Identifier T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIdentifierT(IdentifierT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Id Property Definition T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Id Property Definition T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIdPropertyDefinitionT(IdPropertyDefinitionT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Id Short T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Id Short T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIdShortT(IdShortT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Keys T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Keys T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseKeysT(KeysT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Key T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Key T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseKeyT(KeyT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lang String Set T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lang String Set T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLangStringSetT(LangStringSetT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lang String T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lang String T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLangStringT(LangStringT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Multi Language Property T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Multi Language Property T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMultiLanguagePropertyT(MultiLanguagePropertyT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operation T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operation T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationT(OperationT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operation Variable T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operation Variable T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationVariableT(OperationVariableT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Path Type T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Path Type T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePathTypeT(PathTypeT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Property T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Property T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePropertyT(PropertyT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Qualifier T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Qualifier T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseQualifierT(QualifierT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Qualifier Type T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Qualifier Type T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseQualifierTypeT(QualifierTypeT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Range T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Range T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRangeT(RangeT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reference Element T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reference Element T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReferenceElementT(ReferenceElementT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>References T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>References T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReferencesT(ReferencesT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reference T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reference T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReferenceT(ReferenceT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Relationship Element T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Relationship Element T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRelationshipElementT(RelationshipElementT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Semantic Id T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Semantic Id T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSemanticIdT(SemanticIdT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Submodel Element Abstract T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Submodel Element Abstract T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubmodelElementAbstractT(SubmodelElementAbstractT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Submodel Element Collection T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Submodel Element Collection T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubmodelElementCollectionT(SubmodelElementCollectionT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Submodel Elements T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Submodel Elements T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubmodelElementsT(SubmodelElementsT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Submodel Element T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Submodel Element T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubmodelElementT(SubmodelElementT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Submodel Refs T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Submodel Refs T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubmodelRefsT(SubmodelRefsT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Submodels T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Submodels T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubmodelsT(SubmodelsT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Submodel T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Submodel T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubmodelT(SubmodelT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Value Data Type T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Value Data Type T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseValueDataTypeT(ValueDataTypeT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Views T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Views T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseViewsT(ViewsT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>View T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>View T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseViewT(ViewT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //_0Switch
