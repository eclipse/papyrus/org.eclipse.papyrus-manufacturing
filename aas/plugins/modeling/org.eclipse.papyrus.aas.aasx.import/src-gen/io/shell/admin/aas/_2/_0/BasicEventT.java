/**
 */
package io.shell.admin.aas._2._0;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Basic Event T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.BasicEventT#getObserved <em>Observed</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getBasicEventT()
 * @model extendedMetaData="name='basicEvent_t' kind='elementOnly'"
 * @generated
 */
public interface BasicEventT extends EventAbstractT {
	/**
	 * Returns the value of the '<em><b>Observed</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Observed</em>' containment reference.
	 * @see #setObserved(ReferenceT)
	 * @see io.shell.admin.aas._2._0._0Package#getBasicEventT_Observed()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='observed' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getObserved();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.BasicEventT#getObserved <em>Observed</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Observed</em>' containment reference.
	 * @see #getObserved()
	 * @generated
	 */
	void setObserved(ReferenceT value);

} // BasicEventT
