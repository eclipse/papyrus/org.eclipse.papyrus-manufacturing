/**
 */
package io.shell.admin.aas.abac._2._0;

import io.shell.admin.aas._2._0.ConstraintT;
import io.shell.admin.aas._2._0.IdShortT;
import io.shell.admin.aas._2._0.LangStringSetT;
import io.shell.admin.aas._2._0.ReferenceT;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Access Permission Rule T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.AccessPermissionRuleT#getQualifier <em>Qualifier</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.AccessPermissionRuleT#getIdShort <em>Id Short</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.AccessPermissionRuleT#getCategory <em>Category</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.AccessPermissionRuleT#getDescription <em>Description</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.AccessPermissionRuleT#getParent <em>Parent</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.AccessPermissionRuleT#getTargetSubjectAttributes <em>Target Subject Attributes</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.AccessPermissionRuleT#getPermissionsPerObject <em>Permissions Per Object</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas.abac._2._0._0Package#getAccessPermissionRuleT()
 * @model extendedMetaData="name='accessPermissionRule_t' kind='elementOnly'"
 * @generated
 */
public interface AccessPermissionRuleT extends EObject {
	/**
	 * Returns the value of the '<em><b>Qualifier</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas._2._0.ConstraintT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Qualifier</em>' containment reference list.
	 * @see io.shell.admin.aas.abac._2._0._0Package#getAccessPermissionRuleT_Qualifier()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='qualifier' namespace='http://www.admin-shell.io/aas/2/0'"
	 * @generated
	 */
	EList<ConstraintT> getQualifier();

	/**
	 * Returns the value of the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id Short</em>' containment reference.
	 * @see #setIdShort(IdShortT)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getAccessPermissionRuleT_IdShort()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='idShort' namespace='http://www.admin-shell.io/aas/2/0'"
	 * @generated
	 */
	IdShortT getIdShort();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.AccessPermissionRuleT#getIdShort <em>Id Short</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id Short</em>' containment reference.
	 * @see #getIdShort()
	 * @generated
	 */
	void setIdShort(IdShortT value);

	/**
	 * Returns the value of the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Category</em>' attribute.
	 * @see #setCategory(String)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getAccessPermissionRuleT_Category()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='category' namespace='http://www.admin-shell.io/aas/2/0'"
	 * @generated
	 */
	String getCategory();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.AccessPermissionRuleT#getCategory <em>Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Category</em>' attribute.
	 * @see #getCategory()
	 * @generated
	 */
	void setCategory(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference.
	 * @see #setDescription(LangStringSetT)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getAccessPermissionRuleT_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='description' namespace='http://www.admin-shell.io/aas/2/0'"
	 * @generated
	 */
	LangStringSetT getDescription();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.AccessPermissionRuleT#getDescription <em>Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' containment reference.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(LangStringSetT value);

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' containment reference.
	 * @see #setParent(ReferenceT)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getAccessPermissionRuleT_Parent()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='parent' namespace='http://www.admin-shell.io/aas/2/0'"
	 * @generated
	 */
	ReferenceT getParent();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.AccessPermissionRuleT#getParent <em>Parent</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' containment reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(ReferenceT value);

	/**
	 * Returns the value of the '<em><b>Target Subject Attributes</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas.abac._2._0.SubjectAttributesT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Subject Attributes</em>' containment reference list.
	 * @see io.shell.admin.aas.abac._2._0._0Package#getAccessPermissionRuleT_TargetSubjectAttributes()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='targetSubjectAttributes' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<SubjectAttributesT> getTargetSubjectAttributes();

	/**
	 * Returns the value of the '<em><b>Permissions Per Object</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas.abac._2._0.PermissionPerObjectT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Permissions Per Object</em>' containment reference list.
	 * @see io.shell.admin.aas.abac._2._0._0Package#getAccessPermissionRuleT_PermissionsPerObject()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='permissionsPerObject' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PermissionPerObjectT> getPermissionsPerObject();

} // AccessPermissionRuleT
