/**
 */
package io.shell.admin.iec61360._2._0.impl;

import io.shell.admin.iec61360._2._0.CodeT;
import io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T;
import io.shell.admin.iec61360._2._0.DataTypeIEC61360T;
import io.shell.admin.iec61360._2._0.DocumentRoot;
import io.shell.admin.iec61360._2._0.IdTypeType;
import io.shell.admin.iec61360._2._0.KeyT;
import io.shell.admin.iec61360._2._0.KeysT;
import io.shell.admin.iec61360._2._0.LangStringSetT;
import io.shell.admin.iec61360._2._0.LangStringT;
import io.shell.admin.iec61360._2._0.LevelTypeT;
import io.shell.admin.iec61360._2._0.ReferenceT;
import io.shell.admin.iec61360._2._0.TypeType;
import io.shell.admin.iec61360._2._0.ValueDataTypeT;
import io.shell.admin.iec61360._2._0.ValueListT;
import io.shell.admin.iec61360._2._0.ValueReferencePairT;
import io.shell.admin.iec61360._2._0._0Factory;
import io.shell.admin.iec61360._2._0._0Package;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class _0PackageImpl extends EPackageImpl implements _0Package {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass codeTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataSpecificationIEC61630TEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass keysTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass keyTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass langStringSetTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass langStringTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass referenceTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valueDataTypeTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valueListTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valueReferencePairTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass documentRootEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dataTypeIEC61360TEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum idTypeTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum levelTypeTEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum typeTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType dataTypeIEC61360TObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType idTypeTypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType levelTypeTObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType typeTypeObjectEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see io.shell.admin.iec61360._2._0._0Package#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private _0PackageImpl() {
		super(eNS_URI, _0Factory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link _0Package#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static _0Package init() {
		if (isInited) return (_0Package)EPackage.Registry.INSTANCE.getEPackage(_0Package.eNS_URI);

		// Obtain or create and register package
		Object registered_0Package = EPackage.Registry.INSTANCE.get(eNS_URI);
		_0PackageImpl the_0Package = registered_0Package instanceof _0PackageImpl ? (_0PackageImpl)registered_0Package : new _0PackageImpl();

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(io.shell.admin.aas._2._0._0Package.eNS_URI);
		io.shell.admin.aas._2._0.impl._0PackageImpl the_0Package_1 = (io.shell.admin.aas._2._0.impl._0PackageImpl)(registeredPackage instanceof io.shell.admin.aas._2._0.impl._0PackageImpl ? registeredPackage : io.shell.admin.aas._2._0._0Package.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(io.shell.admin.aas.abac._2._0._0Package.eNS_URI);
		io.shell.admin.aas.abac._2._0.impl._0PackageImpl the_0Package_2 = (io.shell.admin.aas.abac._2._0.impl._0PackageImpl)(registeredPackage instanceof io.shell.admin.aas.abac._2._0.impl._0PackageImpl ? registeredPackage : io.shell.admin.aas.abac._2._0._0Package.eINSTANCE);

		// Create package meta-data objects
		the_0Package.createPackageContents();
		the_0Package_1.createPackageContents();
		the_0Package_2.createPackageContents();

		// Initialize created meta-data
		the_0Package.initializePackageContents();
		the_0Package_1.initializePackageContents();
		the_0Package_2.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		the_0Package.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(_0Package.eNS_URI, the_0Package);
		return the_0Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCodeT() {
		return codeTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataSpecificationIEC61630T() {
		return dataSpecificationIEC61630TEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataSpecificationIEC61630T_Group() {
		return (EAttribute)dataSpecificationIEC61630TEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataSpecificationIEC61630T_PreferredName() {
		return (EReference)dataSpecificationIEC61630TEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataSpecificationIEC61630T_ShortName() {
		return (EReference)dataSpecificationIEC61630TEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataSpecificationIEC61630T_Unit() {
		return (EAttribute)dataSpecificationIEC61630TEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataSpecificationIEC61630T_UnitId() {
		return (EReference)dataSpecificationIEC61630TEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataSpecificationIEC61630T_SourceOfDefinition() {
		return (EAttribute)dataSpecificationIEC61630TEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataSpecificationIEC61630T_Symbol() {
		return (EAttribute)dataSpecificationIEC61630TEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataSpecificationIEC61630T_DataType() {
		return (EAttribute)dataSpecificationIEC61630TEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataSpecificationIEC61630T_Definition() {
		return (EReference)dataSpecificationIEC61630TEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataSpecificationIEC61630T_ValueFormat() {
		return (EAttribute)dataSpecificationIEC61630TEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataSpecificationIEC61630T_ValueList() {
		return (EReference)dataSpecificationIEC61630TEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataSpecificationIEC61630T_Value() {
		return (EReference)dataSpecificationIEC61630TEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataSpecificationIEC61630T_ValueId() {
		return (EReference)dataSpecificationIEC61630TEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataSpecificationIEC61630T_LevelType() {
		return (EAttribute)dataSpecificationIEC61630TEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getKeysT() {
		return keysTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getKeysT_Key() {
		return (EReference)keysTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getKeyT() {
		return keyTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getKeyT_Value() {
		return (EAttribute)keyTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getKeyT_IdType() {
		return (EAttribute)keyTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getKeyT_Local() {
		return (EAttribute)keyTEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getKeyT_Type() {
		return (EAttribute)keyTEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLangStringSetT() {
		return langStringSetTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLangStringSetT_LangString() {
		return (EReference)langStringSetTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLangStringT() {
		return langStringTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLangStringT_Value() {
		return (EAttribute)langStringTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLangStringT_Lang() {
		return (EAttribute)langStringTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReferenceT() {
		return referenceTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReferenceT_Keys() {
		return (EReference)referenceTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getValueDataTypeT() {
		return valueDataTypeTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getValueDataTypeT_Value() {
		return (EAttribute)valueDataTypeTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getValueListT() {
		return valueListTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getValueListT_ValueReferencePair() {
		return (EReference)valueListTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getValueReferencePairT() {
		return valueReferencePairTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getValueReferencePairT_ValueId() {
		return (EReference)valueReferencePairTEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getValueReferencePairT_Value() {
		return (EReference)valueReferencePairTEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDocumentRoot() {
		return documentRootEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDocumentRoot_Mixed() {
		return (EAttribute)documentRootEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_XMLNSPrefixMap() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_XSISchemaLocation() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_Key() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDataTypeIEC61360T() {
		return dataTypeIEC61360TEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getIdTypeType() {
		return idTypeTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getLevelTypeT() {
		return levelTypeTEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getTypeType() {
		return typeTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getDataTypeIEC61360TObject() {
		return dataTypeIEC61360TObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getIdTypeTypeObject() {
		return idTypeTypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getLevelTypeTObject() {
		return levelTypeTObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getTypeTypeObject() {
		return typeTypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public _0Factory get_0Factory() {
		return (_0Factory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		codeTEClass = createEClass(CODE_T);

		dataSpecificationIEC61630TEClass = createEClass(DATA_SPECIFICATION_IEC61630T);
		createEAttribute(dataSpecificationIEC61630TEClass, DATA_SPECIFICATION_IEC61630T__GROUP);
		createEReference(dataSpecificationIEC61630TEClass, DATA_SPECIFICATION_IEC61630T__PREFERRED_NAME);
		createEReference(dataSpecificationIEC61630TEClass, DATA_SPECIFICATION_IEC61630T__SHORT_NAME);
		createEAttribute(dataSpecificationIEC61630TEClass, DATA_SPECIFICATION_IEC61630T__UNIT);
		createEReference(dataSpecificationIEC61630TEClass, DATA_SPECIFICATION_IEC61630T__UNIT_ID);
		createEAttribute(dataSpecificationIEC61630TEClass, DATA_SPECIFICATION_IEC61630T__SOURCE_OF_DEFINITION);
		createEAttribute(dataSpecificationIEC61630TEClass, DATA_SPECIFICATION_IEC61630T__SYMBOL);
		createEAttribute(dataSpecificationIEC61630TEClass, DATA_SPECIFICATION_IEC61630T__DATA_TYPE);
		createEReference(dataSpecificationIEC61630TEClass, DATA_SPECIFICATION_IEC61630T__DEFINITION);
		createEAttribute(dataSpecificationIEC61630TEClass, DATA_SPECIFICATION_IEC61630T__VALUE_FORMAT);
		createEReference(dataSpecificationIEC61630TEClass, DATA_SPECIFICATION_IEC61630T__VALUE_LIST);
		createEReference(dataSpecificationIEC61630TEClass, DATA_SPECIFICATION_IEC61630T__VALUE);
		createEReference(dataSpecificationIEC61630TEClass, DATA_SPECIFICATION_IEC61630T__VALUE_ID);
		createEAttribute(dataSpecificationIEC61630TEClass, DATA_SPECIFICATION_IEC61630T__LEVEL_TYPE);

		keysTEClass = createEClass(KEYS_T);
		createEReference(keysTEClass, KEYS_T__KEY);

		keyTEClass = createEClass(KEY_T);
		createEAttribute(keyTEClass, KEY_T__VALUE);
		createEAttribute(keyTEClass, KEY_T__ID_TYPE);
		createEAttribute(keyTEClass, KEY_T__LOCAL);
		createEAttribute(keyTEClass, KEY_T__TYPE);

		langStringSetTEClass = createEClass(LANG_STRING_SET_T);
		createEReference(langStringSetTEClass, LANG_STRING_SET_T__LANG_STRING);

		langStringTEClass = createEClass(LANG_STRING_T);
		createEAttribute(langStringTEClass, LANG_STRING_T__VALUE);
		createEAttribute(langStringTEClass, LANG_STRING_T__LANG);

		referenceTEClass = createEClass(REFERENCE_T);
		createEReference(referenceTEClass, REFERENCE_T__KEYS);

		valueDataTypeTEClass = createEClass(VALUE_DATA_TYPE_T);
		createEAttribute(valueDataTypeTEClass, VALUE_DATA_TYPE_T__VALUE);

		valueListTEClass = createEClass(VALUE_LIST_T);
		createEReference(valueListTEClass, VALUE_LIST_T__VALUE_REFERENCE_PAIR);

		valueReferencePairTEClass = createEClass(VALUE_REFERENCE_PAIR_T);
		createEReference(valueReferencePairTEClass, VALUE_REFERENCE_PAIR_T__VALUE_ID);
		createEReference(valueReferencePairTEClass, VALUE_REFERENCE_PAIR_T__VALUE);

		documentRootEClass = createEClass(DOCUMENT_ROOT);
		createEAttribute(documentRootEClass, DOCUMENT_ROOT__MIXED);
		createEReference(documentRootEClass, DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
		createEReference(documentRootEClass, DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
		createEReference(documentRootEClass, DOCUMENT_ROOT__KEY);

		// Create enums
		dataTypeIEC61360TEEnum = createEEnum(DATA_TYPE_IEC61360T);
		idTypeTypeEEnum = createEEnum(ID_TYPE_TYPE);
		levelTypeTEEnum = createEEnum(LEVEL_TYPE_T);
		typeTypeEEnum = createEEnum(TYPE_TYPE);

		// Create data types
		dataTypeIEC61360TObjectEDataType = createEDataType(DATA_TYPE_IEC61360T_OBJECT);
		idTypeTypeObjectEDataType = createEDataType(ID_TYPE_TYPE_OBJECT);
		levelTypeTObjectEDataType = createEDataType(LEVEL_TYPE_TOBJECT);
		typeTypeObjectEDataType = createEDataType(TYPE_TYPE_OBJECT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(codeTEClass, CodeT.class, "CodeT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(dataSpecificationIEC61630TEClass, DataSpecificationIEC61630T.class, "DataSpecificationIEC61630T", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDataSpecificationIEC61630T_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, DataSpecificationIEC61630T.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataSpecificationIEC61630T_PreferredName(), this.getLangStringSetT(), null, "preferredName", null, 0, -1, DataSpecificationIEC61630T.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDataSpecificationIEC61630T_ShortName(), this.getLangStringSetT(), null, "shortName", null, 0, -1, DataSpecificationIEC61630T.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getDataSpecificationIEC61630T_Unit(), theXMLTypePackage.getString(), "unit", null, 0, -1, DataSpecificationIEC61630T.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDataSpecificationIEC61630T_UnitId(), this.getReferenceT(), null, "unitId", null, 0, -1, DataSpecificationIEC61630T.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getDataSpecificationIEC61630T_SourceOfDefinition(), theXMLTypePackage.getString(), "sourceOfDefinition", null, 0, -1, DataSpecificationIEC61630T.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getDataSpecificationIEC61630T_Symbol(), theXMLTypePackage.getString(), "symbol", null, 0, -1, DataSpecificationIEC61630T.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getDataSpecificationIEC61630T_DataType(), this.getDataTypeIEC61360T(), "dataType", null, 0, -1, DataSpecificationIEC61630T.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDataSpecificationIEC61630T_Definition(), this.getLangStringSetT(), null, "definition", null, 0, -1, DataSpecificationIEC61630T.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getDataSpecificationIEC61630T_ValueFormat(), theXMLTypePackage.getString(), "valueFormat", null, 0, -1, DataSpecificationIEC61630T.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDataSpecificationIEC61630T_ValueList(), this.getValueListT(), null, "valueList", null, 0, -1, DataSpecificationIEC61630T.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDataSpecificationIEC61630T_Value(), this.getValueDataTypeT(), null, "value", null, 0, -1, DataSpecificationIEC61630T.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDataSpecificationIEC61630T_ValueId(), this.getReferenceT(), null, "valueId", null, 0, -1, DataSpecificationIEC61630T.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getDataSpecificationIEC61630T_LevelType(), this.getLevelTypeT(), "levelType", null, 0, -1, DataSpecificationIEC61630T.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(keysTEClass, KeysT.class, "KeysT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getKeysT_Key(), this.getKeyT(), null, "key", null, 0, -1, KeysT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(keyTEClass, KeyT.class, "KeyT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getKeyT_Value(), theXMLTypePackage.getString(), "value", null, 0, 1, KeyT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getKeyT_IdType(), this.getIdTypeType(), "idType", null, 0, 1, KeyT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getKeyT_Local(), theXMLTypePackage.getBoolean(), "local", null, 0, 1, KeyT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getKeyT_Type(), this.getTypeType(), "type", null, 0, 1, KeyT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(langStringSetTEClass, LangStringSetT.class, "LangStringSetT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLangStringSetT_LangString(), this.getLangStringT(), null, "langString", null, 1, -1, LangStringSetT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(langStringTEClass, LangStringT.class, "LangStringT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLangStringT_Value(), theXMLTypePackage.getString(), "value", null, 0, 1, LangStringT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLangStringT_Lang(), theXMLTypePackage.getString(), "lang", null, 0, 1, LangStringT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(referenceTEClass, ReferenceT.class, "ReferenceT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReferenceT_Keys(), this.getKeysT(), null, "keys", null, 1, 1, ReferenceT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(valueDataTypeTEClass, ValueDataTypeT.class, "ValueDataTypeT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getValueDataTypeT_Value(), theXMLTypePackage.getAnySimpleType(), "value", null, 0, 1, ValueDataTypeT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(valueListTEClass, ValueListT.class, "ValueListT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getValueListT_ValueReferencePair(), this.getValueReferencePairT(), null, "valueReferencePair", null, 1, -1, ValueListT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(valueReferencePairTEClass, ValueReferencePairT.class, "ValueReferencePairT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getValueReferencePairT_ValueId(), this.getReferenceT(), null, "valueId", null, 1, 1, ValueReferencePairT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getValueReferencePairT_Value(), this.getValueDataTypeT(), null, "value", null, 1, 1, ValueReferencePairT.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(documentRootEClass, DocumentRoot.class, "DocumentRoot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDocumentRoot_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, null, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_XMLNSPrefixMap(), ecorePackage.getEStringToStringMapEntry(), null, "xMLNSPrefixMap", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_XSISchemaLocation(), ecorePackage.getEStringToStringMapEntry(), null, "xSISchemaLocation", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_Key(), this.getKeyT(), null, "key", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(dataTypeIEC61360TEEnum, DataTypeIEC61360T.class, "DataTypeIEC61360T");
		addEEnumLiteral(dataTypeIEC61360TEEnum, DataTypeIEC61360T.__);
		addEEnumLiteral(dataTypeIEC61360TEEnum, DataTypeIEC61360T.BOOLEAN);
		addEEnumLiteral(dataTypeIEC61360TEEnum, DataTypeIEC61360T.DATE);
		addEEnumLiteral(dataTypeIEC61360TEEnum, DataTypeIEC61360T.RATIONAL);
		addEEnumLiteral(dataTypeIEC61360TEEnum, DataTypeIEC61360T.RATIONALMEASURE);
		addEEnumLiteral(dataTypeIEC61360TEEnum, DataTypeIEC61360T.REALCOUNT);
		addEEnumLiteral(dataTypeIEC61360TEEnum, DataTypeIEC61360T.REALCURRENCY);
		addEEnumLiteral(dataTypeIEC61360TEEnum, DataTypeIEC61360T.REALMEASURE);
		addEEnumLiteral(dataTypeIEC61360TEEnum, DataTypeIEC61360T.STRING);
		addEEnumLiteral(dataTypeIEC61360TEEnum, DataTypeIEC61360T.STRINGTRANSLATABLE);
		addEEnumLiteral(dataTypeIEC61360TEEnum, DataTypeIEC61360T.TIME);
		addEEnumLiteral(dataTypeIEC61360TEEnum, DataTypeIEC61360T.TIMESTAMP);
		addEEnumLiteral(dataTypeIEC61360TEEnum, DataTypeIEC61360T.URL);
		addEEnumLiteral(dataTypeIEC61360TEEnum, DataTypeIEC61360T.INTEGERCOUNT);
		addEEnumLiteral(dataTypeIEC61360TEEnum, DataTypeIEC61360T.INTEGERCURRENCY);
		addEEnumLiteral(dataTypeIEC61360TEEnum, DataTypeIEC61360T.INTEGERMEASURE);

		initEEnum(idTypeTypeEEnum, IdTypeType.class, "IdTypeType");
		addEEnumLiteral(idTypeTypeEEnum, IdTypeType.CUSTOM);
		addEEnumLiteral(idTypeTypeEEnum, IdTypeType.FRAGEMENT_ID);
		addEEnumLiteral(idTypeTypeEEnum, IdTypeType.ID_SHORT);
		addEEnumLiteral(idTypeTypeEEnum, IdTypeType.IRDI);
		addEEnumLiteral(idTypeTypeEEnum, IdTypeType.IRI);

		initEEnum(levelTypeTEEnum, LevelTypeT.class, "LevelTypeT");
		addEEnumLiteral(levelTypeTEEnum, LevelTypeT.MAX);
		addEEnumLiteral(levelTypeTEEnum, LevelTypeT.MIN);
		addEEnumLiteral(levelTypeTEEnum, LevelTypeT.NOM);
		addEEnumLiteral(levelTypeTEEnum, LevelTypeT.TYP);

		initEEnum(typeTypeEEnum, TypeType.class, "TypeType");
		addEEnumLiteral(typeTypeEEnum, TypeType.ACCESS_PERMISSION_RULE);
		addEEnumLiteral(typeTypeEEnum, TypeType.ANNOTATED_RELATIONSHIP_ELEMENT);
		addEEnumLiteral(typeTypeEEnum, TypeType.ASSET);
		addEEnumLiteral(typeTypeEEnum, TypeType.ASSET_ADMINISTRATION_SHELL);
		addEEnumLiteral(typeTypeEEnum, TypeType.BASIC_EVENT);
		addEEnumLiteral(typeTypeEEnum, TypeType.BLOB);
		addEEnumLiteral(typeTypeEEnum, TypeType.CAPABILITY);
		addEEnumLiteral(typeTypeEEnum, TypeType.CONCEPT_DESCRIPTION);
		addEEnumLiteral(typeTypeEEnum, TypeType.CONCEPT_DICTIONARY);
		addEEnumLiteral(typeTypeEEnum, TypeType.DATA_ELEMENT);
		addEEnumLiteral(typeTypeEEnum, TypeType.ENTITY);
		addEEnumLiteral(typeTypeEEnum, TypeType.EVENT);
		addEEnumLiteral(typeTypeEEnum, TypeType.FILE);
		addEEnumLiteral(typeTypeEEnum, TypeType.FRAGMENT_REFERENCE);
		addEEnumLiteral(typeTypeEEnum, TypeType.GLOBAL_REFERENCE);
		addEEnumLiteral(typeTypeEEnum, TypeType.MULTI_LANGUAGE_PROPERTY);
		addEEnumLiteral(typeTypeEEnum, TypeType.OPERATION);
		addEEnumLiteral(typeTypeEEnum, TypeType.PROPERTY);
		addEEnumLiteral(typeTypeEEnum, TypeType.RANGE);
		addEEnumLiteral(typeTypeEEnum, TypeType.REFERENCE_ELEMENT);
		addEEnumLiteral(typeTypeEEnum, TypeType.RELATIONSHIP_ELEMENT);
		addEEnumLiteral(typeTypeEEnum, TypeType.SUBMODEL);
		addEEnumLiteral(typeTypeEEnum, TypeType.SUBMODEL_ELEMENT);
		addEEnumLiteral(typeTypeEEnum, TypeType.SUBMODEL_ELEMENT_COLLECTION);
		addEEnumLiteral(typeTypeEEnum, TypeType.VIEW);

		// Initialize data types
		initEDataType(dataTypeIEC61360TObjectEDataType, DataTypeIEC61360T.class, "DataTypeIEC61360TObject", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(idTypeTypeObjectEDataType, IdTypeType.class, "IdTypeTypeObject", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(levelTypeTObjectEDataType, LevelTypeT.class, "LevelTypeTObject", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(typeTypeObjectEDataType, TypeType.class, "TypeTypeObject", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http:///org/eclipse/emf/ecore/util/ExtendedMetaData
		createExtendedMetaDataAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createExtendedMetaDataAnnotations() {
		String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";
		addAnnotation
		  (codeTEClass,
		   source,
		   new String[] {
			   "name", "code_t",
			   "kind", "empty"
		   });
		addAnnotation
		  (dataSpecificationIEC61630TEClass,
		   source,
		   new String[] {
			   "name", "dataSpecificationIEC61630_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getDataSpecificationIEC61630T_Group(),
		   source,
		   new String[] {
			   "kind", "group",
			   "name", "group:0"
		   });
		addAnnotation
		  (getDataSpecificationIEC61630T_PreferredName(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "preferredName",
			   "namespace", "##targetNamespace",
			   "group", "#group:0"
		   });
		addAnnotation
		  (getDataSpecificationIEC61630T_ShortName(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "shortName",
			   "namespace", "##targetNamespace",
			   "group", "#group:0"
		   });
		addAnnotation
		  (getDataSpecificationIEC61630T_Unit(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "unit",
			   "namespace", "##targetNamespace",
			   "group", "#group:0"
		   });
		addAnnotation
		  (getDataSpecificationIEC61630T_UnitId(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "unitId",
			   "namespace", "##targetNamespace",
			   "group", "#group:0"
		   });
		addAnnotation
		  (getDataSpecificationIEC61630T_SourceOfDefinition(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "sourceOfDefinition",
			   "namespace", "##targetNamespace",
			   "group", "#group:0"
		   });
		addAnnotation
		  (getDataSpecificationIEC61630T_Symbol(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "symbol",
			   "namespace", "##targetNamespace",
			   "group", "#group:0"
		   });
		addAnnotation
		  (getDataSpecificationIEC61630T_DataType(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "dataType",
			   "namespace", "##targetNamespace",
			   "group", "#group:0"
		   });
		addAnnotation
		  (getDataSpecificationIEC61630T_Definition(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "definition",
			   "namespace", "##targetNamespace",
			   "group", "#group:0"
		   });
		addAnnotation
		  (getDataSpecificationIEC61630T_ValueFormat(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "valueFormat",
			   "namespace", "##targetNamespace",
			   "group", "#group:0"
		   });
		addAnnotation
		  (getDataSpecificationIEC61630T_ValueList(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "valueList",
			   "namespace", "##targetNamespace",
			   "group", "#group:0"
		   });
		addAnnotation
		  (getDataSpecificationIEC61630T_Value(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "value",
			   "namespace", "##targetNamespace",
			   "group", "#group:0"
		   });
		addAnnotation
		  (getDataSpecificationIEC61630T_ValueId(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "valueId",
			   "namespace", "##targetNamespace",
			   "group", "#group:0"
		   });
		addAnnotation
		  (getDataSpecificationIEC61630T_LevelType(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "levelType",
			   "namespace", "##targetNamespace",
			   "group", "#group:0"
		   });
		addAnnotation
		  (dataTypeIEC61360TEEnum,
		   source,
		   new String[] {
			   "name", "dataTypeIEC61360_t"
		   });
		addAnnotation
		  (dataTypeIEC61360TObjectEDataType,
		   source,
		   new String[] {
			   "name", "dataTypeIEC61360_t:Object",
			   "baseType", "dataTypeIEC61360_t"
		   });
		addAnnotation
		  (idTypeTypeEEnum,
		   source,
		   new String[] {
			   "name", "idType_._type"
		   });
		addAnnotation
		  (idTypeTypeObjectEDataType,
		   source,
		   new String[] {
			   "name", "idType_._type:Object",
			   "baseType", "idType_._type"
		   });
		addAnnotation
		  (keysTEClass,
		   source,
		   new String[] {
			   "name", "keys_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getKeysT_Key(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "key",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (keyTEClass,
		   source,
		   new String[] {
			   "name", "key_t",
			   "kind", "simple"
		   });
		addAnnotation
		  (getKeyT_Value(),
		   source,
		   new String[] {
			   "name", ":0",
			   "kind", "simple"
		   });
		addAnnotation
		  (getKeyT_IdType(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "idType"
		   });
		addAnnotation
		  (getKeyT_Local(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "local"
		   });
		addAnnotation
		  (getKeyT_Type(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "type"
		   });
		addAnnotation
		  (langStringSetTEClass,
		   source,
		   new String[] {
			   "name", "langStringSet_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getLangStringSetT_LangString(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "langString",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (langStringTEClass,
		   source,
		   new String[] {
			   "name", "langString_t",
			   "kind", "simple"
		   });
		addAnnotation
		  (getLangStringT_Value(),
		   source,
		   new String[] {
			   "name", ":0",
			   "kind", "simple"
		   });
		addAnnotation
		  (getLangStringT_Lang(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "lang"
		   });
		addAnnotation
		  (levelTypeTEEnum,
		   source,
		   new String[] {
			   "name", "levelType_t"
		   });
		addAnnotation
		  (levelTypeTObjectEDataType,
		   source,
		   new String[] {
			   "name", "levelType_t:Object",
			   "baseType", "levelType_t"
		   });
		addAnnotation
		  (referenceTEClass,
		   source,
		   new String[] {
			   "name", "reference_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getReferenceT_Keys(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "keys",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (typeTypeEEnum,
		   source,
		   new String[] {
			   "name", "type_._type"
		   });
		addAnnotation
		  (typeTypeObjectEDataType,
		   source,
		   new String[] {
			   "name", "type_._type:Object",
			   "baseType", "type_._type"
		   });
		addAnnotation
		  (valueDataTypeTEClass,
		   source,
		   new String[] {
			   "name", "valueDataType_t",
			   "kind", "simple"
		   });
		addAnnotation
		  (getValueDataTypeT_Value(),
		   source,
		   new String[] {
			   "name", ":0",
			   "kind", "simple"
		   });
		addAnnotation
		  (valueListTEClass,
		   source,
		   new String[] {
			   "name", "valueList_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getValueListT_ValueReferencePair(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "valueReferencePair",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (valueReferencePairTEClass,
		   source,
		   new String[] {
			   "name", "valueReferencePair_t",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getValueReferencePairT_ValueId(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "valueId",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getValueReferencePairT_Value(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "value",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (documentRootEClass,
		   source,
		   new String[] {
			   "name", "",
			   "kind", "mixed"
		   });
		addAnnotation
		  (getDocumentRoot_Mixed(),
		   source,
		   new String[] {
			   "kind", "elementWildcard",
			   "name", ":mixed"
		   });
		addAnnotation
		  (getDocumentRoot_XMLNSPrefixMap(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "xmlns:prefix"
		   });
		addAnnotation
		  (getDocumentRoot_XSISchemaLocation(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "xsi:schemaLocation"
		   });
		addAnnotation
		  (getDocumentRoot_Key(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "key",
			   "namespace", "##targetNamespace"
		   });
	}

} //_0PackageImpl
