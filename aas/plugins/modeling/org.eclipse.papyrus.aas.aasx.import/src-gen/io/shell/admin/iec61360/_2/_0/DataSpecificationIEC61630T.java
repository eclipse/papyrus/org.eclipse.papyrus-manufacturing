/**
 */
package io.shell.admin.iec61360._2._0;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Specification IEC61630T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getGroup <em>Group</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getPreferredName <em>Preferred Name</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getShortName <em>Short Name</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getUnit <em>Unit</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getUnitId <em>Unit Id</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getSourceOfDefinition <em>Source Of Definition</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getSymbol <em>Symbol</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getDataType <em>Data Type</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getDefinition <em>Definition</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getValueFormat <em>Value Format</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getValueList <em>Value List</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getValue <em>Value</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getValueId <em>Value Id</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getLevelType <em>Level Type</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.iec61360._2._0._0Package#getDataSpecificationIEC61630T()
 * @model extendedMetaData="name='dataSpecificationIEC61630_t' kind='elementOnly'"
 * @generated
 */
public interface DataSpecificationIEC61630T extends EObject {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' attribute list.
	 * @see io.shell.admin.iec61360._2._0._0Package#getDataSpecificationIEC61630T_Group()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:0'"
	 * @generated
	 */
	FeatureMap getGroup();

	/**
	 * Returns the value of the '<em><b>Preferred Name</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.iec61360._2._0.LangStringSetT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preferred Name</em>' containment reference list.
	 * @see io.shell.admin.iec61360._2._0._0Package#getDataSpecificationIEC61630T_PreferredName()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='preferredName' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<LangStringSetT> getPreferredName();

	/**
	 * Returns the value of the '<em><b>Short Name</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.iec61360._2._0.LangStringSetT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Short Name</em>' containment reference list.
	 * @see io.shell.admin.iec61360._2._0._0Package#getDataSpecificationIEC61630T_ShortName()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='shortName' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<LangStringSetT> getShortName();

	/**
	 * Returns the value of the '<em><b>Unit</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit</em>' attribute list.
	 * @see io.shell.admin.iec61360._2._0._0Package#getDataSpecificationIEC61630T_Unit()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unit' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<String> getUnit();

	/**
	 * Returns the value of the '<em><b>Unit Id</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.iec61360._2._0.ReferenceT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit Id</em>' containment reference list.
	 * @see io.shell.admin.iec61360._2._0._0Package#getDataSpecificationIEC61630T_UnitId()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unitId' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<ReferenceT> getUnitId();

	/**
	 * Returns the value of the '<em><b>Source Of Definition</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Of Definition</em>' attribute list.
	 * @see io.shell.admin.iec61360._2._0._0Package#getDataSpecificationIEC61630T_SourceOfDefinition()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='sourceOfDefinition' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<String> getSourceOfDefinition();

	/**
	 * Returns the value of the '<em><b>Symbol</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symbol</em>' attribute list.
	 * @see io.shell.admin.iec61360._2._0._0Package#getDataSpecificationIEC61630T_Symbol()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='symbol' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<String> getSymbol();

	/**
	 * Returns the value of the '<em><b>Data Type</b></em>' attribute list.
	 * The list contents are of type {@link io.shell.admin.iec61360._2._0.DataTypeIEC61360T}.
	 * The literals are from the enumeration {@link io.shell.admin.iec61360._2._0.DataTypeIEC61360T}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Type</em>' attribute list.
	 * @see io.shell.admin.iec61360._2._0.DataTypeIEC61360T
	 * @see io.shell.admin.iec61360._2._0._0Package#getDataSpecificationIEC61630T_DataType()
	 * @model unique="false" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='dataType' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<DataTypeIEC61360T> getDataType();

	/**
	 * Returns the value of the '<em><b>Definition</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.iec61360._2._0.LangStringSetT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definition</em>' containment reference list.
	 * @see io.shell.admin.iec61360._2._0._0Package#getDataSpecificationIEC61630T_Definition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='definition' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<LangStringSetT> getDefinition();

	/**
	 * Returns the value of the '<em><b>Value Format</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Format</em>' attribute list.
	 * @see io.shell.admin.iec61360._2._0._0Package#getDataSpecificationIEC61630T_ValueFormat()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='valueFormat' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<String> getValueFormat();

	/**
	 * Returns the value of the '<em><b>Value List</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.iec61360._2._0.ValueListT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value List</em>' containment reference list.
	 * @see io.shell.admin.iec61360._2._0._0Package#getDataSpecificationIEC61630T_ValueList()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='valueList' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<ValueListT> getValueList();

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.iec61360._2._0.ValueDataTypeT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference list.
	 * @see io.shell.admin.iec61360._2._0._0Package#getDataSpecificationIEC61630T_Value()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='value' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<ValueDataTypeT> getValue();

	/**
	 * Returns the value of the '<em><b>Value Id</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.iec61360._2._0.ReferenceT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Id</em>' containment reference list.
	 * @see io.shell.admin.iec61360._2._0._0Package#getDataSpecificationIEC61630T_ValueId()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='valueId' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<ReferenceT> getValueId();

	/**
	 * Returns the value of the '<em><b>Level Type</b></em>' attribute list.
	 * The list contents are of type {@link io.shell.admin.iec61360._2._0.LevelTypeT}.
	 * The literals are from the enumeration {@link io.shell.admin.iec61360._2._0.LevelTypeT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Level Type</em>' attribute list.
	 * @see io.shell.admin.iec61360._2._0.LevelTypeT
	 * @see io.shell.admin.iec61360._2._0._0Package#getDataSpecificationIEC61630T_LevelType()
	 * @model unique="false" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='levelType' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<LevelTypeT> getLevelType();

} // DataSpecificationIEC61630T
