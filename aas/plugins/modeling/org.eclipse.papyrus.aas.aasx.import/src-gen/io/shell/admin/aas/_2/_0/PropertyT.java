/**
 */
package io.shell.admin.aas._2._0;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Property T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.PropertyT#getValueType <em>Value Type</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.PropertyT#getValue <em>Value</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.PropertyT#getValueId <em>Value Id</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getPropertyT()
 * @model extendedMetaData="name='property_t' kind='elementOnly'"
 * @generated
 */
public interface PropertyT extends SubmodelElementAbstractT {
	/**
	 * Returns the value of the '<em><b>Value Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Type</em>' containment reference.
	 * @see #setValueType(DataTypeDefT)
	 * @see io.shell.admin.aas._2._0._0Package#getPropertyT_ValueType()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='valueType' namespace='##targetNamespace'"
	 * @generated
	 */
	DataTypeDefT getValueType();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.PropertyT#getValueType <em>Value Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Type</em>' containment reference.
	 * @see #getValueType()
	 * @generated
	 */
	void setValueType(DataTypeDefT value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(ValueDataTypeT)
	 * @see io.shell.admin.aas._2._0._0Package#getPropertyT_Value()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='value' namespace='##targetNamespace'"
	 * @generated
	 */
	ValueDataTypeT getValue();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.PropertyT#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(ValueDataTypeT value);

	/**
	 * Returns the value of the '<em><b>Value Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Id</em>' containment reference.
	 * @see #setValueId(ReferenceT)
	 * @see io.shell.admin.aas._2._0._0Package#getPropertyT_ValueId()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='valueId' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getValueId();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.PropertyT#getValueId <em>Value Id</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Id</em>' containment reference.
	 * @see #getValueId()
	 * @generated
	 */
	void setValueId(ReferenceT value);

} // PropertyT
