/**
 */
package io.shell.admin.aas.abac._2._0;

import io.shell.admin.aas._2._0.PropertyT;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Object Attributes T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.ObjectAttributesT#getObjectAttribute <em>Object Attribute</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas.abac._2._0._0Package#getObjectAttributesT()
 * @model extendedMetaData="name='objectAttributes_t' kind='elementOnly'"
 * @generated
 */
public interface ObjectAttributesT extends EObject {
	/**
	 * Returns the value of the '<em><b>Object Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas._2._0.PropertyT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Attribute</em>' containment reference list.
	 * @see io.shell.admin.aas.abac._2._0._0Package#getObjectAttributesT_ObjectAttribute()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='objectAttribute' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PropertyT> getObjectAttribute();

} // ObjectAttributesT
