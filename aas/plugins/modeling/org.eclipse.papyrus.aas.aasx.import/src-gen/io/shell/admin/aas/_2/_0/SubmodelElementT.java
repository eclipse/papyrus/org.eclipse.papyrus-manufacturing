/**
 */
package io.shell.admin.aas._2._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Submodel Element T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.SubmodelElementT#getMultiLanguageProperty <em>Multi Language Property</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.SubmodelElementT#getProperty <em>Property</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.SubmodelElementT#getRange <em>Range</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.SubmodelElementT#getBlob <em>Blob</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.SubmodelElementT#getFile <em>File</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.SubmodelElementT#getReferenceElement <em>Reference Element</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.SubmodelElementT#getAnnotatedRelationshipElement <em>Annotated Relationship Element</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.SubmodelElementT#getBasicEvent <em>Basic Event</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.SubmodelElementT#getCapability <em>Capability</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.SubmodelElementT#getEntity <em>Entity</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.SubmodelElementT#getOperation <em>Operation</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.SubmodelElementT#getRelationshipElement <em>Relationship Element</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.SubmodelElementT#getSubmodelElementCollection <em>Submodel Element Collection</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getSubmodelElementT()
 * @model extendedMetaData="name='submodelElement_t' kind='elementOnly'"
 * @generated
 */
public interface SubmodelElementT extends EObject {
	/**
	 * Returns the value of the '<em><b>Multi Language Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multi Language Property</em>' containment reference.
	 * @see #setMultiLanguageProperty(MultiLanguagePropertyT)
	 * @see io.shell.admin.aas._2._0._0Package#getSubmodelElementT_MultiLanguageProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='multiLanguageProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	MultiLanguagePropertyT getMultiLanguageProperty();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.SubmodelElementT#getMultiLanguageProperty <em>Multi Language Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Multi Language Property</em>' containment reference.
	 * @see #getMultiLanguageProperty()
	 * @generated
	 */
	void setMultiLanguageProperty(MultiLanguagePropertyT value);

	/**
	 * Returns the value of the '<em><b>Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property</em>' containment reference.
	 * @see #setProperty(PropertyT)
	 * @see io.shell.admin.aas._2._0._0Package#getSubmodelElementT_Property()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='property' namespace='##targetNamespace'"
	 * @generated
	 */
	PropertyT getProperty();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.SubmodelElementT#getProperty <em>Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property</em>' containment reference.
	 * @see #getProperty()
	 * @generated
	 */
	void setProperty(PropertyT value);

	/**
	 * Returns the value of the '<em><b>Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range</em>' containment reference.
	 * @see #setRange(RangeT)
	 * @see io.shell.admin.aas._2._0._0Package#getSubmodelElementT_Range()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='range' namespace='##targetNamespace'"
	 * @generated
	 */
	RangeT getRange();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.SubmodelElementT#getRange <em>Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Range</em>' containment reference.
	 * @see #getRange()
	 * @generated
	 */
	void setRange(RangeT value);

	/**
	 * Returns the value of the '<em><b>Blob</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Blob</em>' containment reference.
	 * @see #setBlob(BlobT)
	 * @see io.shell.admin.aas._2._0._0Package#getSubmodelElementT_Blob()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='blob' namespace='##targetNamespace'"
	 * @generated
	 */
	BlobT getBlob();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.SubmodelElementT#getBlob <em>Blob</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Blob</em>' containment reference.
	 * @see #getBlob()
	 * @generated
	 */
	void setBlob(BlobT value);

	/**
	 * Returns the value of the '<em><b>File</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>File</em>' containment reference.
	 * @see #setFile(FileT)
	 * @see io.shell.admin.aas._2._0._0Package#getSubmodelElementT_File()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='file' namespace='##targetNamespace'"
	 * @generated
	 */
	FileT getFile();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.SubmodelElementT#getFile <em>File</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>File</em>' containment reference.
	 * @see #getFile()
	 * @generated
	 */
	void setFile(FileT value);

	/**
	 * Returns the value of the '<em><b>Reference Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference Element</em>' containment reference.
	 * @see #setReferenceElement(ReferenceElementT)
	 * @see io.shell.admin.aas._2._0._0Package#getSubmodelElementT_ReferenceElement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='referenceElement' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceElementT getReferenceElement();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.SubmodelElementT#getReferenceElement <em>Reference Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference Element</em>' containment reference.
	 * @see #getReferenceElement()
	 * @generated
	 */
	void setReferenceElement(ReferenceElementT value);

	/**
	 * Returns the value of the '<em><b>Annotated Relationship Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotated Relationship Element</em>' containment reference.
	 * @see #setAnnotatedRelationshipElement(AnnotatedRelationshipElementT)
	 * @see io.shell.admin.aas._2._0._0Package#getSubmodelElementT_AnnotatedRelationshipElement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='annotatedRelationshipElement' namespace='##targetNamespace'"
	 * @generated
	 */
	AnnotatedRelationshipElementT getAnnotatedRelationshipElement();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.SubmodelElementT#getAnnotatedRelationshipElement <em>Annotated Relationship Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Annotated Relationship Element</em>' containment reference.
	 * @see #getAnnotatedRelationshipElement()
	 * @generated
	 */
	void setAnnotatedRelationshipElement(AnnotatedRelationshipElementT value);

	/**
	 * Returns the value of the '<em><b>Basic Event</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Basic Event</em>' containment reference.
	 * @see #setBasicEvent(BasicEventT)
	 * @see io.shell.admin.aas._2._0._0Package#getSubmodelElementT_BasicEvent()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='basicEvent' namespace='##targetNamespace'"
	 * @generated
	 */
	BasicEventT getBasicEvent();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.SubmodelElementT#getBasicEvent <em>Basic Event</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Basic Event</em>' containment reference.
	 * @see #getBasicEvent()
	 * @generated
	 */
	void setBasicEvent(BasicEventT value);

	/**
	 * Returns the value of the '<em><b>Capability</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Capability</em>' containment reference.
	 * @see #setCapability(SubmodelElementAbstractT)
	 * @see io.shell.admin.aas._2._0._0Package#getSubmodelElementT_Capability()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='capability' namespace='##targetNamespace'"
	 * @generated
	 */
	SubmodelElementAbstractT getCapability();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.SubmodelElementT#getCapability <em>Capability</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Capability</em>' containment reference.
	 * @see #getCapability()
	 * @generated
	 */
	void setCapability(SubmodelElementAbstractT value);

	/**
	 * Returns the value of the '<em><b>Entity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entity</em>' containment reference.
	 * @see #setEntity(EntityT)
	 * @see io.shell.admin.aas._2._0._0Package#getSubmodelElementT_Entity()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='entity' namespace='##targetNamespace'"
	 * @generated
	 */
	EntityT getEntity();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.SubmodelElementT#getEntity <em>Entity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entity</em>' containment reference.
	 * @see #getEntity()
	 * @generated
	 */
	void setEntity(EntityT value);

	/**
	 * Returns the value of the '<em><b>Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' containment reference.
	 * @see #setOperation(OperationT)
	 * @see io.shell.admin.aas._2._0._0Package#getSubmodelElementT_Operation()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='operation' namespace='##targetNamespace'"
	 * @generated
	 */
	OperationT getOperation();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.SubmodelElementT#getOperation <em>Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operation</em>' containment reference.
	 * @see #getOperation()
	 * @generated
	 */
	void setOperation(OperationT value);

	/**
	 * Returns the value of the '<em><b>Relationship Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relationship Element</em>' containment reference.
	 * @see #setRelationshipElement(RelationshipElementT)
	 * @see io.shell.admin.aas._2._0._0Package#getSubmodelElementT_RelationshipElement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='relationshipElement' namespace='##targetNamespace'"
	 * @generated
	 */
	RelationshipElementT getRelationshipElement();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.SubmodelElementT#getRelationshipElement <em>Relationship Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relationship Element</em>' containment reference.
	 * @see #getRelationshipElement()
	 * @generated
	 */
	void setRelationshipElement(RelationshipElementT value);

	/**
	 * Returns the value of the '<em><b>Submodel Element Collection</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Submodel Element Collection</em>' containment reference.
	 * @see #setSubmodelElementCollection(SubmodelElementCollectionT)
	 * @see io.shell.admin.aas._2._0._0Package#getSubmodelElementT_SubmodelElementCollection()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='submodelElementCollection' namespace='##targetNamespace'"
	 * @generated
	 */
	SubmodelElementCollectionT getSubmodelElementCollection();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.SubmodelElementT#getSubmodelElementCollection <em>Submodel Element Collection</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Submodel Element Collection</em>' containment reference.
	 * @see #getSubmodelElementCollection()
	 * @generated
	 */
	void setSubmodelElementCollection(SubmodelElementCollectionT value);

} // SubmodelElementT
