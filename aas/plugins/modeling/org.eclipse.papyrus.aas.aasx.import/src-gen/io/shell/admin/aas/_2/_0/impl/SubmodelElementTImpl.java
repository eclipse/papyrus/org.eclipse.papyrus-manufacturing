/**
 */
package io.shell.admin.aas._2._0.impl;

import io.shell.admin.aas._2._0.AnnotatedRelationshipElementT;
import io.shell.admin.aas._2._0.BasicEventT;
import io.shell.admin.aas._2._0.BlobT;
import io.shell.admin.aas._2._0.EntityT;
import io.shell.admin.aas._2._0.FileT;
import io.shell.admin.aas._2._0.MultiLanguagePropertyT;
import io.shell.admin.aas._2._0.OperationT;
import io.shell.admin.aas._2._0.PropertyT;
import io.shell.admin.aas._2._0.RangeT;
import io.shell.admin.aas._2._0.ReferenceElementT;
import io.shell.admin.aas._2._0.RelationshipElementT;
import io.shell.admin.aas._2._0.SubmodelElementAbstractT;
import io.shell.admin.aas._2._0.SubmodelElementCollectionT;
import io.shell.admin.aas._2._0.SubmodelElementT;
import io.shell.admin.aas._2._0._0Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Submodel Element T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.impl.SubmodelElementTImpl#getMultiLanguageProperty <em>Multi Language Property</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.SubmodelElementTImpl#getProperty <em>Property</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.SubmodelElementTImpl#getRange <em>Range</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.SubmodelElementTImpl#getBlob <em>Blob</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.SubmodelElementTImpl#getFile <em>File</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.SubmodelElementTImpl#getReferenceElement <em>Reference Element</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.SubmodelElementTImpl#getAnnotatedRelationshipElement <em>Annotated Relationship Element</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.SubmodelElementTImpl#getBasicEvent <em>Basic Event</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.SubmodelElementTImpl#getCapability <em>Capability</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.SubmodelElementTImpl#getEntity <em>Entity</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.SubmodelElementTImpl#getOperation <em>Operation</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.SubmodelElementTImpl#getRelationshipElement <em>Relationship Element</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.SubmodelElementTImpl#getSubmodelElementCollection <em>Submodel Element Collection</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubmodelElementTImpl extends MinimalEObjectImpl.Container implements SubmodelElementT {
	/**
	 * The cached value of the '{@link #getMultiLanguageProperty() <em>Multi Language Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMultiLanguageProperty()
	 * @generated
	 * @ordered
	 */
	protected MultiLanguagePropertyT multiLanguageProperty;

	/**
	 * The cached value of the '{@link #getProperty() <em>Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperty()
	 * @generated
	 * @ordered
	 */
	protected PropertyT property;

	/**
	 * The cached value of the '{@link #getRange() <em>Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRange()
	 * @generated
	 * @ordered
	 */
	protected RangeT range;

	/**
	 * The cached value of the '{@link #getBlob() <em>Blob</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBlob()
	 * @generated
	 * @ordered
	 */
	protected BlobT blob;

	/**
	 * The cached value of the '{@link #getFile() <em>File</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFile()
	 * @generated
	 * @ordered
	 */
	protected FileT file;

	/**
	 * The cached value of the '{@link #getReferenceElement() <em>Reference Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenceElement()
	 * @generated
	 * @ordered
	 */
	protected ReferenceElementT referenceElement;

	/**
	 * The cached value of the '{@link #getAnnotatedRelationshipElement() <em>Annotated Relationship Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotatedRelationshipElement()
	 * @generated
	 * @ordered
	 */
	protected AnnotatedRelationshipElementT annotatedRelationshipElement;

	/**
	 * The cached value of the '{@link #getBasicEvent() <em>Basic Event</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBasicEvent()
	 * @generated
	 * @ordered
	 */
	protected BasicEventT basicEvent;

	/**
	 * The cached value of the '{@link #getCapability() <em>Capability</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCapability()
	 * @generated
	 * @ordered
	 */
	protected SubmodelElementAbstractT capability;

	/**
	 * The cached value of the '{@link #getEntity() <em>Entity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntity()
	 * @generated
	 * @ordered
	 */
	protected EntityT entity;

	/**
	 * The cached value of the '{@link #getOperation() <em>Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperation()
	 * @generated
	 * @ordered
	 */
	protected OperationT operation;

	/**
	 * The cached value of the '{@link #getRelationshipElement() <em>Relationship Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelationshipElement()
	 * @generated
	 * @ordered
	 */
	protected RelationshipElementT relationshipElement;

	/**
	 * The cached value of the '{@link #getSubmodelElementCollection() <em>Submodel Element Collection</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubmodelElementCollection()
	 * @generated
	 * @ordered
	 */
	protected SubmodelElementCollectionT submodelElementCollection;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubmodelElementTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.SUBMODEL_ELEMENT_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiLanguagePropertyT getMultiLanguageProperty() {
		return multiLanguageProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMultiLanguageProperty(MultiLanguagePropertyT newMultiLanguageProperty, NotificationChain msgs) {
		MultiLanguagePropertyT oldMultiLanguageProperty = multiLanguageProperty;
		multiLanguageProperty = newMultiLanguageProperty;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__MULTI_LANGUAGE_PROPERTY, oldMultiLanguageProperty, newMultiLanguageProperty);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMultiLanguageProperty(MultiLanguagePropertyT newMultiLanguageProperty) {
		if (newMultiLanguageProperty != multiLanguageProperty) {
			NotificationChain msgs = null;
			if (multiLanguageProperty != null)
				msgs = ((InternalEObject)multiLanguageProperty).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__MULTI_LANGUAGE_PROPERTY, null, msgs);
			if (newMultiLanguageProperty != null)
				msgs = ((InternalEObject)newMultiLanguageProperty).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__MULTI_LANGUAGE_PROPERTY, null, msgs);
			msgs = basicSetMultiLanguageProperty(newMultiLanguageProperty, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__MULTI_LANGUAGE_PROPERTY, newMultiLanguageProperty, newMultiLanguageProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyT getProperty() {
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProperty(PropertyT newProperty, NotificationChain msgs) {
		PropertyT oldProperty = property;
		property = newProperty;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__PROPERTY, oldProperty, newProperty);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProperty(PropertyT newProperty) {
		if (newProperty != property) {
			NotificationChain msgs = null;
			if (property != null)
				msgs = ((InternalEObject)property).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__PROPERTY, null, msgs);
			if (newProperty != null)
				msgs = ((InternalEObject)newProperty).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__PROPERTY, null, msgs);
			msgs = basicSetProperty(newProperty, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__PROPERTY, newProperty, newProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RangeT getRange() {
		return range;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRange(RangeT newRange, NotificationChain msgs) {
		RangeT oldRange = range;
		range = newRange;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__RANGE, oldRange, newRange);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRange(RangeT newRange) {
		if (newRange != range) {
			NotificationChain msgs = null;
			if (range != null)
				msgs = ((InternalEObject)range).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__RANGE, null, msgs);
			if (newRange != null)
				msgs = ((InternalEObject)newRange).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__RANGE, null, msgs);
			msgs = basicSetRange(newRange, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__RANGE, newRange, newRange));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlobT getBlob() {
		return blob;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBlob(BlobT newBlob, NotificationChain msgs) {
		BlobT oldBlob = blob;
		blob = newBlob;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__BLOB, oldBlob, newBlob);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBlob(BlobT newBlob) {
		if (newBlob != blob) {
			NotificationChain msgs = null;
			if (blob != null)
				msgs = ((InternalEObject)blob).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__BLOB, null, msgs);
			if (newBlob != null)
				msgs = ((InternalEObject)newBlob).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__BLOB, null, msgs);
			msgs = basicSetBlob(newBlob, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__BLOB, newBlob, newBlob));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FileT getFile() {
		return file;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFile(FileT newFile, NotificationChain msgs) {
		FileT oldFile = file;
		file = newFile;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__FILE, oldFile, newFile);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFile(FileT newFile) {
		if (newFile != file) {
			NotificationChain msgs = null;
			if (file != null)
				msgs = ((InternalEObject)file).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__FILE, null, msgs);
			if (newFile != null)
				msgs = ((InternalEObject)newFile).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__FILE, null, msgs);
			msgs = basicSetFile(newFile, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__FILE, newFile, newFile));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceElementT getReferenceElement() {
		return referenceElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReferenceElement(ReferenceElementT newReferenceElement, NotificationChain msgs) {
		ReferenceElementT oldReferenceElement = referenceElement;
		referenceElement = newReferenceElement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__REFERENCE_ELEMENT, oldReferenceElement, newReferenceElement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferenceElement(ReferenceElementT newReferenceElement) {
		if (newReferenceElement != referenceElement) {
			NotificationChain msgs = null;
			if (referenceElement != null)
				msgs = ((InternalEObject)referenceElement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__REFERENCE_ELEMENT, null, msgs);
			if (newReferenceElement != null)
				msgs = ((InternalEObject)newReferenceElement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__REFERENCE_ELEMENT, null, msgs);
			msgs = basicSetReferenceElement(newReferenceElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__REFERENCE_ELEMENT, newReferenceElement, newReferenceElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotatedRelationshipElementT getAnnotatedRelationshipElement() {
		return annotatedRelationshipElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnnotatedRelationshipElement(AnnotatedRelationshipElementT newAnnotatedRelationshipElement, NotificationChain msgs) {
		AnnotatedRelationshipElementT oldAnnotatedRelationshipElement = annotatedRelationshipElement;
		annotatedRelationshipElement = newAnnotatedRelationshipElement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__ANNOTATED_RELATIONSHIP_ELEMENT, oldAnnotatedRelationshipElement, newAnnotatedRelationshipElement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnnotatedRelationshipElement(AnnotatedRelationshipElementT newAnnotatedRelationshipElement) {
		if (newAnnotatedRelationshipElement != annotatedRelationshipElement) {
			NotificationChain msgs = null;
			if (annotatedRelationshipElement != null)
				msgs = ((InternalEObject)annotatedRelationshipElement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__ANNOTATED_RELATIONSHIP_ELEMENT, null, msgs);
			if (newAnnotatedRelationshipElement != null)
				msgs = ((InternalEObject)newAnnotatedRelationshipElement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__ANNOTATED_RELATIONSHIP_ELEMENT, null, msgs);
			msgs = basicSetAnnotatedRelationshipElement(newAnnotatedRelationshipElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__ANNOTATED_RELATIONSHIP_ELEMENT, newAnnotatedRelationshipElement, newAnnotatedRelationshipElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicEventT getBasicEvent() {
		return basicEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBasicEvent(BasicEventT newBasicEvent, NotificationChain msgs) {
		BasicEventT oldBasicEvent = basicEvent;
		basicEvent = newBasicEvent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__BASIC_EVENT, oldBasicEvent, newBasicEvent);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBasicEvent(BasicEventT newBasicEvent) {
		if (newBasicEvent != basicEvent) {
			NotificationChain msgs = null;
			if (basicEvent != null)
				msgs = ((InternalEObject)basicEvent).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__BASIC_EVENT, null, msgs);
			if (newBasicEvent != null)
				msgs = ((InternalEObject)newBasicEvent).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__BASIC_EVENT, null, msgs);
			msgs = basicSetBasicEvent(newBasicEvent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__BASIC_EVENT, newBasicEvent, newBasicEvent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubmodelElementAbstractT getCapability() {
		return capability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCapability(SubmodelElementAbstractT newCapability, NotificationChain msgs) {
		SubmodelElementAbstractT oldCapability = capability;
		capability = newCapability;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__CAPABILITY, oldCapability, newCapability);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCapability(SubmodelElementAbstractT newCapability) {
		if (newCapability != capability) {
			NotificationChain msgs = null;
			if (capability != null)
				msgs = ((InternalEObject)capability).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__CAPABILITY, null, msgs);
			if (newCapability != null)
				msgs = ((InternalEObject)newCapability).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__CAPABILITY, null, msgs);
			msgs = basicSetCapability(newCapability, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__CAPABILITY, newCapability, newCapability));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntityT getEntity() {
		return entity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEntity(EntityT newEntity, NotificationChain msgs) {
		EntityT oldEntity = entity;
		entity = newEntity;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__ENTITY, oldEntity, newEntity);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntity(EntityT newEntity) {
		if (newEntity != entity) {
			NotificationChain msgs = null;
			if (entity != null)
				msgs = ((InternalEObject)entity).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__ENTITY, null, msgs);
			if (newEntity != null)
				msgs = ((InternalEObject)newEntity).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__ENTITY, null, msgs);
			msgs = basicSetEntity(newEntity, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__ENTITY, newEntity, newEntity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationT getOperation() {
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperation(OperationT newOperation, NotificationChain msgs) {
		OperationT oldOperation = operation;
		operation = newOperation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__OPERATION, oldOperation, newOperation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperation(OperationT newOperation) {
		if (newOperation != operation) {
			NotificationChain msgs = null;
			if (operation != null)
				msgs = ((InternalEObject)operation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__OPERATION, null, msgs);
			if (newOperation != null)
				msgs = ((InternalEObject)newOperation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__OPERATION, null, msgs);
			msgs = basicSetOperation(newOperation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__OPERATION, newOperation, newOperation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelationshipElementT getRelationshipElement() {
		return relationshipElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRelationshipElement(RelationshipElementT newRelationshipElement, NotificationChain msgs) {
		RelationshipElementT oldRelationshipElement = relationshipElement;
		relationshipElement = newRelationshipElement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__RELATIONSHIP_ELEMENT, oldRelationshipElement, newRelationshipElement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRelationshipElement(RelationshipElementT newRelationshipElement) {
		if (newRelationshipElement != relationshipElement) {
			NotificationChain msgs = null;
			if (relationshipElement != null)
				msgs = ((InternalEObject)relationshipElement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__RELATIONSHIP_ELEMENT, null, msgs);
			if (newRelationshipElement != null)
				msgs = ((InternalEObject)newRelationshipElement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__RELATIONSHIP_ELEMENT, null, msgs);
			msgs = basicSetRelationshipElement(newRelationshipElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__RELATIONSHIP_ELEMENT, newRelationshipElement, newRelationshipElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubmodelElementCollectionT getSubmodelElementCollection() {
		return submodelElementCollection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSubmodelElementCollection(SubmodelElementCollectionT newSubmodelElementCollection, NotificationChain msgs) {
		SubmodelElementCollectionT oldSubmodelElementCollection = submodelElementCollection;
		submodelElementCollection = newSubmodelElementCollection;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__SUBMODEL_ELEMENT_COLLECTION, oldSubmodelElementCollection, newSubmodelElementCollection);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubmodelElementCollection(SubmodelElementCollectionT newSubmodelElementCollection) {
		if (newSubmodelElementCollection != submodelElementCollection) {
			NotificationChain msgs = null;
			if (submodelElementCollection != null)
				msgs = ((InternalEObject)submodelElementCollection).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__SUBMODEL_ELEMENT_COLLECTION, null, msgs);
			if (newSubmodelElementCollection != null)
				msgs = ((InternalEObject)newSubmodelElementCollection).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.SUBMODEL_ELEMENT_T__SUBMODEL_ELEMENT_COLLECTION, null, msgs);
			msgs = basicSetSubmodelElementCollection(newSubmodelElementCollection, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.SUBMODEL_ELEMENT_T__SUBMODEL_ELEMENT_COLLECTION, newSubmodelElementCollection, newSubmodelElementCollection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.SUBMODEL_ELEMENT_T__MULTI_LANGUAGE_PROPERTY:
				return basicSetMultiLanguageProperty(null, msgs);
			case _0Package.SUBMODEL_ELEMENT_T__PROPERTY:
				return basicSetProperty(null, msgs);
			case _0Package.SUBMODEL_ELEMENT_T__RANGE:
				return basicSetRange(null, msgs);
			case _0Package.SUBMODEL_ELEMENT_T__BLOB:
				return basicSetBlob(null, msgs);
			case _0Package.SUBMODEL_ELEMENT_T__FILE:
				return basicSetFile(null, msgs);
			case _0Package.SUBMODEL_ELEMENT_T__REFERENCE_ELEMENT:
				return basicSetReferenceElement(null, msgs);
			case _0Package.SUBMODEL_ELEMENT_T__ANNOTATED_RELATIONSHIP_ELEMENT:
				return basicSetAnnotatedRelationshipElement(null, msgs);
			case _0Package.SUBMODEL_ELEMENT_T__BASIC_EVENT:
				return basicSetBasicEvent(null, msgs);
			case _0Package.SUBMODEL_ELEMENT_T__CAPABILITY:
				return basicSetCapability(null, msgs);
			case _0Package.SUBMODEL_ELEMENT_T__ENTITY:
				return basicSetEntity(null, msgs);
			case _0Package.SUBMODEL_ELEMENT_T__OPERATION:
				return basicSetOperation(null, msgs);
			case _0Package.SUBMODEL_ELEMENT_T__RELATIONSHIP_ELEMENT:
				return basicSetRelationshipElement(null, msgs);
			case _0Package.SUBMODEL_ELEMENT_T__SUBMODEL_ELEMENT_COLLECTION:
				return basicSetSubmodelElementCollection(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.SUBMODEL_ELEMENT_T__MULTI_LANGUAGE_PROPERTY:
				return getMultiLanguageProperty();
			case _0Package.SUBMODEL_ELEMENT_T__PROPERTY:
				return getProperty();
			case _0Package.SUBMODEL_ELEMENT_T__RANGE:
				return getRange();
			case _0Package.SUBMODEL_ELEMENT_T__BLOB:
				return getBlob();
			case _0Package.SUBMODEL_ELEMENT_T__FILE:
				return getFile();
			case _0Package.SUBMODEL_ELEMENT_T__REFERENCE_ELEMENT:
				return getReferenceElement();
			case _0Package.SUBMODEL_ELEMENT_T__ANNOTATED_RELATIONSHIP_ELEMENT:
				return getAnnotatedRelationshipElement();
			case _0Package.SUBMODEL_ELEMENT_T__BASIC_EVENT:
				return getBasicEvent();
			case _0Package.SUBMODEL_ELEMENT_T__CAPABILITY:
				return getCapability();
			case _0Package.SUBMODEL_ELEMENT_T__ENTITY:
				return getEntity();
			case _0Package.SUBMODEL_ELEMENT_T__OPERATION:
				return getOperation();
			case _0Package.SUBMODEL_ELEMENT_T__RELATIONSHIP_ELEMENT:
				return getRelationshipElement();
			case _0Package.SUBMODEL_ELEMENT_T__SUBMODEL_ELEMENT_COLLECTION:
				return getSubmodelElementCollection();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.SUBMODEL_ELEMENT_T__MULTI_LANGUAGE_PROPERTY:
				setMultiLanguageProperty((MultiLanguagePropertyT)newValue);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__PROPERTY:
				setProperty((PropertyT)newValue);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__RANGE:
				setRange((RangeT)newValue);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__BLOB:
				setBlob((BlobT)newValue);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__FILE:
				setFile((FileT)newValue);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__REFERENCE_ELEMENT:
				setReferenceElement((ReferenceElementT)newValue);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__ANNOTATED_RELATIONSHIP_ELEMENT:
				setAnnotatedRelationshipElement((AnnotatedRelationshipElementT)newValue);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__BASIC_EVENT:
				setBasicEvent((BasicEventT)newValue);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__CAPABILITY:
				setCapability((SubmodelElementAbstractT)newValue);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__ENTITY:
				setEntity((EntityT)newValue);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__OPERATION:
				setOperation((OperationT)newValue);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__RELATIONSHIP_ELEMENT:
				setRelationshipElement((RelationshipElementT)newValue);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__SUBMODEL_ELEMENT_COLLECTION:
				setSubmodelElementCollection((SubmodelElementCollectionT)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.SUBMODEL_ELEMENT_T__MULTI_LANGUAGE_PROPERTY:
				setMultiLanguageProperty((MultiLanguagePropertyT)null);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__PROPERTY:
				setProperty((PropertyT)null);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__RANGE:
				setRange((RangeT)null);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__BLOB:
				setBlob((BlobT)null);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__FILE:
				setFile((FileT)null);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__REFERENCE_ELEMENT:
				setReferenceElement((ReferenceElementT)null);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__ANNOTATED_RELATIONSHIP_ELEMENT:
				setAnnotatedRelationshipElement((AnnotatedRelationshipElementT)null);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__BASIC_EVENT:
				setBasicEvent((BasicEventT)null);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__CAPABILITY:
				setCapability((SubmodelElementAbstractT)null);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__ENTITY:
				setEntity((EntityT)null);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__OPERATION:
				setOperation((OperationT)null);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__RELATIONSHIP_ELEMENT:
				setRelationshipElement((RelationshipElementT)null);
				return;
			case _0Package.SUBMODEL_ELEMENT_T__SUBMODEL_ELEMENT_COLLECTION:
				setSubmodelElementCollection((SubmodelElementCollectionT)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.SUBMODEL_ELEMENT_T__MULTI_LANGUAGE_PROPERTY:
				return multiLanguageProperty != null;
			case _0Package.SUBMODEL_ELEMENT_T__PROPERTY:
				return property != null;
			case _0Package.SUBMODEL_ELEMENT_T__RANGE:
				return range != null;
			case _0Package.SUBMODEL_ELEMENT_T__BLOB:
				return blob != null;
			case _0Package.SUBMODEL_ELEMENT_T__FILE:
				return file != null;
			case _0Package.SUBMODEL_ELEMENT_T__REFERENCE_ELEMENT:
				return referenceElement != null;
			case _0Package.SUBMODEL_ELEMENT_T__ANNOTATED_RELATIONSHIP_ELEMENT:
				return annotatedRelationshipElement != null;
			case _0Package.SUBMODEL_ELEMENT_T__BASIC_EVENT:
				return basicEvent != null;
			case _0Package.SUBMODEL_ELEMENT_T__CAPABILITY:
				return capability != null;
			case _0Package.SUBMODEL_ELEMENT_T__ENTITY:
				return entity != null;
			case _0Package.SUBMODEL_ELEMENT_T__OPERATION:
				return operation != null;
			case _0Package.SUBMODEL_ELEMENT_T__RELATIONSHIP_ELEMENT:
				return relationshipElement != null;
			case _0Package.SUBMODEL_ELEMENT_T__SUBMODEL_ELEMENT_COLLECTION:
				return submodelElementCollection != null;
		}
		return super.eIsSet(featureID);
	}

} //SubmodelElementTImpl
