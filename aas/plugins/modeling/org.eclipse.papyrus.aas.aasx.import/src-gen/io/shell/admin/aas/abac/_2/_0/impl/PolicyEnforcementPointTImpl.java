/**
 */
package io.shell.admin.aas.abac._2._0.impl;

import io.shell.admin.aas.abac._2._0.PolicyEnforcementPointT;
import io.shell.admin.aas.abac._2._0._0Package;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Policy Enforcement Point T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.PolicyEnforcementPointTImpl#isExternalPolicyEnforcementPoint <em>External Policy Enforcement Point</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PolicyEnforcementPointTImpl extends MinimalEObjectImpl.Container implements PolicyEnforcementPointT {
	/**
	 * The default value of the '{@link #isExternalPolicyEnforcementPoint() <em>External Policy Enforcement Point</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExternalPolicyEnforcementPoint()
	 * @generated
	 * @ordered
	 */
	protected static final boolean EXTERNAL_POLICY_ENFORCEMENT_POINT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isExternalPolicyEnforcementPoint() <em>External Policy Enforcement Point</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExternalPolicyEnforcementPoint()
	 * @generated
	 * @ordered
	 */
	protected boolean externalPolicyEnforcementPoint = EXTERNAL_POLICY_ENFORCEMENT_POINT_EDEFAULT;

	/**
	 * This is true if the External Policy Enforcement Point attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean externalPolicyEnforcementPointESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PolicyEnforcementPointTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.POLICY_ENFORCEMENT_POINT_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isExternalPolicyEnforcementPoint() {
		return externalPolicyEnforcementPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExternalPolicyEnforcementPoint(boolean newExternalPolicyEnforcementPoint) {
		boolean oldExternalPolicyEnforcementPoint = externalPolicyEnforcementPoint;
		externalPolicyEnforcementPoint = newExternalPolicyEnforcementPoint;
		boolean oldExternalPolicyEnforcementPointESet = externalPolicyEnforcementPointESet;
		externalPolicyEnforcementPointESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.POLICY_ENFORCEMENT_POINT_T__EXTERNAL_POLICY_ENFORCEMENT_POINT, oldExternalPolicyEnforcementPoint, externalPolicyEnforcementPoint, !oldExternalPolicyEnforcementPointESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetExternalPolicyEnforcementPoint() {
		boolean oldExternalPolicyEnforcementPoint = externalPolicyEnforcementPoint;
		boolean oldExternalPolicyEnforcementPointESet = externalPolicyEnforcementPointESet;
		externalPolicyEnforcementPoint = EXTERNAL_POLICY_ENFORCEMENT_POINT_EDEFAULT;
		externalPolicyEnforcementPointESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, _0Package.POLICY_ENFORCEMENT_POINT_T__EXTERNAL_POLICY_ENFORCEMENT_POINT, oldExternalPolicyEnforcementPoint, EXTERNAL_POLICY_ENFORCEMENT_POINT_EDEFAULT, oldExternalPolicyEnforcementPointESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetExternalPolicyEnforcementPoint() {
		return externalPolicyEnforcementPointESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.POLICY_ENFORCEMENT_POINT_T__EXTERNAL_POLICY_ENFORCEMENT_POINT:
				return isExternalPolicyEnforcementPoint();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.POLICY_ENFORCEMENT_POINT_T__EXTERNAL_POLICY_ENFORCEMENT_POINT:
				setExternalPolicyEnforcementPoint((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.POLICY_ENFORCEMENT_POINT_T__EXTERNAL_POLICY_ENFORCEMENT_POINT:
				unsetExternalPolicyEnforcementPoint();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.POLICY_ENFORCEMENT_POINT_T__EXTERNAL_POLICY_ENFORCEMENT_POINT:
				return isSetExternalPolicyEnforcementPoint();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (externalPolicyEnforcementPoint: ");
		if (externalPolicyEnforcementPointESet) result.append(externalPolicyEnforcementPoint); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //PolicyEnforcementPointTImpl
