/**
 */
package io.shell.admin.aas.abac._2._0.util;

import io.shell.admin.aas.abac._2._0.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see io.shell.admin.aas.abac._2._0._0Package
 * @generated
 */
public class _0AdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static _0Package modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public _0AdapterFactory() {
		if (modelPackage == null) {
			modelPackage = _0Package.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected _0Switch<Adapter> modelSwitch =
		new _0Switch<Adapter>() {
			@Override
			public Adapter caseAccessControlPolicyPointsT(AccessControlPolicyPointsT object) {
				return createAccessControlPolicyPointsTAdapter();
			}
			@Override
			public Adapter caseAccessControlT(AccessControlT object) {
				return createAccessControlTAdapter();
			}
			@Override
			public Adapter caseAccessPermissionRulesT(AccessPermissionRulesT object) {
				return createAccessPermissionRulesTAdapter();
			}
			@Override
			public Adapter caseAccessPermissionRuleT(AccessPermissionRuleT object) {
				return createAccessPermissionRuleTAdapter();
			}
			@Override
			public Adapter caseBlobCertificateT(BlobCertificateT object) {
				return createBlobCertificateTAdapter();
			}
			@Override
			public Adapter caseCertificateAbstractT(CertificateAbstractT object) {
				return createCertificateAbstractTAdapter();
			}
			@Override
			public Adapter caseCertificatesT(CertificatesT object) {
				return createCertificatesTAdapter();
			}
			@Override
			public Adapter caseCertificateT(CertificateT object) {
				return createCertificateTAdapter();
			}
			@Override
			public Adapter caseContainedExtensionsT(ContainedExtensionsT object) {
				return createContainedExtensionsTAdapter();
			}
			@Override
			public Adapter caseInternalInformationPoints(InternalInformationPoints object) {
				return createInternalInformationPointsAdapter();
			}
			@Override
			public Adapter caseObjectAttributesT(ObjectAttributesT object) {
				return createObjectAttributesTAdapter();
			}
			@Override
			public Adapter casePermissionPerObjectT(PermissionPerObjectT object) {
				return createPermissionPerObjectTAdapter();
			}
			@Override
			public Adapter casePermissionsT(PermissionsT object) {
				return createPermissionsTAdapter();
			}
			@Override
			public Adapter casePolicyAdministrationPointT(PolicyAdministrationPointT object) {
				return createPolicyAdministrationPointTAdapter();
			}
			@Override
			public Adapter casePolicyDecisionPointT(PolicyDecisionPointT object) {
				return createPolicyDecisionPointTAdapter();
			}
			@Override
			public Adapter casePolicyEnforcementPointT(PolicyEnforcementPointT object) {
				return createPolicyEnforcementPointTAdapter();
			}
			@Override
			public Adapter casePolicyInformationPointsT(PolicyInformationPointsT object) {
				return createPolicyInformationPointsTAdapter();
			}
			@Override
			public Adapter caseSecurityT(SecurityT object) {
				return createSecurityTAdapter();
			}
			@Override
			public Adapter caseSubjectAttributesT(SubjectAttributesT object) {
				return createSubjectAttributesTAdapter();
			}
			@Override
			public Adapter caseDocumentRoot(DocumentRoot object) {
				return createDocumentRootAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas.abac._2._0.AccessControlPolicyPointsT <em>Access Control Policy Points T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas.abac._2._0.AccessControlPolicyPointsT
	 * @generated
	 */
	public Adapter createAccessControlPolicyPointsTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas.abac._2._0.AccessControlT <em>Access Control T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas.abac._2._0.AccessControlT
	 * @generated
	 */
	public Adapter createAccessControlTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas.abac._2._0.AccessPermissionRulesT <em>Access Permission Rules T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas.abac._2._0.AccessPermissionRulesT
	 * @generated
	 */
	public Adapter createAccessPermissionRulesTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas.abac._2._0.AccessPermissionRuleT <em>Access Permission Rule T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas.abac._2._0.AccessPermissionRuleT
	 * @generated
	 */
	public Adapter createAccessPermissionRuleTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas.abac._2._0.BlobCertificateT <em>Blob Certificate T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas.abac._2._0.BlobCertificateT
	 * @generated
	 */
	public Adapter createBlobCertificateTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas.abac._2._0.CertificateAbstractT <em>Certificate Abstract T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas.abac._2._0.CertificateAbstractT
	 * @generated
	 */
	public Adapter createCertificateAbstractTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas.abac._2._0.CertificatesT <em>Certificates T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas.abac._2._0.CertificatesT
	 * @generated
	 */
	public Adapter createCertificatesTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas.abac._2._0.CertificateT <em>Certificate T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas.abac._2._0.CertificateT
	 * @generated
	 */
	public Adapter createCertificateTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas.abac._2._0.ContainedExtensionsT <em>Contained Extensions T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas.abac._2._0.ContainedExtensionsT
	 * @generated
	 */
	public Adapter createContainedExtensionsTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas.abac._2._0.InternalInformationPoints <em>Internal Information Points</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas.abac._2._0.InternalInformationPoints
	 * @generated
	 */
	public Adapter createInternalInformationPointsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas.abac._2._0.ObjectAttributesT <em>Object Attributes T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas.abac._2._0.ObjectAttributesT
	 * @generated
	 */
	public Adapter createObjectAttributesTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas.abac._2._0.PermissionPerObjectT <em>Permission Per Object T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas.abac._2._0.PermissionPerObjectT
	 * @generated
	 */
	public Adapter createPermissionPerObjectTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas.abac._2._0.PermissionsT <em>Permissions T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas.abac._2._0.PermissionsT
	 * @generated
	 */
	public Adapter createPermissionsTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas.abac._2._0.PolicyAdministrationPointT <em>Policy Administration Point T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas.abac._2._0.PolicyAdministrationPointT
	 * @generated
	 */
	public Adapter createPolicyAdministrationPointTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas.abac._2._0.PolicyDecisionPointT <em>Policy Decision Point T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas.abac._2._0.PolicyDecisionPointT
	 * @generated
	 */
	public Adapter createPolicyDecisionPointTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas.abac._2._0.PolicyEnforcementPointT <em>Policy Enforcement Point T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas.abac._2._0.PolicyEnforcementPointT
	 * @generated
	 */
	public Adapter createPolicyEnforcementPointTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas.abac._2._0.PolicyInformationPointsT <em>Policy Information Points T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas.abac._2._0.PolicyInformationPointsT
	 * @generated
	 */
	public Adapter createPolicyInformationPointsTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas.abac._2._0.SecurityT <em>Security T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas.abac._2._0.SecurityT
	 * @generated
	 */
	public Adapter createSecurityTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas.abac._2._0.SubjectAttributesT <em>Subject Attributes T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas.abac._2._0.SubjectAttributesT
	 * @generated
	 */
	public Adapter createSubjectAttributesTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.aas.abac._2._0.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.aas.abac._2._0.DocumentRoot
	 * @generated
	 */
	public Adapter createDocumentRootAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //_0AdapterFactory
