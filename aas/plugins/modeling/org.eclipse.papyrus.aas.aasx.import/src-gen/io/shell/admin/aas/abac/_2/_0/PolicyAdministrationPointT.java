/**
 */
package io.shell.admin.aas.abac._2._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Policy Administration Point T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.PolicyAdministrationPointT#getLocalAccessControl <em>Local Access Control</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.PolicyAdministrationPointT#isExternalAccessControl <em>External Access Control</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas.abac._2._0._0Package#getPolicyAdministrationPointT()
 * @model extendedMetaData="name='policyAdministrationPoint_t' kind='elementOnly'"
 * @generated
 */
public interface PolicyAdministrationPointT extends EObject {
	/**
	 * Returns the value of the '<em><b>Local Access Control</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Access Control</em>' containment reference.
	 * @see #setLocalAccessControl(AccessControlT)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getPolicyAdministrationPointT_LocalAccessControl()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='localAccessControl' namespace='##targetNamespace'"
	 * @generated
	 */
	AccessControlT getLocalAccessControl();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.PolicyAdministrationPointT#getLocalAccessControl <em>Local Access Control</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Local Access Control</em>' containment reference.
	 * @see #getLocalAccessControl()
	 * @generated
	 */
	void setLocalAccessControl(AccessControlT value);

	/**
	 * Returns the value of the '<em><b>External Access Control</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Access Control</em>' attribute.
	 * @see #isSetExternalAccessControl()
	 * @see #unsetExternalAccessControl()
	 * @see #setExternalAccessControl(boolean)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getPolicyAdministrationPointT_ExternalAccessControl()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='element' name='externalAccessControl' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isExternalAccessControl();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.PolicyAdministrationPointT#isExternalAccessControl <em>External Access Control</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>External Access Control</em>' attribute.
	 * @see #isSetExternalAccessControl()
	 * @see #unsetExternalAccessControl()
	 * @see #isExternalAccessControl()
	 * @generated
	 */
	void setExternalAccessControl(boolean value);

	/**
	 * Unsets the value of the '{@link io.shell.admin.aas.abac._2._0.PolicyAdministrationPointT#isExternalAccessControl <em>External Access Control</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExternalAccessControl()
	 * @see #isExternalAccessControl()
	 * @see #setExternalAccessControl(boolean)
	 * @generated
	 */
	void unsetExternalAccessControl();

	/**
	 * Returns whether the value of the '{@link io.shell.admin.aas.abac._2._0.PolicyAdministrationPointT#isExternalAccessControl <em>External Access Control</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>External Access Control</em>' attribute is set.
	 * @see #unsetExternalAccessControl()
	 * @see #isExternalAccessControl()
	 * @see #setExternalAccessControl(boolean)
	 * @generated
	 */
	boolean isSetExternalAccessControl();

} // PolicyAdministrationPointT
