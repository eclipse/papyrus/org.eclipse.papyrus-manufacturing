/**
 */
package io.shell.admin.aas.abac._2._0.impl;

import io.shell.admin.aas.abac._2._0.AccessPermissionRuleT;
import io.shell.admin.aas.abac._2._0.AccessPermissionRulesT;
import io.shell.admin.aas.abac._2._0._0Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Access Permission Rules T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.AccessPermissionRulesTImpl#getAccessPermissionRule <em>Access Permission Rule</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AccessPermissionRulesTImpl extends MinimalEObjectImpl.Container implements AccessPermissionRulesT {
	/**
	 * The cached value of the '{@link #getAccessPermissionRule() <em>Access Permission Rule</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessPermissionRule()
	 * @generated
	 * @ordered
	 */
	protected EList<AccessPermissionRuleT> accessPermissionRule;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AccessPermissionRulesTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.ACCESS_PERMISSION_RULES_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AccessPermissionRuleT> getAccessPermissionRule() {
		if (accessPermissionRule == null) {
			accessPermissionRule = new EObjectContainmentEList<AccessPermissionRuleT>(AccessPermissionRuleT.class, this, _0Package.ACCESS_PERMISSION_RULES_T__ACCESS_PERMISSION_RULE);
		}
		return accessPermissionRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.ACCESS_PERMISSION_RULES_T__ACCESS_PERMISSION_RULE:
				return ((InternalEList<?>)getAccessPermissionRule()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.ACCESS_PERMISSION_RULES_T__ACCESS_PERMISSION_RULE:
				return getAccessPermissionRule();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.ACCESS_PERMISSION_RULES_T__ACCESS_PERMISSION_RULE:
				getAccessPermissionRule().clear();
				getAccessPermissionRule().addAll((Collection<? extends AccessPermissionRuleT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.ACCESS_PERMISSION_RULES_T__ACCESS_PERMISSION_RULE:
				getAccessPermissionRule().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.ACCESS_PERMISSION_RULES_T__ACCESS_PERMISSION_RULE:
				return accessPermissionRule != null && !accessPermissionRule.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AccessPermissionRulesTImpl
