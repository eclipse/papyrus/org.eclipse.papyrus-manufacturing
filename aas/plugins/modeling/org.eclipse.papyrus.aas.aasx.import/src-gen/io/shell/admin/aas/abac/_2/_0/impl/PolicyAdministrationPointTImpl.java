/**
 */
package io.shell.admin.aas.abac._2._0.impl;

import io.shell.admin.aas.abac._2._0.AccessControlT;
import io.shell.admin.aas.abac._2._0.PolicyAdministrationPointT;
import io.shell.admin.aas.abac._2._0._0Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Policy Administration Point T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.PolicyAdministrationPointTImpl#getLocalAccessControl <em>Local Access Control</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.PolicyAdministrationPointTImpl#isExternalAccessControl <em>External Access Control</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PolicyAdministrationPointTImpl extends MinimalEObjectImpl.Container implements PolicyAdministrationPointT {
	/**
	 * The cached value of the '{@link #getLocalAccessControl() <em>Local Access Control</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalAccessControl()
	 * @generated
	 * @ordered
	 */
	protected AccessControlT localAccessControl;

	/**
	 * The default value of the '{@link #isExternalAccessControl() <em>External Access Control</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExternalAccessControl()
	 * @generated
	 * @ordered
	 */
	protected static final boolean EXTERNAL_ACCESS_CONTROL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isExternalAccessControl() <em>External Access Control</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExternalAccessControl()
	 * @generated
	 * @ordered
	 */
	protected boolean externalAccessControl = EXTERNAL_ACCESS_CONTROL_EDEFAULT;

	/**
	 * This is true if the External Access Control attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean externalAccessControlESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PolicyAdministrationPointTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.POLICY_ADMINISTRATION_POINT_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessControlT getLocalAccessControl() {
		return localAccessControl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLocalAccessControl(AccessControlT newLocalAccessControl, NotificationChain msgs) {
		AccessControlT oldLocalAccessControl = localAccessControl;
		localAccessControl = newLocalAccessControl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.POLICY_ADMINISTRATION_POINT_T__LOCAL_ACCESS_CONTROL, oldLocalAccessControl, newLocalAccessControl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocalAccessControl(AccessControlT newLocalAccessControl) {
		if (newLocalAccessControl != localAccessControl) {
			NotificationChain msgs = null;
			if (localAccessControl != null)
				msgs = ((InternalEObject)localAccessControl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.POLICY_ADMINISTRATION_POINT_T__LOCAL_ACCESS_CONTROL, null, msgs);
			if (newLocalAccessControl != null)
				msgs = ((InternalEObject)newLocalAccessControl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.POLICY_ADMINISTRATION_POINT_T__LOCAL_ACCESS_CONTROL, null, msgs);
			msgs = basicSetLocalAccessControl(newLocalAccessControl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.POLICY_ADMINISTRATION_POINT_T__LOCAL_ACCESS_CONTROL, newLocalAccessControl, newLocalAccessControl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isExternalAccessControl() {
		return externalAccessControl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExternalAccessControl(boolean newExternalAccessControl) {
		boolean oldExternalAccessControl = externalAccessControl;
		externalAccessControl = newExternalAccessControl;
		boolean oldExternalAccessControlESet = externalAccessControlESet;
		externalAccessControlESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.POLICY_ADMINISTRATION_POINT_T__EXTERNAL_ACCESS_CONTROL, oldExternalAccessControl, externalAccessControl, !oldExternalAccessControlESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetExternalAccessControl() {
		boolean oldExternalAccessControl = externalAccessControl;
		boolean oldExternalAccessControlESet = externalAccessControlESet;
		externalAccessControl = EXTERNAL_ACCESS_CONTROL_EDEFAULT;
		externalAccessControlESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, _0Package.POLICY_ADMINISTRATION_POINT_T__EXTERNAL_ACCESS_CONTROL, oldExternalAccessControl, EXTERNAL_ACCESS_CONTROL_EDEFAULT, oldExternalAccessControlESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetExternalAccessControl() {
		return externalAccessControlESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.POLICY_ADMINISTRATION_POINT_T__LOCAL_ACCESS_CONTROL:
				return basicSetLocalAccessControl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.POLICY_ADMINISTRATION_POINT_T__LOCAL_ACCESS_CONTROL:
				return getLocalAccessControl();
			case _0Package.POLICY_ADMINISTRATION_POINT_T__EXTERNAL_ACCESS_CONTROL:
				return isExternalAccessControl();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.POLICY_ADMINISTRATION_POINT_T__LOCAL_ACCESS_CONTROL:
				setLocalAccessControl((AccessControlT)newValue);
				return;
			case _0Package.POLICY_ADMINISTRATION_POINT_T__EXTERNAL_ACCESS_CONTROL:
				setExternalAccessControl((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.POLICY_ADMINISTRATION_POINT_T__LOCAL_ACCESS_CONTROL:
				setLocalAccessControl((AccessControlT)null);
				return;
			case _0Package.POLICY_ADMINISTRATION_POINT_T__EXTERNAL_ACCESS_CONTROL:
				unsetExternalAccessControl();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.POLICY_ADMINISTRATION_POINT_T__LOCAL_ACCESS_CONTROL:
				return localAccessControl != null;
			case _0Package.POLICY_ADMINISTRATION_POINT_T__EXTERNAL_ACCESS_CONTROL:
				return isSetExternalAccessControl();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (externalAccessControl: ");
		if (externalAccessControlESet) result.append(externalAccessControl); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //PolicyAdministrationPointTImpl
