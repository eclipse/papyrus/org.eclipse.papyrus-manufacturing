/**
 */
package io.shell.admin.aas._2._0.impl;

import io.shell.admin.aas._2._0.OperationT;
import io.shell.admin.aas._2._0.OperationVariableT;
import io.shell.admin.aas._2._0._0Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operation T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.impl.OperationTImpl#getInputVariable <em>Input Variable</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.OperationTImpl#getOutputVariable <em>Output Variable</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.OperationTImpl#getInoutputVariable <em>Inoutput Variable</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationTImpl extends SubmodelElementAbstractTImpl implements OperationT {
	/**
	 * The cached value of the '{@link #getInputVariable() <em>Input Variable</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputVariable()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationVariableT> inputVariable;

	/**
	 * The cached value of the '{@link #getOutputVariable() <em>Output Variable</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputVariable()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationVariableT> outputVariable;

	/**
	 * The cached value of the '{@link #getInoutputVariable() <em>Inoutput Variable</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInoutputVariable()
	 * @generated
	 * @ordered
	 */
	protected EList<OperationVariableT> inoutputVariable;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.OPERATION_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationVariableT> getInputVariable() {
		if (inputVariable == null) {
			inputVariable = new EObjectContainmentEList<OperationVariableT>(OperationVariableT.class, this, _0Package.OPERATION_T__INPUT_VARIABLE);
		}
		return inputVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationVariableT> getOutputVariable() {
		if (outputVariable == null) {
			outputVariable = new EObjectContainmentEList<OperationVariableT>(OperationVariableT.class, this, _0Package.OPERATION_T__OUTPUT_VARIABLE);
		}
		return outputVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperationVariableT> getInoutputVariable() {
		if (inoutputVariable == null) {
			inoutputVariable = new EObjectContainmentEList<OperationVariableT>(OperationVariableT.class, this, _0Package.OPERATION_T__INOUTPUT_VARIABLE);
		}
		return inoutputVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.OPERATION_T__INPUT_VARIABLE:
				return ((InternalEList<?>)getInputVariable()).basicRemove(otherEnd, msgs);
			case _0Package.OPERATION_T__OUTPUT_VARIABLE:
				return ((InternalEList<?>)getOutputVariable()).basicRemove(otherEnd, msgs);
			case _0Package.OPERATION_T__INOUTPUT_VARIABLE:
				return ((InternalEList<?>)getInoutputVariable()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.OPERATION_T__INPUT_VARIABLE:
				return getInputVariable();
			case _0Package.OPERATION_T__OUTPUT_VARIABLE:
				return getOutputVariable();
			case _0Package.OPERATION_T__INOUTPUT_VARIABLE:
				return getInoutputVariable();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.OPERATION_T__INPUT_VARIABLE:
				getInputVariable().clear();
				getInputVariable().addAll((Collection<? extends OperationVariableT>)newValue);
				return;
			case _0Package.OPERATION_T__OUTPUT_VARIABLE:
				getOutputVariable().clear();
				getOutputVariable().addAll((Collection<? extends OperationVariableT>)newValue);
				return;
			case _0Package.OPERATION_T__INOUTPUT_VARIABLE:
				getInoutputVariable().clear();
				getInoutputVariable().addAll((Collection<? extends OperationVariableT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.OPERATION_T__INPUT_VARIABLE:
				getInputVariable().clear();
				return;
			case _0Package.OPERATION_T__OUTPUT_VARIABLE:
				getOutputVariable().clear();
				return;
			case _0Package.OPERATION_T__INOUTPUT_VARIABLE:
				getInoutputVariable().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.OPERATION_T__INPUT_VARIABLE:
				return inputVariable != null && !inputVariable.isEmpty();
			case _0Package.OPERATION_T__OUTPUT_VARIABLE:
				return outputVariable != null && !outputVariable.isEmpty();
			case _0Package.OPERATION_T__INOUTPUT_VARIABLE:
				return inoutputVariable != null && !inoutputVariable.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //OperationTImpl
