/**
 */
package io.shell.admin.iec61360._2._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.iec61360._2._0.ReferenceT#getKeys <em>Keys</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.iec61360._2._0._0Package#getReferenceT()
 * @model extendedMetaData="name='reference_t' kind='elementOnly'"
 * @generated
 */
public interface ReferenceT extends EObject {
	/**
	 * Returns the value of the '<em><b>Keys</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Keys</em>' containment reference.
	 * @see #setKeys(KeysT)
	 * @see io.shell.admin.iec61360._2._0._0Package#getReferenceT_Keys()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='keys' namespace='##targetNamespace'"
	 * @generated
	 */
	KeysT getKeys();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._2._0.ReferenceT#getKeys <em>Keys</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Keys</em>' containment reference.
	 * @see #getKeys()
	 * @generated
	 */
	void setKeys(KeysT value);

} // ReferenceT
