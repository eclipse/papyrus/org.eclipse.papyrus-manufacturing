/**
 */
package io.shell.admin.iec61360._2._0.impl;

import io.shell.admin.iec61360._2._0.ValueListT;
import io.shell.admin.iec61360._2._0.ValueReferencePairT;
import io.shell.admin.iec61360._2._0._0Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Value List T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.iec61360._2._0.impl.ValueListTImpl#getValueReferencePair <em>Value Reference Pair</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ValueListTImpl extends MinimalEObjectImpl.Container implements ValueListT {
	/**
	 * The cached value of the '{@link #getValueReferencePair() <em>Value Reference Pair</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueReferencePair()
	 * @generated
	 * @ordered
	 */
	protected EList<ValueReferencePairT> valueReferencePair;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ValueListTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.VALUE_LIST_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ValueReferencePairT> getValueReferencePair() {
		if (valueReferencePair == null) {
			valueReferencePair = new EObjectContainmentEList<ValueReferencePairT>(ValueReferencePairT.class, this, _0Package.VALUE_LIST_T__VALUE_REFERENCE_PAIR);
		}
		return valueReferencePair;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.VALUE_LIST_T__VALUE_REFERENCE_PAIR:
				return ((InternalEList<?>)getValueReferencePair()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.VALUE_LIST_T__VALUE_REFERENCE_PAIR:
				return getValueReferencePair();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.VALUE_LIST_T__VALUE_REFERENCE_PAIR:
				getValueReferencePair().clear();
				getValueReferencePair().addAll((Collection<? extends ValueReferencePairT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.VALUE_LIST_T__VALUE_REFERENCE_PAIR:
				getValueReferencePair().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.VALUE_LIST_T__VALUE_REFERENCE_PAIR:
				return valueReferencePair != null && !valueReferencePair.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ValueListTImpl
