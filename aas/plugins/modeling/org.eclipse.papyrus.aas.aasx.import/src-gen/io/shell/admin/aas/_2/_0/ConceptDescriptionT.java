/**
 */
package io.shell.admin.aas._2._0;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Concept Description T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.ConceptDescriptionT#getIdShort <em>Id Short</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.ConceptDescriptionT#getCategory <em>Category</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.ConceptDescriptionT#getDescription <em>Description</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.ConceptDescriptionT#getParent <em>Parent</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.ConceptDescriptionT#getIdentification <em>Identification</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.ConceptDescriptionT#getAdministration <em>Administration</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.ConceptDescriptionT#getEmbeddedDataSpecification <em>Embedded Data Specification</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.ConceptDescriptionT#getIsCaseOf <em>Is Case Of</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getConceptDescriptionT()
 * @model extendedMetaData="name='conceptDescription_t' kind='elementOnly'"
 * @generated
 */
public interface ConceptDescriptionT extends EObject {
	/**
	 * Returns the value of the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id Short</em>' containment reference.
	 * @see #setIdShort(IdShortT)
	 * @see io.shell.admin.aas._2._0._0Package#getConceptDescriptionT_IdShort()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='idShort' namespace='##targetNamespace'"
	 * @generated
	 */
	IdShortT getIdShort();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.ConceptDescriptionT#getIdShort <em>Id Short</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id Short</em>' containment reference.
	 * @see #getIdShort()
	 * @generated
	 */
	void setIdShort(IdShortT value);

	/**
	 * Returns the value of the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Category</em>' attribute.
	 * @see #setCategory(String)
	 * @see io.shell.admin.aas._2._0._0Package#getConceptDescriptionT_Category()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='category' namespace='##targetNamespace'"
	 * @generated
	 */
	String getCategory();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.ConceptDescriptionT#getCategory <em>Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Category</em>' attribute.
	 * @see #getCategory()
	 * @generated
	 */
	void setCategory(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference.
	 * @see #setDescription(LangStringSetT)
	 * @see io.shell.admin.aas._2._0._0Package#getConceptDescriptionT_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='description' namespace='##targetNamespace'"
	 * @generated
	 */
	LangStringSetT getDescription();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.ConceptDescriptionT#getDescription <em>Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' containment reference.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(LangStringSetT value);

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' containment reference.
	 * @see #setParent(ReferenceT)
	 * @see io.shell.admin.aas._2._0._0Package#getConceptDescriptionT_Parent()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='parent' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getParent();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.ConceptDescriptionT#getParent <em>Parent</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' containment reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(ReferenceT value);

	/**
	 * Returns the value of the '<em><b>Identification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identification</em>' containment reference.
	 * @see #setIdentification(IdentificationT)
	 * @see io.shell.admin.aas._2._0._0Package#getConceptDescriptionT_Identification()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='identification' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentificationT getIdentification();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.ConceptDescriptionT#getIdentification <em>Identification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Identification</em>' containment reference.
	 * @see #getIdentification()
	 * @generated
	 */
	void setIdentification(IdentificationT value);

	/**
	 * Returns the value of the '<em><b>Administration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Administration</em>' containment reference.
	 * @see #setAdministration(AdministrationT)
	 * @see io.shell.admin.aas._2._0._0Package#getConceptDescriptionT_Administration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='administration' namespace='##targetNamespace'"
	 * @generated
	 */
	AdministrationT getAdministration();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.ConceptDescriptionT#getAdministration <em>Administration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Administration</em>' containment reference.
	 * @see #getAdministration()
	 * @generated
	 */
	void setAdministration(AdministrationT value);

	/**
	 * Returns the value of the '<em><b>Embedded Data Specification</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas._2._0.EmbeddedDataSpecificationT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Embedded Data Specification</em>' containment reference list.
	 * @see io.shell.admin.aas._2._0._0Package#getConceptDescriptionT_EmbeddedDataSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='embeddedDataSpecification' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<EmbeddedDataSpecificationT> getEmbeddedDataSpecification();

	/**
	 * Returns the value of the '<em><b>Is Case Of</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas._2._0.ReferenceT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Case Of</em>' containment reference list.
	 * @see io.shell.admin.aas._2._0._0Package#getConceptDescriptionT_IsCaseOf()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='isCaseOf' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ReferenceT> getIsCaseOf();

} // ConceptDescriptionT
