/**
 */
package io.shell.admin.aas._2._0.impl;

import io.shell.admin.aas._2._0.AdministrationT;
import io.shell.admin.aas._2._0.ConceptDescriptionT;
import io.shell.admin.aas._2._0.EmbeddedDataSpecificationT;
import io.shell.admin.aas._2._0.IdShortT;
import io.shell.admin.aas._2._0.IdentificationT;
import io.shell.admin.aas._2._0.LangStringSetT;
import io.shell.admin.aas._2._0.ReferenceT;
import io.shell.admin.aas._2._0._0Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Concept Description T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.impl.ConceptDescriptionTImpl#getIdShort <em>Id Short</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.ConceptDescriptionTImpl#getCategory <em>Category</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.ConceptDescriptionTImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.ConceptDescriptionTImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.ConceptDescriptionTImpl#getIdentification <em>Identification</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.ConceptDescriptionTImpl#getAdministration <em>Administration</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.ConceptDescriptionTImpl#getEmbeddedDataSpecification <em>Embedded Data Specification</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.ConceptDescriptionTImpl#getIsCaseOf <em>Is Case Of</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConceptDescriptionTImpl extends MinimalEObjectImpl.Container implements ConceptDescriptionT {
	/**
	 * The cached value of the '{@link #getIdShort() <em>Id Short</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdShort()
	 * @generated
	 * @ordered
	 */
	protected IdShortT idShort;

	/**
	 * The default value of the '{@link #getCategory() <em>Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCategory()
	 * @generated
	 * @ordered
	 */
	protected static final String CATEGORY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCategory() <em>Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCategory()
	 * @generated
	 * @ordered
	 */
	protected String category = CATEGORY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected LangStringSetT description;

	/**
	 * The cached value of the '{@link #getParent() <em>Parent</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParent()
	 * @generated
	 * @ordered
	 */
	protected ReferenceT parent;

	/**
	 * The cached value of the '{@link #getIdentification() <em>Identification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdentification()
	 * @generated
	 * @ordered
	 */
	protected IdentificationT identification;

	/**
	 * The cached value of the '{@link #getAdministration() <em>Administration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAdministration()
	 * @generated
	 * @ordered
	 */
	protected AdministrationT administration;

	/**
	 * The cached value of the '{@link #getEmbeddedDataSpecification() <em>Embedded Data Specification</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEmbeddedDataSpecification()
	 * @generated
	 * @ordered
	 */
	protected EList<EmbeddedDataSpecificationT> embeddedDataSpecification;

	/**
	 * The cached value of the '{@link #getIsCaseOf() <em>Is Case Of</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsCaseOf()
	 * @generated
	 * @ordered
	 */
	protected EList<ReferenceT> isCaseOf;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConceptDescriptionTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.CONCEPT_DESCRIPTION_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdShortT getIdShort() {
		return idShort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIdShort(IdShortT newIdShort, NotificationChain msgs) {
		IdShortT oldIdShort = idShort;
		idShort = newIdShort;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.CONCEPT_DESCRIPTION_T__ID_SHORT, oldIdShort, newIdShort);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdShort(IdShortT newIdShort) {
		if (newIdShort != idShort) {
			NotificationChain msgs = null;
			if (idShort != null)
				msgs = ((InternalEObject)idShort).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.CONCEPT_DESCRIPTION_T__ID_SHORT, null, msgs);
			if (newIdShort != null)
				msgs = ((InternalEObject)newIdShort).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.CONCEPT_DESCRIPTION_T__ID_SHORT, null, msgs);
			msgs = basicSetIdShort(newIdShort, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.CONCEPT_DESCRIPTION_T__ID_SHORT, newIdShort, newIdShort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCategory(String newCategory) {
		String oldCategory = category;
		category = newCategory;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.CONCEPT_DESCRIPTION_T__CATEGORY, oldCategory, category));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LangStringSetT getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDescription(LangStringSetT newDescription, NotificationChain msgs) {
		LangStringSetT oldDescription = description;
		description = newDescription;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.CONCEPT_DESCRIPTION_T__DESCRIPTION, oldDescription, newDescription);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(LangStringSetT newDescription) {
		if (newDescription != description) {
			NotificationChain msgs = null;
			if (description != null)
				msgs = ((InternalEObject)description).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.CONCEPT_DESCRIPTION_T__DESCRIPTION, null, msgs);
			if (newDescription != null)
				msgs = ((InternalEObject)newDescription).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.CONCEPT_DESCRIPTION_T__DESCRIPTION, null, msgs);
			msgs = basicSetDescription(newDescription, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.CONCEPT_DESCRIPTION_T__DESCRIPTION, newDescription, newDescription));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceT getParent() {
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParent(ReferenceT newParent, NotificationChain msgs) {
		ReferenceT oldParent = parent;
		parent = newParent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.CONCEPT_DESCRIPTION_T__PARENT, oldParent, newParent);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParent(ReferenceT newParent) {
		if (newParent != parent) {
			NotificationChain msgs = null;
			if (parent != null)
				msgs = ((InternalEObject)parent).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.CONCEPT_DESCRIPTION_T__PARENT, null, msgs);
			if (newParent != null)
				msgs = ((InternalEObject)newParent).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.CONCEPT_DESCRIPTION_T__PARENT, null, msgs);
			msgs = basicSetParent(newParent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.CONCEPT_DESCRIPTION_T__PARENT, newParent, newParent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentificationT getIdentification() {
		return identification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIdentification(IdentificationT newIdentification, NotificationChain msgs) {
		IdentificationT oldIdentification = identification;
		identification = newIdentification;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.CONCEPT_DESCRIPTION_T__IDENTIFICATION, oldIdentification, newIdentification);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdentification(IdentificationT newIdentification) {
		if (newIdentification != identification) {
			NotificationChain msgs = null;
			if (identification != null)
				msgs = ((InternalEObject)identification).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.CONCEPT_DESCRIPTION_T__IDENTIFICATION, null, msgs);
			if (newIdentification != null)
				msgs = ((InternalEObject)newIdentification).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.CONCEPT_DESCRIPTION_T__IDENTIFICATION, null, msgs);
			msgs = basicSetIdentification(newIdentification, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.CONCEPT_DESCRIPTION_T__IDENTIFICATION, newIdentification, newIdentification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdministrationT getAdministration() {
		return administration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAdministration(AdministrationT newAdministration, NotificationChain msgs) {
		AdministrationT oldAdministration = administration;
		administration = newAdministration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.CONCEPT_DESCRIPTION_T__ADMINISTRATION, oldAdministration, newAdministration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAdministration(AdministrationT newAdministration) {
		if (newAdministration != administration) {
			NotificationChain msgs = null;
			if (administration != null)
				msgs = ((InternalEObject)administration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.CONCEPT_DESCRIPTION_T__ADMINISTRATION, null, msgs);
			if (newAdministration != null)
				msgs = ((InternalEObject)newAdministration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.CONCEPT_DESCRIPTION_T__ADMINISTRATION, null, msgs);
			msgs = basicSetAdministration(newAdministration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.CONCEPT_DESCRIPTION_T__ADMINISTRATION, newAdministration, newAdministration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EmbeddedDataSpecificationT> getEmbeddedDataSpecification() {
		if (embeddedDataSpecification == null) {
			embeddedDataSpecification = new EObjectContainmentEList<EmbeddedDataSpecificationT>(EmbeddedDataSpecificationT.class, this, _0Package.CONCEPT_DESCRIPTION_T__EMBEDDED_DATA_SPECIFICATION);
		}
		return embeddedDataSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReferenceT> getIsCaseOf() {
		if (isCaseOf == null) {
			isCaseOf = new EObjectContainmentEList<ReferenceT>(ReferenceT.class, this, _0Package.CONCEPT_DESCRIPTION_T__IS_CASE_OF);
		}
		return isCaseOf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.CONCEPT_DESCRIPTION_T__ID_SHORT:
				return basicSetIdShort(null, msgs);
			case _0Package.CONCEPT_DESCRIPTION_T__DESCRIPTION:
				return basicSetDescription(null, msgs);
			case _0Package.CONCEPT_DESCRIPTION_T__PARENT:
				return basicSetParent(null, msgs);
			case _0Package.CONCEPT_DESCRIPTION_T__IDENTIFICATION:
				return basicSetIdentification(null, msgs);
			case _0Package.CONCEPT_DESCRIPTION_T__ADMINISTRATION:
				return basicSetAdministration(null, msgs);
			case _0Package.CONCEPT_DESCRIPTION_T__EMBEDDED_DATA_SPECIFICATION:
				return ((InternalEList<?>)getEmbeddedDataSpecification()).basicRemove(otherEnd, msgs);
			case _0Package.CONCEPT_DESCRIPTION_T__IS_CASE_OF:
				return ((InternalEList<?>)getIsCaseOf()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.CONCEPT_DESCRIPTION_T__ID_SHORT:
				return getIdShort();
			case _0Package.CONCEPT_DESCRIPTION_T__CATEGORY:
				return getCategory();
			case _0Package.CONCEPT_DESCRIPTION_T__DESCRIPTION:
				return getDescription();
			case _0Package.CONCEPT_DESCRIPTION_T__PARENT:
				return getParent();
			case _0Package.CONCEPT_DESCRIPTION_T__IDENTIFICATION:
				return getIdentification();
			case _0Package.CONCEPT_DESCRIPTION_T__ADMINISTRATION:
				return getAdministration();
			case _0Package.CONCEPT_DESCRIPTION_T__EMBEDDED_DATA_SPECIFICATION:
				return getEmbeddedDataSpecification();
			case _0Package.CONCEPT_DESCRIPTION_T__IS_CASE_OF:
				return getIsCaseOf();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.CONCEPT_DESCRIPTION_T__ID_SHORT:
				setIdShort((IdShortT)newValue);
				return;
			case _0Package.CONCEPT_DESCRIPTION_T__CATEGORY:
				setCategory((String)newValue);
				return;
			case _0Package.CONCEPT_DESCRIPTION_T__DESCRIPTION:
				setDescription((LangStringSetT)newValue);
				return;
			case _0Package.CONCEPT_DESCRIPTION_T__PARENT:
				setParent((ReferenceT)newValue);
				return;
			case _0Package.CONCEPT_DESCRIPTION_T__IDENTIFICATION:
				setIdentification((IdentificationT)newValue);
				return;
			case _0Package.CONCEPT_DESCRIPTION_T__ADMINISTRATION:
				setAdministration((AdministrationT)newValue);
				return;
			case _0Package.CONCEPT_DESCRIPTION_T__EMBEDDED_DATA_SPECIFICATION:
				getEmbeddedDataSpecification().clear();
				getEmbeddedDataSpecification().addAll((Collection<? extends EmbeddedDataSpecificationT>)newValue);
				return;
			case _0Package.CONCEPT_DESCRIPTION_T__IS_CASE_OF:
				getIsCaseOf().clear();
				getIsCaseOf().addAll((Collection<? extends ReferenceT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.CONCEPT_DESCRIPTION_T__ID_SHORT:
				setIdShort((IdShortT)null);
				return;
			case _0Package.CONCEPT_DESCRIPTION_T__CATEGORY:
				setCategory(CATEGORY_EDEFAULT);
				return;
			case _0Package.CONCEPT_DESCRIPTION_T__DESCRIPTION:
				setDescription((LangStringSetT)null);
				return;
			case _0Package.CONCEPT_DESCRIPTION_T__PARENT:
				setParent((ReferenceT)null);
				return;
			case _0Package.CONCEPT_DESCRIPTION_T__IDENTIFICATION:
				setIdentification((IdentificationT)null);
				return;
			case _0Package.CONCEPT_DESCRIPTION_T__ADMINISTRATION:
				setAdministration((AdministrationT)null);
				return;
			case _0Package.CONCEPT_DESCRIPTION_T__EMBEDDED_DATA_SPECIFICATION:
				getEmbeddedDataSpecification().clear();
				return;
			case _0Package.CONCEPT_DESCRIPTION_T__IS_CASE_OF:
				getIsCaseOf().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.CONCEPT_DESCRIPTION_T__ID_SHORT:
				return idShort != null;
			case _0Package.CONCEPT_DESCRIPTION_T__CATEGORY:
				return CATEGORY_EDEFAULT == null ? category != null : !CATEGORY_EDEFAULT.equals(category);
			case _0Package.CONCEPT_DESCRIPTION_T__DESCRIPTION:
				return description != null;
			case _0Package.CONCEPT_DESCRIPTION_T__PARENT:
				return parent != null;
			case _0Package.CONCEPT_DESCRIPTION_T__IDENTIFICATION:
				return identification != null;
			case _0Package.CONCEPT_DESCRIPTION_T__ADMINISTRATION:
				return administration != null;
			case _0Package.CONCEPT_DESCRIPTION_T__EMBEDDED_DATA_SPECIFICATION:
				return embeddedDataSpecification != null && !embeddedDataSpecification.isEmpty();
			case _0Package.CONCEPT_DESCRIPTION_T__IS_CASE_OF:
				return isCaseOf != null && !isCaseOf.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (category: ");
		result.append(category);
		result.append(')');
		return result.toString();
	}

} //ConceptDescriptionTImpl
