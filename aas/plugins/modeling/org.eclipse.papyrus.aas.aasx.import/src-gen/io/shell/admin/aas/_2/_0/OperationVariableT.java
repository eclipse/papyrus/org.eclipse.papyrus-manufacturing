/**
 */
package io.shell.admin.aas._2._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operation Variable T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.OperationVariableT#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getOperationVariableT()
 * @model extendedMetaData="name='operationVariable_t' kind='elementOnly'"
 * @generated
 */
public interface OperationVariableT extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(SubmodelElementT)
	 * @see io.shell.admin.aas._2._0._0Package#getOperationVariableT_Value()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='value' namespace='##targetNamespace'"
	 * @generated
	 */
	SubmodelElementT getValue();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.OperationVariableT#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(SubmodelElementT value);

} // OperationVariableT
