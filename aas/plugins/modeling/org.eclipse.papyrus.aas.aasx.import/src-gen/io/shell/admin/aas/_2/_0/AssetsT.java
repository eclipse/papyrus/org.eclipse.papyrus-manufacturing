/**
 */
package io.shell.admin.aas._2._0;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assets T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.AssetsT#getAsset <em>Asset</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getAssetsT()
 * @model extendedMetaData="name='assets_t' kind='elementOnly'"
 * @generated
 */
public interface AssetsT extends EObject {
	/**
	 * Returns the value of the '<em><b>Asset</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas._2._0.AssetT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asset</em>' containment reference list.
	 * @see io.shell.admin.aas._2._0._0Package#getAssetsT_Asset()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='asset' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<AssetT> getAsset();

} // AssetsT
