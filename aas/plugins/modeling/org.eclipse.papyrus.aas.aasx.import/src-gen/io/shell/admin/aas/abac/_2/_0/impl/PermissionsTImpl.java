/**
 */
package io.shell.admin.aas.abac._2._0.impl;

import io.shell.admin.aas._2._0.PropertyT;

import io.shell.admin.aas.abac._2._0.PermissionKind;
import io.shell.admin.aas.abac._2._0.PermissionsT;
import io.shell.admin.aas.abac._2._0._0Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Permissions T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.PermissionsTImpl#getPermission <em>Permission</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.PermissionsTImpl#getKindOfPermission <em>Kind Of Permission</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PermissionsTImpl extends MinimalEObjectImpl.Container implements PermissionsT {
	/**
	 * The cached value of the '{@link #getPermission() <em>Permission</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPermission()
	 * @generated
	 * @ordered
	 */
	protected PropertyT permission;

	/**
	 * The default value of the '{@link #getKindOfPermission() <em>Kind Of Permission</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindOfPermission()
	 * @generated
	 * @ordered
	 */
	protected static final PermissionKind KIND_OF_PERMISSION_EDEFAULT = PermissionKind.ALLOW;

	/**
	 * The cached value of the '{@link #getKindOfPermission() <em>Kind Of Permission</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindOfPermission()
	 * @generated
	 * @ordered
	 */
	protected PermissionKind kindOfPermission = KIND_OF_PERMISSION_EDEFAULT;

	/**
	 * This is true if the Kind Of Permission attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean kindOfPermissionESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PermissionsTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.PERMISSIONS_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyT getPermission() {
		return permission;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPermission(PropertyT newPermission, NotificationChain msgs) {
		PropertyT oldPermission = permission;
		permission = newPermission;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.PERMISSIONS_T__PERMISSION, oldPermission, newPermission);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPermission(PropertyT newPermission) {
		if (newPermission != permission) {
			NotificationChain msgs = null;
			if (permission != null)
				msgs = ((InternalEObject)permission).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.PERMISSIONS_T__PERMISSION, null, msgs);
			if (newPermission != null)
				msgs = ((InternalEObject)newPermission).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.PERMISSIONS_T__PERMISSION, null, msgs);
			msgs = basicSetPermission(newPermission, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.PERMISSIONS_T__PERMISSION, newPermission, newPermission));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PermissionKind getKindOfPermission() {
		return kindOfPermission;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKindOfPermission(PermissionKind newKindOfPermission) {
		PermissionKind oldKindOfPermission = kindOfPermission;
		kindOfPermission = newKindOfPermission == null ? KIND_OF_PERMISSION_EDEFAULT : newKindOfPermission;
		boolean oldKindOfPermissionESet = kindOfPermissionESet;
		kindOfPermissionESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.PERMISSIONS_T__KIND_OF_PERMISSION, oldKindOfPermission, kindOfPermission, !oldKindOfPermissionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetKindOfPermission() {
		PermissionKind oldKindOfPermission = kindOfPermission;
		boolean oldKindOfPermissionESet = kindOfPermissionESet;
		kindOfPermission = KIND_OF_PERMISSION_EDEFAULT;
		kindOfPermissionESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, _0Package.PERMISSIONS_T__KIND_OF_PERMISSION, oldKindOfPermission, KIND_OF_PERMISSION_EDEFAULT, oldKindOfPermissionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetKindOfPermission() {
		return kindOfPermissionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.PERMISSIONS_T__PERMISSION:
				return basicSetPermission(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.PERMISSIONS_T__PERMISSION:
				return getPermission();
			case _0Package.PERMISSIONS_T__KIND_OF_PERMISSION:
				return getKindOfPermission();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.PERMISSIONS_T__PERMISSION:
				setPermission((PropertyT)newValue);
				return;
			case _0Package.PERMISSIONS_T__KIND_OF_PERMISSION:
				setKindOfPermission((PermissionKind)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.PERMISSIONS_T__PERMISSION:
				setPermission((PropertyT)null);
				return;
			case _0Package.PERMISSIONS_T__KIND_OF_PERMISSION:
				unsetKindOfPermission();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.PERMISSIONS_T__PERMISSION:
				return permission != null;
			case _0Package.PERMISSIONS_T__KIND_OF_PERMISSION:
				return isSetKindOfPermission();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (kindOfPermission: ");
		if (kindOfPermissionESet) result.append(kindOfPermission); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //PermissionsTImpl
