/**
 */
package io.shell.admin.aas._2._0;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Abstract T</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see io.shell.admin.aas._2._0._0Package#getEventAbstractT()
 * @model extendedMetaData="name='eventAbstract_t' kind='elementOnly'"
 * @generated
 */
public interface EventAbstractT extends SubmodelElementAbstractT {
} // EventAbstractT
