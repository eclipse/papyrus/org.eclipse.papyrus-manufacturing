/**
 */
package io.shell.admin.aas.abac._2._0.impl;

import io.shell.admin.aas.abac._2._0.PolicyDecisionPointT;
import io.shell.admin.aas.abac._2._0._0Package;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Policy Decision Point T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.PolicyDecisionPointTImpl#isExternalPolicyDecisionPoint <em>External Policy Decision Point</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PolicyDecisionPointTImpl extends MinimalEObjectImpl.Container implements PolicyDecisionPointT {
	/**
	 * The default value of the '{@link #isExternalPolicyDecisionPoint() <em>External Policy Decision Point</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExternalPolicyDecisionPoint()
	 * @generated
	 * @ordered
	 */
	protected static final boolean EXTERNAL_POLICY_DECISION_POINT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isExternalPolicyDecisionPoint() <em>External Policy Decision Point</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExternalPolicyDecisionPoint()
	 * @generated
	 * @ordered
	 */
	protected boolean externalPolicyDecisionPoint = EXTERNAL_POLICY_DECISION_POINT_EDEFAULT;

	/**
	 * This is true if the External Policy Decision Point attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean externalPolicyDecisionPointESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PolicyDecisionPointTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.POLICY_DECISION_POINT_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isExternalPolicyDecisionPoint() {
		return externalPolicyDecisionPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExternalPolicyDecisionPoint(boolean newExternalPolicyDecisionPoint) {
		boolean oldExternalPolicyDecisionPoint = externalPolicyDecisionPoint;
		externalPolicyDecisionPoint = newExternalPolicyDecisionPoint;
		boolean oldExternalPolicyDecisionPointESet = externalPolicyDecisionPointESet;
		externalPolicyDecisionPointESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.POLICY_DECISION_POINT_T__EXTERNAL_POLICY_DECISION_POINT, oldExternalPolicyDecisionPoint, externalPolicyDecisionPoint, !oldExternalPolicyDecisionPointESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetExternalPolicyDecisionPoint() {
		boolean oldExternalPolicyDecisionPoint = externalPolicyDecisionPoint;
		boolean oldExternalPolicyDecisionPointESet = externalPolicyDecisionPointESet;
		externalPolicyDecisionPoint = EXTERNAL_POLICY_DECISION_POINT_EDEFAULT;
		externalPolicyDecisionPointESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, _0Package.POLICY_DECISION_POINT_T__EXTERNAL_POLICY_DECISION_POINT, oldExternalPolicyDecisionPoint, EXTERNAL_POLICY_DECISION_POINT_EDEFAULT, oldExternalPolicyDecisionPointESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetExternalPolicyDecisionPoint() {
		return externalPolicyDecisionPointESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.POLICY_DECISION_POINT_T__EXTERNAL_POLICY_DECISION_POINT:
				return isExternalPolicyDecisionPoint();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.POLICY_DECISION_POINT_T__EXTERNAL_POLICY_DECISION_POINT:
				setExternalPolicyDecisionPoint((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.POLICY_DECISION_POINT_T__EXTERNAL_POLICY_DECISION_POINT:
				unsetExternalPolicyDecisionPoint();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.POLICY_DECISION_POINT_T__EXTERNAL_POLICY_DECISION_POINT:
				return isSetExternalPolicyDecisionPoint();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (externalPolicyDecisionPoint: ");
		if (externalPolicyDecisionPointESet) result.append(externalPolicyDecisionPoint); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //PolicyDecisionPointTImpl
