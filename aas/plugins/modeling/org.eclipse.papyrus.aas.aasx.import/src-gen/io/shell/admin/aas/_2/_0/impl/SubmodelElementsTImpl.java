/**
 */
package io.shell.admin.aas._2._0.impl;

import io.shell.admin.aas._2._0.SubmodelElementT;
import io.shell.admin.aas._2._0.SubmodelElementsT;
import io.shell.admin.aas._2._0._0Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Submodel Elements T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.impl.SubmodelElementsTImpl#getSubmodelElement <em>Submodel Element</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubmodelElementsTImpl extends MinimalEObjectImpl.Container implements SubmodelElementsT {
	/**
	 * The cached value of the '{@link #getSubmodelElement() <em>Submodel Element</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubmodelElement()
	 * @generated
	 * @ordered
	 */
	protected EList<SubmodelElementT> submodelElement;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubmodelElementsTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.SUBMODEL_ELEMENTS_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SubmodelElementT> getSubmodelElement() {
		if (submodelElement == null) {
			submodelElement = new EObjectContainmentEList<SubmodelElementT>(SubmodelElementT.class, this, _0Package.SUBMODEL_ELEMENTS_T__SUBMODEL_ELEMENT);
		}
		return submodelElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.SUBMODEL_ELEMENTS_T__SUBMODEL_ELEMENT:
				return ((InternalEList<?>)getSubmodelElement()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.SUBMODEL_ELEMENTS_T__SUBMODEL_ELEMENT:
				return getSubmodelElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.SUBMODEL_ELEMENTS_T__SUBMODEL_ELEMENT:
				getSubmodelElement().clear();
				getSubmodelElement().addAll((Collection<? extends SubmodelElementT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.SUBMODEL_ELEMENTS_T__SUBMODEL_ELEMENT:
				getSubmodelElement().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.SUBMODEL_ELEMENTS_T__SUBMODEL_ELEMENT:
				return submodelElement != null && !submodelElement.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SubmodelElementsTImpl
