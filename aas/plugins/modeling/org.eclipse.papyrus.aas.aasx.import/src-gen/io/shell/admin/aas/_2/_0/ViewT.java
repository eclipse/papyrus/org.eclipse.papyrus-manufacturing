/**
 */
package io.shell.admin.aas._2._0;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>View T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.ViewT#getIdShort <em>Id Short</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.ViewT#getCategory <em>Category</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.ViewT#getDescription <em>Description</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.ViewT#getParent <em>Parent</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.ViewT#getSemanticId <em>Semantic Id</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.ViewT#getEmbeddedDataSpecification <em>Embedded Data Specification</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.ViewT#getContainedElements <em>Contained Elements</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getViewT()
 * @model extendedMetaData="name='view_t' kind='elementOnly'"
 * @generated
 */
public interface ViewT extends EObject {
	/**
	 * Returns the value of the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id Short</em>' containment reference.
	 * @see #setIdShort(IdShortT)
	 * @see io.shell.admin.aas._2._0._0Package#getViewT_IdShort()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='idShort' namespace='##targetNamespace'"
	 * @generated
	 */
	IdShortT getIdShort();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.ViewT#getIdShort <em>Id Short</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id Short</em>' containment reference.
	 * @see #getIdShort()
	 * @generated
	 */
	void setIdShort(IdShortT value);

	/**
	 * Returns the value of the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Category</em>' attribute.
	 * @see #setCategory(String)
	 * @see io.shell.admin.aas._2._0._0Package#getViewT_Category()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='category' namespace='##targetNamespace'"
	 * @generated
	 */
	String getCategory();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.ViewT#getCategory <em>Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Category</em>' attribute.
	 * @see #getCategory()
	 * @generated
	 */
	void setCategory(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference.
	 * @see #setDescription(LangStringSetT)
	 * @see io.shell.admin.aas._2._0._0Package#getViewT_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='description' namespace='##targetNamespace'"
	 * @generated
	 */
	LangStringSetT getDescription();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.ViewT#getDescription <em>Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' containment reference.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(LangStringSetT value);

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' containment reference.
	 * @see #setParent(ReferenceT)
	 * @see io.shell.admin.aas._2._0._0Package#getViewT_Parent()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='parent' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getParent();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.ViewT#getParent <em>Parent</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' containment reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(ReferenceT value);

	/**
	 * Returns the value of the '<em><b>Semantic Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semantic Id</em>' containment reference.
	 * @see #setSemanticId(SemanticIdT)
	 * @see io.shell.admin.aas._2._0._0Package#getViewT_SemanticId()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='semanticId' namespace='##targetNamespace'"
	 * @generated
	 */
	SemanticIdT getSemanticId();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.ViewT#getSemanticId <em>Semantic Id</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Semantic Id</em>' containment reference.
	 * @see #getSemanticId()
	 * @generated
	 */
	void setSemanticId(SemanticIdT value);

	/**
	 * Returns the value of the '<em><b>Embedded Data Specification</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas._2._0.EmbeddedDataSpecificationT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Embedded Data Specification</em>' containment reference list.
	 * @see io.shell.admin.aas._2._0._0Package#getViewT_EmbeddedDataSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='embeddedDataSpecification' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<EmbeddedDataSpecificationT> getEmbeddedDataSpecification();

	/**
	 * Returns the value of the '<em><b>Contained Elements</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contained Elements</em>' containment reference.
	 * @see #setContainedElements(ContainedElementsT)
	 * @see io.shell.admin.aas._2._0._0Package#getViewT_ContainedElements()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='containedElements' namespace='##targetNamespace'"
	 * @generated
	 */
	ContainedElementsT getContainedElements();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.ViewT#getContainedElements <em>Contained Elements</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Contained Elements</em>' containment reference.
	 * @see #getContainedElements()
	 * @generated
	 */
	void setContainedElements(ContainedElementsT value);

} // ViewT
