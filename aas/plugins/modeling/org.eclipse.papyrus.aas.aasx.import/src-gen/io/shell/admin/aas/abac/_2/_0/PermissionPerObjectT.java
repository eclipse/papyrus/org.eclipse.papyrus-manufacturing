/**
 */
package io.shell.admin.aas.abac._2._0;

import io.shell.admin.aas._2._0.ReferenceT;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Permission Per Object T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.PermissionPerObjectT#getObject <em>Object</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.PermissionPerObjectT#getTargetObjectAttributes <em>Target Object Attributes</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.PermissionPerObjectT#getPermissions <em>Permissions</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas.abac._2._0._0Package#getPermissionPerObjectT()
 * @model extendedMetaData="name='permissionPerObject_t' kind='elementOnly'"
 * @generated
 */
public interface PermissionPerObjectT extends EObject {
	/**
	 * Returns the value of the '<em><b>Object</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object</em>' containment reference.
	 * @see #setObject(ReferenceT)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getPermissionPerObjectT_Object()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='object' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getObject();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.PermissionPerObjectT#getObject <em>Object</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object</em>' containment reference.
	 * @see #getObject()
	 * @generated
	 */
	void setObject(ReferenceT value);

	/**
	 * Returns the value of the '<em><b>Target Object Attributes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Object Attributes</em>' containment reference.
	 * @see #setTargetObjectAttributes(ObjectAttributesT)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getPermissionPerObjectT_TargetObjectAttributes()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='targetObjectAttributes' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectAttributesT getTargetObjectAttributes();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.PermissionPerObjectT#getTargetObjectAttributes <em>Target Object Attributes</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Object Attributes</em>' containment reference.
	 * @see #getTargetObjectAttributes()
	 * @generated
	 */
	void setTargetObjectAttributes(ObjectAttributesT value);

	/**
	 * Returns the value of the '<em><b>Permissions</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Permissions</em>' containment reference.
	 * @see #setPermissions(PermissionsT)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getPermissionPerObjectT_Permissions()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='permissions' namespace='##targetNamespace'"
	 * @generated
	 */
	PermissionsT getPermissions();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.PermissionPerObjectT#getPermissions <em>Permissions</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Permissions</em>' containment reference.
	 * @see #getPermissions()
	 * @generated
	 */
	void setPermissions(PermissionsT value);

} // PermissionPerObjectT
