/**
 */
package io.shell.admin.aas.abac._2._0.impl;

import io.shell.admin.aas.abac._2._0.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class _0FactoryImpl extends EFactoryImpl implements _0Factory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static _0Factory init() {
		try {
			_0Factory the_0Factory = (_0Factory)EPackage.Registry.INSTANCE.getEFactory(_0Package.eNS_URI);
			if (the_0Factory != null) {
				return the_0Factory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new _0FactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public _0FactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case _0Package.ACCESS_CONTROL_POLICY_POINTS_T: return createAccessControlPolicyPointsT();
			case _0Package.ACCESS_CONTROL_T: return createAccessControlT();
			case _0Package.ACCESS_PERMISSION_RULES_T: return createAccessPermissionRulesT();
			case _0Package.ACCESS_PERMISSION_RULE_T: return createAccessPermissionRuleT();
			case _0Package.BLOB_CERTIFICATE_T: return createBlobCertificateT();
			case _0Package.CERTIFICATE_ABSTRACT_T: return createCertificateAbstractT();
			case _0Package.CERTIFICATES_T: return createCertificatesT();
			case _0Package.CERTIFICATE_T: return createCertificateT();
			case _0Package.CONTAINED_EXTENSIONS_T: return createContainedExtensionsT();
			case _0Package.INTERNAL_INFORMATION_POINTS: return createInternalInformationPoints();
			case _0Package.OBJECT_ATTRIBUTES_T: return createObjectAttributesT();
			case _0Package.PERMISSION_PER_OBJECT_T: return createPermissionPerObjectT();
			case _0Package.PERMISSIONS_T: return createPermissionsT();
			case _0Package.POLICY_ADMINISTRATION_POINT_T: return createPolicyAdministrationPointT();
			case _0Package.POLICY_DECISION_POINT_T: return createPolicyDecisionPointT();
			case _0Package.POLICY_ENFORCEMENT_POINT_T: return createPolicyEnforcementPointT();
			case _0Package.POLICY_INFORMATION_POINTS_T: return createPolicyInformationPointsT();
			case _0Package.SECURITY_T: return createSecurityT();
			case _0Package.SUBJECT_ATTRIBUTES_T: return createSubjectAttributesT();
			case _0Package.DOCUMENT_ROOT: return createDocumentRoot();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case _0Package.PERMISSION_KIND:
				return createPermissionKindFromString(eDataType, initialValue);
			case _0Package.PERMISSION_KIND_OBJECT:
				return createPermissionKindObjectFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case _0Package.PERMISSION_KIND:
				return convertPermissionKindToString(eDataType, instanceValue);
			case _0Package.PERMISSION_KIND_OBJECT:
				return convertPermissionKindObjectToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessControlPolicyPointsT createAccessControlPolicyPointsT() {
		AccessControlPolicyPointsTImpl accessControlPolicyPointsT = new AccessControlPolicyPointsTImpl();
		return accessControlPolicyPointsT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessControlT createAccessControlT() {
		AccessControlTImpl accessControlT = new AccessControlTImpl();
		return accessControlT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessPermissionRulesT createAccessPermissionRulesT() {
		AccessPermissionRulesTImpl accessPermissionRulesT = new AccessPermissionRulesTImpl();
		return accessPermissionRulesT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessPermissionRuleT createAccessPermissionRuleT() {
		AccessPermissionRuleTImpl accessPermissionRuleT = new AccessPermissionRuleTImpl();
		return accessPermissionRuleT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlobCertificateT createBlobCertificateT() {
		BlobCertificateTImpl blobCertificateT = new BlobCertificateTImpl();
		return blobCertificateT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CertificateAbstractT createCertificateAbstractT() {
		CertificateAbstractTImpl certificateAbstractT = new CertificateAbstractTImpl();
		return certificateAbstractT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CertificatesT createCertificatesT() {
		CertificatesTImpl certificatesT = new CertificatesTImpl();
		return certificatesT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CertificateT createCertificateT() {
		CertificateTImpl certificateT = new CertificateTImpl();
		return certificateT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ContainedExtensionsT createContainedExtensionsT() {
		ContainedExtensionsTImpl containedExtensionsT = new ContainedExtensionsTImpl();
		return containedExtensionsT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InternalInformationPoints createInternalInformationPoints() {
		InternalInformationPointsImpl internalInformationPoints = new InternalInformationPointsImpl();
		return internalInformationPoints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectAttributesT createObjectAttributesT() {
		ObjectAttributesTImpl objectAttributesT = new ObjectAttributesTImpl();
		return objectAttributesT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PermissionPerObjectT createPermissionPerObjectT() {
		PermissionPerObjectTImpl permissionPerObjectT = new PermissionPerObjectTImpl();
		return permissionPerObjectT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PermissionsT createPermissionsT() {
		PermissionsTImpl permissionsT = new PermissionsTImpl();
		return permissionsT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PolicyAdministrationPointT createPolicyAdministrationPointT() {
		PolicyAdministrationPointTImpl policyAdministrationPointT = new PolicyAdministrationPointTImpl();
		return policyAdministrationPointT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PolicyDecisionPointT createPolicyDecisionPointT() {
		PolicyDecisionPointTImpl policyDecisionPointT = new PolicyDecisionPointTImpl();
		return policyDecisionPointT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PolicyEnforcementPointT createPolicyEnforcementPointT() {
		PolicyEnforcementPointTImpl policyEnforcementPointT = new PolicyEnforcementPointTImpl();
		return policyEnforcementPointT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PolicyInformationPointsT createPolicyInformationPointsT() {
		PolicyInformationPointsTImpl policyInformationPointsT = new PolicyInformationPointsTImpl();
		return policyInformationPointsT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SecurityT createSecurityT() {
		SecurityTImpl securityT = new SecurityTImpl();
		return securityT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubjectAttributesT createSubjectAttributesT() {
		SubjectAttributesTImpl subjectAttributesT = new SubjectAttributesTImpl();
		return subjectAttributesT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DocumentRoot createDocumentRoot() {
		DocumentRootImpl documentRoot = new DocumentRootImpl();
		return documentRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PermissionKind createPermissionKindFromString(EDataType eDataType, String initialValue) {
		PermissionKind result = PermissionKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPermissionKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PermissionKind createPermissionKindObjectFromString(EDataType eDataType, String initialValue) {
		return createPermissionKindFromString(_0Package.Literals.PERMISSION_KIND, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPermissionKindObjectToString(EDataType eDataType, Object instanceValue) {
		return convertPermissionKindToString(_0Package.Literals.PERMISSION_KIND, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public _0Package get_0Package() {
		return (_0Package)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static _0Package getPackage() {
		return _0Package.eINSTANCE;
	}

} //_0FactoryImpl
