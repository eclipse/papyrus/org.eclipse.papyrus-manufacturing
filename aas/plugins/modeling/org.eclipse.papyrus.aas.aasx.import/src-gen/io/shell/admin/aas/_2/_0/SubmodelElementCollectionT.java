/**
 */
package io.shell.admin.aas._2._0;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Submodel Element Collection T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.SubmodelElementCollectionT#getValue <em>Value</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.SubmodelElementCollectionT#isOrdered <em>Ordered</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.SubmodelElementCollectionT#isAllowDuplicates <em>Allow Duplicates</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getSubmodelElementCollectionT()
 * @model extendedMetaData="name='submodelElementCollection_t' kind='elementOnly'"
 * @generated
 */
public interface SubmodelElementCollectionT extends SubmodelElementAbstractT {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(SubmodelElementsT)
	 * @see io.shell.admin.aas._2._0._0Package#getSubmodelElementCollectionT_Value()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='value' namespace='##targetNamespace'"
	 * @generated
	 */
	SubmodelElementsT getValue();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.SubmodelElementCollectionT#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(SubmodelElementsT value);

	/**
	 * Returns the value of the '<em><b>Ordered</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ordered</em>' attribute.
	 * @see #isSetOrdered()
	 * @see #unsetOrdered()
	 * @see #setOrdered(boolean)
	 * @see io.shell.admin.aas._2._0._0Package#getSubmodelElementCollectionT_Ordered()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean" required="true"
	 *        extendedMetaData="kind='element' name='ordered' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isOrdered();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.SubmodelElementCollectionT#isOrdered <em>Ordered</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ordered</em>' attribute.
	 * @see #isSetOrdered()
	 * @see #unsetOrdered()
	 * @see #isOrdered()
	 * @generated
	 */
	void setOrdered(boolean value);

	/**
	 * Unsets the value of the '{@link io.shell.admin.aas._2._0.SubmodelElementCollectionT#isOrdered <em>Ordered</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOrdered()
	 * @see #isOrdered()
	 * @see #setOrdered(boolean)
	 * @generated
	 */
	void unsetOrdered();

	/**
	 * Returns whether the value of the '{@link io.shell.admin.aas._2._0.SubmodelElementCollectionT#isOrdered <em>Ordered</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Ordered</em>' attribute is set.
	 * @see #unsetOrdered()
	 * @see #isOrdered()
	 * @see #setOrdered(boolean)
	 * @generated
	 */
	boolean isSetOrdered();

	/**
	 * Returns the value of the '<em><b>Allow Duplicates</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Allow Duplicates</em>' attribute.
	 * @see #isSetAllowDuplicates()
	 * @see #unsetAllowDuplicates()
	 * @see #setAllowDuplicates(boolean)
	 * @see io.shell.admin.aas._2._0._0Package#getSubmodelElementCollectionT_AllowDuplicates()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean" required="true"
	 *        extendedMetaData="kind='element' name='allowDuplicates' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isAllowDuplicates();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.SubmodelElementCollectionT#isAllowDuplicates <em>Allow Duplicates</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Allow Duplicates</em>' attribute.
	 * @see #isSetAllowDuplicates()
	 * @see #unsetAllowDuplicates()
	 * @see #isAllowDuplicates()
	 * @generated
	 */
	void setAllowDuplicates(boolean value);

	/**
	 * Unsets the value of the '{@link io.shell.admin.aas._2._0.SubmodelElementCollectionT#isAllowDuplicates <em>Allow Duplicates</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAllowDuplicates()
	 * @see #isAllowDuplicates()
	 * @see #setAllowDuplicates(boolean)
	 * @generated
	 */
	void unsetAllowDuplicates();

	/**
	 * Returns whether the value of the '{@link io.shell.admin.aas._2._0.SubmodelElementCollectionT#isAllowDuplicates <em>Allow Duplicates</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Allow Duplicates</em>' attribute is set.
	 * @see #unsetAllowDuplicates()
	 * @see #isAllowDuplicates()
	 * @see #setAllowDuplicates(boolean)
	 * @generated
	 */
	boolean isSetAllowDuplicates();

} // SubmodelElementCollectionT
