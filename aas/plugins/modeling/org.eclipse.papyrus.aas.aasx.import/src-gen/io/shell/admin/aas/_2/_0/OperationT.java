/**
 */
package io.shell.admin.aas._2._0;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operation T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.OperationT#getInputVariable <em>Input Variable</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.OperationT#getOutputVariable <em>Output Variable</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.OperationT#getInoutputVariable <em>Inoutput Variable</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getOperationT()
 * @model extendedMetaData="name='operation_t' kind='elementOnly'"
 * @generated
 */
public interface OperationT extends SubmodelElementAbstractT {
	/**
	 * Returns the value of the '<em><b>Input Variable</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas._2._0.OperationVariableT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Variable</em>' containment reference list.
	 * @see io.shell.admin.aas._2._0._0Package#getOperationT_InputVariable()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='inputVariable' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OperationVariableT> getInputVariable();

	/**
	 * Returns the value of the '<em><b>Output Variable</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas._2._0.OperationVariableT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Variable</em>' containment reference list.
	 * @see io.shell.admin.aas._2._0._0Package#getOperationT_OutputVariable()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='outputVariable' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OperationVariableT> getOutputVariable();

	/**
	 * Returns the value of the '<em><b>Inoutput Variable</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas._2._0.OperationVariableT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inoutput Variable</em>' containment reference list.
	 * @see io.shell.admin.aas._2._0._0Package#getOperationT_InoutputVariable()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='inoutputVariable' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OperationVariableT> getInoutputVariable();

} // OperationT
