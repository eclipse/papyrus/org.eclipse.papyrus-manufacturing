/**
 */
package io.shell.admin.aas.abac._2._0;

import io.shell.admin.aas._2._0.ReferencesT;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Security T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.SecurityT#getAccessControlPolicyPoints <em>Access Control Policy Points</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.SecurityT#getCertificates <em>Certificates</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.SecurityT#getRequiredCertificateExtensions <em>Required Certificate Extensions</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas.abac._2._0._0Package#getSecurityT()
 * @model extendedMetaData="name='security_t' kind='elementOnly'"
 * @generated
 */
public interface SecurityT extends EObject {
	/**
	 * Returns the value of the '<em><b>Access Control Policy Points</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access Control Policy Points</em>' containment reference.
	 * @see #setAccessControlPolicyPoints(AccessControlPolicyPointsT)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getSecurityT_AccessControlPolicyPoints()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='accessControlPolicyPoints' namespace='##targetNamespace'"
	 * @generated
	 */
	AccessControlPolicyPointsT getAccessControlPolicyPoints();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.SecurityT#getAccessControlPolicyPoints <em>Access Control Policy Points</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access Control Policy Points</em>' containment reference.
	 * @see #getAccessControlPolicyPoints()
	 * @generated
	 */
	void setAccessControlPolicyPoints(AccessControlPolicyPointsT value);

	/**
	 * Returns the value of the '<em><b>Certificates</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Certificates</em>' containment reference.
	 * @see #setCertificates(CertificatesT)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getSecurityT_Certificates()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='certificates' namespace='##targetNamespace'"
	 * @generated
	 */
	CertificatesT getCertificates();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.SecurityT#getCertificates <em>Certificates</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Certificates</em>' containment reference.
	 * @see #getCertificates()
	 * @generated
	 */
	void setCertificates(CertificatesT value);

	/**
	 * Returns the value of the '<em><b>Required Certificate Extensions</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Certificate Extensions</em>' containment reference.
	 * @see #setRequiredCertificateExtensions(ReferencesT)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getSecurityT_RequiredCertificateExtensions()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='requiredCertificateExtensions' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferencesT getRequiredCertificateExtensions();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.SecurityT#getRequiredCertificateExtensions <em>Required Certificate Extensions</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required Certificate Extensions</em>' containment reference.
	 * @see #getRequiredCertificateExtensions()
	 * @generated
	 */
	void setRequiredCertificateExtensions(ReferencesT value);

} // SecurityT
