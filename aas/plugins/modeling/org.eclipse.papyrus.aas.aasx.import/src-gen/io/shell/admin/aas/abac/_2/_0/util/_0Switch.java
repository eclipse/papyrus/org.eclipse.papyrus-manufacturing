/**
 */
package io.shell.admin.aas.abac._2._0.util;

import io.shell.admin.aas.abac._2._0.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see io.shell.admin.aas.abac._2._0._0Package
 * @generated
 */
public class _0Switch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static _0Package modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public _0Switch() {
		if (modelPackage == null) {
			modelPackage = _0Package.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case _0Package.ACCESS_CONTROL_POLICY_POINTS_T: {
				AccessControlPolicyPointsT accessControlPolicyPointsT = (AccessControlPolicyPointsT)theEObject;
				T result = caseAccessControlPolicyPointsT(accessControlPolicyPointsT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.ACCESS_CONTROL_T: {
				AccessControlT accessControlT = (AccessControlT)theEObject;
				T result = caseAccessControlT(accessControlT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.ACCESS_PERMISSION_RULES_T: {
				AccessPermissionRulesT accessPermissionRulesT = (AccessPermissionRulesT)theEObject;
				T result = caseAccessPermissionRulesT(accessPermissionRulesT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.ACCESS_PERMISSION_RULE_T: {
				AccessPermissionRuleT accessPermissionRuleT = (AccessPermissionRuleT)theEObject;
				T result = caseAccessPermissionRuleT(accessPermissionRuleT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.BLOB_CERTIFICATE_T: {
				BlobCertificateT blobCertificateT = (BlobCertificateT)theEObject;
				T result = caseBlobCertificateT(blobCertificateT);
				if (result == null) result = caseCertificateAbstractT(blobCertificateT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.CERTIFICATE_ABSTRACT_T: {
				CertificateAbstractT certificateAbstractT = (CertificateAbstractT)theEObject;
				T result = caseCertificateAbstractT(certificateAbstractT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.CERTIFICATES_T: {
				CertificatesT certificatesT = (CertificatesT)theEObject;
				T result = caseCertificatesT(certificatesT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.CERTIFICATE_T: {
				CertificateT certificateT = (CertificateT)theEObject;
				T result = caseCertificateT(certificateT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.CONTAINED_EXTENSIONS_T: {
				ContainedExtensionsT containedExtensionsT = (ContainedExtensionsT)theEObject;
				T result = caseContainedExtensionsT(containedExtensionsT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.INTERNAL_INFORMATION_POINTS: {
				InternalInformationPoints internalInformationPoints = (InternalInformationPoints)theEObject;
				T result = caseInternalInformationPoints(internalInformationPoints);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.OBJECT_ATTRIBUTES_T: {
				ObjectAttributesT objectAttributesT = (ObjectAttributesT)theEObject;
				T result = caseObjectAttributesT(objectAttributesT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.PERMISSION_PER_OBJECT_T: {
				PermissionPerObjectT permissionPerObjectT = (PermissionPerObjectT)theEObject;
				T result = casePermissionPerObjectT(permissionPerObjectT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.PERMISSIONS_T: {
				PermissionsT permissionsT = (PermissionsT)theEObject;
				T result = casePermissionsT(permissionsT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.POLICY_ADMINISTRATION_POINT_T: {
				PolicyAdministrationPointT policyAdministrationPointT = (PolicyAdministrationPointT)theEObject;
				T result = casePolicyAdministrationPointT(policyAdministrationPointT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.POLICY_DECISION_POINT_T: {
				PolicyDecisionPointT policyDecisionPointT = (PolicyDecisionPointT)theEObject;
				T result = casePolicyDecisionPointT(policyDecisionPointT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.POLICY_ENFORCEMENT_POINT_T: {
				PolicyEnforcementPointT policyEnforcementPointT = (PolicyEnforcementPointT)theEObject;
				T result = casePolicyEnforcementPointT(policyEnforcementPointT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.POLICY_INFORMATION_POINTS_T: {
				PolicyInformationPointsT policyInformationPointsT = (PolicyInformationPointsT)theEObject;
				T result = casePolicyInformationPointsT(policyInformationPointsT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.SECURITY_T: {
				SecurityT securityT = (SecurityT)theEObject;
				T result = caseSecurityT(securityT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.SUBJECT_ATTRIBUTES_T: {
				SubjectAttributesT subjectAttributesT = (SubjectAttributesT)theEObject;
				T result = caseSubjectAttributesT(subjectAttributesT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.DOCUMENT_ROOT: {
				DocumentRoot documentRoot = (DocumentRoot)theEObject;
				T result = caseDocumentRoot(documentRoot);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Access Control Policy Points T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Access Control Policy Points T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAccessControlPolicyPointsT(AccessControlPolicyPointsT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Access Control T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Access Control T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAccessControlT(AccessControlT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Access Permission Rules T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Access Permission Rules T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAccessPermissionRulesT(AccessPermissionRulesT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Access Permission Rule T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Access Permission Rule T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAccessPermissionRuleT(AccessPermissionRuleT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Blob Certificate T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Blob Certificate T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlobCertificateT(BlobCertificateT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Certificate Abstract T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Certificate Abstract T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCertificateAbstractT(CertificateAbstractT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Certificates T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Certificates T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCertificatesT(CertificatesT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Certificate T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Certificate T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCertificateT(CertificateT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Contained Extensions T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Contained Extensions T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContainedExtensionsT(ContainedExtensionsT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Internal Information Points</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Internal Information Points</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInternalInformationPoints(InternalInformationPoints object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Object Attributes T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Object Attributes T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseObjectAttributesT(ObjectAttributesT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Permission Per Object T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Permission Per Object T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePermissionPerObjectT(PermissionPerObjectT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Permissions T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Permissions T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePermissionsT(PermissionsT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Policy Administration Point T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Policy Administration Point T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePolicyAdministrationPointT(PolicyAdministrationPointT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Policy Decision Point T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Policy Decision Point T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePolicyDecisionPointT(PolicyDecisionPointT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Policy Enforcement Point T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Policy Enforcement Point T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePolicyEnforcementPointT(PolicyEnforcementPointT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Policy Information Points T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Policy Information Points T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePolicyInformationPointsT(PolicyInformationPointsT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Security T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Security T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSecurityT(SecurityT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Subject Attributes T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Subject Attributes T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubjectAttributesT(SubjectAttributesT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Document Root</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Document Root</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDocumentRoot(DocumentRoot object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //_0Switch
