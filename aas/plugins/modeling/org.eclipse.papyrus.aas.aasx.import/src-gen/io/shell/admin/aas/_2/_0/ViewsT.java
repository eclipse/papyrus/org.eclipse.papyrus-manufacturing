/**
 */
package io.shell.admin.aas._2._0;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Views T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.ViewsT#getView <em>View</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getViewsT()
 * @model extendedMetaData="name='views_t' kind='elementOnly'"
 * @generated
 */
public interface ViewsT extends EObject {
	/**
	 * Returns the value of the '<em><b>View</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas._2._0.ViewT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>View</em>' containment reference list.
	 * @see io.shell.admin.aas._2._0._0Package#getViewsT_View()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='view' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ViewT> getView();

} // ViewsT
