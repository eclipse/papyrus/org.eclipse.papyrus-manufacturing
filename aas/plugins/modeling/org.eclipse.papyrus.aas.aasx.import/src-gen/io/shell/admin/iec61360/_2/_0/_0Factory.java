/**
 */
package io.shell.admin.iec61360._2._0;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see io.shell.admin.iec61360._2._0._0Package
 * @generated
 */
public interface _0Factory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	_0Factory eINSTANCE = io.shell.admin.iec61360._2._0.impl._0FactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Code T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Code T</em>'.
	 * @generated
	 */
	CodeT createCodeT();

	/**
	 * Returns a new object of class '<em>Data Specification IEC61630T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Specification IEC61630T</em>'.
	 * @generated
	 */
	DataSpecificationIEC61630T createDataSpecificationIEC61630T();

	/**
	 * Returns a new object of class '<em>Keys T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Keys T</em>'.
	 * @generated
	 */
	KeysT createKeysT();

	/**
	 * Returns a new object of class '<em>Key T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Key T</em>'.
	 * @generated
	 */
	KeyT createKeyT();

	/**
	 * Returns a new object of class '<em>Lang String Set T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Lang String Set T</em>'.
	 * @generated
	 */
	LangStringSetT createLangStringSetT();

	/**
	 * Returns a new object of class '<em>Lang String T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Lang String T</em>'.
	 * @generated
	 */
	LangStringT createLangStringT();

	/**
	 * Returns a new object of class '<em>Reference T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reference T</em>'.
	 * @generated
	 */
	ReferenceT createReferenceT();

	/**
	 * Returns a new object of class '<em>Value Data Type T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Value Data Type T</em>'.
	 * @generated
	 */
	ValueDataTypeT createValueDataTypeT();

	/**
	 * Returns a new object of class '<em>Value List T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Value List T</em>'.
	 * @generated
	 */
	ValueListT createValueListT();

	/**
	 * Returns a new object of class '<em>Value Reference Pair T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Value Reference Pair T</em>'.
	 * @generated
	 */
	ValueReferencePairT createValueReferencePairT();

	/**
	 * Returns a new object of class '<em>Document Root</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Document Root</em>'.
	 * @generated
	 */
	DocumentRoot createDocumentRoot();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	_0Package get_0Package();

} //_0Factory
