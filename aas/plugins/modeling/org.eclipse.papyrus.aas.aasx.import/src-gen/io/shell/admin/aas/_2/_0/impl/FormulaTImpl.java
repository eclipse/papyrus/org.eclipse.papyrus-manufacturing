/**
 */
package io.shell.admin.aas._2._0.impl;

import io.shell.admin.aas._2._0.FormulaT;
import io.shell.admin.aas._2._0.ReferencesT;
import io.shell.admin.aas._2._0._0Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Formula T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.impl.FormulaTImpl#getDependsOnRefs <em>Depends On Refs</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FormulaTImpl extends MinimalEObjectImpl.Container implements FormulaT {
	/**
	 * The cached value of the '{@link #getDependsOnRefs() <em>Depends On Refs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependsOnRefs()
	 * @generated
	 * @ordered
	 */
	protected ReferencesT dependsOnRefs;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FormulaTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.FORMULA_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferencesT getDependsOnRefs() {
		return dependsOnRefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDependsOnRefs(ReferencesT newDependsOnRefs, NotificationChain msgs) {
		ReferencesT oldDependsOnRefs = dependsOnRefs;
		dependsOnRefs = newDependsOnRefs;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.FORMULA_T__DEPENDS_ON_REFS, oldDependsOnRefs, newDependsOnRefs);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDependsOnRefs(ReferencesT newDependsOnRefs) {
		if (newDependsOnRefs != dependsOnRefs) {
			NotificationChain msgs = null;
			if (dependsOnRefs != null)
				msgs = ((InternalEObject)dependsOnRefs).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.FORMULA_T__DEPENDS_ON_REFS, null, msgs);
			if (newDependsOnRefs != null)
				msgs = ((InternalEObject)newDependsOnRefs).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.FORMULA_T__DEPENDS_ON_REFS, null, msgs);
			msgs = basicSetDependsOnRefs(newDependsOnRefs, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.FORMULA_T__DEPENDS_ON_REFS, newDependsOnRefs, newDependsOnRefs));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.FORMULA_T__DEPENDS_ON_REFS:
				return basicSetDependsOnRefs(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.FORMULA_T__DEPENDS_ON_REFS:
				return getDependsOnRefs();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.FORMULA_T__DEPENDS_ON_REFS:
				setDependsOnRefs((ReferencesT)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.FORMULA_T__DEPENDS_ON_REFS:
				setDependsOnRefs((ReferencesT)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.FORMULA_T__DEPENDS_ON_REFS:
				return dependsOnRefs != null;
		}
		return super.eIsSet(featureID);
	}

} //FormulaTImpl
