/**
 */
package io.shell.admin.aas._2._0;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>References T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.ReferencesT#getReference <em>Reference</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getReferencesT()
 * @model extendedMetaData="name='references_t' kind='elementOnly'"
 * @generated
 */
public interface ReferencesT extends EObject {
	/**
	 * Returns the value of the '<em><b>Reference</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas._2._0.ReferenceT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference</em>' containment reference list.
	 * @see io.shell.admin.aas._2._0._0Package#getReferencesT_Reference()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='reference' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ReferenceT> getReference();

} // ReferencesT
