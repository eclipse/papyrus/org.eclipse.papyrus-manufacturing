/**
 */
package io.shell.admin.aas.abac._2._0;

import io.shell.admin.aas._2._0.SubmodelRefsT;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Internal Information Points</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.InternalInformationPoints#getInternalInformationPoint <em>Internal Information Point</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas.abac._2._0._0Package#getInternalInformationPoints()
 * @model extendedMetaData="name='internalInformationPoints' kind='elementOnly'"
 * @generated
 */
public interface InternalInformationPoints extends EObject {
	/**
	 * Returns the value of the '<em><b>Internal Information Point</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas._2._0.SubmodelRefsT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Internal Information Point</em>' containment reference list.
	 * @see io.shell.admin.aas.abac._2._0._0Package#getInternalInformationPoints_InternalInformationPoint()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='internalInformationPoint' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<SubmodelRefsT> getInternalInformationPoint();

} // InternalInformationPoints
