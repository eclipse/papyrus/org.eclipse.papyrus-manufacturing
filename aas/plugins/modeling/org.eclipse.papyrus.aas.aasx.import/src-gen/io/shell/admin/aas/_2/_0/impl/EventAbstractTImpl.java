/**
 */
package io.shell.admin.aas._2._0.impl;

import io.shell.admin.aas._2._0.EventAbstractT;
import io.shell.admin.aas._2._0._0Package;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event Abstract T</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EventAbstractTImpl extends SubmodelElementAbstractTImpl implements EventAbstractT {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EventAbstractTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.EVENT_ABSTRACT_T;
	}

} //EventAbstractTImpl
