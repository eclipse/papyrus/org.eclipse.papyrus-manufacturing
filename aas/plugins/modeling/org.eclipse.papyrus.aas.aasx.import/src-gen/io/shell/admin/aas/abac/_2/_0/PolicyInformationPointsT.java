/**
 */
package io.shell.admin.aas.abac._2._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Policy Information Points T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.PolicyInformationPointsT#isExternalInformationPoints <em>External Information Points</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.PolicyInformationPointsT#getInternalInformationPoints <em>Internal Information Points</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas.abac._2._0._0Package#getPolicyInformationPointsT()
 * @model extendedMetaData="name='policyInformationPoints_t' kind='elementOnly'"
 * @generated
 */
public interface PolicyInformationPointsT extends EObject {
	/**
	 * Returns the value of the '<em><b>External Information Points</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Information Points</em>' attribute.
	 * @see #isSetExternalInformationPoints()
	 * @see #unsetExternalInformationPoints()
	 * @see #setExternalInformationPoints(boolean)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getPolicyInformationPointsT_ExternalInformationPoints()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean" required="true"
	 *        extendedMetaData="kind='element' name='externalInformationPoints' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isExternalInformationPoints();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.PolicyInformationPointsT#isExternalInformationPoints <em>External Information Points</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>External Information Points</em>' attribute.
	 * @see #isSetExternalInformationPoints()
	 * @see #unsetExternalInformationPoints()
	 * @see #isExternalInformationPoints()
	 * @generated
	 */
	void setExternalInformationPoints(boolean value);

	/**
	 * Unsets the value of the '{@link io.shell.admin.aas.abac._2._0.PolicyInformationPointsT#isExternalInformationPoints <em>External Information Points</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExternalInformationPoints()
	 * @see #isExternalInformationPoints()
	 * @see #setExternalInformationPoints(boolean)
	 * @generated
	 */
	void unsetExternalInformationPoints();

	/**
	 * Returns whether the value of the '{@link io.shell.admin.aas.abac._2._0.PolicyInformationPointsT#isExternalInformationPoints <em>External Information Points</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>External Information Points</em>' attribute is set.
	 * @see #unsetExternalInformationPoints()
	 * @see #isExternalInformationPoints()
	 * @see #setExternalInformationPoints(boolean)
	 * @generated
	 */
	boolean isSetExternalInformationPoints();

	/**
	 * Returns the value of the '<em><b>Internal Information Points</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Internal Information Points</em>' containment reference.
	 * @see #setInternalInformationPoints(InternalInformationPoints)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getPolicyInformationPointsT_InternalInformationPoints()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='internalInformationPoints' namespace='##targetNamespace'"
	 * @generated
	 */
	InternalInformationPoints getInternalInformationPoints();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.PolicyInformationPointsT#getInternalInformationPoints <em>Internal Information Points</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Internal Information Points</em>' containment reference.
	 * @see #getInternalInformationPoints()
	 * @generated
	 */
	void setInternalInformationPoints(InternalInformationPoints value);

} // PolicyInformationPointsT
