/**
 */
package io.shell.admin.iec61360._2._0.impl;

import io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T;
import io.shell.admin.iec61360._2._0.DataTypeIEC61360T;
import io.shell.admin.iec61360._2._0.LangStringSetT;
import io.shell.admin.iec61360._2._0.LevelTypeT;
import io.shell.admin.iec61360._2._0.ReferenceT;
import io.shell.admin.iec61360._2._0.ValueDataTypeT;
import io.shell.admin.iec61360._2._0.ValueListT;
import io.shell.admin.iec61360._2._0._0Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Specification IEC61630T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.iec61360._2._0.impl.DataSpecificationIEC61630TImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.impl.DataSpecificationIEC61630TImpl#getPreferredName <em>Preferred Name</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.impl.DataSpecificationIEC61630TImpl#getShortName <em>Short Name</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.impl.DataSpecificationIEC61630TImpl#getUnit <em>Unit</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.impl.DataSpecificationIEC61630TImpl#getUnitId <em>Unit Id</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.impl.DataSpecificationIEC61630TImpl#getSourceOfDefinition <em>Source Of Definition</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.impl.DataSpecificationIEC61630TImpl#getSymbol <em>Symbol</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.impl.DataSpecificationIEC61630TImpl#getDataType <em>Data Type</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.impl.DataSpecificationIEC61630TImpl#getDefinition <em>Definition</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.impl.DataSpecificationIEC61630TImpl#getValueFormat <em>Value Format</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.impl.DataSpecificationIEC61630TImpl#getValueList <em>Value List</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.impl.DataSpecificationIEC61630TImpl#getValue <em>Value</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.impl.DataSpecificationIEC61630TImpl#getValueId <em>Value Id</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.impl.DataSpecificationIEC61630TImpl#getLevelType <em>Level Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataSpecificationIEC61630TImpl extends MinimalEObjectImpl.Container implements DataSpecificationIEC61630T {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataSpecificationIEC61630TImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.DATA_SPECIFICATION_IEC61630T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, _0Package.DATA_SPECIFICATION_IEC61630T__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LangStringSetT> getPreferredName() {
		return getGroup().list(_0Package.Literals.DATA_SPECIFICATION_IEC61630T__PREFERRED_NAME);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LangStringSetT> getShortName() {
		return getGroup().list(_0Package.Literals.DATA_SPECIFICATION_IEC61630T__SHORT_NAME);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getUnit() {
		return getGroup().list(_0Package.Literals.DATA_SPECIFICATION_IEC61630T__UNIT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReferenceT> getUnitId() {
		return getGroup().list(_0Package.Literals.DATA_SPECIFICATION_IEC61630T__UNIT_ID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getSourceOfDefinition() {
		return getGroup().list(_0Package.Literals.DATA_SPECIFICATION_IEC61630T__SOURCE_OF_DEFINITION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getSymbol() {
		return getGroup().list(_0Package.Literals.DATA_SPECIFICATION_IEC61630T__SYMBOL);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataTypeIEC61360T> getDataType() {
		return getGroup().list(_0Package.Literals.DATA_SPECIFICATION_IEC61630T__DATA_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LangStringSetT> getDefinition() {
		return getGroup().list(_0Package.Literals.DATA_SPECIFICATION_IEC61630T__DEFINITION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getValueFormat() {
		return getGroup().list(_0Package.Literals.DATA_SPECIFICATION_IEC61630T__VALUE_FORMAT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ValueListT> getValueList() {
		return getGroup().list(_0Package.Literals.DATA_SPECIFICATION_IEC61630T__VALUE_LIST);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ValueDataTypeT> getValue() {
		return getGroup().list(_0Package.Literals.DATA_SPECIFICATION_IEC61630T__VALUE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReferenceT> getValueId() {
		return getGroup().list(_0Package.Literals.DATA_SPECIFICATION_IEC61630T__VALUE_ID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LevelTypeT> getLevelType() {
		return getGroup().list(_0Package.Literals.DATA_SPECIFICATION_IEC61630T__LEVEL_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.DATA_SPECIFICATION_IEC61630T__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case _0Package.DATA_SPECIFICATION_IEC61630T__PREFERRED_NAME:
				return ((InternalEList<?>)getPreferredName()).basicRemove(otherEnd, msgs);
			case _0Package.DATA_SPECIFICATION_IEC61630T__SHORT_NAME:
				return ((InternalEList<?>)getShortName()).basicRemove(otherEnd, msgs);
			case _0Package.DATA_SPECIFICATION_IEC61630T__UNIT_ID:
				return ((InternalEList<?>)getUnitId()).basicRemove(otherEnd, msgs);
			case _0Package.DATA_SPECIFICATION_IEC61630T__DEFINITION:
				return ((InternalEList<?>)getDefinition()).basicRemove(otherEnd, msgs);
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_LIST:
				return ((InternalEList<?>)getValueList()).basicRemove(otherEnd, msgs);
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE:
				return ((InternalEList<?>)getValue()).basicRemove(otherEnd, msgs);
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_ID:
				return ((InternalEList<?>)getValueId()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.DATA_SPECIFICATION_IEC61630T__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case _0Package.DATA_SPECIFICATION_IEC61630T__PREFERRED_NAME:
				return getPreferredName();
			case _0Package.DATA_SPECIFICATION_IEC61630T__SHORT_NAME:
				return getShortName();
			case _0Package.DATA_SPECIFICATION_IEC61630T__UNIT:
				return getUnit();
			case _0Package.DATA_SPECIFICATION_IEC61630T__UNIT_ID:
				return getUnitId();
			case _0Package.DATA_SPECIFICATION_IEC61630T__SOURCE_OF_DEFINITION:
				return getSourceOfDefinition();
			case _0Package.DATA_SPECIFICATION_IEC61630T__SYMBOL:
				return getSymbol();
			case _0Package.DATA_SPECIFICATION_IEC61630T__DATA_TYPE:
				return getDataType();
			case _0Package.DATA_SPECIFICATION_IEC61630T__DEFINITION:
				return getDefinition();
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_FORMAT:
				return getValueFormat();
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_LIST:
				return getValueList();
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE:
				return getValue();
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_ID:
				return getValueId();
			case _0Package.DATA_SPECIFICATION_IEC61630T__LEVEL_TYPE:
				return getLevelType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.DATA_SPECIFICATION_IEC61630T__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__PREFERRED_NAME:
				getPreferredName().clear();
				getPreferredName().addAll((Collection<? extends LangStringSetT>)newValue);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__SHORT_NAME:
				getShortName().clear();
				getShortName().addAll((Collection<? extends LangStringSetT>)newValue);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__UNIT:
				getUnit().clear();
				getUnit().addAll((Collection<? extends String>)newValue);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__UNIT_ID:
				getUnitId().clear();
				getUnitId().addAll((Collection<? extends ReferenceT>)newValue);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__SOURCE_OF_DEFINITION:
				getSourceOfDefinition().clear();
				getSourceOfDefinition().addAll((Collection<? extends String>)newValue);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__SYMBOL:
				getSymbol().clear();
				getSymbol().addAll((Collection<? extends String>)newValue);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__DATA_TYPE:
				getDataType().clear();
				getDataType().addAll((Collection<? extends DataTypeIEC61360T>)newValue);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__DEFINITION:
				getDefinition().clear();
				getDefinition().addAll((Collection<? extends LangStringSetT>)newValue);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_FORMAT:
				getValueFormat().clear();
				getValueFormat().addAll((Collection<? extends String>)newValue);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_LIST:
				getValueList().clear();
				getValueList().addAll((Collection<? extends ValueListT>)newValue);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE:
				getValue().clear();
				getValue().addAll((Collection<? extends ValueDataTypeT>)newValue);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_ID:
				getValueId().clear();
				getValueId().addAll((Collection<? extends ReferenceT>)newValue);
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__LEVEL_TYPE:
				getLevelType().clear();
				getLevelType().addAll((Collection<? extends LevelTypeT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.DATA_SPECIFICATION_IEC61630T__GROUP:
				getGroup().clear();
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__PREFERRED_NAME:
				getPreferredName().clear();
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__SHORT_NAME:
				getShortName().clear();
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__UNIT:
				getUnit().clear();
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__UNIT_ID:
				getUnitId().clear();
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__SOURCE_OF_DEFINITION:
				getSourceOfDefinition().clear();
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__SYMBOL:
				getSymbol().clear();
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__DATA_TYPE:
				getDataType().clear();
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__DEFINITION:
				getDefinition().clear();
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_FORMAT:
				getValueFormat().clear();
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_LIST:
				getValueList().clear();
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE:
				getValue().clear();
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_ID:
				getValueId().clear();
				return;
			case _0Package.DATA_SPECIFICATION_IEC61630T__LEVEL_TYPE:
				getLevelType().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.DATA_SPECIFICATION_IEC61630T__GROUP:
				return group != null && !group.isEmpty();
			case _0Package.DATA_SPECIFICATION_IEC61630T__PREFERRED_NAME:
				return !getPreferredName().isEmpty();
			case _0Package.DATA_SPECIFICATION_IEC61630T__SHORT_NAME:
				return !getShortName().isEmpty();
			case _0Package.DATA_SPECIFICATION_IEC61630T__UNIT:
				return !getUnit().isEmpty();
			case _0Package.DATA_SPECIFICATION_IEC61630T__UNIT_ID:
				return !getUnitId().isEmpty();
			case _0Package.DATA_SPECIFICATION_IEC61630T__SOURCE_OF_DEFINITION:
				return !getSourceOfDefinition().isEmpty();
			case _0Package.DATA_SPECIFICATION_IEC61630T__SYMBOL:
				return !getSymbol().isEmpty();
			case _0Package.DATA_SPECIFICATION_IEC61630T__DATA_TYPE:
				return !getDataType().isEmpty();
			case _0Package.DATA_SPECIFICATION_IEC61630T__DEFINITION:
				return !getDefinition().isEmpty();
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_FORMAT:
				return !getValueFormat().isEmpty();
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_LIST:
				return !getValueList().isEmpty();
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE:
				return !getValue().isEmpty();
			case _0Package.DATA_SPECIFICATION_IEC61630T__VALUE_ID:
				return !getValueId().isEmpty();
			case _0Package.DATA_SPECIFICATION_IEC61630T__LEVEL_TYPE:
				return !getLevelType().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(')');
		return result.toString();
	}

} //DataSpecificationIEC61630TImpl
