/**
 */
package io.shell.admin.aas._2._0;

import io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Specification Content T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.DataSpecificationContentT#getDataSpecificationIEC61360 <em>Data Specification IEC61360</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getDataSpecificationContentT()
 * @model extendedMetaData="name='dataSpecificationContent_t' kind='elementOnly'"
 * @generated
 */
public interface DataSpecificationContentT extends EObject {
	/**
	 * Returns the value of the '<em><b>Data Specification IEC61360</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Specification IEC61360</em>' containment reference.
	 * @see #setDataSpecificationIEC61360(DataSpecificationIEC61630T)
	 * @see io.shell.admin.aas._2._0._0Package#getDataSpecificationContentT_DataSpecificationIEC61360()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='dataSpecificationIEC61360' namespace='##targetNamespace'"
	 * @generated
	 */
	DataSpecificationIEC61630T getDataSpecificationIEC61360();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.DataSpecificationContentT#getDataSpecificationIEC61360 <em>Data Specification IEC61360</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Specification IEC61360</em>' containment reference.
	 * @see #getDataSpecificationIEC61360()
	 * @generated
	 */
	void setDataSpecificationIEC61360(DataSpecificationIEC61630T value);

} // DataSpecificationContentT
