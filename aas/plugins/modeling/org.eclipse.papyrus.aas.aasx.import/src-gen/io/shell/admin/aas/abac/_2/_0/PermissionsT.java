/**
 */
package io.shell.admin.aas.abac._2._0;

import io.shell.admin.aas._2._0.PropertyT;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Permissions T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.PermissionsT#getPermission <em>Permission</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.PermissionsT#getKindOfPermission <em>Kind Of Permission</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas.abac._2._0._0Package#getPermissionsT()
 * @model extendedMetaData="name='permissions_t' kind='elementOnly'"
 * @generated
 */
public interface PermissionsT extends EObject {
	/**
	 * Returns the value of the '<em><b>Permission</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Permission</em>' containment reference.
	 * @see #setPermission(PropertyT)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getPermissionsT_Permission()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='permission' namespace='##targetNamespace'"
	 * @generated
	 */
	PropertyT getPermission();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.PermissionsT#getPermission <em>Permission</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Permission</em>' containment reference.
	 * @see #getPermission()
	 * @generated
	 */
	void setPermission(PropertyT value);

	/**
	 * Returns the value of the '<em><b>Kind Of Permission</b></em>' attribute.
	 * The literals are from the enumeration {@link io.shell.admin.aas.abac._2._0.PermissionKind}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind Of Permission</em>' attribute.
	 * @see io.shell.admin.aas.abac._2._0.PermissionKind
	 * @see #isSetKindOfPermission()
	 * @see #unsetKindOfPermission()
	 * @see #setKindOfPermission(PermissionKind)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getPermissionsT_KindOfPermission()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='element' name='kindOfPermission' namespace='##targetNamespace'"
	 * @generated
	 */
	PermissionKind getKindOfPermission();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.PermissionsT#getKindOfPermission <em>Kind Of Permission</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind Of Permission</em>' attribute.
	 * @see io.shell.admin.aas.abac._2._0.PermissionKind
	 * @see #isSetKindOfPermission()
	 * @see #unsetKindOfPermission()
	 * @see #getKindOfPermission()
	 * @generated
	 */
	void setKindOfPermission(PermissionKind value);

	/**
	 * Unsets the value of the '{@link io.shell.admin.aas.abac._2._0.PermissionsT#getKindOfPermission <em>Kind Of Permission</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetKindOfPermission()
	 * @see #getKindOfPermission()
	 * @see #setKindOfPermission(PermissionKind)
	 * @generated
	 */
	void unsetKindOfPermission();

	/**
	 * Returns whether the value of the '{@link io.shell.admin.aas.abac._2._0.PermissionsT#getKindOfPermission <em>Kind Of Permission</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Kind Of Permission</em>' attribute is set.
	 * @see #unsetKindOfPermission()
	 * @see #getKindOfPermission()
	 * @see #setKindOfPermission(PermissionKind)
	 * @generated
	 */
	boolean isSetKindOfPermission();

} // PermissionsT
