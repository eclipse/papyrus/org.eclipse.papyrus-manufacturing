/**
 */
package io.shell.admin.aas.abac._2._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Certificate T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.CertificateT#getBlobCertificate <em>Blob Certificate</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas.abac._2._0._0Package#getCertificateT()
 * @model extendedMetaData="name='certificate_t' kind='elementOnly'"
 * @generated
 */
public interface CertificateT extends EObject {
	/**
	 * Returns the value of the '<em><b>Blob Certificate</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Blob Certificate</em>' containment reference.
	 * @see #setBlobCertificate(BlobCertificateT)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getCertificateT_BlobCertificate()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='blobCertificate' namespace='##targetNamespace'"
	 * @generated
	 */
	BlobCertificateT getBlobCertificate();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.CertificateT#getBlobCertificate <em>Blob Certificate</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Blob Certificate</em>' containment reference.
	 * @see #getBlobCertificate()
	 * @generated
	 */
	void setBlobCertificate(BlobCertificateT value);

} // CertificateT
