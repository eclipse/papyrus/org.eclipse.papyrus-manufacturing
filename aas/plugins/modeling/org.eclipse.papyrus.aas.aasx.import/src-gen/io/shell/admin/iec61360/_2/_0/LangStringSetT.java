/**
 */
package io.shell.admin.iec61360._2._0;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lang String Set T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.iec61360._2._0.LangStringSetT#getLangString <em>Lang String</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.iec61360._2._0._0Package#getLangStringSetT()
 * @model extendedMetaData="name='langStringSet_t' kind='elementOnly'"
 * @generated
 */
public interface LangStringSetT extends EObject {
	/**
	 * Returns the value of the '<em><b>Lang String</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.iec61360._2._0.LangStringT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lang String</em>' containment reference list.
	 * @see io.shell.admin.iec61360._2._0._0Package#getLangStringSetT_LangString()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='langString' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<LangStringT> getLangString();

} // LangStringSetT
