/**
 */
package io.shell.admin.iec61360._2._0.impl;

import io.shell.admin.iec61360._2._0.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class _0FactoryImpl extends EFactoryImpl implements _0Factory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static _0Factory init() {
		try {
			_0Factory the_0Factory = (_0Factory)EPackage.Registry.INSTANCE.getEFactory(_0Package.eNS_URI);
			if (the_0Factory != null) {
				return the_0Factory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new _0FactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public _0FactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case _0Package.CODE_T: return createCodeT();
			case _0Package.DATA_SPECIFICATION_IEC61630T: return createDataSpecificationIEC61630T();
			case _0Package.KEYS_T: return createKeysT();
			case _0Package.KEY_T: return createKeyT();
			case _0Package.LANG_STRING_SET_T: return createLangStringSetT();
			case _0Package.LANG_STRING_T: return createLangStringT();
			case _0Package.REFERENCE_T: return createReferenceT();
			case _0Package.VALUE_DATA_TYPE_T: return createValueDataTypeT();
			case _0Package.VALUE_LIST_T: return createValueListT();
			case _0Package.VALUE_REFERENCE_PAIR_T: return createValueReferencePairT();
			case _0Package.DOCUMENT_ROOT: return createDocumentRoot();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case _0Package.DATA_TYPE_IEC61360T:
				return createDataTypeIEC61360TFromString(eDataType, initialValue);
			case _0Package.ID_TYPE_TYPE:
				return createIdTypeTypeFromString(eDataType, initialValue);
			case _0Package.LEVEL_TYPE_T:
				return createLevelTypeTFromString(eDataType, initialValue);
			case _0Package.TYPE_TYPE:
				return createTypeTypeFromString(eDataType, initialValue);
			case _0Package.DATA_TYPE_IEC61360T_OBJECT:
				return createDataTypeIEC61360TObjectFromString(eDataType, initialValue);
			case _0Package.ID_TYPE_TYPE_OBJECT:
				return createIdTypeTypeObjectFromString(eDataType, initialValue);
			case _0Package.LEVEL_TYPE_TOBJECT:
				return createLevelTypeTObjectFromString(eDataType, initialValue);
			case _0Package.TYPE_TYPE_OBJECT:
				return createTypeTypeObjectFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case _0Package.DATA_TYPE_IEC61360T:
				return convertDataTypeIEC61360TToString(eDataType, instanceValue);
			case _0Package.ID_TYPE_TYPE:
				return convertIdTypeTypeToString(eDataType, instanceValue);
			case _0Package.LEVEL_TYPE_T:
				return convertLevelTypeTToString(eDataType, instanceValue);
			case _0Package.TYPE_TYPE:
				return convertTypeTypeToString(eDataType, instanceValue);
			case _0Package.DATA_TYPE_IEC61360T_OBJECT:
				return convertDataTypeIEC61360TObjectToString(eDataType, instanceValue);
			case _0Package.ID_TYPE_TYPE_OBJECT:
				return convertIdTypeTypeObjectToString(eDataType, instanceValue);
			case _0Package.LEVEL_TYPE_TOBJECT:
				return convertLevelTypeTObjectToString(eDataType, instanceValue);
			case _0Package.TYPE_TYPE_OBJECT:
				return convertTypeTypeObjectToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeT createCodeT() {
		CodeTImpl codeT = new CodeTImpl();
		return codeT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataSpecificationIEC61630T createDataSpecificationIEC61630T() {
		DataSpecificationIEC61630TImpl dataSpecificationIEC61630T = new DataSpecificationIEC61630TImpl();
		return dataSpecificationIEC61630T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public KeysT createKeysT() {
		KeysTImpl keysT = new KeysTImpl();
		return keysT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public KeyT createKeyT() {
		KeyTImpl keyT = new KeyTImpl();
		return keyT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LangStringSetT createLangStringSetT() {
		LangStringSetTImpl langStringSetT = new LangStringSetTImpl();
		return langStringSetT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LangStringT createLangStringT() {
		LangStringTImpl langStringT = new LangStringTImpl();
		return langStringT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceT createReferenceT() {
		ReferenceTImpl referenceT = new ReferenceTImpl();
		return referenceT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueDataTypeT createValueDataTypeT() {
		ValueDataTypeTImpl valueDataTypeT = new ValueDataTypeTImpl();
		return valueDataTypeT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueListT createValueListT() {
		ValueListTImpl valueListT = new ValueListTImpl();
		return valueListT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueReferencePairT createValueReferencePairT() {
		ValueReferencePairTImpl valueReferencePairT = new ValueReferencePairTImpl();
		return valueReferencePairT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DocumentRoot createDocumentRoot() {
		DocumentRootImpl documentRoot = new DocumentRootImpl();
		return documentRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataTypeIEC61360T createDataTypeIEC61360TFromString(EDataType eDataType, String initialValue) {
		DataTypeIEC61360T result = DataTypeIEC61360T.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDataTypeIEC61360TToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdTypeType createIdTypeTypeFromString(EDataType eDataType, String initialValue) {
		IdTypeType result = IdTypeType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIdTypeTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LevelTypeT createLevelTypeTFromString(EDataType eDataType, String initialValue) {
		LevelTypeT result = LevelTypeT.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLevelTypeTToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeType createTypeTypeFromString(EDataType eDataType, String initialValue) {
		TypeType result = TypeType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTypeTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataTypeIEC61360T createDataTypeIEC61360TObjectFromString(EDataType eDataType, String initialValue) {
		return createDataTypeIEC61360TFromString(_0Package.Literals.DATA_TYPE_IEC61360T, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDataTypeIEC61360TObjectToString(EDataType eDataType, Object instanceValue) {
		return convertDataTypeIEC61360TToString(_0Package.Literals.DATA_TYPE_IEC61360T, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdTypeType createIdTypeTypeObjectFromString(EDataType eDataType, String initialValue) {
		return createIdTypeTypeFromString(_0Package.Literals.ID_TYPE_TYPE, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIdTypeTypeObjectToString(EDataType eDataType, Object instanceValue) {
		return convertIdTypeTypeToString(_0Package.Literals.ID_TYPE_TYPE, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LevelTypeT createLevelTypeTObjectFromString(EDataType eDataType, String initialValue) {
		return createLevelTypeTFromString(_0Package.Literals.LEVEL_TYPE_T, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLevelTypeTObjectToString(EDataType eDataType, Object instanceValue) {
		return convertLevelTypeTToString(_0Package.Literals.LEVEL_TYPE_T, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeType createTypeTypeObjectFromString(EDataType eDataType, String initialValue) {
		return createTypeTypeFromString(_0Package.Literals.TYPE_TYPE, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTypeTypeObjectToString(EDataType eDataType, Object instanceValue) {
		return convertTypeTypeToString(_0Package.Literals.TYPE_TYPE, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public _0Package get_0Package() {
		return (_0Package)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static _0Package getPackage() {
		return _0Package.eINSTANCE;
	}

} //_0FactoryImpl
