/**
 */
package io.shell.admin.aas._2._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constraint T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.ConstraintT#getFormula <em>Formula</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.ConstraintT#getQualifier <em>Qualifier</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getConstraintT()
 * @model extendedMetaData="name='constraint_t' kind='elementOnly'"
 * @generated
 */
public interface ConstraintT extends EObject {
	/**
	 * Returns the value of the '<em><b>Formula</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formula</em>' containment reference.
	 * @see #setFormula(FormulaT)
	 * @see io.shell.admin.aas._2._0._0Package#getConstraintT_Formula()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formula' namespace='##targetNamespace'"
	 * @generated
	 */
	FormulaT getFormula();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.ConstraintT#getFormula <em>Formula</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formula</em>' containment reference.
	 * @see #getFormula()
	 * @generated
	 */
	void setFormula(FormulaT value);

	/**
	 * Returns the value of the '<em><b>Qualifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Qualifier</em>' containment reference.
	 * @see #setQualifier(QualifierT)
	 * @see io.shell.admin.aas._2._0._0Package#getConstraintT_Qualifier()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='qualifier' namespace='##targetNamespace'"
	 * @generated
	 */
	QualifierT getQualifier();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.ConstraintT#getQualifier <em>Qualifier</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Qualifier</em>' containment reference.
	 * @see #getQualifier()
	 * @generated
	 */
	void setQualifier(QualifierT value);

} // ConstraintT
