/**
 */
package io.shell.admin.aas._2._0;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see io.shell.admin.aas._2._0._0Package
 * @generated
 */
public interface _0Factory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	_0Factory eINSTANCE = io.shell.admin.aas._2._0.impl._0FactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Aasenv T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aasenv T</em>'.
	 * @generated
	 */
	AasenvT createAasenvT();

	/**
	 * Returns a new object of class '<em>Administration T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Administration T</em>'.
	 * @generated
	 */
	AdministrationT createAdministrationT();

	/**
	 * Returns a new object of class '<em>Annotated Relationship Element T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Annotated Relationship Element T</em>'.
	 * @generated
	 */
	AnnotatedRelationshipElementT createAnnotatedRelationshipElementT();

	/**
	 * Returns a new object of class '<em>Asset Administration Shells T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Asset Administration Shells T</em>'.
	 * @generated
	 */
	AssetAdministrationShellsT createAssetAdministrationShellsT();

	/**
	 * Returns a new object of class '<em>Asset Administration Shell T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Asset Administration Shell T</em>'.
	 * @generated
	 */
	AssetAdministrationShellT createAssetAdministrationShellT();

	/**
	 * Returns a new object of class '<em>Assets T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Assets T</em>'.
	 * @generated
	 */
	AssetsT createAssetsT();

	/**
	 * Returns a new object of class '<em>Asset T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Asset T</em>'.
	 * @generated
	 */
	AssetT createAssetT();

	/**
	 * Returns a new object of class '<em>Basic Event T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Basic Event T</em>'.
	 * @generated
	 */
	BasicEventT createBasicEventT();

	/**
	 * Returns a new object of class '<em>Blob T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blob T</em>'.
	 * @generated
	 */
	BlobT createBlobT();

	/**
	 * Returns a new object of class '<em>Blob Type T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Blob Type T</em>'.
	 * @generated
	 */
	BlobTypeT createBlobTypeT();

	/**
	 * Returns a new object of class '<em>Concept Description Refs T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Concept Description Refs T</em>'.
	 * @generated
	 */
	ConceptDescriptionRefsT createConceptDescriptionRefsT();

	/**
	 * Returns a new object of class '<em>Concept Descriptions T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Concept Descriptions T</em>'.
	 * @generated
	 */
	ConceptDescriptionsT createConceptDescriptionsT();

	/**
	 * Returns a new object of class '<em>Concept Description T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Concept Description T</em>'.
	 * @generated
	 */
	ConceptDescriptionT createConceptDescriptionT();

	/**
	 * Returns a new object of class '<em>Concept Dictionaries T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Concept Dictionaries T</em>'.
	 * @generated
	 */
	ConceptDictionariesT createConceptDictionariesT();

	/**
	 * Returns a new object of class '<em>Concept Dictionary T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Concept Dictionary T</em>'.
	 * @generated
	 */
	ConceptDictionaryT createConceptDictionaryT();

	/**
	 * Returns a new object of class '<em>Constraint T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Constraint T</em>'.
	 * @generated
	 */
	ConstraintT createConstraintT();

	/**
	 * Returns a new object of class '<em>Contained Elements T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Contained Elements T</em>'.
	 * @generated
	 */
	ContainedElementsT createContainedElementsT();

	/**
	 * Returns a new object of class '<em>Data Element Abstract T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Element Abstract T</em>'.
	 * @generated
	 */
	DataElementAbstractT createDataElementAbstractT();

	/**
	 * Returns a new object of class '<em>Data Elements T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Elements T</em>'.
	 * @generated
	 */
	DataElementsT createDataElementsT();

	/**
	 * Returns a new object of class '<em>Data Specification Content T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Specification Content T</em>'.
	 * @generated
	 */
	DataSpecificationContentT createDataSpecificationContentT();

	/**
	 * Returns a new object of class '<em>Data Type Def T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Type Def T</em>'.
	 * @generated
	 */
	DataTypeDefT createDataTypeDefT();

	/**
	 * Returns a new object of class '<em>Document Root</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Document Root</em>'.
	 * @generated
	 */
	DocumentRoot createDocumentRoot();

	/**
	 * Returns a new object of class '<em>Embedded Data Specification T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Embedded Data Specification T</em>'.
	 * @generated
	 */
	EmbeddedDataSpecificationT createEmbeddedDataSpecificationT();

	/**
	 * Returns a new object of class '<em>Entity T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Entity T</em>'.
	 * @generated
	 */
	EntityT createEntityT();

	/**
	 * Returns a new object of class '<em>Event Abstract T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event Abstract T</em>'.
	 * @generated
	 */
	EventAbstractT createEventAbstractT();

	/**
	 * Returns a new object of class '<em>File T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>File T</em>'.
	 * @generated
	 */
	FileT createFileT();

	/**
	 * Returns a new object of class '<em>Formula T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formula T</em>'.
	 * @generated
	 */
	FormulaT createFormulaT();

	/**
	 * Returns a new object of class '<em>Identification T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Identification T</em>'.
	 * @generated
	 */
	IdentificationT createIdentificationT();

	/**
	 * Returns a new object of class '<em>Identifier T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Identifier T</em>'.
	 * @generated
	 */
	IdentifierT createIdentifierT();

	/**
	 * Returns a new object of class '<em>Id Property Definition T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Id Property Definition T</em>'.
	 * @generated
	 */
	IdPropertyDefinitionT createIdPropertyDefinitionT();

	/**
	 * Returns a new object of class '<em>Id Short T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Id Short T</em>'.
	 * @generated
	 */
	IdShortT createIdShortT();

	/**
	 * Returns a new object of class '<em>Keys T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Keys T</em>'.
	 * @generated
	 */
	KeysT createKeysT();

	/**
	 * Returns a new object of class '<em>Key T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Key T</em>'.
	 * @generated
	 */
	KeyT createKeyT();

	/**
	 * Returns a new object of class '<em>Lang String Set T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Lang String Set T</em>'.
	 * @generated
	 */
	LangStringSetT createLangStringSetT();

	/**
	 * Returns a new object of class '<em>Lang String T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Lang String T</em>'.
	 * @generated
	 */
	LangStringT createLangStringT();

	/**
	 * Returns a new object of class '<em>Multi Language Property T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Multi Language Property T</em>'.
	 * @generated
	 */
	MultiLanguagePropertyT createMultiLanguagePropertyT();

	/**
	 * Returns a new object of class '<em>Operation T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operation T</em>'.
	 * @generated
	 */
	OperationT createOperationT();

	/**
	 * Returns a new object of class '<em>Operation Variable T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operation Variable T</em>'.
	 * @generated
	 */
	OperationVariableT createOperationVariableT();

	/**
	 * Returns a new object of class '<em>Path Type T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Path Type T</em>'.
	 * @generated
	 */
	PathTypeT createPathTypeT();

	/**
	 * Returns a new object of class '<em>Property T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Property T</em>'.
	 * @generated
	 */
	PropertyT createPropertyT();

	/**
	 * Returns a new object of class '<em>Qualifier T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Qualifier T</em>'.
	 * @generated
	 */
	QualifierT createQualifierT();

	/**
	 * Returns a new object of class '<em>Qualifier Type T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Qualifier Type T</em>'.
	 * @generated
	 */
	QualifierTypeT createQualifierTypeT();

	/**
	 * Returns a new object of class '<em>Range T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Range T</em>'.
	 * @generated
	 */
	RangeT createRangeT();

	/**
	 * Returns a new object of class '<em>Reference Element T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reference Element T</em>'.
	 * @generated
	 */
	ReferenceElementT createReferenceElementT();

	/**
	 * Returns a new object of class '<em>References T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>References T</em>'.
	 * @generated
	 */
	ReferencesT createReferencesT();

	/**
	 * Returns a new object of class '<em>Reference T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reference T</em>'.
	 * @generated
	 */
	ReferenceT createReferenceT();

	/**
	 * Returns a new object of class '<em>Relationship Element T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Relationship Element T</em>'.
	 * @generated
	 */
	RelationshipElementT createRelationshipElementT();

	/**
	 * Returns a new object of class '<em>Semantic Id T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Semantic Id T</em>'.
	 * @generated
	 */
	SemanticIdT createSemanticIdT();

	/**
	 * Returns a new object of class '<em>Submodel Element Abstract T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Submodel Element Abstract T</em>'.
	 * @generated
	 */
	SubmodelElementAbstractT createSubmodelElementAbstractT();

	/**
	 * Returns a new object of class '<em>Submodel Element Collection T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Submodel Element Collection T</em>'.
	 * @generated
	 */
	SubmodelElementCollectionT createSubmodelElementCollectionT();

	/**
	 * Returns a new object of class '<em>Submodel Elements T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Submodel Elements T</em>'.
	 * @generated
	 */
	SubmodelElementsT createSubmodelElementsT();

	/**
	 * Returns a new object of class '<em>Submodel Element T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Submodel Element T</em>'.
	 * @generated
	 */
	SubmodelElementT createSubmodelElementT();

	/**
	 * Returns a new object of class '<em>Submodel Refs T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Submodel Refs T</em>'.
	 * @generated
	 */
	SubmodelRefsT createSubmodelRefsT();

	/**
	 * Returns a new object of class '<em>Submodels T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Submodels T</em>'.
	 * @generated
	 */
	SubmodelsT createSubmodelsT();

	/**
	 * Returns a new object of class '<em>Submodel T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Submodel T</em>'.
	 * @generated
	 */
	SubmodelT createSubmodelT();

	/**
	 * Returns a new object of class '<em>Value Data Type T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Value Data Type T</em>'.
	 * @generated
	 */
	ValueDataTypeT createValueDataTypeT();

	/**
	 * Returns a new object of class '<em>Views T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Views T</em>'.
	 * @generated
	 */
	ViewsT createViewsT();

	/**
	 * Returns a new object of class '<em>View T</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>View T</em>'.
	 * @generated
	 */
	ViewT createViewT();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	_0Package get_0Package();

} //_0Factory
