/**
 */
package io.shell.admin.aas.abac._2._0.impl;

import io.shell.admin.aas._2._0.ReferenceT;

import io.shell.admin.aas.abac._2._0.ContainedExtensionsT;
import io.shell.admin.aas.abac._2._0._0Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Contained Extensions T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.ContainedExtensionsTImpl#getContainedExtension <em>Contained Extension</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ContainedExtensionsTImpl extends MinimalEObjectImpl.Container implements ContainedExtensionsT {
	/**
	 * The cached value of the '{@link #getContainedExtension() <em>Contained Extension</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainedExtension()
	 * @generated
	 * @ordered
	 */
	protected EList<ReferenceT> containedExtension;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ContainedExtensionsTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.CONTAINED_EXTENSIONS_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReferenceT> getContainedExtension() {
		if (containedExtension == null) {
			containedExtension = new EObjectContainmentEList<ReferenceT>(ReferenceT.class, this, _0Package.CONTAINED_EXTENSIONS_T__CONTAINED_EXTENSION);
		}
		return containedExtension;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.CONTAINED_EXTENSIONS_T__CONTAINED_EXTENSION:
				return ((InternalEList<?>)getContainedExtension()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.CONTAINED_EXTENSIONS_T__CONTAINED_EXTENSION:
				return getContainedExtension();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.CONTAINED_EXTENSIONS_T__CONTAINED_EXTENSION:
				getContainedExtension().clear();
				getContainedExtension().addAll((Collection<? extends ReferenceT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.CONTAINED_EXTENSIONS_T__CONTAINED_EXTENSION:
				getContainedExtension().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.CONTAINED_EXTENSIONS_T__CONTAINED_EXTENSION:
				return containedExtension != null && !containedExtension.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ContainedExtensionsTImpl
