/**
 */
package io.shell.admin.aas._2._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Embedded Data Specification T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.EmbeddedDataSpecificationT#getDataSpecificationContent <em>Data Specification Content</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.EmbeddedDataSpecificationT#getDataSpecification <em>Data Specification</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getEmbeddedDataSpecificationT()
 * @model extendedMetaData="name='embeddedDataSpecification_t' kind='elementOnly'"
 * @generated
 */
public interface EmbeddedDataSpecificationT extends EObject {
	/**
	 * Returns the value of the '<em><b>Data Specification Content</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Specification Content</em>' containment reference.
	 * @see #setDataSpecificationContent(DataSpecificationContentT)
	 * @see io.shell.admin.aas._2._0._0Package#getEmbeddedDataSpecificationT_DataSpecificationContent()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='dataSpecificationContent' namespace='##targetNamespace'"
	 * @generated
	 */
	DataSpecificationContentT getDataSpecificationContent();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.EmbeddedDataSpecificationT#getDataSpecificationContent <em>Data Specification Content</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Specification Content</em>' containment reference.
	 * @see #getDataSpecificationContent()
	 * @generated
	 */
	void setDataSpecificationContent(DataSpecificationContentT value);

	/**
	 * Returns the value of the '<em><b>Data Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Specification</em>' containment reference.
	 * @see #setDataSpecification(ReferenceT)
	 * @see io.shell.admin.aas._2._0._0Package#getEmbeddedDataSpecificationT_DataSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='dataSpecification' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getDataSpecification();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.EmbeddedDataSpecificationT#getDataSpecification <em>Data Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Specification</em>' containment reference.
	 * @see #getDataSpecification()
	 * @generated
	 */
	void setDataSpecification(ReferenceT value);

} // EmbeddedDataSpecificationT
