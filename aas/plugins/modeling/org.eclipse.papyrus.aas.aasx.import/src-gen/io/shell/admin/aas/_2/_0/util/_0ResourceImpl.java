/**
 */
package io.shell.admin.aas._2._0.util;

import java.io.IOException;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.xmi.XMLHelper;
import org.eclipse.emf.ecore.xmi.XMLLoad;
import org.eclipse.emf.ecore.xmi.impl.XMLLoadImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc --> The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * 
 * @see io.shell.admin.aas._2._0.util._0ResourceFactoryImpl
 * @generated
 */
public class _0ResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public _0ResourceImpl(URI uri) {
		super(uri);
	}

	@Override
	protected XMLLoad createXMLLoad(Map<?, ?> options) {
		return new AASXMLLoadImpl(createXMLHelper());
	}

	public class AASXMLLoadImpl extends XMLLoadImpl {
		/**
		 * @param helper
		 */
		public AASXMLLoadImpl(XMLHelper helper) {
			super(helper);
			// TODO Auto-generated constructor stub
		}

		@Override
		protected void handleErrors() throws IOException {
			System.out.println(resource.getErrors());
		}

	}
} // _0ResourceImpl
