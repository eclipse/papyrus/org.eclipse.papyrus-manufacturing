/**
 */
package io.shell.admin.aas.abac._2._0.impl;

import io.shell.admin.aas._2._0.SubmodelRefsT;

import io.shell.admin.aas.abac._2._0.InternalInformationPoints;
import io.shell.admin.aas.abac._2._0._0Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Internal Information Points</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.InternalInformationPointsImpl#getInternalInformationPoint <em>Internal Information Point</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InternalInformationPointsImpl extends MinimalEObjectImpl.Container implements InternalInformationPoints {
	/**
	 * The cached value of the '{@link #getInternalInformationPoint() <em>Internal Information Point</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInternalInformationPoint()
	 * @generated
	 * @ordered
	 */
	protected EList<SubmodelRefsT> internalInformationPoint;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InternalInformationPointsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.INTERNAL_INFORMATION_POINTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SubmodelRefsT> getInternalInformationPoint() {
		if (internalInformationPoint == null) {
			internalInformationPoint = new EObjectContainmentEList<SubmodelRefsT>(SubmodelRefsT.class, this, _0Package.INTERNAL_INFORMATION_POINTS__INTERNAL_INFORMATION_POINT);
		}
		return internalInformationPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.INTERNAL_INFORMATION_POINTS__INTERNAL_INFORMATION_POINT:
				return ((InternalEList<?>)getInternalInformationPoint()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.INTERNAL_INFORMATION_POINTS__INTERNAL_INFORMATION_POINT:
				return getInternalInformationPoint();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.INTERNAL_INFORMATION_POINTS__INTERNAL_INFORMATION_POINT:
				getInternalInformationPoint().clear();
				getInternalInformationPoint().addAll((Collection<? extends SubmodelRefsT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.INTERNAL_INFORMATION_POINTS__INTERNAL_INFORMATION_POINT:
				getInternalInformationPoint().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.INTERNAL_INFORMATION_POINTS__INTERNAL_INFORMATION_POINT:
				return internalInformationPoint != null && !internalInformationPoint.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //InternalInformationPointsImpl
