/**
 */
package io.shell.admin.aas._2._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Key T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.KeyT#getValue <em>Value</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.KeyT#getIdType <em>Id Type</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.KeyT#isLocal <em>Local</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.KeyT#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getKeyT()
 * @model extendedMetaData="name='key_t' kind='simple'"
 * @generated
 */
public interface KeyT extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see io.shell.admin.aas._2._0._0Package#getKeyT_Value()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="name=':0' kind='simple'"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.KeyT#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>Id Type</b></em>' attribute.
	 * The literals are from the enumeration {@link io.shell.admin.aas._2._0.IdTypeType1}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id Type</em>' attribute.
	 * @see io.shell.admin.aas._2._0.IdTypeType1
	 * @see #isSetIdType()
	 * @see #unsetIdType()
	 * @see #setIdType(IdTypeType1)
	 * @see io.shell.admin.aas._2._0._0Package#getKeyT_IdType()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='idType'"
	 * @generated
	 */
	IdTypeType1 getIdType();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.KeyT#getIdType <em>Id Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id Type</em>' attribute.
	 * @see io.shell.admin.aas._2._0.IdTypeType1
	 * @see #isSetIdType()
	 * @see #unsetIdType()
	 * @see #getIdType()
	 * @generated
	 */
	void setIdType(IdTypeType1 value);

	/**
	 * Unsets the value of the '{@link io.shell.admin.aas._2._0.KeyT#getIdType <em>Id Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIdType()
	 * @see #getIdType()
	 * @see #setIdType(IdTypeType1)
	 * @generated
	 */
	void unsetIdType();

	/**
	 * Returns whether the value of the '{@link io.shell.admin.aas._2._0.KeyT#getIdType <em>Id Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Id Type</em>' attribute is set.
	 * @see #unsetIdType()
	 * @see #getIdType()
	 * @see #setIdType(IdTypeType1)
	 * @generated
	 */
	boolean isSetIdType();

	/**
	 * Returns the value of the '<em><b>Local</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local</em>' attribute.
	 * @see #isSetLocal()
	 * @see #unsetLocal()
	 * @see #setLocal(boolean)
	 * @see io.shell.admin.aas._2._0._0Package#getKeyT_Local()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='local'"
	 * @generated
	 */
	boolean isLocal();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.KeyT#isLocal <em>Local</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Local</em>' attribute.
	 * @see #isSetLocal()
	 * @see #unsetLocal()
	 * @see #isLocal()
	 * @generated
	 */
	void setLocal(boolean value);

	/**
	 * Unsets the value of the '{@link io.shell.admin.aas._2._0.KeyT#isLocal <em>Local</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLocal()
	 * @see #isLocal()
	 * @see #setLocal(boolean)
	 * @generated
	 */
	void unsetLocal();

	/**
	 * Returns whether the value of the '{@link io.shell.admin.aas._2._0.KeyT#isLocal <em>Local</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Local</em>' attribute is set.
	 * @see #unsetLocal()
	 * @see #isLocal()
	 * @see #setLocal(boolean)
	 * @generated
	 */
	boolean isSetLocal();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link io.shell.admin.aas._2._0.TypeType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see io.shell.admin.aas._2._0.TypeType
	 * @see #isSetType()
	 * @see #unsetType()
	 * @see #setType(TypeType)
	 * @see io.shell.admin.aas._2._0._0Package#getKeyT_Type()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='type'"
	 * @generated
	 */
	TypeType getType();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.KeyT#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see io.shell.admin.aas._2._0.TypeType
	 * @see #isSetType()
	 * @see #unsetType()
	 * @see #getType()
	 * @generated
	 */
	void setType(TypeType value);

	/**
	 * Unsets the value of the '{@link io.shell.admin.aas._2._0.KeyT#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetType()
	 * @see #getType()
	 * @see #setType(TypeType)
	 * @generated
	 */
	void unsetType();

	/**
	 * Returns whether the value of the '{@link io.shell.admin.aas._2._0.KeyT#getType <em>Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Type</em>' attribute is set.
	 * @see #unsetType()
	 * @see #getType()
	 * @see #setType(TypeType)
	 * @generated
	 */
	boolean isSetType();

} // KeyT
