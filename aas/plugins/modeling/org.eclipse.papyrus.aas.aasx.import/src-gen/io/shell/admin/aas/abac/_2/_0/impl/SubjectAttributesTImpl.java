/**
 */
package io.shell.admin.aas.abac._2._0.impl;

import io.shell.admin.aas._2._0.PropertyT;

import io.shell.admin.aas.abac._2._0.SubjectAttributesT;
import io.shell.admin.aas.abac._2._0._0Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Subject Attributes T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.SubjectAttributesTImpl#getSubjectAttribute <em>Subject Attribute</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubjectAttributesTImpl extends MinimalEObjectImpl.Container implements SubjectAttributesT {
	/**
	 * The cached value of the '{@link #getSubjectAttribute() <em>Subject Attribute</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubjectAttribute()
	 * @generated
	 * @ordered
	 */
	protected EList<PropertyT> subjectAttribute;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubjectAttributesTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.SUBJECT_ATTRIBUTES_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PropertyT> getSubjectAttribute() {
		if (subjectAttribute == null) {
			subjectAttribute = new EObjectContainmentEList<PropertyT>(PropertyT.class, this, _0Package.SUBJECT_ATTRIBUTES_T__SUBJECT_ATTRIBUTE);
		}
		return subjectAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.SUBJECT_ATTRIBUTES_T__SUBJECT_ATTRIBUTE:
				return ((InternalEList<?>)getSubjectAttribute()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.SUBJECT_ATTRIBUTES_T__SUBJECT_ATTRIBUTE:
				return getSubjectAttribute();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.SUBJECT_ATTRIBUTES_T__SUBJECT_ATTRIBUTE:
				getSubjectAttribute().clear();
				getSubjectAttribute().addAll((Collection<? extends PropertyT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.SUBJECT_ATTRIBUTES_T__SUBJECT_ATTRIBUTE:
				getSubjectAttribute().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.SUBJECT_ATTRIBUTES_T__SUBJECT_ATTRIBUTE:
				return subjectAttribute != null && !subjectAttribute.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SubjectAttributesTImpl
