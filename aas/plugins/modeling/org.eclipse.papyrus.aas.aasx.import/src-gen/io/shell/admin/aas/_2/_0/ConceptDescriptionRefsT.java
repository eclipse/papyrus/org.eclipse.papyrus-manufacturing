/**
 */
package io.shell.admin.aas._2._0;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Concept Description Refs T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.ConceptDescriptionRefsT#getConceptDescriptionRef <em>Concept Description Ref</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getConceptDescriptionRefsT()
 * @model extendedMetaData="name='conceptDescriptionRefs_t' kind='elementOnly'"
 * @generated
 */
public interface ConceptDescriptionRefsT extends EObject {
	/**
	 * Returns the value of the '<em><b>Concept Description Ref</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas._2._0.ReferenceT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Concept Description Ref</em>' containment reference list.
	 * @see io.shell.admin.aas._2._0._0Package#getConceptDescriptionRefsT_ConceptDescriptionRef()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='conceptDescriptionRef' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ReferenceT> getConceptDescriptionRef();

} // ConceptDescriptionRefsT
