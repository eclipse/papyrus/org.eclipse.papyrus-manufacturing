/**
 */
package io.shell.admin.aas._2._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Qualifier T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.QualifierT#getSemanticId <em>Semantic Id</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.QualifierT#getType <em>Type</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.QualifierT#getValueType <em>Value Type</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.QualifierT#getValueId <em>Value Id</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.QualifierT#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getQualifierT()
 * @model extendedMetaData="name='qualifier_t' kind='elementOnly'"
 * @generated
 */
public interface QualifierT extends EObject {
	/**
	 * Returns the value of the '<em><b>Semantic Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semantic Id</em>' containment reference.
	 * @see #setSemanticId(SemanticIdT)
	 * @see io.shell.admin.aas._2._0._0Package#getQualifierT_SemanticId()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='semanticId' namespace='##targetNamespace'"
	 * @generated
	 */
	SemanticIdT getSemanticId();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.QualifierT#getSemanticId <em>Semantic Id</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Semantic Id</em>' containment reference.
	 * @see #getSemanticId()
	 * @generated
	 */
	void setSemanticId(SemanticIdT value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' containment reference.
	 * @see #setType(QualifierTypeT)
	 * @see io.shell.admin.aas._2._0._0Package#getQualifierT_Type()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='type' namespace='##targetNamespace'"
	 * @generated
	 */
	QualifierTypeT getType();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.QualifierT#getType <em>Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' containment reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(QualifierTypeT value);

	/**
	 * Returns the value of the '<em><b>Value Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Type</em>' containment reference.
	 * @see #setValueType(DataTypeDefT)
	 * @see io.shell.admin.aas._2._0._0Package#getQualifierT_ValueType()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='valueType' namespace='##targetNamespace'"
	 * @generated
	 */
	DataTypeDefT getValueType();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.QualifierT#getValueType <em>Value Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Type</em>' containment reference.
	 * @see #getValueType()
	 * @generated
	 */
	void setValueType(DataTypeDefT value);

	/**
	 * Returns the value of the '<em><b>Value Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Id</em>' containment reference.
	 * @see #setValueId(ReferenceT)
	 * @see io.shell.admin.aas._2._0._0Package#getQualifierT_ValueId()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='valueId' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getValueId();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.QualifierT#getValueId <em>Value Id</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Id</em>' containment reference.
	 * @see #getValueId()
	 * @generated
	 */
	void setValueId(ReferenceT value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(ValueDataTypeT)
	 * @see io.shell.admin.aas._2._0._0Package#getQualifierT_Value()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='value' namespace='##targetNamespace'"
	 * @generated
	 */
	ValueDataTypeT getValue();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.QualifierT#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(ValueDataTypeT value);

} // QualifierT
