/**
 */
package io.shell.admin.iec61360._2._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value Reference Pair T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.iec61360._2._0.ValueReferencePairT#getValueId <em>Value Id</em>}</li>
 *   <li>{@link io.shell.admin.iec61360._2._0.ValueReferencePairT#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.iec61360._2._0._0Package#getValueReferencePairT()
 * @model extendedMetaData="name='valueReferencePair_t' kind='elementOnly'"
 * @generated
 */
public interface ValueReferencePairT extends EObject {
	/**
	 * Returns the value of the '<em><b>Value Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Id</em>' containment reference.
	 * @see #setValueId(ReferenceT)
	 * @see io.shell.admin.iec61360._2._0._0Package#getValueReferencePairT_ValueId()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='valueId' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getValueId();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._2._0.ValueReferencePairT#getValueId <em>Value Id</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Id</em>' containment reference.
	 * @see #getValueId()
	 * @generated
	 */
	void setValueId(ReferenceT value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(ValueDataTypeT)
	 * @see io.shell.admin.iec61360._2._0._0Package#getValueReferencePairT_Value()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='value' namespace='##targetNamespace'"
	 * @generated
	 */
	ValueDataTypeT getValue();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._2._0.ValueReferencePairT#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(ValueDataTypeT value);

} // ValueReferencePairT
