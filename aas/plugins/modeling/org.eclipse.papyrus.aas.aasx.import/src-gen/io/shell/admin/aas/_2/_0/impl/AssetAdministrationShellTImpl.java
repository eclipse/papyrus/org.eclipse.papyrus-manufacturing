/**
 */
package io.shell.admin.aas._2._0.impl;

import io.shell.admin.aas._2._0.AdministrationT;
import io.shell.admin.aas._2._0.AssetAdministrationShellT;
import io.shell.admin.aas._2._0.ConceptDictionariesT;
import io.shell.admin.aas._2._0.EmbeddedDataSpecificationT;
import io.shell.admin.aas._2._0.IdShortT;
import io.shell.admin.aas._2._0.IdentificationT;
import io.shell.admin.aas._2._0.LangStringSetT;
import io.shell.admin.aas._2._0.ReferenceT;
import io.shell.admin.aas._2._0.SubmodelRefsT;
import io.shell.admin.aas._2._0.ViewsT;
import io.shell.admin.aas._2._0._0Package;

import io.shell.admin.aas.abac._2._0.SecurityT;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Asset Administration Shell T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.impl.AssetAdministrationShellTImpl#getIdShort <em>Id Short</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.AssetAdministrationShellTImpl#getCategory <em>Category</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.AssetAdministrationShellTImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.AssetAdministrationShellTImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.AssetAdministrationShellTImpl#getIdentification <em>Identification</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.AssetAdministrationShellTImpl#getAdministration <em>Administration</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.AssetAdministrationShellTImpl#getEmbeddedDataSpecification <em>Embedded Data Specification</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.AssetAdministrationShellTImpl#getDerivedFrom <em>Derived From</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.AssetAdministrationShellTImpl#getAssetRef <em>Asset Ref</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.AssetAdministrationShellTImpl#getSubmodelRefs <em>Submodel Refs</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.AssetAdministrationShellTImpl#getViews <em>Views</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.AssetAdministrationShellTImpl#getConceptDictionaries <em>Concept Dictionaries</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.AssetAdministrationShellTImpl#getSecurity <em>Security</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AssetAdministrationShellTImpl extends MinimalEObjectImpl.Container implements AssetAdministrationShellT {
	/**
	 * The cached value of the '{@link #getIdShort() <em>Id Short</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdShort()
	 * @generated
	 * @ordered
	 */
	protected IdShortT idShort;

	/**
	 * The default value of the '{@link #getCategory() <em>Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCategory()
	 * @generated
	 * @ordered
	 */
	protected static final String CATEGORY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCategory() <em>Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCategory()
	 * @generated
	 * @ordered
	 */
	protected String category = CATEGORY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected LangStringSetT description;

	/**
	 * The cached value of the '{@link #getParent() <em>Parent</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParent()
	 * @generated
	 * @ordered
	 */
	protected ReferenceT parent;

	/**
	 * The cached value of the '{@link #getIdentification() <em>Identification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdentification()
	 * @generated
	 * @ordered
	 */
	protected IdentificationT identification;

	/**
	 * The cached value of the '{@link #getAdministration() <em>Administration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAdministration()
	 * @generated
	 * @ordered
	 */
	protected AdministrationT administration;

	/**
	 * The cached value of the '{@link #getEmbeddedDataSpecification() <em>Embedded Data Specification</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEmbeddedDataSpecification()
	 * @generated
	 * @ordered
	 */
	protected EList<EmbeddedDataSpecificationT> embeddedDataSpecification;

	/**
	 * The cached value of the '{@link #getDerivedFrom() <em>Derived From</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedFrom()
	 * @generated
	 * @ordered
	 */
	protected ReferenceT derivedFrom;

	/**
	 * The cached value of the '{@link #getAssetRef() <em>Asset Ref</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssetRef()
	 * @generated
	 * @ordered
	 */
	protected ReferenceT assetRef;

	/**
	 * The cached value of the '{@link #getSubmodelRefs() <em>Submodel Refs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubmodelRefs()
	 * @generated
	 * @ordered
	 */
	protected SubmodelRefsT submodelRefs;

	/**
	 * The cached value of the '{@link #getViews() <em>Views</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getViews()
	 * @generated
	 * @ordered
	 */
	protected ViewsT views;

	/**
	 * The cached value of the '{@link #getConceptDictionaries() <em>Concept Dictionaries</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConceptDictionaries()
	 * @generated
	 * @ordered
	 */
	protected ConceptDictionariesT conceptDictionaries;

	/**
	 * The cached value of the '{@link #getSecurity() <em>Security</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSecurity()
	 * @generated
	 * @ordered
	 */
	protected SecurityT security;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssetAdministrationShellTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.ASSET_ADMINISTRATION_SHELL_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdShortT getIdShort() {
		return idShort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIdShort(IdShortT newIdShort, NotificationChain msgs) {
		IdShortT oldIdShort = idShort;
		idShort = newIdShort;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ASSET_ADMINISTRATION_SHELL_T__ID_SHORT, oldIdShort, newIdShort);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdShort(IdShortT newIdShort) {
		if (newIdShort != idShort) {
			NotificationChain msgs = null;
			if (idShort != null)
				msgs = ((InternalEObject)idShort).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ASSET_ADMINISTRATION_SHELL_T__ID_SHORT, null, msgs);
			if (newIdShort != null)
				msgs = ((InternalEObject)newIdShort).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ASSET_ADMINISTRATION_SHELL_T__ID_SHORT, null, msgs);
			msgs = basicSetIdShort(newIdShort, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ASSET_ADMINISTRATION_SHELL_T__ID_SHORT, newIdShort, newIdShort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCategory(String newCategory) {
		String oldCategory = category;
		category = newCategory;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ASSET_ADMINISTRATION_SHELL_T__CATEGORY, oldCategory, category));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LangStringSetT getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDescription(LangStringSetT newDescription, NotificationChain msgs) {
		LangStringSetT oldDescription = description;
		description = newDescription;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ASSET_ADMINISTRATION_SHELL_T__DESCRIPTION, oldDescription, newDescription);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(LangStringSetT newDescription) {
		if (newDescription != description) {
			NotificationChain msgs = null;
			if (description != null)
				msgs = ((InternalEObject)description).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ASSET_ADMINISTRATION_SHELL_T__DESCRIPTION, null, msgs);
			if (newDescription != null)
				msgs = ((InternalEObject)newDescription).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ASSET_ADMINISTRATION_SHELL_T__DESCRIPTION, null, msgs);
			msgs = basicSetDescription(newDescription, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ASSET_ADMINISTRATION_SHELL_T__DESCRIPTION, newDescription, newDescription));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceT getParent() {
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParent(ReferenceT newParent, NotificationChain msgs) {
		ReferenceT oldParent = parent;
		parent = newParent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ASSET_ADMINISTRATION_SHELL_T__PARENT, oldParent, newParent);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParent(ReferenceT newParent) {
		if (newParent != parent) {
			NotificationChain msgs = null;
			if (parent != null)
				msgs = ((InternalEObject)parent).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ASSET_ADMINISTRATION_SHELL_T__PARENT, null, msgs);
			if (newParent != null)
				msgs = ((InternalEObject)newParent).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ASSET_ADMINISTRATION_SHELL_T__PARENT, null, msgs);
			msgs = basicSetParent(newParent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ASSET_ADMINISTRATION_SHELL_T__PARENT, newParent, newParent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentificationT getIdentification() {
		return identification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIdentification(IdentificationT newIdentification, NotificationChain msgs) {
		IdentificationT oldIdentification = identification;
		identification = newIdentification;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ASSET_ADMINISTRATION_SHELL_T__IDENTIFICATION, oldIdentification, newIdentification);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdentification(IdentificationT newIdentification) {
		if (newIdentification != identification) {
			NotificationChain msgs = null;
			if (identification != null)
				msgs = ((InternalEObject)identification).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ASSET_ADMINISTRATION_SHELL_T__IDENTIFICATION, null, msgs);
			if (newIdentification != null)
				msgs = ((InternalEObject)newIdentification).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ASSET_ADMINISTRATION_SHELL_T__IDENTIFICATION, null, msgs);
			msgs = basicSetIdentification(newIdentification, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ASSET_ADMINISTRATION_SHELL_T__IDENTIFICATION, newIdentification, newIdentification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdministrationT getAdministration() {
		return administration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAdministration(AdministrationT newAdministration, NotificationChain msgs) {
		AdministrationT oldAdministration = administration;
		administration = newAdministration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ASSET_ADMINISTRATION_SHELL_T__ADMINISTRATION, oldAdministration, newAdministration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAdministration(AdministrationT newAdministration) {
		if (newAdministration != administration) {
			NotificationChain msgs = null;
			if (administration != null)
				msgs = ((InternalEObject)administration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ASSET_ADMINISTRATION_SHELL_T__ADMINISTRATION, null, msgs);
			if (newAdministration != null)
				msgs = ((InternalEObject)newAdministration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ASSET_ADMINISTRATION_SHELL_T__ADMINISTRATION, null, msgs);
			msgs = basicSetAdministration(newAdministration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ASSET_ADMINISTRATION_SHELL_T__ADMINISTRATION, newAdministration, newAdministration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EmbeddedDataSpecificationT> getEmbeddedDataSpecification() {
		if (embeddedDataSpecification == null) {
			embeddedDataSpecification = new EObjectContainmentEList<EmbeddedDataSpecificationT>(EmbeddedDataSpecificationT.class, this, _0Package.ASSET_ADMINISTRATION_SHELL_T__EMBEDDED_DATA_SPECIFICATION);
		}
		return embeddedDataSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceT getDerivedFrom() {
		return derivedFrom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDerivedFrom(ReferenceT newDerivedFrom, NotificationChain msgs) {
		ReferenceT oldDerivedFrom = derivedFrom;
		derivedFrom = newDerivedFrom;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ASSET_ADMINISTRATION_SHELL_T__DERIVED_FROM, oldDerivedFrom, newDerivedFrom);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDerivedFrom(ReferenceT newDerivedFrom) {
		if (newDerivedFrom != derivedFrom) {
			NotificationChain msgs = null;
			if (derivedFrom != null)
				msgs = ((InternalEObject)derivedFrom).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ASSET_ADMINISTRATION_SHELL_T__DERIVED_FROM, null, msgs);
			if (newDerivedFrom != null)
				msgs = ((InternalEObject)newDerivedFrom).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ASSET_ADMINISTRATION_SHELL_T__DERIVED_FROM, null, msgs);
			msgs = basicSetDerivedFrom(newDerivedFrom, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ASSET_ADMINISTRATION_SHELL_T__DERIVED_FROM, newDerivedFrom, newDerivedFrom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceT getAssetRef() {
		return assetRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssetRef(ReferenceT newAssetRef, NotificationChain msgs) {
		ReferenceT oldAssetRef = assetRef;
		assetRef = newAssetRef;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ASSET_ADMINISTRATION_SHELL_T__ASSET_REF, oldAssetRef, newAssetRef);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssetRef(ReferenceT newAssetRef) {
		if (newAssetRef != assetRef) {
			NotificationChain msgs = null;
			if (assetRef != null)
				msgs = ((InternalEObject)assetRef).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ASSET_ADMINISTRATION_SHELL_T__ASSET_REF, null, msgs);
			if (newAssetRef != null)
				msgs = ((InternalEObject)newAssetRef).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ASSET_ADMINISTRATION_SHELL_T__ASSET_REF, null, msgs);
			msgs = basicSetAssetRef(newAssetRef, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ASSET_ADMINISTRATION_SHELL_T__ASSET_REF, newAssetRef, newAssetRef));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubmodelRefsT getSubmodelRefs() {
		return submodelRefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSubmodelRefs(SubmodelRefsT newSubmodelRefs, NotificationChain msgs) {
		SubmodelRefsT oldSubmodelRefs = submodelRefs;
		submodelRefs = newSubmodelRefs;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ASSET_ADMINISTRATION_SHELL_T__SUBMODEL_REFS, oldSubmodelRefs, newSubmodelRefs);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubmodelRefs(SubmodelRefsT newSubmodelRefs) {
		if (newSubmodelRefs != submodelRefs) {
			NotificationChain msgs = null;
			if (submodelRefs != null)
				msgs = ((InternalEObject)submodelRefs).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ASSET_ADMINISTRATION_SHELL_T__SUBMODEL_REFS, null, msgs);
			if (newSubmodelRefs != null)
				msgs = ((InternalEObject)newSubmodelRefs).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ASSET_ADMINISTRATION_SHELL_T__SUBMODEL_REFS, null, msgs);
			msgs = basicSetSubmodelRefs(newSubmodelRefs, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ASSET_ADMINISTRATION_SHELL_T__SUBMODEL_REFS, newSubmodelRefs, newSubmodelRefs));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ViewsT getViews() {
		return views;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetViews(ViewsT newViews, NotificationChain msgs) {
		ViewsT oldViews = views;
		views = newViews;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ASSET_ADMINISTRATION_SHELL_T__VIEWS, oldViews, newViews);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setViews(ViewsT newViews) {
		if (newViews != views) {
			NotificationChain msgs = null;
			if (views != null)
				msgs = ((InternalEObject)views).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ASSET_ADMINISTRATION_SHELL_T__VIEWS, null, msgs);
			if (newViews != null)
				msgs = ((InternalEObject)newViews).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ASSET_ADMINISTRATION_SHELL_T__VIEWS, null, msgs);
			msgs = basicSetViews(newViews, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ASSET_ADMINISTRATION_SHELL_T__VIEWS, newViews, newViews));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConceptDictionariesT getConceptDictionaries() {
		return conceptDictionaries;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConceptDictionaries(ConceptDictionariesT newConceptDictionaries, NotificationChain msgs) {
		ConceptDictionariesT oldConceptDictionaries = conceptDictionaries;
		conceptDictionaries = newConceptDictionaries;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ASSET_ADMINISTRATION_SHELL_T__CONCEPT_DICTIONARIES, oldConceptDictionaries, newConceptDictionaries);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConceptDictionaries(ConceptDictionariesT newConceptDictionaries) {
		if (newConceptDictionaries != conceptDictionaries) {
			NotificationChain msgs = null;
			if (conceptDictionaries != null)
				msgs = ((InternalEObject)conceptDictionaries).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ASSET_ADMINISTRATION_SHELL_T__CONCEPT_DICTIONARIES, null, msgs);
			if (newConceptDictionaries != null)
				msgs = ((InternalEObject)newConceptDictionaries).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ASSET_ADMINISTRATION_SHELL_T__CONCEPT_DICTIONARIES, null, msgs);
			msgs = basicSetConceptDictionaries(newConceptDictionaries, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ASSET_ADMINISTRATION_SHELL_T__CONCEPT_DICTIONARIES, newConceptDictionaries, newConceptDictionaries));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SecurityT getSecurity() {
		return security;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSecurity(SecurityT newSecurity, NotificationChain msgs) {
		SecurityT oldSecurity = security;
		security = newSecurity;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ASSET_ADMINISTRATION_SHELL_T__SECURITY, oldSecurity, newSecurity);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSecurity(SecurityT newSecurity) {
		if (newSecurity != security) {
			NotificationChain msgs = null;
			if (security != null)
				msgs = ((InternalEObject)security).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ASSET_ADMINISTRATION_SHELL_T__SECURITY, null, msgs);
			if (newSecurity != null)
				msgs = ((InternalEObject)newSecurity).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ASSET_ADMINISTRATION_SHELL_T__SECURITY, null, msgs);
			msgs = basicSetSecurity(newSecurity, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ASSET_ADMINISTRATION_SHELL_T__SECURITY, newSecurity, newSecurity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__ID_SHORT:
				return basicSetIdShort(null, msgs);
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__DESCRIPTION:
				return basicSetDescription(null, msgs);
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__PARENT:
				return basicSetParent(null, msgs);
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__IDENTIFICATION:
				return basicSetIdentification(null, msgs);
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__ADMINISTRATION:
				return basicSetAdministration(null, msgs);
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__EMBEDDED_DATA_SPECIFICATION:
				return ((InternalEList<?>)getEmbeddedDataSpecification()).basicRemove(otherEnd, msgs);
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__DERIVED_FROM:
				return basicSetDerivedFrom(null, msgs);
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__ASSET_REF:
				return basicSetAssetRef(null, msgs);
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__SUBMODEL_REFS:
				return basicSetSubmodelRefs(null, msgs);
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__VIEWS:
				return basicSetViews(null, msgs);
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__CONCEPT_DICTIONARIES:
				return basicSetConceptDictionaries(null, msgs);
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__SECURITY:
				return basicSetSecurity(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__ID_SHORT:
				return getIdShort();
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__CATEGORY:
				return getCategory();
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__DESCRIPTION:
				return getDescription();
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__PARENT:
				return getParent();
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__IDENTIFICATION:
				return getIdentification();
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__ADMINISTRATION:
				return getAdministration();
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__EMBEDDED_DATA_SPECIFICATION:
				return getEmbeddedDataSpecification();
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__DERIVED_FROM:
				return getDerivedFrom();
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__ASSET_REF:
				return getAssetRef();
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__SUBMODEL_REFS:
				return getSubmodelRefs();
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__VIEWS:
				return getViews();
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__CONCEPT_DICTIONARIES:
				return getConceptDictionaries();
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__SECURITY:
				return getSecurity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__ID_SHORT:
				setIdShort((IdShortT)newValue);
				return;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__CATEGORY:
				setCategory((String)newValue);
				return;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__DESCRIPTION:
				setDescription((LangStringSetT)newValue);
				return;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__PARENT:
				setParent((ReferenceT)newValue);
				return;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__IDENTIFICATION:
				setIdentification((IdentificationT)newValue);
				return;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__ADMINISTRATION:
				setAdministration((AdministrationT)newValue);
				return;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__EMBEDDED_DATA_SPECIFICATION:
				getEmbeddedDataSpecification().clear();
				getEmbeddedDataSpecification().addAll((Collection<? extends EmbeddedDataSpecificationT>)newValue);
				return;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__DERIVED_FROM:
				setDerivedFrom((ReferenceT)newValue);
				return;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__ASSET_REF:
				setAssetRef((ReferenceT)newValue);
				return;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__SUBMODEL_REFS:
				setSubmodelRefs((SubmodelRefsT)newValue);
				return;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__VIEWS:
				setViews((ViewsT)newValue);
				return;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__CONCEPT_DICTIONARIES:
				setConceptDictionaries((ConceptDictionariesT)newValue);
				return;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__SECURITY:
				setSecurity((SecurityT)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__ID_SHORT:
				setIdShort((IdShortT)null);
				return;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__CATEGORY:
				setCategory(CATEGORY_EDEFAULT);
				return;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__DESCRIPTION:
				setDescription((LangStringSetT)null);
				return;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__PARENT:
				setParent((ReferenceT)null);
				return;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__IDENTIFICATION:
				setIdentification((IdentificationT)null);
				return;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__ADMINISTRATION:
				setAdministration((AdministrationT)null);
				return;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__EMBEDDED_DATA_SPECIFICATION:
				getEmbeddedDataSpecification().clear();
				return;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__DERIVED_FROM:
				setDerivedFrom((ReferenceT)null);
				return;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__ASSET_REF:
				setAssetRef((ReferenceT)null);
				return;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__SUBMODEL_REFS:
				setSubmodelRefs((SubmodelRefsT)null);
				return;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__VIEWS:
				setViews((ViewsT)null);
				return;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__CONCEPT_DICTIONARIES:
				setConceptDictionaries((ConceptDictionariesT)null);
				return;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__SECURITY:
				setSecurity((SecurityT)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__ID_SHORT:
				return idShort != null;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__CATEGORY:
				return CATEGORY_EDEFAULT == null ? category != null : !CATEGORY_EDEFAULT.equals(category);
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__DESCRIPTION:
				return description != null;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__PARENT:
				return parent != null;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__IDENTIFICATION:
				return identification != null;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__ADMINISTRATION:
				return administration != null;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__EMBEDDED_DATA_SPECIFICATION:
				return embeddedDataSpecification != null && !embeddedDataSpecification.isEmpty();
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__DERIVED_FROM:
				return derivedFrom != null;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__ASSET_REF:
				return assetRef != null;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__SUBMODEL_REFS:
				return submodelRefs != null;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__VIEWS:
				return views != null;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__CONCEPT_DICTIONARIES:
				return conceptDictionaries != null;
			case _0Package.ASSET_ADMINISTRATION_SHELL_T__SECURITY:
				return security != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (category: ");
		result.append(category);
		result.append(')');
		return result.toString();
	}

} //AssetAdministrationShellTImpl
