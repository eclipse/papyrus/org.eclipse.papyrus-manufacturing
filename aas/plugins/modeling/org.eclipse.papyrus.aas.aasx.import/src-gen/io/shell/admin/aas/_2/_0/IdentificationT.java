/**
 */
package io.shell.admin.aas._2._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Identification T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.IdentificationT#getValue <em>Value</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.IdentificationT#getIdType <em>Id Type</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getIdentificationT()
 * @model extendedMetaData="name='identification_t' kind='simple'"
 * @generated
 */
public interface IdentificationT extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see io.shell.admin.aas._2._0._0Package#getIdentificationT_Value()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="name=':0' kind='simple'"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.IdentificationT#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>Id Type</b></em>' attribute.
	 * The literals are from the enumeration {@link io.shell.admin.aas._2._0.IdTypeType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id Type</em>' attribute.
	 * @see io.shell.admin.aas._2._0.IdTypeType
	 * @see #isSetIdType()
	 * @see #unsetIdType()
	 * @see #setIdType(IdTypeType)
	 * @see io.shell.admin.aas._2._0._0Package#getIdentificationT_IdType()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='idType'"
	 * @generated
	 */
	IdTypeType getIdType();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.IdentificationT#getIdType <em>Id Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id Type</em>' attribute.
	 * @see io.shell.admin.aas._2._0.IdTypeType
	 * @see #isSetIdType()
	 * @see #unsetIdType()
	 * @see #getIdType()
	 * @generated
	 */
	void setIdType(IdTypeType value);

	/**
	 * Unsets the value of the '{@link io.shell.admin.aas._2._0.IdentificationT#getIdType <em>Id Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIdType()
	 * @see #getIdType()
	 * @see #setIdType(IdTypeType)
	 * @generated
	 */
	void unsetIdType();

	/**
	 * Returns whether the value of the '{@link io.shell.admin.aas._2._0.IdentificationT#getIdType <em>Id Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Id Type</em>' attribute is set.
	 * @see #unsetIdType()
	 * @see #getIdType()
	 * @see #setIdType(IdTypeType)
	 * @generated
	 */
	boolean isSetIdType();

} // IdentificationT
