/**
 */
package io.shell.admin.aas.abac._2._0;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Access Permission Rules T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.AccessPermissionRulesT#getAccessPermissionRule <em>Access Permission Rule</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas.abac._2._0._0Package#getAccessPermissionRulesT()
 * @model extendedMetaData="name='accessPermissionRules_t' kind='elementOnly'"
 * @generated
 */
public interface AccessPermissionRulesT extends EObject {
	/**
	 * Returns the value of the '<em><b>Access Permission Rule</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas.abac._2._0.AccessPermissionRuleT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access Permission Rule</em>' containment reference list.
	 * @see io.shell.admin.aas.abac._2._0._0Package#getAccessPermissionRulesT_AccessPermissionRule()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='accessPermissionRule' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<AccessPermissionRuleT> getAccessPermissionRule();

} // AccessPermissionRulesT
