/**
 */
package io.shell.admin.aas.abac._2._0;

import io.shell.admin.aas._2._0.BlobT;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Blob Certificate T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.BlobCertificateT#getBlobCertificate <em>Blob Certificate</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.BlobCertificateT#getContainedExtensions <em>Contained Extensions</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.BlobCertificateT#isLastCertificate <em>Last Certificate</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas.abac._2._0._0Package#getBlobCertificateT()
 * @model extendedMetaData="name='blobCertificate_t' kind='elementOnly'"
 * @generated
 */
public interface BlobCertificateT extends CertificateAbstractT {
	/**
	 * Returns the value of the '<em><b>Blob Certificate</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Blob Certificate</em>' containment reference.
	 * @see #setBlobCertificate(BlobT)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getBlobCertificateT_BlobCertificate()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='blobCertificate' namespace='##targetNamespace'"
	 * @generated
	 */
	BlobT getBlobCertificate();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.BlobCertificateT#getBlobCertificate <em>Blob Certificate</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Blob Certificate</em>' containment reference.
	 * @see #getBlobCertificate()
	 * @generated
	 */
	void setBlobCertificate(BlobT value);

	/**
	 * Returns the value of the '<em><b>Contained Extensions</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contained Extensions</em>' containment reference.
	 * @see #setContainedExtensions(ContainedExtensionsT)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getBlobCertificateT_ContainedExtensions()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='containedExtensions' namespace='##targetNamespace'"
	 * @generated
	 */
	ContainedExtensionsT getContainedExtensions();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.BlobCertificateT#getContainedExtensions <em>Contained Extensions</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Contained Extensions</em>' containment reference.
	 * @see #getContainedExtensions()
	 * @generated
	 */
	void setContainedExtensions(ContainedExtensionsT value);

	/**
	 * Returns the value of the '<em><b>Last Certificate</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Certificate</em>' attribute.
	 * @see #isSetLastCertificate()
	 * @see #unsetLastCertificate()
	 * @see #setLastCertificate(boolean)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getBlobCertificateT_LastCertificate()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean" required="true"
	 *        extendedMetaData="kind='element' name='lastCertificate' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isLastCertificate();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.BlobCertificateT#isLastCertificate <em>Last Certificate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Certificate</em>' attribute.
	 * @see #isSetLastCertificate()
	 * @see #unsetLastCertificate()
	 * @see #isLastCertificate()
	 * @generated
	 */
	void setLastCertificate(boolean value);

	/**
	 * Unsets the value of the '{@link io.shell.admin.aas.abac._2._0.BlobCertificateT#isLastCertificate <em>Last Certificate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLastCertificate()
	 * @see #isLastCertificate()
	 * @see #setLastCertificate(boolean)
	 * @generated
	 */
	void unsetLastCertificate();

	/**
	 * Returns whether the value of the '{@link io.shell.admin.aas.abac._2._0.BlobCertificateT#isLastCertificate <em>Last Certificate</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Last Certificate</em>' attribute is set.
	 * @see #unsetLastCertificate()
	 * @see #isLastCertificate()
	 * @see #setLastCertificate(boolean)
	 * @generated
	 */
	boolean isSetLastCertificate();

} // BlobCertificateT
