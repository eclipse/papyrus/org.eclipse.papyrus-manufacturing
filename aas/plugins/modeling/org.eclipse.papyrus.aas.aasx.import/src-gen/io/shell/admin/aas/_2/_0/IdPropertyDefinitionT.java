/**
 */
package io.shell.admin.aas._2._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Id Property Definition T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.IdPropertyDefinitionT#getValue <em>Value</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.IdPropertyDefinitionT#getIdType <em>Id Type</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getIdPropertyDefinitionT()
 * @model extendedMetaData="name='idPropertyDefinition_t' kind='simple'"
 * @generated
 */
public interface IdPropertyDefinitionT extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see io.shell.admin.aas._2._0._0Package#getIdPropertyDefinitionT_Value()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="name=':0' kind='simple'"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.IdPropertyDefinitionT#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>Id Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id Type</em>' attribute.
	 * @see #setIdType(String)
	 * @see io.shell.admin.aas._2._0._0Package#getIdPropertyDefinitionT_IdType()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='idType'"
	 * @generated
	 */
	String getIdType();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.IdPropertyDefinitionT#getIdType <em>Id Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id Type</em>' attribute.
	 * @see #getIdType()
	 * @generated
	 */
	void setIdType(String value);

} // IdPropertyDefinitionT
