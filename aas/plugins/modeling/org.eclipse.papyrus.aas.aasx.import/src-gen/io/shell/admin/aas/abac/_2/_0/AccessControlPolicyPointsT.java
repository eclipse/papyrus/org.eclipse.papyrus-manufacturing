/**
 */
package io.shell.admin.aas.abac._2._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Access Control Policy Points T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.AccessControlPolicyPointsT#getPolicyAdministrationPoint <em>Policy Administration Point</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.AccessControlPolicyPointsT#getPolicyDecisionPoint <em>Policy Decision Point</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.AccessControlPolicyPointsT#getPolicyEnforcementPoint <em>Policy Enforcement Point</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.AccessControlPolicyPointsT#getPolicyInformationPoints <em>Policy Information Points</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas.abac._2._0._0Package#getAccessControlPolicyPointsT()
 * @model extendedMetaData="name='accessControlPolicyPoints_t' kind='elementOnly'"
 * @generated
 */
public interface AccessControlPolicyPointsT extends EObject {
	/**
	 * Returns the value of the '<em><b>Policy Administration Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Policy Administration Point</em>' containment reference.
	 * @see #setPolicyAdministrationPoint(PolicyAdministrationPointT)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getAccessControlPolicyPointsT_PolicyAdministrationPoint()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='policyAdministrationPoint' namespace='##targetNamespace'"
	 * @generated
	 */
	PolicyAdministrationPointT getPolicyAdministrationPoint();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.AccessControlPolicyPointsT#getPolicyAdministrationPoint <em>Policy Administration Point</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Policy Administration Point</em>' containment reference.
	 * @see #getPolicyAdministrationPoint()
	 * @generated
	 */
	void setPolicyAdministrationPoint(PolicyAdministrationPointT value);

	/**
	 * Returns the value of the '<em><b>Policy Decision Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Policy Decision Point</em>' containment reference.
	 * @see #setPolicyDecisionPoint(PolicyDecisionPointT)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getAccessControlPolicyPointsT_PolicyDecisionPoint()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='policyDecisionPoint' namespace='##targetNamespace'"
	 * @generated
	 */
	PolicyDecisionPointT getPolicyDecisionPoint();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.AccessControlPolicyPointsT#getPolicyDecisionPoint <em>Policy Decision Point</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Policy Decision Point</em>' containment reference.
	 * @see #getPolicyDecisionPoint()
	 * @generated
	 */
	void setPolicyDecisionPoint(PolicyDecisionPointT value);

	/**
	 * Returns the value of the '<em><b>Policy Enforcement Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Policy Enforcement Point</em>' containment reference.
	 * @see #setPolicyEnforcementPoint(PolicyEnforcementPointT)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getAccessControlPolicyPointsT_PolicyEnforcementPoint()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='policyEnforcementPoint' namespace='##targetNamespace'"
	 * @generated
	 */
	PolicyEnforcementPointT getPolicyEnforcementPoint();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.AccessControlPolicyPointsT#getPolicyEnforcementPoint <em>Policy Enforcement Point</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Policy Enforcement Point</em>' containment reference.
	 * @see #getPolicyEnforcementPoint()
	 * @generated
	 */
	void setPolicyEnforcementPoint(PolicyEnforcementPointT value);

	/**
	 * Returns the value of the '<em><b>Policy Information Points</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Policy Information Points</em>' containment reference.
	 * @see #setPolicyInformationPoints(PolicyInformationPointsT)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getAccessControlPolicyPointsT_PolicyInformationPoints()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='policyInformationPoints' namespace='##targetNamespace'"
	 * @generated
	 */
	PolicyInformationPointsT getPolicyInformationPoints();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.AccessControlPolicyPointsT#getPolicyInformationPoints <em>Policy Information Points</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Policy Information Points</em>' containment reference.
	 * @see #getPolicyInformationPoints()
	 * @generated
	 */
	void setPolicyInformationPoints(PolicyInformationPointsT value);

} // AccessControlPolicyPointsT
