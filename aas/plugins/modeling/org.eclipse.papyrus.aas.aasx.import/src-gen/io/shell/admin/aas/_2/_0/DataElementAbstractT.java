/**
 */
package io.shell.admin.aas._2._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Element Abstract T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.DataElementAbstractT#getMultiLanguageProperty <em>Multi Language Property</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.DataElementAbstractT#getProperty <em>Property</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.DataElementAbstractT#getRange <em>Range</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.DataElementAbstractT#getBlob <em>Blob</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.DataElementAbstractT#getFile <em>File</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.DataElementAbstractT#getReferenceElement <em>Reference Element</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getDataElementAbstractT()
 * @model extendedMetaData="name='dataElementAbstract_t' kind='elementOnly'"
 * @generated
 */
public interface DataElementAbstractT extends EObject {
	/**
	 * Returns the value of the '<em><b>Multi Language Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multi Language Property</em>' containment reference.
	 * @see #setMultiLanguageProperty(MultiLanguagePropertyT)
	 * @see io.shell.admin.aas._2._0._0Package#getDataElementAbstractT_MultiLanguageProperty()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='multiLanguageProperty' namespace='##targetNamespace'"
	 * @generated
	 */
	MultiLanguagePropertyT getMultiLanguageProperty();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.DataElementAbstractT#getMultiLanguageProperty <em>Multi Language Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Multi Language Property</em>' containment reference.
	 * @see #getMultiLanguageProperty()
	 * @generated
	 */
	void setMultiLanguageProperty(MultiLanguagePropertyT value);

	/**
	 * Returns the value of the '<em><b>Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property</em>' containment reference.
	 * @see #setProperty(PropertyT)
	 * @see io.shell.admin.aas._2._0._0Package#getDataElementAbstractT_Property()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='property' namespace='##targetNamespace'"
	 * @generated
	 */
	PropertyT getProperty();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.DataElementAbstractT#getProperty <em>Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property</em>' containment reference.
	 * @see #getProperty()
	 * @generated
	 */
	void setProperty(PropertyT value);

	/**
	 * Returns the value of the '<em><b>Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range</em>' containment reference.
	 * @see #setRange(RangeT)
	 * @see io.shell.admin.aas._2._0._0Package#getDataElementAbstractT_Range()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='range' namespace='##targetNamespace'"
	 * @generated
	 */
	RangeT getRange();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.DataElementAbstractT#getRange <em>Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Range</em>' containment reference.
	 * @see #getRange()
	 * @generated
	 */
	void setRange(RangeT value);

	/**
	 * Returns the value of the '<em><b>Blob</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Blob</em>' containment reference.
	 * @see #setBlob(BlobT)
	 * @see io.shell.admin.aas._2._0._0Package#getDataElementAbstractT_Blob()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='blob' namespace='##targetNamespace'"
	 * @generated
	 */
	BlobT getBlob();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.DataElementAbstractT#getBlob <em>Blob</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Blob</em>' containment reference.
	 * @see #getBlob()
	 * @generated
	 */
	void setBlob(BlobT value);

	/**
	 * Returns the value of the '<em><b>File</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>File</em>' containment reference.
	 * @see #setFile(FileT)
	 * @see io.shell.admin.aas._2._0._0Package#getDataElementAbstractT_File()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='file' namespace='##targetNamespace'"
	 * @generated
	 */
	FileT getFile();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.DataElementAbstractT#getFile <em>File</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>File</em>' containment reference.
	 * @see #getFile()
	 * @generated
	 */
	void setFile(FileT value);

	/**
	 * Returns the value of the '<em><b>Reference Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference Element</em>' containment reference.
	 * @see #setReferenceElement(ReferenceElementT)
	 * @see io.shell.admin.aas._2._0._0Package#getDataElementAbstractT_ReferenceElement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='referenceElement' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceElementT getReferenceElement();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.DataElementAbstractT#getReferenceElement <em>Reference Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference Element</em>' containment reference.
	 * @see #getReferenceElement()
	 * @generated
	 */
	void setReferenceElement(ReferenceElementT value);

} // DataElementAbstractT
