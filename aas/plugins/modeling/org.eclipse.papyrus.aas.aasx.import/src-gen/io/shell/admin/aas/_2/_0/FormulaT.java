/**
 */
package io.shell.admin.aas._2._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Formula T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.FormulaT#getDependsOnRefs <em>Depends On Refs</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getFormulaT()
 * @model extendedMetaData="name='formula_t' kind='elementOnly'"
 * @generated
 */
public interface FormulaT extends EObject {
	/**
	 * Returns the value of the '<em><b>Depends On Refs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Depends On Refs</em>' containment reference.
	 * @see #setDependsOnRefs(ReferencesT)
	 * @see io.shell.admin.aas._2._0._0Package#getFormulaT_DependsOnRefs()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='dependsOnRefs' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferencesT getDependsOnRefs();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.FormulaT#getDependsOnRefs <em>Depends On Refs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Depends On Refs</em>' containment reference.
	 * @see #getDependsOnRefs()
	 * @generated
	 */
	void setDependsOnRefs(ReferencesT value);

} // FormulaT
