/**
 */
package io.shell.admin.aas._2._0;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Relationship Element T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.RelationshipElementT#getFirst <em>First</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.RelationshipElementT#getSecond <em>Second</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getRelationshipElementT()
 * @model extendedMetaData="name='relationshipElement_t' kind='elementOnly'"
 * @generated
 */
public interface RelationshipElementT extends SubmodelElementAbstractT {
	/**
	 * Returns the value of the '<em><b>First</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First</em>' containment reference.
	 * @see #setFirst(ReferenceT)
	 * @see io.shell.admin.aas._2._0._0Package#getRelationshipElementT_First()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='first' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getFirst();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.RelationshipElementT#getFirst <em>First</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First</em>' containment reference.
	 * @see #getFirst()
	 * @generated
	 */
	void setFirst(ReferenceT value);

	/**
	 * Returns the value of the '<em><b>Second</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Second</em>' containment reference.
	 * @see #setSecond(ReferenceT)
	 * @see io.shell.admin.aas._2._0._0Package#getRelationshipElementT_Second()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='second' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getSecond();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.RelationshipElementT#getSecond <em>Second</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Second</em>' containment reference.
	 * @see #getSecond()
	 * @generated
	 */
	void setSecond(ReferenceT value);

} // RelationshipElementT
