/**
 */
package io.shell.admin.aas._2._0;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Asset Administration Shells T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.AssetAdministrationShellsT#getAssetAdministrationShell <em>Asset Administration Shell</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getAssetAdministrationShellsT()
 * @model extendedMetaData="name='assetAdministrationShells_t' kind='elementOnly'"
 * @generated
 */
public interface AssetAdministrationShellsT extends EObject {
	/**
	 * Returns the value of the '<em><b>Asset Administration Shell</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas._2._0.AssetAdministrationShellT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asset Administration Shell</em>' containment reference list.
	 * @see io.shell.admin.aas._2._0._0Package#getAssetAdministrationShellsT_AssetAdministrationShell()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='assetAdministrationShell' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<AssetAdministrationShellT> getAssetAdministrationShell();

} // AssetAdministrationShellsT
