/**
 */
package io.shell.admin.aas.abac._2._0.impl;

import io.shell.admin.aas._2._0.ConstraintT;
import io.shell.admin.aas._2._0.IdShortT;
import io.shell.admin.aas._2._0.LangStringSetT;
import io.shell.admin.aas._2._0.ReferenceT;

import io.shell.admin.aas.abac._2._0.AccessPermissionRuleT;
import io.shell.admin.aas.abac._2._0.PermissionPerObjectT;
import io.shell.admin.aas.abac._2._0.SubjectAttributesT;
import io.shell.admin.aas.abac._2._0._0Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Access Permission Rule T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.AccessPermissionRuleTImpl#getQualifier <em>Qualifier</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.AccessPermissionRuleTImpl#getIdShort <em>Id Short</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.AccessPermissionRuleTImpl#getCategory <em>Category</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.AccessPermissionRuleTImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.AccessPermissionRuleTImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.AccessPermissionRuleTImpl#getTargetSubjectAttributes <em>Target Subject Attributes</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.AccessPermissionRuleTImpl#getPermissionsPerObject <em>Permissions Per Object</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AccessPermissionRuleTImpl extends MinimalEObjectImpl.Container implements AccessPermissionRuleT {
	/**
	 * The cached value of the '{@link #getQualifier() <em>Qualifier</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQualifier()
	 * @generated
	 * @ordered
	 */
	protected EList<ConstraintT> qualifier;

	/**
	 * The cached value of the '{@link #getIdShort() <em>Id Short</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdShort()
	 * @generated
	 * @ordered
	 */
	protected IdShortT idShort;

	/**
	 * The default value of the '{@link #getCategory() <em>Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCategory()
	 * @generated
	 * @ordered
	 */
	protected static final String CATEGORY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCategory() <em>Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCategory()
	 * @generated
	 * @ordered
	 */
	protected String category = CATEGORY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected LangStringSetT description;

	/**
	 * The cached value of the '{@link #getParent() <em>Parent</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParent()
	 * @generated
	 * @ordered
	 */
	protected ReferenceT parent;

	/**
	 * The cached value of the '{@link #getTargetSubjectAttributes() <em>Target Subject Attributes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetSubjectAttributes()
	 * @generated
	 * @ordered
	 */
	protected EList<SubjectAttributesT> targetSubjectAttributes;

	/**
	 * The cached value of the '{@link #getPermissionsPerObject() <em>Permissions Per Object</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPermissionsPerObject()
	 * @generated
	 * @ordered
	 */
	protected EList<PermissionPerObjectT> permissionsPerObject;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AccessPermissionRuleTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.ACCESS_PERMISSION_RULE_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConstraintT> getQualifier() {
		if (qualifier == null) {
			qualifier = new EObjectContainmentEList<ConstraintT>(ConstraintT.class, this, _0Package.ACCESS_PERMISSION_RULE_T__QUALIFIER);
		}
		return qualifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdShortT getIdShort() {
		return idShort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIdShort(IdShortT newIdShort, NotificationChain msgs) {
		IdShortT oldIdShort = idShort;
		idShort = newIdShort;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_PERMISSION_RULE_T__ID_SHORT, oldIdShort, newIdShort);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdShort(IdShortT newIdShort) {
		if (newIdShort != idShort) {
			NotificationChain msgs = null;
			if (idShort != null)
				msgs = ((InternalEObject)idShort).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_PERMISSION_RULE_T__ID_SHORT, null, msgs);
			if (newIdShort != null)
				msgs = ((InternalEObject)newIdShort).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_PERMISSION_RULE_T__ID_SHORT, null, msgs);
			msgs = basicSetIdShort(newIdShort, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_PERMISSION_RULE_T__ID_SHORT, newIdShort, newIdShort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCategory(String newCategory) {
		String oldCategory = category;
		category = newCategory;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_PERMISSION_RULE_T__CATEGORY, oldCategory, category));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LangStringSetT getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDescription(LangStringSetT newDescription, NotificationChain msgs) {
		LangStringSetT oldDescription = description;
		description = newDescription;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_PERMISSION_RULE_T__DESCRIPTION, oldDescription, newDescription);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(LangStringSetT newDescription) {
		if (newDescription != description) {
			NotificationChain msgs = null;
			if (description != null)
				msgs = ((InternalEObject)description).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_PERMISSION_RULE_T__DESCRIPTION, null, msgs);
			if (newDescription != null)
				msgs = ((InternalEObject)newDescription).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_PERMISSION_RULE_T__DESCRIPTION, null, msgs);
			msgs = basicSetDescription(newDescription, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_PERMISSION_RULE_T__DESCRIPTION, newDescription, newDescription));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceT getParent() {
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParent(ReferenceT newParent, NotificationChain msgs) {
		ReferenceT oldParent = parent;
		parent = newParent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_PERMISSION_RULE_T__PARENT, oldParent, newParent);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParent(ReferenceT newParent) {
		if (newParent != parent) {
			NotificationChain msgs = null;
			if (parent != null)
				msgs = ((InternalEObject)parent).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_PERMISSION_RULE_T__PARENT, null, msgs);
			if (newParent != null)
				msgs = ((InternalEObject)newParent).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.ACCESS_PERMISSION_RULE_T__PARENT, null, msgs);
			msgs = basicSetParent(newParent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.ACCESS_PERMISSION_RULE_T__PARENT, newParent, newParent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SubjectAttributesT> getTargetSubjectAttributes() {
		if (targetSubjectAttributes == null) {
			targetSubjectAttributes = new EObjectContainmentEList<SubjectAttributesT>(SubjectAttributesT.class, this, _0Package.ACCESS_PERMISSION_RULE_T__TARGET_SUBJECT_ATTRIBUTES);
		}
		return targetSubjectAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PermissionPerObjectT> getPermissionsPerObject() {
		if (permissionsPerObject == null) {
			permissionsPerObject = new EObjectContainmentEList<PermissionPerObjectT>(PermissionPerObjectT.class, this, _0Package.ACCESS_PERMISSION_RULE_T__PERMISSIONS_PER_OBJECT);
		}
		return permissionsPerObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.ACCESS_PERMISSION_RULE_T__QUALIFIER:
				return ((InternalEList<?>)getQualifier()).basicRemove(otherEnd, msgs);
			case _0Package.ACCESS_PERMISSION_RULE_T__ID_SHORT:
				return basicSetIdShort(null, msgs);
			case _0Package.ACCESS_PERMISSION_RULE_T__DESCRIPTION:
				return basicSetDescription(null, msgs);
			case _0Package.ACCESS_PERMISSION_RULE_T__PARENT:
				return basicSetParent(null, msgs);
			case _0Package.ACCESS_PERMISSION_RULE_T__TARGET_SUBJECT_ATTRIBUTES:
				return ((InternalEList<?>)getTargetSubjectAttributes()).basicRemove(otherEnd, msgs);
			case _0Package.ACCESS_PERMISSION_RULE_T__PERMISSIONS_PER_OBJECT:
				return ((InternalEList<?>)getPermissionsPerObject()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.ACCESS_PERMISSION_RULE_T__QUALIFIER:
				return getQualifier();
			case _0Package.ACCESS_PERMISSION_RULE_T__ID_SHORT:
				return getIdShort();
			case _0Package.ACCESS_PERMISSION_RULE_T__CATEGORY:
				return getCategory();
			case _0Package.ACCESS_PERMISSION_RULE_T__DESCRIPTION:
				return getDescription();
			case _0Package.ACCESS_PERMISSION_RULE_T__PARENT:
				return getParent();
			case _0Package.ACCESS_PERMISSION_RULE_T__TARGET_SUBJECT_ATTRIBUTES:
				return getTargetSubjectAttributes();
			case _0Package.ACCESS_PERMISSION_RULE_T__PERMISSIONS_PER_OBJECT:
				return getPermissionsPerObject();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.ACCESS_PERMISSION_RULE_T__QUALIFIER:
				getQualifier().clear();
				getQualifier().addAll((Collection<? extends ConstraintT>)newValue);
				return;
			case _0Package.ACCESS_PERMISSION_RULE_T__ID_SHORT:
				setIdShort((IdShortT)newValue);
				return;
			case _0Package.ACCESS_PERMISSION_RULE_T__CATEGORY:
				setCategory((String)newValue);
				return;
			case _0Package.ACCESS_PERMISSION_RULE_T__DESCRIPTION:
				setDescription((LangStringSetT)newValue);
				return;
			case _0Package.ACCESS_PERMISSION_RULE_T__PARENT:
				setParent((ReferenceT)newValue);
				return;
			case _0Package.ACCESS_PERMISSION_RULE_T__TARGET_SUBJECT_ATTRIBUTES:
				getTargetSubjectAttributes().clear();
				getTargetSubjectAttributes().addAll((Collection<? extends SubjectAttributesT>)newValue);
				return;
			case _0Package.ACCESS_PERMISSION_RULE_T__PERMISSIONS_PER_OBJECT:
				getPermissionsPerObject().clear();
				getPermissionsPerObject().addAll((Collection<? extends PermissionPerObjectT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.ACCESS_PERMISSION_RULE_T__QUALIFIER:
				getQualifier().clear();
				return;
			case _0Package.ACCESS_PERMISSION_RULE_T__ID_SHORT:
				setIdShort((IdShortT)null);
				return;
			case _0Package.ACCESS_PERMISSION_RULE_T__CATEGORY:
				setCategory(CATEGORY_EDEFAULT);
				return;
			case _0Package.ACCESS_PERMISSION_RULE_T__DESCRIPTION:
				setDescription((LangStringSetT)null);
				return;
			case _0Package.ACCESS_PERMISSION_RULE_T__PARENT:
				setParent((ReferenceT)null);
				return;
			case _0Package.ACCESS_PERMISSION_RULE_T__TARGET_SUBJECT_ATTRIBUTES:
				getTargetSubjectAttributes().clear();
				return;
			case _0Package.ACCESS_PERMISSION_RULE_T__PERMISSIONS_PER_OBJECT:
				getPermissionsPerObject().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.ACCESS_PERMISSION_RULE_T__QUALIFIER:
				return qualifier != null && !qualifier.isEmpty();
			case _0Package.ACCESS_PERMISSION_RULE_T__ID_SHORT:
				return idShort != null;
			case _0Package.ACCESS_PERMISSION_RULE_T__CATEGORY:
				return CATEGORY_EDEFAULT == null ? category != null : !CATEGORY_EDEFAULT.equals(category);
			case _0Package.ACCESS_PERMISSION_RULE_T__DESCRIPTION:
				return description != null;
			case _0Package.ACCESS_PERMISSION_RULE_T__PARENT:
				return parent != null;
			case _0Package.ACCESS_PERMISSION_RULE_T__TARGET_SUBJECT_ATTRIBUTES:
				return targetSubjectAttributes != null && !targetSubjectAttributes.isEmpty();
			case _0Package.ACCESS_PERMISSION_RULE_T__PERMISSIONS_PER_OBJECT:
				return permissionsPerObject != null && !permissionsPerObject.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (category: ");
		result.append(category);
		result.append(')');
		return result.toString();
	}

} //AccessPermissionRuleTImpl
