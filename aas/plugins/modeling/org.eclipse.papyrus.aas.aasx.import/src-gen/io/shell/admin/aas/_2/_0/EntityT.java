/**
 */
package io.shell.admin.aas._2._0;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Entity T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.EntityT#getStatements <em>Statements</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.EntityT#getEntityType <em>Entity Type</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.EntityT#getAssetRef <em>Asset Ref</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getEntityT()
 * @model extendedMetaData="name='entity_t' kind='elementOnly'"
 * @generated
 */
public interface EntityT extends SubmodelElementAbstractT {
	/**
	 * Returns the value of the '<em><b>Statements</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Statements</em>' containment reference.
	 * @see #setStatements(SubmodelElementsT)
	 * @see io.shell.admin.aas._2._0._0Package#getEntityT_Statements()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='statements' namespace='##targetNamespace'"
	 * @generated
	 */
	SubmodelElementsT getStatements();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.EntityT#getStatements <em>Statements</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Statements</em>' containment reference.
	 * @see #getStatements()
	 * @generated
	 */
	void setStatements(SubmodelElementsT value);

	/**
	 * Returns the value of the '<em><b>Entity Type</b></em>' attribute.
	 * The literals are from the enumeration {@link io.shell.admin.aas._2._0.EntityTypeType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entity Type</em>' attribute.
	 * @see io.shell.admin.aas._2._0.EntityTypeType
	 * @see #isSetEntityType()
	 * @see #unsetEntityType()
	 * @see #setEntityType(EntityTypeType)
	 * @see io.shell.admin.aas._2._0._0Package#getEntityT_EntityType()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='element' name='entityType' namespace='##targetNamespace'"
	 * @generated
	 */
	EntityTypeType getEntityType();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.EntityT#getEntityType <em>Entity Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entity Type</em>' attribute.
	 * @see io.shell.admin.aas._2._0.EntityTypeType
	 * @see #isSetEntityType()
	 * @see #unsetEntityType()
	 * @see #getEntityType()
	 * @generated
	 */
	void setEntityType(EntityTypeType value);

	/**
	 * Unsets the value of the '{@link io.shell.admin.aas._2._0.EntityT#getEntityType <em>Entity Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetEntityType()
	 * @see #getEntityType()
	 * @see #setEntityType(EntityTypeType)
	 * @generated
	 */
	void unsetEntityType();

	/**
	 * Returns whether the value of the '{@link io.shell.admin.aas._2._0.EntityT#getEntityType <em>Entity Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Entity Type</em>' attribute is set.
	 * @see #unsetEntityType()
	 * @see #getEntityType()
	 * @see #setEntityType(EntityTypeType)
	 * @generated
	 */
	boolean isSetEntityType();

	/**
	 * Returns the value of the '<em><b>Asset Ref</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asset Ref</em>' containment reference.
	 * @see #setAssetRef(ReferenceT)
	 * @see io.shell.admin.aas._2._0._0Package#getEntityT_AssetRef()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='assetRef' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getAssetRef();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.EntityT#getAssetRef <em>Asset Ref</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Asset Ref</em>' containment reference.
	 * @see #getAssetRef()
	 * @generated
	 */
	void setAssetRef(ReferenceT value);

} // EntityT
