/**
 */
package io.shell.admin.iec61360._2._0.util;

import io.shell.admin.iec61360._2._0.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see io.shell.admin.iec61360._2._0._0Package
 * @generated
 */
public class _0Switch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static _0Package modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public _0Switch() {
		if (modelPackage == null) {
			modelPackage = _0Package.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case _0Package.CODE_T: {
				CodeT codeT = (CodeT)theEObject;
				T result = caseCodeT(codeT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.DATA_SPECIFICATION_IEC61630T: {
				DataSpecificationIEC61630T dataSpecificationIEC61630T = (DataSpecificationIEC61630T)theEObject;
				T result = caseDataSpecificationIEC61630T(dataSpecificationIEC61630T);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.KEYS_T: {
				KeysT keysT = (KeysT)theEObject;
				T result = caseKeysT(keysT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.KEY_T: {
				KeyT keyT = (KeyT)theEObject;
				T result = caseKeyT(keyT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.LANG_STRING_SET_T: {
				LangStringSetT langStringSetT = (LangStringSetT)theEObject;
				T result = caseLangStringSetT(langStringSetT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.LANG_STRING_T: {
				LangStringT langStringT = (LangStringT)theEObject;
				T result = caseLangStringT(langStringT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.REFERENCE_T: {
				ReferenceT referenceT = (ReferenceT)theEObject;
				T result = caseReferenceT(referenceT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.VALUE_DATA_TYPE_T: {
				ValueDataTypeT valueDataTypeT = (ValueDataTypeT)theEObject;
				T result = caseValueDataTypeT(valueDataTypeT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.VALUE_LIST_T: {
				ValueListT valueListT = (ValueListT)theEObject;
				T result = caseValueListT(valueListT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.VALUE_REFERENCE_PAIR_T: {
				ValueReferencePairT valueReferencePairT = (ValueReferencePairT)theEObject;
				T result = caseValueReferencePairT(valueReferencePairT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case _0Package.DOCUMENT_ROOT: {
				DocumentRoot documentRoot = (DocumentRoot)theEObject;
				T result = caseDocumentRoot(documentRoot);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Code T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Code T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCodeT(CodeT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Specification IEC61630T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Specification IEC61630T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataSpecificationIEC61630T(DataSpecificationIEC61630T object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Keys T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Keys T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseKeysT(KeysT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Key T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Key T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseKeyT(KeyT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lang String Set T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lang String Set T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLangStringSetT(LangStringSetT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lang String T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lang String T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLangStringT(LangStringT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reference T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reference T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReferenceT(ReferenceT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Value Data Type T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Value Data Type T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseValueDataTypeT(ValueDataTypeT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Value List T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Value List T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseValueListT(ValueListT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Value Reference Pair T</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Value Reference Pair T</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseValueReferencePairT(ValueReferencePairT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Document Root</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Document Root</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDocumentRoot(DocumentRoot object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //_0Switch
