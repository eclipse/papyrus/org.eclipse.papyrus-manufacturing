/**
 */
package io.shell.admin.iec61360._2._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Code T</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see io.shell.admin.iec61360._2._0._0Package#getCodeT()
 * @model extendedMetaData="name='code_t' kind='empty'"
 * @generated
 */
public interface CodeT extends EObject {
} // CodeT
