/**
 */
package io.shell.admin.aas.abac._2._0.impl;

import io.shell.admin.aas.abac._2._0.CertificateAbstractT;
import io.shell.admin.aas.abac._2._0._0Package;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Certificate Abstract T</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CertificateAbstractTImpl extends MinimalEObjectImpl.Container implements CertificateAbstractT {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CertificateAbstractTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.CERTIFICATE_ABSTRACT_T;
	}

} //CertificateAbstractTImpl
