/**
 */
package io.shell.admin.aas.abac._2._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Policy Enforcement Point T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.PolicyEnforcementPointT#isExternalPolicyEnforcementPoint <em>External Policy Enforcement Point</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas.abac._2._0._0Package#getPolicyEnforcementPointT()
 * @model extendedMetaData="name='policyEnforcementPoint_t' kind='elementOnly'"
 * @generated
 */
public interface PolicyEnforcementPointT extends EObject {
	/**
	 * Returns the value of the '<em><b>External Policy Enforcement Point</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Policy Enforcement Point</em>' attribute.
	 * @see #isSetExternalPolicyEnforcementPoint()
	 * @see #unsetExternalPolicyEnforcementPoint()
	 * @see #setExternalPolicyEnforcementPoint(boolean)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getPolicyEnforcementPointT_ExternalPolicyEnforcementPoint()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean" required="true"
	 *        extendedMetaData="kind='element' name='externalPolicyEnforcementPoint' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isExternalPolicyEnforcementPoint();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.PolicyEnforcementPointT#isExternalPolicyEnforcementPoint <em>External Policy Enforcement Point</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>External Policy Enforcement Point</em>' attribute.
	 * @see #isSetExternalPolicyEnforcementPoint()
	 * @see #unsetExternalPolicyEnforcementPoint()
	 * @see #isExternalPolicyEnforcementPoint()
	 * @generated
	 */
	void setExternalPolicyEnforcementPoint(boolean value);

	/**
	 * Unsets the value of the '{@link io.shell.admin.aas.abac._2._0.PolicyEnforcementPointT#isExternalPolicyEnforcementPoint <em>External Policy Enforcement Point</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExternalPolicyEnforcementPoint()
	 * @see #isExternalPolicyEnforcementPoint()
	 * @see #setExternalPolicyEnforcementPoint(boolean)
	 * @generated
	 */
	void unsetExternalPolicyEnforcementPoint();

	/**
	 * Returns whether the value of the '{@link io.shell.admin.aas.abac._2._0.PolicyEnforcementPointT#isExternalPolicyEnforcementPoint <em>External Policy Enforcement Point</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>External Policy Enforcement Point</em>' attribute is set.
	 * @see #unsetExternalPolicyEnforcementPoint()
	 * @see #isExternalPolicyEnforcementPoint()
	 * @see #setExternalPolicyEnforcementPoint(boolean)
	 * @generated
	 */
	boolean isSetExternalPolicyEnforcementPoint();

} // PolicyEnforcementPointT
