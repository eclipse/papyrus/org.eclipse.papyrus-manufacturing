/**
 */
package io.shell.admin.aas._2._0.impl;

import io.shell.admin.aas._2._0.BlobT;
import io.shell.admin.aas._2._0.DataElementAbstractT;
import io.shell.admin.aas._2._0.FileT;
import io.shell.admin.aas._2._0.MultiLanguagePropertyT;
import io.shell.admin.aas._2._0.PropertyT;
import io.shell.admin.aas._2._0.RangeT;
import io.shell.admin.aas._2._0.ReferenceElementT;
import io.shell.admin.aas._2._0._0Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Element Abstract T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.impl.DataElementAbstractTImpl#getMultiLanguageProperty <em>Multi Language Property</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.DataElementAbstractTImpl#getProperty <em>Property</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.DataElementAbstractTImpl#getRange <em>Range</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.DataElementAbstractTImpl#getBlob <em>Blob</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.DataElementAbstractTImpl#getFile <em>File</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.impl.DataElementAbstractTImpl#getReferenceElement <em>Reference Element</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataElementAbstractTImpl extends MinimalEObjectImpl.Container implements DataElementAbstractT {
	/**
	 * The cached value of the '{@link #getMultiLanguageProperty() <em>Multi Language Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMultiLanguageProperty()
	 * @generated
	 * @ordered
	 */
	protected MultiLanguagePropertyT multiLanguageProperty;

	/**
	 * The cached value of the '{@link #getProperty() <em>Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperty()
	 * @generated
	 * @ordered
	 */
	protected PropertyT property;

	/**
	 * The cached value of the '{@link #getRange() <em>Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRange()
	 * @generated
	 * @ordered
	 */
	protected RangeT range;

	/**
	 * The cached value of the '{@link #getBlob() <em>Blob</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBlob()
	 * @generated
	 * @ordered
	 */
	protected BlobT blob;

	/**
	 * The cached value of the '{@link #getFile() <em>File</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFile()
	 * @generated
	 * @ordered
	 */
	protected FileT file;

	/**
	 * The cached value of the '{@link #getReferenceElement() <em>Reference Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenceElement()
	 * @generated
	 * @ordered
	 */
	protected ReferenceElementT referenceElement;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataElementAbstractTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.DATA_ELEMENT_ABSTRACT_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiLanguagePropertyT getMultiLanguageProperty() {
		return multiLanguageProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMultiLanguageProperty(MultiLanguagePropertyT newMultiLanguageProperty, NotificationChain msgs) {
		MultiLanguagePropertyT oldMultiLanguageProperty = multiLanguageProperty;
		multiLanguageProperty = newMultiLanguageProperty;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.DATA_ELEMENT_ABSTRACT_T__MULTI_LANGUAGE_PROPERTY, oldMultiLanguageProperty, newMultiLanguageProperty);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMultiLanguageProperty(MultiLanguagePropertyT newMultiLanguageProperty) {
		if (newMultiLanguageProperty != multiLanguageProperty) {
			NotificationChain msgs = null;
			if (multiLanguageProperty != null)
				msgs = ((InternalEObject)multiLanguageProperty).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_ELEMENT_ABSTRACT_T__MULTI_LANGUAGE_PROPERTY, null, msgs);
			if (newMultiLanguageProperty != null)
				msgs = ((InternalEObject)newMultiLanguageProperty).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_ELEMENT_ABSTRACT_T__MULTI_LANGUAGE_PROPERTY, null, msgs);
			msgs = basicSetMultiLanguageProperty(newMultiLanguageProperty, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.DATA_ELEMENT_ABSTRACT_T__MULTI_LANGUAGE_PROPERTY, newMultiLanguageProperty, newMultiLanguageProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyT getProperty() {
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProperty(PropertyT newProperty, NotificationChain msgs) {
		PropertyT oldProperty = property;
		property = newProperty;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.DATA_ELEMENT_ABSTRACT_T__PROPERTY, oldProperty, newProperty);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProperty(PropertyT newProperty) {
		if (newProperty != property) {
			NotificationChain msgs = null;
			if (property != null)
				msgs = ((InternalEObject)property).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_ELEMENT_ABSTRACT_T__PROPERTY, null, msgs);
			if (newProperty != null)
				msgs = ((InternalEObject)newProperty).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_ELEMENT_ABSTRACT_T__PROPERTY, null, msgs);
			msgs = basicSetProperty(newProperty, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.DATA_ELEMENT_ABSTRACT_T__PROPERTY, newProperty, newProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RangeT getRange() {
		return range;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRange(RangeT newRange, NotificationChain msgs) {
		RangeT oldRange = range;
		range = newRange;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.DATA_ELEMENT_ABSTRACT_T__RANGE, oldRange, newRange);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRange(RangeT newRange) {
		if (newRange != range) {
			NotificationChain msgs = null;
			if (range != null)
				msgs = ((InternalEObject)range).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_ELEMENT_ABSTRACT_T__RANGE, null, msgs);
			if (newRange != null)
				msgs = ((InternalEObject)newRange).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_ELEMENT_ABSTRACT_T__RANGE, null, msgs);
			msgs = basicSetRange(newRange, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.DATA_ELEMENT_ABSTRACT_T__RANGE, newRange, newRange));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlobT getBlob() {
		return blob;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBlob(BlobT newBlob, NotificationChain msgs) {
		BlobT oldBlob = blob;
		blob = newBlob;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.DATA_ELEMENT_ABSTRACT_T__BLOB, oldBlob, newBlob);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBlob(BlobT newBlob) {
		if (newBlob != blob) {
			NotificationChain msgs = null;
			if (blob != null)
				msgs = ((InternalEObject)blob).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_ELEMENT_ABSTRACT_T__BLOB, null, msgs);
			if (newBlob != null)
				msgs = ((InternalEObject)newBlob).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_ELEMENT_ABSTRACT_T__BLOB, null, msgs);
			msgs = basicSetBlob(newBlob, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.DATA_ELEMENT_ABSTRACT_T__BLOB, newBlob, newBlob));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FileT getFile() {
		return file;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFile(FileT newFile, NotificationChain msgs) {
		FileT oldFile = file;
		file = newFile;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.DATA_ELEMENT_ABSTRACT_T__FILE, oldFile, newFile);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFile(FileT newFile) {
		if (newFile != file) {
			NotificationChain msgs = null;
			if (file != null)
				msgs = ((InternalEObject)file).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_ELEMENT_ABSTRACT_T__FILE, null, msgs);
			if (newFile != null)
				msgs = ((InternalEObject)newFile).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_ELEMENT_ABSTRACT_T__FILE, null, msgs);
			msgs = basicSetFile(newFile, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.DATA_ELEMENT_ABSTRACT_T__FILE, newFile, newFile));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceElementT getReferenceElement() {
		return referenceElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReferenceElement(ReferenceElementT newReferenceElement, NotificationChain msgs) {
		ReferenceElementT oldReferenceElement = referenceElement;
		referenceElement = newReferenceElement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.DATA_ELEMENT_ABSTRACT_T__REFERENCE_ELEMENT, oldReferenceElement, newReferenceElement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferenceElement(ReferenceElementT newReferenceElement) {
		if (newReferenceElement != referenceElement) {
			NotificationChain msgs = null;
			if (referenceElement != null)
				msgs = ((InternalEObject)referenceElement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_ELEMENT_ABSTRACT_T__REFERENCE_ELEMENT, null, msgs);
			if (newReferenceElement != null)
				msgs = ((InternalEObject)newReferenceElement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.DATA_ELEMENT_ABSTRACT_T__REFERENCE_ELEMENT, null, msgs);
			msgs = basicSetReferenceElement(newReferenceElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.DATA_ELEMENT_ABSTRACT_T__REFERENCE_ELEMENT, newReferenceElement, newReferenceElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.DATA_ELEMENT_ABSTRACT_T__MULTI_LANGUAGE_PROPERTY:
				return basicSetMultiLanguageProperty(null, msgs);
			case _0Package.DATA_ELEMENT_ABSTRACT_T__PROPERTY:
				return basicSetProperty(null, msgs);
			case _0Package.DATA_ELEMENT_ABSTRACT_T__RANGE:
				return basicSetRange(null, msgs);
			case _0Package.DATA_ELEMENT_ABSTRACT_T__BLOB:
				return basicSetBlob(null, msgs);
			case _0Package.DATA_ELEMENT_ABSTRACT_T__FILE:
				return basicSetFile(null, msgs);
			case _0Package.DATA_ELEMENT_ABSTRACT_T__REFERENCE_ELEMENT:
				return basicSetReferenceElement(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.DATA_ELEMENT_ABSTRACT_T__MULTI_LANGUAGE_PROPERTY:
				return getMultiLanguageProperty();
			case _0Package.DATA_ELEMENT_ABSTRACT_T__PROPERTY:
				return getProperty();
			case _0Package.DATA_ELEMENT_ABSTRACT_T__RANGE:
				return getRange();
			case _0Package.DATA_ELEMENT_ABSTRACT_T__BLOB:
				return getBlob();
			case _0Package.DATA_ELEMENT_ABSTRACT_T__FILE:
				return getFile();
			case _0Package.DATA_ELEMENT_ABSTRACT_T__REFERENCE_ELEMENT:
				return getReferenceElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.DATA_ELEMENT_ABSTRACT_T__MULTI_LANGUAGE_PROPERTY:
				setMultiLanguageProperty((MultiLanguagePropertyT)newValue);
				return;
			case _0Package.DATA_ELEMENT_ABSTRACT_T__PROPERTY:
				setProperty((PropertyT)newValue);
				return;
			case _0Package.DATA_ELEMENT_ABSTRACT_T__RANGE:
				setRange((RangeT)newValue);
				return;
			case _0Package.DATA_ELEMENT_ABSTRACT_T__BLOB:
				setBlob((BlobT)newValue);
				return;
			case _0Package.DATA_ELEMENT_ABSTRACT_T__FILE:
				setFile((FileT)newValue);
				return;
			case _0Package.DATA_ELEMENT_ABSTRACT_T__REFERENCE_ELEMENT:
				setReferenceElement((ReferenceElementT)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.DATA_ELEMENT_ABSTRACT_T__MULTI_LANGUAGE_PROPERTY:
				setMultiLanguageProperty((MultiLanguagePropertyT)null);
				return;
			case _0Package.DATA_ELEMENT_ABSTRACT_T__PROPERTY:
				setProperty((PropertyT)null);
				return;
			case _0Package.DATA_ELEMENT_ABSTRACT_T__RANGE:
				setRange((RangeT)null);
				return;
			case _0Package.DATA_ELEMENT_ABSTRACT_T__BLOB:
				setBlob((BlobT)null);
				return;
			case _0Package.DATA_ELEMENT_ABSTRACT_T__FILE:
				setFile((FileT)null);
				return;
			case _0Package.DATA_ELEMENT_ABSTRACT_T__REFERENCE_ELEMENT:
				setReferenceElement((ReferenceElementT)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.DATA_ELEMENT_ABSTRACT_T__MULTI_LANGUAGE_PROPERTY:
				return multiLanguageProperty != null;
			case _0Package.DATA_ELEMENT_ABSTRACT_T__PROPERTY:
				return property != null;
			case _0Package.DATA_ELEMENT_ABSTRACT_T__RANGE:
				return range != null;
			case _0Package.DATA_ELEMENT_ABSTRACT_T__BLOB:
				return blob != null;
			case _0Package.DATA_ELEMENT_ABSTRACT_T__FILE:
				return file != null;
			case _0Package.DATA_ELEMENT_ABSTRACT_T__REFERENCE_ELEMENT:
				return referenceElement != null;
		}
		return super.eIsSet(featureID);
	}

} //DataElementAbstractTImpl
