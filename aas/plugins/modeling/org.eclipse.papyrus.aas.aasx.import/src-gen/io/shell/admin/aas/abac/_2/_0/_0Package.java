/**
 */
package io.shell.admin.aas.abac._2._0;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see io.shell.admin.aas.abac._2._0._0Factory
 * @model kind="package"
 * @generated
 */
public interface _0Package extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "_0";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.admin-shell.io/aas/abac/2/0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "_0";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	_0Package eINSTANCE = io.shell.admin.aas.abac._2._0.impl._0PackageImpl.init();

	/**
	 * The meta object id for the '{@link io.shell.admin.aas.abac._2._0.impl.AccessControlPolicyPointsTImpl <em>Access Control Policy Points T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas.abac._2._0.impl.AccessControlPolicyPointsTImpl
	 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getAccessControlPolicyPointsT()
	 * @generated
	 */
	int ACCESS_CONTROL_POLICY_POINTS_T = 0;

	/**
	 * The feature id for the '<em><b>Policy Administration Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONTROL_POLICY_POINTS_T__POLICY_ADMINISTRATION_POINT = 0;

	/**
	 * The feature id for the '<em><b>Policy Decision Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONTROL_POLICY_POINTS_T__POLICY_DECISION_POINT = 1;

	/**
	 * The feature id for the '<em><b>Policy Enforcement Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONTROL_POLICY_POINTS_T__POLICY_ENFORCEMENT_POINT = 2;

	/**
	 * The feature id for the '<em><b>Policy Information Points</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONTROL_POLICY_POINTS_T__POLICY_INFORMATION_POINTS = 3;

	/**
	 * The number of structural features of the '<em>Access Control Policy Points T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONTROL_POLICY_POINTS_T_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Access Control Policy Points T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONTROL_POLICY_POINTS_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas.abac._2._0.impl.AccessControlTImpl <em>Access Control T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas.abac._2._0.impl.AccessControlTImpl
	 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getAccessControlT()
	 * @generated
	 */
	int ACCESS_CONTROL_T = 1;

	/**
	 * The feature id for the '<em><b>Selectable Subject Attributes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONTROL_T__SELECTABLE_SUBJECT_ATTRIBUTES = 0;

	/**
	 * The feature id for the '<em><b>Default Subject Attributes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONTROL_T__DEFAULT_SUBJECT_ATTRIBUTES = 1;

	/**
	 * The feature id for the '<em><b>Selectable Permissions</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONTROL_T__SELECTABLE_PERMISSIONS = 2;

	/**
	 * The feature id for the '<em><b>Default Permissions</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONTROL_T__DEFAULT_PERMISSIONS = 3;

	/**
	 * The feature id for the '<em><b>Selectable Environment Attributes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONTROL_T__SELECTABLE_ENVIRONMENT_ATTRIBUTES = 4;

	/**
	 * The feature id for the '<em><b>Default Environment Attributes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONTROL_T__DEFAULT_ENVIRONMENT_ATTRIBUTES = 5;

	/**
	 * The feature id for the '<em><b>Access Permission Rules</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONTROL_T__ACCESS_PERMISSION_RULES = 6;

	/**
	 * The number of structural features of the '<em>Access Control T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONTROL_T_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Access Control T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_CONTROL_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas.abac._2._0.impl.AccessPermissionRulesTImpl <em>Access Permission Rules T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas.abac._2._0.impl.AccessPermissionRulesTImpl
	 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getAccessPermissionRulesT()
	 * @generated
	 */
	int ACCESS_PERMISSION_RULES_T = 2;

	/**
	 * The feature id for the '<em><b>Access Permission Rule</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_PERMISSION_RULES_T__ACCESS_PERMISSION_RULE = 0;

	/**
	 * The number of structural features of the '<em>Access Permission Rules T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_PERMISSION_RULES_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Access Permission Rules T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_PERMISSION_RULES_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas.abac._2._0.impl.AccessPermissionRuleTImpl <em>Access Permission Rule T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas.abac._2._0.impl.AccessPermissionRuleTImpl
	 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getAccessPermissionRuleT()
	 * @generated
	 */
	int ACCESS_PERMISSION_RULE_T = 3;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_PERMISSION_RULE_T__QUALIFIER = 0;

	/**
	 * The feature id for the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_PERMISSION_RULE_T__ID_SHORT = 1;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_PERMISSION_RULE_T__CATEGORY = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_PERMISSION_RULE_T__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_PERMISSION_RULE_T__PARENT = 4;

	/**
	 * The feature id for the '<em><b>Target Subject Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_PERMISSION_RULE_T__TARGET_SUBJECT_ATTRIBUTES = 5;

	/**
	 * The feature id for the '<em><b>Permissions Per Object</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_PERMISSION_RULE_T__PERMISSIONS_PER_OBJECT = 6;

	/**
	 * The number of structural features of the '<em>Access Permission Rule T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_PERMISSION_RULE_T_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Access Permission Rule T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_PERMISSION_RULE_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas.abac._2._0.impl.CertificateAbstractTImpl <em>Certificate Abstract T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas.abac._2._0.impl.CertificateAbstractTImpl
	 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getCertificateAbstractT()
	 * @generated
	 */
	int CERTIFICATE_ABSTRACT_T = 5;

	/**
	 * The number of structural features of the '<em>Certificate Abstract T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFICATE_ABSTRACT_T_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Certificate Abstract T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFICATE_ABSTRACT_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas.abac._2._0.impl.BlobCertificateTImpl <em>Blob Certificate T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas.abac._2._0.impl.BlobCertificateTImpl
	 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getBlobCertificateT()
	 * @generated
	 */
	int BLOB_CERTIFICATE_T = 4;

	/**
	 * The feature id for the '<em><b>Blob Certificate</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOB_CERTIFICATE_T__BLOB_CERTIFICATE = CERTIFICATE_ABSTRACT_T_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Contained Extensions</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOB_CERTIFICATE_T__CONTAINED_EXTENSIONS = CERTIFICATE_ABSTRACT_T_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Last Certificate</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOB_CERTIFICATE_T__LAST_CERTIFICATE = CERTIFICATE_ABSTRACT_T_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Blob Certificate T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOB_CERTIFICATE_T_FEATURE_COUNT = CERTIFICATE_ABSTRACT_T_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Blob Certificate T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOB_CERTIFICATE_T_OPERATION_COUNT = CERTIFICATE_ABSTRACT_T_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas.abac._2._0.impl.CertificatesTImpl <em>Certificates T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas.abac._2._0.impl.CertificatesTImpl
	 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getCertificatesT()
	 * @generated
	 */
	int CERTIFICATES_T = 6;

	/**
	 * The feature id for the '<em><b>Certificate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFICATES_T__CERTIFICATE = 0;

	/**
	 * The number of structural features of the '<em>Certificates T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFICATES_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Certificates T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFICATES_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas.abac._2._0.impl.CertificateTImpl <em>Certificate T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas.abac._2._0.impl.CertificateTImpl
	 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getCertificateT()
	 * @generated
	 */
	int CERTIFICATE_T = 7;

	/**
	 * The feature id for the '<em><b>Blob Certificate</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFICATE_T__BLOB_CERTIFICATE = 0;

	/**
	 * The number of structural features of the '<em>Certificate T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFICATE_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Certificate T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFICATE_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas.abac._2._0.impl.ContainedExtensionsTImpl <em>Contained Extensions T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas.abac._2._0.impl.ContainedExtensionsTImpl
	 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getContainedExtensionsT()
	 * @generated
	 */
	int CONTAINED_EXTENSIONS_T = 8;

	/**
	 * The feature id for the '<em><b>Contained Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_EXTENSIONS_T__CONTAINED_EXTENSION = 0;

	/**
	 * The number of structural features of the '<em>Contained Extensions T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_EXTENSIONS_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Contained Extensions T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_EXTENSIONS_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas.abac._2._0.impl.InternalInformationPointsImpl <em>Internal Information Points</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas.abac._2._0.impl.InternalInformationPointsImpl
	 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getInternalInformationPoints()
	 * @generated
	 */
	int INTERNAL_INFORMATION_POINTS = 9;

	/**
	 * The feature id for the '<em><b>Internal Information Point</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_INFORMATION_POINTS__INTERNAL_INFORMATION_POINT = 0;

	/**
	 * The number of structural features of the '<em>Internal Information Points</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_INFORMATION_POINTS_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Internal Information Points</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERNAL_INFORMATION_POINTS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas.abac._2._0.impl.ObjectAttributesTImpl <em>Object Attributes T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas.abac._2._0.impl.ObjectAttributesTImpl
	 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getObjectAttributesT()
	 * @generated
	 */
	int OBJECT_ATTRIBUTES_T = 10;

	/**
	 * The feature id for the '<em><b>Object Attribute</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ATTRIBUTES_T__OBJECT_ATTRIBUTE = 0;

	/**
	 * The number of structural features of the '<em>Object Attributes T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ATTRIBUTES_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Object Attributes T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_ATTRIBUTES_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas.abac._2._0.impl.PermissionPerObjectTImpl <em>Permission Per Object T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas.abac._2._0.impl.PermissionPerObjectTImpl
	 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getPermissionPerObjectT()
	 * @generated
	 */
	int PERMISSION_PER_OBJECT_T = 11;

	/**
	 * The feature id for the '<em><b>Object</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSION_PER_OBJECT_T__OBJECT = 0;

	/**
	 * The feature id for the '<em><b>Target Object Attributes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSION_PER_OBJECT_T__TARGET_OBJECT_ATTRIBUTES = 1;

	/**
	 * The feature id for the '<em><b>Permissions</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSION_PER_OBJECT_T__PERMISSIONS = 2;

	/**
	 * The number of structural features of the '<em>Permission Per Object T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSION_PER_OBJECT_T_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Permission Per Object T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSION_PER_OBJECT_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas.abac._2._0.impl.PermissionsTImpl <em>Permissions T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas.abac._2._0.impl.PermissionsTImpl
	 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getPermissionsT()
	 * @generated
	 */
	int PERMISSIONS_T = 12;

	/**
	 * The feature id for the '<em><b>Permission</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSIONS_T__PERMISSION = 0;

	/**
	 * The feature id for the '<em><b>Kind Of Permission</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSIONS_T__KIND_OF_PERMISSION = 1;

	/**
	 * The number of structural features of the '<em>Permissions T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSIONS_T_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Permissions T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSIONS_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas.abac._2._0.impl.PolicyAdministrationPointTImpl <em>Policy Administration Point T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas.abac._2._0.impl.PolicyAdministrationPointTImpl
	 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getPolicyAdministrationPointT()
	 * @generated
	 */
	int POLICY_ADMINISTRATION_POINT_T = 13;

	/**
	 * The feature id for the '<em><b>Local Access Control</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY_ADMINISTRATION_POINT_T__LOCAL_ACCESS_CONTROL = 0;

	/**
	 * The feature id for the '<em><b>External Access Control</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY_ADMINISTRATION_POINT_T__EXTERNAL_ACCESS_CONTROL = 1;

	/**
	 * The number of structural features of the '<em>Policy Administration Point T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY_ADMINISTRATION_POINT_T_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Policy Administration Point T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY_ADMINISTRATION_POINT_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas.abac._2._0.impl.PolicyDecisionPointTImpl <em>Policy Decision Point T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas.abac._2._0.impl.PolicyDecisionPointTImpl
	 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getPolicyDecisionPointT()
	 * @generated
	 */
	int POLICY_DECISION_POINT_T = 14;

	/**
	 * The feature id for the '<em><b>External Policy Decision Point</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY_DECISION_POINT_T__EXTERNAL_POLICY_DECISION_POINT = 0;

	/**
	 * The number of structural features of the '<em>Policy Decision Point T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY_DECISION_POINT_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Policy Decision Point T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY_DECISION_POINT_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas.abac._2._0.impl.PolicyEnforcementPointTImpl <em>Policy Enforcement Point T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas.abac._2._0.impl.PolicyEnforcementPointTImpl
	 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getPolicyEnforcementPointT()
	 * @generated
	 */
	int POLICY_ENFORCEMENT_POINT_T = 15;

	/**
	 * The feature id for the '<em><b>External Policy Enforcement Point</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY_ENFORCEMENT_POINT_T__EXTERNAL_POLICY_ENFORCEMENT_POINT = 0;

	/**
	 * The number of structural features of the '<em>Policy Enforcement Point T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY_ENFORCEMENT_POINT_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Policy Enforcement Point T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY_ENFORCEMENT_POINT_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas.abac._2._0.impl.PolicyInformationPointsTImpl <em>Policy Information Points T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas.abac._2._0.impl.PolicyInformationPointsTImpl
	 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getPolicyInformationPointsT()
	 * @generated
	 */
	int POLICY_INFORMATION_POINTS_T = 16;

	/**
	 * The feature id for the '<em><b>External Information Points</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY_INFORMATION_POINTS_T__EXTERNAL_INFORMATION_POINTS = 0;

	/**
	 * The feature id for the '<em><b>Internal Information Points</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY_INFORMATION_POINTS_T__INTERNAL_INFORMATION_POINTS = 1;

	/**
	 * The number of structural features of the '<em>Policy Information Points T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY_INFORMATION_POINTS_T_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Policy Information Points T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY_INFORMATION_POINTS_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas.abac._2._0.impl.SecurityTImpl <em>Security T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas.abac._2._0.impl.SecurityTImpl
	 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getSecurityT()
	 * @generated
	 */
	int SECURITY_T = 17;

	/**
	 * The feature id for the '<em><b>Access Control Policy Points</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_T__ACCESS_CONTROL_POLICY_POINTS = 0;

	/**
	 * The feature id for the '<em><b>Certificates</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_T__CERTIFICATES = 1;

	/**
	 * The feature id for the '<em><b>Required Certificate Extensions</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_T__REQUIRED_CERTIFICATE_EXTENSIONS = 2;

	/**
	 * The number of structural features of the '<em>Security T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_T_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Security T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas.abac._2._0.impl.SubjectAttributesTImpl <em>Subject Attributes T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas.abac._2._0.impl.SubjectAttributesTImpl
	 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getSubjectAttributesT()
	 * @generated
	 */
	int SUBJECT_ATTRIBUTES_T = 18;

	/**
	 * The feature id for the '<em><b>Subject Attribute</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBJECT_ATTRIBUTES_T__SUBJECT_ATTRIBUTE = 0;

	/**
	 * The number of structural features of the '<em>Subject Attributes T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBJECT_ATTRIBUTES_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Subject Attributes T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBJECT_ATTRIBUTES_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas.abac._2._0.impl.DocumentRootImpl <em>Document Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas.abac._2._0.impl.DocumentRootImpl
	 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getDocumentRoot()
	 * @generated
	 */
	int DOCUMENT_ROOT = 19;

	/**
	 * The feature id for the '<em><b>Mixed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__MIXED = 0;

	/**
	 * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

	/**
	 * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

	/**
	 * The feature id for the '<em><b>Internal Information Points</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__INTERNAL_INFORMATION_POINTS = 3;

	/**
	 * The number of structural features of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.aas.abac._2._0.PermissionKind <em>Permission Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas.abac._2._0.PermissionKind
	 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getPermissionKind()
	 * @generated
	 */
	int PERMISSION_KIND = 20;

	/**
	 * The meta object id for the '<em>Permission Kind Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.aas.abac._2._0.PermissionKind
	 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getPermissionKindObject()
	 * @generated
	 */
	int PERMISSION_KIND_OBJECT = 21;


	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas.abac._2._0.AccessControlPolicyPointsT <em>Access Control Policy Points T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Access Control Policy Points T</em>'.
	 * @see io.shell.admin.aas.abac._2._0.AccessControlPolicyPointsT
	 * @generated
	 */
	EClass getAccessControlPolicyPointsT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.AccessControlPolicyPointsT#getPolicyAdministrationPoint <em>Policy Administration Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Policy Administration Point</em>'.
	 * @see io.shell.admin.aas.abac._2._0.AccessControlPolicyPointsT#getPolicyAdministrationPoint()
	 * @see #getAccessControlPolicyPointsT()
	 * @generated
	 */
	EReference getAccessControlPolicyPointsT_PolicyAdministrationPoint();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.AccessControlPolicyPointsT#getPolicyDecisionPoint <em>Policy Decision Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Policy Decision Point</em>'.
	 * @see io.shell.admin.aas.abac._2._0.AccessControlPolicyPointsT#getPolicyDecisionPoint()
	 * @see #getAccessControlPolicyPointsT()
	 * @generated
	 */
	EReference getAccessControlPolicyPointsT_PolicyDecisionPoint();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.AccessControlPolicyPointsT#getPolicyEnforcementPoint <em>Policy Enforcement Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Policy Enforcement Point</em>'.
	 * @see io.shell.admin.aas.abac._2._0.AccessControlPolicyPointsT#getPolicyEnforcementPoint()
	 * @see #getAccessControlPolicyPointsT()
	 * @generated
	 */
	EReference getAccessControlPolicyPointsT_PolicyEnforcementPoint();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.AccessControlPolicyPointsT#getPolicyInformationPoints <em>Policy Information Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Policy Information Points</em>'.
	 * @see io.shell.admin.aas.abac._2._0.AccessControlPolicyPointsT#getPolicyInformationPoints()
	 * @see #getAccessControlPolicyPointsT()
	 * @generated
	 */
	EReference getAccessControlPolicyPointsT_PolicyInformationPoints();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas.abac._2._0.AccessControlT <em>Access Control T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Access Control T</em>'.
	 * @see io.shell.admin.aas.abac._2._0.AccessControlT
	 * @generated
	 */
	EClass getAccessControlT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.AccessControlT#getSelectableSubjectAttributes <em>Selectable Subject Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Selectable Subject Attributes</em>'.
	 * @see io.shell.admin.aas.abac._2._0.AccessControlT#getSelectableSubjectAttributes()
	 * @see #getAccessControlT()
	 * @generated
	 */
	EReference getAccessControlT_SelectableSubjectAttributes();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.AccessControlT#getDefaultSubjectAttributes <em>Default Subject Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Default Subject Attributes</em>'.
	 * @see io.shell.admin.aas.abac._2._0.AccessControlT#getDefaultSubjectAttributes()
	 * @see #getAccessControlT()
	 * @generated
	 */
	EReference getAccessControlT_DefaultSubjectAttributes();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.AccessControlT#getSelectablePermissions <em>Selectable Permissions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Selectable Permissions</em>'.
	 * @see io.shell.admin.aas.abac._2._0.AccessControlT#getSelectablePermissions()
	 * @see #getAccessControlT()
	 * @generated
	 */
	EReference getAccessControlT_SelectablePermissions();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.AccessControlT#getDefaultPermissions <em>Default Permissions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Default Permissions</em>'.
	 * @see io.shell.admin.aas.abac._2._0.AccessControlT#getDefaultPermissions()
	 * @see #getAccessControlT()
	 * @generated
	 */
	EReference getAccessControlT_DefaultPermissions();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.AccessControlT#getSelectableEnvironmentAttributes <em>Selectable Environment Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Selectable Environment Attributes</em>'.
	 * @see io.shell.admin.aas.abac._2._0.AccessControlT#getSelectableEnvironmentAttributes()
	 * @see #getAccessControlT()
	 * @generated
	 */
	EReference getAccessControlT_SelectableEnvironmentAttributes();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.AccessControlT#getDefaultEnvironmentAttributes <em>Default Environment Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Default Environment Attributes</em>'.
	 * @see io.shell.admin.aas.abac._2._0.AccessControlT#getDefaultEnvironmentAttributes()
	 * @see #getAccessControlT()
	 * @generated
	 */
	EReference getAccessControlT_DefaultEnvironmentAttributes();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.AccessControlT#getAccessPermissionRules <em>Access Permission Rules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Access Permission Rules</em>'.
	 * @see io.shell.admin.aas.abac._2._0.AccessControlT#getAccessPermissionRules()
	 * @see #getAccessControlT()
	 * @generated
	 */
	EReference getAccessControlT_AccessPermissionRules();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas.abac._2._0.AccessPermissionRulesT <em>Access Permission Rules T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Access Permission Rules T</em>'.
	 * @see io.shell.admin.aas.abac._2._0.AccessPermissionRulesT
	 * @generated
	 */
	EClass getAccessPermissionRulesT();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas.abac._2._0.AccessPermissionRulesT#getAccessPermissionRule <em>Access Permission Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Access Permission Rule</em>'.
	 * @see io.shell.admin.aas.abac._2._0.AccessPermissionRulesT#getAccessPermissionRule()
	 * @see #getAccessPermissionRulesT()
	 * @generated
	 */
	EReference getAccessPermissionRulesT_AccessPermissionRule();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas.abac._2._0.AccessPermissionRuleT <em>Access Permission Rule T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Access Permission Rule T</em>'.
	 * @see io.shell.admin.aas.abac._2._0.AccessPermissionRuleT
	 * @generated
	 */
	EClass getAccessPermissionRuleT();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas.abac._2._0.AccessPermissionRuleT#getQualifier <em>Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Qualifier</em>'.
	 * @see io.shell.admin.aas.abac._2._0.AccessPermissionRuleT#getQualifier()
	 * @see #getAccessPermissionRuleT()
	 * @generated
	 */
	EReference getAccessPermissionRuleT_Qualifier();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.AccessPermissionRuleT#getIdShort <em>Id Short</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Id Short</em>'.
	 * @see io.shell.admin.aas.abac._2._0.AccessPermissionRuleT#getIdShort()
	 * @see #getAccessPermissionRuleT()
	 * @generated
	 */
	EReference getAccessPermissionRuleT_IdShort();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas.abac._2._0.AccessPermissionRuleT#getCategory <em>Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Category</em>'.
	 * @see io.shell.admin.aas.abac._2._0.AccessPermissionRuleT#getCategory()
	 * @see #getAccessPermissionRuleT()
	 * @generated
	 */
	EAttribute getAccessPermissionRuleT_Category();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.AccessPermissionRuleT#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Description</em>'.
	 * @see io.shell.admin.aas.abac._2._0.AccessPermissionRuleT#getDescription()
	 * @see #getAccessPermissionRuleT()
	 * @generated
	 */
	EReference getAccessPermissionRuleT_Description();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.AccessPermissionRuleT#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parent</em>'.
	 * @see io.shell.admin.aas.abac._2._0.AccessPermissionRuleT#getParent()
	 * @see #getAccessPermissionRuleT()
	 * @generated
	 */
	EReference getAccessPermissionRuleT_Parent();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas.abac._2._0.AccessPermissionRuleT#getTargetSubjectAttributes <em>Target Subject Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Target Subject Attributes</em>'.
	 * @see io.shell.admin.aas.abac._2._0.AccessPermissionRuleT#getTargetSubjectAttributes()
	 * @see #getAccessPermissionRuleT()
	 * @generated
	 */
	EReference getAccessPermissionRuleT_TargetSubjectAttributes();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas.abac._2._0.AccessPermissionRuleT#getPermissionsPerObject <em>Permissions Per Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Permissions Per Object</em>'.
	 * @see io.shell.admin.aas.abac._2._0.AccessPermissionRuleT#getPermissionsPerObject()
	 * @see #getAccessPermissionRuleT()
	 * @generated
	 */
	EReference getAccessPermissionRuleT_PermissionsPerObject();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas.abac._2._0.BlobCertificateT <em>Blob Certificate T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Blob Certificate T</em>'.
	 * @see io.shell.admin.aas.abac._2._0.BlobCertificateT
	 * @generated
	 */
	EClass getBlobCertificateT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.BlobCertificateT#getBlobCertificate <em>Blob Certificate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Blob Certificate</em>'.
	 * @see io.shell.admin.aas.abac._2._0.BlobCertificateT#getBlobCertificate()
	 * @see #getBlobCertificateT()
	 * @generated
	 */
	EReference getBlobCertificateT_BlobCertificate();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.BlobCertificateT#getContainedExtensions <em>Contained Extensions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Contained Extensions</em>'.
	 * @see io.shell.admin.aas.abac._2._0.BlobCertificateT#getContainedExtensions()
	 * @see #getBlobCertificateT()
	 * @generated
	 */
	EReference getBlobCertificateT_ContainedExtensions();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas.abac._2._0.BlobCertificateT#isLastCertificate <em>Last Certificate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Certificate</em>'.
	 * @see io.shell.admin.aas.abac._2._0.BlobCertificateT#isLastCertificate()
	 * @see #getBlobCertificateT()
	 * @generated
	 */
	EAttribute getBlobCertificateT_LastCertificate();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas.abac._2._0.CertificateAbstractT <em>Certificate Abstract T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Certificate Abstract T</em>'.
	 * @see io.shell.admin.aas.abac._2._0.CertificateAbstractT
	 * @generated
	 */
	EClass getCertificateAbstractT();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas.abac._2._0.CertificatesT <em>Certificates T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Certificates T</em>'.
	 * @see io.shell.admin.aas.abac._2._0.CertificatesT
	 * @generated
	 */
	EClass getCertificatesT();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas.abac._2._0.CertificatesT#getCertificate <em>Certificate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Certificate</em>'.
	 * @see io.shell.admin.aas.abac._2._0.CertificatesT#getCertificate()
	 * @see #getCertificatesT()
	 * @generated
	 */
	EReference getCertificatesT_Certificate();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas.abac._2._0.CertificateT <em>Certificate T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Certificate T</em>'.
	 * @see io.shell.admin.aas.abac._2._0.CertificateT
	 * @generated
	 */
	EClass getCertificateT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.CertificateT#getBlobCertificate <em>Blob Certificate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Blob Certificate</em>'.
	 * @see io.shell.admin.aas.abac._2._0.CertificateT#getBlobCertificate()
	 * @see #getCertificateT()
	 * @generated
	 */
	EReference getCertificateT_BlobCertificate();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas.abac._2._0.ContainedExtensionsT <em>Contained Extensions T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Contained Extensions T</em>'.
	 * @see io.shell.admin.aas.abac._2._0.ContainedExtensionsT
	 * @generated
	 */
	EClass getContainedExtensionsT();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas.abac._2._0.ContainedExtensionsT#getContainedExtension <em>Contained Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Contained Extension</em>'.
	 * @see io.shell.admin.aas.abac._2._0.ContainedExtensionsT#getContainedExtension()
	 * @see #getContainedExtensionsT()
	 * @generated
	 */
	EReference getContainedExtensionsT_ContainedExtension();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas.abac._2._0.InternalInformationPoints <em>Internal Information Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Internal Information Points</em>'.
	 * @see io.shell.admin.aas.abac._2._0.InternalInformationPoints
	 * @generated
	 */
	EClass getInternalInformationPoints();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas.abac._2._0.InternalInformationPoints#getInternalInformationPoint <em>Internal Information Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Internal Information Point</em>'.
	 * @see io.shell.admin.aas.abac._2._0.InternalInformationPoints#getInternalInformationPoint()
	 * @see #getInternalInformationPoints()
	 * @generated
	 */
	EReference getInternalInformationPoints_InternalInformationPoint();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas.abac._2._0.ObjectAttributesT <em>Object Attributes T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Attributes T</em>'.
	 * @see io.shell.admin.aas.abac._2._0.ObjectAttributesT
	 * @generated
	 */
	EClass getObjectAttributesT();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas.abac._2._0.ObjectAttributesT#getObjectAttribute <em>Object Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Attribute</em>'.
	 * @see io.shell.admin.aas.abac._2._0.ObjectAttributesT#getObjectAttribute()
	 * @see #getObjectAttributesT()
	 * @generated
	 */
	EReference getObjectAttributesT_ObjectAttribute();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas.abac._2._0.PermissionPerObjectT <em>Permission Per Object T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Permission Per Object T</em>'.
	 * @see io.shell.admin.aas.abac._2._0.PermissionPerObjectT
	 * @generated
	 */
	EClass getPermissionPerObjectT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.PermissionPerObjectT#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object</em>'.
	 * @see io.shell.admin.aas.abac._2._0.PermissionPerObjectT#getObject()
	 * @see #getPermissionPerObjectT()
	 * @generated
	 */
	EReference getPermissionPerObjectT_Object();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.PermissionPerObjectT#getTargetObjectAttributes <em>Target Object Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Target Object Attributes</em>'.
	 * @see io.shell.admin.aas.abac._2._0.PermissionPerObjectT#getTargetObjectAttributes()
	 * @see #getPermissionPerObjectT()
	 * @generated
	 */
	EReference getPermissionPerObjectT_TargetObjectAttributes();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.PermissionPerObjectT#getPermissions <em>Permissions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Permissions</em>'.
	 * @see io.shell.admin.aas.abac._2._0.PermissionPerObjectT#getPermissions()
	 * @see #getPermissionPerObjectT()
	 * @generated
	 */
	EReference getPermissionPerObjectT_Permissions();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas.abac._2._0.PermissionsT <em>Permissions T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Permissions T</em>'.
	 * @see io.shell.admin.aas.abac._2._0.PermissionsT
	 * @generated
	 */
	EClass getPermissionsT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.PermissionsT#getPermission <em>Permission</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Permission</em>'.
	 * @see io.shell.admin.aas.abac._2._0.PermissionsT#getPermission()
	 * @see #getPermissionsT()
	 * @generated
	 */
	EReference getPermissionsT_Permission();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas.abac._2._0.PermissionsT#getKindOfPermission <em>Kind Of Permission</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind Of Permission</em>'.
	 * @see io.shell.admin.aas.abac._2._0.PermissionsT#getKindOfPermission()
	 * @see #getPermissionsT()
	 * @generated
	 */
	EAttribute getPermissionsT_KindOfPermission();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas.abac._2._0.PolicyAdministrationPointT <em>Policy Administration Point T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Policy Administration Point T</em>'.
	 * @see io.shell.admin.aas.abac._2._0.PolicyAdministrationPointT
	 * @generated
	 */
	EClass getPolicyAdministrationPointT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.PolicyAdministrationPointT#getLocalAccessControl <em>Local Access Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Local Access Control</em>'.
	 * @see io.shell.admin.aas.abac._2._0.PolicyAdministrationPointT#getLocalAccessControl()
	 * @see #getPolicyAdministrationPointT()
	 * @generated
	 */
	EReference getPolicyAdministrationPointT_LocalAccessControl();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas.abac._2._0.PolicyAdministrationPointT#isExternalAccessControl <em>External Access Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>External Access Control</em>'.
	 * @see io.shell.admin.aas.abac._2._0.PolicyAdministrationPointT#isExternalAccessControl()
	 * @see #getPolicyAdministrationPointT()
	 * @generated
	 */
	EAttribute getPolicyAdministrationPointT_ExternalAccessControl();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas.abac._2._0.PolicyDecisionPointT <em>Policy Decision Point T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Policy Decision Point T</em>'.
	 * @see io.shell.admin.aas.abac._2._0.PolicyDecisionPointT
	 * @generated
	 */
	EClass getPolicyDecisionPointT();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas.abac._2._0.PolicyDecisionPointT#isExternalPolicyDecisionPoint <em>External Policy Decision Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>External Policy Decision Point</em>'.
	 * @see io.shell.admin.aas.abac._2._0.PolicyDecisionPointT#isExternalPolicyDecisionPoint()
	 * @see #getPolicyDecisionPointT()
	 * @generated
	 */
	EAttribute getPolicyDecisionPointT_ExternalPolicyDecisionPoint();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas.abac._2._0.PolicyEnforcementPointT <em>Policy Enforcement Point T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Policy Enforcement Point T</em>'.
	 * @see io.shell.admin.aas.abac._2._0.PolicyEnforcementPointT
	 * @generated
	 */
	EClass getPolicyEnforcementPointT();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas.abac._2._0.PolicyEnforcementPointT#isExternalPolicyEnforcementPoint <em>External Policy Enforcement Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>External Policy Enforcement Point</em>'.
	 * @see io.shell.admin.aas.abac._2._0.PolicyEnforcementPointT#isExternalPolicyEnforcementPoint()
	 * @see #getPolicyEnforcementPointT()
	 * @generated
	 */
	EAttribute getPolicyEnforcementPointT_ExternalPolicyEnforcementPoint();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas.abac._2._0.PolicyInformationPointsT <em>Policy Information Points T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Policy Information Points T</em>'.
	 * @see io.shell.admin.aas.abac._2._0.PolicyInformationPointsT
	 * @generated
	 */
	EClass getPolicyInformationPointsT();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.aas.abac._2._0.PolicyInformationPointsT#isExternalInformationPoints <em>External Information Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>External Information Points</em>'.
	 * @see io.shell.admin.aas.abac._2._0.PolicyInformationPointsT#isExternalInformationPoints()
	 * @see #getPolicyInformationPointsT()
	 * @generated
	 */
	EAttribute getPolicyInformationPointsT_ExternalInformationPoints();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.PolicyInformationPointsT#getInternalInformationPoints <em>Internal Information Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Internal Information Points</em>'.
	 * @see io.shell.admin.aas.abac._2._0.PolicyInformationPointsT#getInternalInformationPoints()
	 * @see #getPolicyInformationPointsT()
	 * @generated
	 */
	EReference getPolicyInformationPointsT_InternalInformationPoints();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas.abac._2._0.SecurityT <em>Security T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Security T</em>'.
	 * @see io.shell.admin.aas.abac._2._0.SecurityT
	 * @generated
	 */
	EClass getSecurityT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.SecurityT#getAccessControlPolicyPoints <em>Access Control Policy Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Access Control Policy Points</em>'.
	 * @see io.shell.admin.aas.abac._2._0.SecurityT#getAccessControlPolicyPoints()
	 * @see #getSecurityT()
	 * @generated
	 */
	EReference getSecurityT_AccessControlPolicyPoints();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.SecurityT#getCertificates <em>Certificates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Certificates</em>'.
	 * @see io.shell.admin.aas.abac._2._0.SecurityT#getCertificates()
	 * @see #getSecurityT()
	 * @generated
	 */
	EReference getSecurityT_Certificates();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.SecurityT#getRequiredCertificateExtensions <em>Required Certificate Extensions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Required Certificate Extensions</em>'.
	 * @see io.shell.admin.aas.abac._2._0.SecurityT#getRequiredCertificateExtensions()
	 * @see #getSecurityT()
	 * @generated
	 */
	EReference getSecurityT_RequiredCertificateExtensions();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas.abac._2._0.SubjectAttributesT <em>Subject Attributes T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Subject Attributes T</em>'.
	 * @see io.shell.admin.aas.abac._2._0.SubjectAttributesT
	 * @generated
	 */
	EClass getSubjectAttributesT();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.aas.abac._2._0.SubjectAttributesT#getSubjectAttribute <em>Subject Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Subject Attribute</em>'.
	 * @see io.shell.admin.aas.abac._2._0.SubjectAttributesT#getSubjectAttribute()
	 * @see #getSubjectAttributesT()
	 * @generated
	 */
	EReference getSubjectAttributesT_SubjectAttribute();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.aas.abac._2._0.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Document Root</em>'.
	 * @see io.shell.admin.aas.abac._2._0.DocumentRoot
	 * @generated
	 */
	EClass getDocumentRoot();

	/**
	 * Returns the meta object for the attribute list '{@link io.shell.admin.aas.abac._2._0.DocumentRoot#getMixed <em>Mixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Mixed</em>'.
	 * @see io.shell.admin.aas.abac._2._0.DocumentRoot#getMixed()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EAttribute getDocumentRoot_Mixed();

	/**
	 * Returns the meta object for the map '{@link io.shell.admin.aas.abac._2._0.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
	 * @see io.shell.admin.aas.abac._2._0.DocumentRoot#getXMLNSPrefixMap()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XMLNSPrefixMap();

	/**
	 * Returns the meta object for the map '{@link io.shell.admin.aas.abac._2._0.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XSI Schema Location</em>'.
	 * @see io.shell.admin.aas.abac._2._0.DocumentRoot#getXSISchemaLocation()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XSISchemaLocation();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.aas.abac._2._0.DocumentRoot#getInternalInformationPoints <em>Internal Information Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Internal Information Points</em>'.
	 * @see io.shell.admin.aas.abac._2._0.DocumentRoot#getInternalInformationPoints()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_InternalInformationPoints();

	/**
	 * Returns the meta object for enum '{@link io.shell.admin.aas.abac._2._0.PermissionKind <em>Permission Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Permission Kind</em>'.
	 * @see io.shell.admin.aas.abac._2._0.PermissionKind
	 * @generated
	 */
	EEnum getPermissionKind();

	/**
	 * Returns the meta object for data type '{@link io.shell.admin.aas.abac._2._0.PermissionKind <em>Permission Kind Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Permission Kind Object</em>'.
	 * @see io.shell.admin.aas.abac._2._0.PermissionKind
	 * @model instanceClass="io.shell.admin.aas.abac._2._0.PermissionKind"
	 *        extendedMetaData="name='permissionKind:Object' baseType='permissionKind'"
	 * @generated
	 */
	EDataType getPermissionKindObject();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	_0Factory get_0Factory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link io.shell.admin.aas.abac._2._0.impl.AccessControlPolicyPointsTImpl <em>Access Control Policy Points T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas.abac._2._0.impl.AccessControlPolicyPointsTImpl
		 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getAccessControlPolicyPointsT()
		 * @generated
		 */
		EClass ACCESS_CONTROL_POLICY_POINTS_T = eINSTANCE.getAccessControlPolicyPointsT();

		/**
		 * The meta object literal for the '<em><b>Policy Administration Point</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESS_CONTROL_POLICY_POINTS_T__POLICY_ADMINISTRATION_POINT = eINSTANCE.getAccessControlPolicyPointsT_PolicyAdministrationPoint();

		/**
		 * The meta object literal for the '<em><b>Policy Decision Point</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESS_CONTROL_POLICY_POINTS_T__POLICY_DECISION_POINT = eINSTANCE.getAccessControlPolicyPointsT_PolicyDecisionPoint();

		/**
		 * The meta object literal for the '<em><b>Policy Enforcement Point</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESS_CONTROL_POLICY_POINTS_T__POLICY_ENFORCEMENT_POINT = eINSTANCE.getAccessControlPolicyPointsT_PolicyEnforcementPoint();

		/**
		 * The meta object literal for the '<em><b>Policy Information Points</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESS_CONTROL_POLICY_POINTS_T__POLICY_INFORMATION_POINTS = eINSTANCE.getAccessControlPolicyPointsT_PolicyInformationPoints();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas.abac._2._0.impl.AccessControlTImpl <em>Access Control T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas.abac._2._0.impl.AccessControlTImpl
		 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getAccessControlT()
		 * @generated
		 */
		EClass ACCESS_CONTROL_T = eINSTANCE.getAccessControlT();

		/**
		 * The meta object literal for the '<em><b>Selectable Subject Attributes</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESS_CONTROL_T__SELECTABLE_SUBJECT_ATTRIBUTES = eINSTANCE.getAccessControlT_SelectableSubjectAttributes();

		/**
		 * The meta object literal for the '<em><b>Default Subject Attributes</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESS_CONTROL_T__DEFAULT_SUBJECT_ATTRIBUTES = eINSTANCE.getAccessControlT_DefaultSubjectAttributes();

		/**
		 * The meta object literal for the '<em><b>Selectable Permissions</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESS_CONTROL_T__SELECTABLE_PERMISSIONS = eINSTANCE.getAccessControlT_SelectablePermissions();

		/**
		 * The meta object literal for the '<em><b>Default Permissions</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESS_CONTROL_T__DEFAULT_PERMISSIONS = eINSTANCE.getAccessControlT_DefaultPermissions();

		/**
		 * The meta object literal for the '<em><b>Selectable Environment Attributes</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESS_CONTROL_T__SELECTABLE_ENVIRONMENT_ATTRIBUTES = eINSTANCE.getAccessControlT_SelectableEnvironmentAttributes();

		/**
		 * The meta object literal for the '<em><b>Default Environment Attributes</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESS_CONTROL_T__DEFAULT_ENVIRONMENT_ATTRIBUTES = eINSTANCE.getAccessControlT_DefaultEnvironmentAttributes();

		/**
		 * The meta object literal for the '<em><b>Access Permission Rules</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESS_CONTROL_T__ACCESS_PERMISSION_RULES = eINSTANCE.getAccessControlT_AccessPermissionRules();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas.abac._2._0.impl.AccessPermissionRulesTImpl <em>Access Permission Rules T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas.abac._2._0.impl.AccessPermissionRulesTImpl
		 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getAccessPermissionRulesT()
		 * @generated
		 */
		EClass ACCESS_PERMISSION_RULES_T = eINSTANCE.getAccessPermissionRulesT();

		/**
		 * The meta object literal for the '<em><b>Access Permission Rule</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESS_PERMISSION_RULES_T__ACCESS_PERMISSION_RULE = eINSTANCE.getAccessPermissionRulesT_AccessPermissionRule();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas.abac._2._0.impl.AccessPermissionRuleTImpl <em>Access Permission Rule T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas.abac._2._0.impl.AccessPermissionRuleTImpl
		 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getAccessPermissionRuleT()
		 * @generated
		 */
		EClass ACCESS_PERMISSION_RULE_T = eINSTANCE.getAccessPermissionRuleT();

		/**
		 * The meta object literal for the '<em><b>Qualifier</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESS_PERMISSION_RULE_T__QUALIFIER = eINSTANCE.getAccessPermissionRuleT_Qualifier();

		/**
		 * The meta object literal for the '<em><b>Id Short</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESS_PERMISSION_RULE_T__ID_SHORT = eINSTANCE.getAccessPermissionRuleT_IdShort();

		/**
		 * The meta object literal for the '<em><b>Category</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACCESS_PERMISSION_RULE_T__CATEGORY = eINSTANCE.getAccessPermissionRuleT_Category();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESS_PERMISSION_RULE_T__DESCRIPTION = eINSTANCE.getAccessPermissionRuleT_Description();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESS_PERMISSION_RULE_T__PARENT = eINSTANCE.getAccessPermissionRuleT_Parent();

		/**
		 * The meta object literal for the '<em><b>Target Subject Attributes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESS_PERMISSION_RULE_T__TARGET_SUBJECT_ATTRIBUTES = eINSTANCE.getAccessPermissionRuleT_TargetSubjectAttributes();

		/**
		 * The meta object literal for the '<em><b>Permissions Per Object</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESS_PERMISSION_RULE_T__PERMISSIONS_PER_OBJECT = eINSTANCE.getAccessPermissionRuleT_PermissionsPerObject();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas.abac._2._0.impl.BlobCertificateTImpl <em>Blob Certificate T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas.abac._2._0.impl.BlobCertificateTImpl
		 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getBlobCertificateT()
		 * @generated
		 */
		EClass BLOB_CERTIFICATE_T = eINSTANCE.getBlobCertificateT();

		/**
		 * The meta object literal for the '<em><b>Blob Certificate</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOB_CERTIFICATE_T__BLOB_CERTIFICATE = eINSTANCE.getBlobCertificateT_BlobCertificate();

		/**
		 * The meta object literal for the '<em><b>Contained Extensions</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOB_CERTIFICATE_T__CONTAINED_EXTENSIONS = eINSTANCE.getBlobCertificateT_ContainedExtensions();

		/**
		 * The meta object literal for the '<em><b>Last Certificate</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BLOB_CERTIFICATE_T__LAST_CERTIFICATE = eINSTANCE.getBlobCertificateT_LastCertificate();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas.abac._2._0.impl.CertificateAbstractTImpl <em>Certificate Abstract T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas.abac._2._0.impl.CertificateAbstractTImpl
		 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getCertificateAbstractT()
		 * @generated
		 */
		EClass CERTIFICATE_ABSTRACT_T = eINSTANCE.getCertificateAbstractT();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas.abac._2._0.impl.CertificatesTImpl <em>Certificates T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas.abac._2._0.impl.CertificatesTImpl
		 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getCertificatesT()
		 * @generated
		 */
		EClass CERTIFICATES_T = eINSTANCE.getCertificatesT();

		/**
		 * The meta object literal for the '<em><b>Certificate</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CERTIFICATES_T__CERTIFICATE = eINSTANCE.getCertificatesT_Certificate();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas.abac._2._0.impl.CertificateTImpl <em>Certificate T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas.abac._2._0.impl.CertificateTImpl
		 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getCertificateT()
		 * @generated
		 */
		EClass CERTIFICATE_T = eINSTANCE.getCertificateT();

		/**
		 * The meta object literal for the '<em><b>Blob Certificate</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CERTIFICATE_T__BLOB_CERTIFICATE = eINSTANCE.getCertificateT_BlobCertificate();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas.abac._2._0.impl.ContainedExtensionsTImpl <em>Contained Extensions T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas.abac._2._0.impl.ContainedExtensionsTImpl
		 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getContainedExtensionsT()
		 * @generated
		 */
		EClass CONTAINED_EXTENSIONS_T = eINSTANCE.getContainedExtensionsT();

		/**
		 * The meta object literal for the '<em><b>Contained Extension</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTAINED_EXTENSIONS_T__CONTAINED_EXTENSION = eINSTANCE.getContainedExtensionsT_ContainedExtension();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas.abac._2._0.impl.InternalInformationPointsImpl <em>Internal Information Points</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas.abac._2._0.impl.InternalInformationPointsImpl
		 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getInternalInformationPoints()
		 * @generated
		 */
		EClass INTERNAL_INFORMATION_POINTS = eINSTANCE.getInternalInformationPoints();

		/**
		 * The meta object literal for the '<em><b>Internal Information Point</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERNAL_INFORMATION_POINTS__INTERNAL_INFORMATION_POINT = eINSTANCE.getInternalInformationPoints_InternalInformationPoint();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas.abac._2._0.impl.ObjectAttributesTImpl <em>Object Attributes T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas.abac._2._0.impl.ObjectAttributesTImpl
		 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getObjectAttributesT()
		 * @generated
		 */
		EClass OBJECT_ATTRIBUTES_T = eINSTANCE.getObjectAttributesT();

		/**
		 * The meta object literal for the '<em><b>Object Attribute</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_ATTRIBUTES_T__OBJECT_ATTRIBUTE = eINSTANCE.getObjectAttributesT_ObjectAttribute();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas.abac._2._0.impl.PermissionPerObjectTImpl <em>Permission Per Object T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas.abac._2._0.impl.PermissionPerObjectTImpl
		 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getPermissionPerObjectT()
		 * @generated
		 */
		EClass PERMISSION_PER_OBJECT_T = eINSTANCE.getPermissionPerObjectT();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERMISSION_PER_OBJECT_T__OBJECT = eINSTANCE.getPermissionPerObjectT_Object();

		/**
		 * The meta object literal for the '<em><b>Target Object Attributes</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERMISSION_PER_OBJECT_T__TARGET_OBJECT_ATTRIBUTES = eINSTANCE.getPermissionPerObjectT_TargetObjectAttributes();

		/**
		 * The meta object literal for the '<em><b>Permissions</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERMISSION_PER_OBJECT_T__PERMISSIONS = eINSTANCE.getPermissionPerObjectT_Permissions();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas.abac._2._0.impl.PermissionsTImpl <em>Permissions T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas.abac._2._0.impl.PermissionsTImpl
		 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getPermissionsT()
		 * @generated
		 */
		EClass PERMISSIONS_T = eINSTANCE.getPermissionsT();

		/**
		 * The meta object literal for the '<em><b>Permission</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERMISSIONS_T__PERMISSION = eINSTANCE.getPermissionsT_Permission();

		/**
		 * The meta object literal for the '<em><b>Kind Of Permission</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERMISSIONS_T__KIND_OF_PERMISSION = eINSTANCE.getPermissionsT_KindOfPermission();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas.abac._2._0.impl.PolicyAdministrationPointTImpl <em>Policy Administration Point T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas.abac._2._0.impl.PolicyAdministrationPointTImpl
		 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getPolicyAdministrationPointT()
		 * @generated
		 */
		EClass POLICY_ADMINISTRATION_POINT_T = eINSTANCE.getPolicyAdministrationPointT();

		/**
		 * The meta object literal for the '<em><b>Local Access Control</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POLICY_ADMINISTRATION_POINT_T__LOCAL_ACCESS_CONTROL = eINSTANCE.getPolicyAdministrationPointT_LocalAccessControl();

		/**
		 * The meta object literal for the '<em><b>External Access Control</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POLICY_ADMINISTRATION_POINT_T__EXTERNAL_ACCESS_CONTROL = eINSTANCE.getPolicyAdministrationPointT_ExternalAccessControl();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas.abac._2._0.impl.PolicyDecisionPointTImpl <em>Policy Decision Point T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas.abac._2._0.impl.PolicyDecisionPointTImpl
		 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getPolicyDecisionPointT()
		 * @generated
		 */
		EClass POLICY_DECISION_POINT_T = eINSTANCE.getPolicyDecisionPointT();

		/**
		 * The meta object literal for the '<em><b>External Policy Decision Point</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POLICY_DECISION_POINT_T__EXTERNAL_POLICY_DECISION_POINT = eINSTANCE.getPolicyDecisionPointT_ExternalPolicyDecisionPoint();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas.abac._2._0.impl.PolicyEnforcementPointTImpl <em>Policy Enforcement Point T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas.abac._2._0.impl.PolicyEnforcementPointTImpl
		 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getPolicyEnforcementPointT()
		 * @generated
		 */
		EClass POLICY_ENFORCEMENT_POINT_T = eINSTANCE.getPolicyEnforcementPointT();

		/**
		 * The meta object literal for the '<em><b>External Policy Enforcement Point</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POLICY_ENFORCEMENT_POINT_T__EXTERNAL_POLICY_ENFORCEMENT_POINT = eINSTANCE.getPolicyEnforcementPointT_ExternalPolicyEnforcementPoint();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas.abac._2._0.impl.PolicyInformationPointsTImpl <em>Policy Information Points T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas.abac._2._0.impl.PolicyInformationPointsTImpl
		 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getPolicyInformationPointsT()
		 * @generated
		 */
		EClass POLICY_INFORMATION_POINTS_T = eINSTANCE.getPolicyInformationPointsT();

		/**
		 * The meta object literal for the '<em><b>External Information Points</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POLICY_INFORMATION_POINTS_T__EXTERNAL_INFORMATION_POINTS = eINSTANCE.getPolicyInformationPointsT_ExternalInformationPoints();

		/**
		 * The meta object literal for the '<em><b>Internal Information Points</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POLICY_INFORMATION_POINTS_T__INTERNAL_INFORMATION_POINTS = eINSTANCE.getPolicyInformationPointsT_InternalInformationPoints();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas.abac._2._0.impl.SecurityTImpl <em>Security T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas.abac._2._0.impl.SecurityTImpl
		 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getSecurityT()
		 * @generated
		 */
		EClass SECURITY_T = eINSTANCE.getSecurityT();

		/**
		 * The meta object literal for the '<em><b>Access Control Policy Points</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY_T__ACCESS_CONTROL_POLICY_POINTS = eINSTANCE.getSecurityT_AccessControlPolicyPoints();

		/**
		 * The meta object literal for the '<em><b>Certificates</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY_T__CERTIFICATES = eINSTANCE.getSecurityT_Certificates();

		/**
		 * The meta object literal for the '<em><b>Required Certificate Extensions</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY_T__REQUIRED_CERTIFICATE_EXTENSIONS = eINSTANCE.getSecurityT_RequiredCertificateExtensions();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas.abac._2._0.impl.SubjectAttributesTImpl <em>Subject Attributes T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas.abac._2._0.impl.SubjectAttributesTImpl
		 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getSubjectAttributesT()
		 * @generated
		 */
		EClass SUBJECT_ATTRIBUTES_T = eINSTANCE.getSubjectAttributesT();

		/**
		 * The meta object literal for the '<em><b>Subject Attribute</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBJECT_ATTRIBUTES_T__SUBJECT_ATTRIBUTE = eINSTANCE.getSubjectAttributesT_SubjectAttribute();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas.abac._2._0.impl.DocumentRootImpl <em>Document Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas.abac._2._0.impl.DocumentRootImpl
		 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getDocumentRoot()
		 * @generated
		 */
		EClass DOCUMENT_ROOT = eINSTANCE.getDocumentRoot();

		/**
		 * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENT_ROOT__MIXED = eINSTANCE.getDocumentRoot_Mixed();

		/**
		 * The meta object literal for the '<em><b>XMLNS Prefix Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XMLNS_PREFIX_MAP = eINSTANCE.getDocumentRoot_XMLNSPrefixMap();

		/**
		 * The meta object literal for the '<em><b>XSI Schema Location</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = eINSTANCE.getDocumentRoot_XSISchemaLocation();

		/**
		 * The meta object literal for the '<em><b>Internal Information Points</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__INTERNAL_INFORMATION_POINTS = eINSTANCE.getDocumentRoot_InternalInformationPoints();

		/**
		 * The meta object literal for the '{@link io.shell.admin.aas.abac._2._0.PermissionKind <em>Permission Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas.abac._2._0.PermissionKind
		 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getPermissionKind()
		 * @generated
		 */
		EEnum PERMISSION_KIND = eINSTANCE.getPermissionKind();

		/**
		 * The meta object literal for the '<em>Permission Kind Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.aas.abac._2._0.PermissionKind
		 * @see io.shell.admin.aas.abac._2._0.impl._0PackageImpl#getPermissionKindObject()
		 * @generated
		 */
		EDataType PERMISSION_KIND_OBJECT = eINSTANCE.getPermissionKindObject();

	}

} //_0Package
