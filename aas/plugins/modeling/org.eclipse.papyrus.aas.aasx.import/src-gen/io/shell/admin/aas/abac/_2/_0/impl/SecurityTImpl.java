/**
 */
package io.shell.admin.aas.abac._2._0.impl;

import io.shell.admin.aas._2._0.ReferencesT;

import io.shell.admin.aas.abac._2._0.AccessControlPolicyPointsT;
import io.shell.admin.aas.abac._2._0.CertificatesT;
import io.shell.admin.aas.abac._2._0.SecurityT;
import io.shell.admin.aas.abac._2._0._0Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Security T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.SecurityTImpl#getAccessControlPolicyPoints <em>Access Control Policy Points</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.SecurityTImpl#getCertificates <em>Certificates</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.SecurityTImpl#getRequiredCertificateExtensions <em>Required Certificate Extensions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SecurityTImpl extends MinimalEObjectImpl.Container implements SecurityT {
	/**
	 * The cached value of the '{@link #getAccessControlPolicyPoints() <em>Access Control Policy Points</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessControlPolicyPoints()
	 * @generated
	 * @ordered
	 */
	protected AccessControlPolicyPointsT accessControlPolicyPoints;

	/**
	 * The cached value of the '{@link #getCertificates() <em>Certificates</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCertificates()
	 * @generated
	 * @ordered
	 */
	protected CertificatesT certificates;

	/**
	 * The cached value of the '{@link #getRequiredCertificateExtensions() <em>Required Certificate Extensions</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredCertificateExtensions()
	 * @generated
	 * @ordered
	 */
	protected ReferencesT requiredCertificateExtensions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SecurityTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.SECURITY_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessControlPolicyPointsT getAccessControlPolicyPoints() {
		return accessControlPolicyPoints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccessControlPolicyPoints(AccessControlPolicyPointsT newAccessControlPolicyPoints, NotificationChain msgs) {
		AccessControlPolicyPointsT oldAccessControlPolicyPoints = accessControlPolicyPoints;
		accessControlPolicyPoints = newAccessControlPolicyPoints;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.SECURITY_T__ACCESS_CONTROL_POLICY_POINTS, oldAccessControlPolicyPoints, newAccessControlPolicyPoints);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessControlPolicyPoints(AccessControlPolicyPointsT newAccessControlPolicyPoints) {
		if (newAccessControlPolicyPoints != accessControlPolicyPoints) {
			NotificationChain msgs = null;
			if (accessControlPolicyPoints != null)
				msgs = ((InternalEObject)accessControlPolicyPoints).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.SECURITY_T__ACCESS_CONTROL_POLICY_POINTS, null, msgs);
			if (newAccessControlPolicyPoints != null)
				msgs = ((InternalEObject)newAccessControlPolicyPoints).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.SECURITY_T__ACCESS_CONTROL_POLICY_POINTS, null, msgs);
			msgs = basicSetAccessControlPolicyPoints(newAccessControlPolicyPoints, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.SECURITY_T__ACCESS_CONTROL_POLICY_POINTS, newAccessControlPolicyPoints, newAccessControlPolicyPoints));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CertificatesT getCertificates() {
		return certificates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCertificates(CertificatesT newCertificates, NotificationChain msgs) {
		CertificatesT oldCertificates = certificates;
		certificates = newCertificates;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.SECURITY_T__CERTIFICATES, oldCertificates, newCertificates);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCertificates(CertificatesT newCertificates) {
		if (newCertificates != certificates) {
			NotificationChain msgs = null;
			if (certificates != null)
				msgs = ((InternalEObject)certificates).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.SECURITY_T__CERTIFICATES, null, msgs);
			if (newCertificates != null)
				msgs = ((InternalEObject)newCertificates).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.SECURITY_T__CERTIFICATES, null, msgs);
			msgs = basicSetCertificates(newCertificates, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.SECURITY_T__CERTIFICATES, newCertificates, newCertificates));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferencesT getRequiredCertificateExtensions() {
		return requiredCertificateExtensions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRequiredCertificateExtensions(ReferencesT newRequiredCertificateExtensions, NotificationChain msgs) {
		ReferencesT oldRequiredCertificateExtensions = requiredCertificateExtensions;
		requiredCertificateExtensions = newRequiredCertificateExtensions;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.SECURITY_T__REQUIRED_CERTIFICATE_EXTENSIONS, oldRequiredCertificateExtensions, newRequiredCertificateExtensions);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredCertificateExtensions(ReferencesT newRequiredCertificateExtensions) {
		if (newRequiredCertificateExtensions != requiredCertificateExtensions) {
			NotificationChain msgs = null;
			if (requiredCertificateExtensions != null)
				msgs = ((InternalEObject)requiredCertificateExtensions).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.SECURITY_T__REQUIRED_CERTIFICATE_EXTENSIONS, null, msgs);
			if (newRequiredCertificateExtensions != null)
				msgs = ((InternalEObject)newRequiredCertificateExtensions).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.SECURITY_T__REQUIRED_CERTIFICATE_EXTENSIONS, null, msgs);
			msgs = basicSetRequiredCertificateExtensions(newRequiredCertificateExtensions, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.SECURITY_T__REQUIRED_CERTIFICATE_EXTENSIONS, newRequiredCertificateExtensions, newRequiredCertificateExtensions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.SECURITY_T__ACCESS_CONTROL_POLICY_POINTS:
				return basicSetAccessControlPolicyPoints(null, msgs);
			case _0Package.SECURITY_T__CERTIFICATES:
				return basicSetCertificates(null, msgs);
			case _0Package.SECURITY_T__REQUIRED_CERTIFICATE_EXTENSIONS:
				return basicSetRequiredCertificateExtensions(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.SECURITY_T__ACCESS_CONTROL_POLICY_POINTS:
				return getAccessControlPolicyPoints();
			case _0Package.SECURITY_T__CERTIFICATES:
				return getCertificates();
			case _0Package.SECURITY_T__REQUIRED_CERTIFICATE_EXTENSIONS:
				return getRequiredCertificateExtensions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.SECURITY_T__ACCESS_CONTROL_POLICY_POINTS:
				setAccessControlPolicyPoints((AccessControlPolicyPointsT)newValue);
				return;
			case _0Package.SECURITY_T__CERTIFICATES:
				setCertificates((CertificatesT)newValue);
				return;
			case _0Package.SECURITY_T__REQUIRED_CERTIFICATE_EXTENSIONS:
				setRequiredCertificateExtensions((ReferencesT)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.SECURITY_T__ACCESS_CONTROL_POLICY_POINTS:
				setAccessControlPolicyPoints((AccessControlPolicyPointsT)null);
				return;
			case _0Package.SECURITY_T__CERTIFICATES:
				setCertificates((CertificatesT)null);
				return;
			case _0Package.SECURITY_T__REQUIRED_CERTIFICATE_EXTENSIONS:
				setRequiredCertificateExtensions((ReferencesT)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.SECURITY_T__ACCESS_CONTROL_POLICY_POINTS:
				return accessControlPolicyPoints != null;
			case _0Package.SECURITY_T__CERTIFICATES:
				return certificates != null;
			case _0Package.SECURITY_T__REQUIRED_CERTIFICATE_EXTENSIONS:
				return requiredCertificateExtensions != null;
		}
		return super.eIsSet(featureID);
	}

} //SecurityTImpl
