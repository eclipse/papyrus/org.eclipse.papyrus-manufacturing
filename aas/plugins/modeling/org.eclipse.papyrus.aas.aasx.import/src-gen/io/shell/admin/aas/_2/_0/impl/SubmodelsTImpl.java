/**
 */
package io.shell.admin.aas._2._0.impl;

import io.shell.admin.aas._2._0.SubmodelT;
import io.shell.admin.aas._2._0.SubmodelsT;
import io.shell.admin.aas._2._0._0Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Submodels T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.impl.SubmodelsTImpl#getSubmodel <em>Submodel</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubmodelsTImpl extends MinimalEObjectImpl.Container implements SubmodelsT {
	/**
	 * The cached value of the '{@link #getSubmodel() <em>Submodel</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubmodel()
	 * @generated
	 * @ordered
	 */
	protected EList<SubmodelT> submodel;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubmodelsTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.SUBMODELS_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SubmodelT> getSubmodel() {
		if (submodel == null) {
			submodel = new EObjectContainmentEList<SubmodelT>(SubmodelT.class, this, _0Package.SUBMODELS_T__SUBMODEL);
		}
		return submodel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.SUBMODELS_T__SUBMODEL:
				return ((InternalEList<?>)getSubmodel()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.SUBMODELS_T__SUBMODEL:
				return getSubmodel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.SUBMODELS_T__SUBMODEL:
				getSubmodel().clear();
				getSubmodel().addAll((Collection<? extends SubmodelT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.SUBMODELS_T__SUBMODEL:
				getSubmodel().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.SUBMODELS_T__SUBMODEL:
				return submodel != null && !submodel.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SubmodelsTImpl
