/**
 */
package io.shell.admin.aas._2._0.impl;

import io.shell.admin.aas._2._0.ContainedElementsT;
import io.shell.admin.aas._2._0.ReferenceT;
import io.shell.admin.aas._2._0._0Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Contained Elements T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.impl.ContainedElementsTImpl#getContainedElementRef <em>Contained Element Ref</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ContainedElementsTImpl extends MinimalEObjectImpl.Container implements ContainedElementsT {
	/**
	 * The cached value of the '{@link #getContainedElementRef() <em>Contained Element Ref</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainedElementRef()
	 * @generated
	 * @ordered
	 */
	protected EList<ReferenceT> containedElementRef;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ContainedElementsTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.CONTAINED_ELEMENTS_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReferenceT> getContainedElementRef() {
		if (containedElementRef == null) {
			containedElementRef = new EObjectContainmentEList<ReferenceT>(ReferenceT.class, this, _0Package.CONTAINED_ELEMENTS_T__CONTAINED_ELEMENT_REF);
		}
		return containedElementRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.CONTAINED_ELEMENTS_T__CONTAINED_ELEMENT_REF:
				return ((InternalEList<?>)getContainedElementRef()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.CONTAINED_ELEMENTS_T__CONTAINED_ELEMENT_REF:
				return getContainedElementRef();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.CONTAINED_ELEMENTS_T__CONTAINED_ELEMENT_REF:
				getContainedElementRef().clear();
				getContainedElementRef().addAll((Collection<? extends ReferenceT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.CONTAINED_ELEMENTS_T__CONTAINED_ELEMENT_REF:
				getContainedElementRef().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.CONTAINED_ELEMENTS_T__CONTAINED_ELEMENT_REF:
				return containedElementRef != null && !containedElementRef.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ContainedElementsTImpl
