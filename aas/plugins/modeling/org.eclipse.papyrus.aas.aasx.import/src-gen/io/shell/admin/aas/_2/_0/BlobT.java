/**
 */
package io.shell.admin.aas._2._0;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Blob T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.BlobT#getValue <em>Value</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.BlobT#getMimeType <em>Mime Type</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getBlobT()
 * @model extendedMetaData="name='blob_t' kind='elementOnly'"
 * @generated
 */
public interface BlobT extends SubmodelElementAbstractT {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(BlobTypeT)
	 * @see io.shell.admin.aas._2._0._0Package#getBlobT_Value()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='value' namespace='##targetNamespace'"
	 * @generated
	 */
	BlobTypeT getValue();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.BlobT#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(BlobTypeT value);

	/**
	 * Returns the value of the '<em><b>Mime Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mime Type</em>' attribute.
	 * @see #setMimeType(String)
	 * @see io.shell.admin.aas._2._0._0Package#getBlobT_MimeType()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='mimeType' namespace='##targetNamespace'"
	 * @generated
	 */
	String getMimeType();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.BlobT#getMimeType <em>Mime Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mime Type</em>' attribute.
	 * @see #getMimeType()
	 * @generated
	 */
	void setMimeType(String value);

} // BlobT
