/**
 */
package io.shell.admin.aas.abac._2._0.impl;

import io.shell.admin.aas._2._0.ReferenceT;

import io.shell.admin.aas.abac._2._0.ObjectAttributesT;
import io.shell.admin.aas.abac._2._0.PermissionPerObjectT;
import io.shell.admin.aas.abac._2._0.PermissionsT;
import io.shell.admin.aas.abac._2._0._0Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Permission Per Object T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.PermissionPerObjectTImpl#getObject <em>Object</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.PermissionPerObjectTImpl#getTargetObjectAttributes <em>Target Object Attributes</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.impl.PermissionPerObjectTImpl#getPermissions <em>Permissions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PermissionPerObjectTImpl extends MinimalEObjectImpl.Container implements PermissionPerObjectT {
	/**
	 * The cached value of the '{@link #getObject() <em>Object</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObject()
	 * @generated
	 * @ordered
	 */
	protected ReferenceT object;

	/**
	 * The cached value of the '{@link #getTargetObjectAttributes() <em>Target Object Attributes</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetObjectAttributes()
	 * @generated
	 * @ordered
	 */
	protected ObjectAttributesT targetObjectAttributes;

	/**
	 * The cached value of the '{@link #getPermissions() <em>Permissions</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPermissions()
	 * @generated
	 * @ordered
	 */
	protected PermissionsT permissions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PermissionPerObjectTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.PERMISSION_PER_OBJECT_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceT getObject() {
		return object;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObject(ReferenceT newObject, NotificationChain msgs) {
		ReferenceT oldObject = object;
		object = newObject;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.PERMISSION_PER_OBJECT_T__OBJECT, oldObject, newObject);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObject(ReferenceT newObject) {
		if (newObject != object) {
			NotificationChain msgs = null;
			if (object != null)
				msgs = ((InternalEObject)object).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.PERMISSION_PER_OBJECT_T__OBJECT, null, msgs);
			if (newObject != null)
				msgs = ((InternalEObject)newObject).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.PERMISSION_PER_OBJECT_T__OBJECT, null, msgs);
			msgs = basicSetObject(newObject, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.PERMISSION_PER_OBJECT_T__OBJECT, newObject, newObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectAttributesT getTargetObjectAttributes() {
		return targetObjectAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTargetObjectAttributes(ObjectAttributesT newTargetObjectAttributes, NotificationChain msgs) {
		ObjectAttributesT oldTargetObjectAttributes = targetObjectAttributes;
		targetObjectAttributes = newTargetObjectAttributes;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.PERMISSION_PER_OBJECT_T__TARGET_OBJECT_ATTRIBUTES, oldTargetObjectAttributes, newTargetObjectAttributes);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetObjectAttributes(ObjectAttributesT newTargetObjectAttributes) {
		if (newTargetObjectAttributes != targetObjectAttributes) {
			NotificationChain msgs = null;
			if (targetObjectAttributes != null)
				msgs = ((InternalEObject)targetObjectAttributes).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.PERMISSION_PER_OBJECT_T__TARGET_OBJECT_ATTRIBUTES, null, msgs);
			if (newTargetObjectAttributes != null)
				msgs = ((InternalEObject)newTargetObjectAttributes).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.PERMISSION_PER_OBJECT_T__TARGET_OBJECT_ATTRIBUTES, null, msgs);
			msgs = basicSetTargetObjectAttributes(newTargetObjectAttributes, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.PERMISSION_PER_OBJECT_T__TARGET_OBJECT_ATTRIBUTES, newTargetObjectAttributes, newTargetObjectAttributes));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PermissionsT getPermissions() {
		return permissions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPermissions(PermissionsT newPermissions, NotificationChain msgs) {
		PermissionsT oldPermissions = permissions;
		permissions = newPermissions;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.PERMISSION_PER_OBJECT_T__PERMISSIONS, oldPermissions, newPermissions);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPermissions(PermissionsT newPermissions) {
		if (newPermissions != permissions) {
			NotificationChain msgs = null;
			if (permissions != null)
				msgs = ((InternalEObject)permissions).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.PERMISSION_PER_OBJECT_T__PERMISSIONS, null, msgs);
			if (newPermissions != null)
				msgs = ((InternalEObject)newPermissions).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.PERMISSION_PER_OBJECT_T__PERMISSIONS, null, msgs);
			msgs = basicSetPermissions(newPermissions, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.PERMISSION_PER_OBJECT_T__PERMISSIONS, newPermissions, newPermissions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.PERMISSION_PER_OBJECT_T__OBJECT:
				return basicSetObject(null, msgs);
			case _0Package.PERMISSION_PER_OBJECT_T__TARGET_OBJECT_ATTRIBUTES:
				return basicSetTargetObjectAttributes(null, msgs);
			case _0Package.PERMISSION_PER_OBJECT_T__PERMISSIONS:
				return basicSetPermissions(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.PERMISSION_PER_OBJECT_T__OBJECT:
				return getObject();
			case _0Package.PERMISSION_PER_OBJECT_T__TARGET_OBJECT_ATTRIBUTES:
				return getTargetObjectAttributes();
			case _0Package.PERMISSION_PER_OBJECT_T__PERMISSIONS:
				return getPermissions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.PERMISSION_PER_OBJECT_T__OBJECT:
				setObject((ReferenceT)newValue);
				return;
			case _0Package.PERMISSION_PER_OBJECT_T__TARGET_OBJECT_ATTRIBUTES:
				setTargetObjectAttributes((ObjectAttributesT)newValue);
				return;
			case _0Package.PERMISSION_PER_OBJECT_T__PERMISSIONS:
				setPermissions((PermissionsT)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.PERMISSION_PER_OBJECT_T__OBJECT:
				setObject((ReferenceT)null);
				return;
			case _0Package.PERMISSION_PER_OBJECT_T__TARGET_OBJECT_ATTRIBUTES:
				setTargetObjectAttributes((ObjectAttributesT)null);
				return;
			case _0Package.PERMISSION_PER_OBJECT_T__PERMISSIONS:
				setPermissions((PermissionsT)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.PERMISSION_PER_OBJECT_T__OBJECT:
				return object != null;
			case _0Package.PERMISSION_PER_OBJECT_T__TARGET_OBJECT_ATTRIBUTES:
				return targetObjectAttributes != null;
			case _0Package.PERMISSION_PER_OBJECT_T__PERMISSIONS:
				return permissions != null;
		}
		return super.eIsSet(featureID);
	}

} //PermissionPerObjectTImpl
