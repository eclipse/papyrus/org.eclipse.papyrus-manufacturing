/**
 */
package io.shell.admin.aas._2._0;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Asset T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.AssetT#getIdShort <em>Id Short</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.AssetT#getCategory <em>Category</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.AssetT#getDescription <em>Description</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.AssetT#getParent <em>Parent</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.AssetT#getIdentification <em>Identification</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.AssetT#getAdministration <em>Administration</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.AssetT#getEmbeddedDataSpecification <em>Embedded Data Specification</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.AssetT#getAssetIdentificationModelRef <em>Asset Identification Model Ref</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.AssetT#getBillOfMaterialRef <em>Bill Of Material Ref</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.AssetT#getKind <em>Kind</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getAssetT()
 * @model extendedMetaData="name='asset_t' kind='elementOnly'"
 * @generated
 */
public interface AssetT extends EObject {
	/**
	 * Returns the value of the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id Short</em>' containment reference.
	 * @see #setIdShort(IdShortT)
	 * @see io.shell.admin.aas._2._0._0Package#getAssetT_IdShort()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='idShort' namespace='##targetNamespace'"
	 * @generated
	 */
	IdShortT getIdShort();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.AssetT#getIdShort <em>Id Short</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id Short</em>' containment reference.
	 * @see #getIdShort()
	 * @generated
	 */
	void setIdShort(IdShortT value);

	/**
	 * Returns the value of the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Category</em>' attribute.
	 * @see #setCategory(String)
	 * @see io.shell.admin.aas._2._0._0Package#getAssetT_Category()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='category' namespace='##targetNamespace'"
	 * @generated
	 */
	String getCategory();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.AssetT#getCategory <em>Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Category</em>' attribute.
	 * @see #getCategory()
	 * @generated
	 */
	void setCategory(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference.
	 * @see #setDescription(LangStringSetT)
	 * @see io.shell.admin.aas._2._0._0Package#getAssetT_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='description' namespace='##targetNamespace'"
	 * @generated
	 */
	LangStringSetT getDescription();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.AssetT#getDescription <em>Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' containment reference.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(LangStringSetT value);

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' containment reference.
	 * @see #setParent(ReferenceT)
	 * @see io.shell.admin.aas._2._0._0Package#getAssetT_Parent()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='parent' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getParent();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.AssetT#getParent <em>Parent</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' containment reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(ReferenceT value);

	/**
	 * Returns the value of the '<em><b>Identification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identification</em>' containment reference.
	 * @see #setIdentification(IdentificationT)
	 * @see io.shell.admin.aas._2._0._0Package#getAssetT_Identification()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='identification' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentificationT getIdentification();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.AssetT#getIdentification <em>Identification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Identification</em>' containment reference.
	 * @see #getIdentification()
	 * @generated
	 */
	void setIdentification(IdentificationT value);

	/**
	 * Returns the value of the '<em><b>Administration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Administration</em>' containment reference.
	 * @see #setAdministration(AdministrationT)
	 * @see io.shell.admin.aas._2._0._0Package#getAssetT_Administration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='administration' namespace='##targetNamespace'"
	 * @generated
	 */
	AdministrationT getAdministration();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.AssetT#getAdministration <em>Administration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Administration</em>' containment reference.
	 * @see #getAdministration()
	 * @generated
	 */
	void setAdministration(AdministrationT value);

	/**
	 * Returns the value of the '<em><b>Embedded Data Specification</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas._2._0.EmbeddedDataSpecificationT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Embedded Data Specification</em>' containment reference list.
	 * @see io.shell.admin.aas._2._0._0Package#getAssetT_EmbeddedDataSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='embeddedDataSpecification' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<EmbeddedDataSpecificationT> getEmbeddedDataSpecification();

	/**
	 * Returns the value of the '<em><b>Asset Identification Model Ref</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asset Identification Model Ref</em>' containment reference.
	 * @see #setAssetIdentificationModelRef(ReferenceT)
	 * @see io.shell.admin.aas._2._0._0Package#getAssetT_AssetIdentificationModelRef()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='assetIdentificationModelRef' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getAssetIdentificationModelRef();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.AssetT#getAssetIdentificationModelRef <em>Asset Identification Model Ref</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Asset Identification Model Ref</em>' containment reference.
	 * @see #getAssetIdentificationModelRef()
	 * @generated
	 */
	void setAssetIdentificationModelRef(ReferenceT value);

	/**
	 * Returns the value of the '<em><b>Bill Of Material Ref</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bill Of Material Ref</em>' containment reference.
	 * @see #setBillOfMaterialRef(ReferenceT)
	 * @see io.shell.admin.aas._2._0._0Package#getAssetT_BillOfMaterialRef()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='billOfMaterialRef' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getBillOfMaterialRef();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.AssetT#getBillOfMaterialRef <em>Bill Of Material Ref</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bill Of Material Ref</em>' containment reference.
	 * @see #getBillOfMaterialRef()
	 * @generated
	 */
	void setBillOfMaterialRef(ReferenceT value);

	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link io.shell.admin.aas._2._0.AssetKindT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see io.shell.admin.aas._2._0.AssetKindT
	 * @see #isSetKind()
	 * @see #unsetKind()
	 * @see #setKind(AssetKindT)
	 * @see io.shell.admin.aas._2._0._0Package#getAssetT_Kind()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='element' name='kind' namespace='##targetNamespace'"
	 * @generated
	 */
	AssetKindT getKind();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.AssetT#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind</em>' attribute.
	 * @see io.shell.admin.aas._2._0.AssetKindT
	 * @see #isSetKind()
	 * @see #unsetKind()
	 * @see #getKind()
	 * @generated
	 */
	void setKind(AssetKindT value);

	/**
	 * Unsets the value of the '{@link io.shell.admin.aas._2._0.AssetT#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetKind()
	 * @see #getKind()
	 * @see #setKind(AssetKindT)
	 * @generated
	 */
	void unsetKind();

	/**
	 * Returns whether the value of the '{@link io.shell.admin.aas._2._0.AssetT#getKind <em>Kind</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Kind</em>' attribute is set.
	 * @see #unsetKind()
	 * @see #getKind()
	 * @see #setKind(AssetKindT)
	 * @generated
	 */
	boolean isSetKind();

} // AssetT
