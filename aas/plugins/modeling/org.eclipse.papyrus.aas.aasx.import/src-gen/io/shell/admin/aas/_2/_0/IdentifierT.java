/**
 */
package io.shell.admin.aas._2._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Identifier T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.IdentifierT#getId <em>Id</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.IdentifierT#getIdType <em>Id Type</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getIdentifierT()
 * @model extendedMetaData="name='identifier_t' kind='elementOnly'"
 * @generated
 */
public interface IdentifierT extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see io.shell.admin.aas._2._0._0Package#getIdentifierT_Id()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='id' namespace='##targetNamespace'"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.IdentifierT#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Id Type</b></em>' attribute.
	 * The literals are from the enumeration {@link io.shell.admin.aas._2._0.IdentifierTypeT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id Type</em>' attribute.
	 * @see io.shell.admin.aas._2._0.IdentifierTypeT
	 * @see #isSetIdType()
	 * @see #unsetIdType()
	 * @see #setIdType(IdentifierTypeT)
	 * @see io.shell.admin.aas._2._0._0Package#getIdentifierT_IdType()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='element' name='idType' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentifierTypeT getIdType();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.IdentifierT#getIdType <em>Id Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id Type</em>' attribute.
	 * @see io.shell.admin.aas._2._0.IdentifierTypeT
	 * @see #isSetIdType()
	 * @see #unsetIdType()
	 * @see #getIdType()
	 * @generated
	 */
	void setIdType(IdentifierTypeT value);

	/**
	 * Unsets the value of the '{@link io.shell.admin.aas._2._0.IdentifierT#getIdType <em>Id Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIdType()
	 * @see #getIdType()
	 * @see #setIdType(IdentifierTypeT)
	 * @generated
	 */
	void unsetIdType();

	/**
	 * Returns whether the value of the '{@link io.shell.admin.aas._2._0.IdentifierT#getIdType <em>Id Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Id Type</em>' attribute is set.
	 * @see #unsetIdType()
	 * @see #getIdType()
	 * @see #setIdType(IdentifierTypeT)
	 * @generated
	 */
	boolean isSetIdType();

} // IdentifierT
