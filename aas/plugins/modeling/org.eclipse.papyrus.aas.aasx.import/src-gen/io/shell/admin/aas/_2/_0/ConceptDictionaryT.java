/**
 */
package io.shell.admin.aas._2._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Concept Dictionary T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.ConceptDictionaryT#getIdShort <em>Id Short</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.ConceptDictionaryT#getCategory <em>Category</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.ConceptDictionaryT#getDescription <em>Description</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.ConceptDictionaryT#getParent <em>Parent</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.ConceptDictionaryT#getConceptDescriptionRefs <em>Concept Description Refs</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getConceptDictionaryT()
 * @model extendedMetaData="name='conceptDictionary_t' kind='elementOnly'"
 * @generated
 */
public interface ConceptDictionaryT extends EObject {
	/**
	 * Returns the value of the '<em><b>Id Short</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id Short</em>' containment reference.
	 * @see #setIdShort(IdShortT)
	 * @see io.shell.admin.aas._2._0._0Package#getConceptDictionaryT_IdShort()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='idShort' namespace='##targetNamespace'"
	 * @generated
	 */
	IdShortT getIdShort();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.ConceptDictionaryT#getIdShort <em>Id Short</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id Short</em>' containment reference.
	 * @see #getIdShort()
	 * @generated
	 */
	void setIdShort(IdShortT value);

	/**
	 * Returns the value of the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Category</em>' attribute.
	 * @see #setCategory(String)
	 * @see io.shell.admin.aas._2._0._0Package#getConceptDictionaryT_Category()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='category' namespace='##targetNamespace'"
	 * @generated
	 */
	String getCategory();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.ConceptDictionaryT#getCategory <em>Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Category</em>' attribute.
	 * @see #getCategory()
	 * @generated
	 */
	void setCategory(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference.
	 * @see #setDescription(LangStringSetT)
	 * @see io.shell.admin.aas._2._0._0Package#getConceptDictionaryT_Description()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='description' namespace='##targetNamespace'"
	 * @generated
	 */
	LangStringSetT getDescription();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.ConceptDictionaryT#getDescription <em>Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' containment reference.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(LangStringSetT value);

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' containment reference.
	 * @see #setParent(ReferenceT)
	 * @see io.shell.admin.aas._2._0._0Package#getConceptDictionaryT_Parent()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='parent' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getParent();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.ConceptDictionaryT#getParent <em>Parent</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' containment reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(ReferenceT value);

	/**
	 * Returns the value of the '<em><b>Concept Description Refs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Concept Description Refs</em>' containment reference.
	 * @see #setConceptDescriptionRefs(ConceptDescriptionRefsT)
	 * @see io.shell.admin.aas._2._0._0Package#getConceptDictionaryT_ConceptDescriptionRefs()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='conceptDescriptionRefs' namespace='##targetNamespace'"
	 * @generated
	 */
	ConceptDescriptionRefsT getConceptDescriptionRefs();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.ConceptDictionaryT#getConceptDescriptionRefs <em>Concept Description Refs</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Concept Description Refs</em>' containment reference.
	 * @see #getConceptDescriptionRefs()
	 * @generated
	 */
	void setConceptDescriptionRefs(ConceptDescriptionRefsT value);

} // ConceptDictionaryT
