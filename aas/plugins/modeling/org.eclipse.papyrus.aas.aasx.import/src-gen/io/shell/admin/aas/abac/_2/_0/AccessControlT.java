/**
 */
package io.shell.admin.aas.abac._2._0;

import io.shell.admin.aas._2._0.ReferenceT;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Access Control T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.AccessControlT#getSelectableSubjectAttributes <em>Selectable Subject Attributes</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.AccessControlT#getDefaultSubjectAttributes <em>Default Subject Attributes</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.AccessControlT#getSelectablePermissions <em>Selectable Permissions</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.AccessControlT#getDefaultPermissions <em>Default Permissions</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.AccessControlT#getSelectableEnvironmentAttributes <em>Selectable Environment Attributes</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.AccessControlT#getDefaultEnvironmentAttributes <em>Default Environment Attributes</em>}</li>
 *   <li>{@link io.shell.admin.aas.abac._2._0.AccessControlT#getAccessPermissionRules <em>Access Permission Rules</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas.abac._2._0._0Package#getAccessControlT()
 * @model extendedMetaData="name='accessControl_t' kind='elementOnly'"
 * @generated
 */
public interface AccessControlT extends EObject {
	/**
	 * Returns the value of the '<em><b>Selectable Subject Attributes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selectable Subject Attributes</em>' containment reference.
	 * @see #setSelectableSubjectAttributes(ReferenceT)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getAccessControlT_SelectableSubjectAttributes()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='selectableSubjectAttributes' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getSelectableSubjectAttributes();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.AccessControlT#getSelectableSubjectAttributes <em>Selectable Subject Attributes</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selectable Subject Attributes</em>' containment reference.
	 * @see #getSelectableSubjectAttributes()
	 * @generated
	 */
	void setSelectableSubjectAttributes(ReferenceT value);

	/**
	 * Returns the value of the '<em><b>Default Subject Attributes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Subject Attributes</em>' containment reference.
	 * @see #setDefaultSubjectAttributes(ReferenceT)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getAccessControlT_DefaultSubjectAttributes()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='defaultSubjectAttributes' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getDefaultSubjectAttributes();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.AccessControlT#getDefaultSubjectAttributes <em>Default Subject Attributes</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Subject Attributes</em>' containment reference.
	 * @see #getDefaultSubjectAttributes()
	 * @generated
	 */
	void setDefaultSubjectAttributes(ReferenceT value);

	/**
	 * Returns the value of the '<em><b>Selectable Permissions</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selectable Permissions</em>' containment reference.
	 * @see #setSelectablePermissions(ReferenceT)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getAccessControlT_SelectablePermissions()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='selectablePermissions' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getSelectablePermissions();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.AccessControlT#getSelectablePermissions <em>Selectable Permissions</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selectable Permissions</em>' containment reference.
	 * @see #getSelectablePermissions()
	 * @generated
	 */
	void setSelectablePermissions(ReferenceT value);

	/**
	 * Returns the value of the '<em><b>Default Permissions</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Permissions</em>' containment reference.
	 * @see #setDefaultPermissions(ReferenceT)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getAccessControlT_DefaultPermissions()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='defaultPermissions' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getDefaultPermissions();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.AccessControlT#getDefaultPermissions <em>Default Permissions</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Permissions</em>' containment reference.
	 * @see #getDefaultPermissions()
	 * @generated
	 */
	void setDefaultPermissions(ReferenceT value);

	/**
	 * Returns the value of the '<em><b>Selectable Environment Attributes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selectable Environment Attributes</em>' containment reference.
	 * @see #setSelectableEnvironmentAttributes(ReferenceT)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getAccessControlT_SelectableEnvironmentAttributes()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='selectableEnvironmentAttributes' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getSelectableEnvironmentAttributes();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.AccessControlT#getSelectableEnvironmentAttributes <em>Selectable Environment Attributes</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selectable Environment Attributes</em>' containment reference.
	 * @see #getSelectableEnvironmentAttributes()
	 * @generated
	 */
	void setSelectableEnvironmentAttributes(ReferenceT value);

	/**
	 * Returns the value of the '<em><b>Default Environment Attributes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Environment Attributes</em>' containment reference.
	 * @see #setDefaultEnvironmentAttributes(ReferenceT)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getAccessControlT_DefaultEnvironmentAttributes()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defaultEnvironmentAttributes' namespace='##targetNamespace'"
	 * @generated
	 */
	ReferenceT getDefaultEnvironmentAttributes();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.AccessControlT#getDefaultEnvironmentAttributes <em>Default Environment Attributes</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Environment Attributes</em>' containment reference.
	 * @see #getDefaultEnvironmentAttributes()
	 * @generated
	 */
	void setDefaultEnvironmentAttributes(ReferenceT value);

	/**
	 * Returns the value of the '<em><b>Access Permission Rules</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access Permission Rules</em>' containment reference.
	 * @see #setAccessPermissionRules(AccessPermissionRulesT)
	 * @see io.shell.admin.aas.abac._2._0._0Package#getAccessControlT_AccessPermissionRules()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='accessPermissionRules' namespace='##targetNamespace'"
	 * @generated
	 */
	AccessPermissionRulesT getAccessPermissionRules();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas.abac._2._0.AccessControlT#getAccessPermissionRules <em>Access Permission Rules</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access Permission Rules</em>' containment reference.
	 * @see #getAccessPermissionRules()
	 * @generated
	 */
	void setAccessPermissionRules(AccessPermissionRulesT value);

} // AccessControlT
