/**
 */
package io.shell.admin.aas._2._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aasenv T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.AasenvT#getAssetAdministrationShells <em>Asset Administration Shells</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.AasenvT#getAssets <em>Assets</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.AasenvT#getSubmodels <em>Submodels</em>}</li>
 *   <li>{@link io.shell.admin.aas._2._0.AasenvT#getConceptDescriptions <em>Concept Descriptions</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas._2._0._0Package#getAasenvT()
 * @model extendedMetaData="name='aasenv_t' kind='elementOnly'"
 * @generated
 */
public interface AasenvT extends EObject {
	/**
	 * Returns the value of the '<em><b>Asset Administration Shells</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asset Administration Shells</em>' containment reference.
	 * @see #setAssetAdministrationShells(AssetAdministrationShellsT)
	 * @see io.shell.admin.aas._2._0._0Package#getAasenvT_AssetAdministrationShells()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='assetAdministrationShells' namespace='##targetNamespace'"
	 * @generated
	 */
	AssetAdministrationShellsT getAssetAdministrationShells();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.AasenvT#getAssetAdministrationShells <em>Asset Administration Shells</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Asset Administration Shells</em>' containment reference.
	 * @see #getAssetAdministrationShells()
	 * @generated
	 */
	void setAssetAdministrationShells(AssetAdministrationShellsT value);

	/**
	 * Returns the value of the '<em><b>Assets</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assets</em>' containment reference.
	 * @see #setAssets(AssetsT)
	 * @see io.shell.admin.aas._2._0._0Package#getAasenvT_Assets()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='assets' namespace='##targetNamespace'"
	 * @generated
	 */
	AssetsT getAssets();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.AasenvT#getAssets <em>Assets</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assets</em>' containment reference.
	 * @see #getAssets()
	 * @generated
	 */
	void setAssets(AssetsT value);

	/**
	 * Returns the value of the '<em><b>Submodels</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Submodels</em>' containment reference.
	 * @see #setSubmodels(SubmodelsT)
	 * @see io.shell.admin.aas._2._0._0Package#getAasenvT_Submodels()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='submodels' namespace='##targetNamespace'"
	 * @generated
	 */
	SubmodelsT getSubmodels();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.AasenvT#getSubmodels <em>Submodels</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Submodels</em>' containment reference.
	 * @see #getSubmodels()
	 * @generated
	 */
	void setSubmodels(SubmodelsT value);

	/**
	 * Returns the value of the '<em><b>Concept Descriptions</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Concept Descriptions</em>' containment reference.
	 * @see #setConceptDescriptions(ConceptDescriptionsT)
	 * @see io.shell.admin.aas._2._0._0Package#getAasenvT_ConceptDescriptions()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='conceptDescriptions' namespace='##targetNamespace'"
	 * @generated
	 */
	ConceptDescriptionsT getConceptDescriptions();

	/**
	 * Sets the value of the '{@link io.shell.admin.aas._2._0.AasenvT#getConceptDescriptions <em>Concept Descriptions</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Concept Descriptions</em>' containment reference.
	 * @see #getConceptDescriptions()
	 * @generated
	 */
	void setConceptDescriptions(ConceptDescriptionsT value);

} // AasenvT
