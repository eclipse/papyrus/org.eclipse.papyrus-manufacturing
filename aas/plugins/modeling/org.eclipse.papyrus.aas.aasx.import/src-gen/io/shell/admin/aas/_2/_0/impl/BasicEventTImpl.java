/**
 */
package io.shell.admin.aas._2._0.impl;

import io.shell.admin.aas._2._0.BasicEventT;
import io.shell.admin.aas._2._0.ReferenceT;
import io.shell.admin.aas._2._0._0Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Basic Event T</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas._2._0.impl.BasicEventTImpl#getObserved <em>Observed</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BasicEventTImpl extends EventAbstractTImpl implements BasicEventT {
	/**
	 * The cached value of the '{@link #getObserved() <em>Observed</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObserved()
	 * @generated
	 * @ordered
	 */
	protected ReferenceT observed;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BasicEventTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.BASIC_EVENT_T;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceT getObserved() {
		return observed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObserved(ReferenceT newObserved, NotificationChain msgs) {
		ReferenceT oldObserved = observed;
		observed = newObserved;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, _0Package.BASIC_EVENT_T__OBSERVED, oldObserved, newObserved);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObserved(ReferenceT newObserved) {
		if (newObserved != observed) {
			NotificationChain msgs = null;
			if (observed != null)
				msgs = ((InternalEObject)observed).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - _0Package.BASIC_EVENT_T__OBSERVED, null, msgs);
			if (newObserved != null)
				msgs = ((InternalEObject)newObserved).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - _0Package.BASIC_EVENT_T__OBSERVED, null, msgs);
			msgs = basicSetObserved(newObserved, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, _0Package.BASIC_EVENT_T__OBSERVED, newObserved, newObserved));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case _0Package.BASIC_EVENT_T__OBSERVED:
				return basicSetObserved(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case _0Package.BASIC_EVENT_T__OBSERVED:
				return getObserved();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case _0Package.BASIC_EVENT_T__OBSERVED:
				setObserved((ReferenceT)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case _0Package.BASIC_EVENT_T__OBSERVED:
				setObserved((ReferenceT)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case _0Package.BASIC_EVENT_T__OBSERVED:
				return observed != null;
		}
		return super.eIsSet(featureID);
	}

} //BasicEventTImpl
