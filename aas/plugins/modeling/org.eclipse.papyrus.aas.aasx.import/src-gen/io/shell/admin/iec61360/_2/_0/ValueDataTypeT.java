/**
 */
package io.shell.admin.iec61360._2._0;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value Data Type T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.iec61360._2._0.ValueDataTypeT#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.iec61360._2._0._0Package#getValueDataTypeT()
 * @model extendedMetaData="name='valueDataType_t' kind='simple'"
 * @generated
 */
public interface ValueDataTypeT extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(Object)
	 * @see io.shell.admin.iec61360._2._0._0Package#getValueDataTypeT_Value()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.AnySimpleType"
	 *        extendedMetaData="name=':0' kind='simple'"
	 * @generated
	 */
	Object getValue();

	/**
	 * Sets the value of the '{@link io.shell.admin.iec61360._2._0.ValueDataTypeT#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Object value);

} // ValueDataTypeT
