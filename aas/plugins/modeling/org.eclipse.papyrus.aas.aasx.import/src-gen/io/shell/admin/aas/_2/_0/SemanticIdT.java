/**
 */
package io.shell.admin.aas._2._0;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Semantic Id T</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see io.shell.admin.aas._2._0._0Package#getSemanticIdT()
 * @model extendedMetaData="name='semanticId_t' kind='elementOnly'"
 * @generated
 */
public interface SemanticIdT extends ReferenceT {
} // SemanticIdT
