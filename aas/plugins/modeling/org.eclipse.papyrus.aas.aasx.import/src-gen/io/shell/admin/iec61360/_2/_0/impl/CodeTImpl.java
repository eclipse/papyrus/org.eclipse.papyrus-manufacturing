/**
 */
package io.shell.admin.iec61360._2._0.impl;

import io.shell.admin.iec61360._2._0.CodeT;
import io.shell.admin.iec61360._2._0._0Package;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Code T</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CodeTImpl extends MinimalEObjectImpl.Container implements CodeT {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CodeTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return _0Package.Literals.CODE_T;
	}

} //CodeTImpl
