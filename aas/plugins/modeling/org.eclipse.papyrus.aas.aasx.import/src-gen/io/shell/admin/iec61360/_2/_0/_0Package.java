/**
 */
package io.shell.admin.iec61360._2._0;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see io.shell.admin.iec61360._2._0._0Factory
 * @model kind="package"
 * @generated
 */
public interface _0Package extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "_0";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.admin-shell.io/IEC61360/2/0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "_0";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	_0Package eINSTANCE = io.shell.admin.iec61360._2._0.impl._0PackageImpl.init();

	/**
	 * The meta object id for the '{@link io.shell.admin.iec61360._2._0.impl.CodeTImpl <em>Code T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.iec61360._2._0.impl.CodeTImpl
	 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getCodeT()
	 * @generated
	 */
	int CODE_T = 0;

	/**
	 * The number of structural features of the '<em>Code T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_T_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Code T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.iec61360._2._0.impl.DataSpecificationIEC61630TImpl <em>Data Specification IEC61630T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.iec61360._2._0.impl.DataSpecificationIEC61630TImpl
	 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getDataSpecificationIEC61630T()
	 * @generated
	 */
	int DATA_SPECIFICATION_IEC61630T = 1;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Preferred Name</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T__PREFERRED_NAME = 1;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T__SHORT_NAME = 2;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T__UNIT = 3;

	/**
	 * The feature id for the '<em><b>Unit Id</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T__UNIT_ID = 4;

	/**
	 * The feature id for the '<em><b>Source Of Definition</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T__SOURCE_OF_DEFINITION = 5;

	/**
	 * The feature id for the '<em><b>Symbol</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T__SYMBOL = 6;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T__DATA_TYPE = 7;

	/**
	 * The feature id for the '<em><b>Definition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T__DEFINITION = 8;

	/**
	 * The feature id for the '<em><b>Value Format</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T__VALUE_FORMAT = 9;

	/**
	 * The feature id for the '<em><b>Value List</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T__VALUE_LIST = 10;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T__VALUE = 11;

	/**
	 * The feature id for the '<em><b>Value Id</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T__VALUE_ID = 12;

	/**
	 * The feature id for the '<em><b>Level Type</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T__LEVEL_TYPE = 13;

	/**
	 * The number of structural features of the '<em>Data Specification IEC61630T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T_FEATURE_COUNT = 14;

	/**
	 * The number of operations of the '<em>Data Specification IEC61630T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SPECIFICATION_IEC61630T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.iec61360._2._0.impl.KeysTImpl <em>Keys T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.iec61360._2._0.impl.KeysTImpl
	 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getKeysT()
	 * @generated
	 */
	int KEYS_T = 2;

	/**
	 * The feature id for the '<em><b>Key</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYS_T__KEY = 0;

	/**
	 * The number of structural features of the '<em>Keys T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYS_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Keys T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEYS_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.iec61360._2._0.impl.KeyTImpl <em>Key T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.iec61360._2._0.impl.KeyTImpl
	 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getKeyT()
	 * @generated
	 */
	int KEY_T = 3;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_T__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Id Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_T__ID_TYPE = 1;

	/**
	 * The feature id for the '<em><b>Local</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_T__LOCAL = 2;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_T__TYPE = 3;

	/**
	 * The number of structural features of the '<em>Key T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_T_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Key T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.iec61360._2._0.impl.LangStringSetTImpl <em>Lang String Set T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.iec61360._2._0.impl.LangStringSetTImpl
	 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getLangStringSetT()
	 * @generated
	 */
	int LANG_STRING_SET_T = 4;

	/**
	 * The feature id for the '<em><b>Lang String</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANG_STRING_SET_T__LANG_STRING = 0;

	/**
	 * The number of structural features of the '<em>Lang String Set T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANG_STRING_SET_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Lang String Set T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANG_STRING_SET_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.iec61360._2._0.impl.LangStringTImpl <em>Lang String T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.iec61360._2._0.impl.LangStringTImpl
	 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getLangStringT()
	 * @generated
	 */
	int LANG_STRING_T = 5;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANG_STRING_T__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Lang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANG_STRING_T__LANG = 1;

	/**
	 * The number of structural features of the '<em>Lang String T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANG_STRING_T_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Lang String T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANG_STRING_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.iec61360._2._0.impl.ReferenceTImpl <em>Reference T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.iec61360._2._0.impl.ReferenceTImpl
	 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getReferenceT()
	 * @generated
	 */
	int REFERENCE_T = 6;

	/**
	 * The feature id for the '<em><b>Keys</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_T__KEYS = 0;

	/**
	 * The number of structural features of the '<em>Reference T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Reference T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.iec61360._2._0.impl.ValueDataTypeTImpl <em>Value Data Type T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.iec61360._2._0.impl.ValueDataTypeTImpl
	 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getValueDataTypeT()
	 * @generated
	 */
	int VALUE_DATA_TYPE_T = 7;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_DATA_TYPE_T__VALUE = 0;

	/**
	 * The number of structural features of the '<em>Value Data Type T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_DATA_TYPE_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Value Data Type T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_DATA_TYPE_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.iec61360._2._0.impl.ValueListTImpl <em>Value List T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.iec61360._2._0.impl.ValueListTImpl
	 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getValueListT()
	 * @generated
	 */
	int VALUE_LIST_T = 8;

	/**
	 * The feature id for the '<em><b>Value Reference Pair</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_LIST_T__VALUE_REFERENCE_PAIR = 0;

	/**
	 * The number of structural features of the '<em>Value List T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_LIST_T_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Value List T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_LIST_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.iec61360._2._0.impl.ValueReferencePairTImpl <em>Value Reference Pair T</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.iec61360._2._0.impl.ValueReferencePairTImpl
	 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getValueReferencePairT()
	 * @generated
	 */
	int VALUE_REFERENCE_PAIR_T = 9;

	/**
	 * The feature id for the '<em><b>Value Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_REFERENCE_PAIR_T__VALUE_ID = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_REFERENCE_PAIR_T__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Value Reference Pair T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_REFERENCE_PAIR_T_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Value Reference Pair T</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_REFERENCE_PAIR_T_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.iec61360._2._0.impl.DocumentRootImpl <em>Document Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.iec61360._2._0.impl.DocumentRootImpl
	 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getDocumentRoot()
	 * @generated
	 */
	int DOCUMENT_ROOT = 10;

	/**
	 * The feature id for the '<em><b>Mixed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__MIXED = 0;

	/**
	 * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

	/**
	 * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

	/**
	 * The feature id for the '<em><b>Key</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__KEY = 3;

	/**
	 * The number of structural features of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link io.shell.admin.iec61360._2._0.DataTypeIEC61360T <em>Data Type IEC61360T</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.iec61360._2._0.DataTypeIEC61360T
	 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getDataTypeIEC61360T()
	 * @generated
	 */
	int DATA_TYPE_IEC61360T = 11;

	/**
	 * The meta object id for the '{@link io.shell.admin.iec61360._2._0.IdTypeType <em>Id Type Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.iec61360._2._0.IdTypeType
	 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getIdTypeType()
	 * @generated
	 */
	int ID_TYPE_TYPE = 12;

	/**
	 * The meta object id for the '{@link io.shell.admin.iec61360._2._0.LevelTypeT <em>Level Type T</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.iec61360._2._0.LevelTypeT
	 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getLevelTypeT()
	 * @generated
	 */
	int LEVEL_TYPE_T = 13;

	/**
	 * The meta object id for the '{@link io.shell.admin.iec61360._2._0.TypeType <em>Type Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.iec61360._2._0.TypeType
	 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getTypeType()
	 * @generated
	 */
	int TYPE_TYPE = 14;

	/**
	 * The meta object id for the '<em>Data Type IEC61360T Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.iec61360._2._0.DataTypeIEC61360T
	 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getDataTypeIEC61360TObject()
	 * @generated
	 */
	int DATA_TYPE_IEC61360T_OBJECT = 15;

	/**
	 * The meta object id for the '<em>Id Type Type Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.iec61360._2._0.IdTypeType
	 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getIdTypeTypeObject()
	 * @generated
	 */
	int ID_TYPE_TYPE_OBJECT = 16;

	/**
	 * The meta object id for the '<em>Level Type TObject</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.iec61360._2._0.LevelTypeT
	 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getLevelTypeTObject()
	 * @generated
	 */
	int LEVEL_TYPE_TOBJECT = 17;

	/**
	 * The meta object id for the '<em>Type Type Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see io.shell.admin.iec61360._2._0.TypeType
	 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getTypeTypeObject()
	 * @generated
	 */
	int TYPE_TYPE_OBJECT = 18;


	/**
	 * Returns the meta object for class '{@link io.shell.admin.iec61360._2._0.CodeT <em>Code T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Code T</em>'.
	 * @see io.shell.admin.iec61360._2._0.CodeT
	 * @generated
	 */
	EClass getCodeT();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T <em>Data Specification IEC61630T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Specification IEC61630T</em>'.
	 * @see io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T
	 * @generated
	 */
	EClass getDataSpecificationIEC61630T();

	/**
	 * Returns the meta object for the attribute list '{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getGroup()
	 * @see #getDataSpecificationIEC61630T()
	 * @generated
	 */
	EAttribute getDataSpecificationIEC61630T_Group();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getPreferredName <em>Preferred Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Preferred Name</em>'.
	 * @see io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getPreferredName()
	 * @see #getDataSpecificationIEC61630T()
	 * @generated
	 */
	EReference getDataSpecificationIEC61630T_PreferredName();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getShortName <em>Short Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Short Name</em>'.
	 * @see io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getShortName()
	 * @see #getDataSpecificationIEC61630T()
	 * @generated
	 */
	EReference getDataSpecificationIEC61630T_ShortName();

	/**
	 * Returns the meta object for the attribute list '{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getUnit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Unit</em>'.
	 * @see io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getUnit()
	 * @see #getDataSpecificationIEC61630T()
	 * @generated
	 */
	EAttribute getDataSpecificationIEC61630T_Unit();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getUnitId <em>Unit Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Unit Id</em>'.
	 * @see io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getUnitId()
	 * @see #getDataSpecificationIEC61630T()
	 * @generated
	 */
	EReference getDataSpecificationIEC61630T_UnitId();

	/**
	 * Returns the meta object for the attribute list '{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getSourceOfDefinition <em>Source Of Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Source Of Definition</em>'.
	 * @see io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getSourceOfDefinition()
	 * @see #getDataSpecificationIEC61630T()
	 * @generated
	 */
	EAttribute getDataSpecificationIEC61630T_SourceOfDefinition();

	/**
	 * Returns the meta object for the attribute list '{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getSymbol <em>Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Symbol</em>'.
	 * @see io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getSymbol()
	 * @see #getDataSpecificationIEC61630T()
	 * @generated
	 */
	EAttribute getDataSpecificationIEC61630T_Symbol();

	/**
	 * Returns the meta object for the attribute list '{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getDataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Data Type</em>'.
	 * @see io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getDataType()
	 * @see #getDataSpecificationIEC61630T()
	 * @generated
	 */
	EAttribute getDataSpecificationIEC61630T_DataType();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getDefinition <em>Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Definition</em>'.
	 * @see io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getDefinition()
	 * @see #getDataSpecificationIEC61630T()
	 * @generated
	 */
	EReference getDataSpecificationIEC61630T_Definition();

	/**
	 * Returns the meta object for the attribute list '{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getValueFormat <em>Value Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Value Format</em>'.
	 * @see io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getValueFormat()
	 * @see #getDataSpecificationIEC61630T()
	 * @generated
	 */
	EAttribute getDataSpecificationIEC61630T_ValueFormat();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getValueList <em>Value List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Value List</em>'.
	 * @see io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getValueList()
	 * @see #getDataSpecificationIEC61630T()
	 * @generated
	 */
	EReference getDataSpecificationIEC61630T_ValueList();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Value</em>'.
	 * @see io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getValue()
	 * @see #getDataSpecificationIEC61630T()
	 * @generated
	 */
	EReference getDataSpecificationIEC61630T_Value();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getValueId <em>Value Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Value Id</em>'.
	 * @see io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getValueId()
	 * @see #getDataSpecificationIEC61630T()
	 * @generated
	 */
	EReference getDataSpecificationIEC61630T_ValueId();

	/**
	 * Returns the meta object for the attribute list '{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getLevelType <em>Level Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Level Type</em>'.
	 * @see io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T#getLevelType()
	 * @see #getDataSpecificationIEC61630T()
	 * @generated
	 */
	EAttribute getDataSpecificationIEC61630T_LevelType();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.iec61360._2._0.KeysT <em>Keys T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Keys T</em>'.
	 * @see io.shell.admin.iec61360._2._0.KeysT
	 * @generated
	 */
	EClass getKeysT();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.iec61360._2._0.KeysT#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Key</em>'.
	 * @see io.shell.admin.iec61360._2._0.KeysT#getKey()
	 * @see #getKeysT()
	 * @generated
	 */
	EReference getKeysT_Key();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.iec61360._2._0.KeyT <em>Key T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Key T</em>'.
	 * @see io.shell.admin.iec61360._2._0.KeyT
	 * @generated
	 */
	EClass getKeyT();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.iec61360._2._0.KeyT#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see io.shell.admin.iec61360._2._0.KeyT#getValue()
	 * @see #getKeyT()
	 * @generated
	 */
	EAttribute getKeyT_Value();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.iec61360._2._0.KeyT#getIdType <em>Id Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id Type</em>'.
	 * @see io.shell.admin.iec61360._2._0.KeyT#getIdType()
	 * @see #getKeyT()
	 * @generated
	 */
	EAttribute getKeyT_IdType();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.iec61360._2._0.KeyT#isLocal <em>Local</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Local</em>'.
	 * @see io.shell.admin.iec61360._2._0.KeyT#isLocal()
	 * @see #getKeyT()
	 * @generated
	 */
	EAttribute getKeyT_Local();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.iec61360._2._0.KeyT#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see io.shell.admin.iec61360._2._0.KeyT#getType()
	 * @see #getKeyT()
	 * @generated
	 */
	EAttribute getKeyT_Type();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.iec61360._2._0.LangStringSetT <em>Lang String Set T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lang String Set T</em>'.
	 * @see io.shell.admin.iec61360._2._0.LangStringSetT
	 * @generated
	 */
	EClass getLangStringSetT();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.iec61360._2._0.LangStringSetT#getLangString <em>Lang String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Lang String</em>'.
	 * @see io.shell.admin.iec61360._2._0.LangStringSetT#getLangString()
	 * @see #getLangStringSetT()
	 * @generated
	 */
	EReference getLangStringSetT_LangString();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.iec61360._2._0.LangStringT <em>Lang String T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lang String T</em>'.
	 * @see io.shell.admin.iec61360._2._0.LangStringT
	 * @generated
	 */
	EClass getLangStringT();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.iec61360._2._0.LangStringT#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see io.shell.admin.iec61360._2._0.LangStringT#getValue()
	 * @see #getLangStringT()
	 * @generated
	 */
	EAttribute getLangStringT_Value();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.iec61360._2._0.LangStringT#getLang <em>Lang</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lang</em>'.
	 * @see io.shell.admin.iec61360._2._0.LangStringT#getLang()
	 * @see #getLangStringT()
	 * @generated
	 */
	EAttribute getLangStringT_Lang();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.iec61360._2._0.ReferenceT <em>Reference T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference T</em>'.
	 * @see io.shell.admin.iec61360._2._0.ReferenceT
	 * @generated
	 */
	EClass getReferenceT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.iec61360._2._0.ReferenceT#getKeys <em>Keys</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Keys</em>'.
	 * @see io.shell.admin.iec61360._2._0.ReferenceT#getKeys()
	 * @see #getReferenceT()
	 * @generated
	 */
	EReference getReferenceT_Keys();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.iec61360._2._0.ValueDataTypeT <em>Value Data Type T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value Data Type T</em>'.
	 * @see io.shell.admin.iec61360._2._0.ValueDataTypeT
	 * @generated
	 */
	EClass getValueDataTypeT();

	/**
	 * Returns the meta object for the attribute '{@link io.shell.admin.iec61360._2._0.ValueDataTypeT#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see io.shell.admin.iec61360._2._0.ValueDataTypeT#getValue()
	 * @see #getValueDataTypeT()
	 * @generated
	 */
	EAttribute getValueDataTypeT_Value();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.iec61360._2._0.ValueListT <em>Value List T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value List T</em>'.
	 * @see io.shell.admin.iec61360._2._0.ValueListT
	 * @generated
	 */
	EClass getValueListT();

	/**
	 * Returns the meta object for the containment reference list '{@link io.shell.admin.iec61360._2._0.ValueListT#getValueReferencePair <em>Value Reference Pair</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Value Reference Pair</em>'.
	 * @see io.shell.admin.iec61360._2._0.ValueListT#getValueReferencePair()
	 * @see #getValueListT()
	 * @generated
	 */
	EReference getValueListT_ValueReferencePair();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.iec61360._2._0.ValueReferencePairT <em>Value Reference Pair T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value Reference Pair T</em>'.
	 * @see io.shell.admin.iec61360._2._0.ValueReferencePairT
	 * @generated
	 */
	EClass getValueReferencePairT();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.iec61360._2._0.ValueReferencePairT#getValueId <em>Value Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value Id</em>'.
	 * @see io.shell.admin.iec61360._2._0.ValueReferencePairT#getValueId()
	 * @see #getValueReferencePairT()
	 * @generated
	 */
	EReference getValueReferencePairT_ValueId();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.iec61360._2._0.ValueReferencePairT#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see io.shell.admin.iec61360._2._0.ValueReferencePairT#getValue()
	 * @see #getValueReferencePairT()
	 * @generated
	 */
	EReference getValueReferencePairT_Value();

	/**
	 * Returns the meta object for class '{@link io.shell.admin.iec61360._2._0.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Document Root</em>'.
	 * @see io.shell.admin.iec61360._2._0.DocumentRoot
	 * @generated
	 */
	EClass getDocumentRoot();

	/**
	 * Returns the meta object for the attribute list '{@link io.shell.admin.iec61360._2._0.DocumentRoot#getMixed <em>Mixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Mixed</em>'.
	 * @see io.shell.admin.iec61360._2._0.DocumentRoot#getMixed()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EAttribute getDocumentRoot_Mixed();

	/**
	 * Returns the meta object for the map '{@link io.shell.admin.iec61360._2._0.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
	 * @see io.shell.admin.iec61360._2._0.DocumentRoot#getXMLNSPrefixMap()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XMLNSPrefixMap();

	/**
	 * Returns the meta object for the map '{@link io.shell.admin.iec61360._2._0.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XSI Schema Location</em>'.
	 * @see io.shell.admin.iec61360._2._0.DocumentRoot#getXSISchemaLocation()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XSISchemaLocation();

	/**
	 * Returns the meta object for the containment reference '{@link io.shell.admin.iec61360._2._0.DocumentRoot#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Key</em>'.
	 * @see io.shell.admin.iec61360._2._0.DocumentRoot#getKey()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Key();

	/**
	 * Returns the meta object for enum '{@link io.shell.admin.iec61360._2._0.DataTypeIEC61360T <em>Data Type IEC61360T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Data Type IEC61360T</em>'.
	 * @see io.shell.admin.iec61360._2._0.DataTypeIEC61360T
	 * @generated
	 */
	EEnum getDataTypeIEC61360T();

	/**
	 * Returns the meta object for enum '{@link io.shell.admin.iec61360._2._0.IdTypeType <em>Id Type Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Id Type Type</em>'.
	 * @see io.shell.admin.iec61360._2._0.IdTypeType
	 * @generated
	 */
	EEnum getIdTypeType();

	/**
	 * Returns the meta object for enum '{@link io.shell.admin.iec61360._2._0.LevelTypeT <em>Level Type T</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Level Type T</em>'.
	 * @see io.shell.admin.iec61360._2._0.LevelTypeT
	 * @generated
	 */
	EEnum getLevelTypeT();

	/**
	 * Returns the meta object for enum '{@link io.shell.admin.iec61360._2._0.TypeType <em>Type Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Type Type</em>'.
	 * @see io.shell.admin.iec61360._2._0.TypeType
	 * @generated
	 */
	EEnum getTypeType();

	/**
	 * Returns the meta object for data type '{@link io.shell.admin.iec61360._2._0.DataTypeIEC61360T <em>Data Type IEC61360T Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Data Type IEC61360T Object</em>'.
	 * @see io.shell.admin.iec61360._2._0.DataTypeIEC61360T
	 * @model instanceClass="io.shell.admin.iec61360._2._0.DataTypeIEC61360T"
	 *        extendedMetaData="name='dataTypeIEC61360_t:Object' baseType='dataTypeIEC61360_t'"
	 * @generated
	 */
	EDataType getDataTypeIEC61360TObject();

	/**
	 * Returns the meta object for data type '{@link io.shell.admin.iec61360._2._0.IdTypeType <em>Id Type Type Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Id Type Type Object</em>'.
	 * @see io.shell.admin.iec61360._2._0.IdTypeType
	 * @model instanceClass="io.shell.admin.iec61360._2._0.IdTypeType"
	 *        extendedMetaData="name='idType_._type:Object' baseType='idType_._type'"
	 * @generated
	 */
	EDataType getIdTypeTypeObject();

	/**
	 * Returns the meta object for data type '{@link io.shell.admin.iec61360._2._0.LevelTypeT <em>Level Type TObject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Level Type TObject</em>'.
	 * @see io.shell.admin.iec61360._2._0.LevelTypeT
	 * @model instanceClass="io.shell.admin.iec61360._2._0.LevelTypeT"
	 *        extendedMetaData="name='levelType_t:Object' baseType='levelType_t'"
	 * @generated
	 */
	EDataType getLevelTypeTObject();

	/**
	 * Returns the meta object for data type '{@link io.shell.admin.iec61360._2._0.TypeType <em>Type Type Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Type Type Object</em>'.
	 * @see io.shell.admin.iec61360._2._0.TypeType
	 * @model instanceClass="io.shell.admin.iec61360._2._0.TypeType"
	 *        extendedMetaData="name='type_._type:Object' baseType='type_._type'"
	 * @generated
	 */
	EDataType getTypeTypeObject();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	_0Factory get_0Factory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link io.shell.admin.iec61360._2._0.impl.CodeTImpl <em>Code T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.iec61360._2._0.impl.CodeTImpl
		 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getCodeT()
		 * @generated
		 */
		EClass CODE_T = eINSTANCE.getCodeT();

		/**
		 * The meta object literal for the '{@link io.shell.admin.iec61360._2._0.impl.DataSpecificationIEC61630TImpl <em>Data Specification IEC61630T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.iec61360._2._0.impl.DataSpecificationIEC61630TImpl
		 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getDataSpecificationIEC61630T()
		 * @generated
		 */
		EClass DATA_SPECIFICATION_IEC61630T = eINSTANCE.getDataSpecificationIEC61630T();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_SPECIFICATION_IEC61630T__GROUP = eINSTANCE.getDataSpecificationIEC61630T_Group();

		/**
		 * The meta object literal for the '<em><b>Preferred Name</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SPECIFICATION_IEC61630T__PREFERRED_NAME = eINSTANCE.getDataSpecificationIEC61630T_PreferredName();

		/**
		 * The meta object literal for the '<em><b>Short Name</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SPECIFICATION_IEC61630T__SHORT_NAME = eINSTANCE.getDataSpecificationIEC61630T_ShortName();

		/**
		 * The meta object literal for the '<em><b>Unit</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_SPECIFICATION_IEC61630T__UNIT = eINSTANCE.getDataSpecificationIEC61630T_Unit();

		/**
		 * The meta object literal for the '<em><b>Unit Id</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SPECIFICATION_IEC61630T__UNIT_ID = eINSTANCE.getDataSpecificationIEC61630T_UnitId();

		/**
		 * The meta object literal for the '<em><b>Source Of Definition</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_SPECIFICATION_IEC61630T__SOURCE_OF_DEFINITION = eINSTANCE.getDataSpecificationIEC61630T_SourceOfDefinition();

		/**
		 * The meta object literal for the '<em><b>Symbol</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_SPECIFICATION_IEC61630T__SYMBOL = eINSTANCE.getDataSpecificationIEC61630T_Symbol();

		/**
		 * The meta object literal for the '<em><b>Data Type</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_SPECIFICATION_IEC61630T__DATA_TYPE = eINSTANCE.getDataSpecificationIEC61630T_DataType();

		/**
		 * The meta object literal for the '<em><b>Definition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SPECIFICATION_IEC61630T__DEFINITION = eINSTANCE.getDataSpecificationIEC61630T_Definition();

		/**
		 * The meta object literal for the '<em><b>Value Format</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_SPECIFICATION_IEC61630T__VALUE_FORMAT = eINSTANCE.getDataSpecificationIEC61630T_ValueFormat();

		/**
		 * The meta object literal for the '<em><b>Value List</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SPECIFICATION_IEC61630T__VALUE_LIST = eINSTANCE.getDataSpecificationIEC61630T_ValueList();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SPECIFICATION_IEC61630T__VALUE = eINSTANCE.getDataSpecificationIEC61630T_Value();

		/**
		 * The meta object literal for the '<em><b>Value Id</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SPECIFICATION_IEC61630T__VALUE_ID = eINSTANCE.getDataSpecificationIEC61630T_ValueId();

		/**
		 * The meta object literal for the '<em><b>Level Type</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_SPECIFICATION_IEC61630T__LEVEL_TYPE = eINSTANCE.getDataSpecificationIEC61630T_LevelType();

		/**
		 * The meta object literal for the '{@link io.shell.admin.iec61360._2._0.impl.KeysTImpl <em>Keys T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.iec61360._2._0.impl.KeysTImpl
		 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getKeysT()
		 * @generated
		 */
		EClass KEYS_T = eINSTANCE.getKeysT();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference KEYS_T__KEY = eINSTANCE.getKeysT_Key();

		/**
		 * The meta object literal for the '{@link io.shell.admin.iec61360._2._0.impl.KeyTImpl <em>Key T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.iec61360._2._0.impl.KeyTImpl
		 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getKeyT()
		 * @generated
		 */
		EClass KEY_T = eINSTANCE.getKeyT();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute KEY_T__VALUE = eINSTANCE.getKeyT_Value();

		/**
		 * The meta object literal for the '<em><b>Id Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute KEY_T__ID_TYPE = eINSTANCE.getKeyT_IdType();

		/**
		 * The meta object literal for the '<em><b>Local</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute KEY_T__LOCAL = eINSTANCE.getKeyT_Local();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute KEY_T__TYPE = eINSTANCE.getKeyT_Type();

		/**
		 * The meta object literal for the '{@link io.shell.admin.iec61360._2._0.impl.LangStringSetTImpl <em>Lang String Set T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.iec61360._2._0.impl.LangStringSetTImpl
		 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getLangStringSetT()
		 * @generated
		 */
		EClass LANG_STRING_SET_T = eINSTANCE.getLangStringSetT();

		/**
		 * The meta object literal for the '<em><b>Lang String</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LANG_STRING_SET_T__LANG_STRING = eINSTANCE.getLangStringSetT_LangString();

		/**
		 * The meta object literal for the '{@link io.shell.admin.iec61360._2._0.impl.LangStringTImpl <em>Lang String T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.iec61360._2._0.impl.LangStringTImpl
		 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getLangStringT()
		 * @generated
		 */
		EClass LANG_STRING_T = eINSTANCE.getLangStringT();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LANG_STRING_T__VALUE = eINSTANCE.getLangStringT_Value();

		/**
		 * The meta object literal for the '<em><b>Lang</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LANG_STRING_T__LANG = eINSTANCE.getLangStringT_Lang();

		/**
		 * The meta object literal for the '{@link io.shell.admin.iec61360._2._0.impl.ReferenceTImpl <em>Reference T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.iec61360._2._0.impl.ReferenceTImpl
		 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getReferenceT()
		 * @generated
		 */
		EClass REFERENCE_T = eINSTANCE.getReferenceT();

		/**
		 * The meta object literal for the '<em><b>Keys</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE_T__KEYS = eINSTANCE.getReferenceT_Keys();

		/**
		 * The meta object literal for the '{@link io.shell.admin.iec61360._2._0.impl.ValueDataTypeTImpl <em>Value Data Type T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.iec61360._2._0.impl.ValueDataTypeTImpl
		 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getValueDataTypeT()
		 * @generated
		 */
		EClass VALUE_DATA_TYPE_T = eINSTANCE.getValueDataTypeT();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VALUE_DATA_TYPE_T__VALUE = eINSTANCE.getValueDataTypeT_Value();

		/**
		 * The meta object literal for the '{@link io.shell.admin.iec61360._2._0.impl.ValueListTImpl <em>Value List T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.iec61360._2._0.impl.ValueListTImpl
		 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getValueListT()
		 * @generated
		 */
		EClass VALUE_LIST_T = eINSTANCE.getValueListT();

		/**
		 * The meta object literal for the '<em><b>Value Reference Pair</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VALUE_LIST_T__VALUE_REFERENCE_PAIR = eINSTANCE.getValueListT_ValueReferencePair();

		/**
		 * The meta object literal for the '{@link io.shell.admin.iec61360._2._0.impl.ValueReferencePairTImpl <em>Value Reference Pair T</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.iec61360._2._0.impl.ValueReferencePairTImpl
		 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getValueReferencePairT()
		 * @generated
		 */
		EClass VALUE_REFERENCE_PAIR_T = eINSTANCE.getValueReferencePairT();

		/**
		 * The meta object literal for the '<em><b>Value Id</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VALUE_REFERENCE_PAIR_T__VALUE_ID = eINSTANCE.getValueReferencePairT_ValueId();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VALUE_REFERENCE_PAIR_T__VALUE = eINSTANCE.getValueReferencePairT_Value();

		/**
		 * The meta object literal for the '{@link io.shell.admin.iec61360._2._0.impl.DocumentRootImpl <em>Document Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.iec61360._2._0.impl.DocumentRootImpl
		 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getDocumentRoot()
		 * @generated
		 */
		EClass DOCUMENT_ROOT = eINSTANCE.getDocumentRoot();

		/**
		 * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENT_ROOT__MIXED = eINSTANCE.getDocumentRoot_Mixed();

		/**
		 * The meta object literal for the '<em><b>XMLNS Prefix Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XMLNS_PREFIX_MAP = eINSTANCE.getDocumentRoot_XMLNSPrefixMap();

		/**
		 * The meta object literal for the '<em><b>XSI Schema Location</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = eINSTANCE.getDocumentRoot_XSISchemaLocation();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__KEY = eINSTANCE.getDocumentRoot_Key();

		/**
		 * The meta object literal for the '{@link io.shell.admin.iec61360._2._0.DataTypeIEC61360T <em>Data Type IEC61360T</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.iec61360._2._0.DataTypeIEC61360T
		 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getDataTypeIEC61360T()
		 * @generated
		 */
		EEnum DATA_TYPE_IEC61360T = eINSTANCE.getDataTypeIEC61360T();

		/**
		 * The meta object literal for the '{@link io.shell.admin.iec61360._2._0.IdTypeType <em>Id Type Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.iec61360._2._0.IdTypeType
		 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getIdTypeType()
		 * @generated
		 */
		EEnum ID_TYPE_TYPE = eINSTANCE.getIdTypeType();

		/**
		 * The meta object literal for the '{@link io.shell.admin.iec61360._2._0.LevelTypeT <em>Level Type T</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.iec61360._2._0.LevelTypeT
		 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getLevelTypeT()
		 * @generated
		 */
		EEnum LEVEL_TYPE_T = eINSTANCE.getLevelTypeT();

		/**
		 * The meta object literal for the '{@link io.shell.admin.iec61360._2._0.TypeType <em>Type Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.iec61360._2._0.TypeType
		 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getTypeType()
		 * @generated
		 */
		EEnum TYPE_TYPE = eINSTANCE.getTypeType();

		/**
		 * The meta object literal for the '<em>Data Type IEC61360T Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.iec61360._2._0.DataTypeIEC61360T
		 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getDataTypeIEC61360TObject()
		 * @generated
		 */
		EDataType DATA_TYPE_IEC61360T_OBJECT = eINSTANCE.getDataTypeIEC61360TObject();

		/**
		 * The meta object literal for the '<em>Id Type Type Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.iec61360._2._0.IdTypeType
		 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getIdTypeTypeObject()
		 * @generated
		 */
		EDataType ID_TYPE_TYPE_OBJECT = eINSTANCE.getIdTypeTypeObject();

		/**
		 * The meta object literal for the '<em>Level Type TObject</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.iec61360._2._0.LevelTypeT
		 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getLevelTypeTObject()
		 * @generated
		 */
		EDataType LEVEL_TYPE_TOBJECT = eINSTANCE.getLevelTypeTObject();

		/**
		 * The meta object literal for the '<em>Type Type Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see io.shell.admin.iec61360._2._0.TypeType
		 * @see io.shell.admin.iec61360._2._0.impl._0PackageImpl#getTypeTypeObject()
		 * @generated
		 */
		EDataType TYPE_TYPE_OBJECT = eINSTANCE.getTypeTypeObject();

	}

} //_0Package
