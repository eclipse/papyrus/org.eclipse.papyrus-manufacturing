/**
 */
package io.shell.admin.iec61360._2._0.util;

import io.shell.admin.iec61360._2._0.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see io.shell.admin.iec61360._2._0._0Package
 * @generated
 */
public class _0AdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static _0Package modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public _0AdapterFactory() {
		if (modelPackage == null) {
			modelPackage = _0Package.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected _0Switch<Adapter> modelSwitch =
		new _0Switch<Adapter>() {
			@Override
			public Adapter caseCodeT(CodeT object) {
				return createCodeTAdapter();
			}
			@Override
			public Adapter caseDataSpecificationIEC61630T(DataSpecificationIEC61630T object) {
				return createDataSpecificationIEC61630TAdapter();
			}
			@Override
			public Adapter caseKeysT(KeysT object) {
				return createKeysTAdapter();
			}
			@Override
			public Adapter caseKeyT(KeyT object) {
				return createKeyTAdapter();
			}
			@Override
			public Adapter caseLangStringSetT(LangStringSetT object) {
				return createLangStringSetTAdapter();
			}
			@Override
			public Adapter caseLangStringT(LangStringT object) {
				return createLangStringTAdapter();
			}
			@Override
			public Adapter caseReferenceT(ReferenceT object) {
				return createReferenceTAdapter();
			}
			@Override
			public Adapter caseValueDataTypeT(ValueDataTypeT object) {
				return createValueDataTypeTAdapter();
			}
			@Override
			public Adapter caseValueListT(ValueListT object) {
				return createValueListTAdapter();
			}
			@Override
			public Adapter caseValueReferencePairT(ValueReferencePairT object) {
				return createValueReferencePairTAdapter();
			}
			@Override
			public Adapter caseDocumentRoot(DocumentRoot object) {
				return createDocumentRootAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.iec61360._2._0.CodeT <em>Code T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.iec61360._2._0.CodeT
	 * @generated
	 */
	public Adapter createCodeTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T <em>Data Specification IEC61630T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.iec61360._2._0.DataSpecificationIEC61630T
	 * @generated
	 */
	public Adapter createDataSpecificationIEC61630TAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.iec61360._2._0.KeysT <em>Keys T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.iec61360._2._0.KeysT
	 * @generated
	 */
	public Adapter createKeysTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.iec61360._2._0.KeyT <em>Key T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.iec61360._2._0.KeyT
	 * @generated
	 */
	public Adapter createKeyTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.iec61360._2._0.LangStringSetT <em>Lang String Set T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.iec61360._2._0.LangStringSetT
	 * @generated
	 */
	public Adapter createLangStringSetTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.iec61360._2._0.LangStringT <em>Lang String T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.iec61360._2._0.LangStringT
	 * @generated
	 */
	public Adapter createLangStringTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.iec61360._2._0.ReferenceT <em>Reference T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.iec61360._2._0.ReferenceT
	 * @generated
	 */
	public Adapter createReferenceTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.iec61360._2._0.ValueDataTypeT <em>Value Data Type T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.iec61360._2._0.ValueDataTypeT
	 * @generated
	 */
	public Adapter createValueDataTypeTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.iec61360._2._0.ValueListT <em>Value List T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.iec61360._2._0.ValueListT
	 * @generated
	 */
	public Adapter createValueListTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.iec61360._2._0.ValueReferencePairT <em>Value Reference Pair T</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.iec61360._2._0.ValueReferencePairT
	 * @generated
	 */
	public Adapter createValueReferencePairTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link io.shell.admin.iec61360._2._0.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see io.shell.admin.iec61360._2._0.DocumentRoot
	 * @generated
	 */
	public Adapter createDocumentRootAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //_0AdapterFactory
