/**
 */
package io.shell.admin.aas.abac._2._0;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Certificates T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.aas.abac._2._0.CertificatesT#getCertificate <em>Certificate</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.aas.abac._2._0._0Package#getCertificatesT()
 * @model extendedMetaData="name='certificates_t' kind='elementOnly'"
 * @generated
 */
public interface CertificatesT extends EObject {
	/**
	 * Returns the value of the '<em><b>Certificate</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.aas.abac._2._0.CertificateT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Certificate</em>' containment reference list.
	 * @see io.shell.admin.aas.abac._2._0._0Package#getCertificatesT_Certificate()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='certificate' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<CertificateT> getCertificate();

} // CertificatesT
