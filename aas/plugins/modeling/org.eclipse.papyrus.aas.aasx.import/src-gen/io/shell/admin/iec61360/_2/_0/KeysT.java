/**
 */
package io.shell.admin.iec61360._2._0;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Keys T</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link io.shell.admin.iec61360._2._0.KeysT#getKey <em>Key</em>}</li>
 * </ul>
 *
 * @see io.shell.admin.iec61360._2._0._0Package#getKeysT()
 * @model extendedMetaData="name='keys_t' kind='elementOnly'"
 * @generated
 */
public interface KeysT extends EObject {
	/**
	 * Returns the value of the '<em><b>Key</b></em>' containment reference list.
	 * The list contents are of type {@link io.shell.admin.iec61360._2._0.KeyT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' containment reference list.
	 * @see io.shell.admin.iec61360._2._0._0Package#getKeysT_Key()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='key' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<KeyT> getKey();

} // KeysT
