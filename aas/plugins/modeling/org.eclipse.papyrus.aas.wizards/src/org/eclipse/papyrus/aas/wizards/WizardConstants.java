/*****************************************************************************
 * Copyright (c) 2019 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   
 *
 *****************************************************************************/

package org.eclipse.papyrus.aas.wizards;


public class WizardConstants {

	public static final String AAS_CONTEXT = "org.eclipse.papyrus.aas.ml"; //$NON-NLS-1$
	public static final String VIEWPOINT_PREFIX = "org.eclipse.papyrus.aas."; //$NON-NLS-1$
	public static final String AAS_VIEWPOINT = "viewpoint"; //$NON-NLS-1$
}
