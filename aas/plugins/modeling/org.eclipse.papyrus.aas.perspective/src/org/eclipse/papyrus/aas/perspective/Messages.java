package org.eclipse.papyrus.aas.perspective;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.papyrus.aas.perspective.messages"; //$NON-NLS-1$
	public static String PapyrusAASPerspective_model;
	public static String PapyrusAASPerspective_project;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
