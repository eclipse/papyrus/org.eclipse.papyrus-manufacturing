/**
 */
package elementtypefilter;

import org.eclipse.papyrus.infra.filters.Filter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parent Matcher Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link elementtypefilter.ParentMatcherFilter#getParentType <em>Parent Type</em>}</li>
 *   <li>{@link elementtypefilter.ParentMatcherFilter#isSubtypesCheck <em>Subtypes Check</em>}</li>
 * </ul>
 *
 * @see elementtypefilter.ElementtypefilterPackage#getParentMatcherFilter()
 * @model
 * @generated
 */
public interface ParentMatcherFilter extends Filter {
	/**
	 * Returns the value of the '<em><b>Parent Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Type</em>' attribute.
	 * @see #setParentType(String)
	 * @see elementtypefilter.ElementtypefilterPackage#getParentMatcherFilter_ParentType()
	 * @model required="true"
	 * @generated
	 */
	String getParentType();

	/**
	 * Sets the value of the '{@link elementtypefilter.ParentMatcherFilter#getParentType <em>Parent Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Type</em>' attribute.
	 * @see #getParentType()
	 * @generated
	 */
	void setParentType(String value);

	/**
	 * Returns the value of the '<em><b>Subtypes Check</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subtypes Check</em>' attribute.
	 * @see #setSubtypesCheck(boolean)
	 * @see elementtypefilter.ElementtypefilterPackage#getParentMatcherFilter_SubtypesCheck()
	 * @model
	 * @generated
	 */
	boolean isSubtypesCheck();

	/**
	 * Sets the value of the '{@link elementtypefilter.ParentMatcherFilter#isSubtypesCheck <em>Subtypes Check</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Subtypes Check</em>' attribute.
	 * @see #isSubtypesCheck()
	 * @generated
	 */
	void setSubtypesCheck(boolean value);

} // ParentMatcherFilter
