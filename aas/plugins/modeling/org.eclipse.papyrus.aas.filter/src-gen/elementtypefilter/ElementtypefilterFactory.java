/**
 */
package elementtypefilter;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see elementtypefilter.ElementtypefilterPackage
 * @generated
 */
public interface ElementtypefilterFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ElementtypefilterFactory eINSTANCE = elementtypefilter.impl.ElementtypefilterFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Parent Matcher Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parent Matcher Filter</em>'.
	 * @generated
	 */
	ParentMatcherFilter createParentMatcherFilter();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ElementtypefilterPackage getElementtypefilterPackage();

} //ElementtypefilterFactory
