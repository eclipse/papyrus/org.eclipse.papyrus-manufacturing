/**
 */
package elementtypefilter.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import elementtypefilter.ElementtypefilterFactory;
import elementtypefilter.ElementtypefilterPackage;
import elementtypefilter.ParentMatcherFilter;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class ElementtypefilterFactoryImpl extends EFactoryImpl implements ElementtypefilterFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static ElementtypefilterFactory init() {
		try {
			ElementtypefilterFactory theElementtypefilterFactory = (ElementtypefilterFactory) EPackage.Registry.INSTANCE.getEFactory(ElementtypefilterPackage.eNS_URI);
			if (theElementtypefilterFactory != null) {
				return theElementtypefilterFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ElementtypefilterFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ElementtypefilterFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case ElementtypefilterPackage.PARENT_MATCHER_FILTER:
			return createParentMatcherFilter();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */

	@Override
	public ParentMatcherFilter createParentMatcherFilter() {
		ParentMatcherFilterImpl parentMatcherFilter = new ParentMatcherFilterImpl();
		return parentMatcherFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */

	@Override
	public ElementtypefilterPackage getElementtypefilterPackage() {
		return (ElementtypefilterPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ElementtypefilterPackage getPackage() {
		return ElementtypefilterPackage.eINSTANCE;
	}

} // ElementtypefilterFactoryImpl
