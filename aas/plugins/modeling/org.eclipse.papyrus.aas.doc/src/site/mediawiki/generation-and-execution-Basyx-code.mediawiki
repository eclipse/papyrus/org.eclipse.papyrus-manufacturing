===Basyx code generation===

For each AAS, a Basyx project could be generated.

In the model explorer, right click on an AAS -> AAS -> Genrate AAS Basyx code

[[File:images/AAS31.png|500px|frame]]

And the generated code is on the Project explore tree

[[File:images/AAS32.png|500px|frame]]

===Basyx AAS module execution===

For the execution of the Basyx code, browse the generated project -> select "Device AASServer.java" -> right click on it -> Run as Java application.

[[File:images/AAS33.png|500px|frame]]

The URL to launch the AAS is generated in the console view

[[File:images/AAS34.png|900px|frame]]

The result in the browser

[[File:images/AAS35.png|1200px|frame]]
