/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.api.reference;

import io.adminshell.aas.v3.model.KeyElements;
import io.adminshell.aas.v3.model.KeyType;
import io.adminshell.aas.v3.model.impl.DefaultKey;

public class Key extends DefaultKey implements IKey {
	
	private boolean isLocal;
	
	/**
	 * Constructor for the class {@code Key} with its {@code KeyElements}, 
	 * a boolean flag indicating if the {@code Key} is Global or Local, 
	 * a value of the {@code Key} and its {@code KeyType} as parameters.
	 * 
	 * @param type
	 * @param local
	 * @param value
	 * @param idType
	 */
	public Key(KeyElements type, boolean local, String value, KeyType idType) {
		super();
		super.setType(type);
		this.isLocal = local;
		super.setValue(value);
		super.setIdType(idType);
	}

	/**
	 * Returns a boolean value indicating if the Key is Local or Global.
	 * If true, Key is Local.
	 * If false, Key is Global.
	 * 
	 * @return the isLocal
	 */
	@Override
	public boolean isLocal() {
		return this.isLocal;
	}

	/**
	 * Sets the boolean value {@code isLocal} to indicate if the Key is Local
	 * or Global.
	 * 
	 * If set true, Key is Local.
	 * If set false, Key is Global.
	 *  
	 * @param isLocal the isLocal to set
	 */
	@Override
	public void setLocal(boolean isLocal) {
		this.isLocal = isLocal;
	}
	
	
}

