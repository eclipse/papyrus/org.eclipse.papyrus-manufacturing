/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.api.communications;

import org.apache.commons.validator.routines.UrlValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Endpoint {
	
	private static final Logger logger = LoggerFactory.getLogger(Endpoint.class);
	private String name;
	private ProtocolKind protocol;
	private String address;
	
	/**
	 * Constructor of {@code Endpoint} with the {@code name} of the endpoint, 
	 * {@code ProtocolKind} of the endpoint, {@code address} of the endpoint.
	 * 
	 * @param address
	 * @param protocol
	 * @param name
	 */
	public Endpoint(String name, ProtocolKind protocol, String address) {
		this.name = name;
		this.protocol = protocol;
		
		String schemes[] = {"http", "https", "opc.tcp"};
		UrlValidator urlValidator = new UrlValidator(schemes, UrlValidator.ALLOW_LOCAL_URLS);
		
		if (urlValidator.isValid(address)) {
			this.address = address;
			logger.info("Endpoint initialised.");
		}
		
		else {
			logger.error("Probable URL-Schema Error Detected. Please check!");
		}
	}

	/**
	 * Gets the Address of the Endpoint in consideration. 
	 * 
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the Address of the Endpoint in consideration. 
	 * 
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Gets the kind of Protocol the Endpoint is assigned to. 
	 * 
	 * @return the protocol
	 */
	public ProtocolKind getProtocol() {
		return protocol;
	}

	/**
	 * Sets the kind of Protocol the Endpoint is to be assigned with.
	 * 
	 * @param protocol the protocol to set
	 */
	public void setProtocol(ProtocolKind protocol) {
		this.protocol = protocol;
	}

	/**
	 * Gets the name of the Endpoint in consideration. 
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets a name to the Endpoint in consideration. 
	 * 
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

}

