/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.api.submodel.submodelelement.dataelement;

import org.eclipse.aas.api.submodel.submodelelement.dataelement.valuetypes.ValueType;

import io.adminshell.aas.v3.model.ModelingKind;
import io.adminshell.aas.v3.model.impl.DefaultOperationVariable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OperationVariable extends DefaultOperationVariable implements IOperationVariable {
	
	private static final Logger logger = LoggerFactory.getLogger(OperationVariable.class);
	private Property opVar;
	private ValueType type;
	
	
	/**
	 * Constructor for the class {@code OperationVariable} with the
	 * OperationVariable Name and its Value Type as 
	 * parameters.
	 * 
	 * @param opVarName
	 * @param type
	 */
	public OperationVariable(String opVarName, ValueType type) {
		super();
		this.type = type;
		this.opVar = new Property(opVarName);
		this.opVar.setValueType(type);
		this.opVar.setKind(ModelingKind.TEMPLATE);
		
		super.setValue(opVar);
		
		logger.info("OperationVariable: " + this.opVar.getIdShort() + " initialised.");
	}
	
	/**
	 * Sets the Value type of the {@code OperationVariable} in consideration. 
	 * 
	 * @param type
	 */
	@Override
	public void setValueType(ValueType type) {
		this.type = type;
		this.opVar.setValueType(type);
	}
	
	/**
	 * Gets the value of the {@code OperationVariable} in consideration.
	 */
	@Override
	public String getValueType() {
		return this.opVar.getValueType();
	}
	
	
	
}
