/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.api.asset;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.aas.api.communications.Endpoint;

import io.adminshell.aas.v3.model.AssetKind;
import io.adminshell.aas.v3.model.IdentifierType;
import io.adminshell.aas.v3.model.impl.DefaultAsset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Asset extends DefaultAsset {
	
	private static final Logger logger = LoggerFactory.getLogger(Asset.class);
	private AssetKind kind;
	private List<Endpoint> endpoints = new ArrayList<Endpoint>();
	
	/**
	 * Constructor of the {@code Asset} with just {@code idShort} of the same as
	 * parameter. 
	 * 
	 * @param idShort
	 */
	public Asset(String idShort) {
		super();
		super.setIdShort(idShort);
		logger.info("Asset initialised with just idShort.");
	}
	
	/**
	 * Constructor of the {@code Asset} with {@code idShort} and {@code AssetKind} 
	 * of the Asset in consideration as parameter. 
	 * 
	 * @param idShort
	 * @param kind
	 */
	public Asset(String idShort, AssetKind kind) {
		super();
		super.setIdShort(idShort);
		this.kind = kind;
		logger.info("Asset initialised with idShort and AssetKind.");
	}
	
	/**
	 * Constructor of the {@code Asset} with {@code idShort}, {@code AssetKind}
	 * and {@code Endpoint} of the Asset in consideration as parameter. 
	 * 
	 * @param idShort
	 * @param kind
	 * @param endpoint
	 */
	public Asset(String idShort, AssetKind kind, Endpoint endpoint) {
		super();
		super.setIdShort(idShort);
		this.kind = kind;
		this.endpoints.add(endpoint);
		logger.info("Asset Administration Shell Class initialised with "
				+ "idShort, AssetKind and Endpoint.");
	}

	/**
	 * Gets the AssetKind of the Asset in consideration. 
	 * 
	 * @return the kind
	 */
	public AssetKind getKind() {
		return kind;
	}

	/**
	 * Sets the AssetKind of the Asset in consideration. 
	 * 
	 * @param kind the kind to set
	 */
	public void setKind(AssetKind kind) {
		this.kind = kind;
	}

	/**
	 * Gets the List of Endpoints assigned to the Asset in consideration. 
	 * 
	 * @return the endpoint
	 */
	public List<Endpoint> getEndpoints() {
		return this.endpoints;
	}

	/**
	 * Sets the Asset in consideration with a single endpoint. 
	 * 
	 * @param endpoint the endpoint to set to the Asset.
	 */
	public void setEndpoint(Endpoint endpoint) {
		this.endpoints.add(endpoint);
	}
	
	/**
	 * Sets the Asset with multiple Endpoints. But with this method multiple 
	 * Endpoints can be passed as individual instances. 
	 * 
	 * @param endpoints
	 */
	public void setEndpoints(Endpoint... endpoints) {
		for (Endpoint endpoint : endpoints) {
			this.endpoints.add(endpoint);
		}
	}
	
	/**
	 * Sets the Asset with multiple Endpoints. But with this method all the 
	 * Endpoint instances should be contained in a List. This could be then used
	 * as parameters to the method.
	 * @param endpoints
	 */
	public void setEndpoints(List<Endpoint> endpoints) {
		this.endpoints = endpoints;
	}
	
	/**
	 * Gets the Identifier Type of the Asset in consideration.
	 * 
	 * @return the idType
	 */
	public IdentifierType getIdType() {
		return super.getIdentification().getIdType();
	}

	/**
	 * Sets the Identifier Type of the Asset in consideration.
	 * @param idType the idType to set
	 */
	public void setIdType(IdentifierType idType) {
		super.identification.setIdType(idType);
	}

	/**
	 * Gets the Identification (Identifier) for the Asset in consideration. 
	 * 
	 * @return the id
	 */
	public String getId() {
		return super.getIdentification().getIdentifier();
	}

	/**
	 * Sets the Identification (Identifier) for the Asset in consideration. 
	 * 
	 * @param id the id to set
	 */
	public void setId(String id) {
		super.identification.setIdentifier(id);
	}

}

