/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.api.submodel.submodelelement.dataelement;

import java.util.UUID;

import org.eclipse.aas.api.communications.Endpoint;
import org.eclipse.aas.api.reference.IReference;
import org.eclipse.aas.api.submodel.SubModel;
import org.eclipse.aas.api.submodel.parts.ConceptDescription;
import org.eclipse.aas.api.submodel.submodelelement.SubModelElementCollection;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.valuetypes.ValueType;

import io.adminshell.aas.v3.model.ModelingKind;
import io.adminshell.aas.v3.model.Reference;
import io.adminshell.aas.v3.model.SubmodelElement;
import io.adminshell.aas.v3.model.impl.DefaultProperty;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Property extends DefaultProperty implements SubmodelElement {
	
	private static final Logger logger = LoggerFactory.getLogger(Property.class);
	
	private Object value;
	
	private IReference semanticId;
	// Trial - ConceptDescription
	private ConceptDescription conceptDesc;
	
	private SubModelElementCollection parentSEC;	
	private SubModel parentSub;
	
	private boolean isDynamic = false;
	private Endpoint endpoint;
	private int nameSpaceIndex;
	private Object identifier;

	
	/**
	 * Constructor for the class {@code Property} with no 
	 * parameters.
	 */
	public Property() {
		super();
		super.kind = ModelingKind.INSTANCE;
		super.valueType = ValueType.None.toString();
		logger.info("Property Initialised with no IdShort");
	}
	
	/**
	 * Constructor for the class {@code Property} with idShort as 
	 * parameter.
	 * 
	 * @param idShort
	 */
	public Property(String idShort) {
		super();
		super.setIdShort(idShort);
		super.kind = ModelingKind.INSTANCE;
		super.valueType = ValueType.None.toString();
		logger.info("Property Initialised with idShort: " + idShort);
	}
	
	/**
	 * Constructor for the class {@code Property} with idShort and isDynamic as 
	 * parameters.
	 * 
	 * @param idShort
	 * @param isDynamic
	 */
	public Property(String idShort, boolean isDynamic) {
		super();
		super.setIdShort(idShort);
		super.kind = ModelingKind.INSTANCE;
		super.valueType = ValueType.None.toString();
		logger.info("Property Initialised with idShort: " + idShort);
	}
	
	/**
	 *
	 * @param ref
	 */
	@Override @Deprecated
	public void setSemanticId(Reference ref) {
		super.setSemanticId(ref);
	}
	
	/**
	 * Sets the semantic identifier of the {@code Property} 
	 * in consideration.
	 * 
	 * @param ref
	 */
	public void setSemanticIdentifier(IReference ref) {
		this.semanticId = ref;
	}
	
	/**
	 * Sets the semantic identifier of the {@code Property} in consideration. 
	 * The semantic identifier is in this case a {@code ConceptDescription}. 
	 * 
	 * @param concept
	 */
	public void setSemanticDescription(ConceptDescription concept) {
		this.conceptDesc = concept;
	}	
	
	/**
	 * 
	 */
	@Override @Deprecated
	public Reference getSemanticId() {
		return super.getSemanticId();
	}
	
	/**
	 * Gets the semantic Identifier of the {@code Property} 
	 * in consideration.
	 * 
	 * @return	Returns an instance of the IReference.
	 */
	public IReference getSemanticIdentifier() {
		return this.semanticId;
	}
	
	/**
	 * Gets the semantic Description of the {@code Property} in consideration.
	 * 
	 * @return	Returns an instance of the ConceptDescription.
	 */
	public ConceptDescription getSemanticDescription() {
		return this.conceptDesc;
	}
	
	/**
	 * Sets the Value type of the {@code Property} in consideration. 
	 * 
	 * @param type
	 */
	public void setValueType(ValueType type) {
		super.setValueType(type.toString());
	}
	
	/**
	 * Gets the Value type of the {@code Property} in consideration. 
	 * 
	 * @return Returns the ValueType of the Property in String format. 
	 * 
	 */
	public String getValueType() {
		return super.getValueType();
	}
	
	/**
	 * Sets the value of the {@code Property} in consideration.
	 * 
	 * @param value
	 */
	public void setValue(Object value) {
		this.value = value;
	}
	
	/**
	 * Sets the value of the {@code Property} in consideration.
	 * 
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Gets the value of the {@code Property} in consideration.
	 */
	@Override
	public String getValue() {
		
		if (this.value!=null) {
			return this.value.toString() ;			
		}
		
		else {
			return null;
		}

	}
	
	/**
	 * Sets the parent of the {@code Property} in consideration as a 
	 * {@code SubModelElementCollection} determined by the instance passed in as 
	 * parameter to this function.
	 * 
	 * [NOTE]: This function is not to be used by user. 
	 * 
	 * @param parent
	 */
	public void setParent (SubModelElementCollection parent) {
		this.parentSEC = parent;
	}
	
	/**
	 * Sets the parent of the {@code Property} in consideration as a 
	 * {@code SubModel} determined by the instance passed in as 
	 * parameter to this function.
	 * 
	 * [NOTE]: This function is not to be used by user.  
	 * 
	 * @param parent
	 */
	public void setParent (SubModel parent) {
		this.parentSub = parent;
	}
	
	/**
	 * Gets the parent of the {@code Property} in consideration if it is a 
	 * {@code SubModelElementCollection}.
	 * 
	 * [NOTE]: This function is not to be used by user. 
	 * @return
	 */
	public SubModelElementCollection getParentSEC() {
		return this.parentSEC;
	}
	
	/**
	 * Gets the parent of the {@code Property} in consideration if it is a 
	 * {@code SubModel}.
	 * 
	 * [NOTE]: This function is not to be used by user. 
	 * @return
	 */
	public SubModel getParentSub() {
		return this.parentSub;
	}
	
	/**
	 * 
	 */
	@Deprecated @Override
	public void setValueType(String valueType) {
	    super.setValueType(valueType);
	}
	
	/**
	 * 
	 */
//	@Deprecated @Override
//	public void setValue(String value) {
//		super.setValue(value);
//	}
	
	/**
	 * Returns true, if the {@code Property} in consideration is 
	 * Dynamic, else false. 
	 * @return the isDynamic
	 */
	public boolean isDynamic() {
		return isDynamic;
	}

	/**
	 * Sets the {@code Property} in consideration to either
	 * Dynamic or Static.
	 * 
	 * Pass in {@code true} - If Dynamic.
	 * Pass in {@code false} - If Static.
	 * @param isDynamic the isDynamic to set
	 */
	public void setDynamic(boolean isDynamic) {
		this.isDynamic = isDynamic;
	}

	/**
	 * Gets the Endpoint assigned to the {@code Property}, 
	 * if assigned. 
	 * 
	 * @return the endpoint
	 */
	public Endpoint getEndpoint() {
		return endpoint;
	}

	/**
	 * Sets an Endpoint to the {@code Property}.
	 * 
	 * @param endpoint the endpoint to set
	 */
	public void setEndpoint(Endpoint endpoint) {
		this.endpoint = endpoint;
	}
	
	
	/****
	 * Having a Namespace and Identifier assigned to the Property
	 * is weird. Because for the moment they will be only for OPCUA
	 * Server. 
	 * 
	 * But in the future if there is a MQTT or anyother server, 
	 * this functions will have to be rewritten for all the protocols 
	 * and that will be just too much duplicate code. 
	 * 
	 * 
	 */

	/**
	 * Gets the OPCUA NamespaceIndex of the OPCUA Node, this {@code Property}
	 * should map to.
	 *  
	 * @return the nameSpaceIndex
	 */
	public int getNameSpaceIndex() {
		return nameSpaceIndex;
	}

	/**
	 * Sets the OPCUA NamespaceIndex of the OPCUA Node, this {@code Property}
	 * should map to.
	 * @param nameSpaceIndex the nameSpaceIndex to set
	 */
	public void setNameSpaceIndex(int nameSpaceIndex) {
		this.nameSpaceIndex = nameSpaceIndex;
	}

	/**
	 * Gets the OPCUA Identifier of the OPCUA Node, this {@code Property}
	 * should map to.
	 * 
	 * @return the identifier
	 */
	public Object getIdentifier() {
		return identifier;
	}

	/**
	 * Sets the OPCUA Identifier of the OPCUA Node, this {@code Property}
	 * should map to.
	 * 
	 * @param identifier the identifier to set
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	
	/**
	 * Sets the OPCUA Identifier of the OPCUA Node, this {@code Property}
	 * should map to.
	 * 
	 * @param identifier
	 */
	public void setIdentifier(long identifier) {
		this.identifier = identifier;
	}
	
	/**
	 * Sets the OPCUA Identifier of the OPCUA Node, this {@code Property}
	 * should map to.
	 * 
	 * @param identifier
	 */
	public void setIdentifier(UUID identifier) {
		this.identifier = identifier;
	}
	
	/**
	 * Sets the OPCUA Identifier of the OPCUA Node, this {@code Property}
	 * should map to.
	 * 
	 * @param identifier
	 */
	public void setIdentifier(byte[] identifier) {
		this.identifier = identifier;
	}
	
}
