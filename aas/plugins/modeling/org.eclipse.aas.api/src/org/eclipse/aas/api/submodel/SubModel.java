/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.api.submodel;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.aas.api.reference.IReference;
import org.eclipse.aas.api.submodel.parts.ConceptDescription;
import org.eclipse.aas.api.submodel.submodelelement.Operation;
import org.eclipse.aas.api.submodel.submodelelement.SubModelElementCollection;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.File;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.MultiLanguageProperty;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.Property;

import io.adminshell.aas.v3.model.ModelingKind;
import io.adminshell.aas.v3.model.Reference;
import io.adminshell.aas.v3.model.SubmodelElement;
import io.adminshell.aas.v3.model.impl.DefaultSubmodel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SubModel extends DefaultSubmodel {
	
	private static final Logger logger = LoggerFactory.getLogger(SubModel.class);
	
	private IReference semanticId;
	// Trial - ConceptDescription
	private ConceptDescription conceptDesc;
	
	private List<Property> properties = new ArrayList<Property>();
	private List<File> files = new ArrayList<File>();
	private List<Operation> operations = new ArrayList<Operation>();
	private List<MultiLanguageProperty> multiLanguageProperties = new ArrayList<MultiLanguageProperty>();
	private List<SubModelElementCollection> subModelElementCollections = new ArrayList<SubModelElementCollection>();
	
	/**
	 * Constructor for the class {@code SubModel} with no parameters. 
	 */
	public SubModel() {
		super();
		super.kind = ModelingKind.INSTANCE;
		logger.info("SubModel Initialised with no IdShort");
	}
	
	/**
	 * Constructor for the class {@code SubModel} with just idShort as its 
	 * parameter.
	 * 
	 * @param idShort
	 */
	public SubModel(String idShort) {
		super();
		super.idShort = idShort;
		super.kind = ModelingKind.INSTANCE;
		logger.info("SubModel Initialised with IdShort: " + idShort);
	}
	
	/**
	 *
	 * @param ref
	 */
	@Override @Deprecated
	public void setSemanticId(Reference ref) {
		super.setSemanticId(ref);
	}
	
	/**
	 * Sets the semantic identifier of the {@code SubModel} under consideration.
	 * 
	 * @param ref
	 */
	public void setSemanticIdentifier(IReference ref) {
		this.semanticId = ref;
	}
	
	/**
	 * Sets the semantic identifier of the {@code SubModel} in consideration. 
	 * The semantic identifier is in this case a {@code ConceptDescription}. 
	 * 
	 * @param concept
	 */
	public void setSemanticDescription(ConceptDescription concept) {
		this.conceptDesc = concept;
	}	
	
	/**
	 * 
	 */
	@Override @Deprecated
	public Reference getSemanticId() {
		return super.getSemanticId();
	}
	
	/**
	 * Gets the semantic Identifier of the {@code SubModel} in consideration. 
	 * 
	 * @return Returns an instance of the IReference. 
	 */
	public IReference getSemanticIdentifier() {
		return this.semanticId;
	}
	
	/**
	 * Gets the semantic Description of the {@code SubModel} in consideration.
	 * 
	 * @return	Returns an instance of the ConceptDescription.
	 */
	public ConceptDescription getSemanticDescription() {
		return this.conceptDesc;
	}
	
	/**
	 * Sets a {@code File} to the {@code SubModel} 
	 * in consideration. 
	 * 
	 * @param fileInstance
	 */
	public void setFile(File fileInstance) {
		fileInstance.setParent(this);
		this.files.add(fileInstance);
		
		logger.info("File " + fileInstance.getIdShort() + " set to "
				+ "SubModel: " + super.getIdShort());
		
		setSuperSubmodelElements(fileInstance);
	}
	
	/**
	 * Sets multiple files to the {@code SubModel} in 
	 * consideration. The method accepts multiple individual instances of 
	 * {@code File}. 
	 * 
	 * @param fileInstances
	 */
	public void setFiles(File... fileInstances) {
		
		for (File fileInstance : fileInstances) {
			fileInstance.setParent(this);
			this.files.add(fileInstance);
			
			logger.info("Files " + fileInstance.getIdShort() + " set to "
					+ "SubModel: " + super.getIdShort());
			
			setSuperSubmodelElements(fileInstance);
		}
	}
	
	/**
	 * Sets a List containing multiple instances of {@code File} to the
	 * {@code SubModel} in consideration. 
	 * 
	 * @param files
	 */
	public void setFiles(List<File> files) {
		this.files = files;
		
		logger.info("A List of Files received and set to SubModel: "
				+ super.getIdShort());
		
		for (File file : files) {
			file.setParent(this);
			setSuperSubmodelElements(file);
		}
	}
	
	/**
	 * Gets a List containing multiple instances of {@code File} that are
	 * assigned to the {@code SubModel} in consideration. 
	 * 
	 * @return
	 */
	public List<File> getFiles() {
		return this.files;
	}
	
	/**
	 * Sets a {@code Property} to the {@code SubModel} in consideration. 
	 * 
	 * @param propInstance
	 */
	public void setProperty(Property propInstance) {
		this.properties.add(propInstance);
		propInstance.setParent(this);
		logger.info("Property " + propInstance.getIdShort() + " set to "
				+ "SubModel: " + super.getIdShort());
		
		// Internally adding to the Superclass Submodelelement list. So that a getSubmodelElements() call also works. 
		setSuperSubmodelElements(propInstance);
		
	}
	
	/**
	 * Sets multiple properties to the {@code SubModel} in consideration.
	 * The method accepts multiple individual instances of {@code Property}. 
	 * 
	 * @param propInstances
	 */
	public void setProperties(Property... propInstances) {
		
		for (Property propInstance : propInstances) {
			
			this.properties.add(propInstance);
			propInstance.setParent(this);
			logger.info("Property " + propInstance.getIdShort() + " set to "
					+ "SubModel: " + super.getIdShort());
			
			setSuperSubmodelElements(propInstance);
		}
	}
	
	/**
	 * Sets a List containing multiple instances of {@code Property} to the
	 * {@code SubModel} in consideration. 
	 * 
	 * @param properties
	 */
	public void setProperties (List<Property> properties) {
		this.properties = properties;
		
		logger.info("A List of Property received and set to SubModel: "
				+ super.getIdShort());
		
		for (Property property : properties) {
			property.setParent(this);
			setSuperSubmodelElements(property);
		}
		
	}
	
	/**
	 * Gets a List containing multiple instances of {@code Property} that are
	 * assigned to the {@code SubModel} in consideration. 
	 * 
	 * @return
	 */
	public List<Property> getProperties() {
		return this.properties;
	}
	
	/**
	 * Sets a {@code Operation} to the {@code SubModel} in consideration.
	 * 
	 * @param opInstance
	 */
	public void setOperation(Operation opInstance) {
		this.operations.add(opInstance);
		opInstance.setParent(this);
		logger.info("Operation " + opInstance.getIdShort() + " set to "
				+ "SubModel: " + super.getIdShort());
		
		setSuperSubmodelElements(opInstance);

	}
	
	/**
	 * Sets multiple operations to the {@code SubModel} in consideration.
	 * The method accepts multiple individual instances of {@code Operation}. 
	 * 
	 * @param opInstances
	 */
	public void setOperations(Operation... opInstances) {
		
		for (Operation opInstance : opInstances) {
			this.operations.add(opInstance);
			opInstance.setParent(this);
			logger.info("Operation " + opInstance.getIdShort() + " set to "
					+ "SubModel: " + super.getIdShort());
			
			setSuperSubmodelElements(opInstance);

		}
	}
	
	/**
	 * Sets a List containing multiple instances of {@code Operation} to the
	 * {@code SubModel} in consideration. 
	 * 
	 * @param operations
	 */
	public void setOperations(List<Operation> operations) {
		this.operations = operations;
		
		logger.info("A List of Operations received and set to SubModel: "
				+ super.getIdShort());
		
		for (Operation operation : operations) {
			operation.setParent(this);
			setSuperSubmodelElements(operation);
		}

	}
	
	/**
	 * Gets a List containing multiple instances of {@code Operation} that are
	 * assigned to the {@code SubModel} in consideration. 
	 * 
	 * @return
	 */
	public List<Operation> getOperations() {
		return this.operations;
	}
	
	/**
	 * Sets a {@code MultiLanguageProperty} to the {@code SubModel} in consideration.
	 * 
	 * @param multiLangProp
	 */
	public void setMultiLanguageProperty(MultiLanguageProperty multiLangProp) {
		
		this.multiLanguageProperties.add(multiLangProp);
		multiLangProp.setParent(this);
		logger.info("MultiLanguageProperty " + multiLangProp.getIdShort() + 
				" set to SubModel: " + super.getIdShort());
		
		setSuperSubmodelElements(multiLangProp);
		
	}
	
	/**
	 * Sets multiple multi-language properties to the {@code SubModel} in consideration.
	 * The method accepts multiple individual instances of {@code MultiLanguageProperty}. 
	 * 
	 * @param multiLangProps
	 */
	public void setMultiLanguageProperties(MultiLanguageProperty... multiLangProps) {
		
		for (MultiLanguageProperty multiLangProp : multiLangProps) {
			this.multiLanguageProperties.add(multiLangProp);
			multiLangProp.setParent(this);
			logger.info("MultiLanguageProperty " + multiLangProp.getIdShort() + 
					" set to SubModel: " + super.getIdShort());
			
			setSuperSubmodelElements(multiLangProp);
		}
		
	}
	
	/**
	 * Sets a List containing multiple instances of {@code MultiLanguageProperty} to the
	 * {@code SubModel} in consideration. 
	 * 
	 * @param multiLangProps
	 */
	public void setMultiLanguageProperties(List<MultiLanguageProperty> multiLangProps) {
		this.multiLanguageProperties = multiLangProps;
		
		logger.info("A List of MultiLanguageProperties received and set to SubModel: "
				+ super.getIdShort());
		
		for (MultiLanguageProperty multiLangProp : multiLangProps) {
			multiLangProp.setParent(this);
			setSuperSubmodelElements(multiLangProp);
		}
		
	}
	
	/**
	 * Gets a List containing multiple instances of {@code MultiLanguageProperty} 
	 * that are assigned to the {@code SubModel} in consideration. 
	 * 
	 * @return
	 */
	public List<MultiLanguageProperty> getMultiLanguageProperties() {
		return this.multiLanguageProperties;
	}
	
	/**
	 * Sets a {@code SubModelElementCollection} to the {@code SubModel} in 
	 * consideration.
	 * 
	 * @param sec
	 */
	public void setSubModelElementCollection(SubModelElementCollection sec) {
		this.subModelElementCollections.add(sec);
		sec.setParent(this);
		logger.info("SubmodelElementCollection " + sec.getIdShort() + 
				" set to SubModel: " + super.getIdShort());
		
		setSuperSubmodelElements(sec);

	}
	
	/**
	 * Sets multiple submodelelementcollections to the {@code SubModel} in consideration.
	 * The method accepts multiple individual instances of {@code SubModelElementCollection}. 
	 * 
	 * @param secs
	 */
	public void setSubModelElementCollections(SubModelElementCollection... secs) {
		
		for (SubModelElementCollection sec : secs) {
			this.subModelElementCollections.add(sec);
			sec.setParent(this);
			logger.info("SubmodelElementCollection " + sec.getIdShort() + 
					" set to SubModel: " + super.getIdShort());
			
			setSuperSubmodelElements(sec);
		}
	}
	
	/**
	 * Sets a List containing multiple instances of {@code SubModelElementCollection} to the
	 * {@code SubModel} in consideration. 
	 * 
	 * @param secs
	 */
	public void setSubModelElementCollections(List<SubModelElementCollection> secs) {
		this.subModelElementCollections = secs;
		
		logger.info("A List of SubmodelElementCollection received and set to SubModel: "
				+ super.getIdShort());
		
		for (SubModelElementCollection sec : secs) {
			sec.setParent(this);
			setSuperSubmodelElements(sec);
		}
		
		
	}
	
	/**
	 * Gets a List containing multiple instances of {@code SubModelElementCollection} 
	 * that are assigned to the {@code SubModel} in consideration. 
	 * 
	 * @return
	 */
	public List<SubModelElementCollection> getSubModelElementCollections() {
		return this.subModelElementCollections;
	}
	
	/**
	 * Sets the SubModelElements in the SubModel in consideration also to the parent 
	 * class. This is done so that the methods in the parent class also stay relevant 
	 * and at par for the Child class (SubModel). 
	 * 
	 * @param subElem
	 */
	private void setSuperSubmodelElements(SubmodelElement subElem) {
		super.getSubmodelElements().add(subElem);
		
		logger.info("Internal call to parent class method successful.");
	}

}

