/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.api.communications;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AASEndpoint {
	
	private static final Logger logger = LoggerFactory.getLogger(AASEndpoint.class);
	private String address;
	private long port;
	
	/**
	 * Constructor of {@code AASEndpoint} with the {@code address} of the aasEndpoint,
	 * {@code port} of the aasEndpoint.
	 * 
	 * @param address: String	Host address of the {@code AASEndpoint}. 
	 * @param port: Integer		Port number of the {@code AASEndpoint}.
	 */
	public AASEndpoint(String address, int port) {
		
		this.address = address;
		this.port = port;
		
		logger.info("AASEndpoint initialised.");
	}

	/**
	 * Gets the Address of the {@code AASEndpoint} in consideration. 
	 * 
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the Address of the {@code AASEndpoint} in consideration. 
	 * 
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Gets the Port of the {@code AASEndpoint} in consideration.
	 * 
	 * @return the port
	 */
	public long getPort() {
		return port;
	}

	/**
	 * Sets the Port of the {@code AASEndpoint} in consideration. 
	 * 
	 * @param port the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}
	
	

}
