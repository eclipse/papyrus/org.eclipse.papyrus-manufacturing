/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.api.submodel.submodelelement.dataelement;

import java.util.UUID;

import org.eclipse.aas.api.communications.Endpoint;
import org.eclipse.aas.api.reference.IReference;
import org.eclipse.aas.api.submodel.SubModel;
import org.eclipse.aas.api.submodel.parts.ConceptDescription;
import org.eclipse.aas.api.submodel.submodelelement.SubModelElementCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.adminshell.aas.v3.model.ModelingKind;
import io.adminshell.aas.v3.model.Reference;
import io.adminshell.aas.v3.model.impl.DefaultFile;

public class File extends DefaultFile {
	
	private static final Logger logger = LoggerFactory.getLogger(File.class);
	
	private IReference semanticId;
	// Trial - ConceptDescription
	private ConceptDescription conceptDesc;
	
	private SubModelElementCollection parentSEC;
	private SubModel parentSub;
	
	private boolean isDynamic = false;
	private Endpoint endpoint;
	private int nameSpaceIndex;
	private Object identifier;

	
	
	/**
	 * Constructor for the class {@code File}  with no parameters. 
	 */
	public File() {
		super();
		super.kind = ModelingKind.INSTANCE;
		logger.info("File Initialised with no IdShort.");
	}
	
	/**
	 * Constructor for the class {@code File} with idShort as parameter.
	 * 
	 * @param idShort
	 */
	public File(String idShort) {
		super();
		super.setIdShort(idShort);
		super.kind = ModelingKind.INSTANCE;
		logger.info("File Initialised with idShort: " + idShort);
	}
	
	/**
	 * Constructor for the class {@code File} with idShort and mimeType as
	 * parameters. 
	 * 
	 * @param idShort
	 * @param mimeType
	 */
	public File(String idShort, String mimeType) {
		super();
		super.setIdShort(idShort);
		super.setMimeType(mimeType);
		super.kind = ModelingKind.INSTANCE;
		logger.info("File Initialised with idShort: " + idShort + " and mimeType: " + mimeType);
	}
	
	/**
	 * 
	 */
	@Override @Deprecated
	public void setSemanticId(Reference ref) {
		super.setSemanticId(ref);
	}
	
	/**
	 * Sets the semantic identifier of the {@code File} 
	 * in consideration.
	 * 
	 * @param ref
	 */
	public void setSemanticIdentifier(IReference ref) {
		this.semanticId = ref;
	}
	
	/**
	 * Sets the semantic identifier of the {@code File} in consideration. 
	 * The semantic identifier is in this case a {@code ConceptDescription}. 
	 * 
	 * @param concept
	 */
	public void setSemanticDescription(ConceptDescription concept) {
		this.conceptDesc = concept;
	}	
	
	/**
	 * 
	 */
	@Override @Deprecated
	public Reference getSemanticId() {
		return super.getSemanticId();
	}
	
	/**
	 * Gets the semantic Identifier of the {@code File} 
	 * in consideration.
	 * 
	 * @return	Returns an instance of the IReference.
	 */
	public IReference getSemanticIdentifier() {
		return this.semanticId;
	}
	
	/**
	 * Gets the semantic Description of the {@code File} in consideration.
	 * 
	 * @return	Returns an instance of the ConceptDescription.
	 */
	public ConceptDescription getSemanticDescription() {
		return this.conceptDesc;
	}
	
	/**
	 * Sets the parent of the {@code File} in consideration as a 
	 * {@code SubModelElementCollection} determined by the instance passed in as 
	 * parameter to this function.
	 * 
	 * [NOTE]: This function is not to be used by user. 
	 * 
	 * @param parent
	 */
	public void setParent (SubModelElementCollection parent) {
		this.parentSEC = parent;
	}
	
	/**
	 * Sets the parent of the {@code File} in consideration as a 
	 * {@code SubModel} determined by the instance passed in as 
	 * parameter to this function.
	 * 
	 * [NOTE]: This function is not to be used by user.  
	 * 
	 * @param parent
	 */
	public void setParent (SubModel parent) {
		this.parentSub = parent;
	}
	
	/**
	 * Gets the parent of the {@code File} in consideration if it is a 
	 * {@code SubModelElementCollection}.
	 * 
	 * [NOTE]: This function is not to be used by user. 
	 * @return
	 */
	public SubModelElementCollection getParentSEC() {
		return this.parentSEC;
	}
	
	/**
	 * Gets the parent of the {@code File} in consideration if it is a 
	 * {@code SubModel}.
	 * 
	 * [NOTE]: This function is not to be used by user. 
	 * @return
	 */
	public SubModel getParentSub() {
		return this.parentSub;
	}
	
	/**
	 * Returns true, if the {@code File} in consideration is 
	 * Dynamic, else false. 
	 * @return the isDynamic
	 */
	public boolean isDynamic() {
		return isDynamic;
	}

	/**
	 * Sets the {@code File} in consideration to either
	 * Dynamic or Static.
	 * 
	 * Pass in {@code true} - If Dynamic.
	 * Pass in {@code false} - If Static.
	 * @param isDynamic the isDynamic to set
	 */
	public void setDynamic(boolean isDynamic) {
		this.isDynamic = isDynamic;
	}

	/**
	 * Gets the Endpoint assigned to the {@code File}, 
	 * if assigned. 
	 * 
	 * @return the endpoint
	 */
	public Endpoint getEndpoint() {
		return endpoint;
	}

	/**
	 * Sets an Endpoint to the {@code File}.
	 * 
	 * @param endpoint the endpoint to set
	 */
	public void setEndpoint(Endpoint endpoint) {
		this.endpoint = endpoint;
	}
	
	
	/****
	 * Having a Namespace and Identifier assigned to the File
	 * is weird. Because for the moment they will be only for OPCUA
	 * Server. 
	 * 
	 * But in the future if there is a MQTT or anyother server, 
	 * this functions will have to be rewritten for all the protocols 
	 * and that will be just too much duplicate code. 
	 * 
	 * 
	 */

	/**
	 * Gets the OPCUA NamespaceIndex of the OPCUA Node, this {@code File}
	 * should map to.
	 *  
	 * @return the nameSpaceIndex
	 */
	public int getNameSpaceIndex() {
		return nameSpaceIndex;
	}

	/**
	 * Sets the OPCUA NamespaceIndex of the OPCUA Node, this {@code File}
	 * should map to.
	 * @param nameSpaceIndex the nameSpaceIndex to set
	 */
	public void setNameSpaceIndex(int nameSpaceIndex) {
		this.nameSpaceIndex = nameSpaceIndex;
	}

	/**
	 * Gets the OPCUA Identifier of the OPCUA Node, this {@code File}
	 * should map to.
	 * 
	 * @return the identifier
	 */
	public Object getIdentifier() {
		return identifier;
	}

	/**
	 * Sets the OPCUA Identifier of the OPCUA Node, this {@code File}
	 * should map to.
	 * 
	 * @param identifier the identifier to set
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	
	/**
	 * Sets the OPCUA Identifier of the OPCUA Node, this {@code File}
	 * should map to.
	 * 
	 * @param identifier
	 */
	public void setIdentifier(long identifier) {
		this.identifier = identifier;
	}
	
	/**
	 * Sets the OPCUA Identifier of the OPCUA Node, this {@code File}
	 * should map to.
	 * 
	 * @param identifier
	 */
	public void setIdentifier(UUID identifier) {
		this.identifier = identifier;
	}
	
	/**
	 * Sets the OPCUA Identifier of the OPCUA Node, this {@code File}
	 * should map to.
	 * 
	 * @param identifier
	 */
	public void setIdentifier(byte[] identifier) {
		this.identifier = identifier;
	}


}

