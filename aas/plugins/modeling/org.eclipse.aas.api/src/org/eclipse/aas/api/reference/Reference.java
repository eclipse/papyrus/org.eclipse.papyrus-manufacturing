/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.api.reference;

import java.util.ArrayList;
import java.util.List;

import io.adminshell.aas.v3.model.impl.DefaultReference;

public class Reference implements IReference {
	
	private List<Key> keys = new ArrayList<>();
	
	/**
	 * Constructor for the class {@code Reference} with no parameters. 
	 */
	public Reference () {
		
	}
	
	/**
	 * Constructor for the class {@code Reference} with a single {@code Key}
	 * as parameter. 
	 * 
	 * @param key
	 */
	public Reference(Key key) {
		this.keys.add(key);
	}
	
	/**
	 * Constructor for the class {@code Reference} which accepts a List containing
	 * multiple keys. 
	 * 
	 * @param keys
	 */
	public Reference(List<Key> keys) {
		
		for (Key key : keys) {
			this.keys.add(key);
		}
	}
	
	/**
	 * Gets a List of Keys that are contained in the {@code Reference} under 
	 * consideration. 
	 * 
	 * @return  
	 */
	@Override
	public List<Key> getKeys() {
		return this.keys;
	}
	
	/**
	 * Sets the {@code Reference} in consideration with a List of many Keys.
	 * @param keys
	 */
	@Override
	public void setKeys(List<Key> keys) {
		for (Key key : keys) {
			this.keys.add(key);
		}
	}
	
	/**
	 * Sets the {@code Reference} in consideration with a single Key.
	 * 
	 * @param key
	 */
	public void setKey(Key key) {
		this.keys.add(key);
	}
}

