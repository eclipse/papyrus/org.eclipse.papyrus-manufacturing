/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *     CEA LIST - Saadia DHOUIB <saadia.dhouib@cea.fr>
 *******************************************************************************/
package org.eclipse.aas.api.communications;

public enum ProtocolKind {
	
	HTTP,
	MQTT,
	OPCUA,
	CoAP,
	ROS,
	WebSocket,
	OTHER

}
