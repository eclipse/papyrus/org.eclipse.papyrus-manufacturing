/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.api.submodel.submodelelement;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.aas.api.reference.IReference;
//import org.eclipse.papyrus.aas.Papyrus4Manufacturing.api.reference.Reference;
import org.eclipse.aas.api.submodel.SubModel;
import org.eclipse.aas.api.submodel.parts.ConceptDescription;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.IOperationVariable;

import io.adminshell.aas.v3.model.impl.DefaultOperation;
import io.adminshell.aas.v3.model.ModelingKind;
import io.adminshell.aas.v3.model.OperationVariable;
import io.adminshell.aas.v3.model.Reference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Operation extends DefaultOperation {
	
	private static final Logger logger = LoggerFactory.getLogger(Operation.class);
	
	private IReference semanticId;
	// Trial - ConceptDescription
	private ConceptDescription conceptDesc;
	
	private String opCode = "";
	private List<IOperationVariable> inputVariables = new ArrayList<IOperationVariable>();
	private List<IOperationVariable> inoutputVariables = new ArrayList<IOperationVariable>();
	private List<IOperationVariable> outputVariables = new ArrayList<IOperationVariable>();
	
	private SubModelElementCollection parentSEC;	
	private SubModel parentSub;
	
	/**
	 * Constructor for the class {@code Operation} with no parameters. 
	 */
	public Operation() {
		super();
		super.kind = ModelingKind.INSTANCE;
		logger.info("Operation Initialised with no IdShort");
	}
	
	/**
	 *
	 * @param ref
	 */
	@Override @Deprecated
	public void setSemanticId(Reference ref) {
		super.setSemanticId(ref);
	}
	
	/**
	 * Sets the semantic identifier of the {@code Operation} in consideration.
	 * @param ref
	 */
	public void setSemanticIdentifier(IReference ref) {
		this.semanticId = ref;
	}
	
	/**
	 * Sets the semantic identifier of the {@code Operation} in consideration. 
	 * The semantic identifier is in this case a {@code ConceptDescription}. 
	 * 
	 * @param concept
	 */
	public void setSemanticDescription(ConceptDescription concept) {
		this.conceptDesc = concept;
	}
	
	/**
	 * 
	 */
	@Override @Deprecated
	public Reference getSemanticId() {
		return super.getSemanticId();
	}
	
	/**
	 * Gets the semantic Identifier of the {@code Operation} in consideration.
	 * 
	 * @return	Returns an instance of the IReference.
	 */
	public IReference getSemanticIdentifier() {
		return this.semanticId;
	}
	
	/**
	 * Gets the semantic Description of the {@code Operation} in consideration.
	 * 
	 * @return	Returns an instance of the ConceptDescription.
	 */
	public ConceptDescription getSemanticDescription() {
		return this.conceptDesc;
	}
	
	/**
	 * Constructor for the class {@code Operation} with its idShort as parameter.
	 * 
	 * @param idShort
	 */
	public Operation(String idShort) {
		super();
		super.idShort = idShort;
		super.kind = ModelingKind.INSTANCE;
		logger.info("Operation Initialised with IdShort: " + idShort);
	}
	
	/**
	 * Sets a {@code OperationVariable} to the {@code Operation} in consideration.
	 * 
	 * @param inputVariable
	 */
	public void setInputVariable(IOperationVariable inputVariable) {
		this.inputVariables.add(inputVariable);
		
		logger.info("InputVariable " + inputVariable.getValue().getIdShort() + 
				"set to " + "Operation: " + super.getIdShort());

	}
	
	/**
	 * Sets multiple inputVariables to the {@code Operation} in consideration.
	 * The method accepts multiple individual instances of {@code OperationVariable}
	 * which it uses for the assignment.
	 * 
	 * @param inputVariables
	 */
	public void setInputVariables(IOperationVariable... inputVariables) {
		for (IOperationVariable inputVariable : inputVariables) {
			
			this.inputVariables.add(inputVariable);
			
			logger.info("InputVariable " + inputVariable.getValue().getIdShort() + 
					"set to " + "Operation: " + super.getIdShort());
		}
	}
	
	@Override @Deprecated
	public List<OperationVariable> getInputVariables() {
		return null;
	}
	
	/**
	 * Gets a List containing multiple instances of [InputVariables]
	 * {@code OperationVariable} that are assigned to the {@code Operation} 
	 * in consideration.
	 * 
	 * @return
	 */
	public List<IOperationVariable> getInputVars() {
		return this.inputVariables;
	}
	
	@Override @Deprecated
	public List<OperationVariable> getInoutputVariables() {
		return null;
	}
	
	/**
	 * Gets a List containing multiple instances of [InOutputVariables] 
	 * {@code OperationVariable} that are assigned to the {@code Operation} 
	 * in consideration.
	 * 
	 * @return
	 */
	public List<IOperationVariable> getInoutputVars() {
		return this.inoutputVariables;
	}
	
	@Override @Deprecated
	public List<OperationVariable> getOutputVariables() {
		return null;
	}
	
	/**
	 * Gets a List containing multiple instances of [OutputVariables] 
	 * {@code OperationVariable} that are assigned to the {@code Operation}.
	 * 
	 * @return
	 */
	public List<IOperationVariable> getOutputVars() {
		return this.outputVariables;
	}
	
	/**
	 * Sets a [InOutputVariable] {@code OperationVariable} to the {@code Operation} in consideration.
	 * 
	 * [NOTE:] For the moment (11.01.2022), the Papyrus4Manufacturing Project 
	 * handles the InOutput Variables of an Operation as Input Parameters of the same. 
	 * This effectively means, that if any InOutput Parameter is defined for an
	 * Operation, this parameter will be added to the operation as an Input Parameter. 
	 * 
	 * All InOutput Parameters --> Input Parameters. 
	 * 
	 * @param inoutputVariable
	 */
	public void setInOutputVariable(IOperationVariable inoutputVariable) {
		
//		this.inoutputVariables.add(inoutputVariable);	Commented out as part of the special handling of InOutput variables for Papyrus4Manufacturing Project.
		
		this.inputVariables.add(inoutputVariable);		// Written as part of the special handling of InOutput Variables for Papyrus4Manufacturing Project.
		
//		logger.info("InoutputVariable " + inoutputVariable.getValue().getIdShort() + 
//				"set to " + "Operation: " + super.getIdShort());
		
		logger.info("InoutputVariable " + inoutputVariable.getValue().getIdShort() + 
				"set to " + "Operation: " + super.getIdShort() + " as Input Variables");
	}
	
	/**
	 * Sets multiple inputVariables to the {@code Operation} in consideration.
	 * The method accepts multiple individual instances of {@code OperationVariable}
	 * which it uses for the assignment.
	 * 
	 * [NOTE:] For the moment (11.01.2022), the Papyrus4Manufacturing Project 
	 * handles the InOutput Variables of an Operation as Input Parameters of the same. 
	 * This effectively means, that if any InOutput Parameter is defined for an
	 * Operation, this parameter will be added to the operation as an Input Parameter. 
	 * 
	 * All InOutput Parameters --> Input Parameters. 
	 * 
	 * @param inoutputVariables
	 */
	public void setInOutputVariables(IOperationVariable... inoutputVariables) {
		for (IOperationVariable inoutputVariable : inoutputVariables) {
			
//			this.inoutputVariables.add(inoutputVariable);	Commented out as part of the special handling of InOutput variables for Papyrus4Manufacturing Project.
			
			this.inputVariables.add(inoutputVariable);		// Written as part of the special handling of InOutput Variables for Papyrus4Manufacturing Project.
			
//			logger.info("InoutputVariable " + inoutputVariable.getValue().getIdShort() + 
//					"set to " + "Operation: " + super.getIdShort());;
			
			logger.info("InoutputVariable " + inoutputVariable.getValue().getIdShort() + 
					"set to " + "Operation: " + super.getIdShort() + " as Input Variables");
		}
	}
	
	/**
	 * Sets a [OutputVariable] {@code OperationVariable} to the {@code Operation} in consideration.
	 * 
	 * @param outputVariable
	 */
	public void setOutputVariable(IOperationVariable outputVariable) {
		
		this.outputVariables.add(outputVariable);
		
		logger.info("OutputVariable " + outputVariable.getValue().getIdShort() + 
				"set to " + "Operation: " + super.getIdShort());
	}
	
	/**
	 * Sets the parent of the {@code Operation} in consideration as a 
	 * {@code SubModelElementCollection} determined by the instance passed in as 
	 * parameter to this function.
	 * 
	 * [NOTE]: This function is not to be used by user. 
	 * 
	 * @param parent
	 */
	public void setParent (SubModelElementCollection parent) {
		this.parentSEC = parent;
	}
	
	/**
	 * Sets the parent of the {@code Operation} in consideration as a 
	 * {@code SubModel} determined by the instance passed in as 
	 * parameter to this function.
	 * 
	 * [NOTE]: This function is not to be used by user.  
	 * 
	 * @param parent
	 */
	public void setParent (SubModel parent) {
		this.parentSub = parent;
	}
	
	/**
	 * Gets the parent of the {@code Operation} in consideration if it is a 
	 * {@code SubModelElementCollection}.
	 * 
	 * [NOTE]: This function is not to be used by user. 
	 * @return
	 */
	public SubModelElementCollection getParentSEC() {
		return this.parentSEC;
	}
	
	/**
	 * Gets the parent of the {@code Operation} in consideration if it is a 
	 * {@code SubModel}.
	 * 
	 * [NOTE]: This function is not to be used by user. 
	 * @return
	 */
	public SubModel getParentSub() {
		return this.parentSub;
	}
	
	/**
	 * Getting any custom code passed on by user.
	 * 
	 * @return the opCode
	 */
	public String getOpCode() {
		return opCode;
	}

	/**
	 * Setting custom code passed on by user to the {@code Operation} in
	 * consideration.
	 * 
	 * [Note]: In general avoid using this. Rather write your code directly in 
	 * the {@code DEWorkspace.java} class in the generated code for the 
	 * respective {@code SubModel}. While using JDT Sync with generated code
	 * please refrain from using this method. 
	 * 
	 * @param opCode the opCode to set
	 */
	public void setOpCode(String opCode) {
		this.opCode = opCode;
		
		logger.info("Operation Code set for Operation: " + super.getIdShort());
	}

}
