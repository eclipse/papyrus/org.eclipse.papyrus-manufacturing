/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.api.submodel.submodelelement.dataelement;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.aas.api.reference.IReference;
import org.eclipse.aas.api.submodel.SubModel;
import org.eclipse.aas.api.submodel.parts.ConceptDescription;
import org.eclipse.aas.api.submodel.submodelelement.SubModelElementCollection;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.valuetypes.ValueType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.adminshell.aas.v3.model.impl.DefaultMultiLanguageProperty;
import io.adminshell.aas.v3.model.LangString;
import io.adminshell.aas.v3.model.ModelingKind;
import io.adminshell.aas.v3.model.Reference;

public class MultiLanguageProperty extends DefaultMultiLanguageProperty {
	
	private static final Logger logger = LoggerFactory.getLogger(MultiLanguageProperty.class);
			
	private List<LangString> multipleLangs = new ArrayList<LangString>();
	private IReference semanticId;
	// Trial - ConceptDescription
	private ConceptDescription conceptDesc;
	
	private SubModelElementCollection parentSEC;	
	private SubModel parentSub;

	/**
	 * Constructor for the class {@code MultiLanguageProperty} with no 
	 * parameters.
	 */
	public MultiLanguageProperty() {
		super();
		super.kind = ModelingKind.INSTANCE;
		logger.info("MultiLanguageProperty Initialised with no IdShort");
	}
	
	/**
	 * Constructor for the class {@code MultiLanguageProperty} with its 
	 * idShort as the only parameter. 
	 * 
	 * @param idShort
	 */
	public MultiLanguageProperty(String idShort) {
		super();
		super.setIdShort(idShort);
		super.kind = ModelingKind.INSTANCE;
		logger.info("MultiLanguageProperty Initialised with IdShort: " + idShort);
	}
	
	/**
	 *
	 * @param ref
	 */
	@Override @Deprecated
	public void setSemanticId(Reference ref) {
		super.setSemanticId(ref);
	}
	
	/**
	 * Sets the semantic identifier of the {@code MultiLanguageProperty} 
	 * in consideration.
	 * 
	 * @param ref
	 */
	public void setSemanticIdentifier(IReference ref) {
		this.semanticId = ref;
	}
	
	/**
	 * Sets the semantic identifier of the {@code MultiLanguageProperty} in consideration. 
	 * The semantic identifier is in this case a {@code ConceptDescription}. 
	 * 
	 * @param concept
	 */
	public void setSemanticDescription(ConceptDescription concept) {
		this.conceptDesc = concept;
	}	
	
	/**
	 * 
	 */
	@Override @Deprecated
	public Reference getSemanticId() {
		return super.getSemanticId();
	}
	
	/**
	 * Gets the semantic Identifier of the {@code MultiLanguageProperty} 
	 * in consideration.
	 * 
	 * @return	Returns an instance of the IReference.
	 */
	public IReference getSemanticIdentifier() {
		return this.semanticId;
	}
	
	/**
	 * Gets the semantic Description of the {@code MultiLanguageProperty} in consideration.
	 * 
	 * @return	Returns an instance of the ConceptDescription.
	 */
	public ConceptDescription getSemanticDescription() {
		return this.conceptDesc;
	}
	
	/**
	 * Sets a Value to the MultiLanguageProperty using a LangString.
	 * Since creation of a LangString needs a "LangCode" and a "Value", therefore,
	 * the parameters of this function are formulated as seen. 
	 * 
	 * @param langCode Language Codes of the Supported Languages.
	 * @param value The String in the specified Languages. 
	 */
	public void setValue(String langCode, String value) {
		LangString val = new LangString(value, langCode);
		this.multipleLangs.add(val);
		super.setValues(this.multipleLangs);
	}
	
	/**
	 * Sets the parent of the {@code MultiLanguageProperty} in consideration as a 
	 * {@code SubModelElementCollection} determined by the instance passed in as 
	 * parameter to this function.
	 * 
	 * [NOTE]: This function is not to be used by user. 
	 * 
	 * @param parent
	 */
	public void setParent (SubModelElementCollection parent) {
		this.parentSEC = parent;
	}
	
	/**
	 * Sets the parent of the {@code MultiLanguageProperty} in consideration as a 
	 * {@code SubModel} determined by the instance passed in as 
	 * parameter to this function.
	 * 
	 * [NOTE]: This function is not to be used by user.  
	 * 
	 * @param parent
	 */
	public void setParent (SubModel parent) {
		this.parentSub = parent;
	}
	
	/**
	 * Gets the parent of the {@code MultiLanguageProperty} in consideration if 
	 * it is a {@code SubModelElementCollection}.
	 * 
	 * [NOTE]: This function is not to be used by user. 
	 * @return
	 */
	public SubModelElementCollection getParentSEC() {
		return this.parentSEC;
	}
	
	/**
	 * Gets the parent of the {@code MultiLanguageProperty} in consideration if it is a 
	 * {@code SubModel}.
	 * 
	 * [NOTE]: This function is not to be used by user. 
	 * @return
	 */
	public SubModel getParentSub() {
		return this.parentSub;
	}
	

}
