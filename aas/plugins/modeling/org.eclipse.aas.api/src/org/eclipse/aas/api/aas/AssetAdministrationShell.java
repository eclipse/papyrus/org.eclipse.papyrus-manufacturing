/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.api.aas;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.aas.api.aas.parts.ConceptDictionary;
import org.eclipse.aas.api.asset.Asset;
import org.eclipse.aas.api.communications.AASEndpoint;
import org.eclipse.aas.api.communications.Endpoint;
import org.eclipse.aas.api.submodel.SubModel;

import io.adminshell.aas.v3.model.IdentifierType;
import io.adminshell.aas.v3.model.Reference;
import io.adminshell.aas.v3.model.impl.DefaultAssetAdministrationShell;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AssetAdministrationShell extends DefaultAssetAdministrationShell {
	
	private static final Logger logger = LoggerFactory.getLogger(AssetAdministrationShell.class);
	private Asset asset;
	private AASEndpoint aasEndpoint;
	private List<SubModel> subModels = new ArrayList<SubModel>();
	private ConceptDictionary conceptDictionary;
	
	/**
	 * Constructor of the {@code AssetAdministrationShell} with 
	 * just {@code idShort}  of the same as parameter. 
	 * 
	 * @param idShort
	 */
	public AssetAdministrationShell(String idShort) {
		super();
		super.setIdShort(idShort);
		logger.info("Asset Administration Shell Class initialised with just "
				+ "idShort.");

	}

	
	/**
	 * Constructor of the {@code AssetAdministrationShell} with
	 * {@code idShort} and {@code endpoint} as parameters. 
	 * 
	 * @param idShort
	 * @param endpoint
	 */
	public AssetAdministrationShell(String idShort, AASEndpoint aasEndpoint) {
		super();
		super.setIdShort(idShort);
		this.aasEndpoint = aasEndpoint;
		logger.info("Asset Administration Shell Class initialised with just "
				+ "idShort and an Endpoint.");
	}

	/**
	 * Returns the Asset Instance assigned to the Asset Administration Shell. 
	 * 
	 * @return The Asset
	 */
	public Asset getAsset() {
		return asset;
	}

	/**
	 * Sets an Asset instance to the Asset Administration Shell.
	 * 
	 * @param asset 		The Asset Instance to set. 
	 */
	public void setAsset(Asset asset) {
		this.asset = asset;
	}
	
	/**
	 * Returns the AASEndpoint the Asset Administration Shell is hosted on. 
	 * 
	 * @return The AASEndpoint instance. 
	 */
	public AASEndpoint getAASEndpoint() {
		return this.aasEndpoint;
	}
	
	/**
	 * Sets an AASEndpoint to the Asset Administration Shell, on which
	 * it is hosted. 
	 * 
	 * @param endpoint The AASEndpoint instance to be assigned
	 */
	public void setEndpoint(AASEndpoint aasEndpoint) {
		this.aasEndpoint = aasEndpoint;
	}

	/**
	 * Returns the IdentifierType of the Asset Administration Shell. 
	 * 
	 * @return The IdentifierType
	 */
	public IdentifierType getIdType() {
		return super.getIdentification().getIdType();
	}

	/**
	 * Sets the IdentifierType of the Asset Administration Shell. 
	 * 
	 * @param idType 		The IdentifierType to set
	 */
	public void setIdType(IdentifierType idType) {
		super.identification.setIdType(idType);
	}

	/**
	 * Gets the Identifier of the Asset Administration Shell. 
	 * 
	 * @return The Identifier
	 */
	public String getId() {
		return super.getIdentification().getIdentifier();
	}

	/**
	 * Sets the Identification of the Asset Administration Shell. 
	 * 
	 * @param id the id to set
	 */
	public void setId(String id) {
		super.identification.setIdentifier(id);
	}

	/**
	 * Returns a List of SubModels the Asset Administration Shell under consideration 
	 * contains. 
	 * 
	 * @return the subModels
	 */
	public List<SubModel> getSubModels() {
		return subModels;
	}
	
	@Override @Deprecated
	public List<Reference> getSubmodels() {
		return super.getSubmodels();
	}

	/**
	 * Assigns the Asset Administration Shell with a List of SubModel, those are
	 * supposed to belong to it. 
	 * 
	 * @param subModels the subModels to set
	 */
	public void setSubModels(List<SubModel> subModels) {
		this.subModels = subModels;
	}
	
	/**
	 * Assigns the Asset Administration Shell with multiple individual instances 
	 * of SubModels, those are supposed to belong to it. 
	 * 
	 * @param subModels 
	 */
	public void setSubModels(SubModel... subModels) {
		for (SubModel subModel : subModels) {
			this.subModels.add(subModel);
		}
	}
	
	@Override @Deprecated
	public void setSubmodels(List<Reference> submodels) {
		super.setSubmodels(submodels);
	}
	
	/**
	 * Gets the {@code ConceptDictionary} instance which contains 
	 * {@code ConceptDescription}s known to the Asset Administration Shell 
	 * under consideration.
	 * 
	 * @return the conceptDictionary
	 */
	public ConceptDictionary getConceptDictionary() {
		return conceptDictionary;
	}

	/**
	 * Sets the {@code ConceptDictionary} instance which contains 
	 * {@code ConceptDescription}s known to the Asset Administration Shell 
	 * under consideration.
	 * 
	 * @param conceptDictionary the conceptDictionary to set
	 */
	public void setConceptDictionary(ConceptDictionary conceptDictionary) {
		this.conceptDictionary = conceptDictionary;
	}

}
