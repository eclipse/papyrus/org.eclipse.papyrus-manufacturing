/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.api.submodel.submodelelement;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.aas.api.communications.Endpoint;
import org.eclipse.aas.api.reference.IReference;
//import org.eclipse.aas.api.reference.Reference;
import org.eclipse.aas.api.submodel.SubModel;
import org.eclipse.aas.api.submodel.parts.ConceptDescription;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.File;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.MultiLanguageProperty;
import org.eclipse.aas.api.submodel.submodelelement.dataelement.Property;

import io.adminshell.aas.v3.model.ModelingKind;
import io.adminshell.aas.v3.model.Reference;
import io.adminshell.aas.v3.model.Submodel;
import io.adminshell.aas.v3.model.SubmodelElement;
import io.adminshell.aas.v3.model.SubmodelElementCollection;
import io.adminshell.aas.v3.model.impl.DefaultSubmodelElementCollection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SubModelElementCollection extends DefaultSubmodelElementCollection {
	
	private static final Logger logger = LoggerFactory.getLogger(SubModelElementCollection.class);
	
	private IReference semanticId;
	// Trial - ConceptDescription
	private ConceptDescription conceptDesc;
	
	private List<Property> properties = new ArrayList<Property>();
	private List<File> files = new ArrayList<File>();
	private List<Operation> operations = new ArrayList<Operation>();
	private List<MultiLanguageProperty> multiLanguageProperties = new ArrayList<MultiLanguageProperty>();
	private List<SubModelElementCollection> subModelElementCollections = new ArrayList<SubModelElementCollection>();
	
	private SubModelElementCollection parentSEC;	
	private SubModel parentSub;
	
	private boolean isDynamic = false;
	private Endpoint endpoint;
	
	/**
	 * Constructor for the class {@code SubModelElementCollection} with no 
	 * parameters.
	 */
	public SubModelElementCollection() {
		super();
		super.kind = ModelingKind.INSTANCE;
		logger.info("SubmodelElementCollection Initialised with no IdShort");
	}
	
	/**
	 * Constructor for the class {@code SubModelElementCollection} with its 
	 * idShort as the only parameter. 
	 * 
	 * @param idShort
	 */
	public SubModelElementCollection(String idShort) {
		super();
		super.idShort = idShort;
		super.kind = ModelingKind.INSTANCE;
		logger.info("SubmodelElementCollection Initialised with IdShort: " + idShort);
	}
	
	/**
	 *
	 * @param ref
	 */
	@Override @Deprecated
	public void setSemanticId(Reference ref) {
		super.setSemanticId(ref);
	}
	
	/**
	 * Sets the semantic identifier of the {@code SubModelElementCollection} 
	 * in consideration.
	 * 
	 * @param ref
	 */
	public void setSemanticIdentifier(IReference ref) {
		this.semanticId = ref;
	}
	
	/**
	 * Sets the semantic identifier of the {@code SubModelElementCollection} in consideration. 
	 * The semantic identifier is in this case a {@code ConceptDescription}. 
	 * 
	 * @param concept
	 */
	public void setSemanticDescription(ConceptDescription concept) {
		this.conceptDesc = concept;
	}	
	
	/**
	 * 
	 */
	@Override @Deprecated
	public Reference getSemanticId() {
		return super.getSemanticId();
	}
	
	/**
	 * Gets the semantic Identifier of the {@code SubModelElementCollection} 
	 * in consideration.
	 * 
	 * @return	Returns an instance of the IReference.
	 */
	public IReference getSemanticIdentifier() {
		return this.semanticId;
	}
	
	/**
	 * Gets the semantic Description of the {@code SubModelElementCollection} in consideration.
	 * 
	 * @return	Returns an instance of the ConceptDescription.
	 */
	public ConceptDescription getSemanticDescription() {
		return this.conceptDesc;
	}
	
	/**
	 * Sets a {@code File} to the {@code SubModelElementCollection} 
	 * in consideration. 
	 * 
	 * @param fileInstance
	 */
	public void setFile(File fileInstance) {
		fileInstance.setParent(this);
		this.files.add(fileInstance);
		
		logger.info("File " + fileInstance.getIdShort() + " set to "
				+ "SubModelElementCollection: " + super.getIdShort());
		
		setSuperSubmodelElements(fileInstance);
	}
	
	/**
	 * Sets multiple files to the {@code SubModelElementCollection} in 
	 * consideration. The method accepts multiple individual instances of 
	 * {@code File}. 
	 * 
	 * @param fileInstances
	 */
	public void setFiles(File... fileInstances) {
		
		for (File fileInstance : fileInstances) {
			fileInstance.setParent(this);
			this.files.add(fileInstance);
			
			logger.info("Files " + fileInstance.getIdShort() + " set to "
					+ "SubModelElementCollection: " + super.getIdShort());
			
			setSuperSubmodelElements(fileInstance);
		}
	}
	
	/**
	 * Sets a List containing multiple instances of {@code File} to the
	 * {@code SubModelElementCollection} in consideration. 
	 * 
	 * @param files
	 */
	public void setFiles(List<File> files) {
		this.files = files;
		
		logger.info("A List of Files received and set to SubModelElementCollection: "
				+ super.getIdShort());
		
		for (File file : files) {
			file.setParent(this);
			setSuperSubmodelElements(file);
		}
	}
	
	/**
	 * Gets a List containing multiple instances of {@code File} that are
	 * assigned to the {@code SubModelElementCollection} in consideration. 
	 * 
	 * @return
	 */
	public List<File> getFiles() {
		return this.files;
	}
	
	/**
	 * Sets a {@code Property} to the {@code SubModelElementCollection} 
	 * in consideration. 
	 * 
	 * @param propInstance
	 */
	public void setProperty(Property propInstance) {
		propInstance.setParent(this);
		this.properties.add(propInstance);
		
		logger.info("Property " + propInstance.getIdShort() + " set to "
				+ "SubModelElementCollection: " + super.getIdShort());
		
		setSuperSubmodelElements(propInstance);
	}
	
	/**
	 * Sets multiple properties to the {@code SubModelElementCollection} in 
	 * consideration. The method accepts multiple individual instances of 
	 * {@code Property}. 
	 * 
	 * @param propInstances
	 */
	public void setProperties(Property... propInstances) {
		
		for (Property propInstance : propInstances) {
			propInstance.setParent(this);
			this.properties.add(propInstance);
			
			logger.info("Property " + propInstance.getIdShort() + " set to "
					+ "SubModelElementCollection: " + super.getIdShort());
			
			setSuperSubmodelElements(propInstance);
		}
	}
	
	/**
	 * Sets a List containing multiple instances of {@code Property} to the
	 * {@code SubModelElementCollection} in consideration. 
	 * 
	 * @param properties
	 */
	public void setProperties (List<Property> properties) {
		this.properties = properties;
		
		logger.info("A List of Property received and set to SubModelElementCollection: "
				+ super.getIdShort());
		
		for (Property property : properties) {
			property.setParent(this);
			setSuperSubmodelElements(property);
		}
	}
	
	/**
	 * Gets a List containing multiple instances of {@code Property} that are
	 * assigned to the {@code SubModelElementCollection} in consideration. 
	 * 
	 * @return
	 */
	public List<Property> getProperties() {
		return this.properties;
	}
	
	/**
	 * Sets a {@code Operation} to the {@code SubModelElementCollection} 
	 * in consideration.
	 * 
	 * @param opInstance
	 */
	public void setOperation(Operation opInstance) {
		opInstance.setParent(this);
		this.operations.add(opInstance);
		
		logger.info("Operation " + opInstance.getIdShort() + " set to "
				+ "SubModelElementCollection: " + super.getIdShort());
		
		setSuperSubmodelElements(opInstance);
	}
	
	/**
	 * Sets multiple operations to the {@code SubModelElementCollection} in 
	 * consideration. The method accepts multiple individual instances 
	 * of {@code Operation}. 
	 * 
	 * @param opInstances
	 */
	public void setOperations(Operation... opInstances) {
		
		for (Operation opInstance : opInstances) {
			opInstance.setParent(this);
			this.operations.add(opInstance);
			
			logger.info("Operation " + opInstance.getIdShort() + " set to "
					+ "SubModelElementCollection: " + super.getIdShort());
			
			setSuperSubmodelElements(opInstance);
		}
	}
	
	/**
	 * Sets a List containing multiple instances of {@code Operation} to the
	 * {@code SubModelElementCollection} in consideration. 
	 * 
	 * @param operations
	 */
	public void setOperations(List<Operation> operations) {
		this.operations = operations;
		
		logger.info("A List of Operations received and set to "
				+ "SubModelElementCollection: " + super.getIdShort());
		
		for (Operation operation : operations) {
			operation.setParent(this);
			setSuperSubmodelElements(operation);
		}
	}
	
	/**
	 * Gets a List containing multiple instances of {@code Operation} that are
	 * assigned to the {@code SubModelElementCollection} in consideration. 
	 * 
	 * @return
	 */
	public List<Operation> getOperations() {
		return this.operations;
	}
	
	/**
	 * Sets a {@code MultiLanguageProperty} to the 
	 * {@code SubModelElementCollection} in consideration.
	 * 
	 * @param multiLangProp
	 */
	public void setMultiLanguageProperty(MultiLanguageProperty multiLangProp) {
		multiLangProp.setParent(this);
		this.multiLanguageProperties.add(multiLangProp);
		
		logger.info("MultiLanguageProperty " + multiLangProp.getIdShort() + 
				" set to SubModelElementCollection: " + super.getIdShort());
		
		setSuperSubmodelElements(multiLangProp);
	}
	
	/**
	 * Sets multiple multi-language properties to the {@code SubModelElementCollection} 
	 * in consideration. The method accepts multiple individual instances of 
	 * {@code MultiLanguageProperty}. 
	 * 
	 * @param multiLangProps
	 */
	public void setMultiLanguageProperties(MultiLanguageProperty... multiLangProps) {
		
		for (MultiLanguageProperty multiLangProp : multiLangProps) {
			multiLangProp.setParent(this);
			this.multiLanguageProperties.add(multiLangProp);
			
			logger.info("MultiLanguageProperty " + multiLangProp.getIdShort() + 
					" set to SubModelElementCollection: " + super.getIdShort());
			
			setSuperSubmodelElements(multiLangProp);
		}
	}
	
	/**
	 * Sets a List containing multiple instances of {@code MultiLanguageProperty} to the
	 * {@code SubModelElementCollection} in consideration. 
	 * 
	 * @param multiLangProps
	 */
	public void setMultiLanguageProperties(List<MultiLanguageProperty> multiLangProps) {
		this.multiLanguageProperties = multiLangProps;
		
		logger.info("A List of MultiLanguageProperties received and set "
				+ "to SubModelElementCollection: " + super.getIdShort());
		
		for (MultiLanguageProperty multiLangProp : multiLangProps) {
			multiLangProp.setParent(this);
			setSuperSubmodelElements(multiLangProp);
		}
		
	}
	
	/**
	 * Gets a List containing multiple instances of {@code MultiLanguageProperty} 
	 * that are assigned to the {@code SubModelElementCollection} in consideration. 
	 * 
	 * @return
	 */
	public List<MultiLanguageProperty> getMultiLanguageProperties() {
		return this.multiLanguageProperties;
	}
	
	/**
	 * Sets a {@code SubModelElementCollection} to the {@code SubModelElementCollection} 
	 * in consideration.
	 * 
	 * @param sec
	 */
	public void setSubModelElementCollection(SubModelElementCollection sec) {
		sec.setParent(this);
		this.subModelElementCollections.add(sec);
		
		logger.info("SubmodelElementCollection " + sec.getIdShort() + 
				" set to SubModelElementCollection: " + super.getIdShort());
		
		setSuperSubmodelElements(sec);
	}
	
	/**
	 * Sets multiple submodelelementcollections to the {@code SubModelElementCollection} 
	 * in consideration. The method accepts multiple individual instances of 
	 * {@code SubModelElementCollection}. 
	 * 
	 * @param secs
	 */
	public void setSubModelElementCollections(SubModelElementCollection... secs) {
		
		for (SubModelElementCollection sec : secs) {
			sec.setParent(this);
			this.subModelElementCollections.add(sec);
			
			logger.info("SubmodelElementCollection " + sec.getIdShort() + 
					" set to SubModelElementCollection: " + super.getIdShort());
			
			setSuperSubmodelElements(sec);
		}
	}
	
	/**
	 * Sets a List containing multiple instances of {@code SubModelElementCollection} to the
	 * {@code SubModelElementCollection} in consideration. 
	 * 
	 * @param secs
	 */
	public void setSubModelElementCollections(List<SubModelElementCollection> secs) {
		this.subModelElementCollections = secs;
		
		logger.info("A List of SubmodelElementCollection received and "
				+ "set to SubModelElementCollection: " + super.getIdShort());
		
		for (SubModelElementCollection sec : secs) {
			sec.setParent(this);
			setSuperSubmodelElements(sec);
		}
	}
	
	/**
	 * Gets a List containing multiple instances of {@code SubModelElementCollection} 
	 * that are assigned to the {@code SubModelElementCollection} in consideration. 
	 * 
	 * @return
	 */
	public List<SubModelElementCollection> getSubModelElementCollections() {
		return this.subModelElementCollections;
	}
	
	/**
	 * Sets the parent of the {@code SubModelElementCollection} in consideration as a 
	 * {@code SubModelElementCollection} determined by the instance passed in as 
	 * parameter to this function.
	 * 
	 * [NOTE]: This function is not to be used by user. 
	 * 
	 * @param parent
	 */
	public void setParent (SubModelElementCollection parent) {
		this.parentSEC = parent;
	}
	
	/**
	 * Sets the parent of the {@code SubModelElementCollection} in consideration as a 
	 * {@code SubModel} determined by the instance passed in as 
	 * parameter to this function.
	 * 
	 * [NOTE]: This function is not to be used by user.  
	 * 
	 * @param parent
	 */
	public void setParent (SubModel parent) {
		this.parentSub = parent;
	}
	
	/**
	 * Gets the parent of the {@code SubModelElementCollection} in consideration if it is a 
	 * {@code SubModelElementCollection}.
	 * 
	 * [NOTE]: This function is not to be used by user. 
	 * @return
	 */
	public SubModelElementCollection getParentSEC() {
		return this.parentSEC;
	}
	
	/**
	 * Gets the parent of the {@code SubModelElementCollection} in consideration if it is a 
	 * {@code SubModel}.
	 * 
	 * [NOTE]: This function is not to be used by user. 
	 * @return
	 */
	public SubModel getParentSub() {
		return this.parentSub;
	}
	
	/**
	 * Returns true, if the {@code SubModelElementCollection} in consideration is 
	 * Dynamic, else false. 
	 * @return the isDynamic
	 */
	public boolean isDynamic() {
		return isDynamic;
	}

	/**
	 * Sets the {@code SubModelElementCollection} in consideration to either
	 * Dynamic or Static.
	 * 
	 * Pass in {@code true} - If Dynamic.
	 * Pass in {@code false} - If Static.
	 * @param isDynamic the isDynamic to set
	 */
	public void setDynamic(boolean isDynamic) {
		this.isDynamic = isDynamic;
	}

	/**
	 * Gets the Endpoint assigned to the {@code SubModelElementCollection}, 
	 * if assigned. 
	 * 
	 * @return the endpoint
	 */
	public Endpoint getEndpoint() {
		return endpoint;
	}

	/**
	 * Sets an Endpoint to the {@code SubModelElementCollection}.
	 * 
	 * @param endpoint the endpoint to set
	 */
	public void setEndpoint(Endpoint endpoint) {
		this.endpoint = endpoint;
	}
	
	
	/**
	 * Sets the SubModelElements in the SubModelCollection in consideration also 
	 * to the parent class. This is done so that the methods in the parent class 
	 * also stay relevant and at par for the Child class (SubModelElementCollection). 
	 * 
	 * @param subElem
	 */
	private void setSuperSubmodelElements(SubmodelElement subElem) {
		super.getValues().add(subElem);
		
		logger.info("Internal call to parent class method successful.");
	}

}

