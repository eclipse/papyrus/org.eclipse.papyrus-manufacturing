/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.api.submodel.parts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.adminshell.aas.v3.model.Identifier;
import io.adminshell.aas.v3.model.Reference;
import io.adminshell.aas.v3.model.impl.DefaultConceptDescription;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.aas.api.reference.IReference;


public class ConceptDescription extends DefaultConceptDescription {
	
	private static final Logger logger = LoggerFactory.getLogger(ConceptDescription.class);
	private List<IReference> isCaseOfs = new ArrayList<>();
	
	
	/**
	 * Constructor for the class {@code ConceptDescription} with the following 
	 * mandatory parameters:-
	 * 		
	 * 	@param idShort 
	 * 
	 * [Note]: The category of a Concept Description is by default set to 
	 * {@code PROPERTY} according to page 80, version 3
	 */
	public ConceptDescription(String idShort) {
		super();
		super.setIdShort(idShort);
		super.setCategory(Category.PROPERTY.toString());
		
		logger.info("ConceptDescription: " + idShort + " initialized.");
	}
	
	/**
	 * Constructor for the class {@code ConceptDescription} with the following 
	 * mandatory parameters:-
	 * 		
	 * 	@param idShort 
	 * 	@param identification
	 * 
	 * [Note]: The category of a Concept Description is by default set to 
	 * {@code PROPERTY} according to page 80, version 3
	 */
	public ConceptDescription(String idShort, Identifier identification) {
		super();
		super.setIdShort(idShort);
		super.setIdentification(identification);
		super.setCategory(Category.PROPERTY.toString());
		
		logger.info("ConceptDescription: " + idShort + " initialized.");
	}
	
	/**
	 * Sets the category of the {@code ConceptDescription} in consideration.
	 *   
	 * @param conceptCategory
	 */
	public void setCategory(Category conceptCategory) {
		super.setCategory(conceptCategory.toString()); 
		
		logger.info("Category: " + conceptCategory.toString() + " set to ConceptDescription: " 
				+ idShort);
	}
	
	/**
	 * Sets the {@code ConceptDescription} under consideration a Reference.
	 * The {@code ConceptDescription} under consideration is a case of this 
	 * Reference. 
	 * 
	 * @param isCaseOf
	 */
	public void setIsCaseOf(IReference isCaseOf) {
		this.isCaseOfs.add(isCaseOf);
	}
	
	/**
	 * Sets the {@code ConceptDescription} under consideration multiple References.
	 * The {@code ConceptDescription} under consideration is a case of all these 
	 * References. 
	 * 
	 * @param isCaseOf
	 */
	public void setIsCaseOf(IReference... isCaseOfs) {
		for (IReference isCaseOf : isCaseOfs) {
			this.isCaseOfs.add(isCaseOf);
		}
	}
	
	/**
	 * Sets the {@code ConceptDescription} under consideration multiple References.
	 * The {@code ConceptDescription} under consideration is a case of all these 
	 * References. 
	 * 
	 * @param isCaseOfs
	 */
	public void setIsCaseOf(List<IReference> isCaseOfs) {
		this.isCaseOfs = isCaseOfs;
	}
	
	/**
	 * Gets the List of References the {@code ConceptDescription} under
	 * consideration is a case of.
	 * 
	 * @return	A List of References. 
	 */
	public List<IReference> getIsCaseOf() {
		
		return this.isCaseOfs;
	}
	
	@Override @Deprecated
	public void setIsCaseOfs(List<Reference> isCaseOfs) {
		
	}
	
	@Override @Deprecated
	public List<Reference> getIsCaseOfs() {
		return super.isCaseOfs;
	}
	
	/**
	 * @param conceptCategory
	 */
	@Override @Deprecated
	public void setCategory(String conceptCategory) {
		
	}
	
}
