/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.api.submodel.submodelelement.dataelement.valuetypes;

public enum ValueType {
	Int8, 
	Int16, 
	Int32, 
	Int64,
	UInt8, 
	UInt16, 
	UInt32, 
	UInt64,
	String, 
	LangString,
	AnyURI, 
	Base64Binary, 
	HexBinary, 
	NOTATION, 
	ENTITY, ID, IDREF,
	Integer, NonPositiveInteger, 
	NonNegativeInteger, 
	PositiveInteger, NegativeInteger,
	Double, Float, Boolean,
	Duration, DayTimeDuration, YearMonthDuration,
	DateTime, DateTimeStamp, GDay, GMonth, GMonthDay, 
	GYear, GYearMonth,
	QName,
	None, AnyType, AnySimpleType;
}
