/*******************************************************************************
 * Copyright (c) 2023 DFKI.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     DFKI - Tapanta Bhanja <tapanta.bhanja@dfki.de>
 *******************************************************************************/
package org.eclipse.aas.api.aas.parts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.aas.api.submodel.parts.ConceptDescription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConceptDictionary {
	
	private static final Logger logger = LoggerFactory.getLogger(ConceptDictionary.class);
	
	private List<ConceptDescription> conceptDescriptions = new ArrayList<ConceptDescription>();
	
	/**
	 * Constructor for the class {@code ConceptDictionary} with no parameters.
	 */
	public ConceptDictionary() {
		logger.info("ConceptDictionary Initialised with no parameters.");
	}
	
	/**
	 * Constructor for the class {@code ConceptDictionary} with a single instance
	 * of {@code ConceptDescription}.
	 * 
	 * @param conceptDesc	An instance of ConceptDescription. 
	 */
	public ConceptDictionary(ConceptDescription conceptDesc) {
		this.conceptDescriptions.add(conceptDesc);
	}
	
	/**
	 * Constructor for the class {@code ConceptDictionary} with multiple instances
	 * of {@code ConceptDescription}.
	 * 
	 * @param conceptDescs	Multiple instances of ConceptDescription. 
	 */
	public ConceptDictionary(ConceptDescription... conceptDescs) {
		
		for (ConceptDescription conceptDesc : conceptDescs) {
			this.conceptDescriptions.add(conceptDesc);
		}
		
	}
	
	/**
	 * Constructor for the class {@code ConceptDictionary} with a {@code List}
	 * of ConceptDescription instances. 
	 * 
	 * @param conceptDescs	A list of ConceptDescriptions. 
	 */
	public ConceptDictionary(List<ConceptDescription> conceptDescs) {

		this.conceptDescriptions = conceptDescs;
		
	}
	
	/**
	 * Sets a {@code ConceptDescription} to the {@code ConceptDictionary} of 
	 * the Asset Administration Shell. 
	 * 
	 * @param conceptDesc	An instance of ConceptDescription. 
	 */
	public void setConceptDescription(ConceptDescription conceptDesc) {
		this.conceptDescriptions.add(conceptDesc);
	}
	
	/**
	 * Sets multiple {@code ConceptDescription}s  to the {@code ConceptDictionary}
	 * of the Asset Administration Shell. 
	 * 
	 * @param conceptDescs	Multiple instances of ConceptDescription. 
	 */
	public void setConceptDescriptions(ConceptDescription... conceptDescs) {
		
		for (ConceptDescription conceptDesc : conceptDescs) {
			this.conceptDescriptions.add(conceptDesc);
		}
		
	}
	
	/**
	 * Sets multiple {@code ConceptDescription}s, but as a {@code List} to the
	 * {@code ConceptDictionary} of the Asset Administration Shell. 
	 * 
	 * @param conceptDescs	A list of ConceptDescriptions. 
	 */
	public void setConceptDescriptions(List<ConceptDescription> conceptDescs) {
		this.conceptDescriptions = conceptDescs;
	}
	
	/**
	 * Gets a {@code List} of {@code ConceptDescription}s that are known to the 
	 * {@code ConceptDictionary} of the Asset Administration Shell under 
	 * consideration.
	 * 
	 * @return	A {@code List} of ConceptDescriptions for the 
	 * {@code ConceptDictionary} under consideration.
	 */
	public List<ConceptDescription> getConceptDescriptions() {
		return this.conceptDescriptions;
	}

}

