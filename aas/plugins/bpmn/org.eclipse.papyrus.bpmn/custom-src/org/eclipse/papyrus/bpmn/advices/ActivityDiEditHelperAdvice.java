/*****************************************************************************
 * Copyright (c) 2021 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.bpmn.advices;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.GetEditContextRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest;
import org.eclipse.papyrus.infra.core.resource.ModelSet;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.emf.utils.ServiceUtilsForEObject;
import org.eclipse.papyrus.infra.gmfdiag.common.model.NotationUtils;
import org.eclipse.uml2.uml.Element;

/**
 * @author YH262742
 *
 */
public class ActivityDiEditHelperAdvice extends AbstractEditHelperAdvice{
	
	private Element previousContainer;
	@Override
	public void configureRequest(IEditCommandRequest request) {
		request.getClass();
		request.toString();
	}
	@Override
	protected ICommand getBeforeEditContextCommand(GetEditContextRequest request) {
		IEditCommandRequest editCommandRequest = request.getEditCommandRequest();
		if (editCommandRequest instanceof CreateElementRequest) {
			IElementType type = ((CreateElementRequest) editCommandRequest).getElementType();
			EObject container = ((CreateElementRequest) editCommandRequest).getContainer();
		}
		return super.getBeforeEditContextCommand(request);

	}
	
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		EObject newElement = request.getElementToConfigure();
		ModelSet modelSet;
		try {
			modelSet = ServiceUtilsForEObject.getInstance().getModelSet(newElement);
			Resource resource = NotationUtils.getNotationResource(modelSet);
			
		} catch (ServiceException ex) {
			ex.printStackTrace();
		}
		return super.getAfterConfigureCommand(request);
	}
}
