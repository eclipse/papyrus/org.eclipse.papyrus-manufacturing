/*****************************************************************************
 * Copyright (c) 2021 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.bpmn.advices;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.commands.ConfigureElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.CreateElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.papyrus.infra.core.resource.ModelSet;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.emf.utils.ServiceUtilsForEObject;
import org.eclipse.papyrus.infra.gmfdiag.common.model.NotationUtils;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Element;

/**
 * @author YH262742
 *
 */
public class ActivityConfigurationCommand extends ConfigureElementCommand {

	private final org.eclipse.uml2.uml.Activity activity;
	
	private IProgressMonitor progressMonitor;
	
	private IAdaptable info;
	
	private IElementType elementType;
	
	private final IElementType operationElementType;
	/**
	 * Constructor.
	 *
	 * @param request
	 */
	public ActivityConfigurationCommand(ConfigureRequest request) {
		super(request);
		this.activity = (Activity)request.getElementToConfigure();
		this.elementType = request.getTypeToConfigure();
		this.operationElementType = ElementTypeRegistry.getInstance().getType("org.eclipse.papyrus.aAS.Operation_Operation");
	}
	
	


	/**
	 * @see org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand#doExecuteWithResult(org.eclipse.core.runtime.IProgressMonitor, org.eclipse.core.runtime.IAdaptable)
	 *
	 * @param monitor
	 * @param info
	 * @return
	 * @throws ExecutionException
	 */
	@Override
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		this.progressMonitor = monitor;
		this.info = info;
		
		createElement(activity, operationElementType);
		
		return CommandResult.newOKCommandResult(activity);
	}
	
	protected EObject createElement(Activity activity, IElementType elementType) throws ExecutionException {
		if (activity == null) {
			throw new ExecutionException("element is null");
		}
		EObject newElement = null;
		EObject container = activity.getOwner();
		if (container != null) {
			// create a new operation for this class
			CreateElementRequest createElementRequest = new CreateElementRequest(container,operationElementType);
			CreateElementCommand command = new CreateElementCommand(createElementRequest);
			command.execute(progressMonitor, info);
			newElement = command.getNewElement();
			if (newElement != null) {
				Operation newOperation = (Operation)newElement;
				newOperation.getMethods().add(activity);
				newOperation.setName("workflow");
			}else {
				throw new ExecutionException("Operation creation problem!");
			}
		}
		
		return newElement;
	}

}
