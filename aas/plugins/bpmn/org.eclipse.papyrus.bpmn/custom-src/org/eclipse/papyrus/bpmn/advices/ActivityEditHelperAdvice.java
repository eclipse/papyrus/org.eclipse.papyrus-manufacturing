/*****************************************************************************
 * Copyright (c) 2021 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.bpmn.advices;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.papyrus.infra.core.resource.ModelSet;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.emf.utils.ServiceUtilsForEObject;
import org.eclipse.papyrus.infra.gmfdiag.common.model.NotationUtils;

/**
 * @author YH262742
 *
 */
public class ActivityEditHelperAdvice extends AbstractEditHelperAdvice{

	/**
	 * Constructor.
	 *
	 */
	public ActivityEditHelperAdvice() {
		super();
	}
	
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		EObject element = request.getElementToConfigure();
		
		ModelSet modelSet;
		try {
			modelSet = ServiceUtilsForEObject.getInstance().getModelSet(element);
			Resource resource = NotationUtils.getNotationResource(modelSet);
			EList<Resource> resources = modelSet.getResources();
			System.out.println(resource);
		} catch (ServiceException ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	@Override
	protected ICommand getBeforeConfigureCommand(ConfigureRequest request) {
		return new ActivityConfigurationCommand(request);
	}
}
